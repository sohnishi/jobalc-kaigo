<?php
App::uses('AppController', 'Controller');
class SitemapController extends AppController
{
    public $uses = Array('Recruits.Recruit');
 
    public function index() {

        $this->layout = '';
        
        $data = $this->{$this->modelClass}->find('all');
        $this->set('data', $data);

        // 施設形態リストを取得
		$shisetsukeitaiFirsts = $this->{$this->modelClass}->Facility->ShisetsukeitaiFirst->find('list', array('order' => 'ShisetsukeitaiFirst.weight ASC'));
        $this->set('shisetsukeitaiFirsts', $shisetsukeitaiFirsts);
        
        $shokushu_options = Configure::read('shokushu');
        $this->set('shokushu_options', $shokushu_options);

        $kinmu_keitai_options = Configure::read('kinmu_keitai');
        $this->set('kinmu_keitai_options', $kinmu_keitai_options);

        $shikaku_options = Configure::read('shikaku');
        $this->set('shikaku_options', $shikaku_options);
    }
}
?>