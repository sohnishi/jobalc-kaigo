<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
Router::parseExtensions();
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/', array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'index'));
	/* 
		sitemap.xml
	*/
	Router::connect('/sitemap.xml', array('controller' => 'sitemap', 'action' => 'sitemap'));
	/* 
		施設
		page:1に対応させるため、末尾/*
	*/
	Router::connect('/facility/:id/*', array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results'), array("id"=>"[0-9]+"));
	/* 勤務形態 */
	Router::connect('/employment/:id/*', array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results'), array("id"=>"[0-9]+"));
	/* 
		エリア
		id=>都道府県
		都道府県検索時のpage:1対応は下記のcityで受け取り、値にpage:が含まれているか判定する
	*/
	Router::connect('/area/:id', array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results'));
	/* 
		エリア
		id=>都道府県
		city=>市区町村
	*/
	Router::connect('/area/:id/:city/*', array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results'));
	/* 資格 */
	Router::connect('/qualification/:id/*', array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results'), array("id"=>"[0-9]+"));
	/* 職種 */
	Router::connect('/job/:id/*', array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results'), array("id"=>"[0-9]+"));
	/* こだわり */
	Router::connect('/feature/:id/*', array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results'), array("id"=>"[0-9]+"));
	// 未入力検索　/favoritesより上に記載しないと未入力検索時に/favoritesでリダイレクトされてしまう
	Router::connect('/recruits/results/*', array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results'));
	/* お気に入り */
	Router::connect('/favorites', array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results'));
	
	/* 
		登録フォーム
	*/
	Router::connect('/entry-form', array('plugin' => 'forms', 'controller' => 'forms', 'action' => 'display', 'admin' => false));
	/* 
		登録フォーム from詳細 or 一覧
	*/
	Router::connect('/entry-form/:id/*', array('plugin' => 'forms', 'controller' => 'forms', 'action' => 'display', 'admin' => false), array("id"=>"[0-9]+"));
	/* 
		登録フォーム POST
	*/
	Router::connect('/entry-form/add', array('plugin' => 'forms', 'controller' => 'forms', 'action' => 'forms', 'admin' => false));
	/* 
		登録フォーム POST $ ID
	*/
	Router::connect('/entry-form/add/:id', array('plugin' => 'forms', 'controller' => 'forms', 'action' => 'forms', 'admin' => false), array("id"=>"[0-9]+"));


	/* 
		相談フォーム
	*/
	Router::connect('/consultation-form', array('plugin' => 'forms', 'controller' => 'forms', 'action' => 'display', 'admin' => false)); 
	/* 
		相談フォーム POST
	*/
	Router::connect('/consultation-form/add', array('plugin' => 'forms', 'controller' => 'forms', 'action' => 'forms', 'admin' => false));


	/*
		非公開求人フォーム
	*/
	Router::connect('/private-form', array('plugin' => 'forms', 'controller' => 'forms', 'action' => 'display', 'admin' => false));
	/* 
		非公開求人フォーム from詳細
	*/
	Router::connect('/private-form/:id/*', array('plugin' => 'forms', 'controller' => 'forms', 'action' => 'display', 'admin' => false), array("id"=>"[0-9]+"));
	/* 
		非公開求人フォーム POST
	*/
	Router::connect('/private-form/add', array('plugin' => 'forms', 'controller' => 'forms', 'action' => 'forms', 'admin' => false));
	/* 
		非公開求人フォーム POST $ ID
	*/
	Router::connect('/private-form/add/:id', array('plugin' => 'forms', 'controller' => 'forms', 'action' => 'forms', 'admin' => false), array("id"=>"[0-9]+"));


	/* 
		給与相場フォーム
	*/
	Router::connect('/average-form', array('plugin' => 'forms', 'controller' => 'forms', 'action' => 'display', 'admin' => false));
	/* 
		給与相場フォーム from詳細
	*/
	Router::connect('/average-form/:id/*', array('plugin' => 'forms', 'controller' => 'forms', 'action' => 'display', 'admin' => false), array("id"=>"[0-9]+"));
	/* 
		給与相場フォーム POST
	*/
	Router::connect('/average-form/add', array('plugin' => 'forms', 'controller' => 'forms', 'action' => 'forms', 'admin' => false));
	/* 
		給与相場フォーム POST $ ID
	*/
	Router::connect('/average-form/add/:id', array('plugin' => 'forms', 'controller' => 'forms', 'action' => 'forms', 'admin' => false), array("id"=>"[0-9]+"));

	
	
	
	Router::connect('/admin', array('plugin' => 'facilities', 'controller' => 'facilities', 'action' => 'index', 'admin' => true));
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
	Router::connect('/corporation', array('controller' => 'pages', 'action' => 'display', 'corporation'));
	Router::connect('/terms', array('controller' => 'pages', 'action' => 'display', 'terms'));
	Router::connect('/privacy', array('controller' => 'pages', 'action' => 'display', 'privacy'));
	Router::connect('/contact', array('controller' => 'pages', 'action' => 'contact'));
	Router::connect('/forms', array('controller' => 'pages', 'action' => 'forms'));
	Router::connect('/guide', array('controller' => 'pages', 'action' => 'display', 'guide'));

	

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
