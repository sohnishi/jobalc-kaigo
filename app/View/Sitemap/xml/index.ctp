<?php
echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

    <url>
        <loc><?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'index'), true); ?></loc>
        <lastmod><?php 
                $date = new DateTime();
                $date->setTimezone(new DateTimeZone('Asia/Tokyo'));
                echo $date->format(DateTime::W3C); 
            ?></lastmod>
        <changefreq>daily</changefreq>
        <priority>1.0</priority>
    </url>

    <url>
        <loc><?php 
            $url = $this->Html->url(array('controller' => 'entry-form'), true);
            $url = $url . '/';
            echo $url;
        ?></loc>
        <lastmod><?php 
                $date = new DateTime();
                $date->setTimezone(new DateTimeZone('Asia/Tokyo'));
                echo $date->format(DateTime::W3C); 
            ?></lastmod>
        <changefreq>daily</changefreq>
        <priority>0.5</priority>
    </url>

    <url>
        <loc><?php 
            $url = $this->Html->url(array('controller' => 'private-form'), true);
            $url = $url . '/';
            echo $url;
        ?></loc>
        <lastmod><?php 
                $date = new DateTime();
                $date->setTimezone(new DateTimeZone('Asia/Tokyo'));
                echo $date->format(DateTime::W3C); 
            ?></lastmod>
        <changefreq>daily</changefreq>
        <priority>0.5</priority>
    </url>

    <url>
        <loc><?php 
            $url = $this->Html->url(array('controller' => 'average-form'), true);
            $url = $url . '/';
            echo $url;
        ?></loc>
        <lastmod><?php 
                $date = new DateTime();
                $date->setTimezone(new DateTimeZone('Asia/Tokyo'));
                echo $date->format(DateTime::W3C); 
            ?></lastmod>
        <changefreq>daily</changefreq>
        <priority>0.5</priority>
    </url>

    <url>
        <loc><?php 
            $url = $this->Html->url(array('controller' => 'consultation-form'), true);
            $url = $url . '/';
            echo $url;
        ?></loc>
        <lastmod><?php 
                $date = new DateTime();
                $date->setTimezone(new DateTimeZone('Asia/Tokyo'));
                echo $date->format(DateTime::W3C); 
            ?></lastmod>
        <changefreq>daily</changefreq>
        <priority>0.5</priority>
    </url>
    

<?php for ($i=1; $i<=47; $i++) { ?>
    <url>
        <loc><?php echo $this->Html->url(array('controller' => 'area', 'action' => 'pref' . $i), true); ?></loc>
        <lastmod><?php 
                $date = new DateTime();
                $date->setTimezone(new DateTimeZone('Asia/Tokyo'));
                echo $date->format(DateTime::W3C); 
            ?></lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.8</priority>
    </url>
<?php } ?>
<?php
    $pref_cities = array(
        'pref1' => 'city1',
        'pref4' => 'city2',
        'pref15' => 'city3',
        'pref12' => 'city4',
        'pref11' => 'city5',
        'pref14' => 'city6',
        'pref14' => 'city7',
        'pref14' => 'city8',
        'pref22' => 'city9',
        'pref22' => 'city10',
        'pref23' => 'city11',
        'pref26' => 'city12',
        'pref27' => 'city13',
        'pref27' => 'city14',
        'pref28' => 'city15',
        'pref33' => 'city16',
        'pref34' => 'city17',
        'pref40' => 'city18',
        'pref40' => 'city19',
        'pref43' => 'city20',
    );
?>

<?php foreach ($pref_cities as $key => $value): ?>
    <url>
        <loc><?php echo $this->Html->url(array('plugin' => 'area', 'controller' => $key, 'action' => $value), true); ?></loc>
        <lastmod><?php 
                $date = new DateTime();
                $date->setTimezone(new DateTimeZone('Asia/Tokyo'));
                echo $date->format(DateTime::W3C); 
            ?></lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.7</priority>
    </url>
<?php endforeach; ?>


<?php foreach ($shisetsukeitaiFirsts as $key => $value): ?>
    <url>
        <loc><?php echo $this->Html->url(array('controller' => 'facility', 'action' => $key), true); ?></loc>
        <lastmod><?php 
                $date = new DateTime();
                $date->setTimezone(new DateTimeZone('Asia/Tokyo'));
                echo $date->format(DateTime::W3C); 
            ?></lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.8</priority>
    </url>
<?php endforeach; ?>



<?php
    foreach ($shokushu_options as $key => $value):
?>
    <url>
        <loc><?php 
                $url = $this->Html->url(array('controller' => 'job', 'action' => $key), true);
                if ($key == 0) {
                    $url = $url . '/0';
                }
                echo $url;
            ?></loc>
        <lastmod><?php 
                $date = new DateTime();
                $date->setTimezone(new DateTimeZone('Asia/Tokyo'));
                echo $date->format(DateTime::W3C); 
            ?></lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.8</priority>
    </url>
<?php endforeach; ?>


<?php 
    foreach ($kinmu_keitai_options as $key => $value):
?>
    <url>
        <loc><?php 
                $url = $this->Html->url(array('controller' => 'employment', 'action' => $key), true);
                if ($key == 0) {
                    $url = $url . '/0';
                }
                echo $url;
            ?></loc>
        <lastmod><?php 
                $date = new DateTime();
                $date->setTimezone(new DateTimeZone('Asia/Tokyo'));
                echo $date->format(DateTime::W3C); 
            ?></lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.8</priority>
    </url>
<?php endforeach; ?>



<?php 
    foreach ($shikaku_options as $key => $value):
?>
    <url>
        <loc><?php 
                $url = $this->Html->url(array('controller' => 'qualification', 'action' => $key), true);
                if ($key == 0) {
                    $url = $url . '/0';
                }
                echo $url;
            ?></loc>
        <lastmod><?php 
                $date = new DateTime();
                $date->setTimezone(new DateTimeZone('Asia/Tokyo'));
                echo $date->format(DateTime::W3C); 
            ?></lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.8</priority>
    </url>
<?php endforeach; ?>


<?php foreach ($data as $d): ?>
    <url>
        <loc><?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'detail', 'action' => $d['Recruit']['id']), true); ?></loc>
        <lastmod><?php 
                $date = new DateTime($d['Recruit']['modified']);
                echo $date->format(DateTime::W3C); 
            ?></lastmod>
        <changefreq>daily</changefreq>
        <priority>0.5</priority>
    </url>
<?php endforeach; ?>
</urlset>
