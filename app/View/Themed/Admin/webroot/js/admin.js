/**
 * Admin Scripts
 */

$(function () {
	var nav = $('#nav-wrapper'),
		offset = nav.offset();

	$(window).scroll(function () {
		if ($(window).scrollTop() > offset.top) {
			nav.addClass('navbar-fixed-top');
			$('#page-wrapper').css('padding-top','47px');
		} else {
			nav.removeClass('navbar-fixed-top');
			$('#page-wrapper').css('padding-top','');
		}
	});
});

