﻿/*
 Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
*/
CKEDITOR.addTemplates("default",{imagesPath:CKEDITOR.getUrl(CKEDITOR.plugins.getPath("templates")+"templates/images/"),templates:[{title:"テンプレート❶",image:"template01.png",description:"",html:'<div class="h3_title"><span class="icon001"><img src="/cw/theme/Jobalc/img/icon013.svg" alt=""></span><h3 class="top_h3">スタッフから一言</h3></div><div class="recruit_text"><p>スタッフからの一言です。<br><br>改行テストを行います。</p></div>'}]});
// CKEDITOR.addTemplates("default",{imagesPath:CKEDITOR.getUrl(CKEDITOR.plugins.getPath("templates")+"templates/images/"),templates:[{title:"囲いコメント",image:"",description:"",html:'<!--PREFECTURE_CONTENT_START-->この中に記述されたHTMLのみ保存が可能です。<!--PREFECTURE_CONTENT_END-->'}]});