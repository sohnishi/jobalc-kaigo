<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $this->fetch('title'); ?></title>
<?php
	echo $this->Html->meta('icon');

	echo $this->fetch('meta');

	echo $this->Html->css(array(
		'bootstrap',
		'admin'
	));

	echo $this->fetch('css');
?>

<!--[if lt IE 9]><?php echo $this->Html->script('html5shiv'); ?><![endif]-->

<?php
	echo $this->Html->script(array(
		'jquery.min',
		'bootstrap.min',
		'admin'
	));
?>
<?php echo $this->fetch('script'); ?>

</head>
<body>
<div class="container">
	<div class="row">
		<?php echo $this->fetch('content'); ?>
	</div>
</div>
<?php //echo $this->element('sql_dump'); ?>
</body>
</html>
