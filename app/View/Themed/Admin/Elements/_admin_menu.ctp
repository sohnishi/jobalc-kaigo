<div id="nav-wrapper" class="navbar navbar-default" role="navigation">
	<div class="container">
		<ul class="nav navbar-top-links">
			<li class="dropdown">
				<a href="<?php echo $this->Html->url('/admin/facilities'); ?>" class="dropdown-toggle" data-toggle="dropdown">施設管理 <i class="fa fa-caret-down"></i></a>
				<ul class="dropdown-menu">
					<li><a href="<?php echo $this->Html->url('/admin/facilities'); ?>">施設検索</a></li>
					<li><a href="<?php echo $this->Html->url('/admin/facilities/add'); ?>">施設登録</a></li>
					<li><a href="<?php echo $this->Html->url('/admin/facilities/import'); ?>">施設情報CSV取込</a></li>
				</ul>
			</li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">求人管理 <i class="fa fa-caret-down"></i></a>
				<ul class="dropdown-menu">
					<li><a href="<?php echo $this->Html->url('/admin/recruits'); ?>">求人検索</a></li>
					<li><a href="<?php echo $this->Html->url('/admin/recruits/add'); ?>">求人登録</a></li>
					<li><a href="<?php echo $this->Html->url('/admin/recruits/import'); ?>">求人CSV取込</a></li>
				</ul>
			</li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">顧客管理 <i class="fa fa-caret-down"></i></a>
				<ul class="dropdown-menu">
					<li><a href="<?php echo $this->Html->url('/admin/customers'); ?>">顧客検索</a></li>
					<li><a href="<?php echo $this->Html->url('/admin/customers/add'); ?>">顧客登録</a></li>
				</ul>
			</li>
			<li class="dropdown">
				<a href="<?php echo $this->Html->url('/admin/users'); ?>" class="dropdown-toggle" data-toggle="dropdown">アカウント管理 <i class="fa fa-caret-down"></i></a>
				<ul class="dropdown-menu">
					<li><a href="<?php echo $this->Html->url('/admin/users'); ?>">管理者一覧</a></li>
					<li><a href="<?php echo $this->Html->url('/admin/users/add'); ?>">管理者登録</a></li>
				</ul>
			</li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">環境設定 <i class="fa fa-caret-down"></i></a>
				<ul class="dropdown-menu">
					<li><a href="<?php echo $this->Html->url('/admin/facilities/groups'); ?>">グループ名マスタ</a></li>
					<li><a href="<?php echo $this->Html->url('/admin/facilities/corporations'); ?>">法人名マスタ</a></li>
					<li><a href="<?php echo $this->Html->url('/admin/ekidata/railroad_preves'); ?>">都道府県マスタ</a></li>
					<li><a href="<?php echo $this->Html->url('/admin/ekidata/railroad_lines'); ?>">路線マスタ</a></li>
					<li><a href="<?php echo $this->Html->url('/admin/ekidata/railroad_stations'); ?>">駅マスタ</a></li>
					<li><a href="<?php echo $this->Html->url('/admin/staffs'); ?>">担当者マスタ</a></li>
					<li><a href="<?php echo $this->Html->url('/admin/contacts'); ?>">連絡履歴マスタ</a></li>
					<li><a href="<?php echo $this->Html->url('/admin/facilities/shisetsukeitai_firsts'); ?>">施設形態大区分マスタ</a></li>
					<li><a href="<?php echo $this->Html->url('/admin/facilities/shisetsukeitai_seconds'); ?>">施設形態中区分マスタ</a></li>
					<li><a href="<?php echo $this->Html->url('/admin/facilities/shinryoukamokus'); ?>">診療科目マスタ</a></li>
					<li><a href="<?php echo $this->Html->url('/admin/recruits/nensyus'); ?>">給与（年収）マスタ</a></li>
					<li><a href="<?php echo $this->Html->url('/admin/recruits/kyujitsus'); ?>">休日体制マスタ</a></li>
					<li><a href="<?php echo $this->Html->url('/admin/recruits/kodawaris'); ?>">こだわりマスタ</a></li>
				</ul>
			</li>
		</ul>
	</div>
</div>