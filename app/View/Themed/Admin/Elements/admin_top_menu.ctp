<div class="header-nav pull-right">
	<ul class="nav nav-pills">
		<li class="dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"><?php echo $loggedUser['name']; ?></a>
			<ul class="dropdown-menu">
				<li><?php echo $this->Html->link('管理者の更新', array('plugin' => 'users', 'controller' => 'users', 'action' => 'edit', $loggedUser['id'], 'admin' => true)); ?></li>
				<li class="divider"></li>
				<li><a href="<?php echo $this->Html->url('/admin/logout'); ?>">ログアウト</a></li>
			</ul>
		</li>
		<li><a href="<?php echo $this->Html->url('/'); ?>" target="_blank">サイト表示</a></li>
	</ul>
</div>
