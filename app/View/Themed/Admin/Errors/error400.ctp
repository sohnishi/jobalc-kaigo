<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Errors
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<?php $this->set('title_for_layout', 'エラーが発生しました'); ?>
<div class="row">
	<div class="col-xs-12">
		<h2>お探しのページはありません</h2>
		<p>リクエストされた"<?php echo urldecode($url); ?>"はサイト内で見つかりません。</p>
		<p>URLを直接入力された場合は間違いがないかご確認ください。</p>
	</div>
</div>
