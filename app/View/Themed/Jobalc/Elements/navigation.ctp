<nav id="main-menu" class="collapse navbar-collapse">
	<div class="container">
		<ul class="nav navbar-nav">
			<li><a href="<?php echo $this->Html->url('/'); ?>">TOP</a></li>
			<li><a href="<?php echo $this->Html->url('/recruits/search'); ?>">求人を探す</a></li>
			<li><a href="<?php echo $this->Html->url('/guide'); ?>">ご利用ガイド</a></li>
			<li><a href="#">スタッフBLOG</a></li>
			<li class="entry"><a href="<?php echo $this->Html->url('/customers/forms'); ?>"><i class="fa fa-caret-right"></i>【無料】登録して就職相談をする</a></li>
		</ul>
	</div>
</nav>

