<div class="search widget">
	<h4><i class="fa fa-heart"></i> キーワードから探す</h4>
	<?php
		echo $this->Form->create('Recruit', array(
			'inputDefaults' => array(
				'label' => false,
				'wrapInput' => false,
				'div' => false,
				'class' => 'form-control input-lg'
			),
			'class' => 'form-inline',
			'url' => array(
				'plugin' => 'recruits',
				'controller' => 'recruits',
				'action' => 'results'
			)
		));
	?>
	<div class="form-group">
		<div class="input-group">
			<?php echo $this->Form->input('keyword', array('type' => 'text')); ?>
			<div class="input-group-addon"><button type="submit" class="btn"><i class="fa fa-search"></i></button></div>
		</div>
	</div>
	<?php echo $this->Form->end(); ?>
</div>
