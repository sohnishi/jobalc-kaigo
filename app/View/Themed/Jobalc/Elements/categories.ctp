<div class="category widget">
	<h4><i class="fa fa-heart"></i> 人気カテゴリーから探す</h4>
	<ul class="list-unstyled">
		<li><a href="<?php echo $this->Html->url('/recruits/results?kinmu_keitai[0]=1&kinmu_keitai[1]=4'); ?>"><?php echo $this->Html->image('category1.jpg'); ?></a></li>
		<li><a href="<?php echo $this->Html->url('/recruits/results?kodawari[0]=6'); ?>"><?php echo $this->Html->image('category2.jpg'); ?></a></li>
		<li><a href="<?php echo $this->Html->url('/recruits/results?kinmu_keitai[0]=4'); ?>"><?php echo $this->Html->image('category3.jpg'); ?></a></li>
		<li><a href="<?php echo $this->Html->url('/recruits/results?shikaku[0]=1'); ?>"><?php echo $this->Html->image('category4.jpg'); ?></a></li>
	</ul>
</div>
