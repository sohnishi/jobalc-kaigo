<div class="list widget">
	<h4><i class="fa fa-heart"></i> 資格から探す</h4>
	<ul>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shikaku[0]=0')); ?>">看護師</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shikaku[0]=1')); ?>">準看護師</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shikaku[0]=2')); ?>">保健師</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shikaku[0]=3')); ?>">助産師</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shikaku[0]=4')); ?>">介護福祉士</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shikaku[0]=6')); ?>">介護支援専門員</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shikaku[0]=7&shikaku[1]=8')); ?>">ヘルパー1級・2級</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shikaku[0]=9')); ?>">介護職員実務者研修</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shikaku[0]=10')); ?>">介護職員初任者研修</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shikaku[0]=11')); ?>">介護職員基礎研修</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shikaku[0]=13')); ?>">理学療法士</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shikaku[0]=14')); ?>">作業療法士</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shikaku[0]=15')); ?>">言語聴覚士</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shikaku[0]=16')); ?>">臨床検査技師</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shikaku[0]=17')); ?>">臨床工学技士</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shikaku[0]=18')); ?>">診療放射線技師</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shikaku[0]=19')); ?>">薬剤師</a></li>
	</ul>
</div>

<div class="list widget">
	<h4><i class="fa fa-heart"></i> 働き方から探す</h4>
	<ul>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'kinmu_keitai[0]=0')); ?>">常勤（夜勤あり）</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'kinmu_keitai[0]=1')); ?>">常勤（日勤のみ）</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'kinmu_keitai[0]=2')); ?>">常勤（夜勤のみ）</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'kinmu_keitai[0]=3')); ?>">夜勤アルバイト</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'kinmu_keitai[0]=4')); ?>">日勤パート</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'kinmu_keitai[0]=5')); ?>">期間限定</a></li>
	</ul>
</div>

<div class="list widget">
	<h4><i class="fa fa-heart"></i> 施設から探す</h4>
	<ul>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_first[0]=1')); ?>">病院</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_first[0]=2')); ?>">介護施設</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_first[0]=3')); ?>">クリニック</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_first[0]=4')); ?>">訪問系</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_first[0]=5')); ?>">企業・治験・学校</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_first[0]=10')); ?>">調剤薬局</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_first[0]=11')); ?>">ドラッグストア</a></li>
	</ul>
</div>

<div class="list widget">
	<h4><i class="fa fa-heart"></i> 部署から探す</h4>
	<ul>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=1')); ?>">一般病棟</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=2')); ?>">療養病棟</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=3')); ?>">回復期病棟</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=4')); ?>">地域包括ケア病棟</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=5')); ?>">障害者病棟</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=6')); ?>">緩和ケア病棟</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=7')); ?>">精神科病棟</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=8')); ?>">産科</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=9')); ?>">ICU</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=10')); ?>">HCU</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=11')); ?>">CCU</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=12')); ?>">NICU</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=13')); ?>">外来</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=14')); ?>">救急外来</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=15')); ?>">手術室</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=16')); ?>">透析</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=17')); ?>">内視鏡</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=18')); ?>">美容系</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=19')); ?>">訪問看護</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=20')); ?>">往診</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=22')); ?>">健診</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=23')); ?>">治験</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=21')); ?>">企業</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=24')); ?>">保育園</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=25')); ?>">教員・研究員</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=26')); ?>">老健</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=27')); ?>">特養</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=28')); ?>">有料老人ホーム</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=29')); ?>">(医療)ショートステイ</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=30')); ?>">(介護)ショートステイ </a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=31')); ?>">小規模多機能</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=32')); ?>">グループホーム</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=33')); ?>">デイサービス</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=34')); ?>">訪問介護</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=35')); ?>">訪問入浴</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=36')); ?>">デイケア</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=37')); ?>">居宅介護支援</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=38')); ?>">居宅療養管理</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=39')); ?>">地域包括</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=40')); ?>">理学療法</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=41')); ?>">作業療法</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=42')); ?>">言語聴覚療法</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=43')); ?>">ADL訓練</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=44')); ?>">検査室</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=45')); ?>">病理</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=46')); ?>">放射線治療</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=47')); ?>">神経科ケア</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=48')); ?>">訪問リハビリ</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=49')); ?>">調剤薬局</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=50')); ?>">ドラッグストア</a></li>
		<li><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results', '?' => 'shisetsukeitai_second_id[0]=51')); ?>">その他</a></li>
	</ul>
</div>
