<div class="section visible-xs">
	<div class="action">
		<a href="tel:0120932929" class="btn btn-default btn-lg btn-block"><span class="phone"><i class="fa fa-phone"></i></span>電話で非公開求人を紹介してもらう<span class="arrow fa fa-chevron-right"></span></a>
		<a href="<?php echo $this->Html->url('/customers/forms'); ?>" class="btn btn-default btn-lg btn-block"><span class="free">無料</span>非公開求人を紹介してもらう<span class="arrow fa fa-chevron-right"></span></a>
	</div>
</div>
