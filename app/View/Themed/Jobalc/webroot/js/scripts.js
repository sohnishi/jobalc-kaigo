/* Scripts */

$(function () {
	$('.collapse').on('shown.bs.collapse', function () {
		$(this).parent().find(".fa-plus").removeClass("fa-plus").addClass("fa-minus");
	}).on('hidden.bs.collapse', function () {
		$(this).parent().find(".fa-minus").removeClass("fa-minus").addClass("fa-plus");
	});
});

$(function () {
	$(".search-check :checked").closest("label").addClass("chekd");
	$("label").on('click', function () {
		$(".search-check label").removeClass("chekd");
		$(".search-check :checked").closest("label").addClass("chekd");
	});
});
