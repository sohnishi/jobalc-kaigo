'use strict';

{
    const sform = document.getElementById('sform');
    const search_btn = document.getElementById('modal_search');
    const search_btn_sp_header = document.getElementById('modal_search_sp_header');
    const search_btn_footer_pc = document.getElementById('modal_search_footer_pc');
    const search_btn_footer_sp = document.getElementById('modal_search_footer_sp');
    const modal_search_kuwashiku = document.getElementById('modal_search_kuwashiku');
    const area = document.getElementById('modal_area');
    const shikaku = document.getElementById('modal_shikaku');
    const shisetsu = document.getElementById('modal_shisetsu');
    const jouken = document.getElementById('modal_jouken');
    const close = document.querySelector('.modal_close.js-modal');
    const inner = document.querySelector('.modal_inner');
    const img = document.querySelectorAll('img');

    // function open_modal(s){
        // s.addEventListener('click',() => {
        //     console.log('aaaaa');
        //     sform.classList.remove('is-none');
        //     sform.classList.add('is-show');
        //     document.body.style.backgroundColor = 'rgba(0,0,0,.5)';
        //     document.body.style.overflow = 'hidden';
        //     document.getElementById('menu').style.opacity = '.5';
        //     document.querySelector('.ichiran_top_image').style.opacity = '.5';
        //     img.forEach((value) => {
        //         value.style.opacity = '.5';
        //     });
        //     document.querySelector('.search_icon').style.opacity = '1';
        // });
    // }

    // function close_modal(s){
    //     s.addEventListener('click',(e) => {
    //         sform.classList.remove('is-show');
    //         sform.classList.add('is-none');
    //         document.body.style.backgroundColor = 'rgba(0,0,0,0)';
    //         document.body.style.overflow = 'visible';
    //         document.getElementById('menu').style.opacity = '1';
    //         document.querySelector('.ichiran_top_image').style.opacity = '1';
    //         img.forEach((value) => {
    //             value.style.opacity = '1';
    //         });
    //     });
    // }
    
    var doms = [];
    doms.push(search_btn);
    doms.push(search_btn_sp_header);
    doms.push(search_btn_footer_pc);
    doms.push(search_btn_footer_sp);
    doms.push(area);
    doms.push(shikaku);
    doms.push(shisetsu);
    doms.push(jouken);

    // SP OPEN_MODAL
    $('.s_d_btn').on("click", doms, function() {
        sform.classList.remove('is-none');
        sform.classList.add('is-show');
        document.body.style.backgroundColor = 'rgba(0,0,0,.5)';
        document.body.style.overflow = 'hidden';
        var menu_sp = document.getElementById('menu_sp');
        if (menu_sp != null) {
            menu_sp.style.opacity = '.5';
        }
        // document.querySelector('.ichiran_top_image').style.opacity = '.5';
        img.forEach((value) => {
            value.style.opacity = '.5';
        });
        document.querySelector('.search_icon').style.opacity = '1';
    });

    // SP OPEN_MODAL
    $('.s_d_btn_end').on("click", doms, function() {
        sform.classList.remove('is-none');
        sform.classList.add('is-show');
        document.body.style.backgroundColor = 'rgba(0,0,0,.5)';
        document.body.style.overflow = 'hidden';
        var menu_sp = document.getElementById('menu_sp');
        if (menu_sp != null) {
            menu_sp.style.opacity = '.5';
        }
        // document.querySelector('.ichiran_top_image').style.opacity = '.5';
        img.forEach((value) => {
            value.style.opacity = '.5';
        });
        document.querySelector('.search_icon').style.opacity = '1';
    });

    // PC OPEN_MODAL
    $('.s_d_btn_pc').on("click", doms, function() {
        sform.classList.remove('is-none');
        sform.classList.add('is-show');
        document.body.style.backgroundColor = 'rgba(0,0,0,.5)';
        document.body.style.overflow = 'hidden';
        document.getElementById('menu_pc').style.opacity = '.5';
        // document.querySelector('.ichiran_top_image').style.opacity = '.5';
        img.forEach((value) => {
            value.style.opacity = '.5';
        });
        document.querySelector('.search_icon').style.opacity = '1';
    });

    // PC OPEN_MODAL
    $('.map_text_btn').on("click", modal_search_kuwashiku, function() {
        sform.classList.remove('is-none');
        sform.classList.add('is-show');
        document.body.style.backgroundColor = 'rgba(0,0,0,.5)';
        document.body.style.overflow = 'hidden';
        document.getElementById('menu_pc').style.opacity = '.5';
        // document.querySelector('.ichiran_top_image').style.opacity = '.5';
        img.forEach((value) => {
            value.style.opacity = '.5';
        });
        document.querySelector('.search_icon').style.opacity = '1';
    });

    // PC OPEN_MODAL NAVIGATION
    $('#nav_modal_search_parent').on("click", search_btn, function() {
        sform.classList.remove('is-none');
        sform.classList.add('is-show');
        document.body.style.backgroundColor = 'rgba(0,0,0,.5)';
        document.body.style.overflow = 'hidden';
        document.getElementById('menu_pc').style.opacity = '.5';
        if (document.querySelector('.ichiran_top_image')) {
            document.querySelector('.ichiran_top_image').style.opacity = '.5';
        }
        img.forEach((value) => {
            value.style.opacity = '.5';
        });
        document.querySelector('.search_icon').style.opacity = '1';
    });

    // PC OPEN_MODAL FOOTER
    $('#modal_search_footer_parent').on("click", search_btn_footer_pc, function() {
        sform.classList.remove('is-none');
        sform.classList.add('is-show');
        document.body.style.backgroundColor = 'rgba(0,0,0,.5)';
        document.body.style.overflow = 'hidden';
        document.getElementById('menu_pc').style.opacity = '.5';
        if (document.querySelector('.ichiran_top_image')) {
            document.querySelector('.ichiran_top_image').style.opacity = '.5';
        }
        if (document.querySelector('.new_job')) {
            document.querySelector('.new_job').style.opacity = '0';
        }
        if (document.querySelector('.user_voice_area')) {
            document.querySelector('.user_voice_area').style.opacity = '0';
        }
        if (document.querySelector('#last_search_details_sp')) {
            document.querySelector('#last_search_details_sp').style.opacity = '0';
        }
        img.forEach((value) => {
            value.style.opacity = '.5';
        });
        document.querySelector('.search_icon').style.opacity = '1';
    });

    // SP OPEN_MODAL FOOTER
    $('#modal_search_footer_parent_sp').on("click", search_btn_footer_sp, function() {
        sform.classList.remove('is-none');
        sform.classList.add('is-show');
        document.body.style.backgroundColor = 'rgba(0,0,0,.5)';
        document.body.style.overflow = 'hidden';
        var menu_sp = document.getElementById('menu_sp');
        if (menu_sp != null) {
            menu_sp.style.opacity = '.5';
        }
        if (document.querySelector('.ichiran_top_image')) {
            document.querySelector('.ichiran_top_image').style.opacity = '.5';
        }
        if (document.querySelector('.new_job')) {
            document.querySelector('.new_job').style.opacity = '0';
        }
        if (document.querySelector('.user_voice_area')) {
            document.querySelector('.user_voice_area').style.opacity = '0';
        }
        if (document.querySelector('#last_search_details_sp')) {
            document.querySelector('#last_search_details_sp').style.opacity = '0';
        }
        img.forEach((value) => {
            value.style.opacity = '.5';
        });
        document.querySelector('.search_icon').style.opacity = '1';
    });

    // SP/PC CLOSE_MODAL
    $('.modal_close_wrapper').on("click", close, function() {
        sform.classList.remove('is-show');
        sform.classList.add('is-none');
        document.body.style.backgroundColor = 'rgba(0,0,0,0)';
        document.body.style.overflow = 'visible';
        var menu_sp = document.getElementById('menu_sp');
        if (menu_sp != null) {
            menu_sp.style.opacity = '1';
        }
        
        document.getElementById('menu_pc').style.opacity = '1';
        if (document.querySelector('.ichiran_top_image')) {
            document.querySelector('.ichiran_top_image').style.opacity = '1';
        }
        if (document.querySelector('.new_job')) {
            document.querySelector('.new_job').style.opacity = '1';
        }
        if (document.querySelector('.user_voice_area')) {
            document.querySelector('.user_voice_area').style.opacity = '1';
        }
        if (document.querySelector('#last_search_details_sp')) {
            document.querySelector('#last_search_details_sp').style.opacity = '1';
        }
        img.forEach((value) => {
            value.style.opacity = '1';
        });
    });

    // open_modal(search_btn);
    // open_modal(area);
    // open_modal(shikaku);
    // open_modal(shisetsu);
    // open_modal(jouken);
    // close_modal(close);

    const checkbox = document.querySelectorAll('.input-checkboxInner');

    checkbox.forEach((value) => {
        const check = document.createElement('i');
        check.classList.add('fas');
        check.classList.add('fa-check');
        check.classList.add('check_icon');
        value.addEventListener('click', () => {
            if(value.classList.contains('checked')){
                value.classList.remove('checked');
                /*
                    20190531 sohnishi
                    value.removeChild(check); では追加したiタグが削除できなかったため、
                    jQueryにて対応
                */ 
                // value.removeChild(check);
                $(value).children("i").remove();
            }else{
                value.classList.add('checked');
                value.appendChild(check);
            }
        });
    });

    const chiiki_select = document.getElementById('chiiki_select');
    const rosen_select = document.getElementById('rosen_select');
    const chiiki_region = document.getElementById('chiiki_region');
    const rosen_region = document.getElementById('rosen_region');
    const chiiki_div = document.getElementById('chiiki_div');
    const rosen_div = document.getElementById('rosen_div');


    rosen_select.addEventListener('click',() => {
        rosen_region.classList.add('select');
        chiiki_region.classList.remove('select');
        rosen_div.classList.remove('u-d-n');
        chiiki_div.classList.add('u-d-n');
        // 地域選択時の都道府県、市区町村の選択値を初期化
        document.getElementById("RecruitTodoufuken").selectedIndex = 0;
        document.getElementById("RecruitShikutyouson").selectedIndex = 0;
        // name属性を外したため、手動でselect_areaのチェックを外す
        document.getElementById("select_area").checked = false;
    });

    chiiki_select.addEventListener('click',() => {
        chiiki_region.classList.add('select');
        rosen_region.classList.remove('select');
        chiiki_div.classList.remove('u-d-n');
        rosen_div.classList.add('u-d-n');
        // 地域選択時の都道府県、市区町村の選択値を初期化
        document.getElementById("RecruitPrefCd").selectedIndex = 0;
        document.getElementById("RecruitLineCd").selectedIndex = 0;
        document.getElementById("RecruitStationCd").selectedIndex = 0;
        // name属性を外したため、手動でselect_lnのチェックを外す
        document.getElementById("select_ln").checked = false;
    });


    // 条件をクリア押下時
    document.getElementById("restbtn").onclick = function() {

        isClear = true;
        // 地域 - 都道府県 以下紐づる
        $("#RecruitTodoufuken").val("").change();
        // 路線 - 都道府県 以下紐づる
        $("#RecruitPrefCd").val("").change();
        // チェックボックス類全般
        $('.reset').prop('checked',false);
        // フリーテキスト
        $(".resettext").val("");
        // チェックマーク等
        $('.input-checkboxInner.checked').removeClass('checked');
        $('.check_icon').remove();

        $('#real_time_count').html(activeCount);

        isClear = false;
        
    };
}
