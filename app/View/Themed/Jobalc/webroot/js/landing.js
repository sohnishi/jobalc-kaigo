/* landing script */

jQuery(function ($) {
    var $form = $('#landing-form');

    $form.validationEngine({
        promptPosition: 'inline',
        scroll: false,
        addSuccessCssClassToField: 'inputbox-success',
        addFailureCssClassToField: 'inputbox-error'
    });

    $form.bind('jqv.field.result', function (event, field, errorFound, prompText) {

    });

    $form.children('#landing-form-content').steps({
        headerTag: 'h3',
        bodyTag: 'section',
        transitionEffect: 'slideLeft',
        autoFocus: true,
        titleTemplate: '<span class="number">#index#</span>',
        enablePagination: false,
        onStepChanging: function (event, currentIndex, newIndex) {
            if (currentIndex > newIndex) {
                return true;
            }
            return $form.validationEngine('validate');
        },
        onStepChanged: function (event, currentIndex, newIndex) {
            if (currentIndex == 0) {
                $('#stepsPrev').addClass('disabled').attr('disabled', true);
            } else {
                $('#stepsPrev').removeClass('disabled').attr('disabled', false);
            }

            if (currentIndex == ($('.steps li').length - 1)) {
                $('#landing-form #stepsNext').css('display', 'none');
                $('#landing-form #stepsFinish').css('display', 'block');
            } else {
                $('#landing-form #stepsNext').css('display', 'block');
                $('#landing-form #stepsFinish').css('display', 'none');
            }
        },
        onFinishing: function (event, currentIndex) {
            return $form.validationEngine('validate');
        },
        onFinished: function (event, currentIndex) {
            $form.submit();
        }
    });

    // steps title
    var stepTitle = '<div class="steps-title">STEP</div>';
    $('#landing-form-content .steps').prepend(stepTitle);

    $('#stepsNext').on('click', function () {
        $form.children('#landing-form-content').steps('next');
    });
    $('#stepsPrev').on('click', function () {
        $form.children('#landing-form-content').steps('previous');
    });
    $('#stepsFinish').on('click', function () {
        $form.children('#landing-form-content').steps('finish');
    });

    $.fn.autoKana('#CustomerName', '#CustomerHurigana', {katakana: true});
});
