<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="format-detection" content="telephone=no">
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
<title><?php echo $this->fetch('title'); ?> | 友人に教えたくなる看護師の求人・転職情報サイト【Jobアルク】</title>
<?php
	echo $this->Html->meta('icon');
	echo $this->fetch('meta');
	echo $this->Html->css(array(
		'bootstrap.min',
		'base'
	));
	echo $this->fetch('css');
?>

<!--[if lt IE 9]>
<?php
	echo $this->Html->script(array(
		'html5shiv.min',
		'respond.min'
	));
?>
<script type="text/javascript">document.createElement('main');</script>
<![endif]-->

<?php
	echo $this->Html->script(array(
		'jquery-1.11.2.min',
		'bootstrap.min',
		'scripts'
	));
	echo $this->fetch('script');
?>

</head>
<body>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-TMH9GK"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TMH9GK');</script>
<!-- End Google Tag Manager -->
	<div id="body-wrapper">
		<div id="top-bar" class="text-right">
			<div class="container">
				<h1>友人に教えたくなる看護師の求人・転職情報サイト【jobアルク】</h1>
			</div>
		</div>
		<header id="header" class="navbar navbar-default">
			<div class="container">
				<div class="navbar-header">
					<button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menu">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<?php echo $this->Html->link($this->Html->image('logo.png', array('alt' => 'jobアルク')), '/', array('escape' => false)); ?>
				</div>
				<div class="information hidden-xs">
					<div class="phone">
						<div>お電話でのお問い合わせ</div>
						<div class="tel">0120-932-929</div>
						<div>【通話料無料】受付9:20～20:00</div>
					</div>
				</div>
			</div>
		</header>

		<?php echo $this->element('navigation'); ?>

		<div id="main">
			<div class="container">
				<div class="row">

					<main id="contents" class="col-sm-12">

						<div class="page-top">
							<?php echo $this->fetch('page-top'); ?>
						</div>

						<div class="page-body">

							<?php echo $this->Session->flash(); ?>
							<?php echo $this->fetch('content'); ?>

						</div>

						<div class="page-bottom">
							<?php echo $this->fetch('page-bottom'); ?>
						</div>

					</main>

				</div>
			</div>
		</div>

		<footer id="footer-wrapper">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<?php echo $this->element('footer_nav'); ?>
					</div>
					<div class="col-sm-6">
						<div class="information">
							<div class="phone">
								<div>お電話でのお問い合わせ</div>
								<div class="tel"><a href="tel:0120932929">0120-932-929</a></div>
								<div>【通話料無料】受付9:20～20:00</div>
							</div>
						</div>
					</div>
				</div>
				<div id="footer-bottom" class="text-center">
					<div>厚生大臣許可 27-ュ-201641</div>
					<address> Copyright &copy; <?php echo date('Y'); ?> ALC Co.Ltd All Rights Reserved.</address>
				</div>
			</div>
		</footer>
	</div>
</body>
</html>
