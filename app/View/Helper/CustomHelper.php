<?php

App::uses('AppHelper', 'View/Helper');

class CustomHelper extends AppHelper {

	public function birthToAge($ymd) {
		$base = new DateTime();
		$today = $base->format('Ymd');

		$birth = new DateTime($ymd);
		$birthday = $birth->format('Ymd');

		$age = (int)(($today - $birthday) / 10000);

		return $age;
	}

}
