<?php

App::uses('AppHelper', 'View/Helper');

class FastCSVHelper extends AppHelper {

	protected $handle;

	public $table = array();

	public $delimiter = ',';

	public $enclosure = '"';

	public $filename = 'ExportFile.csv';

	public $to_encoding = 'sjis';

	public $from_encoding = 'utf8';

	public function __construct(View $View, $settings = array()) {
		parent::__construct($View, $settings);
		$this->_init();
	}

	public function _init() {
		$this->handle = fopen('php://output', 'w');
	}

	public function fastExport($table, $filename = null, $modelClass = false) {
		if ($filename) {
			$this->setFilename($filename);
		}
		if (is_string($modelClass)) {
			$this->formatTable($table, $modelClass);
		} else {
			$this->set($table);
		}
		$this->export();
	}

	public function formatTable($table, $modelClass) {
		foreach ($table as $row) {
			$this->setRow($row[$modelClass]);
		}
	}

	public function setTable($table = array()) {
		$this->table = $table;
	}

	public function setRows($table = array()) {
		$this->setTable($table);
	}

	public function setRow($row = array()) {
		$this->table[] = $row;
	}

	public function setFirstRow($row = array()) {
		array_unshift($this->table, $row);
	}

	public function setLastRow($row = array()) {
		$this->setRow($row);
	}

	public function setFilename($filename) {
		$this->filename = $filename;
		if (strtolower(substr($this->filename, -4)) !== '.csv') {
			$this->filename .= '.csv';
		}
	}

	public function setHeaders() {
		header("Content-Type: application/x-csv");
		header("Content-disposition: attachment;filename=" . $this->filename);
	}

	public function export() {
		$this->setFilename($this->filename);
		$this->setHeaders();
		mb_convert_variables($this->to_encoding, $this->from_encoding, $this->table);
		foreach ($this->table as $row) {
			fputcsv($this->handle, $row, $this->delimiter, $this->enclosure);
		}
	}
}
