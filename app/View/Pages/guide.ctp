<?php echo $this->set('title_for_layout', 'ご利用ガイド'); ?>
<div class="section">
	<h2 class="page-header">ご利用ガイド</h2>
</div>

<?php $this->start('page-bottom'); ?>
<div class="section">
	<?php echo $this->Html->image('howto.jpg', array('alt' => '', 'class' => 'img-responsive')); ?>
</div>

<div class="section">
	<a href="<?php echo $this->Html->url('/customers/forms'); ?>">
		<?php echo $this->Html->image('secret.jpg', array('alt' => '', 'class' => 'img-responsive')); ?>
	</a>
</div>

<?php echo $this->element('global_contact'); ?>

<?php $this->end(); ?>

<?php

$this->start('sidebox-pc');
echo $this->element('support');
echo $this->element('search_link');
echo $this->element('guide');
echo $this->element('blog');
echo $this->element('search_form');
echo $this->element('categories');
$this->end();

$this->start('sidebox-sp');
$this->end();
