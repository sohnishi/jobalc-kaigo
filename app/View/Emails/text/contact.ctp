<?php
	// $shikaku = Configure::read('shikaku');
	$shikaku = Configure::read('form_shikaku');
	$kinmukaishijiki = Configure::read('kinmukaishijiki');
	$todoufuken = empty($Customer['todoufuken']) ? '' : $Customer['todoufuken'];
	$shikutyouson = empty($Customer['shikutyouson']) ? '' : $Customer['shikutyouson'];
	$banchi = empty($Customer['banchi']) ? '' : $Customer['banchi'];

	$tmpCertificate = $CustomerCertificate;
	$certificate = '';
	foreach ($CustomerCertificate as $val) {
		$certificate .= $shikaku[$val['shikaku']];
		if (next($tmpCertificate)) {
			$certificate .= "、";
		}
	}
	$siteUrl = '登録フォームから直接の申し込み';
	if (isset($Customer['recruit_id'])) {
		$siteUrl = $this->Html->url('/recruits/detail/' . $Customer['recruit_id'], true);
	}
?>
ジョブアルク登録通知
==============================
登録URL／<?php echo $siteUrl; ?>


お名前／<?php echo $Customer['name']; ?>


フリガナ／<?php echo $Customer['hurigana']; ?>


ご住所／郵便<?php echo $Customer['zip'] . ' '; ?>
<?php echo $todoufuken . $shikutyouson . $banchi; ?>


生年月日／<?php echo $Customer['birth']['year']; ?>年<?php echo $Customer['birth']['month']; ?>月<?php echo $Customer['birth']['day']; ?>日

お電話番号／<?php echo $Customer['keitai_tel']; ?>


メール／<?php echo empty($Customer['email']) ? '' : $Customer['email']; ?>


取得資格／<?php	echo $certificate; ?>


希望の働き方／<?php echo $CustomerCondition[0]['kibouwork']; ?>


就職希望時期／<?php echo $kinmukaishijiki[$CustomerCondition[0]['kinmukaishijiki']]; ?>


その他ご希望条件など／
<?php echo empty($Customer['memo']) ? '記載なし' : $Customer['memo']; ?>
