<?php
	/*
		20190610 sohnishi
		フォーム専用の資格に変更
	*/
	$shikaku = Configure::read('form_shikaku');
	$kinmukaishijiki = Configure::read('form_kinmukaishijiki');


	$tmpCertificate = $CustomerCertificate;
	$certificate = '';
	foreach ($CustomerCertificate as $val) {
		$certificate .= $shikaku[$val['shikaku']];
		if (next($tmpCertificate)) {
			$certificate .= "、";
		}
	}
?>
<?php echo $Customer['name']; ?>　 様

この度は、ジョブアルク転職サポートにご登録いただきまして、誠にありがとうございます。
今後の流れについての説明やご登録の内容についての確認を24時間以内に、お電話にてご連絡をさせていただきます。
しばらくお待ちくださいますようお願いいたします。

今回お問い合わせいただいている内容
－－－－－－－－－－－－－－－－－
　◆お名前／<?php echo $Customer['name']; ?>　様
　◆資格／<?php	echo $certificate . "\n"; ?>
　◆希望の働き方／<?php echo $CustomerCondition[0]['kibouwork'] . "\n"; ?>
　◆就職希望時期／<?php echo $kinmukaishijiki[$CustomerCondition[0]['kinmukaishijiki']] . "\n"; ?>
　◆お問い合わせ内容／
 <?php echo empty($Customer['memo']) ? '記載なし' : $Customer['memo']; ?>

－－－－－－－－－－－－－－－－－

また、追加のご質問などがございましたらお電話の際に、お伝え下さい。

お急ぎの場合は、お電話でもかまいません。
　【フリーダイヤル】
　　0120-932-929
　　受付：9:30～20:00

それでは、弊社からの連絡をしばらくお待ちくださいますようお願いいたします。

━━━━━━━━━━━━━━━━━
　ジョブアルク運営事務局
　【株式会社ALC（アルク）】
　受付：9:30～20:00
　0120-932-929（フリーダイヤル）
　URL：https://job-alc.com
　Mail：info@alc-medi.com

-----本社----------------------------------
　〒540-0029
　大阪市中央区本町橋5-14
　オージービル本町橋5F　
　06-6360-9505（tel）
　06-6360-9506（fax）
------------------------------------------

-----福岡支社------------------------------
　〒810-0074
　福岡市中央区大手門2-7-11
　榊原ビル6F
　092-406-6898（tel）
　092-406-6899（fax）
------------------------------------------
━━━━━━━━━━━━━━━━━

※このメールは、ジョブアルク転職サポート登録フォームから送信されました。