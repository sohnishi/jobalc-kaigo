<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

	public $components = array(
		//'DebugKit.Toolbar' => array('panels' => array('Configure')),
		'Auth' => array(
			'flash' => array(
				'element' => 'alert',
				'key' => 'auth',
				'params' => array('plugin' => 'BoostCake', 'class' => 'alert-danger')
			)
		),
		'Session',
		'Cookie',
		'Security' => array('csrfUseOnce' => false, 'csrfExpires' => '+1 hour'),
		'Paginator',
		'Search.Prg',
		'RequestHandler'
	);

	public $helpers = array(
		'Html' => array('className' => 'BoostCake.BoostCakeHtml'),
		'Form' => array('className' => 'BoostCake.BoostCakeForm'),
		'Js',
		'Session',
		'Paginator' => array('className' => 'BoostCake.BoostCakePaginator'),
		'Text',
		'Custom'
	);

	public $theme = 'Jobalc';

	public function beforeFilter() {
		parent::beforeFilter();
		$this->_setupAuth();

		$this->set('loggedUser', $this->Auth->user());

		if (empty($this->request->params['admin'])) {
			$this->Auth->allow($this->request->params['action']);
		}

		$this->RequestHandler->setContent('json', 'text/x-json');

		if ($this->RequestHandler->isAjax()) {
			$this->layout = 'ajax';
		}

		if (isset($this->request->params['admin'])) {
			$this->theme = 'Admin';
		}
	}

	protected function _setupAuth() {

		$this->Auth->authenticate = array(
			'Form' => array(
				'fields' => array(
					'username' => 'username',
					'password' => 'password'
				),
				'userModel' => 'Users.User',
				'scope' => array(
					'User.active' => 1,
				)
			)
		);

		$this->Auth->authError = 'アクセスするにはログインしてください';
		$this->Auth->loginRedirect = '/admin';
		$this->Auth->logoutRedirect = array('plugin' => 'users', 'controller' => 'users', 'action' => 'login', 'return_to' => 'admin', 'admin' => true);
		$this->Auth->loginAction = array('plugin' => 'users', 'controller' => 'users', 'action' => 'login', 'return_to' => 'admin', 'admin' => true);
	}

	public function flashMsg($message, $class = 'alert-success', $id = 'flash') {
		$element = 'alert';
		$params = array(
			'plugin' => 'BoostCake',
			'class' => $class
		);
		CakeSession::write('Message.' . $id, compact('message', 'element', 'params'));
	}

}
