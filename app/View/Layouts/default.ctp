<!DOCTYPE html>
<html>
<head lang="ja">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $this->fetch('title'); ?></title>
<?php
	echo $this->Html->meta('icon');

	echo $this->fetch('meta');

	echo $this->Html->css(array(
		'bootstrap',
		'bootstrap-social',
		'font-awesome.min',
	));

	echo $this->fetch('css');
?>

<!--[if lt IE 9]><?php echo $this->Html->script('html5shiv'); ?><![endif]-->

<?php
	echo $this->Html->script(array(
		'jquery.min',
		'bootstrap.min',
	));
?>
<?php echo $this->fetch('script'); ?>

</head>
<body>
<div id="wrapper">
	<div id="header-wrapper">
		<div class="container">
			<div class="navbar-header">
				<h2 class="navbar-brand"><?php echo $this->fetch('title'); ?></h2>
			</div>
		</div>
	</div>

	<div id="page-wrapper">
		<div class="container">
			<?php echo $this->Session->flash(); ?>
			<?php echo $this->fetch('content'); ?>
		</div>
	</div>

</div>
<?php echo $this->element('sql_dump'); ?>
</body>
</html>
