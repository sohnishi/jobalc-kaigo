<?php

App::uses('AppModel', 'Model');

class Mailer extends AppModel {

	public $useTable = false;

	public $validate = array(
		'mail' => array(
			'rule' => 'email',
			'message' => '有効なメールアドレスを入力してください'
		)
	);
}
