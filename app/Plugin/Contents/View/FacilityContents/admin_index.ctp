<?php
?>
<div class="row">
	<div class="col-xs-12 contents-actions">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#">施設形態別コンテンツ一覧</a></li>
			<!-- 全ての施設形態を作り終わったら新規投稿リンクを表示しない -->
			<?php if ($this->Paginator->counter('{:count}') != count($shisetsukeitaiFirsts)) { ?>
				<li><a href="<?php echo $this->Html->url('/admin/contents/facilityContents/add'); ?>">施設形態別コンテンツを新規登録</a></li>
			<?php } ?>
		</ul>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">

		<div class="panel panel-default">
			<div class="panel-heading">公開画面用貼り付けコード</div>
			<div class="panel-body">
				<div class="well well-sm" style="margin:0;"><?php echo h("<?php echo $" . "facilityContent['FacilityContent']['content']; ?>"); ?></div>
			</div>
		</div>

	</div>
</div>


<div class="row">
	<div class="col-xs-12">

		<div class="panel panel-default">
			<div class="panel-heading">施設形態別コンテンツ一覧</div>
			<div class="panel-body">

				<table class="table table-striped">
					<thead>
						<tr>
							<th><?php echo $this->Paginator->sort('shisetsukeitai_first_id', '施設形態'); ?></th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($facilityContents as $facilityContent) : ?>
						<tr>
							<td><?php echo $shisetsukeitaiFirsts[$facilityContent[$model]['shisetsukeitai_first_id']]; ?></td>
							<td>
								<ul class="list-inline">
									<li><?php echo $this->Html->link('編集', array('action' => 'edit', $facilityContent[$model]['id'])); ?></li>
									<li><?php echo $this->Form->postLink('削除', array('action' => 'delete', $facilityContent[$model]['id']), '', '本当に削除しますか？元には戻せません'); ?></li>
								</ul>
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
				<div class="text-center">
					<?php echo $this->Paginator->pagination(array('ul' => 'pagination')); ?>
				</div>

			</div>
		</div>

	</div>
</div>