<div id="result-top-pc" class="hidden-xs">
	<div class="container">
		<div class="result-top-inner">
			<div class="count">
				<strong><?php echo $text; ?></strong>の公開している求人&nbsp;<strong><?php echo sprintf('%s件', $count); ?></strong>
			</div>
			<div class="text">
				サイト内で見つかる求人だけではありません。<br>
				人気の求人や情報を出せない求人もあり非公開となっています。<br>
				あなたにあった求人を合わせてご紹介できます。
			</div>
			<a href="<?php echo $this->Html->url('/customers/forms'); ?>" class="button"><?php echo $this->Html->image('result_top_btn.png', array('alt' => '求人を紹介してもらう', 'class' => 'img-responsive')); ?></a>
		</div>
		<div class="image">
			<?php echo $this->Html->image('result_image.png', array('alt' => '', 'class' => 'img-responsive')); ?>
		</div>
	</div>
</div>

<div id="result-top-sp" class="visible-xs">
	<div class="container">
		<div class="text">
			<strong><?php echo $text; ?></strong>の公開している求人&nbsp;<strong><?php echo sprintf('%s件', $count); ?></strong>
		</div>
		<div class="image">
			<a href="<?php echo $this->Html->url('/customers/forms'); ?>"><?php echo $this->Html->image('result_image_sp.png', array('alt' => '', 'class' => 'img-responsive')); ?></a>
		</div>
	</div>
</div>
