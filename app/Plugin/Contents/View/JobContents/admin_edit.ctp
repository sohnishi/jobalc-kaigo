<?php
	echo $this->Html->script('/theme/Admin/js/ckeditor/ckeditor.js', array('inline' => false));

	// 職種リストを取得
	$shokushus = Configure::read('shokushu');
	// 更新対象の職種以外を選択肢から削除する
	foreach ($shokushus as $key => $value) {
		if($key != $this->request->data['JobContent']['job_id']) {
			unset($shokushus[$key]);
		} 
	}

	// CSS等混みテンプレート
	$template2 = '<!DOCTYPE html><html lang="ja"><head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="format-detection" content="telephone=no"><meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no"><title>介護求人の一覧【ジョブアルク介護】</title><link href="/cw/favicon.ico" type="image/x-icon" rel="icon"><link href="/cw/favicon.ico" type="image/x-icon" rel="shortcut icon"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous"><link rel="stylesheet" type="text/css" href="/cw/theme/Jobalc/css/base.css" /><link rel="stylesheet" type="text/css" href="/cw/theme/Jobalc/css/modal.css" /><!--[if lt IE 9]><script type="text/javascript" src="/theme/Jobalc/js/html5shiv.min.js"></script><script type="text/javascript" src="/theme/Jobalc/js/respond.min.js"></script><![endif]--><script type="text/javascript" src="/cw/theme/Jobalc/js/jquery-1.11.2.min.js"></script><script type="text/javascript" src="/cw/theme/Jobalc/js/jquery.cookie.js"></script><script type="text/javascript" src="/cw/theme/Jobalc/js/bootstrap.min.js"></script></head><body>' . $this->request->data['JobContent']['content'] . '</body></html>';
?>
<div class="row">
	<div class="col-xs-12 contents-actions">
		<ul class="nav nav-tabs">
			<li><a href="<?php echo $this->Html->url('/admin/contents/jobContents'); ?>">職種別コンテンツ一覧</a></li>
			<li class="active"><a href="#">職種別コンテンツを更新</a></li>
		</ul>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">

		<div class="panel panel-default">
			<div class="panel-heading">職種別コンテンツを更新</div>
			<div class="panel-body">

				<?php
					echo $this->Form->create($model, array(
						'inputDefaults' => array(
							'label' => false,
							'div' => false,
							'class' => 'form-control'
						)
					));
					echo $this->Form->input('id');
				?>
				<table class="table table-bordered">
					<tbody>
						<tr>
							<th><?php echo $this->Form->label('job_id', '職種'); ?></th>							
							<td>
								<div class="col-xs-3">
									<?php echo $this->Form->input('job_id', array('type' => 'select', 'options' => $shokushus)); ?>
								</div>
							</td>
						</tr>
						<tr>
							<th><?php echo $this->Form->label('content', 'コンテンツ'); ?></th>
							<td>
							
								<div class="col-xs-12">
								<!-- <div class="panel panel-default">
									<div class="panel-heading">囲いコメント</div>
									<div class="panel-body">
										<div class="col-xs-12" style="padding-left:0;padding-right:0;">
											<br> -->
											<!-- <input class="form-control" id="kakoi_comment" type="text" value=" --><!--JOB_CONTENT_START--><!--この中にHTMLを記述してください --><!--JOB_CONTENT_END--><!--"> -->
										<!-- </div>
										<div class="clearfix"></div>
										<br>
										<div class="alert alert-info">
											<p>追加するHTMLを上記囲いコメントで囲ってください。</p>
										</div>
									</div> -->
								<!-- </div> -->
									<?php 
										echo $this->Form->input('content',array(  
											'label'=> false,  
											'size' => false,  
											'div'=>false,  
											'class'=>'form-textarea required',  
											'id'=>'ckeditor',
											'value'=>$template2,
											)  
										);  
									?>
								</div>
							</td>
						</tr>
						<script type="text/javascript">  
							// JOBALC-TODO: 職種ごとに読み込むURLを変更する
							// var defaultValue = loadData('/cw/area/pref27/');
							// console.log(defaultValue);
							//Ajax同期通信で外部テンプレートを読み込んでデータを返します。
							function loadData(url) {
									//XMLHttpRequestオブジェクト初期化
									var getData = new XMLHttpRequest();
									//同期通信リクエスト作成
									getData.open("GET", url, false);
									//リクエスト送信
									getData.send(null);
									//レスポンスデータを取得して値を返す
									return(getData.responseText);
							}
							var editor = CKEDITOR.replace('ckeditor');  
							// CKEDITOR.config.height = 500;
							CKEDITOR.config.height = 400;
							CKEDITOR.config.fullPage = true;
							CKEDITOR.config.allowedContent = true;
							// テンプレート挿入時に編集中データを置き換えないようにする
							CKEDITOR.config.templates_replaceContent = false;
							// Enterを押した際に改行タグを挿入
							CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;
							// Shift+Enterを押した際に段落タグを挿入
							CKEDITOR.config.shiftEnterMode = CKEDITOR.ENTER_P;
							CKEDITOR.config.contentsCss = ['https://use.fontawesome.com/releases/v5.8.2/css/all.css', '/cw/theme/Jobalc/css/base.css'];
							// CKEDITOR.instances['ckeditor'].setData(defaultValue);

						</script> 
					</tbody>
				</table>

				<div class="form-group" style="margin-top:20px;">
					<ul class="list-inline">
						<li><?php echo $this->Form->submit('職種別コンテンツを更新する', array('class' => 'btn btn-primary')); ?></li>
					</ul>
				</div>
				<?php echo $this->Form->end(); ?>

			</div>
		</div>

	</div>
</div>