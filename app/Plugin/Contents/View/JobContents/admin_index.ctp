<?php
// 職種リストを取得
$shokushus = Configure::read('shokushu');
?>
<div class="row">
	<div class="col-xs-12 contents-actions">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#">職種別コンテンツ一覧</a></li>
			<!-- 全ての職種を作り終わったら新規投稿リンクを表示しない -->
			<?php if ($this->Paginator->counter('{:count}') != count($shokushus)) { ?>
				<li><a href="<?php echo $this->Html->url('/admin/contents/jobContents/add'); ?>">職種別コンテンツを新規登録</a></li>
			<?php } ?>
		</ul>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">

		<div class="panel panel-default">
			<div class="panel-heading">公開画面用貼り付けコード</div>
			<div class="panel-body">
				<div class="well well-sm" style="margin:0;"><?php echo h("<?php echo $" . "jobContent['JobContent']['content']; ?>"); ?></div>
			</div>
		</div>

	</div>
</div>


<div class="row">
	<div class="col-xs-12">

		<div class="panel panel-default">
			<div class="panel-heading">職種別コンテンツ一覧</div>
			<div class="panel-body">

				<table class="table table-striped">
					<thead>
						<tr>
							<th><?php echo $this->Paginator->sort('job_id', '職種'); ?></th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($jobContents as $jobContent) : ?>
						<tr>
							<td><?php echo $shokushus[$jobContent[$model]['job_id']]; ?></td>
							<td>
								<ul class="list-inline">
									<li><?php echo $this->Html->link('編集', array('action' => 'edit', $jobContent[$model]['id'])); ?></li>
									<li><?php echo $this->Form->postLink('削除', array('action' => 'delete', $jobContent[$model]['id']), '', '本当に削除しますか？元には戻せません'); ?></li>
								</ul>
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
				<div class="text-center">
					<?php echo $this->Paginator->pagination(array('ul' => 'pagination')); ?>
				</div>

			</div>
		</div>

	</div>
</div>