<?php

App::uses('ContentsAppController', 'Contents.Controller');

class JobContentsController extends ContentsAppController {

	public $name = 'JobContents';

	public $uses = array('JobContent');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('model', $this->modelClass);
		$this->set('title_for_layout', '職種別コンテンツマスタ');
		$this->set('currentUser', $this->Auth->user());

	}

	public function admin_index() {
		$this->Paginator->settings[$this->modelClass] = array(
			'order' => array($this->modelClass . '.job_id' => 'ASC')
		);
		$this->set('jobContents', $this->Paginator->paginate());
	}

	public function admin_add() {
		if (!empty($this->request->data)) {
			// body内のみ取り出す
			$content = substr($this->request->data['JobContent']['content'], strpos($this->request->data['JobContent']['content'],'<body>') + mb_strlen('<body>'));
			$content = substr($content, 0, strpos($content,'</body>'));
			$this->request->data['JobContent']['content'] = $content;
			$this->{$this->modelClass}->create();
			if ($this->{$this->modelClass}->save($this->request->data)) {
				$this->flashMsg('職種別コンテンツを登録しました');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->flashMsg('職種別コンテンツを登録中にエラーが発生しました', 'alert-warning');
			}
		} else {
			$jobContents = $this->{$this->modelClass}->find('all', 
				array(
					'fields' => 'job_id'	
				)
			);
			// 職種リストを取得
			$shokushus = Configure::read('shokushu');
			// 全ての職種を作り終わっている場合は新規登録画面へ遷移させない
			if (count($jobContents) == count($shokushus)) {
				$this->redirect(array('action' => 'index'));
			}
			$jobContents = Hash::combine($jobContents, '{n}.JobContent.job_id', '{n}');
			$this->set('jobContents', $jobContents);
		}
	}

	public function admin_edit($id = null) {
		if (!$id || !$this->{$this->modelClass}->exists($id)) {
			$this->flashMsg('無効な操作です', 'alert-danger');
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->request->data)) {
			// body内のみ取り出す
			$content = substr($this->request->data['JobContent']['content'], strpos($this->request->data['JobContent']['content'],'<body>') + mb_strlen('<body>'));
			$content = substr($content, 0, strpos($content,'</body>'));
			$this->request->data['JobContent']['content'] = $content;
			if ($this->{$this->modelClass}->save($this->request->data)) {
				$this->flashMsg('職種別コンテンツを更新しました');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->flashMsg('職種別コンテンツを更新中にエラーが発生しました', 'alert-warning');
			}
		} else {
			$this->request->data = $this->{$this->modelClass}->read(null, $id);
		}
	}

	public function admin_delete($id = null) {
		if (!$id || !$this->{$this->modelClass}->exists($id)) {
			$this->flashMsg('無効な操作です', 'alert-warning');
			$this->redirect(array('action' => 'index'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->{$this->modelClass}->delete($id)) {
				$this->flashMsg('職種別コンテンツを削除しました');
			} else {
				$this->flashMsg('職種別コンテンツを削除中にエラーが発生しました', 'alert-warning');
			}
		}
		$this->redirect(array('action' => 'index'));
	}
}
