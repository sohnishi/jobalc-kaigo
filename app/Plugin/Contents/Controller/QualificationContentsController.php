<?php

App::uses('ContentsAppController', 'Contents.Controller');

class QualificationContentsController extends ContentsAppController {

	public $name = 'QualificationContents';

	public $uses = array('QualificationContent');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('model', $this->modelClass);
		$this->set('title_for_layout', '資格別コンテンツマスタ');
		$this->set('currentUser', $this->Auth->user());

	}

	public function admin_index() {
		$this->Paginator->settings[$this->modelClass] = array(
			'order' => array($this->modelClass . '.qualification_id' => 'ASC')
		);
		$this->set('qualificationContents', $this->Paginator->paginate());
	}

	public function admin_add() {
		if (!empty($this->request->data)) {
			// body内のみ取り出す
			$content = substr($this->request->data['QualificationContent']['content'], strpos($this->request->data['QualificationContent']['content'],'<body>') + mb_strlen('<body>'));
			$content = substr($content, 0, strpos($content,'</body>'));
			$this->request->data['QualificationContent']['content'] = $content;
			$this->{$this->modelClass}->create();
			if ($this->{$this->modelClass}->save($this->request->data)) {
				$this->flashMsg('資格別コンテンツを登録しました');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->flashMsg('資格別コンテンツを登録中にエラーが発生しました', 'alert-warning');
			}
		} else {
			$qualificationContents = $this->{$this->modelClass}->find('all', 
				array(
					'fields' => 'qualification_id'	
				)
			);
			// 資格リストを取得
			$shikakus = Configure::read('shikaku');
			// 全ての資格を作り終わっている場合は新規登録画面へ遷移させない
			if (count($qualificationContents) == count($shikakus)) {
				$this->redirect(array('action' => 'index'));
			}
			$qualificationContents = Hash::combine($qualificationContents, '{n}.QualificationContent.qualification_id', '{n}');
			$this->set('qualificationContents', $qualificationContents);
		}
	}

	public function admin_edit($id = null) {
		if (!$id || !$this->{$this->modelClass}->exists($id)) {
			$this->flashMsg('無効な操作です', 'alert-danger');
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->request->data)) {
			// body内のみ取り出す
			$content = substr($this->request->data['QualificationContent']['content'], strpos($this->request->data['QualificationContent']['content'],'<body>') + mb_strlen('<body>'));
			$content = substr($content, 0, strpos($content,'</body>'));
			$this->request->data['QualificationContent']['content'] = $content;
			if ($this->{$this->modelClass}->save($this->request->data)) {
				$this->flashMsg('資格別コンテンツを更新しました');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->flashMsg('資格別コンテンツを更新中にエラーが発生しました', 'alert-warning');
			}
		} else {
			$this->request->data = $this->{$this->modelClass}->read(null, $id);
		}
	}

	public function admin_delete($id = null) {
		if (!$id || !$this->{$this->modelClass}->exists($id)) {
			$this->flashMsg('無効な操作です', 'alert-warning');
			$this->redirect(array('action' => 'index'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->{$this->modelClass}->delete($id)) {
				$this->flashMsg('資格別コンテンツを削除しました');
			} else {
				$this->flashMsg('資格別コンテンツを削除中にエラーが発生しました', 'alert-warning');
			}
		}
		$this->redirect(array('action' => 'index'));
	}
}
