<?php

App::uses('ContentsAppController', 'Contents.Controller');

class EmploymentContentsController extends ContentsAppController {

	public $name = 'EmploymentContents';

	public $uses = array('EmploymentContent');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('model', $this->modelClass);
		$this->set('title_for_layout', '勤務形態別コンテンツマスタ');
		$this->set('currentUser', $this->Auth->user());

	}

	public function admin_index() {
		$this->Paginator->settings[$this->modelClass] = array(
			'order' => array($this->modelClass . '.employment_id' => 'ASC')
		);
		$this->set('employmentContents', $this->Paginator->paginate());
	}

	public function admin_add() {
		if (!empty($this->request->data)) {
			// body内のみ取り出す
			$content = substr($this->request->data['EmploymentContent']['content'], strpos($this->request->data['EmploymentContent']['content'],'<body>') + mb_strlen('<body>'));
			$content = substr($content, 0, strpos($content,'</body>'));
			$this->request->data['EmploymentContent']['content'] = $content;
			$this->{$this->modelClass}->create();
			if ($this->{$this->modelClass}->save($this->request->data)) {
				$this->flashMsg('勤務形態別コンテンツを登録しました');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->flashMsg('勤務形態別コンテンツを登録中にエラーが発生しました', 'alert-warning');
			}
		} else {
			$employmentContents = $this->{$this->modelClass}->find('all', 
				array(
					'fields' => 'employment_id'	
				)
			);
			// 勤務形態リストを取得
			$kinmu_keitais = Configure::read('kinmu_keitai');
			// 全ての勤務形態を作り終わっている場合は新規登録画面へ遷移させない
			if (count($employmentContents) == count($kinmu_keitais)) {
				$this->redirect(array('action' => 'index'));
			}
			$employmentContents = Hash::combine($employmentContents, '{n}.EmploymentContent.employment_id', '{n}');
			$this->set('employmentContents', $employmentContents);
		}
	}

	public function admin_edit($id = null) {
		if (!$id || !$this->{$this->modelClass}->exists($id)) {
			$this->flashMsg('無効な操作です', 'alert-danger');
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->request->data)) {
			// body内のみ取り出す
			$content = substr($this->request->data['EmploymentContent']['content'], strpos($this->request->data['EmploymentContent']['content'],'<body>') + mb_strlen('<body>'));
			$content = substr($content, 0, strpos($content,'</body>'));
			$this->request->data['EmploymentContent']['content'] = $content;
			if ($this->{$this->modelClass}->save($this->request->data)) {
				$this->flashMsg('勤務形態別コンテンツを更新しました');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->flashMsg('勤務形態別コンテンツを更新中にエラーが発生しました', 'alert-warning');
			}
		} else {
			$this->request->data = $this->{$this->modelClass}->read(null, $id);
		}
	}

	public function admin_delete($id = null) {
		if (!$id || !$this->{$this->modelClass}->exists($id)) {
			$this->flashMsg('無効な操作です', 'alert-warning');
			$this->redirect(array('action' => 'index'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->{$this->modelClass}->delete($id)) {
				$this->flashMsg('勤務形態別コンテンツを削除しました');
			} else {
				$this->flashMsg('勤務形態別コンテンツを削除中にエラーが発生しました', 'alert-warning');
			}
		}
		$this->redirect(array('action' => 'index'));
	}
}
