<?php

App::uses('ContentsAppController', 'Contents.Controller');

class PrefectureContentsController extends ContentsAppController {

	public $name = 'PrefectureContents';

	public $uses = array('PrefectureContent');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('model', $this->modelClass);
		$this->set('title_for_layout', '都道府県別コンテンツマスタ');
		$this->set('currentUser', $this->Auth->user());

	}

	public function admin_index() {
		$this->Paginator->settings[$this->modelClass] = array(
			'order' => array($this->modelClass . '.prefecture_code' => 'ASC')
		);
		$this->set('prefectureContents', $this->Paginator->paginate());
	}

	public function admin_add() {
		if (!empty($this->request->data)) {
			// body内のみ取り出す
			$content = substr($this->request->data['PrefectureContent']['content'], strpos($this->request->data['PrefectureContent']['content'],'<body>') + mb_strlen('<body>'));
			$content = substr($content, 0, strpos($content,'</body>'));
			$this->request->data['PrefectureContent']['content'] = $content;
			$this->{$this->modelClass}->create();
			if ($this->{$this->modelClass}->save($this->request->data)) {
				$this->flashMsg('都道府県別コンテンツを登録しました');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->flashMsg('都道府県別コンテンツを登録中にエラーが発生しました', 'alert-warning');
			}
		} else {
			$prefectureContents = $this->{$this->modelClass}->find('all', 
				array(
					'fields' => 'prefecture_code'	
				)
			);
			// 全ての都道府県を作り終わっている場合は新規登録画面へ遷移させない
			if (count($prefectureContents) == 47) {
				$this->redirect(array('action' => 'index'));
			}
			$prefectureContents = Hash::combine($prefectureContents, '{n}.PrefectureContent.prefecture_code', '{n}');
			$this->set('prefectureContents', $prefectureContents);
		}
		// $this->set('kinmus', $this->kinmus);
	}

	public function admin_edit($id = null) {
		if (!$id || !$this->{$this->modelClass}->exists($id)) {
			$this->flashMsg('無効な操作です', 'alert-danger');
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->request->data)) {
			// body内のみ取り出す
			$content = substr($this->request->data['PrefectureContent']['content'], strpos($this->request->data['PrefectureContent']['content'],'<body>') + mb_strlen('<body>'));
			$content = substr($content, 0, strpos($content,'</body>'));
			$this->request->data['PrefectureContent']['content'] = $content;

			if ($this->{$this->modelClass}->save($this->request->data)) {
				$this->flashMsg('都道府県別コンテンツを更新しました');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->flashMsg('都道府県別コンテンツを更新中にエラーが発生しました', 'alert-warning');
			}
		} else {
			$this->request->data = $this->{$this->modelClass}->read(null, $id);
		}
		// $this->set('kinmus', $this->kinmus);
	}

	public function admin_delete($id = null) {
		if (!$id || !$this->{$this->modelClass}->exists($id)) {
			$this->flashMsg('無効な操作です', 'alert-warning');
			$this->redirect(array('action' => 'index'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->{$this->modelClass}->delete($id)) {
				$this->flashMsg('都道府県別コンテンツを削除しました');
			} else {
				$this->flashMsg('都道府県別コンテンツを削除中にエラーが発生しました', 'alert-warning');
			}
		}
		$this->redirect(array('action' => 'index'));
	}
}
