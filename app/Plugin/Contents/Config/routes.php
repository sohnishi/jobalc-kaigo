<?php

/**
 * Contents Plugin Routes
 */
// Router::connect('/admin/contents/add', array('plugin' => 'contents', 'controller' => 'prefecture_contents', 'action' => 'add', 'admin' => true));
// Router::connect('/admin/contents/edit/*', array('plugin' => 'contents', 'controller' => 'prefecture_contents', 'action' => 'edit', 'admin' => true));
// Router::connect('/admin/contents/view/*', array('plugin' => 'contents', 'controller' => 'prefecture_contents', 'action' => 'view', 'admin' => true));
// Router::connect('/admin/contents/import', array('plugin' => 'contents', 'controller' => 'prefecture_contents', 'action' => 'import', 'admin' => true));
Router::connect('/admin/contents/:controller/:action/*', array('plugin' => 'contents', 'admin' => true));

