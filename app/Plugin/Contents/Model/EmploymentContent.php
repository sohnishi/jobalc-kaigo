<?php

App::uses('ContentsAppModel', 'Contents.Model');

class EmploymentContent extends ContentsAppModel {

	public $name = 'EmploymentContent';

	public $validate = array(
		'employment_id' => array(
			'required' => array(
				'rule' => array('notEmpty', 'employment_id'),
				'required' => true,
				'allowEmpty' => false,
				'message' => '勤務形態を選択してください'
			)
		)
	);

	// public $hasAndBelongsToMany = array(
	// 	'Recruit' => array(
	// 		'className' => 'Recruits.Recruit',
	// 		'joinTable' => 'recruits_kodawaris',
	// 		'foreignKey' => 'kodawari_id',
	// 		'associationForeignKey' => 'recruit_id',
	// 		'unique' => true,
	// 		'conditions' => '',
	// 		'fields' => '',
	// 		'order' => '',
	// 		'limit' => '',
	// 		'offset' => '',
	// 		'finderQuery' => '',
	// 		'deleteQuery' => '',
	// 		'insertQuery' => ''
	// 	)
	// );
}
