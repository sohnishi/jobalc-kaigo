<?php

App::uses('ContentsAppModel', 'Contents.Model');

class PrefectureContent extends ContentsAppModel {

	public $name = 'PrefectureContent';

	public $validate = array(
		'prefecture_code' => array(
			'required' => array(
				'rule' => array('notEmpty', 'prefecture_code'),
				'required' => true,
				'allowEmpty' => false,
				'message' => '都道府県を選択してください'
			)
		)
	);

	// public $hasAndBelongsToMany = array(
	// 	'Recruit' => array(
	// 		'className' => 'Recruits.Recruit',
	// 		'joinTable' => 'recruits_kodawaris',
	// 		'foreignKey' => 'kodawari_id',
	// 		'associationForeignKey' => 'recruit_id',
	// 		'unique' => true,
	// 		'conditions' => '',
	// 		'fields' => '',
	// 		'order' => '',
	// 		'limit' => '',
	// 		'offset' => '',
	// 		'finderQuery' => '',
	// 		'deleteQuery' => '',
	// 		'insertQuery' => ''
	// 	)
	// );
}
