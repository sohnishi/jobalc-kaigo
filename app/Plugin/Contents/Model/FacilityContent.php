<?php

App::uses('ContentsAppModel', 'Contents.Model');

class FacilityContent extends ContentsAppModel {

	public $name = 'FacilityContent';

	public $validate = array(
		'shisetsukeitai_first_id' => array(
			'required' => array(
				'rule' => array('notEmpty', 'shisetsukeitai_first_id'),
				'required' => true,
				'allowEmpty' => false,
				'message' => '施設形態を選択してください'
			)
		)
	);

	// public $hasAndBelongsToMany = array(
	// 	'Recruit' => array(
	// 		'className' => 'Recruits.Recruit',
	// 		'joinTable' => 'recruits_kodawaris',
	// 		'foreignKey' => 'kodawari_id',
	// 		'associationForeignKey' => 'recruit_id',
	// 		'unique' => true,
	// 		'conditions' => '',
	// 		'fields' => '',
	// 		'order' => '',
	// 		'limit' => '',
	// 		'offset' => '',
	// 		'finderQuery' => '',
	// 		'deleteQuery' => '',
	// 		'insertQuery' => ''
	// 	)
	// );
}
