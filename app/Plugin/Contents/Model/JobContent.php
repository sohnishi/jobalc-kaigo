<?php

App::uses('ContentsAppModel', 'Contents.Model');

class JobContent extends ContentsAppModel {

	public $name = 'JobContent';

	public $validate = array(
		'job_id' => array(
			'required' => array(
				'rule' => array('notEmpty', 'job_id'),
				'required' => true,
				'allowEmpty' => false,
				'message' => '職種を選択してください'
			)
		)
	);

	// public $hasAndBelongsToMany = array(
	// 	'Recruit' => array(
	// 		'className' => 'Recruits.Recruit',
	// 		'joinTable' => 'recruits_kodawaris',
	// 		'foreignKey' => 'kodawari_id',
	// 		'associationForeignKey' => 'recruit_id',
	// 		'unique' => true,
	// 		'conditions' => '',
	// 		'fields' => '',
	// 		'order' => '',
	// 		'limit' => '',
	// 		'offset' => '',
	// 		'finderQuery' => '',
	// 		'deleteQuery' => '',
	// 		'insertQuery' => ''
	// 	)
	// );
}
