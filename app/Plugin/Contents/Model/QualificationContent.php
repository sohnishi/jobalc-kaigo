<?php

App::uses('ContentsAppModel', 'Contents.Model');

class QualificationContent extends ContentsAppModel {

	public $name = 'QualificationContent';

	public $validate = array(
		'qualification_id' => array(
			'required' => array(
				'rule' => array('notEmpty', 'qualification_id'),
				'required' => true,
				'allowEmpty' => false,
				'message' => '資格を選択してください'
			)
		)
	);

	// public $hasAndBelongsToMany = array(
	// 	'Recruit' => array(
	// 		'className' => 'Recruits.Recruit',
	// 		'joinTable' => 'recruits_kodawaris',
	// 		'foreignKey' => 'kodawari_id',
	// 		'associationForeignKey' => 'recruit_id',
	// 		'unique' => true,
	// 		'conditions' => '',
	// 		'fields' => '',
	// 		'order' => '',
	// 		'limit' => '',
	// 		'offset' => '',
	// 		'finderQuery' => '',
	// 		'deleteQuery' => '',
	// 		'insertQuery' => ''
	// 	)
	// );
}
