<?php

App::uses('StaffsAppController', 'Staffs.Controller');

class StaffsController extends StaffsAppController {

	public $name = 'Staffs';

	public $uses = array('Staffs.Staff');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('model', $this->modelClass);
		$this->set('title_for_layout', '担当者管理');
		$this->Security->unlockedActions = array('admin_update', 'admin_read');
		$this->set('currentUser', $this->Auth->user());

	}

	public function admin_index() {
		$this->Paginator->settings['limit'] = 50;
		$this->Paginator->settings['order'] = array(
			$this->modelClass . '.facility_id' => 'ASC',
			$this->modelClass . '.primary_check' => 'DESC',
			$this->modelClass . '.busyo' => 'ASC',
			$this->modelClass . '.created' => 'DESC'
		);
		$this->Paginator->settings['contain'] = array(
			'Facility'
		);
		$this->set('staffs', $this->Paginator->paginate());
	}

	public function admin_add() {
		if (!empty($this->request->data)) {
			$this->{$this->modelClass}->create();
			if ($this->{$this->modelClass}->save($this->request->data)) {
				$this->flashMsg('担当者を登録しました');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->flashMsg('担当者を登録中にエラーが発生しました', 'alert-warning');
			}
		}

		$facilities = $this->{$this->modelClass}->Facility->find('list', array('fields' => array('Facility.id',	'Facility.shisetsumei')));
		$this->set('facilities', $facilities);
	}

	public function admin_edit($id = null) {
		if (!$id || !$this->{$this->modelClass}->exists($id)) {
			$this->flashMsg('無効な操作です', 'alert-danger');
			$this->redirect(array('action' => 'index'));
		}

		if (!empty($this->request->data)) {
			if ($this->{$this->modelClass}->save($this->request->data)) {
				$this->flashMsg('担当者を更新しました');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->flashMsg('担当者を更新中にエラーが発生しました', 'alert-warning');
			}
		} else {
			$this->request->data = $this->{$this->modelClass}->read(null, $id);
		}
		$facilities = $this->{$this->modelClass}->Facility->find('list', array('fields' => array('Facility.id',	'Facility.shisetsumei')));
		$this->set('facilities', $facilities);
	}

	public function admin_delete($id = null) {
		if (!$id || !$this->{$this->modelClass}->exists($id)) {
			$this->flashMsg('無効な操作です', 'alert-danger');
			$this->redirect(array('action' => 'index'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->{$this->modelClass}->delete($id)) {
				$this->flashMsg('担当者を削除しました');
			} else {
				$this->flashMsg('担当者を削除中にエラーが発生しました', 'alert-warning');
			}
		}
		$this->redirect(array('action' => 'index'));
	}

	public function admin_read($id = null) {
		if (!$id) {
			$this->_jsonRender();
			exit;
		}

		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			Configure::read('debug', 0);

			$staff = $this->{$this->modelClass}->read(null, $id);
			$this->_jsonRender($staff);
		}
	}

	public function admin_update() {
		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			Configure::write('debug', 0);

			if (empty($this->request->data['id'])) {
				unset($this->request->data['id']);
				$this->{$this->modelClass}->create();
			}

			$succeed = $this->{$this->modelClass}->save($this->request->data);
			$message = $succeed ? '更新しました' : '更新に失敗しました';

			if (!$succeed && $this->{$this->modelClass}->validationErrors) {
				$validationError = array_shift($this->{$this->modelClass}->validationErrors);
				$message = $validationError[0];
			}

			$_data = compact('succeed', 'message');
			$this->_jsonRender($_data);
		}
	}

	public function admin_ajaxDelete($id = null) {
		if (!$id) {
			$this->_jsonRender();
			exit;
		}

		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			Configure::write('debug', 0);
			$succeed = $this->{$this->modelClass}->delete($id);
			$message = $succeed ? '更新しました' : '更新に失敗しました';
			$_data = compact('succeed', 'message');
			$this->_jsonRender($_data);
		}
	}

	public function admin_getLatest() {
		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			Configure::write('debug', 0);

			$facilityId = $this->request->params['pass'];
			$query = array(
				'conditions' => array(
					$this->modelClass . '.facility_id' => $facilityId
				),
				'order' => array(
					$this->modelClass . '.primary_check' => 'DESC',
					$this->modelClass . '.busyo' => 'ASC',
					$this->modelClass . '.created' => 'DESC'
				)
			);
			$_data = $this->{$this->modelClass}->find('all', $query);
			$this->_jsonRender($_data);
		}
	}

	private function _jsonRender($_data = array()) {
		header("Content-Type: application/json; charset=utf-8");
		echo json_encode($_data);
	}
}
