<div class="row">
	<div class="col-xs-12 contents-actions">
		<ul class="nav nav-tabs">
			<li class="active"><a href="javascript:void(0);">担当者一覧</a></li>
			<li><?php echo $this->Html->link('担当者を新規登録', array('action' => 'add')); ?></li>
		</ul>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">

		<div class="panel panel-default">
			<div class="panel-heading">担当者一覧</div>
			<div class="panel-body">

				<table class="table table-striped">
					<thead>
						<tr>
							<th><?php echo $this->Paginator->sort('id', 'ID'); ?></th>
							<th><?php echo $this->Paginator->sort('facility_id', '担当施設'); ?></th>
							<th><?php echo $this->Paginator->sort('busyo', '部署名'); ?></th>
							<th><?php echo $this->Paginator->sort('yakuwari', '役職名'); ?></th>
							<th><?php echo $this->Paginator->sort('name', '担当者名'); ?></th>
							<th><?php echo $this->Paginator->sort('primary_check', 'メイン'); ?></th>
							<th><?php echo $this->Paginator->sort('primary_tel', '直通TEL'); ?></th>
							<th><?php echo $this->Paginator->sort('keitai_tel', '携帯TEL'); ?></th>
							<th><?php echo $this->Paginator->sort('primary_fax', '直通FAX'); ?></th>
							<th><?php echo $this->Paginator->sort('email', 'メールアドレス'); ?></th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($staffs as $staff) : ?>
						<tr>
							<td><?php echo $staff[$model]['id']; ?></td>
							<td><?php echo $staff['Facility']['shisetsumei']; ?></td>
							<td><?php echo $staff[$model]['busyo']; ?></td>
							<td><?php echo $staff[$model]['yakuwari']; ?></td>
							<td><?php echo $staff[$model]['name']; ?></td>
							<td><?php echo ($staff[$model]['primary_check'] == 1) ? '<i class="fa fa-check"></i>' : ''; ?></td>
							<td><?php echo $staff[$model]['primary_tel']; ?></td>
							<td><?php echo $staff[$model]['keitai_tel']; ?></td>
							<td><?php echo $staff[$model]['primary_fax']; ?></td>
							<td><?php echo $staff[$model]['email']; ?></td>
							<td>
								<ul class="list-inline">
									<li><?php echo $this->Html->link('編集', array('action' => 'edit', $staff[$model]['id'])); ?></li>
									<li><?php echo $this->Form->postLink('削除', array('action' => 'delete', $staff[$model]['id']), '', '本当に削除しますか？元には戻せません'); ?></li>
								</ul>
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>

				<div class="text-center">
					<?php echo $this->Paginator->pagination(array('ul' => 'pagination')); ?>
				</div>

			</div>
		</div>

	</div>
</div>