<div class="row">
	<div class="col-xs-12 contents-actions">
		<ul class="nav nav-tabs">
			<li><?php echo $this->Html->link('担当者一覧', array('action' => 'index')); ?></li>
			<li class="active"><a href="javascript:void(0);">担当者を更新</a></li>
		</ul>
	</div>
</div>

<div class="row">
	<div class="col-xs-8 col-xs-offset-2">

		<div class="panel panel-default">
			<div class="panel-heading">担当者を更新</div>
			<?php
				echo $this->Form->create($model, array(
					'inputDefaults' => array(
						'label' => false,
						'wrapInput' => false,
						'div' => false,
						'class' => 'form-control'
					)
				));

				echo $this->Form->input('id');
			?>
			<div class="panel-body">

				<div class="row">
					<div class="col-xs-6">
						<div class="form-group">
							<?php echo $this->Form->label('facility_id', '担当施設'); ?>
							<?php echo $this->Form->input('facility_id'); ?>
						</div>

						<div class="form-group">
							<?php echo $this->Form->label('busyo', '部署名'); ?>
							<?php echo $this->Form->input('busyo'); ?>
						</div>

						<div class="form-group">
							<?php echo $this->Form->label('yakuwari', '役職名'); ?>
							<?php echo $this->Form->input('yakuwari'); ?>
						</div>

						<div class="form-group">
							<?php echo $this->Form->label('name', '担当者名 <span class="req">*</span>'); ?>
							<?php echo $this->Form->input('name'); ?>
						</div>

						<div class="form-group">
							<?php echo $this->Form->input('primary_check', array('type' => 'checkbox', 'label' => array('text' => 'メイン担当者', 'class' => false), 'class' => false)); ?>
							<span class="help-block">メイン担当者にするにはチェックしてください</span>
						</div>
					</div>

					<div class="col-xs-6">
						<div class="form-group">
							<?php echo $this->Form->label('primary_tel', '直通TEL'); ?>
							<?php echo $this->Form->input('primary_tel'); ?>
							<span class="help-block">0xx-xxx-xxxxの形式で入力してください</span>
						</div>

						<div class="form-group">
							<?php echo $this->Form->label('keitai_tel', '携帯TEL'); ?>
							<?php echo $this->Form->input('keitai_tel'); ?>
							<span class="help-block">0xx-xxxx-xxxxの形式で入力してください</span>
						</div>

						<div class="form-group">
							<?php echo $this->Form->label('primary_fax', '直通FAX'); ?>
							<?php echo $this->Form->input('primary_fax'); ?>
							<span class="help-block">0xx-xxx-xxxxの形式で入力してください</span>
						</div>

						<div class="form-group">
							<?php echo $this->Form->label('email', 'メールアドレス'); ?>
							<?php echo $this->Form->input('email'); ?>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<?php echo $this->Form->label('memo', '備考'); ?>
							<?php echo $this->Form->input('memo', array('rows' => 3)); ?>
						</div>
					</div>
				</div>

			</div>
			<div class="panel-footer">
				<?php echo $this->Form->submit('担当者を更新する', array('class' => 'btn btn-primary')); ?>
			</div>
			<?php echo $this->Form->end(); ?>
		</div>

	</div>
</div>