<?php

App::uses('StaffsAppModel', 'Staffs.Model');

class Staff extends StaffsAppModel {

	public $name = 'Staff';

	public $validate = array(
		'name' => array(
			'required' => array(
				'rule' => array('notEmpty', 'name'),
				'required' => true,
				'notEmpty' => false,
				'message' => '担当者名を入力してください'
			)
		),
		'primary_tel' => array(
			'rule' => array('custom', '/^(0\d{1,4}-\d{1,4}-\d{4})$/'),
			// ハイフン不問
			//'rule' => array('custom', '/^(0\d{1,4}[\s-]?\d{1,4}[\s-]?\d{4})$/'),
			'allowEmpty' => true,
			'message' => '直通TEL番号の入力形式が違います'
		),
		'ketai_tel' => array(
			'rule' => array('custom', '/^(0\d{1,4}-\d{1,4}-\d{4})$/'),
			// ハイフン不問
			//'rule' => array('custom', '/^(0\d{1,4}[\s-]?\d{1,4}[\s-]?\d{4})$/'),
			'allowEmpty' => true,
			'message' => '携帯番号の入力形式が違います'
		),
		'primary_fax' => array(
			'rule' => array('custom', '/^(0\d{1,4}-\d{1,4}-\d{4})$/'),
			// ハイフン不問
			//'rule' => array('custom', '/^(0\d{1,4}[\s-]?\d{1,4}[\s-]?\d{4})$/'),
			'allowEmpty' => true,
			'message' => '直通FAX番号の入力形式が違います'
		),
		'email' => array(
			'rule' => array('email', 'email'),
			'allowEmpty' => true,
			'message' => '有効なメールアドレスを入力してください'
		)
	);

	public $belongsTo = array(
		'Facility' => array(
			'className' => 'Facility',
			'foreignKey' => 'facility_id',
			'conditions' => '',
			'type' => '',
			'fields' => '',
			'order' => '',
			'counterCache' => '',
			'counterScope' => ''
		)
	);
}
