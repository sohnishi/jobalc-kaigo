<?php
    // ページタイトル
    $page_title = 'ジョブアルク看護師の個人情報保護方針';
    $this->assign('title', $page_title);
    // 使用レイアウト
    $this->layout = 'ns_header-footer';
?>

<div class="section">
    <div class="container">
        <h2 class="page-header">個人情報保護方針</h2>

        <div class="section-body text-center">
            <p>株式会社ALCは、高度情報化通信社会における個人情報保護の重要性を認識し、<br>保健・医療・介護・福祉に関する人材紹介業を実施する企業として、個人情報の保護に関する法令等を<br>遵守するととともに、以下に基づき、当社が取り扱う個人情報の保護に努めます。</p>
        </div>

        <div class="section-body text-center">
            <p>制定:2016年3月1日</p><p>
            </p><p>株式会社 ALC　代表取締役 山﨑博友</p>
        </div>

        <div class="section-body">
            <h3 class="page-title">1.個人情報取得の目的</h3>
            <p>以下の各号に掲げる目的で個人情報を取得し、その目的の範囲内でのみ利用します。</p>
            <ul class="list-unstyled list-indent">
                <li>（1）適切な職業紹介及び転職に関する情報提供</li>
                <li>（2）利用者の承諾を前提とした求人企業・施設などへの個人情報開示</li>
                <li>（3）当社が提供するサービスのご案内や資料の送付</li>
                <li>（4）メールマガジン、ニュース、アンケートの配信</li>
            </ul>
        </div>

        <div class="section-body">
            <h3 class="page-title">2.個人情報の利用及び提供</h3>
            <p>お預かりした個人情報は「個人情報取得の目的」の範囲内でお取り扱いをしております。</p>
            <p>ご本人の同意がない限り、個人情報を企業及び第三者に開示・提供することは一切ございません。</p>
        </div>

        <div class="section-body">
            <h3 class="page-title">3. 個人情報の適正かつ安全な管理</h3>
            <p>当社は、取得した個人情報の適正な管理を行い、個人情報の漏えい、滅失またはき損の防止および是正に取り組み、個人情報の安全性の確保を図ります。</p>
        </div>

        <div class="section-body">
            <h3 class="page-title">4. 個人情報に関する法令・規範等の遵守</h3>
            <p>個人情報に関する法令、規範およびガイドラインに対応したコンプライアンスに基づき、 体系化されたコンプライアンス・プログラム、個人情報保護に関する法令及びその他の規範を遵守し、適正な管理を行います。</p>
        </div>

        <div class="section-body">
            <h3 class="page-title">5.苦情及び相談への対応</h3>
            <p>当社は、個人情報の取扱いに関して、ご本人からの苦情および相談を受け付け、迅速かつ適切な対応を行なうために必要な体制を整備し、これを遵守します。</p>
        </div>

        <div class="section-body">
            <h3 class="page-title">6. お問い合わせ先</h3>
            <p>当社の個人情報保護に関するお問合わせは、下記にて受付けております。</p>
        </div>

        <div class="section-body">
            <p>【株式会社ALC （アルク）法務部 個人情報担当】</p>
            <p>〒540-0029 大阪府大阪市中央区本町橋5-14オージービル本町橋5階</p>
            <p>TEL:06-6360-9505</p>
            <p>E-mail:info@alc-medi.com</p>
        </div>

        <div class="section-body">
            <p>制定:2016年3月1日</p>
        </div>
    </div>
</div>
