<?php
    // ページタイトル
    $page_title = '会社概要【Jobアルク】';
    $this->assign('title', $page_title);
    // 使用レイアウト
    $this->layout = 'landing_default';
?>

<div class="section">
    <div class="container">
        <h2 class="page-header">会社概要</h2>
        <div class="section-body">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <th>企業名</th>
                        <td>株式会社　ALC（アルク）　</td>
                    </tr>
                    <tr>
                        <th>設立</th>
                        <td>2013年10月</td>
                    </tr>
                    <tr>
                        <th>資本金</th>
                        <td>500万円</td>
                    </tr>
                    <tr>
                        <th>代表取締役</th>
                        <td>山﨑　博友</td>
                    </tr>
                    <tr>
                        <th>事業内容</th>
                        <td>
                            <ul class="list-unstyled">
                                <li>・厚生労働大臣許可・有料職業紹介事業（許可番号　27-ュ-301641）</li>
                                <li>・人事コンサルティング全般</li>
                                <li>・各種セミナー、研修イベントの企画・立案及び運営</li>
                                <li>・医療・福祉機関 ホームページの制作</li>
                                <li>・就職・転職支援サイトの運営</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <th>住　所</th>
                        <td>
                            <ul class="list-unstyled">
                                <li>大阪本社：大阪市中央区本町橋5-14オージービル本町橋5F</li>
                                <li>福岡支社：福岡県福岡市中央区今泉2-4-25−1003</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <th>電話番号</th>
                        <td><span data-action="call" data-tel="0120932929">0120-932-929</span></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
