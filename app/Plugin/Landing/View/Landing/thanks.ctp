<?php
    // ページタイトル
    $page_title = '友人に教えたくなる求人・転職情報サイト【Jobアルク】';
    $this->assign('title', $page_title);
    // 使用レイアウト
    $this->layout = 'thanks';
?>

<div id="thanks">
    <div class="container">
        <div class="thanks-box">
            <h2 class="thanks-title">無料web登録</h2>
            <div class="thanks-inner">
                <p>この度は、ジョブアルク転職サポートにご登録いただきまして、誠にありがとうございます。</p>
                <p>今後の流れについての説明やご登録の内容についての確認を24時間以内に、お電話にてご連絡をさせていただきます。</p>
                <p>しばらくお待ちくださいますようお願いいたします。</p>
            </div>
        </div>
    </div>
</div>
