<?php
    // ページタイトル
    $page_title = '介護の非公開求人をご紹介｜ジョブアルク';
    $this->assign('title', $page_title);
    // 使用レイアウト
    $this->layout = 'cw_hikoukai-header-footer';
?>
<div id="masthead" class="section">
    <div class="container">
        <?php echo $this->Html->image('/landing/img/hikoukai-catch.png', array('alt' => '介護の求人に詳しいハローワーク、ジョブアルクにしかない非公開求人をご紹介します！')); ?>
    </div>
</div>
<section class="kyujin">
	<h2 class="kyujin">
		<span class="kyujin-title">非公開求人を一部ご紹介します</span>
	</h2>
	<ul>
        <li class="kyujin-file">
			<div class="pointBox">
				<p class="point"><i class="fa fa-check" aria-hidden="true"></i>福利厚生◎離職率3％</p>
				<p class="salary"><i class="fa fa-jpy" aria-hidden="true"></i>月給 260,000円~</p>
			</div>
			<h3>退職金・住宅手当などの福利厚生が充実☆生活面もしっかりサポートします！</h3>
			<div class="form-btn"><a id="" href="">求人の詳細はこちら<i class="fa fa-angle-right" aria-hidden="true"></i></a></div>
		</li>
        <li class="kyujin-file">
			<div class="pointBox">
				<p class="point"><i class="fa fa-check" aria-hidden="true"></i>駅チカ！アクセス抜群</p>
				<p class="salary"><i class="fa fa-jpy" aria-hidden="true"></i>月給 240,000円~</p>
			</div>
			<h3>2路線利用可能！どちらも徒歩5~6分圏内で通勤の利便性抜群です。</h3>
			<div class="form-btn"><a id="" href="">求人の詳細はこちら<i class="fa fa-angle-right" aria-hidden="true"></i></a></div>
		</li>
        <li class="kyujin-file">
			<div class="pointBox">
				<p class="point"><i class="fa fa-check" aria-hidden="true"></i>無資格・未経験OK◎</p>
				<p class="salary"><i class="fa fa-jpy" aria-hidden="true"></i>月給 230,000円~</p>
			</div>
			<h3>無資格･未経験･ブランクのある方も応募可能です♪ 先輩スタッフがサポートしますので安心してください☆</h3>
			<div class="form-btn"><a id="" href="">求人の詳細はこちら<i class="fa fa-angle-right" aria-hidden="true"></i></a></div>
		</li>
        <li class="kyujin-file">
			<div class="pointBox">
				<p class="point"><i class="fa fa-check" aria-hidden="true"></i>ママさん看護助手活躍中！</p>
				<p class="salary"><i class="fa fa-jpy" aria-hidden="true"></i>月給 240,000円~</p>
			</div>
			<h3>24時間託児所完備しており、ママさん看護助手が大勢活躍している施設です！</h3>
			<div class="form-btn"><a id="" href="">求人の詳細はこちら<i class="fa fa-angle-right" aria-hidden="true"></i></a></div>
		</li>
        <li class="kyujin-file">
			<div class="pointBox">
				<p class="point"><i class="fa fa-check" aria-hidden="true"></i>人間関係で悩まない職場です！</p>
				<p class="salary"><i class="fa fa-jpy" aria-hidden="true"></i>月給 245,000円~</p>
			</div>
			<h3>アットホームな雰囲気と職員の笑顔が自慢の施設で一緒に入所者さんのお世話を行います☆</h3>
			<div class="form-btn"><a id="" href="">求人の詳細はこちら<i class="fa fa-angle-right" aria-hidden="true"></i></a></div>
		</li>
        <li class="kyujin-file">
			<div class="pointBox">
				<p class="point"><i class="fa fa-check" aria-hidden="true"></i>家庭との両立が可能☆</p>
				<p class="salary"><i class="fa fa-jpy" aria-hidden="true"></i>月給 255,000円~</p>
			</div>
			<h3>残業もほとんどないので、仕事とプライベートの両立可能です☆スタッフの働きやすさを考えた職場です！</h3>
			<div class="form-btn"><a id="" href="">求人の詳細はこちら<i class="fa fa-angle-right" aria-hidden="true"></i></a></div>
		</li>
    </ul>

	<div class="popular">
		<h3><img class="pc" src="" alt="この求人の他にも豊富にございます。"><img class="sp" src="" alt="この求人の他にも豊富にございます。"></h3>
		<p>地域密着なのでジョブアルクにしかない求人も豊富です。人気の求人も豊富にございますが、すぐに募集が締め切られる可能性もあります<br>お早めにお問い合わせください。</p>
	</div>

	<div class="secret">
		<h3><img class="pc" src="" alt="非公開求人ってなに?"><img class="sp" src="" alt="非公開求人ってなに?"></h3>
		<p>ネット上で公開している求人情報はほんの一部です。募集を公開しにくい求人や水面下で募集したい法人さんもあるのでそのような求人は会員限定で公開しております。地域の情報に詳しい介護専門の求人アドバイザーが希望に沿った求人を紹介または代理で交渉したり探します。</p>
		<img class="graph sp" src="" alt="入職された方の70％が非公開求人です">
	</div>

</section>
<div id="form-wizard" class="section">
    <div class="container">
        <div class="form-box">
            <div class="form-inner">
                <?php
                    echo $this->Form->create('Customer', array(
                        'inputDefaults' => array('label' => false, 'div' => false),
                        'id' => 'landing-form',
                        'class' => 'form-horizontal',
                        'url' => '/landing/forms'
                    ));
                ?>
                <div id="landing-form-content">
                    <h3>保有資格</h3>
                    <section>
                        <h4 class="question">あなたの保有資格を選択してください<br><span>(※複数選択可)</span></h4>
                        <div class="answer">
                            <div id="shikaku_error"></div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="checkbox">
                                        <input type="checkbox" name="data[Customer][shikaku][]" value="4" id="CustomerShikaku4" data-validation-engine="validate[minCheckbox[1]]" data-prompt-target="shikaku_error">
                                        <label for="CustomerShikaku4">介護福祉士</label>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="checkbox">
                                        <input type="checkbox" name="data[Customer][shikaku][]" value="8" id="CustomerShikaku8" data-validation-engine="validate[minCheckbox[1]]" data-prompt-target="shikaku_error">
                                        <label for="CustomerShikaku8">ヘルパー</label>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="checkbox">
                                        <input type="checkbox" name="data[Customer][shikaku][]" value="6" id="CustomerShikaku6" data-validation-engine="validate[minCheckbox[1]]" data-prompt-target="shikaku_error">
                                        <label for="CustomerShikaku6">ケアマネ</label>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="checkbox">
                                        <input type="checkbox" name="data[Customer][shikaku][]" value="20" id="CustomerShikaku20" data-validation-engine="validate[minCheckbox[1]]" data-prompt-target="shikaku_error">
                                        <label for="CustomerShikaku20">その他</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <h3>雇用形態</h3>
                    <section>
                        <h4 class="question">ご希望の雇用形態を選択してください</h4>
                        <div class="answer mb30">
                            <div id="kibouwork_error"></div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="checkbox">
                                        <input type="checkbox" name="data[Customer][kibouwork][]" value="22" id="CustomerKibouwork22" data-validation-engine="validate[minCheckbox[1]]" data-prompt-target="kibouwork_error">
                                        <label for="CustomerKibouwork22">常勤(夜勤可)</label>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="checkbox">
                                        <input type="checkbox" name="data[Customer][kibouwork][]" value="23" id="CustomerKibouwork23" data-validation-engine="validate[minCheckbox[1]]" data-prompt-target="kibouwork_error">
                                        <label for="CustomerKibouwork23">日勤常勤</label>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="checkbox">
                                        <input type="checkbox" name="data[Customer][kibouwork][]" value="24" id="CustomerKibouwork24" data-validation-engine="validate[minCheckbox[1]]" data-prompt-target="kibouwork_error">
                                        <label for="CustomerKibouwork24">夜勤バイト</label>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="checkbox">
                                        <input type="checkbox" name="data[Customer][kibouwork][]" value="25" id="CustomerKibouwork25" data-validation-engine="validate[minCheckbox[1]]" data-prompt-target="kibouwork_error">
                                        <label for="CustomerKibouwork25">日勤バイト</label>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="checkbox">
                                        <input type="checkbox" name="data[Customer][kibouwork][]" value="26" id="CustomerKibouwork26" data-validation-engine="validate[minCheckbox[1]]" data-prompt-target="kibouwork_error">
                                        <label for="CustomerKibouwork26">その他</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h4 class="question">転職を希望する時期を選択してください</h4>
                        <div class="answer">
                            <div class="radio-style clearfix">
                                <div class="radio">
                                    <input type="radio" name="data[Customer][kinmukaishijiki]" value="6" checked="checked" id="CustomerKinmukaishijiki6">
                                    <label for="CustomerKinmukaishijiki6">いますぐ</label>
                                    <div class="check"></div>
                                </div>
                                <div class="radio">
                                    <input type="radio" name="data[Customer][kinmukaishijiki]" value="1" id="CustomerKinmukaishijiki1">
                                    <label for="CustomerKinmukaishijiki1">3ヶ月以内</label>
                                    <div class="check"></div>
                                </div>
                                <div class="radio">
                                    <input type="radio" name="data[Customer][kinmukaishijiki]" value="7" id="CustomerKinmukaishijiki7">
                                    <label for="CustomerKinmukaishijiki7">年度末</label>
                                    <div class="check"></div>
                                </div>
                                <div class="radio">
                                    <input type="radio" name="data[Customer][kinmukaishijiki]" value="8" id="CustomerKinmukaishijiki8">
                                    <label for="CustomerKinmukaishijiki8">その他</label>
                                    <div class="check"></div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <h3>住所</h3>
                    <section>
                        <h4 class="question">お住まい、生まれた年を入力してください</h4>
                        <div class="answer">
                            <div class="form-group zip">
                                <?php echo $this->Form->label('zip', '郵便番号', array('class' => 'control-label col-xs-3')); ?>
                                <div class="col-xs-9">
                                    <?php echo $this->Form->input('zip', array('type' => 'text', 'wrapInput' => false, 'class' => 'form-control', 'placeholder' => '例）000-0000')); ?>
                                    <div class="addon">※ハイフンなしOK</div>
                                </div>
                            </div>
                            <div id="birth_error"></div>
                            <div class="form-group birth">
                                <?php echo $this->Form->label('Customer.birth.year', '生まれ年', array('class' => 'control-label col-xs-3')); ?>
                                <div class="col-xs-9">
                                    <?php echo $this->Form->year('Customer.birth.year', '1958', date('Y') - 18, array('class' => 'form-control', 'empty' => '西暦', 'data-validation-engine' => 'validate[required]', 'data-prompt-target' => 'birth_error')); ?>
                                    <div class="addon">年生まれ</div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <h3>連絡先</h3>
                    <section>
                        <h4 class="question">お名前、ご連絡先を入力してください</h4>
                        <div class="answer">
                            <div class="form-group">
                                <?php echo $this->Form->label('name', 'お名前', array('class' => 'control-label col-xs-3')); ?>
                                <div class="col-xs-9">
                                    <?php echo $this->Form->input('name', array('type' => 'text', 'class' => 'form-control', 'placeholder' => '例）福岡花子', 'data-validation-engine' => 'validate[required]')); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo $this->Form->label('hurigana', 'フリガナ', array('class' => 'control-label col-xs-3')); ?>
                                <div class="col-xs-9">
                                    <?php echo $this->Form->input('hurigana', array('type' => 'text', 'class' => 'form-control', 'placeholder' => '例）フクオカハナコ', 'data-validation-engine' => 'validate[required,custom[onlyKatakana]]')); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo $this->Form->label('keitai_tel', '電話番号', array('class' => 'control-label col-xs-3')); ?>
                                <div class="col-xs-9">
                                    <?php echo $this->Form->input('keitai_tel', array('type' => 'text', 'class' => 'form-control', 'placeholder' => '例）000-0000-0000', 'data-validation-engine' => 'validate[required,minSize[9],maxSize[13],custom[phone]]')); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo $this->Form->label('email', 'メール', array('class' => 'control-label col-xs-3')); ?>
                                <div class="col-xs-9">
                                    <?php echo $this->Form->input('email', array('type' => 'email', 'class' => 'form-control', 'placeholder' => '例）jobalk@info.jp', 'data-validation-engine' => 'validate[required, custom[email]]')); ?>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="actions clearfix">
                    <div class="row">
                        <div class="col-xs-6">
                            <button type="button" id="stepsPrev" class="btn btn-block disabled" disabled="disabled"><span>もどる</span></button>
                        </div>
                        <div class="col-xs-6">
                            <button type="button" id="stepsNext" class="btn btn-block"><span>つぎへ</span></button>
                            <button type="button" id="stepsFinish" class="btn btn-block"><span>登録する</span></button>
                        </div>
                    </div>
                </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>

<div class="contact-banner section">
    <p class="tel-micro">※入力が苦手やめんどくさい方も電話でOK</p>
    <div class="container">
        <?php echo $this->Html->link($this->Html->image('/landing/img/hikoukai-tel.png'), 'tel:0120016898', array('escape' => false)); ?>
    </div>
    <p class="tel-micro">※無理に転職をお勧めすることはありません。</p>
</div>