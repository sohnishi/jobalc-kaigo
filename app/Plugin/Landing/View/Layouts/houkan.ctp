<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="format-detection" content="telephone=no">
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
<title><?php echo $this->fetch('title'); ?></title>
<link href="<?php echo $this->Html->url('/landing/favicon.ico'); ?>" type="image/x-icon" rel="icon">
<link href="<?php echo $this->Html->url('/landing/favicon.ico'); ?>" type="image/x-icon" rel="shortcut icon">

<?php echo $this->fetch('meta'); ?>
<?php
    echo $this->Html->css('Landing.bootstrap.min.css');
    echo $this->Html->css('Landing.validationEngine.jquery.css');
    echo $this->Html->css('Landing.landing.css');
    echo $this->fetch('css');
?>

<?php
    echo $this->Html->script('Landing.jquery-1.11.2.min.js');
    echo $this->Html->script('Landing.bootstrap.min.js');
    echo $this->Html->script('Landing.jquery.steps.min.js');
    echo $this->Html->script('Landing.jquery.validationEngine-ja.js');
    echo $this->Html->script('Landing.jquery.validationEngine.js');
    echo $this->Html->script('Landing.jquery.autoKana.js');
    echo $this->Html->script('Landing.landing.js');
    echo $this->fetch('script');
?>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TMH9GK');</script>
<!-- End Google Tag Manager -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TMH9GK"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="body-wrapper">
    <header id="header" class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <p class="navbar-brand"><img src="/landing/img/jobalc_houkan_logo.png" alt="jobアルク" title="jobアルク"></p>
            <div class="information hidden-xs">
                <div class="phone">
                    <div>お電話でのお問い合わせ</div>
                    <div class="tel">0120-932-929</div>
                    <div>【通話料無料】受付9:20～20:00</div>
                </div>
            </div>
        </div>
    </header>

    <div id="main">
        <div id="landing-content">
            <?php echo $this->fetch('content'); ?>
        </div>
    </div>

    <footer id="footer-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <ul class="footer-nav list-inline">
                        <li><a href="<?php echo $this->Html->url('/landing/corporation'); ?>">会社概要</a></li>
                        <li><a href="<?php echo $this->Html->url('/landing/terms'); ?>">利用規約</a></li>
                        <li><a href="<?php echo $this->Html->url('/landing/privacy'); ?>">個人情報保護方針</a></li>
                    </ul>
                </div>
                <div class="col-sm-6">
                    <div class="information">
                        <div class="phone">
                            <div>お電話でのお問い合わせ</div>
                            <div class="tel"><a href="tel:0120932929">0120-932-929</a></div>
                            <div>【通話料無料】受付9:20～20:00</div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="footer-bottom" class="text-center">
                <div>厚生大臣許可 27-ュ-201641</div>
                <address> Copyright &copy; <?php echo date('Y'); ?> ALC Co.Ltd All Rights Reserved.</address>
            </div>
        </div>
    </footer>
</div>

</body>
</html>
