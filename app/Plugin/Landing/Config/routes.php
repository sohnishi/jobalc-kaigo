<?php
// 注意：app/webroot/が優先されるので特定のURLを指定したい場合はapp/webroot/内を整理する必要がある
// 例えばapp/webroot/lp/cm/は既に使用されているので、このroutes.phpで指定しても無効となる

// デフォルト
Router::connect('/landing/forms', array('plugin' => 'landing', 'controller' => 'landing', 'action' => 'forms', 'admin' => false));
Router::connect('/landing/thanks', array('plugin' => 'landing', 'controller' => 'landing', 'action' => 'thanks', 'admin' => false));
Router::connect('/landing/*', array('plugin' => 'landing', 'controller' => 'landing', 'action' => 'display', 'admin' => false));

Router::connect('/landing/corporation', array('plugin' => 'landing', 'controller' => 'landing', 'action' => 'display', 'corporation', 'admin' => false));
Router::connect('/landing/terms', array('plugin' => 'landing', 'controller' => 'landing', 'action' => 'display', 'terms', 'admin' => false));
Router::connect('/landing/privacy', array('plugin' => 'landing', 'controller' => 'landing', 'action' => 'display', 'privacy', 'admin' => false));

// 特定のルーティングを指定したい場合は以下に記入する
