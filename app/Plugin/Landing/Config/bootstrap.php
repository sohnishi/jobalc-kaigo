<?php
// ページタイトル初期値。viewのassignで特定のページタイトルに変更する
// @see app/Plugin/Landing/View/Landing/default.ctp
Configure::write('landing_default_title', '友人に教えたくなる看護師の求人・転職情報サイト【Jobアルク】');


// 送信先メールアドレス
Configure::write('reception27', 'osaka@alc-medi.com');
Configure::write('reception34', 'hiroshima@alc-medi.com');
Configure::write('reception40', 'fukuoka@alc-medi.com');

