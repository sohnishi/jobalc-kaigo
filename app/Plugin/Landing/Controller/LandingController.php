<?php

App::uses('LandingAppController', 'Landing.Controller');

/**
 * ランディングページコントローラ
 *
 * Class LandingController
 */
class LandingController extends LandingAppController
{
    public $name = 'Landing';

    // カスタマーモデルを使用
    public $uses = array('Customers.Customer');

    public function beforeFilter()
    {
        parent::beforeFilter();

        $this->set('title_for_layout', Configure::read('landing_default_title'));
        $this->Security->unlockedActions = array('forms');

        // レイアウト
        $this->layout = 'landing';
    }

    /**
     * お申し込みフォーム表示処理
     * ・URLを基準にページを読み込むため適切なviewが必要
     * ・viewをコピーして別のページを作ることが可能
     * ・view名が一致すればroutingを指定することなく表示可能
     */
    public function display()
    {
        // URLを配列で取得
        $path = func_get_args();

        $count = count($path);
        if (!$count) {
            // URLが不適切な場合はトップページへ戻す
            return $this->redirect('/');
        }
        $page = null;
        $subpage = null;
        if (!empty($path[0])) {
            // 例）/lp/default
            $page = $path[0];
        }
        if (!empty($path[1])) {
            // 例）/lp/default/subpage
            $subpage = $path[1];
        }
        $this->set(compact('page', 'subpage'));

        try {
            // 指定されたviewを表示
            $this->render(implode('/', $path));
        } catch (MissingViewException $e) {
            // viewが無い場合はトップページへ戻す
            $this->redirect('/');
        }
    }

    /**
     * フォーム内容を受け取りデータベース保存後にメールを送信する
     * 求人IDと関連付けされた場合は管理画面に表示される
     *
     * @param null $id recruit id
     */
    public function forms($id = null)
    {
        $this->autoRender = false;

        if ($this->request->is('post') || $this->request->is('put')) {

            $data = $this->request->data;
            $referer = $this->referer(null, true);
            $uri = explode('/', $referer);

            // 受信先をデータに含める
            $reception = '';
            if ($uri[2] == 'pref27') {
                $reception = Configure::read('reception27');
            } elseif ($uri[2] == 'pref34') {
                $reception = Configure::read('reception34');
            } elseif ($uri[2] == 'pref40') {
                $reception = Configure::read('reception40');
            }

            $data['Customer']['reception'] = $reception;


            // 求人IDがあればデータに含める
            if (!empty($id)) {
                $data['Customer']['recruit_id'] = $id;
            }

            // DB必須項目を埋める
            $data['Customer']['birth']['month']['month'] = '01';
            $data['Customer']['birth']['day']['day'] = '01';
            $this->log($data, 'debug');

            // 保存が成功したらメール送信
            if ($this->Customer->formDataSave($data)) {
                $this->redirect(array('action' => 'thanks'));
            } else {
                // 保存に失敗したらリファラで戻す
                $this->redirect($this->referer());
            }
        }
    }

    /**
     * お申し込み完了ページ
     */
    public function thanks()
    {
        $this->set('title_for_layout', '無料転職登録のお申込みありがとうございます');
        // viewの表示のみ
    }

}