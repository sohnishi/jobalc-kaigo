<?php

/**
 * Recruits Plugin Routes
 */
Router::connect('/recruits/search', array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'search'));
Router::connect('/recruits/results/*', array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'results'));
Router::connect('/recruits/detail/*', array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'detail'));
Router::connect('/admin/recruits/add', array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'add', 'admin' => true));
Router::connect('/admin/recruits/edit/*', array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'edit', 'admin' => true));
Router::connect('/admin/recruits/view/*', array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'view', 'admin' => true));
Router::connect('/admin/recruits/import', array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'import', 'admin' => true));
Router::connect('/admin/recruits/:controller/:action/*', array('plugin' => 'recruits', 'admin' => true));

