<?php
	// 求人オプション
	$shikaku_options = Configure::read('shikaku');
	$kinmu_keitai_options = Configure::read('kinmu_keitai');
?>
<div class="search-selected">
	<h4 class="page-header"><i class="fa fa-search"></i> 検索条件</h4>
	<div class="selected-body">
		<ul class="list-inline">
			<?php if (isset($selected['todoufuken']) || isset($selected['pref_cd'])) : ?>
			<li>都道府県：
				<?php
					$todoufuken = isset($selected['todoufuken']) ? '<span class="label label-primary">' . $selected['todoufuken'] . "</span>\n" : '';
					$prefCd = isset($selected['pref_cd']) ? '<span class="label label-primary">' . $prefs[$selected['pref_cd']] . "</span>\n" : '';
					echo $todoufuken . $prefCd;
				?>
			</li>
			<?php endif; ?>
			<?php if (isset($selected['shikutyouson'])) : ?>
			<li>市区町村：
				<?php
					echo '<span class="label label-primary">' . $selected['shikutyouson'] . "</span>\n";
				?>
			</li>
			<?php endif; ?>
			<?php if (isset($selected['line_cd'])) : ?>
			<li>路線：
				<?php
					echo '<span class="label label-primary">' . $lines[$selected['line_cd']] . "</span>\n";
				?>
			</li>
			<?php endif; ?>
			<?php if (isset($selected['station_cd'])) : ?>
			<li>駅：
				<?php
					echo '<span class="label label-primary">' . $stations[$selected['station_cd']] . "</span>\n";
				?>
			</li>
			<?php endif; ?>
			<?php if (isset($selected['shikaku'])) : ?>
			<li>資格：
				<?php
					foreach ($selected['shikaku'] as $shikaku) {
						echo '<span class="label label-primary">' . $shikaku_options[$shikaku] . "</span>\n";
					}
				?>
			</li>
			<?php endif; ?>
			<?php if (isset($selected['kinmu_keitai'])) : ?>
			<li>勤務形態：
				<?php
					foreach ($selected['kinmu_keitai'] as $kinmu_keitai) {
						echo '<span class="label label-primary">' . $kinmu_keitai_options[$kinmu_keitai] . "</span>\n";
					}
				?>
			</li>
			<?php endif; ?>
			<?php if (isset($selected['shisetsukeitai_first'])) : ?>
			<li>施設：
				<?php
					if (isset($selected['shisetsukeitai_first'])) {
						foreach ($selected['shisetsukeitai_first'] as $shisetsukeitai_first) {
							echo '<span class="label label-primary">' . $shisetsukeitaiFirsts[$shisetsukeitai_first] . "</span>\n";
						}
					} else {
						echo $nonselect;
					}
				?>
			</li>
			<?php endif; ?>
			<?php if (isset($selected['shisetsukeitai_second_id'])) : ?>
			<li>募集部署：
				<?php
					foreach ($selected['shisetsukeitai_second_id'] as $shisetsukeitai_second) {
						echo '<span class="label label-primary">' . $shisetsukeitaiSeconds[$shisetsukeitai_second] . "</span>\n";
					}
				?>
			</li>
			<?php endif; ?>
			<?php if (isset($selected['shinryoukamoku'])) : ?>
			<li>診療科目：
				<?php
					foreach ($selected['shinryoukamoku'] as $shinryoukamoku) {
						echo '<span class="label label-primary">' . $shinryoukamokus[$shinryoukamoku] . "</span>\n";
					}
				?>
			</li>
			<?php endif; ?>
			<?php if (isset($selected['kodawari'])) : ?>
			<li>こだわった条件：
				<?php
					foreach ($selected['kodawari'] as $kodawari) {
						echo '<span class="label label-primary">' . $kodawaris[$kodawari] . "</span>\n";
					}
				?>
			</li>
			<?php endif; ?>
			<?php if (isset($selected['keyword'])) : ?>
			<li>キーワード：
				<?php
					echo '<span class="label label-primary">' . $selected['keyword'] . "</span>\n";
				?>
			</li>
			<?php endif; ?>
		</ul>
	</div>
</div>
