<?php
	$shikaku_options = Configure::read('shikaku');
	$kinmu_keitai_options = Configure::read('kinmu_keitai');
?>
<article class="facility-item">
	<div class="row">
		<div class="col-xs-12">
			<hgroup>
				<h2>
					<?php
						if ($item['Facility']['FacilityShow']['groupmei_chk'] == 1) {
							echo empty($item['Facility']['group']) ? '' : $item['Facility']['group'] . '&nbsp;';
						}
						if ($item['Facility']['FacilityShow']['houjinmei_chk'] == 1) {
							echo empty($item['Facility']['corporation']) ? '' : $item['Facility']['corporation'] . '&nbsp;';
						}
					?>
				</h2>
				<h3><?php echo $item['Facility']['shisetsumei']; ?></h3>
			</hgroup>
			<div class="address">
				<?php if (!empty($item['Facility']['todoufuken']) || !empty($item['Facility']['shikutyouson'])) : ?>
				<i class="fa fa-circle text-primary"></i>&nbsp;
				<?php echo !empty($item['Facility']['todoufuken']) ? $item['Facility']['todoufuken'] : ''; ?>
				<?php echo !empty($item['Facility']['shikutyouson']) ? $item['Facility']['shikutyouson'] : ''; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-6">
			<div class="facility-image">
				<?php
					$facilityImg = !empty($item['Facility']['facility_image']) ? '/files/facility/facility_image/' . $item['Facility']['facility_dir'] . '/' . $item['Facility']['facility_image'] : 'no_image.jpg';
					echo $this->Html->image($facilityImg, array('class' => 'img-responsive'));
				?>
			</div>
		</div>
		<div class="col-xs-6">
			<div class="kodawari hidden-xs">
				<ul class="list-inline">
					<?php
						foreach ($item['Kodawari'] as $kodawariItem) {
							echo '<li><span>' . $kodawariItem['name'] . '</span></li>';
						}
					?>
				</ul>
			</div>
			<p class="description"><?php echo !empty($item['Facility']['kanrenshisetsu']) ? nl2br($item['Facility']['kanrenshisetsu']) : ''; ?></p>
		</div>
	</div>
	<div class="recruit-item">
		<div class="row">
			<div class="col-xs-7">
			<?php if ($item['RecruitShow']['shikaku_chk']) : ?>
			<dl>
				<dt><span>資　　格：</span></dt>
				<dd><?php echo $shikaku_options[$item['Recruit']['shikaku']]; ?></dd>
			</dl>
			<?php endif; ?>
			<?php if ($item['RecruitShow']['busyo_chk']) : ?>
			<dl>
				<dt><span>部　　署：</span></dt>
				<dd><?php echo $shisetsukeitaiSeconds[$item['Recruit']['shisetsukeitai_second_id']]; ?></dd>
			</dl>
			<?php endif; ?>
			<?php if ($item['RecruitShow']['kinmu_keitai_chk']) : ?>
			<dl>
				<dt><span>勤務形態：</span></dt>
				<dd><?php echo $kinmu_keitai_options[$item['Recruit']['kinmu_keitai']]; ?></dd>
			</dl>
			<?php endif; ?>
			<?php if ($item['RecruitShow']['kyuyo_chk']) : ?>
			<dl>
				<dt><span>給与(月給)：</span></dt>
				<dd style="margin-left:85px;"><?php echo nl2br($item['Recruit']['kyuyo']); ?></dd>
			</dl>
			<?php endif; ?>
				<div class="arrow visible-xs">
					<span class="fa fa-chevron-right"></span>
				</div>
			</div>
			<div class="col-xs-5 text-right">
				<a href="<?php echo $this->Html->url('/recruits/detail/' . $item['Recruit']['id']); ?>" class="recruit-btn btn btn-primary btn-lg hidden-xs">この求人の詳細を見る</a>
			</div>
		</div>
	</div>
	<a class="pc-link" href="<?php echo $this->Html->url('/recruits/detail/' . $item['Recruit']['id']); ?>"></a>
	<a class="sp-link" href="<?php echo $this->Html->url('/recruits/detail/' . $item['Recruit']['id']); ?>"></a>
</article>
