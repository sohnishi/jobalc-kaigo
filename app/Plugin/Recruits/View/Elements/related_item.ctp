<?php
	$shikaku_options = Configure::read('shikaku');
	$kinmu_keitai_options = Configure::read('kinmu_keitai');
?>

<div class="recruit-item">
	<div class="row">
		<div class="col-xs-7">
			<?php if ($item['RecruitShow']['shikaku_chk']) : ?>
			<dl>
				<dt><span>資　　格：</span></dt>
				<dd><?php echo $shikaku_options[$item['Recruit']['shikaku']]; ?></dd>
			</dl>
			<?php endif; ?>
			<?php if ($item['RecruitShow']['busyo_chk']) : ?>
			<dl>
				<dt><span>部　　署：</span></dt>
				<dd><?php echo $shisetsukeitaiSeconds[$item['Recruit']['shisetsukeitai_second_id']]; ?></dd>
			</dl>
			<?php endif; ?>
			<?php if ($item['RecruitShow']['kinmu_keitai_chk']) : ?>
			<dl>
				<dt><span>勤務形態：</span></dt>
				<dd><?php echo $kinmu_keitai_options[$item['Recruit']['kinmu_keitai']]; ?></dd>
			</dl>
			<?php endif; ?>
			<?php if ($item['RecruitShow']['kyuyo_chk']) : ?>
			<dl>
				<dt><span>給与(月給)：</span></dt>
				<dd style="margin-left:85px;"><?php echo nl2br($item['Recruit']['kyuyo']); ?></dd>
			</dl>
			<?php endif; ?>
			<div class="arrow visible-xs">
				<span class="fa fa-chevron-right"></span>
			</div>
		</div>
		<div class="col-xs-5 text-right">
			<a href="<?php echo $this->Html->url('/recruits/detail/' . $item['Recruit']['id']); ?>" class="recruit-btn btn btn-primary btn-lg hidden-xs">この求人の詳細を見る</a>
		</div>
	</div>
	<a class="sp-link" href="<?php echo $this->Html->url('/recruits/detail/' . $item['Recruit']['id']); ?>"></a>
</div>
