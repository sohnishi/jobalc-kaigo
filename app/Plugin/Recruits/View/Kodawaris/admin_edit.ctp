<div class="row">
	<div class="col-xs-12 contents-actions">
		<ul class="nav nav-tabs">
			<li><a href="<?php echo $this->Html->url('/admin/recruits/kodawaris'); ?>">こだわり一覧</a></li>
			<li class="active"><a href="#">こだわりを更新</a></li>
		</ul>
	</div>
</div>

<div class="row">
	<div class="col-xs-4 col-xs-offset-4">

		<div class="panel panel-default">
			<div class="panel-heading">こだわりを更新</div>
			<div class="panel-body">

				<?php
					echo $this->Form->create($model, array(
						'inputDefaults' => array(
							'label' => false,
							'div' => false,
							'class' => 'form-control'
						)
					));
					echo $this->Form->input('id');
				?>
				<div class="form-group">
					<?php echo $this->Form->input('name', array('label' => 'こだわり')); ?>
				</div>

				<div class="form-group">
					<?php echo $this->Form->input('weight', array('type' => 'text', 'label' => '並び順')); ?>
				</div>

				<div class="form-group">
					<ul class="list-inline">
						<li><?php echo $this->Form->submit('こだわりを更新する', array('class' => 'btn btn-primary')); ?></li>
					</ul>
				</div>
				<?php echo $this->Form->end(); ?>

			</div>
		</div>

	</div>
</div>