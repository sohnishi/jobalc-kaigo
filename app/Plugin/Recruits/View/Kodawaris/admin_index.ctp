<div class="row">
	<div class="col-xs-12 contents-actions">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#">こだわり一覧</a></li>
			<li><a href="<?php echo $this->Html->url('/admin/recruits/kodawaris/add'); ?>">こだわりを新規登録</a></li>
		</ul>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">

		<div class="panel panel-default">
			<div class="panel-heading">こだわり一覧</div>
			<div class="panel-body">

				<table class="table table-striped">
					<thead>
						<tr>
							<th><?php echo $this->Paginator->sort('id', 'ID'); ?></th>
							<th><?php echo $this->Paginator->sort('name', 'こだわり'); ?></th>
							<th><?php echo $this->Paginator->sort('weight', '並び順'); ?></th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($kodawaris as $kodawari) : ?>
						<tr>
							<td><?php echo $kodawari[$model]['id']; ?></td>
							<td><?php echo $kodawari[$model]['name']; ?></td>
							<td><?php echo $kodawari[$model]['weight']; ?></td>
							<td>
								<ul class="list-inline">
									<li><?php echo $this->Html->link('編集', array('action' => 'edit', $kodawari[$model]['id'])); ?></li>
									<li><?php echo $this->Form->postLink('削除', array('action' => 'delete', $kodawari[$model]['id']), '', '本当に削除しますか？元には戻せません'); ?></li>
								</ul>
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
				<div class="text-center">
					<?php echo $this->Paginator->pagination(array('ul' => 'pagination')); ?>
				</div>

			</div>
		</div>

	</div>
</div>