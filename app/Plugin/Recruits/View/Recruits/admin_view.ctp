<?php

	$shikaku_options = Configure::read('shikaku');
	/*
		20190527 sohnishi	
		職種追加
	*/
	$shokushu_options = Configure::read('shokushu');
	$busyo_options = Configure::read('busyo');
	$kinmu_keitai_options = Configure::read('kinmu_keitai');
	$syoukyu_options = Configure::read('syoukyu');
	$syouyo_kaisu_options = Configure::read('syouyo_kaisu');
	$syakaihoken_options = Configure::read('syakaihoken');
	$kodawari_options = Configure::read('kodawari');
	$show_options = Configure::read('recruit_show_options');

	$show_portal = ' <span class="text-success">*</span>';

	$recruit = $recruits[0];

?>
<div class="row">
	<div class="col-xs-12 contents-actions">
		<ul class="nav nav-tabs">
			<li><?php echo $this->Html->link('求人情報検索', array('action' => 'index', '?' => $this->request->query)); ?></li>
			<li><?Php echo $this->Html->link('求人情報を更新', Hash::merge(array('action' => 'edit', $recruit[$model]['id']), $this->request->params['named'], array('?' => $this->Session->read('recruitquerystring')))); ?></li>
			<li class="active"><a href="javascript:void(0);">求人情報を表示</a></li>
		</ul>
	</div>
</div>

<div class="row">
	<div class="col-xs-6">
		<h3><?php echo $recruit['Facility']['shisetsumei']; ?> の求人情報</h3>
	</div>
	<div class="col-xs-6">
		<ul class="pager">
			<?php echo $this->Paginator->prev('< 前へ', array('class' => 'previous', 'disabled' => 'previous disabled')); ?>
			<li><?php echo $this->Paginator->counter('{:page} / {:pages}'); ?></li>
			<?php echo $this->Paginator->next('次へ >', array('class' => 'next', 'disabled' => 'next disabled')); ?>
		</ul>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">

		<div class="panel panel-default">
			<div class="panel-heading">求人情報の表示 [<span class="text-success">*</span>] はポータルサイトに表示</div>
			<div class="panel-body">

				<div class="col-xs-6">
					<table class="table table-bordered">
						<tbody>
							<tr>
								<th>求人掲載施設</th>
								<td><?php echo $recruit['Facility']['shisetsumei']; ?></td>
							</tr>
							<tr>
								<th>掲載状態</th>
								<td><?php echo ($recruit[$model]['active'] == 1) ? '<span class="text-success">掲載中</span>' : '非掲載'; ?></td>
							</tr>
							<tr>
								<th>掲載ページ</th>
								<td>
									<?php 
										if ($recruit[$model]['active'] == 1) {
											echo $this->Html->link(
												FULL_BASE_URL . '/cw/recruits/detail/' . $recruit[$model]['id'],
												FULL_BASE_URL . '/cw/recruits/detail/' . $recruit[$model]['id'],
												array('escape' => false)); 
										}
									?>
								</td>
							</tr>
							<tr>
								<th>新着求人</th>
								<td><?php echo ($recruit[$model]['shintyaku_kyujin'] == 1) ? '<i class="fa fa-check"></i>' : ''; ?></td>
							</tr>
							<tr>
								<th>人気求人</th>
								<td><?php echo ($recruit[$model]['ninki_kyujin'] == 1) ? '<i class="fa fa-check"></i>' : ''; ?></td>
							</tr>
							<tr>
								<th>こだわりポイント</th>
								<td>
									<?php foreach ($recruit['Kodawari'] as $kodawari) : ?>
									<span>[<?php echo $kodawari['name']; ?>]</span>
									<?php endforeach; ?>
								</td>
							</tr>
							<tr>
								<th>資格<?php if ($recruit['RecruitShow']['shikaku_chk']) { echo $show_portal; } ?></th>
								<td><?php echo $shikaku_options[$recruit['Recruit']['shikaku']]; ?></td>
							</tr>
							<!-- 
								20190527 sohnishi	
								職種追加
							-->
							<tr>
								<th>職種<?php if ($recruit['RecruitShow']['shokushu_chk']) { echo $show_portal; } ?></th>
								<td><?php echo $shokushu_options[$recruit['Recruit']['shokushu']]; ?></td>
							</tr>
							<tr>
								<th>部署<?php if ($recruit['RecruitShow']['busyo_chk']) { echo $show_portal; } ?>	</th>
								<td><?php echo $recruit['ShisetsukeitaiSecond']['name']; ?></td>
							</tr>
							<tr>
								<th>勤務形態<?php if ($recruit['RecruitShow']['kinmu_keitai_chk']) { echo $show_portal; } ?></th>
								<td><?php echo $kinmu_keitai_options[$recruit['Recruit']['kinmu_keitai']]; ?></td>
							</tr>
							<tr>
								<th>勤務時間<?php if ($recruit['RecruitShow']['kinmu_jikan_chk']) { echo $show_portal; } ?></th>
								<td><?php echo nl2br($recruit['Recruit']['kinmu_jikan']); ?></td>
							</tr>
							<tr>
								<th>仕事内容<?php if ($recruit['RecruitShow']['shigoto_chk']) { echo $show_portal; } ?></th>
								<td><?php echo nl2br($recruit['Recruit']['shigoto']); ?></td>
							</tr>
							<tr>
								<th>給与（月給）<?php if ($recruit['RecruitShow']['kyuyo_chk']) { echo $show_portal; } ?></th>
								<td><?php echo nl2br($recruit['Recruit']['kyuyo']); ?></td>
							</tr>
							<tr>
								<th>給与（年収）<?php if ($recruit['RecruitShow']['nensyu_chk']) { echo $show_portal; } ?></th>
								<td>
									<?php foreach ($recruit['Nensyu'] as $nensyu) : ?>
									<span>[<?php echo $nensyu['name']; ?>]</span>
									<?php endforeach; ?>
								</td>
							</tr>
							<tr>
								<th>夜勤手当<?php if ($recruit['RecruitShow']['yakin_chk']) { echo $show_portal; } ?></th>
								<td><?php echo nl2br($recruit['Recruit']['yakin_kingaku']); ?></td>
							</tr>
							<tr>
								<th>その他手当<?php if ($recruit['RecruitShow']['teate_chk']) { echo $show_portal; } ?></th>
								<td><?php echo nl2br($recruit['Recruit']['sonota_teate']); ?></td>
							</tr>
							<tr>
								<th>休日体制<?php if ($recruit['RecruitShow']['kyujitsu_chk']) { echo $show_portal; } ?></th>
								<td>
									<?php foreach ($recruit['Kyujitsu'] as $kyujitsu) : ?>
									<span>[<?php echo $kyujitsu['name']; ?>]</span>
									<?php endforeach; ?>
								</td>
							</tr>
							<tr>
								<th>昇給</th>
								<td><?php echo $syoukyu_options[$recruit['Recruit']['syoukyu']]; ?></td>
							</tr>
							<tr>
								<th>賞与（回数/年）</th>
								<td><?php echo $syouyo_kaisu_options[$recruit['Recruit']['syouyo_kaisu']]; ?></td>
							</tr>
							<tr>
								<th>賞与（実績/年）</th>
								<td><?php echo !empty($recruit['Recruit']['syouyo_jisseki']) ? $recruit['Recruit']['syouyo_jisseki'] . 'ヶ月' : ''; ?></td>
							</tr>
							<tr>
								<th>年間休日<?php if ($recruit['RecruitShow']['nenkan_kyujitsu_chk']) { echo $show_portal; } ?></th>
								<td>
									<?php if (!empty($recruit['Recruit']['nenkan_kyujitsu'])) : ?>
									<?php echo $recruit['Recruit']['nenkan_kyujitsu']; ?>日
									<?php endif; ?>
								</td>
							</tr>
							<tr>
								<th>有給<?php if ($recruit['RecruitShow']['yukyu_chk']) { echo $show_portal; } ?></th>
								<td><?php echo nl2br($recruit['Recruit']['yukyu']); ?></td>
							</tr>
							<tr>
								<th>有給消化率<?php if ($recruit['RecruitShow']['yukyu_syouka_chk']) { echo $show_portal; } ?></th>
								<td>
									<?php if (!empty($recruit['Recruit']['yukyu_syouka'])) : ?>
									<?php echo $recruit['Recruit']['yukyu_syouka']; ?>%
									<?php endif; ?>
								</td>
							</tr>
						</tbody>
					</table>
				</div>

				<div class="col-xs-6">
					<table class="table table-bordered">
						<tr>
							<th>産休育休<?php if ($recruit['RecruitShow']['sankyu_chk']) { echo $show_portal; } ?></th>
							<td><?php echo ($recruit['Recruit']['sankyu'] == 1) ? '実績あり' : '実績なし'; ?></td>
						</tr>
						<tr>
							<th>産休育休詳細<?php if ($recruit['RecruitShow']['sankyu_syousai_chk']) { echo $show_portal; } ?></th>
							<td><?php echo nl2br($recruit['Recruit']['sankyu_syousai']); ?></td>
						</tr>
						<tr>
							<th>託児所<?php if ($recruit['RecruitShow']['takujisyo_chk']) { echo $show_portal; } ?></th>
							<td><?php echo ($recruit['Recruit']['takujisyo'] == 1) ? 'あり' : 'なし'; ?></td>
						</tr>
						<tr>
							<th>託児所詳細<?php if ($recruit['RecruitShow']['takujisyo_syousai_chk']) { echo $show_portal; } ?></th>
							<td><?php echo nl2br($recruit['Recruit']['takujisyo_syousai']); ?></td>
						</tr>
						<tr>
							<th>寮<?php if ($recruit['RecruitShow']['ryou_chk']) { echo $show_portal; } ?></th>
							<td><?php echo ($recruit['Recruit']['ryou'] == 1) ? 'あり' : 'なし'; ?></td>
						</tr>
						<tr>
							<th>寮詳細<?php if ($recruit['RecruitShow']['ryou_syousai_chk']) { echo $show_portal; } ?></th>
							<td><?php echo nl2br($recruit['Recruit']['ryou_syousai']); ?></td>
						</tr>
						<tr>
							<th>車通勤<?php if ($recruit['RecruitShow']['tukin_chk']) { echo $show_portal; } ?></th>
							<td><?php echo ($recruit['Recruit']['tukin'] == 1) ? '可' : '不可'; ?></td>
						</tr>
						<tr>
							<th>車通勤詳細<?php if ($recruit['RecruitShow']['tukin_syousai_chk']) { echo $show_portal; } ?></th>
							<td><?php echo nl2br($recruit['Recruit']['tukin_syousai']); ?></td>
						</tr>
						<tr>
							<th>退職金<?php if ($recruit['RecruitShow']['taisyokukin_chk']) { echo $show_portal; } ?></th>
							<td><?php echo ($recruit['Recruit']['taisyokukin'] == 1) ? 'あり' : 'なし'; ?></td>
						</tr>
						<tr>
							<th>退職金詳細<?php if ($recruit['RecruitShow']['taisyokukin_syousai_chk']) { echo $show_portal; } ?></th>
							<td><?php echo nl2br($recruit['Recruit']['taisyokukin_syousai']); ?></td>
						</tr>
						<tr>
							<th>社会保険<?php if ($recruit['RecruitShow']['syakaihoken_chk']) { echo $show_portal; } ?></th>
							<td>
								<?php foreach ($recruit['Recruit']['syakaihoken'] as $syakaihoken) : ?>
								<span>[<?php echo $syakaihoken_options[$syakaihoken]; ?>]</span>
								<?php endforeach; ?>
							</td>
						</tr>
						<tr>
							<th>その他福利厚生<?php if ($recruit['RecruitShow']['hukurikousei_chk']) { echo $show_portal; } ?></th>
							<td><?php echo nl2br($recruit['Recruit']['hukurikousei']); ?></td>
						</tr>
						<!-- 
							20190513 sohnishi
							求人予備項目を追加
							$index：配列作成用のインデックス アソシエーションモデルのデータを同時に保存するために配列構造にする必要がある
							$recruitPreliminaryItems：求人予備項目マスタリスト
						-->
						<?php
							$index = 0;
							foreach($recruitPreliminaryItems as $id => $recruitPreliminaryItem) {
						?>
								<tr>
									<th><?php echo $recruitPreliminaryItem; if (count($recruit['RecruitPreliminaryItem']) > $index && $recruit['RecruitPreliminaryItem'][$index]['RecruitsRecruitPreliminaryItem']['is_show']) { echo $show_portal; } ?></th>
									<td><?php if (count($recruit['RecruitPreliminaryItem']) > $index) { echo nl2br($recruit['RecruitPreliminaryItem'][$index]['RecruitsRecruitPreliminaryItem']['content']); } ?></td>
								</tr>
						<?php
								$index += 1;
							}
						?>
					</table>
				</div>

			</div>
		</div>

	</div>
</div>

<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-default">
			<div class="panel-heading">応募状況</div>
			<div class="panel-body">
				<?php
					$shikaku = Configure::read('shikaku');
					$kinmukaishijiki = Configure::read('kinmukaishijiki');

					$customers = $recruit['Customer'];
					foreach ($customers as $customer) :
				?>
				<table class="table table-bordered table-condensed table-fixed" style="margin-bottom:5px;">
					<tbody>
						<tr>
							<th>名前</th>
							<td><?php echo $customer['name']; ?></td>
							<th>フリガナ</th>
							<td><?php echo $customer['hurigana']; ?></td>
							<th>生年月日</th>
							<td><?php echo date('Y年n月j日', strtotime($customer['birth'])); ?>(<?php echo $this->Custom->birthToAge($customer['birth']); ?>歳)</td>
						</tr>
						<tr>
							<th>電話番号</th>
							<td><?php echo $customer['keitai_tel']; ?></td>
							<th>メールアドレス</th>
							<td><?php echo $customer['email']; ?></td>
							<th>住所</th>
							<td>
								<?php echo '〒' . preg_replace("/^(\d{3})(\d{4})$/", "$1-$2", $customer['zip']); ?>
								<?php echo $customer['todoufuken'] . $customer['shikutyouson'] . $customer['banchi']; ?>
							</td>
						</tr>
						<tr>
							<th>保有資格</th>
							<td>
								<?php
									foreach ($customer['CustomerCertificate'] as $certificate) {
										echo '[' . $shikaku[$certificate['shikaku']] . ']';
									}
								?>
							</td>
							<th>希望の働き方</th>
							<td>
								<?php
									$tmpCondition = $customer['CustomerCondition'];
									foreach ($customer['CustomerCondition'] as $condition) {
										echo nl2br($condition['kibouwork']);
										if (next($tmpCondition)) {
											echo "、";
										}
									}
								?>
							</td>
							<th>希望入職時期</th>
							<td>
								<?php
									foreach ($customer['CustomerCondition'] as $condition) {
										echo '[' . $kinmukaishijiki[$condition['kinmukaishijiki']] . ']';
									}
								?>
							</td>
						</tr>
						<tr>
							<th>その他</th>
							<td colspan="4"><?php echo nl2br($customer['memo']); ?></td>
							<td class="text-right">
								<?php echo $this->Form->postLink('削除', array('plugin' => 'customers', 'controller' => 'customers', 'action' => 'delete', $customer['id'], 'admin' => true), array('class' => 'btn btn-danger btn-xs'), '本当に削除しますか？元には戻せません'); ?>
								<?php echo $this->Html->link('編集', array('plugin' => 'customers', 'controller' => 'customers', 'action' => 'edit', $customer['id'], 'admin' => true), array('class' => 'btn btn-primary btn-xs')); ?>
							</td>
						</tr>
					</body>
				</table>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</div>
