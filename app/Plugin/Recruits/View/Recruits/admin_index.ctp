<?php
	echo $this->Html->css('bootstrap-multiselect', array('inline' => false));
	echo $this->Html->script('bootstrap-multiselect', array('inline' => false));

	$shikaku_options = Configure::read('shikaku');
	$busyo_options = Configure::read('busyo');
	$kinmu_keitai_options = Configure::read('kinmu_keitai');
	$syoukyu_options = Configure::read('syoukyu');
	$syouyo_kaisu_options = Configure::read('syouyo_kaisu');
	$syouyo_jisseki_options = array(
		'2.0ヶ月以下',
		'2.1～2.9ヶ月',
		'3.0～3.9ヶ月',
		'4.0ヶ月以上'
	);
	$nenkan_kyujitsu_options = array(
		'100日以下',
		'100～109日',
		'110～119日',
		'120日以上'
	);

?>

<?php echo $this->Html->scriptStart(); ?>
$(function () {
	$('.collapse').on('hidden.bs.collapse', function () {
		$(this).parent().find(".fa-minus").removeClass("fa-minus").addClass("fa-plus");
	}).on('show.bs.collapse', function () {
		$(this).parent().find(".fa-plus").removeClass("fa-plus").addClass("fa-minus");
	});

	$('#RecruitShikaku, #RecruitShisetsukeitaiSecondId, #RecruitKinmuKeitai, #RecruitNensyu, #RecruitSyoukyu, #RecruitSyouyoKaisu, #RecruitSyouyoJisseki, #RecruitKyujitsu, #RecruitNenkanKyujitsu, #RecruitKodawari').multiselect({
		nonSelectedText: '選択してください',
		nSelectedText: '選択',
		allSelectedText: '全選択',
		numberDisplayed: 1
	});
});
<?php echo $this->Html->scriptEnd(); ?>

<div class="row">
	<div class="col-xs-12 contents-actions">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#">求人情報検索</a></li>
			<li><a href="<?php echo $this->Html->url('/admin/recruits/add'); ?>">求人情報を新規登録</a></li>
		</ul>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<a href="#recruit-search" data-toggle="collapse" aria-expanded="false" aria-controls="recruit-search"><i class="fa fa-plus"></i> 求人情報を検索</a>
			</div>
			<div class="panel-body collapse" id='recruit-search'>

				<?php
					echo $this->Form->create($model, array(
						'inputDefaults' => array(
							'label' => false,
							'wrapInput' => false,
							'div' => false,
							'class' => 'form-control'
						),
						'url' => array_merge(
								array(
									'plugin' => 'recruits',
									'controller' => 'recruits',
									'action' => 'index',
									'admin' => true
								),
								$this->params['pass']
						)
					));
				?>
				<div class="row">
					<div class="col-xs-2">
						<?php echo $this->Form->label('shikaku', '資格'); ?>
						<div class="form-group">
							<?php echo $this->Form->input('shikaku', array('type' => 'select', 'options' => $shikaku_options, 'multiple' => true)); ?>
						</div>
					</div>
					<div class="col-xs-2">
						<?php echo $this->Form->label('shisetsukeitai_second_id', '部署'); ?>
						<div class="form-group">
							<?php echo $this->Form->input('shisetsukeitai_second_id', array('type' => 'select', 'options' => $shisetsukeitaiSeconds, 'multiple' => true)); ?>
						</div>
					</div>
					<div class="col-xs-2">
						<?php echo $this->Form->label('kinmu_keitai', '勤務形態'); ?>
						<div class="form-group">
							<?php echo $this->Form->input('kinmu_keitai', array('type' => 'select', 'options' => $kinmu_keitai_options, 'multiple' => true)); ?>
						</div>
					</div>
					<div class="col-xs-2">
						<?php echo $this->Form->label('nensyu', '給与（年収）'); ?>
						<div class="form-group">
							<?php echo $this->Form->input('nensyu', array('type' => 'select', 'options' => $nensyus, 'multiple' => true)); ?>
						</div>
					</div>
					<div class="col-xs-4">
						<div class="form-group">
							<?php echo $this->Form->input('sonota_teate', array('type' => 'text', 'label' => 'その他手当て')); ?>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-2">
						<?php echo $this->Form->label('syoukyu', '昇給'); ?>
						<div class="form-group">
							<?php echo $this->Form->input('syoukyu', array('type' => 'select', 'options' => $syoukyu_options, 'multiple' => true)); ?>
						</div>
					</div>
					<div class="col-xs-2">
						<?php echo $this->Form->label('syouyo_kaisu', '賞与（回数/年）'); ?>
						<div class="form-group">
							<?php echo $this->Form->input('syouyo_kaisu', array('type' => 'select', 'options' => $syouyo_kaisu_options, 'multiple' => true)); ?>
						</div>
					</div>
					<div class="col-xs-2">
						<?php echo $this->Form->label('syouyo_jisseki', '賞与（実績/年）'); ?>
						<div class="form-group">
							<?php echo $this->Form->input('syouyo_jisseki', array('type' => 'select', 'options' => $syouyo_jisseki_options, 'multiple' => true)); ?>
						</div>
					</div>
					<div class="col-xs-2">
						<?php echo $this->Form->label('kyujitsu', '休日体制'); ?>
						<div class="form-group">
							<?php echo $this->Form->input('kyujitsu', array('type' => 'select', 'options' => $kyujitsus, 'multiple' => true)); ?>
						</div>
					</div>
					<div class="col-xs-2">
						<?php echo $this->Form->label('nenkan_kyujitsu', '年間休日'); ?>
						<div class="form-group">
							<?php echo $this->Form->input('nenkan_kyujitsu', array('type' => 'select', 'options' => $nenkan_kyujitsu_options, 'multiple' => true)); ?>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->input('sankyu', array('type' => 'select', 'options' => array(0 => 'なし', 1 => 'あり'), 'empty' => '-- 選択してください --', 'label' => '産休育休')); ?>
						</div>
					</div>
					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->input('ryou', array('type' => 'select', 'options' => array(0 => 'なし', 1 => 'あり'), 'empty' => '-- 選択してください --', 'label' => '寮')); ?>
						</div>
					</div>
					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->input('takujisyo', array('type' => 'select', 'options' => array(0 => 'なし', 1 => 'あり'), 'empty' => '-- 選択してください --', 'label' => '託児所')); ?>
						</div>
					</div>
					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->input('tukin', array('type' => 'select', 'options' => array(0 => '不可', 1 => '可'), 'empty' => '-- 選択してください --', 'label' => '車通勤')); ?>
						</div>
					</div>
					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->input('taisyokukin', array('type' => 'select', 'options' => array(0 => 'なし', 1 => 'あり'), 'empty' => '-- 選択してください --', 'label' => '退職金')); ?>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-2">
						<?php echo $this->Form->label('kodawari', 'こだわりポイント'); ?>
						<div class="form-group">
							<?php echo $this->Form->input('kodawari', array('type' => 'select', 'options' => $kodawaris, 'multiple' => true)); ?>
						</div>
					</div>
					<div class="col-xs-2">
						<?php echo $this->Form->label('shintyaku_kyujin', '新着チェック'); ?>
						<?php echo $this->Form->unlockField('shintyaku_kyujin'); ?>
						<?php echo $this->Form->input('shintyaku_kyujin', array('label' => '新着求人', 'class' => false, 'hiddenField' => false)); ?>
					</div>
					<div class="col-xs-2">
						<?php echo $this->Form->label('ninki_kyujin', '人気チェック'); ?>
						<?php echo $this->Form->unlockField('ninki_kyujin'); ?>
						<?php echo $this->Form->input('ninki_kyujin', array('label' => '人気求人', 'class' => false, 'hiddenField' => false)); ?>
					</div>
				</div>
				<!-- 
					20190513 sohnishi
					求人予備項目を追加
					$index: 配列作成用のインデックス アソシエーションモデルのデータを同時に保存するために配列構造にする必要がある
				-->
				<div class="row">
				<?php
					$index = 0;
					foreach($recruitPreliminaryItems as $id => $recruitPreliminaryItem) {
				?>
					<div class="col-xs-4">
						<div class="form-group">
							<?php echo $this->Form->input('Recruit.recruit_preliminary_item.' . $index, array('type' => 'text', 'label' => $recruitPreliminaryItem, 'required' => 'false')); ?>
						</div>
					</div>
				<?php
						$index += 1;
					}
				?>
				</div>
				<hr>

				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<ul class="list-inline">
								<li><button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> 求人情報検索</button></li>
								<li><a href="<?php echo $this->Html->url(array('action' => 'index')); ?>" class="btn btn-warning"><i class="fa fa-refresh"></i> リセット</a></li>
							</ul>
						</div>
					</div>
				</div>

				<?php echo $this->Form->end(); ?>

			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">

		<div class="panel panel-default">
			<div class="panel-heading">求人情報一覧</div>
			<div class="panel-body">

				<table class="table table-striped">
					<thead>
						<tr>
							<th>表示</th>
							<th><?php echo $this->Paginator->sort('facility_id', '施設情報'); ?></th>
							<th><?php echo $this->Paginator->sort('shikaku', '資格'); ?></th>
							<th><?php echo $this->Paginator->sort('shisetsukeitai_second_id', '部署'); ?></th>
							<th><?php echo $this->Paginator->sort('kinmu_keitai', '勤務形態'); ?></th>
							<th><?php echo $this->Paginator->sort('kyuyo', '給与（月給）'); ?></th>
							<th><?php echo $this->Paginator->sort('active', '公開'); ?></th>
							<th><?php echo $this->Paginator->sort('shintyaku_kyujin', '新着'); ?></th>
							<th><?php echo $this->Paginator->sort('ninki_kyujin', '人気'); ?></th>
							<th><?php echo $this->Paginator->sort('customerCount', '応募'); ?></th>
							<th><?php echo $this->Paginator->sort('modified', '更新日'); ?></th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($recruits as $i => $recruit) : ?>
						<?php
							$paging = $this->request->params['paging'][$model];
							$start = ($paging['page'] - 1) * $paging['limit'] + 1;
							$pageNum = $start + $i;
							$sort = isset($paging['options']['sort']) ? $paging['options']['sort'] : '';
							$viewLink = Hash::merge(
								array('action' => 'view'),
								$this->request->params['named'],
								array('page' => $pageNum),
								array('?' => $this->request->query)
							);
						?>
						<tr>
							<td><?php echo $this->Html->link('表示', $viewLink); ?></td>
							<td><?php echo $recruit['Facility']['shisetsumei']; ?></td>
							<td><?php echo $shikaku_options[$recruit[$model]['shikaku']]; ?></td>
							<td><?php echo $recruit['ShisetsukeitaiSecond']['name']; ?></td>
							<td><?php echo $kinmu_keitai_options[$recruit[$model]['kinmu_keitai']]; ?></td>
							<td><?php echo nl2br($recruit[$model]['kyuyo']); ?></td>
							<td><?php echo $recruit[$model]['active'] == 1 ? '<i class="fa fa-check"></i>' : ''; ?></td>
							<td><?php echo $recruit[$model]['shintyaku_kyujin'] == 1 ? '<i class="fa fa-check"></i>' : ''; ?></td>
							<td><?php echo $recruit[$model]['ninki_kyujin'] == 1 ? '<i class="fa fa-check"></i>' : ''; ?></td>
							<td><?php echo $recruit[$model]['customer_count']; ?></td>
							<td><?php echo date('Y-m-d', strtotime($recruit[$model]['modified'])); ?></td>
							<td>
								<?php echo $this->Form->postLink('削除', array('action' => 'delete', $recruit[$model]['id']), '', '本当に削除しますか？元には戻せません'); ?>
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
				<div class="text-center">
					<?php echo $this->Paginator->pagination(array('ul' => 'pagination')); ?>
				</div>

			</div>
		</div>

	</div>
</div>
