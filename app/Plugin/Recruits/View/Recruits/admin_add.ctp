<?php

	$shikaku_options = Configure::read('shikaku');
	/*
		20190527 sohnishi	
		職種追加
	*/
	$shokushu_options = Configure::read('shokushu');
	$kinmu_keitai_options = Configure::read('kinmu_keitai');
	$syoukyu_options = Configure::read('syoukyu');
	$syouyo_kaisu_options = Configure::read('syouyo_kaisu');
	$syakaihoken_options = Configure::read('syakaihoken');
	$show_options = Configure::read('recruit_show_options');
?>
<div class="row">
	<div class="col-xs-12 contents-actions">
		<ul class="nav nav-tabs">
			<li><?php echo $this->Html->link('求人情報検索', array('action' => 'index', '?' => $this->Session->read('recruitquerystring'))); ?></li>
			<li class="active"><a href="javascript:void(0);">求人情報を新規登録</a></li>
		</ul>
	</div>
</div>

<?php
	echo $this->Form->create($model, array(
		'inputDefaults' => array(
			'label' => false,
			'wrapInput' => false,
			'div' => false,
			'class' => 'form-control'
		),
	));
?>

<div class="row">
	<div class="col-xs-12">

		<div class="panel panel-default">
			<div class="panel-heading">求人情報の更新</div>
			<div class="panel-body">

				<div class="row">
					<div class="col-xs-6">
						<table class="table table-bordered">
							<tbody>
								<tr>
									<th><?php echo $this->Form->label('facility_id', '求人掲載施設'); ?></th>
									<td colspan="3">
										<?php echo $this->Form->input('facility_id'); ?>
										<span class="help-block">求人情報を掲載する施設を選択します</span>
									</td>
								</tr>
								<tr>
									<th><?php echo $this->Form->label('active', '掲載状態'); ?></th>
									<td colspan="3"><?php echo $this->Form->input('active', array('label' => '掲載はチェック', 'class' => false, 'default' => 0)); ?></td>
								</tr>
								<tr>
									<th><?php echo $this->Form->label('shintyaku_kyujin', '新着求人'); ?></th>
									<td colspan="3"><?php echo $this->Form->input('shintyaku_kyujin', array('label' => '新着はチェック', 'class' => false)); ?></td>
								</tr>
								<tr>
									<th><?php echo $this->Form->label('ninki_kyujin', '人気求人'); ?></th>
									<td colspan="3"><?php echo $this->Form->input('ninki_kyujin', array('label' => '人気はチェック', 'class' => false)); ?></td>
								</tr>
								<tr>
									<th><?php echo $this->Form->label('Kodawari', 'こだわりポイント'); ?></th>
									<td colspan="3"><?php echo $this->Form->input('Kodawari', array('type' => 'select', 'multiple' => 'checkbox', 'class' => 'checkbox-inline')); ?></td>
								</tr>
								<tr>
									<th><?php echo $this->Form->label('shikaku', '資格'); ?></th>
									<td colspan="3">
										<div class="row">
											<div class="col-xs-6">
												<?php echo $this->Form->input('shikaku', array('options' => $shikaku_options)); ?>
											</div>
										</div>
									</td>
								</tr>
								<!-- 
									20190527 sohnishi	
									職種追加
								-->
								<tr>
									<th><?php echo $this->Form->label('shokushu', '職種'); ?></th>
									<td colspan="3">
										<div class="row">
											<div class="col-xs-6">
												<?php echo $this->Form->input('shokushu', array('options' => $shokushu_options)); ?>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<th><?php echo $this->Form->label('shisetsukeitai_second_id', '部署'); ?></th>
									<td colspan="3">
										<div class="row">
											<div class="col-xs-6">
												<?php echo $this->Form->input('shisetsukeitai_second_id'); ?>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<th><?php echo $this->Form->label('kinmu_keitai', '勤務形態'); ?></th>
									<td colspan="3">
										<div class="row">
											<div class="col-xs-6">
												<?php echo $this->Form->input('kinmu_keitai', array('options' => $kinmu_keitai_options)); ?>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<th><?php echo $this->Form->label('kinmu_jikan', '勤務時間'); ?></th>
									<td colspan="3"><?php echo $this->Form->input('kinmu_jikan', array('type' => 'textarea', 'rows' => 2, 'style' => 'resize:vertical;')); ?></td>
								</tr>
								<tr>
									<th><?php echo $this->Form->label('shigoto', '仕事内容'); ?></th>
									<td colspan="3"><?php echo $this->Form->input('shigoto', array('rows' => 5, 'style' => 'resize:vertical;')); ?></td>
								</tr>
								<tr>
									<th><?php echo $this->Form->label('kyuyo', '給与（月給） <span class="req">*</span>'); ?></th>
									<td colspan="3"><?php echo $this->Form->input('kyuyo', array('type' => 'textarea', 'rows' => 2, 'style' => 'resize:vertical;')); ?></td>
								</tr>
								<tr>
									<th><?php echo $this->Form->label('Nensyu', '給与（年収）'); ?></th>
									<td colspan="3"><?php echo $this->Form->input('Nensyu', array('type' => 'select', 'multiple' => 'checkbox', 'class' => 'checkbox-inline')); ?></td>
								</tr>
								<tr>
									<th><?php echo $this->Form->label('yakin_kingaku', '夜勤手当'); ?></th>
									<td colspan="3"><?php echo $this->Form->input('yakin_kingaku', array('type' => 'textarea', 'rows' => 2, 'style' => 'resize:vertical;')); ?></td>
								</tr>
								<tr>
									<th><?php echo $this->Form->label('sonota_teate', 'その他手当て'); ?></th>
									<td colspan="3"><?php echo $this->Form->input('sonota_teate', array('rows' => 3)); ?></td>
								</tr>
								<tr>
									<th><?php echo $this->Form->label('Kyujitsu', '休日体制'); ?></th>
									<td colspan="3"><?php echo $this->Form->input('Kyujitsu', array('type' => 'select', 'multiple' => 'checkbox', 'class' => 'checkbox-inline')); ?></td>
								</tr>
								<tr>
									<th><?php echo $this->Form->label('syoukyu', '昇給'); ?></th>
									<td colspan="3">
										<div class="row">
											<div class="col-xs-4">
												<?php echo $this->Form->input('syoukyu', array('options' => $syoukyu_options)); ?>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<th><?php echo $this->Form->label('syouyo_kaisu', '賞与（回数/年）'); ?></th>
									<td colspan="3">
										<div class="row">
											<div class="col-xs-4">
												<?php echo $this->Form->input('syouyo_kaisu', array('options' => $syouyo_kaisu_options)); ?>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<th><?php echo $this->Form->label('syouyo_jisseki', '賞与（実績/年）'); ?></th>
									<td colspan="3">
										<div class="row">
											<div class="col-xs-4">
												<?php echo $this->Form->input('syouyo_jisseki', array('type' => 'text', 'beforeInput' => '<div class="input-group">', 'afterInput' => '<span class="input-group-addon">ヶ月</span></div>', 'maxlength' => array(4))); ?>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<th><?php echo $this->Form->label('nenkan_kyujitsu', '年間休日'); ?></th>
									<td colspan="3">
										<div class="row">
											<div class="col-xs-4">
												<?php echo $this->Form->input('nenkan_kyujitsu', array('type' => 'text', 'beforeInput' => '<div class="input-group">', 'afterInput' => '<span class="input-group-addon">日</span></div>', 'maxlength' => 3)); ?>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<th><?php echo $this->Form->label('yukyu', '有給'); ?></th>
									<td colspan="3">
										<?php echo $this->Form->input('yukyu'); ?>
									</td>
								</tr>
								<tr>
									<th><?php echo $this->Form->label('yukyu_syouka', '有給消化率'); ?></th>
									<td colspan="3">
										<div class="row">
											<div class="col-xs-4">
												<?php echo $this->Form->input('yukyu_syouka', array('type' => 'text', 'beforeInput' => '<div class="input-group">', 'afterInput' => '<span class="input-group-addon">%</span></div>', 'maxlength' => 3)); ?>
											</div>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>

					<div class="col-xs-6">
						<table class="table table-bordered">
							<tbody>
								<tr>
									<th><?php echo $this->Form->label('sankyu', '産休育休'); ?></th>
									<td>
										<div class="row">
											<div class="col-xs-4">
												<?php echo $this->Form->input('sankyu', array('type' => 'select', 'options' => array('0' => '実績なし', '1' => '実績あり'))); ?>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<th><?php echo $this->Form->label('sankyu_syousai', '産休育休詳細'); ?></th>
									<td><?php echo $this->Form->input('sankyu_syousai', array('rows' => 3, 'style' => 'resize:vertical;')); ?></td>
								</tr>
								<tr>
									<th><?php echo $this->Form->label('ryou', '寮'); ?></th>
									<td>
										<div class="row">
											<div class="col-xs-4">
												<?php echo $this->Form->input('ryou', array('type' => 'select', 'options' => array('0' => 'なし', '1' => 'あり'))); ?>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<th><?php echo $this->Form->label('ryou_syousai', '寮詳細'); ?></th>
									<td><?php echo $this->Form->input('ryou_syousai', array('rows' => 3, 'style' => 'resize:vertical;')); ?></td>
								</tr>
								<tr>
									<th><?php echo $this->Form->label('takujisyo', '託児所'); ?></th>
									<td>
										<div class="row">
											<div class="col-xs-4">
												<?php echo $this->Form->input('takujisyo', array('type' => 'select', 'options' => array('0' => 'なし', '1' => 'あり'))); ?>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<th><?php echo $this->Form->label('takujisyo_syousai', '託児所詳細'); ?></th>
									<td><?php echo $this->Form->input('takujisyo_syousai', array('rows' => 3, 'style' => 'resize:vertical;')); ?></td>
								</tr>
								<tr>
									<th><?php echo $this->Form->label('tukin', '車通勤'); ?></th>
									<td>
										<div class="row">
											<div class="col-xs-4">
												<?php echo $this->Form->input('tukin', array('type' => 'select', 'options' => array('0' => '不可', '1' => '可'))); ?>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<th><?php echo $this->Form->label('tukin_syousai', '車通勤詳細'); ?></th>
									<td><?php echo $this->Form->input('tukin_syousai', array('rows' => 3, 'style' => 'resize:vertical;')); ?></td>
								</tr>
								<tr>
									<th><?php echo $this->Form->label('taisyokukin', '退職金'); ?></th>
									<td>
										<div class="row">
											<div class="col-xs-4">
												<?php echo $this->Form->input('taisyokukin', array('type' => 'select', 'options' => array('0' => 'なし', '1' => 'あり'))); ?>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<th><?php echo $this->Form->label('taisyokukin_syousai', '退職金詳細'); ?></th>
									<td><?php echo $this->Form->input('taisyokukin_syousai', array('rows' => 3, 'style' => 'resize:vertical;')); ?></td>
								</tr>
								<tr>
									<th><?php echo $this->Form->label('syakaihoken', '社会保険'); ?></th>
									<td><?php echo $this->Form->input('syakaihoken', array('type' => 'select', 'options' => $syakaihoken_options, 'multiple' => 'checkbox', 'class' => 'checkbox-inline')); ?></td>
								</tr>
								<tr>
									<th><?php echo $this->Form->label('hukurikousei', 'その他福利厚生'); ?></th>
									<td><?php echo $this->Form->input('hukurikousei', array('rows' => 3, 'style' => 'resize:vertical;')); ?></td>
								</tr>
								<!-- 
									20190508 sohnishi
									求人予備項目を追加
									$index: 配列作成用のインデックス アソシエーションモデルのデータを同時に保存するために配列構造にする必要がある
								 -->
								 <?php
									$index = 0;
									foreach($recruitPreliminaryItems as $id => $recruitPreliminaryItem) {
								?>
										<tr>
											<th><?php echo $this->Form->label('RecruitPreliminaryItem', $recruitPreliminaryItem); ?></th>
											<?php echo $this->Form->input('RecruitPreliminaryItem.' . $index . '.recruit_preliminary_item_id', array('type' => 'hidden', 'value' => $id)); ?>
											<td><?php echo $this->Form->input('RecruitPreliminaryItem.' . $index . '.content', array('rows' => 3)); ?></td>
										</tr>
								<?php
									$index += 1;
									}
								?>
							</tbody>
						</table>
					</div>
				</div>

			</div>
		</div>

	</div>
</div>

<div class="row">
	<div class="col-xs-12">

		<div class="panel panel-default">
			<div class="panel-heading">掲載オプション</div>
			<div class="panel-body">

				<div class="row">
					<div class="form-group col-xs-12">
						<div class="form-inline">
							<?php echo $this->Form->input('RecruitShow.shikaku_chk', array('label' => $show_options['shikaku'], 'class' => false, 'default' => 1)); ?>
							<!-- 
								20190527 sohnishi	
								職種追加
							-->
							<?php echo $this->Form->input('RecruitShow.shokushu_chk', array('type' => 'checkbox', 'label' => $show_options['shokushu'], 'class' => false, 'default' => 1)); ?>
							<?php echo $this->Form->input('RecruitShow.busyo_chk', array('label' => $show_options['busyo'], 'class' => false, 'default' => 1)); ?>
							<?php echo $this->Form->input('RecruitShow.kinmu_keitai_chk', array('label' => $show_options['kinmu_keitai'], 'class' => false, 'default' => 1)); ?>
							<?php echo $this->Form->input('RecruitShow.kinmu_jikan_chk', array('label' => $show_options['kinmu_jikan'], 'class' => false, 'default' => 1)); ?>
							<?php echo $this->Form->input('RecruitShow.shigoto_chk', array('label' => $show_options['shigoto'], 'class' => false, 'default' => 1)); ?>
							<?php echo $this->Form->input('RecruitShow.kyuyo_chk', array('label' => $show_options['kyuyo'], 'class' => false, 'default' => 1)); ?>
							<?php echo $this->Form->input('RecruitShow.nensyu_chk', array('label' => $show_options['nensyu'], 'class' => false, 'default' => 1)); ?>
							<?php echo $this->Form->input('RecruitShow.yakin_chk', array('label' => $show_options['yakin'], 'class' => false, 'default' => 1)); ?>
							<?php echo $this->Form->input('RecruitShow.teate_chk', array('label' => $show_options['teate'], 'class' => false, 'default' => 1)); ?>
							<?php echo $this->Form->input('RecruitShow.syoukyu_chk', array('label' => $show_options['syoukyu'], 'class' => false, 'default' => 1)); ?>
							<?php echo $this->Form->input('RecruitShow.syouyo_kaisu_chk', array('label' => $show_options['syouyo_kaisu'], 'class' => false, 'default' => 1)); ?>
							<?php echo $this->Form->input('RecruitShow.syouyo_jisseki_chk', array('label' => $show_options['syouyo_jisseki'], 'class' => false, 'default' => 1)); ?>
							<?php echo $this->Form->input('RecruitShow.kyujitsu_chk', array('label' => $show_options['kyujitsu'], 'class' => false, 'default' => 1)); ?>
							<?php echo $this->Form->input('RecruitShow.nenkan_kyujitsu_chk', array('label' => $show_options['nenkan_kyujitsu'], 'class' => false, 'default' => 1)); ?>
							<?php echo $this->Form->input('RecruitShow.yukyu_chk', array('label' => $show_options['yukyu'], 'class' => false, 'default' => 1)); ?>
							<?php echo $this->Form->input('RecruitShow.yukyu_syouka_chk', array('label' => $show_options['yukyu_syouka'], 'class' => false, 'default' => 1)); ?>
							<?php echo $this->Form->input('RecruitShow.sankyu_chk', array('label' => $show_options['sankyu'], 'class' => false, 'default' => 1)); ?>
							<?php echo $this->Form->input('RecruitShow.sankyu_syousai_chk', array('label' => $show_options['sankyu_syousai'], 'class' => false, 'default' => 1)); ?>
							<?php echo $this->Form->input('RecruitShow.ryou_chk', array('label' => $show_options['ryou'], 'class' => false, 'default' => 1)); ?>
							<?php echo $this->Form->input('RecruitShow.ryou_syousai_chk', array('label' => $show_options['ryou_syousai'], 'class' => false, 'default' => 1)); ?>
							<?php echo $this->Form->input('RecruitShow.takujisyo_chk', array('label' => $show_options['takujisyo'], 'class' => false, 'default' => 1)); ?>
							<?php echo $this->Form->input('RecruitShow.takujisyo_syousai_chk', array('label' => $show_options['takujisyo_syousai'], 'class' => false, 'default' => 1)); ?>
							<?php echo $this->Form->input('RecruitShow.tukin_chk', array('label' => $show_options['tukin'], 'class' => false, 'default' => 1)); ?>
							<?php echo $this->Form->input('RecruitShow.tukin_syousai_chk', array('label' => $show_options['tukin_syousai'], 'class' => false, 'default' => 1)); ?>
							<?php echo $this->Form->input('RecruitShow.syakaihoken_chk', array('label' => $show_options['syakaihoken'], 'class' => false, 'default' => 1)); ?>
							<?php echo $this->Form->input('RecruitShow.taisyokukin_chk', array('label' => $show_options['taisyokukin'], 'class' => false, 'default' => 1)); ?>
							<?php echo $this->Form->input('RecruitShow.taisyokukin_syousai_chk', array('label' => $show_options['taisyokukin_syousai'], 'class' => false, 'default' => 1)); ?>
							<?php echo $this->Form->input('RecruitShow.hukurikousei_chk', array('label' => $show_options['hukurikousei'], 'class' => false, 'default' => 1)); ?>
							<!-- 
								20190513 sohnishi
								求人予備項目を追加
								$index: 配列作成用のインデックス アソシエーションモデルのデータを同時に保存するために配列構造にする必要がある
							-->
							<?php
								$index = 0;
								foreach($recruitPreliminaryItems as $id => $recruitPreliminaryItem) {
							?>
								<?php echo $this->Form->input('RecruitPreliminaryItem.' . $index . '.is_show', array('type' => 'checkbox', 'label' => $recruitPreliminaryItem, 'class' => false, 'default' => 0)); ?>
							<?php
									$index += 1;
								}
							?>
						</div>
						<span class="help-block">求人情報ごとに掲載の有無をチェックできます</span>
					</div>
				</div>

			</div>
		</div>

	</div>
</div>

<div class="row">
	<div class="col-xs-12">
		<?php echo $this->Form->submit('求人情報を登録する', array('class' => 'btn btn-primary')); ?>
	</div>
</div>

<?php echo $this->Form->end();