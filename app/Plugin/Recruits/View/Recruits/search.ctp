<?php
	echo $this->Html->script('addrajax/addrajax', array('inline' => false));

	$shikaku_options = Configure::read('shikaku');
	$kinmu_keitai_options = Configure::read('kinmu_keitai');

	// 最寄り路線・駅
	$datas = array(
		'lineEmptyText' => '最寄り路線を選択',
		'stationEmptyText' => '最寄り駅を選択',
		'prefId' => '#RecruitPrefCd',
		'lineId' => '#RecruitLineCd',
		'stationId' => '#RecruitStationCd'
	);
	echo $this->element('interface', $datas, array('plugin' => 'Ekidata'));
?>
<?php echo $this->Html->scriptStart(); ?>
$(function () {
	var dd = new ADDRAjax('data[Recruit][todoufuken]', 'data[Recruit][shikutyouson]');
	dd.JSONDATA = '<?php echo $this->Html->url('/js/addrajax/data'); ?>';
	dd.init();
});
<?php echo $this->Html->scriptEnd(); ?>

<?php
$this->start('page-top');
	$this->Html->addCrumb('検索');
	echo $this->Html->getCrumbList(array('lastClass' => 'active', 'class' => 'breadcrumb'), 'トップ');
$this->end();
?>



<h2 class="page-header"><?php echo $title_for_layout; ?></h2>

<div id="search">
	<?php
		echo $this->Form->create($model, array(
			'inputDefaults' => array(
				'label' => false,
				'inputWrap' => false,
				'div' => false,
				'class' => 'form-control'
			),
			'url' => array_merge(array('action' => 'results'), $this->params['pass'])
		));
	?>
	<div class="section">
		<h3 class="section-title">勤務地を選択する</h3>
		<div class="section-body">
			<div class="row">
				<div class="col-sm-4">
					<div class="form-group">
						<?php echo $this->Form->input('todoufuken', array('type' => 'select', 'options' => '', 'empty' => '都道府県を選択')); ?>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
						<?php echo $this->Form->input('shikutyouson', array('type' => 'select', 'options' => '', 'empty' => '市区町村を選択')); ?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="section">
		<h3 class="section-title">最寄り路線・駅を選択する</h3>
		<div class="section-body">
			<div class="row">
				<div class="col-sm-4">
					<div class="form-group">
						<?php echo $this->Form->input('pref_cd', array('type' => 'select', 'options' => $prefs, 'empty' => '都道府県を選択')); ?>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
						<?php echo $this->Form->input('line_cd', array('type' => 'select', 'options' => $lines, 'empty' => '最寄り路線を選択')); ?>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
						<?php echo $this->Form->input('station_cd', array('type' => 'select', 'options' => $stations, 'empty' => '最寄り駅を選択')); ?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="section">
		<h3 class="section-title">資格を選択する</h3>
		<div class="row">
			<div class="col-sm-12">
				<?php echo $this->Form->input('shikaku', array('type' => 'select', 'options' => $shikaku_options, 'multiple' => 'checkbox', 'class' => false, 'div' => 'search-check')); ?>
			</div>
		</div>
	</div>

	<div class="section">
		<h3 class="section-title">勤務形態を選択する</h3>
		<div class="row">
			<div class="col-sm-12">
				<?php echo $this->Form->input('kinmu_keitai', array('type' => 'select', 'options' => $kinmu_keitai_options, 'multiple' => 'checkbox', 'class' => false, 'div' => 'search-check')); ?>
			</div>
		</div>
	</div>

	<div class="section">
		<h3 class="section-title">施設を選択する</h3>
		<div class="row">
			<div class="col-sm-12">
				<?php echo $this->Form->input('shisetsukeitai_first', array('type' => 'select', 'multiple' => 'checkbox', 'class' => false, 'div' => 'search-check')); ?>
			</div>
		</div>
	</div>

	<div class="section">
		<h3 class="section-title">募集部署を選択する</h3>
		<div class="row">
			<div class="col-sm-12">
				<?php echo $this->Form->input('shisetsukeitai_second_id', array('type' => 'select', 'multiple' => 'checkbox', 'class' => false, 'div' => 'search-check')); ?>
			</div>
		</div>
	</div>

	<div class="section">
		<h3 class="section-title"><a href="#shinryoukamoku" data-toggle="collapse">診療科目を選択する <span class="fa fa-plus"></span></a></h3>
		<div id="shinryoukamoku" class="collapse">
			<div class="row">
				<div class="col-sm-12">
					<?php echo $this->Form->input('shinryoukamoku', array('type' => 'select', 'multiple' => 'checkbox', 'class' => false, 'div' => 'search-check')); ?>
				</div>
			</div>
		</div>
	</div>

	<div class="section">
		<h3 class="section-title"><a href="#kodawari" data-toggle="collapse">こだわった条件を選択する <span class="fa fa-plus"></span></a></h3>
		<div id="kodawari" class="collapse">
			<div class="row">
				<div class="col-sm-12">
					<?php echo $this->Form->input('kodawari', array('type' => 'select', 'multiple' => 'checkbox', 'class' => false, 'div' => 'search-check')); ?>
				</div>
			</div>
		</div>
	</div>

	<div class="section noborder">
		<?php echo $this->Form->submit('選択された条件で検索', array('class' => 'btn btn-primary btn-lg btn-search', 'div' => false)); ?>
	</div>
	<?php echo $this->Form->end(); ?>
</div>

<?php $this->start('page-bottom'); ?>

<?php echo $this->element('global_contact'); ?>

<?php $this->end(); ?>

<?php
$this->start('sidebox-pc');
echo $this->element('support');
echo $this->element('search_link');
echo $this->element('guide');
echo $this->element('blog');
echo $this->element('search_form');
echo $this->element('categories');
echo $this->element('sidebar_link');
$this->end();

$this->start('sidebox-sp');
$this->end();
