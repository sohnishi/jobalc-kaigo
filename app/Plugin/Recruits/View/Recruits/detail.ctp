<?php
    $shokushu_options = Configure::read('shokushu');
    $kinmu_keitai_options = Configure::read('kinmu_keitai');
    // 構造化データ用
    $employmentType = Configure::read('employmentType');
    $shikaku_options = Configure::read('shikaku');
    $todoufukens = Configure::read('todoufukens');
	$cities = Configure::read('cities');

    $shikakus = Configure::read('form_shikaku');
    $kibouworks = Configure::read('form_kibouwork');
    $kinmukaishijiki = Configure::read('form_kinmukaishijiki');

    $syoukyu_options = Configure::read('syoukyu');
	$syouyo_kaisu_options = Configure::read('syouyo_kaisu');
	$syakaihoken_options = Configure::read('syakaihoken');

?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="format-detection" content="telephone=no">
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
<title><?php echo $detail['Facility']['shisetsumei']; ?>の介護求人（求人No.<?php echo $detail['Recruit']['id']; ?>）【ジョブアルク介護】</title>
<link href="<?php echo $this->Html->url('/favicon.ico'); ?>" type="image/x-icon" rel="icon">
<link href="<?php echo $this->Html->url('/favicon.ico'); ?>" type="image/x-icon" rel="shortcut icon">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
<?php 
    echo $this->Html->css('base');
    echo $this->Html->css('modal');
    echo $this->Html->css('validationEngine.jquery'); 
?>
<?php echo $this->Html->script('jquery-1.11.2.min'); ?>
<?php echo $this->Html->script('jquery.cookie'); ?>
<?php echo $this->Html->script('bootstrap.min'); ?>
<?php echo $this->Html->script('addrajax/addrajax'); ?>
<?php echo $this->Html->script('jquery.steps.min'); ?>
<?php echo $this->Html->script('jquery.validationEngine-ja'); ?>
<?php echo $this->Html->script('jquery.validationEngine'); ?>
<?php echo $this->Html->script('jquery.autoKana'); ?>
<?php echo $this->Html->script('landing'); ?>


<!--[if lt IE 9]>
	<script type="text/javascript" src="theme/Jobalc/js/html5shiv.min.js"></script>
	<script type="text/javascript" src="theme/Jobalc/js/respond.min.js"></script>
	<script type="text/javascript">document.createElement('main');</script>
<![endif]-->


<script type="application/ld+json"> 
{
    "@context": "https://schema.org/",
    "@type": "JobPosting",
    "title": "<?php echo nl2br($shisetsukeitaiFirsts[$detail['Facility']['ShisetsukeitaiFirst'][0]['id']]); ?>の介護職/<?php if ($detail['RecruitShow']['shokushu_chk']) { echo $shokushu_options[$detail['Recruit']['shokushu']]; } ?>(<?php if ($detail['RecruitShow']['kinmu_keitai_chk']) { echo $kinmu_keitai_options[$detail['Recruit']['kinmu_keitai']]; } ?>)",
    "description": "
------------------------------------------------
■ ポイント
------------------------------------------------

<?php echo !empty($detail['Facility']['kanrenshisetsu']) ? nl2br($detail['Facility']['kanrenshisetsu']) : ''; ?>


------------------------------------------------
■ 募集職種
------------------------------------------------

<?php 
    if ($detail['RecruitShow']['shokushu_chk']) {
        echo $shokushu_options[$detail['Recruit']['shokushu']]; 
    }
?>


------------------------------------------------
■ 施設形態
------------------------------------------------

<?php echo nl2br($shisetsukeitaiFirsts[$detail['Facility']['ShisetsukeitaiFirst'][0]['id']]); ?>


------------------------------------------------
■ 勤務場所
------------------------------------------------

<?php echo $detail['Facility']['todoufuken']; ?><?php echo $detail['Facility']['shikutyouson']; ?><?php echo $detail['Facility']['banchi']; ?><?php echo $detail['Facility']['tatemono']; ?>


------------------------------------------------
■ 最寄駅
------------------------------------------------
<?php 
    $rosenekiIndex = 0;
    foreach ($detail['Facility']['Roseneki'] as $roseneki) : 
        // 2行以上の場合は改行
        if ($rosenekiIndex >= 1) {
            echo '<br>';
        }
?>  
<?php echo isset($roseneki['RailroadLine']['line_name']) ? $roseneki['RailroadLine']['line_name'] : ''; ?>
<?php echo isset($roseneki['RailroadStation']['station_name']) ? $roseneki['RailroadStation']['station_name'] . '駅' : ''; ?>
    <?php
        $rosenekiIndex += 1;
    ?>
<?php endforeach; ?>


------------------------------------------------
■ 応募資格
------------------------------------------------

<?php 
    if ($detail['RecruitShow']['shikaku_chk']) {
        echo $shikaku_options[$detail['Recruit']['shikaku']]; 
    }
?>


------------------------------------------------
■ 雇用形態
------------------------------------------------

<?php 
    if ($detail['RecruitShow']['kinmu_keitai_chk']) {
        echo $kinmu_keitai_options[$detail['Recruit']['kinmu_keitai']]; 
    }
?>


------------------------------------------------
■ 仕事内容
------------------------------------------------

<?php 
    if ($detail['RecruitShow']['shigoto_chk']) {
        echo $detail['Recruit']['shigoto']; 
    } 
?>


------------------------------------------------
■ 給与
------------------------------------------------

<?php 
    if ($detail['RecruitShow']['kyuyo_chk'] == 1) {
        echo $detail['Recruit']['kyuyo']; 
    }
?>


------------------------------------------------
■ 勤務時間
------------------------------------------------

<?php 
    if ($detail['RecruitShow']['kinmu_jikan_chk']) {
        echo $detail['Recruit']['kinmu_jikan']; 
    }
?>


------------------------------------------------
■ 年間休日
------------------------------------------------

<?php 
    if ($detail['RecruitShow']['nenkan_kyujitsu_chk']) { 
        echo $detail['Recruit']['nenkan_kyujitsu'];
    }
?>


------------------------------------------------
■ 休日詳細　※休日の値
------------------------------------------------

<?php 
    if ($detail['RecruitShow']['kyujitsu_chk']) { 
        foreach ($detail['Kyujitsu'] as $kyujitsu) {
            echo $kyujitsu['name'];
        }
    }
?>


------------------------------------------------
■ 福利厚生
------------------------------------------------

<?php 
    if ($detail['RecruitShow']['hukurikousei_chk']) {
        echo $detail['Recruit']['hukurikousei'];
    }
?>


------------------------------------------------
■ スタッフからの声
------------------------------------------------

<?php 
    if ($detail['RecruitPreliminaryItem'][2]['RecruitsRecruitPreliminaryItem']['is_show']) {
        echo $detail['RecruitPreliminaryItem'][2]['RecruitsRecruitPreliminaryItem']['content'];
    }
?>


------------------------------------------------
■ 応募方法
------------------------------------------------

【１】ジョブアルクの応募フォームよりエントリー
※この段階では応募ではないので安心ください。
↓
【２】ジョブアルクよりお電話にてエントリーの確認
↓
【３】応募・面接日程の調整をします
↓
【４】面接実施後に採用結果のご連絡をします
↓
【５】入職手続きを調整をします。

※応募から内定までは約1週間ほどになります。
※即日希望の方は【２】の際にお伝えください。
※在職中で内定後の退職調整の方も応募可能です。
",
    "datePosted": " <?php echo $detail['Recruit']['created'] ?>",
    "employmentType": "<?php if ($detail['RecruitShow']['kinmu_keitai_chk']) { echo $employmentType[$detail['Recruit']['kinmu_keitai']]; } ?>",
    "hiringOrganization": {
        "@type": "Organization",
        "name": "<?php echo $detail['Facility']['shisetsumei']; ?><?php if ($detail['Facility']['FacilityShow']['houjinmei_chk'] == 1) { echo empty($detail['Facility']['corporation']) ? '' : '/' . $detail['Facility']['corporation'] . '&nbsp;'; } ?>",
        "logo": "<?php echo FULL_BASE_URL . $this->Html->url('/img/logo_gfj.jpg'); ?>"
    },
    "jobLocation": {
        "@type": "Place",
        "address": {
            "@type": "PostalAddress",
            "addressRegion": "<?php echo $detail['Facility']['todoufuken']; ?>",
            "addressLocality": "<?php echo $detail['Facility']['shikutyouson']; ?>",
            "streetAddress": "<?php echo $detail['Facility']['banchi']; ?><?php echo $detail['Facility']['tatemono']; ?>",
            "addressCountry": "JP"
        }
    },
    "baseSalary": {
        "@type": "MonetaryAmount",
        "currency": "JPY",
        "value": {
            "@type": "QuantitativeValue",
            "value": 1300,
            "unitText": "HOUR"
        }
    }
}
</script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TMH9GK');</script>
<!-- End Google Tag Manager -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TMH9GK"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php 
    $favoritesArray = array();
    $favoritesString = $_COOKIE["job-alc-kaigo-favorites"];
    if ($favoritesString) {
        $revisedString = str_replace('[', '', $favoritesString);
        $revisedString = str_replace('"', '', $revisedString);
        $revisedString = str_replace(']', '', $revisedString);
        $favoritesArray = explode(",", $revisedString);
    }
?>
    <div id="body_wrapper">
        <div id="top_bar">
			<div class="container">
				<h1>介護の求人・転職なら【ジョブアルク介護】</h1>
			</div>
		</div>
		<header>
			<div class="container">
                <div class="header">
                    <div class="logo">
						<?php 
							echo $this->Html->link(
							$this->Html->image('logo.svg', array('alt' => 'ジョブアルク介護ロゴ')),
							'/',
							array('escape' => false)); 
						?>
                    </div>
                    <!--ハンバーガーメニュ-->
                    <!--参考　https://www.nxworld.net/tips/12-css-hamburger-menu-active-effect.html-->
                    <div class="header_tel pc">
                        <span class="header_tel" title="お電話でのお仕事相談・登録ができます。0120-932-929：【受付】9:30～20:00（土・日・祝も可能）">
								<?php echo $this->Html->image('top_tel.svg', array('alt' => 'お電話でのお仕事相談・登録ができます。0120-932-929：【受付】9:30～20:00（土・日・祝も可能）')); ?>
                        </span>
                    </div>
                    <div class="favlist pc">
                        <span class="favlist_all">
							<?php 
								echo $this->Html->link(
								'<count class="fav_count"><span>' . count($favoritesArray) . '</span></count>' . 
								$this->Html->image('favorites.svg', array('alt' => 'お気に入り')),
								'/favorites',
								array('escape' => false)); 
							?>
                        </span>
                    </div>
                    <div class="header_entry pc">
						<?php 
							echo $this->Html->link(
							$this->Html->image('btn001_pc.svg', array('alt' => '無料　アドバイザーに相談したい')),
							'/consultation-form/',
							array('escape' => false)); 
						?>
                    </div>
                    <div class="header_kensaku sp s_d_btn">						
						<a id='modal_search_sp_header'><img src="/cw/theme/Jobalc/img/kensaku.svg" alt="求人を検索する"></a>
                    </div>
                    <div id="nav-drawer">
                        <input id="nav-input" type="checkbox" class="nav-unshown">
                        <label id="nav-open" for="nav-input"><span></span></label>
                        <label class="nav-unshown" id="nav-close" for="nav-input"></label>
                        <div id="nav-content">
                        <div class="nav__title dfont">MENU<label class="close" for="nav-input"><span></span></label></div>
                        <ul class="menu_navi" id="menu_sp">
                            <li class="navi_sp"><a href="/cw/"><img src="/cw/theme/Jobalc/img/navi_sp01.svg" alt="ホーム"></a></li>
                            <li class="navi_sp"><a href="/cw/about/"><img src="/cw/theme/Jobalc/img/navi_sp02.svg" alt="はじめての方"></a></li>
                            <li class="navi_sp"><a href="/cw/user_voice/"><img src="/cw/theme/Jobalc/img/navi_sp03.svg" alt="ご利用者の声"></a></li>
                            <li class="navi_sp"><a href="/cw/service/"><img src="/cw/theme/Jobalc/img/navi_sp04.svg" alt="ご利用の流れ"></a></li>
                            <li class="navi_sp"><a href="/cw/guide/"><img src="/cw/theme/Jobalc/img/navi_sp05.svg" alt="転職ガイド"></a></li>
                        </ul>
                        <div class="nav__btn">
                            <p>＼具体的にお探しの方はこちらから／</p>
                            <a href="/cw/entry-form/"><img src="/cw/theme/Jobalc/img/navi_sp06.svg" alt="希望の仕事を紹介してもらう"></a>
                            <p>＼探したけどいい求人がなかった／</p>
                            <a href="/cw/private-form/"><img src="/cw/theme/Jobalc/img/navi_sp07.svg" alt="非公開求人"></a>
                        </div>
                        </div>
                    </div>
                    <div class="favlist_sp sp">
                        <span class="favlist_all">
							<?php 
								echo $this->Html->link(
                                '<count class="fav_count_sp"><span>' . count($favoritesArray) . '</span></count>' . 
								$this->Html->image('favorites_sp.svg', array('alt' => 'お気に入り')),
								'/favorites',
								array('escape' => false)); 
							?>
                        </span>
                    </div>
                </div>
			</div>
		</header>
		<nav class="nav pc">
            <div class="container">
            <!--ナビゲーション-->
            <!--参考　https://web-creators-tips.com/matome/css%E3%82%92%E4%BD%BF%E7%94%A8%E3%81%97%E3%81%9F%E7%94%BB%E5%83%8F%E3%81%AE%E3%83%AD%E3%83%BC%E3%83%AB%E3%82%AA%E3%83%BC%E3%83%90%E3%83%BC%E3%80%80%E6%A8%AA%E3%83%A1%E3%83%8B%E3%83%A5%E3%83%BC/-->
                <ul class="menu_navi" id="menu_pc">
					<li id="navi01">
						<?php
							echo $this->Html->link(
							'ジョブアルク介護',
							'/',
							array('escape' => false)); 
						?>
                    </li>
                    <li class="navi02"><a href="/cw/about/">はじめての方</a></li>
                    <li class="navi03" id="nav_modal_search_parent"><a id="modal_search">求人の検索</a></li>
                    <li class="navi04"><a href="/cw/user_voice/">ご利用者の声</a></li>
                    <li class="navi05"><a href="/cw/service/">ご利用の流れ</a></li>
                    <li class="navi06"><a href="/cw/guide/">転職ガイド</a></li>
                    <li class="navi07">
						<?php 
							echo $this->Html->link(
							'登録してみる',
							'/entry-form/',
							array('escape' => false)); 
						?>
					</li>
                </ul>
            </div>
        </nav>
        <div class="bread pc">
            <div class="container">
                <span>
                    <?php
                        echo $this->Html->link(
                            '<span>介護の求人TOP</span>',
                            '/',
                            array('escape' => false)
                        ); 
                        if (!empty($middle_breadcrumb)) {
                            foreach ($middle_breadcrumb as $name => $url) {
                                echo '&nbsp;&gt;&nbsp;';
                                echo $this->Html->link(
                                    '<span>' . $name . '</span>',
                                    $url,
                                    array('escape' => false)
                                );
                            }
                        }
                    ?>&nbsp;&gt;&nbsp;
                    <span><?php echo $detail['Facility']['shisetsumei']; ?></span>
                </span>
            </div>
        </div>
		<div id="main">
            <div class="container">
                <div class="row">
                    <main id="contents">
                        <article class="ichiran_item">
                            <div class="ichiran_top">
                                <div class="like_image">
									<?php 
										echo $this->Html->image(in_array($detail['Recruit']['id'], $favoritesArray) ? 'like.svg' : 'not_like.svg', array('class' => in_array($detail['Recruit']['id'], $favoritesArray) ? 'like_img' : 'not_like_img', 'id' => 'f_' . $detail['Recruit']['id'])); 
									?>
                                </div>
                                <h2>
									<span class="houjin">
										<?php
											if ($detail['Facility']['FacilityShow']['groupmei_chk'] == 1) {
												echo empty($detail['Facility']['group']) ? '' : $detail['Facility']['group'] . '&nbsp;';
											}
											if ($detail['Facility']['FacilityShow']['houjinmei_chk'] == 1) {
												echo empty($detail['Facility']['corporation']) ? '' : $detail['Facility']['corporation'] . '&nbsp;';
											}
										?>
									</span>
									<span class="shisetsu">
										<?php echo $detail['Facility']['shisetsumei']; ?>
									</span>
								</h2>
                            </div>
                            <div class="description">
                                <p class="catchphrase_text">
                                    <?php echo !empty($detail['Facility']['kanrenshisetsu']) ? nl2br($detail['Facility']['kanrenshisetsu']) : ''; ?>
                                </p>
                            </div>
                            <div class="h3_title">
                                <span class="icon001">
									<?php
										echo $this->Html->image('icon005.svg');
									?>
                                </span>
                                <h3 class="top_h3">特徴・おすすめ</h3>
                            </div>
                            <div class="kodawari">
                                <ul class="list_inline">
                                    <?php
                                        foreach ($detail['Kodawari'] as $kodawariItem) {
                                            echo '<li><span>' . $kodawariItem['name'] . '</span></li>';
                                        }
                                    ?>
                                </ul>
                            </div>
                            <div class="ichiran_middle">
                                <div class="ichiran_image">
									<?php
                                        $alt = '';
                                        if (!empty($detail['Facility']['corporation'])) {
                                            $alt = $detail['Facility']['corporation'] . ' ';
                                        }
                                        $alt = $alt . $detail['Facility']['shisetsumei'];

                                        $facilityImg = !empty($detail['Facility']['facility_image']) ? '/files/facility/facility_image/' . $detail['Facility']['facility_dir'] . '/' . $detail['Facility']['facility_image'] : '/img/default.svg';
										echo $this->Html->image($facilityImg, array('class' => 'img-responsive', 'alt' => $alt . 'の外観や内観になります。'));
									?>
                                </div>
                                <div class="ichiran_text">
                                    <div class="ichiran_text01">
                                        <p class="job_no_text">求人NO</p>
                                        <p class="job_no">
                                            <?php echo $detail['Recruit']['id']; ?>
                                        </p>
                                    </div>
                                    <div class="ichiran_text02">
										<?php
											echo $this->Html->image('ichiran_area.svg');
										?>
                                        <p class="ichiran_area">
                                            <?php echo $detail['Facility']['shikutyouson']; ?>
                                        </p>
                                    </div>

                                    <!-- 
                                        20190604 sohnisi
                                        掲載オプションが非掲載であれば、枠自体非表示とする
                                    -->
                                    <?php if ($detail['RecruitShow']['shokushu_chk']) { ?>
                                        <div class="ichiran_text02">
                                            <?php echo $this->Html->image('ichiran_license.svg'); ?>
                                            <p class="ichiran_license"><?php echo $shokushu_options[$detail['Recruit']['shokushu']]; ?></p>
                                        </div>
                                    <?php } ?>
                                    <!-- 
                                        20190604 sohnisi
                                        掲載オプションが非掲載であれば、枠自体非表示とする
                                    -->
                                    <?php if ($detail['RecruitShow']['kinmu_keitai_chk']) { ?>
                                    <div class="ichiran_text02">
                                        <?php echo $this->Html->image('ichiran_employ.svg'); ?>
                                        <p class="ichiran_employ"><?php echo $kinmu_keitai_options[$detail['Recruit']['kinmu_keitai']]; ?></p>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>

                            <!-- 
                                20190605 sohnisi
                                アドバイザーからポイント
                                掲載オプションが非掲載であれば、枠自体非表示とする
                            -->
                            <?php if ($detail['RecruitPreliminaryItem'][1]['RecruitsRecruitPreliminaryItem']['is_show']) { ?>

                                <div class="h3_title">
                                    <span class="icon001">
                                        <?php
                                            echo $this->Html->image('icon010.svg');
                                        ?>
                                    </span>
                                    <h3 class="top_h3">アドバイザーからポイント</h3>
                                </div>
                                <div class="catchphrase">
                                    <p class="catchphrase_text">
                                        <!-- 1 => 求人予備項目ID -->
                                        <?php echo $detail['RecruitPreliminaryItem'][1]['RecruitsRecruitPreliminaryItem']['content']; ?>
                                    </p>
                                </div>
                                
                            <?php } ?>


                            <span class="shosai_mcopy002 sp">※現在の募集状況や内容を詳しく知りたいetc...</span>
                            <div class="shosai_entry_btn sp">
                                <?php 
                                    echo $this->Html->link(
                                    '<span class="shosai_free_search">無料</span>
                                    <span class="shosai_btn_area">この求人について</span>
                                    <span class="shosai_btn_text">問い合わせる</span>' . $this->Html->image('yajirushi_w.svg'),
                                    '/entry-form/' . $detail['Recruit']['id'],
                                    array('escape' => false, 'class' => 'search_btn001'));
                                ?>
                            </div>
                            <span class="shosai_mcopy002 sp">※選考に進みませんので、お気軽にご相談ください</span>
                            <div class="h3_title">
                                <span class="icon001">
									<?php
										echo $this->Html->image('icon011.svg');
									?>
                                </span>
                                <h3 class="top_h3">募集の内容について</h3>
                            </div>
                            <div id="recruit_detail">
                                <table class="table-bordered">
                                    <tbody>
                                    <!-- 職種
                                    施設形態
                                    勤務先
                                    最寄駅 -->
                                    <!-- 応募資格
                                    雇用形態 -->
                                    <!-- 仕事内容　【改行可能性あり】
                                    ↑※構造化データにも反映されているか確認してください。 -->
                                    <!-- 給与（月給）
                                    給与（年収） -->
                                    <!-- 夜勤手当
                                    その他手当【改行可能性あり】 -->
                                    <!-- 勤務時間【改行可能性あり】
                                    年間休日
                                    休日 -->
                                    <!-- 昇給
                                    賞与（回数/年）
                                    賞与（実績/年） -->
                                    <!-- 産休育休
                                    産休育休詳細
                                    託児所
                                    託児所詳細
                                    寮
                                    寮詳細
                                    車通勤
                                    車通勤詳細
                                    退職金
                                    退職金詳細
                                    社会保険
                                    福利厚生【改行可能性あり】 -->
                                        <!-- 
                                            20190604 sohnisi
                                            掲載オプションが非掲載であれば、枠自体非表示とする
                                        -->
                                        <?php if ($detail['RecruitShow']['shokushu_chk']) { ?>
                                            <tr>
                                                <th>職種</th>
                                                <td>
                                                    <?php echo $shokushu_options[$detail['Recruit']['shokushu']]; ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        <tr>
                                            <th>施設形態</th>
                                            <td>
                                                <?php echo nl2br($shisetsukeitaiFirsts[$detail['Facility']['ShisetsukeitaiFirst'][0]['id']]); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>勤務先</th>
                                            <td>
                                                <?php echo $detail['Facility']['todoufuken']; ?>
                                                <?php echo $detail['Facility']['shikutyouson']; ?>
                                                <?php echo $detail['Facility']['banchi']; ?>
                                                <?php echo $detail['Facility']['tatemono']; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>最寄駅</th>
                                            <td>
                                                <?php 
                                                    $rosenekiIndex = 0;
                                                    foreach ($detail['Facility']['Roseneki'] as $roseneki) : 
                                                        // 2行以上の場合は改行
                                                        if ($rosenekiIndex >= 1) {
                                                            echo '<br>';
                                                        }
                                                ?>  
                                                    <?php echo isset($roseneki['RailroadLine']['line_name']) ? $roseneki['RailroadLine']['line_name'] : ''; ?>
                                                    <?php echo isset($roseneki['RailroadStation']['station_name']) ? $roseneki['RailroadStation']['station_name'] . '駅' : ''; ?>
                                                    <?php
                                                        $rosenekiIndex += 1;
                                                    ?>
                                                <?php endforeach; ?>
                                            </td>
                                        </tr>
                                        <!-- 
                                            20190604 sohnisi
                                            掲載オプションが非掲載であれば、枠自体非表示とする
                                        -->
                                        <?php if ($detail['RecruitShow']['shikaku_chk']) { ?>
                                            <tr>
                                                <th>応募資格</th>
                                                <td>
                                                    <?php echo $shikaku_options[$detail['Recruit']['shikaku']]; ?>
                                                </td>
                                            </tr>
                                        <?php } ?>

                                        <!-- 
                                            20190604 sohnisi
                                            掲載オプションが非掲載であれば、枠自体非表示とする
                                        -->
                                        <?php if ($detail['RecruitShow']['kinmu_keitai_chk']) { ?>
                                            <tr>
                                                <th>雇用形態</th>
                                                <td>
                                                    <?php echo $kinmu_keitai_options[$detail['Recruit']['kinmu_keitai']]; ?>
                                                </td>
                                            </tr>
                                        <?php } ?>

                                        <?php if ($detail['RecruitShow']['shigoto_chk']) { ?>
                                            <tr>
                                                <th>仕事内容</th>
                                                <td>
                                                    <?php echo nl2br($detail['Recruit']['shigoto']); ?>
                                                </td>
                                            </tr>
                                        <?php } ?>

                                        <!-- 
                                            20190604 sohnisi
                                            掲載オプションが非掲載であれば、枠自体非表示とする
                                        -->
                                        <?php if ($detail['RecruitShow']['kyuyo_chk'] == 1) { ?>

                                            <tr>
                                                <th>給与（月収）</th>
                                                <td>
                                                    <?php echo nl2br($detail['Recruit']['kyuyo']); ?>
                                                    <div class="kyuyo_entry_btn">
                                                        <?php 
                                                            echo $this->Html->link(
                                                            $this->Html->image('kyuyo_btn.svg'),
                                                            '/average-form/' . $detail['Recruit']['id'],
                                                            array('escape' => false, 'class' => 'kyuyo_btn001'));
                                                        ?>
                                                    </div>
                                                </td>
                                            </tr>

                                        <?php } ?>

                                        <?php if ($detail['RecruitShow']['nensyu_chk']) { ?>
                                            <tr>
                                                <th>給与（年収）</th>
                                                <td>
                                                    <?php foreach ($detail['Nensyu'] as $nensyu) : ?>
                                                        <span><?php echo $nensyu['name']; ?></span><br>
                                                    <?php endforeach; ?>
                                                </td>
                                            </tr>
                                        <?php } ?>

                                        <?php if ($detail['RecruitShow']['yakin_chk']) { ?>
                                            <tr>
                                                <th>夜勤手当</th>
                                                <td>
                                                    <?php echo nl2br($detail['Recruit']['yakin_kingaku']); ?>
                                                </td>
                                            </tr>
                                        <?php } ?>

                                        <?php if ($detail['RecruitShow']['teate_chk']) { ?>
                                            <tr>
                                                <th>その他手当</th>
                                                <td>
                                                    <?php echo nl2br($detail['Recruit']['sonota_teate']); ?>
                                                </td>
                                            </tr>
                                        <?php } ?>



                                        <!-- 
                                            20190604 sohnisi
                                            掲載オプションが非掲載であれば、枠自体非表示とする
                                        -->
                                        <?php if ($detail['RecruitShow']['kinmu_jikan_chk']) { ?>
                                            <tr>
                                                <th>勤務時間</th>
                                                <td>
                                                    <?php echo nl2br($detail['Recruit']['kinmu_jikan']); ?>
                                                </td>
                                            </tr>
                                        <?php } ?>

                                        <!-- 
                                            20190604 sohnisi
                                            掲載オプションが非掲載であれば、枠自体非表示とする
                                        -->
                                        <?php if ($detail['RecruitShow']['nenkan_kyujitsu_chk']) { ?>
                                            <tr>
                                                <th>年間休日</th>
                                                <td>
                                                    <?php echo $detail['Recruit']['nenkan_kyujitsu']; ?>日
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        
                                        <!-- 
                                            20190604 sohnisi
                                            掲載オプションが非掲載であれば、枠自体非表示とする
                                        -->
                                        <?php if ($detail['RecruitShow']['kyujitsu_chk']) { ?>
                                            <tr>
                                                <th>休日</th>
                                                <td>
                                                    <?php foreach ($detail['Kyujitsu'] as $kyujitsu) : ?>
                                                    <span><?php echo $kyujitsu['name'];?></span><br>
                                                    <?php endforeach; ?>
                                                </td>
                                            </tr>
                                        <?php } ?>

                                        <tr>
                                            <th>昇給</th>
                                            <td>
                                                <?php echo $syoukyu_options[$detail['Recruit']['syoukyu']]; ?>
                                            </td>
                                        </tr>

                                        <tr>
                                            <th>賞与（回数/年）</th>
                                            <td>
                                                <?php echo $syouyo_kaisu_options[$detail['Recruit']['syouyo_kaisu']]; ?>
                                            </td>
                                        </tr>

                                        <tr>
                                            <th>賞与（実績/年）</th>
                                            <td>
                                                <?php echo !empty($detail['Recruit']['syouyo_jisseki']) ? $detail['Recruit']['syouyo_jisseki'] . 'ヶ月' : ''; ?>
                                            </td>
                                        </tr>

                                        
                                        <?php if ($detail['RecruitShow']['sankyu_chk']) { ?>
                                            <tr>
                                                <th>産休育休</th>
                                                <td>
                                                    <?php echo ($detail['Recruit']['sankyu'] == 1) ? '実績あり' : '実績なし'; ?>
                                                </td>
                                            </tr>
                                        <?php } ?>

                                        <?php if ($detail['RecruitShow']['sankyu_syousai_chk']) { ?>
                                            <tr>
                                                <th>産休育休詳細</th>
                                                <td>
                                                    <?php echo nl2br($detail['Recruit']['sankyu_syousai']);; ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        
                                        <?php if ($detail['RecruitShow']['takujisyo_chk']) { ?>
                                            <tr>
                                                <th>託児所</th>
                                                <td>
                                                    <?php echo ($detail['Recruit']['takujisyo'] == 1) ? 'あり' : 'なし'; ?>
                                                </td>
                                            </tr>
                                        <?php } ?>

                                        <?php if ($detail['RecruitShow']['takujisyo_syousai_chk']) { ?>
                                            <tr>
                                                <th>託児所詳細</th>
                                                <td>
                                                    <?php echo nl2br($detail['Recruit']['takujisyo_syousai']); ?>
                                                </td>
                                            </tr>
                                        <?php } ?>

                                        <?php if ($detail['RecruitShow']['ryou_chk']) { ?>
                                            <tr>
                                                <th>寮</th>
                                                <td>
                                                    <?php echo ($detail['Recruit']['ryou'] == 1) ? 'あり' : 'なし'; ?>
                                                </td>
                                            </tr>
                                        <?php } ?>

                                        <?php if ($detail['RecruitShow']['ryou_syousai_chk']) { ?>
                                            <tr>
                                                <th>寮詳細</th>
                                                <td>
                                                    <?php echo nl2br($detail['Recruit']['ryou_syousai']); ?>
                                                </td>
                                            </tr>
                                        <?php } ?>

                                        <?php if ($detail['RecruitShow']['tukin_chk']) { ?>
                                            <tr>
                                                <th>車通勤</th>
                                                <td>
                                                    <?php echo ($detail['Recruit']['tukin'] == 1) ? '可' : '不可'; ?>
                                                </td>
                                            </tr>
                                        <?php } ?>

                                        <?php if ($detail['RecruitShow']['tukin_syousai_chk']) { ?>
                                            <tr>
                                                <th>車通勤詳細</th>
                                                <td>
                                                    <?php echo nl2br($detail['Recruit']['tukin_syousai']); ?>
                                                </td>
                                            </tr>
                                        <?php } ?>

                                        <?php if ($detail['RecruitShow']['taisyokukin_chk']) { ?>
                                            <tr>
                                                <th>退職金</th>
                                                <td>
                                                    <?php echo ($detail['Recruit']['taisyokukin'] == 1) ? 'あり' : 'なし'; ?>
                                                </td>
                                            </tr>
                                        <?php } ?>

                                        <?php if ($detail['RecruitShow']['taisyokukin_syousai_chk']) { ?>
                                            <tr>
                                                <th>退職金詳細</th>
                                                <td>
                                                    <?php echo nl2br($detail['Recruit']['taisyokukin_syousai']); ?>
                                                </td>
                                            </tr>
                                        <?php } ?>

                                        <?php if ($detail['RecruitShow']['syakaihoken_chk']) { ?>
                                            <tr>
                                                <th>社会保険</th>
                                                <td>
                                                    <?php foreach ($detail['Recruit']['syakaihoken'] as $syakaihoken) : ?>
                                                    <span><?php echo $syakaihoken_options[$syakaihoken]; ?></span><br>
                                                    <?php endforeach; ?>
                                                </td>
                                            </tr>
                                        <?php } ?>

                                        <!-- 
                                            20190604 sohnisi
                                            掲載オプションが非掲載であれば、枠自体非表示とする
                                        -->
                                        <?php if ($detail['RecruitShow']['hukurikousei_chk']) { ?>
                                            <tr>
                                                <th>福利厚生</th>
                                                <td>
                                                    <?php echo nl2br($detail['Recruit']['hukurikousei']); ?>
                                                </td>
                                            </tr>
                                        <?php } ?>

                                    </tbody>
                                </table>
                            </div>
                            <div class="h3_title">
                                <span class="icon001">
									<?php
										echo $this->Html->image('icon012.svg');
									?>
                                </span>
                                <h3 class="top_h3">非公開情報・口コミ</h3>
                            </div>
                            <div class="hikoukai_kuchikomi">
								<?php 
									echo $this->Html->link(
									$this->Html->image('hikoukai_kuchikomi.svg'),
									'/private-form/' . $detail['Recruit']['id'],
									array('escape' => false, 'class' => 'hikoukai_btn001'));
								?>
                            </div>
                            <div class="recruit_text">
                                <p>たくさんの方から口コミをお聞きして集めておりますので、現場の状況や環境、そして給与面など口コミの情報についてご登録いただいた求職者の方に限定でご紹介しております。</p>
                                <p>※口コミを伝えたい、情報をたくさん知っているという方でご協力いただける方もお待ちしております。お気軽にお問い合わせください。
                                </p>
                            </div>



                        　　<!-- 
                                20190605 sohnisi
                                スタッフから一言
                                掲載オプションが非掲載であれば、枠自体非表示とする
                            -->
                            <?php if ($detail['RecruitPreliminaryItem'][2]['RecruitsRecruitPreliminaryItem']['is_show']) { ?>

                                <div class="h3_title">
                                    <span class="icon001">
                                        <?php
                                            echo $this->Html->image('icon013.svg');
                                        ?>
                                    </span>
                                    <h3 class="top_h3">スタッフから一言</h3>
                                </div>
                                <div class="recruit_text">
                                    <!-- 2 => 求人予備項目ID -->
                                    <p>
                                        <?php echo nl2br($detail['RecruitPreliminaryItem'][2]['RecruitsRecruitPreliminaryItem']['content']); ?>
                                    </p>
                                </div>
                                
                            <?php } ?>


                            <!-- 
                                20190605 sohnisi
                                キャリアアドバイザーより
                                掲載オプションが非掲載であれば、枠自体非表示とする
                            -->
                            <?php if ($detail['RecruitPreliminaryItem'][3]['RecruitsRecruitPreliminaryItem']['is_show']) { ?>

                                <div class="h3_title">
                                    <span class="icon001">
                                        <?php
                                            echo $this->Html->image('icon014.svg');
                                        ?>
                                    </span>
                                    <h3 class="top_h3">キャリアアドバイザーより</h3>
                                </div>
                                <div class="recruit_text">
                                    <!-- 3 => 求人予備項目ID -->
                                    <p>
                                        <?php echo nl2br($detail['RecruitPreliminaryItem'][3]['RecruitsRecruitPreliminaryItem']['content']); ?>
                                    </p>
                                </div>

                            <?php } ?>


                            <!-- 
                                20190605 sohnisi
                                キャリアアドバイザーより
                                掲載オプションが非掲載であれば、枠自体非表示とする
                            -->
                            <?php if ($detail['RecruitPreliminaryItem'][4]['RecruitsRecruitPreliminaryItem']['is_show']) { ?>

                                <div class="h3_title">
                                    <span class="icon001">
                                        <?php
                                            echo $this->Html->image('icon015.svg');
                                        ?>
                                    </span>
                                    <h3 class="top_h3">応募方法</h3>
                                </div>
                                <div class="recruit_text">
                                    <!-- 3 => 求人予備項目ID -->
                                    <p>
                                        <?php echo nl2br($detail['RecruitPreliminaryItem'][4]['RecruitsRecruitPreliminaryItem']['content']); ?>
                                    </p>
                                </div>

                            <?php } ?>

                            <!--登録フォーム-->
                            <div id="form-wizard" class="section">
                                <h3 class="form_title">【無料】求人を問い合わせる</h3>
                                <div class="container">
                                    <div class="form-box">
                                        <div class="form-inner">
                                            <?php
                                                echo $this->Form->create('Customer', array(
                                                    'inputDefaults' => array('label' => false, 'div' => false),
                                                    'id' => 'landing-form',
                                                    'class' => 'form-horizontal',
                                                    'url' => '/entry-form/add'
                                                ));
                                            ?>
                                            <div id="landing-form-content">
                                                <h3>保有資格</h3>
                                                <section>
                                                    <h4 class="question">お持ちの資格を教えてください<br><span>(※複数選択可)</span></h4>
                                                    <div class="answer">
                                                        <div id="shikaku_error"></div>
                                                        <div class="row">
                                                            <?php foreach ($shikakus as $key => $value) : ?>
                                                                <div class="col-xs-6">
                                                                    <div class="checkbox">
                                                                        <input type="checkbox" name="data[Customer][shikaku][]" value="<?php echo $key; ?>" id="CustomerShikaku<?php echo $key; ?>" data-validation-engine="validate[minCheckbox[1]]" data-prompt-target="shikaku_error">
                                                                        <label for="CustomerShikaku<?php echo $key; ?>"><?php echo $value; ?></label>
                                                                    </div>
                                                                </div>
                                                            <?php endforeach; ?>
                                                        </div>
                                                    </div>
                                                </section>
                                                <h3>今の状況と働き方</h3>
                                                <section>
                                                    <h4 class="question">希望の働き方を選択してください<br><span>(※複数選択可)</span></h4>
                                                    <div class="answer mb30">
                                                        <div id="kibouwork_error"></div>
                                                        <div class="row">
                                                            <?php foreach ($kibouworks as $key => $value) : ?>
                                                                <div class="col-xs-6">
                                                                    <div class="checkbox">
                                                                        <input type="checkbox" name="data[Customer][kibouwork][]" value="<?php echo $key; ?>" id="CustomerKibouwork<?php echo $key; ?>" data-validation-engine="validate[minCheckbox[1]]" data-prompt-target="kibouwork_error">
                                                                        <label for="CustomerKibouwork<?php echo $key; ?>"><?php echo $value; ?></label>
                                                                    </div>
                                                                </div>
                                                            <?php endforeach; ?>
                                                        </div>
                                                    </div>
                                                    <h4 class="question">今の状況を教えてください</h4>
                                                    <div class="answer">
                                                        <div class="radio-style clearfix">
                                                            <?php foreach ($kinmukaishijiki as $key => $value) : ?>
                                                                <div class="radio">
                                                                    <input type="radio" name="data[Customer][kinmukaishijiki]" value="<?php echo $key; ?>" checked="checked" id="CustomerKinmukaishijiki<?php echo $key; ?>">
                                                                    <label for="CustomerKinmukaishijiki<?php echo $key; ?>"><?php echo $value; ?></label>
                                                                    <div class="check"></div>
                                                                </div>
                                                            <?php endforeach; ?>
                                                        </div>
                                                    </div>
                                                </section>
                                                <h3>住所</h3>
                                                <section>
                                                    <h4 class="question">郵便番号を入力、生まれ年を選択</h4>
                                                    <div class="answer">
                                                        <div class="form-group zip">
                                                            <?php echo $this->Form->label('zip', '郵便番号', array('class' => 'control-label col-xs-3')); ?>
                                                            <div class="col-xs-9">
                                                                <?php echo $this->Form->input('zip', array('type' => 'text', 'wrapInput' => false, 'class' => 'form-control', 'placeholder' => '例）000-0000', 'maxlength' => '128')); ?>
                                                                <div class="addon">※ハイフンなしOK</div>
                                                            </div>
                                                        </div>
                                                        <div id="birth_error"></div>
                                                        <div class="form-group birth">
                                                            <?php echo $this->Form->label('Customer.birth.year', '生まれ年', array('class' => 'control-label col-xs-3')); ?>
                                                            <div class="col-xs-9">
                                                                <!-- 
                                                                    20190610 sohnishi
                                                                    西暦/和暦表記に変更
                                                                -->
                                                                <?php echo $this->Form->year('Customer.birth.year', '1958', date('Y') - 18, array('class' => 'form-control', 'empty' => '西暦/和暦', 'data-validation-engine' => 'validate[required]', 'data-prompt-target' => 'birth_error')); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                                <h3>連絡先</h3>
                                                <section>
                                                    <h4 class="question">こちらが最後です☆<br>名前、連絡先を入力してください</h4>
                                                    <div class="answer">
                                                        <div class="form-group">
                                                            <?php echo $this->Form->label('name', 'お名前', array('class' => 'control-label col-xs-3')); ?>
                                                            <div class="col-xs-9">
                                                                <?php echo $this->Form->input('name', array('type' => 'text', 'class' => 'form-control', 'placeholder' => '例）大阪花子', 'data-validation-engine' => 'validate[required]')); ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <?php echo $this->Form->label('hurigana', 'フリガナ', array('class' => 'control-label col-xs-3')); ?>
                                                            <div class="col-xs-9">
                                                                <?php echo $this->Form->input('hurigana', array('type' => 'text', 'class' => 'form-control', 'placeholder' => '例）フクオカハナコ', 'data-validation-engine' => 'validate[required,custom[onlyKatakana]]')); ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <?php echo $this->Form->label('keitai_tel', '電話番号', array('class' => 'control-label col-xs-3')); ?>
                                                            <div class="col-xs-9">
                                                                <?php echo $this->Form->input('keitai_tel', array('type' => 'text', 'class' => 'form-control', 'placeholder' => '例）000-0000-0000', 'data-validation-engine' => 'validate[required,minSize[9],maxSize[13],custom[phone]]')); ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <?php echo $this->Form->label('email', 'メール', array('class' => 'control-label col-xs-3')); ?>
                                                            <div class="col-xs-9">
                                                                <?php echo $this->Form->input('email', array('type' => 'email', 'class' => 'form-control', 'placeholder' => '例）jobalc@info.jp', 'data-validation-engine' => 'validate[required, custom[email]]')); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                            </div>
                                            <div class="actions clearfix">
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <button type="button" id="stepsPrev" class="btn btn-block disabled" disabled="disabled"><span>もどる</span></button>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <button type="button" id="stepsNext" class="btn btn-block"><span>つぎへ</span></button>
                                                        <button type="button" id="stepsFinish" class="btn btn-block"><span>登録する</span></button>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php echo $this->Form->end(); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>

                        
                        

                        <!-- 
                            20190604 sohnisi
                            近隣の新着求人がない場合は、
                            枠ごと非表示とする
                        -->
                        <?php if (count($news) >= 1) {?>
                        <div class="new_job similar">
                            <div class="h2_title">
                                <span class="icon001">
									<?php
										echo $this->Html->image('icon003.svg');
									?>
                                </span>
                                <h2 class="top_h2">近隣の新着求人</h2>
                            </div>
                            <div class="new_job_area">
                                <ul class="horizontal-list">
									<?php 
										$newsForIndex = 0;
										foreach ($news as $key => $item) {
									?>
											<li class="item">
												<a href="">
												<div class="new_job_post01">
													<h3 class="new_job_title">
														<?php
															if ($item['Facility']['FacilityShow']['groupmei_chk'] == 1) {
																echo empty($item['Facility']['group']) ? '' : $item['Facility']['group'] . '&nbsp;';
															}
															if ($item['Facility']['FacilityShow']['houjinmei_chk'] == 1) {
																echo empty($item['Facility']['corporation']) ? '' : $item['Facility']['corporation'] . '&nbsp;';
															}
															echo $item['Facility']['shisetsumei']; 
														?>
													</h3>
													<div class="details">

														<!-- 
															20190527 sohnisi
															掲載オプションが非掲載であれば、画像と値を非表示とする
															高さを保持するため、要素は出力する
														-->

														<div class="new_job_btn01">
															<span class="icon002">
																<?php 
																	if ($item['RecruitShow']['kyuyo_chk'] == 1) {
																		echo $this->Html->image('newjob_en.svg'); 
																	}
																?>
															</span>
															<p class="new_job_value01">
																<?php 
																	if ($item['RecruitShow']['kyuyo_chk'] == 1) {
																		echo nl2br($item['Recruit']['kyuyo']); 
																	}
																?>
															</p>
														</div>



														
														<div class="new_job_btn02">
															<span class="icon003">
																<?php 
																	echo $this->Html->image('newjob_map.svg'); 
																?>
															</span>
															<p class="new_job_value02">
																<?php echo $item['Facility']['shikutyouson']; ?>
															</p>
														</div>

														
														<!-- 
															20190527 sohnisi
															掲載オプションが非掲載であれば、枠自体非表示とする
														-->
														<?php if ($item['RecruitShow']['shokushu_chk']) { ?>

															<div class="new_job_btn02">
																<span class="icon003">
																	<?php 
																		echo $this->Html->image('newjob_shikaku.svg'); 
																	?>
																</span>
																<p class="new_job_value02">
																	<?php echo $shokushu_options[$item['Recruit']['shokushu']]; ?>
																</p>
															</div>

														<?php } ?>

														
                                                        <!-- 
															20190527 sohnisi
															掲載オプションが非掲載であれば、枠自体非表示とする
														-->
														<?php if ($item['RecruitShow']['kinmu_keitai_chk']) { ?>

															<div class="new_job_btn02">
																<span class="icon003">
																	<?php 
																		echo $this->Html->image('newjob_koyo.svg'); 
																	?>
																</span>
																<p class="new_job_value02">
																	<?php echo $kinmu_keitai_options[$item['Recruit']['kinmu_keitai']]; ?>
																</p>
															</div>
															
														<?php } ?>

													</div>
												</div>
												</a>
											</li>

									<?php
											$newsForIndex+=1;
										}
									?>
                                    
                                </ul>
                            </div>
                        </div>
                        <?php } ?>

                        <div class="new_job similar">
                            <div class="h2_title">
                                <span class="icon001">
									<?php
										echo $this->Html->image('icon018.svg');
									?>
                                </span>
                                <h2 class="top_h2">近隣の関連リンク</h2>
                            </div>
                            <div>
                                <h3 class="area_grp">都道府県</h3>
                                <ul class="list_inline area_ken_shi">
                                    <li>
                                        <?php 
                                            echo $this->Html->link(
                                            '<span>' .
                                            $detail['Facility']['todoufuken'] . 
                                            '</span>',
                                            '/area/pref' . array_search($detail['Facility']['todoufuken'], $todoufukens),
                                            array('escape' => false));
                                        ?>
                                    </li>
                                </ul>
                                
                                
                                <!-- 
                                    20190604 sohnisi
                                    市区町村がNULLの場合、見出しから非表示とする
                                    都道府県はNOTNULL
                                -->
                                <?php if ($detail['Facility']['shikutyouson']) { ?>
                                    <h3 class="area_grp">市区町村</h3>
                                    <ul class="list_inline area_ken_shi">
                                        <li>
                                                <?php 
                                                    echo $this->Html->link(
                                                    '<span>' .
                                                    $detail['Facility']['shikutyouson'] . 
                                                    '</span>',
                                                    '/recruits/results/todoufuken:' . $detail['Facility']['todoufuken'] . '/shikutyouson:' . $detail['Facility']['shikutyouson'],
                                                    array('escape' => false));
                                                ?>
                                        </li>
                                    </ul>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="private">
                            <div class="private_area">
                                <div class="h2_title">
                                    <span class="icon001">
										<?php
											echo $this->Html->image('icon007.svg');
										?>
                                    </span>
                                    <h2 class="top_h2">いい求人が見つからなかった方</h2>
                                </div>
                                <div class="private_text">
                                    <p class="widget02_text">公開している求人だけではない？</p>
                                    <h4 class="widget03_text">非公開を希望されるケースは少なくありません。</h4>
                                    <p class="widget04_text">現在、働いている方への影響を避けたいなど、管理職の求人など、さまざま事情により公開を避けたい場合もあります。非公開求人として管理しているので、希望の条件などをお伝えし、相談してみましょう。</p>
                                </div>
                                <div class="private_btn">
                                    <a href="/cw/private-form/">
                                        <span>
											<?php
												echo $this->Html->image('private.svg', array('alt' =>'登録者限定 非公開求人 ※求人数や募集期間に限りがございます。'));
											?>
										</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </main>
                    <aside id="sidebar" class="pc">
						<div class="sidebox hidden-xs">
							<div class="support_widget">
                                <h4>
									<?php 
                                        echo $this->Html->link(
                                        $this->Html->image('touroku.svg', array('alt' => '登録 希望の条件のお仕事を紹介してもらう。')),
                                        '/entry-form/',
                                        array('escape' => false));
                                    ?>
                                </h4>
                            </div>
                            <div class="support_widget">
                                <h4>
									<?php 
                                        echo $this->Html->link(
                                        $this->Html->image('start.svg', array('alt' => 'はじめての方はこちらから')),
                                        '/about',
                                        array('escape' => false));
                                    ?>
                                </h4>
                            </div>
                            <div class="list_widget">
                                <h4>施設の種類から探す</h4>
                                <ul>
									
									<?php 
										$shisetsukeitaiFirstsForIndex = 0;
										foreach ($shisetsukeitaiFirsts as $key => $value) {
											if ($shisetsukeitaiFirstsForIndex == 0) {
												echo '<li class="li_fst">';
											} else {
												echo '<li>';
											}
									?>
											
												<?php 
													echo $this->Html->link(
														$value,
														'/facility/' . $key,
														array('escape' => false)
													); 
													echo $this->Html->image('yajirushi.svg'); 
												?>
											</li>
									<?php
											$shisetsukeitaiFirstsForIndex+=1;
										}
									?>

                                </ul>
                            </div>
                            <div class="list_widget">
                                <h4>職種から探す</h4>
                                <ul>
                                    <?php 
										$shokushuOptionsForIndex = 0;
										foreach ($shokushu_options as $key => $value) {
											if ($shokushuOptionsForIndex == 0) {
												echo '<li class="li_fst">';
											} else {
												echo '<li>';
											}
									?>
											
												<?php 
													echo $this->Html->link(
														$value,
														'/job/' . $key,
														array('escape' => false)
													); 
													echo $this->Html->image('yajirushi.svg'); 
												?>
											</li>
									<?php
											$shokushuOptionsForIndex+=1;
										}
									?>
                                </ul>
                            </div>
                            <div class="list_widget">
                                <h4>勤務形態から探す</h4>
                                <ul>

									<?php 
										$kinmuKeitaiOptionsForIndex = 0;
										foreach ($kinmu_keitai_options as $key => $value) {
											if ($kinmuKeitaiOptionsForIndex == 0) {
												echo '<li class="li_fst">';
											} else {
												echo '<li>';
											}
									?>
											
												<?php 
													echo $this->Html->link(
														$value,
														'/employment/' . $key,
														array('escape' => false)
													); 
													echo $this->Html->image('yajirushi.svg'); 
												?>
											</li>
									<?php
											$kinmuKeitaiOptionsForIndex+=1;
										}
									?>
                                    
                                </ul>
                            </div>
                            <div class="list_widget">
                                <h4>資格から探す</h4>
                                <ul>
                                    <?php 
										$shikakuOptionsForIndex = 0;
										foreach ($shikaku_options as $key => $value) {
											if ($shikakuOptionsForIndex == 0) {
												echo '<li class="li_fst">';
											} else {
												echo '<li>';
											}
									?>
											
												<?php 
													echo $this->Html->link(
														$value,
														'/qualification/' . $key,
														array('escape' => false)
													); 
													echo $this->Html->image('yajirushi.svg'); 
												?>
											</li>
									<?php
											$shikakuOptionsForIndex+=1;
										}
									?>
                                </ul>
                            </div>
                        </div>
                    </aside>
                </div>
            </div>
            <div class="bread sp">
				<div class="container">
                    <span>

                        <?php
                            echo $this->Html->link(
                                '<span>介護の求人TOP</span>',
                                '/',
                                array('escape' => false)
                            ); 
                            if (!empty($middle_breadcrumb)) {
                                foreach ($middle_breadcrumb as $name => $url) {
                                    echo '&nbsp;&gt;&nbsp;';
                                    echo $this->Html->link(
                                        '<span>' . $name . '</span>',
                                        $url,
                                        array('escape' => false)
                                    );
                                }
                            }
                        ?>
                        <span><?php echo $detail['Facility']['shisetsumei']; ?></span>
                    </span>
				</div>
			</div>
            <footer class="footer_sp sp">
            <div class="container">
				<div class="row">
                    <div class="footer">
                        <div class="footer_Menus">
                            <ul class="footer_menu">
                                <li class="footerMenu_list">
									<?php 
                                        echo $this->Html->link(
										$this->Html->image('1.svg', array('alt' => '無料登録')),
                                        '/entry-form/',
                                        array('escape' => false));
                                    ?>
								</li>
                                <li class="footerMenu_list">
									<?php 
										echo $this->Html->link(
										$this->Html->image('2.svg', array('alt' => '個人情報')),
										'/privacy',
										array('escape' => false)); 
									?>
								</li>
                                <li class="footerMenu_list">
									<?php 
										echo $this->Html->link(
										$this->Html->image('3.svg', array('alt' => '会社概要')),
										'/corporation',
										array('escape' => false)); 
									?>
								</li>
								<li class="footerMenu_list" id="modal_search_footer_parent_sp">
                                    <a class='modal_search_footer'><?php echo $this->Html->image('4.svg', array('alt' => '求人検索')) ?></a>
								</li>
                                <li class="footerMenu_list_last">
									<?php 
										echo $this->Html->link(
										$this->Html->image('5.svg', array('alt' => '利用規約')),
										'/terms',
										array('escape' => false)); 
									?>
								</li>
                            </ul>
                        </div>
                    </div>
				</div>
				<div class="footer-bottom text-center">
					<address class="address"> Copyright © 2019 ALC Co.Ltd All Rights Reserved.</address>
				</div>
			</div>
		</footer>
        <footer class="footer_pc pc">
            <div class="container">
				<div class="row">
                    <div class="footer">
                        <div class="footer_Menus">
                            <ul class="footer_menu">
                                <li class="footerMenu_title">メニュー</li>
                                <li class="footerMenu_list">
								<a href="">ホーム</a></li>
                                <li class="footerMenu_list"><a href="/cw/about/">はじめの方</a></li>
                                <li class="footerMenu_list" id="modal_search_footer_parent"><a class='modal_search_footer'>求人検索</a></li>
                                <li class="footerMenu_list"><a href="/cw/user_voice/">ご利用者の声</a></li>
                                <li class="footerMenu_list"><a href="/cw/service/">ご利用の流れ</a></li>
                            </ul>
                            <ul class="footer_menu">
                                <li class="footerMenu_title">転職コラム</li>
                                <li class="footerMenu_list"><a href="/">転職コラムTOP</a></li>
                                <li class="footerMenu_list"><a href="/">求人探しのポイント</a></li>
                                <li class="footerMenu_list"><a href="/">履歴書の書き方</a></li>
                                <li class="footerMenu_list"><a href="/">面接の対策</a></li>
                                <li class="footerMenu_list"><a href="/">入職への心構え</a></li>
                            </ul>
                            <ul class="footer_menu">
                                <li class="footerMenu_title">運用情報</li>
                                <li class="footerMenu_list"><a href="/cw/corporation/">会社概要</a></li>
                                <li class="footerMenu_list"><a href="/cw/terms/">利用規約</a></li>
                                <li class="footerMenu_list"><a href="/cw/privacy/">プライバシーポリシー</a></li>
                            </ul>
                            <ul class="footer_menu">
                                <li class="footerMenu_title">アルクの運営サイト</li>
                                <li class="footerMenu_list"><a href="/ns/">ジョブアルク看護師</a></li>
                                <li class="footerMenu_list"><a href="/fm/">ジョブアルク薬剤師</a></li>
                                <li class="footerMenu_list"><a href="/rh/">ジョブアルクPOS</a></li>
                            </ul>
                        </div>
                    </div>
					<div class="footer_tel">
						<div class="information">
							<?php 
								echo $this->Html->image('top_tel.svg', array('alt' => 'お電話でのお仕事相談・登録ができます。0120-932-929：【受付】9:30～20:00（土・日・祝も可能）')); 
							?>
                        </div>
                    </div>
					<div class="footer_btn">
						<div class="footer_entry">
							<?php 
								echo $this->Html->link(
								$this->Html->image('btn001_pc.svg', array('alt' => '無料　アドバイザーに相談したい')),
								'/consultation-form/',
								array('escape' => false)); 
							?>
                        </div>
                    </div>
				</div>
				<div class="footer-bottom text-center">
					<div>厚生大臣許可 27-ュ-201641</div>
					<address> Copyright © 2019 ALC Co.Ltd All Rights Reserved.</address>
				</div>
			</div>
		</footer>
        </div>
    </div>


    <!-- 検索モーダル -->

	<div class="modal_content">
        <div class="modal_window">
			<!-- <form action="/r/search.php" method="post" class="modal modal-foot is-none" data-modal-name="search" id="sform"> -->
			<?php
				echo $this->Form->create($model, array(
					'inputDefaults' => array(
						'label' => false,
						'inputWrap' => false,
						'div' => false,
					),
					'class' => 'form-control modal modal-foot is-none',
					'data-modal-name' => 'search',
					'id' => 'sform',
					// 'url' => '/recruits/results/'
					'url' => array_merge(array('action' => 'results'), $this->params['pass'])
				));
			?>
			<!-- <input type="hidden" name="formredirect" value="1"> -->
			<div class="modal_close_wrapper">
              <div class="modal_close js-modal" data-target-modal="search">
                <i class="fas fa-times close_icon"></i>
			  </div>
			</div>
              <div class="modal_inner">
                <div class="modal_head">
                  <h1 class="modal_headTit">求人検索</h1>
                  <p class="modal_headLink"><a id="restbtn">条件をクリア</a></p>
                  <!-- <p class="modal_headLink"><a href="/search-list/">履歴から検索</a></p> -->
                </div>
                <div class="modal_body">
                  <h2 class="ttl-m" id="area_sentaku">エリアを選択</h2>

                  <div class="l-grid l-grid-margin-xs-3 l-grid-margin-md-5 u-mb-xs-15">
                    <div class="l-grid_col col-xs-4 col-md-2">
                      <label class="input-radio js-tab" data-tab-group="area" data-tab-parent-name="1">
                        <input type="radio" id="select_area" checked="">
                        <span class="input-radioInner" id = "chiiki_select">
                          <span class="input-radioIcon"></span>
                          <span class="input-radioTxt">地域</span>
                        </span>
                      </label>
                    </div>
                    <div class="l-grid_col col-xs-5 col-md-2">
                      <label class="input-radio js-tab" data-tab-group="area" data-tab-parent-name="2">
                        <input type="radio" id="select_ln">
                        <span class="input-radioInner" id = "rosen_select">
                          <span class="input-radioIcon"></span>
                          <span class="input-radioTxt">路線・駅</span>
                        </span>
                      </label>
                    </div>
                  </div>

                  <div class="" data-tab-group="area" data-tab-child-name="1" id="chiiki_div">
                    <div class="l-grid u-mb-xs-30">

                      <!-- 地域 選択時に表示 -->
                      <div class="l-grid_col col-xs-12 col-md-4 u-mb-xs-10">
                        <label class="select js-selectToggle" data-target-toggle="chiiki_region" id="chiiki_region">
							<label for="pr" class="chdown"><i class="fas fa-angle-down chdown_icon"></i></label>
							<?php echo $this->Form->select('todoufuken', null, array('div' => false, 'empty' => '都道府県を選択', 'class' => 'reset prlist areapr')); ?>
                        </label>
                      </div>
                      <!-- /地域 選択時に表示 -->

                      <!-- 地域 → 都道府県 選択時に表示 -->
                      <div class="l-grid_col col-xs-12 col-md-4" data-toggle-name="chiiki_region">
                        <div class="js-checkBoxToggle" data-target-toggle="shikuchoson">
                            <label class="select js-selectToggle" data-target-toggle="chiiki_region">
							<label for="pr" class="chdown"><i class="fas fa-angle-down chdown_icon"></i></label>
							<?php echo $this->Form->select('shikutyouson', null, array('div' => false, 'empty' => '市区町村を選択', 'class' => 'reset prlist areapr')); ?>
                              <!-- <select name="pr" class="reset prlist areapr">
                                <option value="">市区町村を選択</option>
                              </select> -->
                            </label>
                        </div>
                      </div>
                      <!-- /地域 → 都道府県 選択時に表示 -->

                    </div>
                  </div>


                  <div class=" u-d-n" data-tab-group="area" data-tab-child-name="2" id="rosen_div">
                    <div class="l-grid u-mb-xs-30">
                      <!-- 路線 選択時に表示 -->

                      <div class="l-grid_col col-xs-12 col-md-4 u-mb-xs-10">
                        <label class="select js-selectToggle" data-target-toggle="rosen_region" id="rosen_region">
							<label for="pr" class="chdown"><i class="fas fa-angle-down chdown_icon"></i></label>
							<?php echo $this->Form->select('pref_cd', $prefs, array('div' => false, 'empty' => '都道府県を選択', 'class' => 'reset prlist areapr')); ?>
                        </label>
                      </div>
                      <!-- /路線 選択時に表示 -->

                      <!-- 路線 → 都道府県 選択時に表示 -->
                      <div class="l-grid_col col-xs-12 col-md-4 lnclick" data-toggle-name="rosen_region">
                        <div class="js-checkBoxToggle" data-target-toggle="rosen">
                            <label class="select js-selectToggle select_mbt" data-target-toggle="rosen_region">
							<label for="pr" class="chdown"><i class="fas fa-angle-down chdown_icon"></i></label>
							<?php echo $this->Form->select('line_cd', $lines, array('div' => false, 'empty' => '最寄り路線を選択', 'class' => 'reset prlist areapr')); ?>
                              <!-- <select name="pr" class="reset prlist lnpr">
                                <option value="">路線を選択</option>
							 </select> -->
						</label>
                        </div>
                      </div>
                      <!-- /路線 → 都道府県 選択時に表示 -->

                      <!-- 路線 → 都道府県 → 路線 選択時に表示 -->
                      <div class="l-grid_col col-xs-12 col-md-4 lnclick" data-toggle-name="rosen_region">
                        <div class="js-checkBoxToggle" data-target-toggle="rosen">
                            <label class="select js-selectToggle" data-target-toggle="rosen_region">
							<label for="pr" class="chdown"><i class="fas fa-angle-down chdown_icon"></i></label>
							<?php echo $this->Form->select('station_cd', $stations, array('div' => false, 'empty' => '最寄り駅を選択', 'class' => 'reset prlist areapr')); ?>
                              <!-- <select name="pr" class="reset prlist lnpr">
                                <option value="">駅を選択</option>
							  </select> -->
						</label>
                        </div>
                      </div>
                      <!-- 路線 → 都道府県 → 路線 選択時に表示 -->

                    </div>
				  </div>
				  


				  <h2 class="ttl-m">職種を選択<span class="ttl-m_sub">（複数選択可）</span></h2>
                  <div class="l-grid l-grid-margin-xs-2 l-grid-margin-md-5 u-mb-xs-25">

                    <?php 
                        foreach ($shokushu_options as $key => $value) {
                    ?>
                            
                            <div class="l-grid_col col-xs-6 col-md-2 u-mb-xs-5 u-mb-md-10">
                            <label class="input-checkbox">
                                <?php
                                    echo $this->Form->input('Recruit.shokushu.' . $key, array('type' => 'checkbox', 'class' => 'reset', 'value' => $key, 'hiddenField' => false));
                                ?>
                                <span class="input-checkboxInner">
                                    <span class="input-checkboxIcon"></span>
                                    <span class="input-checkboxTxt"><?php echo $value; ?></span>
                                </span>
                            </label>
                            </div>
                    <?php
                        }
                    ?>

                  </div>


                  <h2 class="ttl-m">勤務形態を選択<span class="ttl-m_sub">（複数選択可）</span></h2>
                  <div class="l-grid l-grid-margin-xs-2 l-grid-margin-md-5 u-mb-xs-25">

                    <?php 
                        foreach ($kinmu_keitai_options as $key => $value) {
                    ?>
                            
                            <div class="l-grid_col col-xs-6 col-md-2 u-mb-xs-5 u-mb-md-10">
                            <label class="input-checkbox">
                                <?php
                                    echo $this->Form->input('Recruit.kinmu_keitai.' . $key, array('type' => 'checkbox', 'class' => 'reset', 'value' => $key, 'hiddenField' => false));
                                ?>
                                <span class="input-checkboxInner">
                                    <span class="input-checkboxIcon"></span>
                                    <span class="input-checkboxTxt"><?php echo $value; ?></span>
                                </span>
                            </label>
                            </div>
                    <?php
                        }
                    ?>

				  </div>
				  

                  <h2 class="ttl-m" id="shikaku_sentaku">資格を選択<span class="ttl-m_sub">（複数選択可）</span></h2>
                  <div class="l-grid l-grid-margin-xs-2 l-grid-margin-md-5 u-mb-xs-25">

                  	<?php 
                        foreach ($shikaku_options as $key => $value) {
                    ?>
                            
                            <div class="l-grid_col col-xs-6 col-md-2 u-mb-xs-5 u-mb-md-10">
                            <label class="input-checkbox">
                                <?php 
									echo $this->Form->input('Recruit.shikaku.' . $key, array('type' => 'checkbox', 'class' => 'reset', 'value' => $key, 'hiddenField' => false));
                                ?>
                                <span class="input-checkboxInner">
                                    <span class="input-checkboxIcon"></span>
                                    <span class="input-checkboxTxt"><?php echo $value; ?></span>
                                </span>
                            </label>
                            </div>
                    <?php
                        }
                    ?>

                    
                  </div>

                  <h2 class="ttl-m" id="shisetsu_sentaku">施設業態を選択<span class="ttl-m_sub">（複数選択可）</span></h2>
                  <div class="l-grid l-grid-margin-xs-2 l-grid-margin-md-5 u-mb-xs-25">
                    

                    <?php 
                        foreach ($shisetsukeitaiFirsts as $key => $value) {
                    ?>
                            <div class="l-grid_col col-xs-6 col-md-2 u-mb-xs-5 u-mb-md-10">
                                <label class="input-checkbox">
                                    <?php 
										echo $this->Form->input('Recruit.shisetsukeitai_first.' . $key, array('type' => 'checkbox', 'class' => 'reset', 'value' => $key, 'hiddenField' => false));
                                    ?>
                                    <span class="input-checkboxInner">
                                        <span class="input-checkboxIcon"></span>
                                        <span class="input-checkboxTxt"><?php echo $value; ?></span>
                                    </span>
                                </label>
                            </div>
                    <?php
                        }
                    ?>
                  
                  </div>

                  <h2 class="ttl-m" id="jouken_sentaku">こだわり条件を選択<span class="ttl-m_sub">（複数選択可）</span></h2>
                  <div class="l-grid l-grid-margin-xs-2 l-grid-margin-md-5 u-mb-xs-25">

                    <?php 
                        foreach ($kodawaris as $key => $value) {
                    ?>
                            <div class="l-grid_col col-xs-6 col-md-2 u-mb-xs-5 u-mb-md-10">
                                <label class="input-checkbox">
                                    <?php 
										echo $this->Form->input('Recruit.kodawari.' . $key, array('type' => 'checkbox', 'class' => 'reset', 'value' => $key, 'hiddenField' => false));
                                    ?>
                                    <span class="input-checkboxInner">
                                        <span class="input-checkboxIcon"></span>
                                        <span class="input-checkboxTxt"><?php echo $value; ?></span>
                                    </span>
                                </label>
                            </div>
                    <?php
                        }
                    ?>
                  </div>

                  <h2 class="ttl-m">キーワードを入力</h2>
                  <div class="l-grid">
                    <div class="l-grid_col col-xs-12 col-md-6">
                        <div>
                            <?php echo $this->Form->input('keyword', array('type' => 'text', 'class' => 'input-txt resettext', 'placeholder' => 'ここにキーワードを入力')); ?>
                        </div>
                      <p class="u-fz-xs-12rem">例：東京駅、世田谷、◯◯◯老人ホーム</p>
                    </div>
                  </div>
                </div>
              </div>

              <div class="modal_foot">
                <div class="l-grid l-grid-ali-xs-c l-grid-juc-md-c">
                  <div class="l-grid_col col-xs-7 col-md-3">
                    <p class="modal_footResult"><span class="modal_footResultTxt" style="font-size: inherit" id="gai">該当求人数</span><span class="js-count" id="real_time_count"><?php echo $activeCount; ?></span><span style="font-size: inherit" id="ken">件</span></p>
                  </div>
                  <div class="l-grid_col col-xs-5 col-md-3">


                    <!-- desabledパターン is-disabled -->
                    <p class="modal_footSubmit">
					
                        <button type="button" class="btn-m btn-search2" onclick="search_before()">
                            <span class="btn_inner">
                                <div class="absearch">
									<?php
										echo $this->Html->image('icon002.svg', array('alt' => 'ジョブアルク介護ロゴ', 'class' => 'search_icon', 'style'=>'width: 40px'));
									?>
                                        <p>検索する<p>
                                </div>
                            </span>
                        </button>
                    </p>
                  </div>
                </div>
			  </div>
			  
			  <?php echo $this->Form->end(); ?>

        </div>
    </div>

<!-- 検索モーダル -->

<?php echo $this->Html->scriptStart(); ?>

// 検索フォーム - 条件クリア押下時にセットする掲載求人数
var activeCount = '<?php echo $activeCount; ?>';
// 条件をクリア押下フラグ
var isClear;

$(document).ready(function () {

// お気に入りに登録していない求人の気になるボタンをタップした時
$(document).on("click", '.not_like_img', function () {
        
    var _this = $(this);
    if (_this.attr('id')) {
        $.ajax({
            type: 'POST',
            url: '<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'updateFavorites')); ?>',
            data: {
                    'rec_id':_this.attr('id').substr(2),
                },
            success: function (data, textStatus, jpXHR) {
                _this.attr('class', 'like_img');
                _this.attr('src', '/cw/theme/Jobalc/img/like.svg');
            },
            error: function (jpXHR, textStatus, errorThrown) {
                careerMessage('通信に失敗しました' + errorThrown, false);
            }
        });
    
        // cookie有効化           	
        //$.cookie.json = true;
        // cookie読み込み
        //var myFavorites = $.cookie("job-alc-kaigo-favorites");
        //var addFavorites = [];
        //if (myFavorites) {
            // 既存のお気に入りが存在する場合
            //addFavorites = myFavorites;
            // 重複しなければ追加を行う
            //if (!IsArrayExists(addFavorites, $(this).attr('id').substr(2))) {
                // 先頭に追加(追加順に表示させたいため)
                //addFavorites.unshift($(this).attr('id').substr(2));
            //}

        //} else {
            // 既存のお気に入りが存在しない場合
            // 先頭に追加(追加順に表示させたいため)
            //addFavorites.unshift($(this).attr('id').substr(2));
        //}
        // cookie保存(1000日間保存)
        //$.cookie('job-alc-kaigo-favorites', addFavorites, { expires: 1000 , path: "/" });
        //$(this).attr('class', 'like_img');
        //$(this).attr('src', '/cw/theme/Jobalc/img/like.svg');
    }
});

// お気に入りに登録済み求人の気になるボタンをタップした時
$(document).on("click", '.like_img', function () {
    var _this = $(this);
    if (_this.attr('id')) {

        $.ajax({
            type: 'POST',
            url: '<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'deleteFavorite')); ?>',
            data: {
                    'rec_id':_this.attr('id').substr(2),
                },
            success: function (data, textStatus, jpXHR) {
                _this.attr('class', 'not_like_img');
                _this.attr('src', '/cw/theme/Jobalc/img/not_like.svg');
            },
            error: function (jpXHR, textStatus, errorThrown) {
                careerMessage('通信に失敗しました' + errorThrown, false);
            }
        });

        // cookie有効化           	
        //$.cookie.json = true;
        // cookie読み込み
        //var myFavorites = $.cookie("job-alc-kaigo-favorites");
        //var addFavorites = [];
        //if (myFavorites) {
            // 既存のお気に入りが存在する場合
            //addFavorites = myFavorites;
            //var target = $(this).attr('id').substr(2);
            //要素を削除する
            //addFavorites.some(function(v, i){
                //if (v==target) addFavorites.splice(i,1);    
            //});
            // cookie更新
            //$.cookie('job-alc-kaigo-favorites', addFavorites, { expires: 1000 , path: "/" });
            //$(this).attr('class', 'not_like_img');
            //$(this).attr('src', '/cw/theme/Jobalc/img/not_like.svg');

        //}

    }
});

 
// 配列に特定の値が存在するか検索するメソッド
function IsArrayExists(array, value) {
	// 配列の最後までループ
	for (var i =0, len = array.length; i < len; i++) {
		if (value == array[i]) {
		// 存在したらtrueを返す
		return true;
		}
	}
	// 存在しない場合falseを返す
	return false;
}



// results.ctpでしか使用しないが、同じaddrajax.jsを参照しているため、変数のみ宣言しておく
var RecruitShikutyousonVal;
	
$(function () {
	var dd = new ADDRAjax('data[Recruit][todoufuken]', 'data[Recruit][shikutyouson]');
	dd.JSONDATA = '<?php echo $this->Html->url('/theme/Jobalc/js/addrajax/data'); ?>';
	dd.init();
});


$("#sform").change(function(){
    // 条件をクリア押下時は件数を取得しない
    if (!isClear) {
        $.ajax({
            type: 'POST',
            url: '<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'searchCount')); ?>',
            data: $(this).serializeArray(),
            success: function (data, textStatus, jpXHR) {
                // 該当求人数更新
                $('#real_time_count').html(data);
            },
            error: function (jpXHR, textStatus, errorThrown) {
                
            }
        });
    } 
});

});

function search_before() {
	
	var pref_val = $('#RecruitTodoufuken').val();
	var city_val = $('#RecruitShikutyouson').val();
	var form_values = $("#sform").serializeArray();

	var excludeds = new Array('_method', 'data[_Token][key]', 'data[Recruit][todoufuken]', 'data[Recruit][shikutyouson]', 'data[_Token][fields]');
	var submit_flg = 0;
	form_values.forEach((item) => {
		if (item['value'] != null && item['value'] != '') {
			if (excludeds.indexOf(item['name']) == -1){
				// 存在しない
				submit_flg = 1;
			}
		}
	});
	
	if (submit_flg == 0) {
		if (pref_val != null && pref_val != '') {
			var redirect_url = '/cw/area/pref';
			var prefs = <?php echo json_encode($todoufukens); ?>;
			const pref_key = Object.keys(prefs).reduce( (r, key) => { 
				return prefs[key] === pref_val ? key : r 
			}, null);
			redirect_url = redirect_url + pref_key + '/';

			if (city_val != null && city_val != '') {
				var cities = <?php echo json_encode($cities); ?>;
				const city_key = Object.keys(cities).reduce( (r, key) => { 
					return cities[key] === city_val ? key : r 
				}, null);
				redirect_url = redirect_url + 'city' + city_key + '/';
			}

			location.href = redirect_url;
			
		} else {
			$("#sform").submit();
		}
	} else {
		$("#sform").submit();
	}
	
}

<?php echo $this->Html->scriptEnd(); ?>

<?php echo $this->Html->script('modal'); ?>

</body>
</html>