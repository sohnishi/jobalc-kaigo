<?php
    $shokushu_options = Configure::read('shokushu');
    $kinmu_keitai_options = Configure::read('kinmu_keitai');
    $shikaku_options = Configure::read('shikaku');
    $todoufukens = Configure::read('todoufukens');
    $cities = Configure::read('cities');
    
    if (count($paginatorUrlArray) != 0) {
        $this->Paginator->options(
            array(
                'url' => $paginatorUrlArray
            )
        );
    } else {
        $this->Paginator->options(
            array(
                'url' => array('plugin'=>'recruits', 'controller'=>'results', 'action'=>$search_param)
            )
        );
    }
?>
<!DOCTYPE html>
<html lang="ja">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="format-detection" content="telephone=no">
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
<meta property="og:title" content="<?php echo $search_pref; ?>の様々な介護求人の一覧【ジョブアルク介護】<?php echo $this->Paginator->counter('{:page}'); ?>ページ目/<?php echo $this->Paginator->counter('{:pages}'); ?>ページ" />
<meta property="og:type" content="article" />
<meta property="og:url" content="<?php echo Router::reverse($this->request, true); ?>" />
<meta property="og:image" content="https://job-alc.com/cw/img/ogp.png" />
<meta property="og:site_name" content="介護の求人・転職なら【ジョブアルク介護】" />
<?php 
if($url) {
    // 都道府県別HEAD
    if ($url == 'area' && !$isCitySearch) {
        echo $this->element('area/head/' . $id);
    }
    // 政令都市別HEAD
    if ($url == 'area' && $isCitySearch) {
        echo $this->element('city/head/' . $city);
    }

    // 勤務形態別HEAD
    if ($url == 'employment') {
        echo $this->element('employment/head/' . $id);
    }

    // 職種別HEAD
    if ($url == 'job') {
        echo $this->element('job/head/' . $id);
    }

    // 施設形態別HEAD
    if ($url == 'facility') {
        echo $this->element('facility/head/' . $id);
    }

    // 資格別HEAD
    if ($url == 'qualification') {
        echo $this->element('qualification/head/' . $id);
    }
} else {
    echo '<meta property="og:description" content="ジョブアルク介護は、介護福祉士、介護職、ケアマネの求人・転職サービスです。 |  ' . $search_pref . 'の様々な介護求人の一覧を掲載しています。現在のページは' . $this->Paginator->counter('{:pages}'); ?>ページ中の<?php echo $this->Paginator->counter('{:page}') . 'ページ目を表示しています。【ジョブアルク介護】" />';
}
    
?>


<title><?php echo $search_pref; ?>の様々な介護求人の一覧【ジョブアルク介護】<?php echo $this->Paginator->counter('{:page}'); ?>ページ目/<?php echo $this->Paginator->counter('{:pages}'); ?>ページ</title>
<link href="<?php echo $this->Html->url('/favicon.ico'); ?>" type="image/x-icon" rel="icon">
<link href="<?php echo $this->Html->url('/favicon.ico'); ?>" type="image/x-icon" rel="shortcut icon">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
<?php 
    echo $this->Html->css('base');
    echo $this->Html->css('modal');
?>
<?php echo $this->Html->script('jquery-1.11.2.min'); ?>
<?php echo $this->Html->script('jquery.cookie'); ?>
<?php echo $this->Html->script('bootstrap.min'); ?>
<!--[if lt IE 9]>
    <script type="text/javascript" src="theme/Jobalc/js/html5shiv.min.js"></script>
    <script type="text/javascript" src="theme/Jobalc/js/respond.min.js"></script>
    <script type="text/javascript">document.createElement('main');</script>
<![endif]-->

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TMH9GK');</script>
<!-- End Google Tag Manager -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TMH9GK"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php 
    $favoritesArray = array();
    $favoritesString = $_COOKIE["job-alc-kaigo-favorites"];
    if ($favoritesString) {
        $revisedString = str_replace('[', '', $favoritesString);
        $revisedString = str_replace('"', '', $revisedString);
        $revisedString = str_replace(']', '', $revisedString);
        $favoritesArray = explode(",", $revisedString);
    }
?>
    <div id="body_wrapper">
        <div id="top_bar">
			<div class="container">
				<h1><?php echo $search_pref; ?>の介護求人の一覧【ジョブアルク介護】</h1>
			</div>
		</div>
		<header>
			<div class="container">
                <div class="header">
                    <div class="logo">
						<?php 
							echo $this->Html->link(
							$this->Html->image('logo.svg', array('alt' => 'ジョブアルク介護ロゴ')),
							'/',
							array('escape' => false)); 
						?>
                    </div>
                    <!--ハンバーガーメニュ-->
                    <!--参考　https://www.nxworld.net/tips/12-css-hamburger-menu-active-effect.html-->
                    <div class="header_tel pc">
                        <span class="header_tel" title="お電話でのお仕事相談・登録ができます。0120-932-929：【受付】9:30～20:00（土・日・祝も可能）">
								<?php echo $this->Html->image('top_tel.svg', array('alt' => 'お電話でのお仕事相談・登録ができます。0120-932-929：【受付】9:30～20:00（土・日・祝も可能）')); ?>
                        </span>
                    </div>
                    <div class="favlist pc">
                        <span class="favlist_all">
							<?php 
								echo $this->Html->link(
								'<count class="fav_count"><span>' . count($favoritesArray) . '</span></count>' . 
								$this->Html->image('favorites.svg', array('alt' => 'お気に入り')),
								'/favorites',
								array('escape' => false)); 
							?>
                        </span>
                    </div>
                    <div class="header_entry pc">
						<?php 
							echo $this->Html->link(
							$this->Html->image('btn001_pc.svg', array('alt' => '無料　アドバイザーに相談したい')),
							'/consultation-form/',
							array('escape' => false)); 
						?>
                    </div>
                    <div class="header_kensaku sp s_d_btn">						
						<a id='modal_search_sp_header'><img src="/cw/theme/Jobalc/img/kensaku.svg" alt="求人を検索する"></a>
                    </div>
                    <div id="nav-drawer">
                        <input id="nav-input" type="checkbox" class="nav-unshown">
                        <label id="nav-open" for="nav-input"><span></span></label>
                        <label class="nav-unshown" id="nav-close" for="nav-input"></label>
                        <div id="nav-content">
                        <div class="nav__title dfont">MENU<label class="close" for="nav-input"><span></span></label></div>
                        <ul class="menu_navi" id="menu_sp">
                            <li class="navi_sp"><a href="/cw/"><img src="/cw/theme/Jobalc/img/navi_sp01.svg" alt="ホーム"></a></li>
                            <li class="navi_sp"><a href="/cw/about/"><img src="/cw/theme/Jobalc/img/navi_sp02.svg" alt="はじめての方"></a></li>
                            <li class="navi_sp"><a href="/cw/user_voice/"><img src="/cw/theme/Jobalc/img/navi_sp03.svg" alt="ご利用者の声"></a></li>
                            <li class="navi_sp"><a href="/cw/service/"><img src="/cw/theme/Jobalc/img/navi_sp04.svg" alt="ご利用の流れ"></a></li>
                            <li class="navi_sp"><a href="/cw/guide/"><img src="/cw/theme/Jobalc/img/navi_sp05.svg" alt="転職ガイド"></a></li>
                        </ul>
                        <div class="nav__btn">
                            <p>＼具体的にお探しの方はこちらから／</p>
                            <a href="/cw/entry-form/"><img src="/cw/theme/Jobalc/img/navi_sp06.svg" alt="希望の仕事を紹介してもらう"></a>
                            <p>＼探したけどいい求人がなかった／</p>
                            <a href="/cw/private-form/"><img src="/cw/theme/Jobalc/img/navi_sp07.svg" alt="非公開求人"></a>
                        </div>
                        </div>
                    </div>
                    <div class="favlist_sp sp">
                        <span class="favlist_all">
							<?php 
								echo $this->Html->link(
                                '<count class="fav_count_sp"><span>' . count($favoritesArray) . '</span></count>' . 
								$this->Html->image('favorites_sp.svg', array('alt' => 'お気に入り')),
								'/favorites',
								array('escape' => false)); 
							?>
                        </span>
                    </div>
                </div>
			</div>
		</header>
		<nav class="nav pc">
            <div class="container">
            <!--ナビゲーション-->
            <!--参考　https://web-creators-tips.com/matome/css%E3%82%92%E4%BD%BF%E7%94%A8%E3%81%97%E3%81%9F%E7%94%BB%E5%83%8F%E3%81%AE%E3%83%AD%E3%83%BC%E3%83%AB%E3%82%AA%E3%83%BC%E3%83%90%E3%83%BC%E3%80%80%E6%A8%AA%E3%83%A1%E3%83%8B%E3%83%A5%E3%83%BC/-->
                <ul class="menu_navi" id="menu_pc">
					<li id="navi01">
						<?php
							echo $this->Html->link(
							'ジョブアルク介護',
							'/',
							array('escape' => false)); 
						?>
                    </li>
                    <li class="navi02"><a href="/cw/about/">はじめての方</a></li>
                    <li class="navi03" id="nav_modal_search_parent"><a id="modal_search">求人の検索</a></li>
                    <li class="navi04"><a href="/cw/user_voice/">ご利用者の声</a></li>
                    <li class="navi05"><a href="/cw/service/">ご利用の流れ</a></li>
                    <li class="navi06"><a href="/cw/guide/">転職ガイド</a></li>
                    <li class="navi07">
						<?php 
							echo $this->Html->link(
							'登録してみる',
							'/entry-form/',
							array('escape' => false)); 
						?>
					</li>
                </ul>
            </div>
        </nav>
        <div class="bread pc">
            <div class="container">
                <?php //echo $this->Html->getCrumbs(); ?>
                <div class="container">
                    <span>
                        <?php
                            echo $this->Html->link(
                                '<span>介護の求人TOP</span>',
                                '/',
                                array('escape' => false)
                            );
                            if (!empty($middle_breadcrumb)) {
                                foreach ($middle_breadcrumb as $name => $url) {
                                    echo '&nbsp;&gt;&nbsp;';
                                    echo $this->Html->link(
                                        '<span>' . $name . '</span>',
                                        $url,
                                        array('escape' => false)
                                    );
                                }
                            }
                        ?>&nbsp;&gt;&nbsp;
                        <span><?php echo $search_pref; ?>の介護職求人</span>
                    </span>
                </div>
            </div>
        </div>
        <div class="masthead">
            <div class="container">
                <div class="ichiran_top_image pc">
                    <div class="kyujinsu">
                        <p class="kyujinsu_area"><span><?php echo $search_pref; ?></span>の公開求人数</p>
                        <p class="kyujinsu_atai"><?php echo $this->Paginator->counter('{:count}'); ?><span>件</span></p>
                        <p class="kyujinsu_text">非公開の求人数は含みません</p>
                    </div>
                    <div class="shibori_text">
                        <p>細かく絞り込みたい方は下記よりお進みくさだい。</p>
                        <ul>
                            <li class="s_d_btn_pc shibori_001">
                                <?php 
                                    echo $this->Html->link(
                                    $this->Html->image('shibori_001.svg', array('alt' => 'エリアを絞り込む')),
                                    '#area_sentaku',
                                    array('escape' => false, 'id' => 'modal_area'));
                                ?>
                            </li>
                            <li class="s_d_btn_pc shibori_002">
                                <?php 
                                    echo $this->Html->link(
                                    $this->Html->image('shibori_002.svg', array('alt' => '資格を絞り込む')),
                                    '#shikaku_sentaku',
                                    array('escape' => false, 'id' => 'modal_shikaku'));
                                ?>
                            </li>
                            <li class="s_d_btn_pc shibori_003">
                                <?php 
                                    echo $this->Html->link(
                                    $this->Html->image('shibori_003.svg', array('alt' => '施設を絞り込む')),
                                    '#shisetsu_sentaku',
                                    array('escape' => false, 'id' => 'modal_shisetsu'));
                                ?>
                            </li>
                            <li class="s_d_btn_pc shibori_004">
                                <?php 
                                    echo $this->Html->link(
                                    $this->Html->image('shibori_004.svg', array('alt' => '条件を絞り込む')),
                                    '#jouken_sentaku',
                                    array('escape' => false, 'id' => 'modal_jouken'));
                                ?>
                            </li>
                        </ul>
                    </div>
                    <div class="h1_text">
                        <h1><span><?php echo $search_pref; ?></span>の介護求人</h1>
                    </div>
                    <div class="kensaku_jyoken">
                        <p><?php echo $condition_detail_txt; ?></p>
                        <!-- <p><span>【資格】</span>介護職、ケアマネ</p>
                        <p><span>【施設】</span>有料老人ホーム</p>
                        <p><span>【条件】</span>夜勤アルバイト</p> -->
                    </div>
                    <div class="hikoukai_btn">
                        <?php 
                            echo $this->Html->link(
                            $this->Html->image('hikoukai_btn.svg', array('alt' => '完全無料　この条件に合う非公開求人を知りたい')),
                            '/private-form/',
                            array('escape' => false)); 
                        ?>
                    </div>
                </div>
            </div>
        </div>
		<div id="main">
			<div class="container">
				<div class="row">
                    <main id="contents">
                        <div class="h1_title sp">
                            <span class="icon001">
                                <?php echo $this->Html->image('icon001.svg'); ?>
                            </span>
                            <h1 class="top_h1"><?php echo $search_pref; ?>の介護求人</h1>
                        </div>
                        <span class="mcopy002 sp">※この条件で詳しく求人を知りたいetc...</span>
                        <div class="search_entry_btn sp">
                            <a href="/cw/private-form/" class="search_btn001">
                                <span class="free_search">無料</span>
                                <span class="search_btn_area">この条件の非公開求人</span>
                                <span class="search_btn_text">を紹介してもらう</span>
                                <?php 
                                    echo $this->Html->image('yajirushi_w.svg'); 
                                ?>
                            </a>
                        </div>
                        <div class="search_details sp">
                            <ul class="search_result">
                                <li>現在の条件で公開されている求人数<span class="jobs"><?php echo $this->Paginator->counter('{:count}'); ?></span>件</li>
                            </ul>
                            <p class="search_memo">※件数が多い場合は、条件を追加して再度ご検索ください。</p>
                            <p class="search_memo">※条件を変更したい場合でも下記のボタンから可能です。</p>
                            <div class="search_details_area">
                            <ul>
                                <li class="s_d_btn">
                                    <?php 
                                        echo $this->Html->link(
                                        $this->Html->image('area_shibori.svg', array('alt' => 'エリアを絞り込む')),
                                        '#area_sentaku',
                                        array('escape' => false, 'id' => 'modal_area'));
                                    ?>
                                </li>
                                <li class="s_d_btn">
                                    <?php 
                                        echo $this->Html->link(
                                        $this->Html->image('shikaku_shibori.svg', array('alt' => 'エリアを絞り込む')),
                                        '#shikaku_sentaku',
                                        array('escape' => false, 'id' => 'modal_shikaku'));
                                    ?>
                                </li>
                                <li class="s_d_btn">
                                    <?php 
                                        echo $this->Html->link(
                                        $this->Html->image('shisetsu_shibori.svg', array('alt' => 'エリアを絞り込む')),
                                        '#shisetsu_sentaku',
                                        array('escape' => false, 'id' => 'modal_shisetsu'));
                                    ?>
                                </li>
                                <li class="s_d_btn_end">
                                    <?php 
                                        echo $this->Html->link(
                                        $this->Html->image('jyoken_shibori.svg', array('alt' => 'エリアを絞り込む')),
                                        '#jyoken_sentaku',
                                        array('escape' => false, 'id' => 'modal_jyoken'));
                                    ?>
                                </li>
                            </ul>
                            </div>
                        </div>
                        <div class="prev_next">
                            <p class="count">
                                <?php echo $this->Paginator->counter('{:count}件中{:start}-{:end}件を表示'); ?>
                            </p>
                            <ul class="prev_next_number">
                                <?php 
                                    $modulus = 6;
                                    if ($this->Paginator->hasPrev()) {
                                        echo $this->Paginator->first('<<', array('class'=>'first_page'));
                                    }
                                    if ($this->Paginator->hasPrev() && $this->Paginator->hasNext()) {
                                        $modulus = 5;
                                    }
                                    echo $this->Paginator->numbers(array(
                                        'modulus'=>$modulus,//ページ番号を幾つ表示するか（デフォルト値：8）
                                        'currentTag'=> 'p',
                                        'currentClass'=>'disabled'
                                        )
                                    );
                                    if ($this->Paginator->hasNext()) {
                                        $count = $this->Paginator->counter(
                                            '{:count}'
                                        );
                                        // 下記構文では最終ページのリンクを生成できないため、切り上げ(求人数 / 10)で計算を行う
                                        // echo $this->Paginator->last('>>');
                                        echo '<li>';
                                        $last_link = '';
                                        if (count($paginatorUrlArray) != 0) {
                                            $last_link = $paginatorUrlArray['plugin'] . '/' . $paginatorUrlArray['controller'] . '/' . $paginatorUrlArray['action'] . '/page:' . ceil($count / 10);
                                        } else {
                                            $last_link = '/recruits/results' . $search_param . '/page:' . ceil($count / 10);
                                        }
                                        echo $this->Html->link(
                                            '>>',
                                            $last_link,
                                            array('escape' => false)
                                        );
                                        echo '</li>';
                                    }
                                ?>
                            </ul>
                        </div>
                        <div>
                        <?php 
                            foreach ($results as $key => $item) {
                        ?>
                                <article class="ichiran_item">
                                    <div class="ichiran_top">
                                        <div class="like_image">
                                            <?php 
                                                echo $this->Html->image(in_array($item['Recruit']['id'], $favoritesArray) ? 'like.svg' : 'not_like.svg', array('class' => in_array($item['Recruit']['id'], $favoritesArray) ? 'like_img' : 'not_like_img', 'id' => 'f_' . $item['Recruit']['id'])); 
                                            ?>
                                        </div>
                                        <h2>
                                            <span class="houjin">
                                                <?php
                                                    if ($item['Facility']['FacilityShow']['groupmei_chk'] == 1) {
                                                        echo empty($item['Facility']['group']) ? '' : $item['Facility']['group'] . '&nbsp;';
                                                    }
                                                    if ($item['Facility']['FacilityShow']['houjinmei_chk'] == 1) {
                                                        echo empty($item['Facility']['corporation']) ? '' : $item['Facility']['corporation'] . '&nbsp;';
                                                    }
                                                ?>
                                            </span>
                                            <span class="shisetsu">
                                                <?php echo $item['Facility']['shisetsumei']; ?>
                                            </span>
                                        </h2>
                                    </div>
                                    <div class="kodawari">
                                        <ul class="list_inline">
                                            <?php
                                                foreach ($item['Kodawari'] as $kodawariItem) {
                                                    echo '<li><span>' . $kodawariItem['name'] . '</span></li>';
                                                }
                                            ?>
                                        </ul>
                                    </div>
                                    <div class="ichiran_middle">
                                        <div class="ichiran_image">
                                            <?php
                                                $alt = '';
                                                if (!empty($item['Facility']['corporation'])) {
                                                    $alt = $item['Facility']['corporation'] . ' ';
                                                }
                                                $alt = $alt . $item['Facility']['shisetsumei'];

                                                $facilityImg = !empty($item['Facility']['facility_image']) ? '/files/facility/facility_image/' . $item['Facility']['facility_dir'] . '/' . $item['Facility']['facility_image'] : '/img/default.svg';
                                                echo $this->Html->image($facilityImg, array('class' => 'img-responsive', 'alt' => $alt . 'の外観や内観になります。'));
                                            ?>
                                        </div>
                                        <div class="ichiran_text">
                                            <div class="ichiran_text01">
                                                <p class="job_no_text">求人NO</p>
                                                <p class="job_no"><?php echo $item['Recruit']['id']; ?></p>
                                            </div>
                                            <div class="ichiran_text02">
                                                <?php echo $this->Html->image('ichiran_area.svg'); ?>
                                                <p class="ichiran_area"><?php echo $item['Facility']['shikutyouson']; ?></p>
                                            </div>
                                            <!-- 
                                                20190527 sohnisi
                                                掲載オプションが非掲載であれば、枠自体非表示とする
                                            -->
                                            <?php if ($item['RecruitShow']['shokushu_chk']) { ?>
                                                <div class="ichiran_text02">
                                                    <?php echo $this->Html->image('ichiran_license.svg'); ?>
                                                    <p class="ichiran_license"><?php echo $shokushu_options[$item['Recruit']['shokushu']]; ?></p>
                                                </div>
                                            <?php } ?>
                                            <!-- 
                                                20190527 sohnisi
                                                掲載オプションが非掲載であれば、枠自体非表示とする
                                            -->
                                            <?php if ($item['RecruitShow']['kinmu_keitai_chk']) { ?>
                                            <div class="ichiran_text02">
                                                <?php echo $this->Html->image('ichiran_employ.svg'); ?>
                                                <p class="ichiran_employ"><?php echo $kinmu_keitai_options[$item['Recruit']['kinmu_keitai']]; ?></p>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="catchphrase">
                                        <p class="catchphrase_text">
                                            <?php echo !empty($item['Facility']['kanrenshisetsu']) ? nl2br($item['Facility']['kanrenshisetsu']) : ''; ?>
                                        </p>
                                    </div>
                                    <span class="mcopy002 sp">※この条件で詳しく求人を知りたいetc...</span>
                                    <div id="recruit_detail">
                                        <table class="table-bordered">
                                            <tbody>
                                                <!-- 
                                                    20190527 sohnisi
                                                    掲載オプションが非掲載であれば、枠自体非表示とする
                                                -->
                                                <?php if ($item['RecruitShow']['kyuyo_chk'] == 1) { ?>
                                                    <tr>
                                                        <th>給与</th>
                                                        <td>
                                                            <?php echo nl2br($item['Recruit']['kyuyo']); ?>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                                <tr>
                                                    <th>施設形態</th>
                                                    <td>
                                                        <?php echo nl2br($shisetsukeitaiFirsts[$item['Facility']['ShisetsukeitaiFirst'][0]['id']]); ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>勤務先</th>
                                                    <td>
                                                        <?php echo $item['Facility']['todoufuken']; ?>
                                                        <?php echo $item['Facility']['shikutyouson']; ?>
                                                        <?php echo $item['Facility']['banchi']; ?>
                                                        <?php echo $item['Facility']['tatemono']; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>最寄駅</th>
                                                    <td>
                                                    <?php 
                                                        $rosenekiIndex = 0;
                                                        foreach ($item['Facility']['Roseneki'] as $roseneki) : 
                                                            // 2行以上の場合は改行
                                                            if ($rosenekiIndex >= 1) {
                                                                echo '<br>';
                                                            }
                                                    ?>  
                                                        <?php echo isset($roseneki['RailroadLine']['line_name']) ? $roseneki['RailroadLine']['line_name'] : ''; ?>
                                                        <?php echo isset($roseneki['RailroadStation']['station_name']) ? $roseneki['RailroadStation']['station_name'] . '駅' : ''; ?>
                                                        <?php
                                                            $rosenekiIndex += 1;
                                                        ?>
                                                    <?php endforeach; ?>
                                                    </td>
                                                </tr>
                                                <!-- 
                                                    20190527 sohnisi
                                                    掲載オプションが非掲載であれば、枠自体非表示とする
                                                -->
                                                <?php if ($item['RecruitShow']['shikaku_chk']) { ?>
                                                    <tr>
                                                        <th>応募資格</th>
                                                        <td>
                                                            <?php echo $shikaku_options[$item['Recruit']['shikaku']]; ?>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="ichiran_bottom">
                                        <div class="details_btn">
                                            <a href="">
                                                
                                            </a>

                                            <?php 
                                                echo $this->Html->link(
                                                $this->Html->image('shosai.svg') . '<span class="details_btn_text">詳細を見る</span>',
                                                '/recruits/detail/' . $item['Recruit']['id'],
                                                array('escape' => false));
                                            ?>
                                        </div>
                                        <div class="item_entry_btn">
                                            <?php 
                                                echo $this->Html->link(
                                                '<span class="item_entry_btn_text">求人を問い合わせる</span>' . $this->Html->image('yajirushi_w.svg'),
                                                '/entry-form/' . $item['Recruit']['id'],
                                                array('escape' => false));
                                            ?>
                                        </div>
                                    </div>
                                </article>
                            <?php
                                }
                            ?>
                        </div>
                        <div class="prev_next">
                            <p class="count">
                                <?php echo $this->Paginator->counter('{:count}件中{:start}-{:end}件を表示'); ?>
                            </p>
                            <div class="prev_next_btn">
                                <div class="prev">
                                        <?php 
                                            if ($this->Paginator->hasPrev()) {
                                                echo $this->Paginator->prev(
                                                    $this->Html->image('yajirushi_o_left.svg') . '<span class="prev_text" style="color:#F7931E;font-weight: 600;">前の10件に戻る</span>',
                                                    array(
                                                        'escape' => false,
                                                        'tag' => false
                                                    ),
                                                    null,
                                                    array(
                                                        'class' => 'prev disabled',
                                                        'escape' => false,
                                                        'tag' => false
                                                        )
                                                );
                                            } else {
                                                echo $this->Html->image('yajirushi_left.svg') . '<span class="prev_text">前の10件に戻る</span>';
                                            }
                                        ?>
                                        
                                    <?php
                                    ?>
                                </div>
                                <div class="next">
                                    <?php 
                                        if ($this->Paginator->hasNext()) {
                                            echo $this->Paginator->next(
                                                $this->Html->image('yajirushi.svg') . '<span class="next_text">次の10件に進む</span>',
                                                array(
                                                    'escape' => false,
                                                    'tag' => false
                                                ),
                                                null,
                                                array(
                                                    'class' => 'next disabled',
                                                    'escape' => false,
                                                    'tag' => false
                                                    )
                                            );
                                        } else {
                                            // JOBALC-TODO: yajirushiを灰色に変更
                                            echo $this->Html->image('yajirushi_right.svg') . '<span class="next_text">次の10件に進む</span>';
                                        }
                                    ?>
                                </div>
                            </div>
                            <ul class="prev_next_number">
                                <?php
                                    $modulus = 6;
                                    if ($this->Paginator->hasPrev()) {
                                        echo $this->Paginator->first('<<', array('class'=>'first_page'));
                                    }
                                    if ($this->Paginator->hasPrev() && $this->Paginator->hasNext()) {
                                        $modulus = 5;
                                    }
                                    echo $this->Paginator->numbers(array(
                                        'modulus'=>$modulus,//ページ番号を幾つ表示するか（デフォルト値：8）
                                        'currentTag'=> 'p',
                                        'currentClass'=>'disabled',
                                        // 'url' => array('controller' => 'area', 'action' => 'pref27')
                                        )
                                    );
                                    if ($this->Paginator->hasNext()) {
                                        $count = $this->Paginator->counter(
                                            '{:count}'
                                        );
                                        // 下記構文では最終ページのリンクを生成できないため、切り上げ(求人数 / 10)で計算を行う
                                        // echo $this->Paginator->last('>>');
                                        echo '<li>';
                                        $last_link = '';
                                        if (count($paginatorUrlArray) != 0) {
                                            $last_link = $paginatorUrlArray['plugin'] . '/' . $paginatorUrlArray['controller'] . '/' . $paginatorUrlArray['action'] . '/page:' . ceil($count / 10);
                                        } else {
                                            $last_link = '/recruits/results' . $search_param . '/page:' . ceil($count / 10);
                                        }

                                        echo $this->Html->link(
                                            '>>',
                                            $last_link,
                                            array('escape' => false)
                                        );
                                        echo '</li>';
                                    }
                                ?>
                            </ul>
                        </div>
                        <div class="search_details sp" id="last_search_details_sp">
                            <ul class="search_result">
                                <li>現在の条件で公開されている求人数<span class="jobs"><?php echo $this->Paginator->counter('{:count}'); ?></span>件</li>
                            </ul>
                            <p class="search_memo">※件数が多い場合は、条件を追加して再度ご検索ください。</p>
                            <p class="search_memo">※条件を変更したい場合でも下記のボタンから可能です。</p>
                            <div class="search_details_area">
                            <ul>
                                <li class="s_d_btn">
                                    <?php 
                                        echo $this->Html->link(
                                        $this->Html->image('area_shibori.svg', array('alt' => 'エリアを絞り込む')),
                                        '#area_sentaku',
                                        array('escape' => false, 'id' => 'modal_area'));
                                    ?>
                                </li>
                                <li class="s_d_btn">
                                    <?php 
                                        echo $this->Html->link(
                                        $this->Html->image('shikaku_shibori.svg', array('alt' => 'エリアを絞り込む')),
                                        '#shikaku_sentaku',
                                        array('escape' => false, 'id' => 'modal_shikaku'));
                                    ?>
                                </li>
                                <li class="s_d_btn">
                                    <?php 
                                        echo $this->Html->link(
                                        $this->Html->image('shisetsu_shibori.svg', array('alt' => 'エリアを絞り込む')),
                                        '#shisetsu_sentaku',
                                        array('escape' => false, 'id' => 'modal_shisetsu'));
                                    ?>
                                </li>
                                <li class="s_d_btn_end">
                                    <?php 
                                        echo $this->Html->link(
                                        $this->Html->image('jyoken_shibori.svg', array('alt' => 'エリアを絞り込む')),
                                        '#jyoken_sentaku',
                                        array('escape' => false, 'id' => 'modal_jyoken'));
                                    ?>
                                </li>
                            </ul>
                            </div>
                        </div>
                        <!--登録フォーム-->
                        <div class="private">
                            <div class="private_area">
                                <div class="h2_title">
                                    <span class="icon001">
                                        <?php echo $this->Html->image('icon007.svg'); ?>
                                    </span>
                                    <h2 class="top_h2">いい求人が見つからなかった方</h2>
                                </div>
                                <div class="private_text">
                                    <p class="widget02_text">公開している求人だけではない？</p>
                                    <h4 class="widget03_text">非公開を希望されるケースは少なくありません。</h4>
                                    <p class="widget04_text">現在、働いている方への影響を避けたいなど、管理職の求人など、さまざま事情により公開を避けたい場合もあります。非公開求人として管理しているので、希望の条件などをお伝えし、相談してみましょう。</p>
                                </div>
                                <div class="private_btn">
                                    <?php 
                                        echo $this->Html->link(
                                        '<span>' . $this->Html->image('private.svg') . '</span>',
                                        '/private-form/',
                                        array('escape' => false));
                                    ?>
                                </div>
                            </div>
                        </div>
                        <?php
                            // 都道府県別ELEMENTS - group_1
                            if ($url == 'area' && !$isCitySearch) {
                                echo $this->element('area/group_1/' . $id);
                            }

                            // 政令都市別ELEMENTS - group_1
                            if ($url == 'area' && $isCitySearch) {
                                echo $this->element('city/group_1/' . $city);
                            }

                            // 勤務形態別ELEMENTS - group_1
                            if ($url == 'employment') {
                                echo $this->element('employment/group_1/' . $id);
                            }

                            // 職種別ELEMENTS - group_1
                            if ($url == 'job') {
                                echo $this->element('job/group_1/' . $id);
                            }

                            // 施設形態別ELEMENTS - group_1
                            if ($url == 'facility') {
                                echo $this->element('facility/group_1/' . $id);
                            }

                            // 資格別ELEMENTS - group_1
                            if ($url == 'qualification') {
                                echo $this->element('qualification/group_1/' . $id);
                            }
                        ?>
                    </main>
                    <aside id="sidebar" class="pc">
						<div class="sidebox hidden-xs">
							<div class="support_widget">
                                <h4>
                                    <?php 
                                        echo $this->Html->link(
                                        $this->Html->image('touroku.svg', array('alt' => '登録 希望の条件のお仕事を紹介してもらう。')),
                                        '/entry-form/',
                                        array('escape' => false)); 
                                    ?>
                                </h4>
                            </div>
                            <div class="support_widget">
                                <h4>
                                    <?php 
                                        echo $this->Html->link(
                                        $this->Html->image('start.svg', array('alt' => 'はじめての方はこちらから')),
                                        '/about',
                                        array('escape' => false)); 
                                    ?>
                                </h4>
                            </div>
                            <div class="list_widget">
                                <h4>施設の種類から探す</h4>
                                <ul>
									
									<?php 
										$shisetsukeitaiFirstsForIndex = 0;
										foreach ($shisetsukeitaiFirsts as $key => $value) {
											if ($shisetsukeitaiFirstsForIndex == 0) {
												echo '<li class="li_fst">';
											} else {
												echo '<li>';
											}
									?>
											
                                                <?php 
                                                    $url = '/facility/' . $key;
                                                    if ($todoufuken != null) {
                                                        if ($city != null) {
                                                            $url = '/recruits/results/todoufuken:' . $todoufuken . '/shikutyouson:' . $city . '/shisetsukeitai_first%5B13%5D:' . $key;    
                                                        } else {
                                                            $url = '/recruits/results/todoufuken:' . $todoufuken . '/shisetsukeitai_first%5B13%5D:' . $key;
                                                        }
                                                    }
													echo $this->Html->link(
														$value,
														$url,
														array('escape' => false)
													); 
													echo $this->Html->image('yajirushi.svg'); 
												?>
											</li>
									<?php
											$shisetsukeitaiFirstsForIndex+=1;
										}
									?>

                                </ul>
                            </div>
                            <div class="list_widget">
                                <h4>職種から探す</h4>
                                <ul>
                                    <?php 
										$shokushuOptionsForIndex = 0;
										foreach ($shokushu_options as $key => $value) {
											if ($shokushuOptionsForIndex == 0) {
												echo '<li class="li_fst">';
											} else {
												echo '<li>';
											}
									?>
											
                                                <?php 
                                                    $url = '/job/' . $key;
                                                    if ($todoufuken != null) {
                                                        if ($city != null) {
                                                            $url = '/recruits/results/todoufuken:' . $todoufuken . '/shikutyouson:' . $city . '/shokushu%5B1%5D:' . $key;    
                                                        } else {
                                                            $url = '/recruits/results/todoufuken:' . $todoufuken . '/shokushu%5B1%5D:' . $key;
                                                        }
                                                    }
													echo $this->Html->link(
														$value,
														$url,
														array('escape' => false)
													); 
													echo $this->Html->image('yajirushi.svg'); 
												?>
											</li>
									<?php
											$shokushuOptionsForIndex+=1;
										}
									?>
                                </ul>
                            </div>
                            <div class="list_widget">
                                <h4>勤務形態から探す</h4>
                                <ul>

									<?php 
										$kinmuKeitaiOptionsForIndex = 0;
										foreach ($kinmu_keitai_options as $key => $value) {
											if ($kinmuKeitaiOptionsForIndex == 0) {
												echo '<li class="li_fst">';
											} else {
												echo '<li>';
											}
									?>
											
                                                <?php 
                                                    $url = '/employment/' . $key;
                                                    if ($todoufuken != null) {
                                                        if ($city != null) {
                                                            $url = '/recruits/results/todoufuken:' . $todoufuken . '/shikutyouson:' . $city . '/kinmu_keitai%5B0%5D:' . $key;    
                                                        } else {
                                                            $url = '/recruits/results/todoufuken:' . $todoufuken . '/kinmu_keitai%5B0%5D:' . $key;
                                                        }
                                                    }
													echo $this->Html->link(
														$value,
														$url,
														array('escape' => false)
													); 
													echo $this->Html->image('yajirushi.svg'); 
												?>
											</li>
									<?php
											$kinmuKeitaiOptionsForIndex+=1;
										}
									?>
                                    
                                </ul>
                            </div>
                            <div class="list_widget">
                                <h4>資格から探す</h4>
                                <ul>
                                    <?php 
										$shikakuOptionsForIndex = 0;
										foreach ($shikaku_options as $key => $value) {
											if ($shikakuOptionsForIndex == 0) {
												echo '<li class="li_fst">';
											} else {
												echo '<li>';
											}
									?>
											
                                                <?php 
                                                    $url = '/qualification/' . $key;
                                                    if ($todoufuken != null) {
                                                        if ($city != null) {
                                                            $url = '/recruits/results/todoufuken:' . $todoufuken . '/shikutyouson:' . $city . '/shikaku%5B1%5D:' . $key;    
                                                        } else {
                                                            $url = '/recruits/results/todoufuken:' . $todoufuken . '/shikaku%5B1%5D:' . $key;
                                                        }
                                                    }
													echo $this->Html->link(
														$value,
														$url,
														array('escape' => false)
													); 
													echo $this->Html->image('yajirushi.svg'); 
												?>
											</li>
									<?php
											$shikakuOptionsForIndex+=1;
										}
									?>
                                </ul>
                            </div>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
        <div class="bread sp">
            <div class="container">
                <span>
                    <?php
                        echo $this->Html->link(
                            '<span>介護の求人TOP</span>',
                            '/',
                            array('escape' => false)
                        ); 
                        if (!empty($middle_breadcrumb)) {
                            foreach ($middle_breadcrumb as $name => $url) {
                                echo '&nbsp;&gt;&nbsp;';
                                echo $this->Html->link(
                                    '<span>' . $name . '</span>',
                                    $url,
                                    array('escape' => false)
                                );
                            }
                        }
                    ?>&nbsp;&gt;&nbsp;
                    <span><?php echo $search_pref; ?>の介護職求人</span>
                </span>
            </div>
        </div>
        <footer class="footer_sp sp">
            <div class="container">
				<div class="row">
                    <div class="footer">
                        <div class="footer_Menus">
                            <ul class="footer_menu">
                                <li class="footerMenu_list">
									<?php 
                                        echo $this->Html->link(
										$this->Html->image('1.svg', array('alt' => '無料登録')),
                                        '/entry-form/',
                                        array('escape' => false));
                                    ?>
								</li>
                                <li class="footerMenu_list">
									<?php 
										echo $this->Html->link(
										$this->Html->image('2.svg', array('alt' => '個人情報')),
										'/privacy',
										array('escape' => false)); 
									?>
								</li>
                                <li class="footerMenu_list">
									<?php 
										echo $this->Html->link(
										$this->Html->image('3.svg', array('alt' => '会社概要')),
										'/corporation',
										array('escape' => false)); 
									?>
								</li>
								<li class="footerMenu_list" id="modal_search_footer_parent_sp">
                                    <a class='modal_search_footer'><?php echo $this->Html->image('4.svg', array('alt' => '求人検索')) ?></a>
								</li>
                                <li class="footerMenu_list_last">
									<?php 
										echo $this->Html->link(
										$this->Html->image('5.svg', array('alt' => '利用規約')),
										'/terms',
										array('escape' => false)); 
									?>
								</li>
                            </ul>
                        </div>
                    </div>
				</div>
				<div class="footer-bottom text-center">
					<address class="address"> Copyright © 2019 ALC Co.Ltd All Rights Reserved.</address>
				</div>
			</div>
		</footer>
        <footer class="footer_pc pc">
            <div class="container">
				<div class="row">
                    <div class="footer">
                        <div class="footer_Menus">
                            <ul class="footer_menu">
                                <li class="footerMenu_title">メニュー</li>
                                <li class="footerMenu_list">
								<a href="">ホーム</a></li>
                                <li class="footerMenu_list"><a href="/cw/about/">はじめの方</a></li>
                                <li class="footerMenu_list" id="modal_search_footer_parent"><a class='modal_search_footer'>求人検索</a></li>
                                <li class="footerMenu_list"><a href="/cw/user_voice/">ご利用者の声</a></li>
                                <li class="footerMenu_list"><a href="/cw/service/">ご利用の流れ</a></li>
                            </ul>
                            <ul class="footer_menu">
                                <li class="footerMenu_title">転職コラム</li>
                                <li class="footerMenu_list"><a href="/">転職コラムTOP</a></li>
                                <li class="footerMenu_list"><a href="/">求人探しのポイント</a></li>
                                <li class="footerMenu_list"><a href="/">履歴書の書き方</a></li>
                                <li class="footerMenu_list"><a href="/">面接の対策</a></li>
                                <li class="footerMenu_list"><a href="/">入職への心構え</a></li>
                            </ul>
                            <ul class="footer_menu">
                                <li class="footerMenu_title">運用情報</li>
                                <li class="footerMenu_list"><a href="/cw/corporation/">会社概要</a></li>
                                <li class="footerMenu_list"><a href="/cw/terms/">利用規約</a></li>
                                <li class="footerMenu_list"><a href="/cw/privacy/">プライバシーポリシー</a></li>
                            </ul>
                            <ul class="footer_menu">
                                <li class="footerMenu_title">アルクの運営サイト</li>
                                <li class="footerMenu_list"><a href="/ns/">ジョブアルク看護師</a></li>
                                <li class="footerMenu_list"><a href="/fm/">ジョブアルク薬剤師</a></li>
                                <li class="footerMenu_list"><a href="/rh/">ジョブアルクPOS</a></li>
                            </ul>
                        </div>
                    </div>
					<div class="footer_tel">
						<div class="information">
							<?php 
								echo $this->Html->image('top_tel.svg', array('alt' => 'お電話でのお仕事相談・登録ができます。0120-932-929：【受付】9:30～20:00（土・日・祝も可能）')); 
							?>
                        </div>
                    </div>
					<div class="footer_btn">
						<div class="footer_entry">
							<?php 
								echo $this->Html->link(
								$this->Html->image('btn001_pc.svg', array('alt' => '無料　アドバイザーに相談したい')),
								'/consultation-form/',
								array('escape' => false)); 
							?>
                        </div>
                    </div>
				</div>
				<div class="footer-bottom text-center">
					<div>厚生大臣許可 27-ュ-201641</div>
					<address> Copyright © 2019 ALC Co.Ltd All Rights Reserved.</address>
				</div>
			</div>
		</footer>
    </div>



    <!-- 検索モーダル -->

	<div class="modal_content">
        <div class="modal_window">
            <!-- <form action="/r/search.php" method="post" class="modal modal-foot is-none" data-modal-name="search" id="sform"> -->
            <?php
				echo $this->Form->create($model, array(
					'inputDefaults' => array(
						'label' => false,
						'inputWrap' => false,
						'div' => false,
					),
					'class' => 'form-control modal modal-foot is-none',
					'data-modal-name' => 'search',
                    'id' => 'sform',
                    // 'url' => $paginatorUrlArray
					'url' => array_merge(array('action' => 'results'), $this->params['pass'])
				));
			?>
            <!-- <input type="hidden" name="formredirect" value="1"> -->
              <div class="modal_close_wrapper">
                <div class="modal_close js-modal" data-target-modal="search" onclick="">
                    <i class="fas fa-times close_icon"></i>
                </div>
              </div>
              <div class="modal_inner">
                <div class="modal_head">
                  <h1 class="modal_headTit">求人検索</h1>
                  <p class="modal_headLink"><a href="#" id="restbtn">条件をクリア</a></p>
                  <!-- <p class="modal_headLink"><a href="/search-list/">履歴から検索</a></p> -->
                </div>
                <div class="modal_body">
                  <h2 class="ttl-m" id="area_sentaku">エリアを選択</h2>

                  <div class="l-grid l-grid-margin-xs-3 l-grid-margin-md-5 u-mb-xs-15">
                    <div class="l-grid_col col-xs-4 col-md-2">
                      <label class="input-radio js-tab" data-tab-group="area" data-tab-parent-name="1">
                        <input type="radio" id="select_area" checked="">
                        <span class="input-radioInner" id = "chiiki_select">
                          <span class="input-radioIcon"></span>
                          <span class="input-radioTxt">地域</span>
                        </span>
                      </label>
                    </div>
                    <div class="l-grid_col col-xs-5 col-md-2">
                      <label class="input-radio js-tab" data-tab-group="area" data-tab-parent-name="2">
                        <input type="radio" id="select_ln">
                        <span class="input-radioInner" id = "rosen_select">
                          <span class="input-radioIcon"></span>
                          <span class="input-radioTxt">路線・駅</span>
                        </span>
                      </label>
                    </div>
                  </div>

                  <div class="" data-tab-group="area" data-tab-child-name="1" id="chiiki_div">
                    <div class="l-grid u-mb-xs-30">

                      <!-- 地域 選択時に表示 -->
                      <div class="l-grid_col col-xs-12 col-md-4 u-mb-xs-10">
                        <label class="select js-selectToggle" data-target-toggle="chiiki_region" id="chiiki_region">
                            <label for="pr" class="chdown"><i class="fas fa-angle-down chdown_icon"></i></label>
                            <?php echo $this->Form->select('todoufuken', null, array('div' => false, 'empty' => '都道府県を選択', 'class' => 'reset prlist areapr')); ?>
                        </label>
                      </div>
                      <!-- /地域 選択時に表示 -->

                      <!-- 地域 → 都道府県 選択時に表示 -->
                      <div class="l-grid_col col-xs-12 col-md-4" data-toggle-name="chiiki_region">
                        <div class="js-checkBoxToggle" data-target-toggle="shikuchoson">
                            <label class="select js-selectToggle" data-target-toggle="chiiki_region">
                            <label for="pr" class="chdown"><i class="fas fa-angle-down chdown_icon"></i></label>
                            <?php echo $this->Form->select('shikutyouson', null, array('div' => false, 'empty' => '市区町村を選択', 'class' => 'reset prlist areapr')); ?>
                              <!-- <select name="pr" class="reset prlist areapr">
                                <option value="">市区町村を選択</option>
                              </select> -->
                            </label>
                        </div>
                      </div>
                      <!-- /地域 → 都道府県 選択時に表示 -->

                    </div>
                  </div>


                  <div class=" u-d-n" data-tab-group="area" data-tab-child-name="2" id="rosen_div">
                    <div class="l-grid u-mb-xs-30">
                      <!-- 路線 選択時に表示 -->

                      <div class="l-grid_col col-xs-12 col-md-4 u-mb-xs-10">
                        <label class="select js-selectToggle" data-target-toggle="rosen_region" id="rosen_region">
                            <label for="pr" class="chdown"><i class="fas fa-angle-down chdown_icon"></i></label>
                            <?php echo $this->Form->select('pref_cd', $prefs, array('div' => false, 'empty' => '都道府県を選択', 'class' => 'reset prlist areapr')); ?>
                        </label>
                      </div>
                      <!-- /路線 選択時に表示 -->

                      <!-- 路線 → 都道府県 選択時に表示 -->
                      <div class="l-grid_col col-xs-12 col-md-4 lnclick" data-toggle-name="rosen_region">
                        <div class="js-checkBoxToggle" data-target-toggle="rosen">
                            <label class="select js-selectToggle select_mbt" data-target-toggle="rosen_region">
                            <label for="pr" class="chdown"><i class="fas fa-angle-down chdown_icon"></i></label>
                            <?php echo $this->Form->select('line_cd', $lines, array('div' => false, 'empty' => '最寄り路線を選択', 'class' => 'reset prlist areapr')); ?>
                              <!-- <select name="pr" class="reset prlist lnpr">
                                <option value="">路線を選択</option>
                              </select> -->
                        </label>
                        </div>
                      </div>
                      <!-- /路線 → 都道府県 選択時に表示 -->

                      <!-- 路線 → 都道府県 → 路線 選択時に表示 -->
                      <div class="l-grid_col col-xs-12 col-md-4 lnclick" data-toggle-name="rosen_region">
                        <div class="js-checkBoxToggle" data-target-toggle="rosen">
                            <label class="select js-selectToggle" data-target-toggle="rosen_region">
                            <label for="pr" class="chdown"><i class="fas fa-angle-down chdown_icon"></i></label>
                            <?php echo $this->Form->select('station_cd', $stations, array('div' => false, 'empty' => '最寄り駅を選択', 'class' => 'reset prlist areapr')); ?>
                              <!-- <select name="pr" class="reset prlist lnpr">
                                <option value="">駅を選択</option>
                            　</select> -->
                        </label>
                        </div>
                      </div>
                      <!-- 路線 → 都道府県 → 路線 選択時に表示 -->

                    </div>
                  </div>


                  <h2 class="ttl-m">職種を選択<span class="ttl-m_sub">（複数選択可）</span></h2>
                  <div class="l-grid l-grid-margin-xs-2 l-grid-margin-md-5 u-mb-xs-25">

                    <?php 
                        foreach ($shokushu_options as $key => $value) {
                    ?>
                            
                            <div class="l-grid_col col-xs-6 col-md-2 u-mb-xs-5 u-mb-md-10">
                            <label class="input-checkbox">
                                <?php
                                    echo $this->Form->input('Recruit.shokushu.' . $key, array('type' => 'checkbox', 'class' => 'reset', 'value' => $key, 'hiddenField' => false, 'checked' => in_array($key, $this->request->data['Recruit']['shokushu']) ? true : false));
                                ?>
                                <span class="<?= in_array($key, $this->request->data['Recruit']['shokushu']) ? 'input-checkboxInner checked' : 'input-checkboxInner' ?>">
                                    <span class="input-checkboxIcon"></span>
                                    <span class="input-checkboxTxt"><?php echo $value; ?></span>
                                    <?= in_array($key, $this->request->data['Recruit']['shokushu']) ? '<i class="fas fa-check check_icon"></i>' : '' ?>
                                </span>
                            </label>
                            </div>
                    <?php
                        }
                    ?>

                  </div>


                  <h2 class="ttl-m">勤務形態を選択<span class="ttl-m_sub">（複数選択可）</span></h2>
                  <div class="l-grid l-grid-margin-xs-2 l-grid-margin-md-5 u-mb-xs-25">

                    <?php 
                        foreach ($kinmu_keitai_options as $key => $value) {
                    ?>
                            
                            <div class="l-grid_col col-xs-6 col-md-2 u-mb-xs-5 u-mb-md-10">
                            <label class="input-checkbox">
                                <?php
                                    echo $this->Form->input('Recruit.kinmu_keitai.' . $key, array('type' => 'checkbox', 'class' => 'reset', 'value' => $key, 'hiddenField' => false, 'checked' => in_array($key, $this->request->data['Recruit']['kinmu_keitai']) ? true : false));
                                ?>
                                <span class="<?= in_array($key, $this->request->data['Recruit']['kinmu_keitai']) ? 'input-checkboxInner checked' : 'input-checkboxInner' ?>">
                                    <span class="input-checkboxIcon"></span>
                                    <span class="input-checkboxTxt"><?php echo $value; ?></span>
                                    <?= in_array($key, $this->request->data['Recruit']['kinmu_keitai']) ? '<i class="fas fa-check check_icon"></i>' : '' ?>
                                </span>
                            </label>
                            </div>
                    <?php
                        }
                    ?>

                  </div>


                  <h2 class="ttl-m" id="shikaku_sentaku">資格を選択<span class="ttl-m_sub">（複数選択可）</span></h2>
                  <div class="l-grid l-grid-margin-xs-2 l-grid-margin-md-5 u-mb-xs-25">

                    <?php 
                        foreach ($shikaku_options as $key => $value) {
                    ?>
                            
                            <div class="l-grid_col col-xs-6 col-md-2 u-mb-xs-5 u-mb-md-10">
                            <label class="input-checkbox">
                                <?php
                                    echo $this->Form->input('Recruit.shikaku.' . $key, array('type' => 'checkbox', 'class' => 'reset', 'value' => $key, 'hiddenField' => false, 'checked' => in_array($key, $this->request->data['Recruit']['shikaku']) ? true : false));
                                ?>
                                <span class="<?= in_array($key, $this->request->data['Recruit']['shikaku']) ? 'input-checkboxInner checked' : 'input-checkboxInner' ?>">
                                    <span class="input-checkboxIcon"></span>
                                    <span class="input-checkboxTxt"><?php echo $value; ?></span>
                                    <?= in_array($key, $this->request->data['Recruit']['shikaku']) ? '<i class="fas fa-check check_icon"></i>' : '' ?>
                                </span>
                            </label>
                            </div>
                    <?php
                        }
                    ?>

                    
                  </div>

                  <h2 class="ttl-m" id="shisetsu_sentaku">施設業態を選択<span class="ttl-m_sub">（複数選択可）</span></h2>
                  <div class="l-grid l-grid-margin-xs-2 l-grid-margin-md-5 u-mb-xs-25">
                    

                    <?php 
                        foreach ($shisetsukeitaiFirsts as $key => $value) {
                    ?>
                            <div class="l-grid_col col-xs-6 col-md-2 u-mb-xs-5 u-mb-md-10">
                                <label class="input-checkbox">
                                    <?php 
                                        echo $this->Form->input('Recruit.shisetsukeitai_first.' . $key, array('type' => 'checkbox', 'class' => 'reset', 'value' => $key, 'hiddenField' => false, 'checked' => in_array($key, $this->request->data['Recruit']['shisetsukeitai_first']) ? true : false));
                                    ?>
                                    <span class="<?= in_array($key, $this->request->data['Recruit']['shisetsukeitai_first']) ? 'input-checkboxInner checked' : 'input-checkboxInner' ?>">
                                        <span class="input-checkboxIcon"></span>
                                        <span class="input-checkboxTxt"><?php echo $value; ?></span>
                                        <?= in_array($key, $this->request->data['Recruit']['shisetsukeitai_first']) ? '<i class="fas fa-check check_icon"></i>' : '' ?>
                                    </span>
                                </label>
                            </div>
                    <?php
                        }
                    ?>
                  
                  </div>

                  <h2 class="ttl-m" id="jouken_sentaku">こだわり条件を選択<span class="ttl-m_sub">（複数選択可）</span></h2>
                  <div class="l-grid l-grid-margin-xs-2 l-grid-margin-md-5 u-mb-xs-25">

                    <?php 
                        foreach ($kodawaris as $key => $value) {
                    ?>
                            <div class="l-grid_col col-xs-6 col-md-2 u-mb-xs-5 u-mb-md-10">
                                <label class="input-checkbox">
                                    <?php 
                                        echo $this->Form->input('Recruit.kodawari.' . $key, array('type' => 'checkbox', 'class' => 'reset', 'value' => $key, 'hiddenField' => false, 'checked' => in_array($key, $this->request->data['Recruit']['kodawari']) ? true : false));
                                    ?>
                                    <span class="<?= in_array($key, $this->request->data['Recruit']['kodawari']) ? 'input-checkboxInner checked' : 'input-checkboxInner' ?>">
                                        <span class="input-checkboxIcon"></span>
                                        <span class="input-checkboxTxt"><?php echo $value; ?></span>
                                        <?= in_array($key, $this->request->data['Recruit']['kodawari']) ? '<i class="fas fa-check check_icon"></i>' : '' ?>
                                    </span>
                                </label>
                            </div>
                    <?php
                        }
                    ?>
                  </div>

                  <h2 class="ttl-m">キーワードを入力</h2>
                  <div class="l-grid">
                    <div class="l-grid_col col-xs-12 col-md-6">
                        <div>
                            <?php echo $this->Form->input('keyword', array('type' => 'text', 'class' => 'input-txt resettext', 'placeholder' => 'ここにキーワードを入力')); ?>
                        </div>
                      <p class="u-fz-xs-12rem">例：東京駅、世田谷、◯◯◯老人ホーム</p>
                    </div>
                  </div>
                </div>
              </div>

              <div class="modal_foot">
                <div class="l-grid l-grid-ali-xs-c l-grid-juc-md-c">
                  <div class="l-grid_col col-xs-7 col-md-3">
                    <p class="modal_footResult"><span class="modal_footResultTxt" style="font-size: inherit" id="gai">該当求人数</span><span class="js-count" id="real_time_count"><?php echo $activeCount; ?></span><span style="font-size: inherit" id="ken">件</span></p>
                  </div>
                  <div class="l-grid_col col-xs-5 col-md-3">


                    <!-- desabledパターン is-disabled -->
                    <p class="modal_footSubmit">
                        <button type="button" class="btn-m btn-search2" onclick="search_before()">

                            <span class="btn_inner">
                                <div class="absearch">
									<!-- <img src="img/.svg" class="search_icon" width="40px"> -->
									<?php
										echo $this->Html->image('icon002.svg', array('alt' => 'ジョブアルク介護ロゴ', 'class' => 'search_icon', 'style'=>'width: 40px'));
									?>
                                        <p>検索する<p>
                                </div>
                            </span>
                        </button>
                    </p>
                  </div>
                </div>
              </div>
            </form>
        </div>
    </div>

	<!-- 検索モーダル -->
    
<?php echo $this->Html->scriptStart(); ?>

    // 検索フォーム - 条件クリア押下時にセットする掲載求人数
    var activeCount = '<?php echo $activeCount; ?>';
    // 条件をクリア押下フラグ
    var isClear;

    // 政令都市選択でリダイレクトした場合にaddrajaxで参照する用の市区町村データをセットしておく
    var RecruitShikutyousonVal;
    <?php
        if (isset($this->request->data['Recruit']['shikutyouson'])) {
    ?>
            RecruitShikutyousonVal = '<?php echo $this->request->data['Recruit']['shikutyouson']; ?>';
    <?php
        } else {
    ?>
            RecruitShikutyousonVal = null;
    <?php
        }
    ?>

$(document).ready(function () {

    // お気に入りに登録していない求人の気になるボタンをタップした時
    $(document).on("click", '.not_like_img', function () {
        var _this = $(this);
        if (_this.attr('id')) {
            $.ajax({
                type: 'POST',
                url: '<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'updateFavorites')); ?>',
                data: {
                        'rec_id':_this.attr('id').substr(2),
                    },
                success: function (data, textStatus, jpXHR) {
                    _this.attr('class', 'like_img');
                    _this.attr('src', '/cw/theme/Jobalc/img/like.svg');
                },
                error: function (jpXHR, textStatus, errorThrown) {
                    careerMessage('通信に失敗しました' + errorThrown, false);
                }
            });
        
            // cookie有効化           	
            //$.cookie.json = true;
            // cookie読み込み
            //var myFavorites = $.cookie("job-alc-kaigo-favorites");
            //var addFavorites = [];
            //if (myFavorites) {
                // 既存のお気に入りが存在する場合
                //addFavorites = myFavorites;
                // 重複しなければ追加を行う
                //if (!IsArrayExists(addFavorites, $(this).attr('id').substr(2))) {
                    // 先頭に追加(追加順に表示させたいため)
                    //addFavorites.unshift($(this).attr('id').substr(2));
                //}

            //} else {
                // 既存のお気に入りが存在しない場合
                // 先頭に追加(追加順に表示させたいため)
                //addFavorites.unshift($(this).attr('id').substr(2));
            //}
            // cookie保存(1000日間保存)
            //$.cookie('job-alc-kaigo-favorites', addFavorites, { expires: 1000 , path: "/" });
            //$(this).attr('class', 'like_img');
            //$(this).attr('src', '/cw/theme/Jobalc/img/like.svg');
        }
    });

    // お気に入りに登録済み求人の気になるボタンをタップした時
    $(document).on("click", '.like_img', function () {
        var _this = $(this);
        if (_this.attr('id')) {

            $.ajax({
                type: 'POST',
                url: '<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'deleteFavorite')); ?>',
                data: {
                        'rec_id':_this.attr('id').substr(2),
                    },
                success: function (data, textStatus, jpXHR) {
                    _this.attr('class', 'not_like_img');
                    _this.attr('src', '/cw/theme/Jobalc/img/not_like.svg');
                },
                error: function (jpXHR, textStatus, errorThrown) {
                    careerMessage('通信に失敗しました' + errorThrown, false);
                }
            });

            // cookie有効化           	
            //$.cookie.json = true;
            // cookie読み込み
            //var myFavorites = $.cookie("job-alc-kaigo-favorites");
            //var addFavorites = [];
            //if (myFavorites) {
                // 既存のお気に入りが存在する場合
                //addFavorites = myFavorites;
                //var target = $(this).attr('id').substr(2);
                //要素を削除する
                //addFavorites.some(function(v, i){
                    //if (v==target) addFavorites.splice(i,1);    
                //});
                // cookie更新
                //$.cookie('job-alc-kaigo-favorites', addFavorites, { expires: 1000 , path: "/" });
                //$(this).attr('class', 'not_like_img');
                //$(this).attr('src', '/cw/theme/Jobalc/img/not_like.svg');

            //}

        }
    });

     
    // 配列に特定の値が存在するか検索するメソッド
    function IsArrayExists(array, value) {
        // 配列の最後までループ
        for (var i =0, len = array.length; i < len; i++) {
            if (value == array[i]) {
            // 存在したらtrueを返す
            return true;
            }
        }
        // 存在しない場合falseを返す
        return false;
    }

    // 一覧ページ表示時に現在の検索条件の件数を取得する
    $.ajax({
        type: 'POST',
        url: '<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'searchCount')); ?>',
        data: $('#sform').serializeArray(),
        success: function (data, textStatus, jpXHR) {
            // 該当求人数更新
            $('#real_time_count').html(data);
        },
        error: function (jpXHR, textStatus, errorThrown) {
            
        }
    });
});

$(function () {
	var dd = new ADDRAjax('data[Recruit][todoufuken]', 'data[Recruit][shikutyouson]');
	dd.JSONDATA = '<?php echo $this->Html->url('/theme/Jobalc/js/addrajax/data'); ?>';
    dd.init();
    
    <?php
        if (isset($this->request->data['Recruit']['todoufuken'])) {
    ?>
        $("#RecruitTodoufuken").val("<?php echo $this->request->data['Recruit']['todoufuken']; ?>").change();
    <?php
        }
    ?>
    
});


$("#sform").change(function(){
    // 条件をクリア押下時は件数を取得しない
    if (!isClear) {
        $.ajax({
            type: 'POST',
            url: '<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'searchCount')); ?>',
            data: $(this).serializeArray(),
            success: function (data, textStatus, jpXHR) {
                // 該当求人数更新
                $('#real_time_count').html(data);
            },
            error: function (jpXHR, textStatus, errorThrown) {
                
            }
        });
    }
});

var controller_name = '<?php echo $paginatorUrlArray['controller']; ?>';

if (controller_name != null && typeof controller_name != 'undefined' && (controller_name == 'job' || controller_name == 'employment' || controller_name == 'qualification')) {
    $($('.prev_next_number').children()).each(function(index, element){

        var href = $(element).children().attr('href');
        if (typeof $(element).attr('class') != 'undefined' && ($(element).attr('class') == 'first_page')) {
            href = href + '/0';
        }
        if (typeof href != 'undefined') {
            href = href.replace('index', '0');
            $(element).children().attr('href', href);
        }
    });

    $($('.prev_next_btn').children()).each(function(index, element){

        var href = $(element).children().attr('href');
        if (typeof href != 'undefined') {
            if (href == '/cw/' + controller_name) {
                href = href + '/0';
            }
            href = href.replace('index', '0');
            $(element).children().attr('href', href);
        }
        
    });
}

function search_before() {
	
	var pref_val = $('#RecruitTodoufuken').val();
	var city_val = $('#RecruitShikutyouson').val();
	var form_values = $("#sform").serializeArray();

	var excludeds = new Array('_method', 'data[_Token][key]', 'data[Recruit][todoufuken]', 'data[Recruit][shikutyouson]', 'data[_Token][fields]');
	var submit_flg = 0;
	form_values.forEach((item) => {
		if (item['value'] != null && item['value'] != '') {
			if (excludeds.indexOf(item['name']) == -1){
				// 存在しない
				submit_flg = 1;
			}
		}
	});
	
	if (submit_flg == 0) {
		if (pref_val != null && pref_val != '') {
			var redirect_url = '/cw/area/pref';
			var prefs = <?php echo json_encode($todoufukens); ?>;
			const pref_key = Object.keys(prefs).reduce( (r, key) => { 
				return prefs[key] === pref_val ? key : r 
			}, null);
			redirect_url = redirect_url + pref_key + '/';

			if (city_val != null && city_val != '') {
				var cities = <?php echo json_encode($cities); ?>;
				const city_key = Object.keys(cities).reduce( (r, key) => { 
					return cities[key] === city_val ? key : r 
				}, null);
				redirect_url = redirect_url + 'city' + city_key + '/';
			}

			location.href = redirect_url;
			
		} else {
			$("#sform").submit();
		}
	} else {
		$("#sform").submit();
	}
	
}

<?php echo $this->Html->scriptEnd(); ?>
<?php echo $this->Html->script('modal'); ?>
<?php echo $this->Html->script('addrajax/addrajax'); ?>
<?php
// 最寄り路線・駅
$datas = array(
    'lineEmptyText' => '最寄り路線を選択',
    'stationEmptyText' => '最寄り駅を選択',
    'prefId' => '#RecruitPrefCd',
    'lineId' => '#RecruitLineCd',
    'stationId' => '#RecruitStationCd'
);
echo $this->element('interface', $datas, array('plugin' => 'Ekidata'));
?>
</body>
</html>