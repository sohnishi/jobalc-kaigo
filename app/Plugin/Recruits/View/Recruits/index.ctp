<?php
    $shokushu_options = Configure::read('shokushu');
    $kinmu_keitai_options = Configure::read('kinmu_keitai');
	$shikaku_options = Configure::read('shikaku');
	$todoufukens = Configure::read('todoufukens');
	$cities = Configure::read('cities');
	$this->Html->addCrumb('<span>介護の求人TOP</span>', '/');
?>
<!DOCTYPE html>
<html lang="ja">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="format-detection" content="telephone=no">
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
<meta property="og:title" content="介護の求人・転職なら【ジョブアルク介護】" />
<meta property="og:type" content="website" />
<meta property="og:url" content="https://job-alc.com/cw/" />
<meta property="og:image" content="https://job-alc.com/cw/img/ogp.png" />
<meta property="og:site_name" content="介護の求人・転職なら【ジョブアルク介護】" />
<meta property="og:description" content="介護の求人・転職ならジョブアルク介護にお任せください。介護福祉士、介護職、生活相談員、ケアマネから内情や条件などリアルな口コミを集めており、詳しい求人・転職情報の中から希望の条件に合わせた求人をご紹介します。" />
<meta content="介護の求人・転職ならジョブアルク介護にお任せください。介護福祉士、介護職、生活相談員、ケアマネから内情や条件などリアルな口コミを集めており、詳しい求人・転職情報の中から希望の条件に合わせた求人をご紹介します。" name="description">
<title>介護の求人・転職なら【ジョブアルク介護】</title>
<link href="<?php echo $this->Html->url('/favicon.ico'); ?>" type="image/x-icon" rel="icon">
<link href="<?php echo $this->Html->url('/favicon.ico'); ?>" type="image/x-icon" rel="shortcut icon">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
<?php 
	echo $this->Html->css('base'); 
	echo $this->Html->css('modal'); 
?>

<?php echo $this->Html->script('jquery-1.11.2.min'); ?>
<?php echo $this->Html->script('bootstrap.min'); ?>
<?php echo $this->Html->script('addrajax/addrajax'); ?>
<?php echo $this->Html->script('jquery.steps.min'); ?>
<!--[if lt IE 9]>
	<script type="text/javascript" src="theme/Jobalc/js/html5shiv.min.js"></script>
	<script type="text/javascript" src="theme/Jobalc/js/respond.min.js"></script>
	<script type="text/javascript">document.createElement('main');</script>
<![endif]-->

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TMH9GK');</script>
<!-- End Google Tag Manager -->
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "WebSite",
  "url": "https://job-alc.com/cw/",
  "potentialAction": {
    "@type": "SearchAction",
↓※こちらの記載方法の参考になります。
    "target": "https://job-alc.com/cw/recruits/results/keyword:{search_term_string}",
    "query-input": "required name=search_term_string"
  }
}
</script>

</head>
<body>
    <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TMH9GK"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <div id="body_wrapper">
        <div id="top_bar">
			<div class="container">
				<h1>介護の求人・転職なら【ジョブアルク介護】</h1>
			</div>
		</div>
		<header>
			<div class="container">
                <div class="header">
                    <div class="logo">
						<?php 
							echo $this->Html->link(
							$this->Html->image('logo.svg', array('alt' => 'ジョブアルク介護ロゴ')),
							'/',
							array('escape' => false)); 
						?>
                    </div>
                    <!--ハンバーガーメニュ-->
                    <!--参考　https://www.nxworld.net/tips/12-css-hamburger-menu-active-effect.html-->
                    <div class="header_tel pc">
                        <span class="header_tel" title="お電話でのお仕事相談・登録ができます。0120-932-929：【受付】9:30～20:00（土・日・祝も可能）">
								<?php echo $this->Html->image('top_tel.svg', array('alt' => 'お電話でのお仕事相談・登録ができます。0120-932-929：【受付】9:30～20:00（土・日・祝も可能）')); ?>
                        </span>
                    </div>
                    <div class="favlist pc">
                        <span class="favlist_all">
							<?php 
								echo $this->Html->link(
								'<count class="fav_count"><span>' . count($favoritesArray) . '</span></count>' . 
								$this->Html->image('favorites.svg', array('alt' => 'お気に入り')),
								'/favorites',
								array('escape' => false)); 
							?>
                        </span>
                    </div>
                    <div class="header_entry pc">
						<?php 
							echo $this->Html->link(
							$this->Html->image('btn001_pc.svg', array('alt' => '無料　アドバイザーに相談したい')),
							'/consultation-form/',
							array('escape' => false)); 
						?>
                    </div>
                    <div class="header_kensaku sp s_d_btn">						
						<a id='modal_search_sp_header'><img src="/cw/theme/Jobalc/img/kensaku.svg" alt="求人を検索する"></a>
                    </div>
                    <div id="nav-drawer">
                        <input id="nav-input" type="checkbox" class="nav-unshown">
                        <label id="nav-open" for="nav-input"><span></span></label>
                        <label class="nav-unshown" id="nav-close" for="nav-input"></label>
                        <div id="nav-content">
                        <div class="nav__title dfont">MENU<label class="close" for="nav-input"><span></span></label></div>
                        <ul class="menu_navi" id="menu_sp">
                            <li class="navi_sp"><a href="/cw/"><img src="/cw/theme/Jobalc/img/navi_sp01.svg" alt="ホーム"></a></li>
                            <li class="navi_sp"><a href="/cw/about/"><img src="/cw/theme/Jobalc/img/navi_sp02.svg" alt="はじめての方"></a></li>
                            <li class="navi_sp"><a href="/cw/user_voice/"><img src="/cw/theme/Jobalc/img/navi_sp03.svg" alt="ご利用者の声"></a></li>
                            <li class="navi_sp"><a href="/cw/service/"><img src="/cw/theme/Jobalc/img/navi_sp04.svg" alt="ご利用の流れ"></a></li>
                            <li class="navi_sp"><a href="/cw/guide/"><img src="/cw/theme/Jobalc/img/navi_sp05.svg" alt="転職ガイド"></a></li>
                        </ul>
                        <div class="nav__btn">
                            <p>＼具体的にお探しの方はこちらから／</p>
                            <a href="/cw/entry-form/"><img src="/cw/theme/Jobalc/img/navi_sp06.svg" alt="希望の仕事を紹介してもらう"></a>
                            <p>＼探したけどいい求人がなかった／</p>
                            <a href="/cw/private-form/"><img src="/cw/theme/Jobalc/img/navi_sp07.svg" alt="非公開求人"></a>
                        </div>
                        </div>
                    </div>
                    <div class="favlist_sp sp">
                        <span class="favlist_all">
							<?php 
								echo $this->Html->link(
                                '<count class="fav_count_sp"><span>' . count($favoritesArray) . '</span></count>' . 
								$this->Html->image('favorites_sp.svg', array('alt' => 'お気に入り')),
								'/favorites',
								array('escape' => false)); 
							?>
                        </span>
                    </div>
                </div>
			</div>
		</header>
		<nav class="nav pc">
            <div class="container">
            <!--ナビゲーション-->
            <!--参考　https://web-creators-tips.com/matome/css%E3%82%92%E4%BD%BF%E7%94%A8%E3%81%97%E3%81%9F%E7%94%BB%E5%83%8F%E3%81%AE%E3%83%AD%E3%83%BC%E3%83%AB%E3%82%AA%E3%83%BC%E3%83%90%E3%83%BC%E3%80%80%E6%A8%AA%E3%83%A1%E3%83%8B%E3%83%A5%E3%83%BC/-->
                <ul class="menu_navi" id="menu_pc">
					<li id="navi01">
						<?php
							echo $this->Html->link(
							'ジョブアルク介護',
							'/',
							array('escape' => false)); 
						?>
                    </li>
                    <li class="navi02"><a href="/cw/about/">はじめての方</a></li>
                    <li class="navi03" id="nav_modal_search_parent"><a id="modal_search">求人の検索</a></li>
                    <li class="navi04"><a href="/cw/user_voice/">ご利用者の声</a></li>
                    <li class="navi05"><a href="/cw/service/">ご利用の流れ</a></li>
                    <li class="navi06"><a href="/cw/guide/">転職ガイド</a></li>
                    <li class="navi07">
						<?php 
							echo $this->Html->link(
							'登録してみる',
							'/entry-form/',
							array('escape' => false)); 
						?>
					</li>
                </ul>
            </div>
        </nav>
        <div class="masthead">
            <div class="container">
                <div class="index_top_image pc">
                    <div class="kyujinsu">
                        <h1 class="kyujinsu_area"><span>全国</span>の公開求人数</h1>
                        <p class="kyujinsu_atai"><?php echo $activeCount; ?><span>件</span></p>
                        <p class="kyujinsu_text">非公開の求人数は含みません</p>
                    </div>
                    <div class="shibori_text">
                        <p>細かく絞り込みたい方は下記よりお進みくさだい。</p>
                        <ul>
                            <li class="s_d_btn_pc shibori_001">
								<?php 
									echo $this->Html->link(
									$this->Html->image('shibori_001.svg', array('alt' => 'エリアを絞り込む')),
									'#area_sentaku',
									array('escape' => false, 'id' => 'modal_area')); 
								?>
                            </li>
                            <li class="s_d_btn_pc shibori_002">
								<?php 
									echo $this->Html->link(
									$this->Html->image('shibori_002.svg', array('alt' => '資格を絞り込む')),
									'#shikaku_sentaku',
									array('escape' => false, 'id' => 'modal_shikaku'));
								?>
                            </li>
                            <li class="s_d_btn_pc shibori_003">
								<?php 
									echo $this->Html->link(
									$this->Html->image('shibori_003.svg', array('alt' => '施設を絞り込む')),
									'#shisetsu_sentaku',
									array('escape' => false, 'id' => 'modal_shisetsu'));
								?>
                            </li>
                            <li class="s_d_btn_pc shibori_004">
								<?php 
									echo $this->Html->link(
									$this->Html->image('shibori_004.svg', array('alt' => '条件を絞り込む')),
									'#jouken_sentaku',
									array('escape' => false, 'id' => 'modal_jouken'));
								?>
                            </li>
                        </ul>
                    </div>
                    <!-- <div class="kensaku_jyoken">
                        <p><span>【資格】</span>介護職、ケアマネ</p>
                        <p><span>【施設】</span>有料老人ホーム</p>
                        <p><span>【条件】</span>夜勤アルバイト</p>
                    </div> -->
                    <div class="hikoukai_btn">
						<?php 
							echo $this->Html->link(
							$this->Html->image('hikoukai_btn.svg', array('alt' => '完全無料　この条件に合う非公開求人を知りたい')),
							'/private-form/',
							array('escape' => false)); 
						?>
                    </div>
                </div>
                <div class="top_image sp">
					<?php 
						echo $this->Html->image('top_gazou.svg', array('class' => 'sp')); 
					?>
                </div>
            </div>
        </div>

		<div id="main">
			<div class="container">
				<div class="row">
                    <main id="contents">
                        <div class="btn sp">
							<?php 
								echo $this->Html->link(
								$this->Html->image('about.svg', array('alt' =>'はじめての方はこちらからサイトの利用の仕方をご確認ください。')),
								'/about',
								array('escape' => false, 'class' => 'btn002')); 
							?>
                            <span class="mcopy001">※あなたのご希望の条件をお伝えください。</span>
							<?php 
								echo $this->Html->link(
								$this->Html->image('btn003.svg', array('alt' =>'登録は無料です。希望の条件のお仕事を紹介してもらいたい方はこちらよりお進みください。')),
								'/entry-form/',
								array('escape' => false, 'class' => 'btn003')); 
							?>
                            <span class="mcopy001">※希望の条件を代理で交渉・確認します。</span>
						</div>
						<!-- エリアから探すの戻り用にid指定 -->
                        <div class="search_map" id="area_search">
                            <div class="h2_title">
                                <span class="icon001">
									<?php 
										echo $this->Html->image('icon001.svg'); 
									?>
                                </span>
                                <h2 class="top_h2">エリアから介護の求人を探したい</h2>
                            </div>
                            <div class="map mapall sp" id="map_modal_sp">
								<h3>エリア</h3>
								<section>
                                <div class="step1">
                                    <div class="area_map">
										<?php 
											echo $this->Html->image('japan_map.svg', array('alt' =>'日本地図を表記していることでエリアから検索できるように案内しています')); 
										?>
                                    </div>
                                    <div class="map_btn">
                                        <ul>
                                            <li id="map_hokkaido_tohoku" class="syuyou hidari">
												<a class="area_move" id="st1">
													<?php 
														echo $this->Html->image('hokkaido_tohoku.svg', array('alt' =>'北海道・東北エリア')); 
													?>
												</a>
											</li>
                                            <li id="map_kanto" class="syuyou migi">
												<a class="area_move" id="st2">
													<?php 
														echo $this->Html->image('kanto.svg', array('alt' =>'首都圏エリア')); 
													?>
												</a>
											</li>
											<li id="map_koshinetsu_hokuriku" class="img/syuyou hidari">
												<a class="area_move" id="st3">
													<?php 
														echo $this->Html->image('koshinetsu_hokuriku.svg', array('alt' =>'甲信越・北陸エリア')); 
													?>
												</a>
											</li>
                                            <li id="map_tokai" class="syuyou hidari">
												<a class="area_move" id="st4">
													<?php 
														echo $this->Html->image('tokai.svg', array('alt' =>'東海エリア')); 
													?>
												</a>
											</li>
                                            <li id="map_kansai" class="syuyou migi">
												<a class="area_move" id="st5">
													<?php 
														echo $this->Html->image('kansai.svg', array('alt' =>'関西エリア')); 
													?>
												</a>
											</li>
                                            <li id="map_chugoku_shikoku" class="syuyou hidari">
												<a class="area_move" id="st6">
													<?php 
														echo $this->Html->image('chugoku_shikoku.svg', array('alt' =>'中国・四国エリア')); 
													?>
												</a>
											</li>
                                            <li id="map_comment" class="syuyou migi">
													<?php 
														echo $this->Html->image('comment.svg', array('alt' =>'※エリアを選択すると都道府県が選べます')); 
													?>
											</li>
                                            <li id="map_kyusyu_okinawa" class="syuyou hidari">
												<a class="area_move" id="st7">
													<?php 
														echo $this->Html->image('kyusyu_okinawa.svg', array('alt' =>'※九州・沖縄エリアを選択すると都道府県が選べます')); 
													?>
												</a>
											</li>
                                        </ul>
                                    </div>
								</div>
								</section>
								<h3>北海道・東北</h3>
								<section>
                                <div class="step2">
                                    <div id="hokkaido_tohoku" class="map_inner">
                                        <ul class="pref_grp">
                                            <li class="pref hidari_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('hokkaido.svg', array('alt' =>'北海道')),
													'/area/pref1',
													array('escape' => false)); 
												?>
											</li>
                                            <li class="pref migi_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('aomori.svg', array('alt' =>'青森県')),
													'/area/pref2',
													array('escape' => false)); 
												?>
											</li>
                                            <li class="pref hidari_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('akita.svg', array('alt' =>'秋田県')),
													'/area/pref5',
													array('escape' => false)); 
												?>
											</li>
                                            <li class="pref migi_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('iwate.svg', array('alt' =>'岩手県')),
													'/area/pref3',
													array('escape' => false)); 
												?>
											</li>
                                            <li class="pref hidari_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('yamagata.svg', array('alt' =>'山形県')),
													'/area/pref6',
													array('escape' => false)); 
												?>
											</li>
                                            <li class="pref migi_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('miyagi.svg', array('alt' =>'宮城県')),
													'/area/pref4',
													array('escape' => false)); 
												?>
											</li>
                                            <li class="pref hidari_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('fukushima.svg', array('alt' =>'福島県')),
													'/area/pref7',
													array('escape' => false)); 
												?>
											</li>
                                        </ul>
                                        <div class="map_back pref">
											<a class="area_search_a">
												<?php 
													echo $this->Html->image('map_back.svg', array('alt' =>'戻る')); 
												?>
											</a>
                                        </div>
									</div>
								</div>
								</section>
								<h3>首都圏</h3>
								<section>
                                <div class="step2">
                                    <div id="syutoken" class="map_inner">
                                        <ul class="pref_grp">
                                            <li class="pref hidari_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('tochigi.svg', array('alt' =>'栃木県')),
													'/area/pref9',
													array('escape' => false)); 
												?>
											</li>
                                            <li class="pref migi_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('ibaraki.svg', array('alt' =>'茨城県')),
													'/area/pref8',
													array('escape' => false)); 
												?>
											</li>
                                            <li class="pref hidari_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('gunma.svg', array('alt' =>'群馬県')),
													'/area/pref10',
													array('escape' => false)); 
												?>
											</li>
                                            <li class="pref migi_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('chiba.svg', array('alt' =>'千葉県')),
													'/area/pref12',
													array('escape' => false)); 
												?>
											</li>
                                            <li class="pref hidari_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('saitama.svg', array('alt' =>'埼玉県')),
													'/area/pref11',
													array('escape' => false)); 
												?>
											</li>
                                            <li class="pref migi_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('tokyo.svg', array('alt' =>'東京都')),
													'/area/pref13',
													array('escape' => false)); 
												?>
											</li>
                                            <li class="pref hidari_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('yamanashi.svg', array('alt' =>'山梨県')),
													'/area/pref19',
													array('escape' => false)); 
												?>
											</li>
                                            <li class="pref migi_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('kanagawa.svg', array('alt' =>'神奈川県')),
													'/area/pref14',
													array('escape' => false)); 
												?>
											</li>
                                        </ul>
                                        <div class="map_back pref">
											<a class="area_search_a">
												<?php 
													echo $this->Html->image('map_back.svg', array('alt' =>'戻る')); 
												?>
											</a>
                                        </div>
									</div>
								</div>
								</section>
								<h3>甲信越・北陸</h3>
								<section>
								<div class="step2">
                                    <div id="koshinetsu_hokuriku" class="map_inner">
                                        <ul class="pref_grp">
                                            <li class="pref hidari_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('ishikawa.svg', array('alt' =>'石川県')),
													'/area/pref17',
													array('escape' => false)); 
												?>
											</li>
                                            <li class="pref migi_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('niigata.svg', array('alt' =>'新潟県')),
													'/area/pref15',
													array('escape' => false)); 
												?>
											</li>
                                            <li class="pref hidari_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('fukui.svg', array('alt' =>'福井県')),
													'/area/pref18',
													array('escape' => false)); 
												?>
											</li>
                                            <li class="pref migi_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('nagano.svg', array('alt' =>'長野県')),
													'/area/pref20',
													array('escape' => false)); 
												?>
											</li>
                                            <li class="pref hidari_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('toyama.svg', array('alt' =>'富山県')),
													'/area/pref16',
													array('escape' => false)); 
												?>
											</li>
                                        </ul>
                                        <div class="map_back pref">
											<a class="area_search_a">
												<?php 
													echo $this->Html->image('map_back.svg', array('alt' =>'戻る')); 
												?>
											</a>
                                        </div>
									</div>
								</div>
								</section>
								<h3>東海</h3>
								<section>
								<div class="step2">
                                    <div id="tokai" class="map_inner">
                                        <ul class="pref_grp">
                                            <li class="pref hidari_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('gifu.svg', array('alt' =>'岐阜県')),
													'/area/pref21',
													array('escape' => false)); 
												?>
											</li>
                                            <li class="pref migi_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('shizuoka.svg', array('alt' =>'静岡県')),
													'/area/pref22',
													array('escape' => false)); 
												?>
											</li>
                                            <li class="pref hidari_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('mie.svg', array('alt' =>'三重県')),
													'/area/pref24',
													array('escape' => false)); 
												?>
											</li>
                                            <li class="pref migi_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('aichi.svg', array('alt' =>'愛知県')),
													'/area/pref23',
													array('escape' => false)); 
												?>
											</li>
                                        </ul>
                                        <div class="map_back pref">
											<a class="area_search_a">
												<?php 
													echo $this->Html->image('map_back.svg', array('alt' =>'戻る')); 
												?>
											</a>
                                        </div>
									</div>
								</div>
								</section>
								<h3>近畿</h3>
								<section>
								<div class="step2">
                                    <div id="kinki" class="map_inner">
                                        <ul class="pref_grp">
                                            <li class="pref hidari_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('osaka.svg', array('alt' =>'大阪府')),
													'/area/pref27',
													array('escape' => false)); 
												?>
											</li>
                                            <li class="pref migi_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('shiga.svg', array('alt' =>'滋賀県')),
													'/area/pref25',
													array('escape' => false)); 
												?>
											</li>
                                            <li class="pref hidari_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('hyogo.svg', array('alt' =>'兵庫県')),
													'/area/pref28',
													array('escape' => false)); 
												?>
											</li>
                                            <li class="pref migi_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('kyoto.svg', array('alt' =>'京都府')),
													'/area/pref26',
													array('escape' => false)); 
												?>
											</li>
                                            <li class="pref hidari_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('wakayama.svg', array('alt' =>'和歌山県')),
													'/area/pref30',
													array('escape' => false)); 
												?>
											</li>
                                            <li class="pref migi_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('nara.svg', array('alt' =>'奈良県')),
													'/area/pref29',
													array('escape' => false)); 
												?>
											</li>
                                        </ul>
                                        <div class="map_back pref">
											<a class="area_search_a">
												<?php 
													echo $this->Html->image('map_back.svg', array('alt' =>'戻る')); 
												?>
											</a>
                                        </div>
									</div>
									</div>
									</section>
									<h3>中国・四国</h3>
									<section>
									<div class="step2">
                                    <div id="chugoku_shikoku" class="map_inner">
                                        <ul class="pref_grp">
                                            <li class="pref hidari_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('hiroshima.svg', array('alt' =>'広島県')),
													'/area/pref34',
													array('escape' => false)); 
												?>
											</li>
                                            <li class="pref migi_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('okayama.svg', array('alt' =>'岡山県')),
													'/area/pref33',
													array('escape' => false)); 
												?>
											</li>
                                            <li class="pref hidari_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('tottori.svg', array('alt' =>'鳥取県')),
													'/area/pref31',
													array('escape' => false)); 
												?>
											</li>
                                            <li class="pref migi_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('shimane.svg', array('alt' =>'島根県')),
													'/area/pref32',
													array('escape' => false)); 
												?>
											</li>
                                            <li class="pref hidari_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('yamaguchi.svg', array('alt' =>'山口県')),
													'/area/pref35',
													array('escape' => false)); 
												?>
											</li>
                                            <li class="pref migi_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('ehime.svg', array('alt' =>'愛媛県')),
													'/area/pref38',
													array('escape' => false)); 
												?>
											</li>
                                            <li class="pref hidari_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('kagawa.svg', array('alt' =>'香川県')),
													'/area/pref37',
													array('escape' => false)); 
												?>
											</li>
                                            <li class="pref migi_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('tokushima.svg', array('alt' =>'徳島県')),
													'/area/pref36',
													array('escape' => false)); 
												?>
											</li>
                                            <li class="pref hidari_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('kochi.svg', array('alt' =>'高知県')),
													'/area/pref39',
													array('escape' => false)); 
												?>
											</li>
                                        </ul>
                                        <div class="map_back pref">
											<a class="area_search_a">
												<?php 
													echo $this->Html->image('map_back.svg', array('alt' =>'戻る')); 
												?>
											</a>
                                        </div>
									</div>
									</div>
									</section>
									<h3>九州・沖縄</h3>
									<section>
									<div class="step2">
                                    <div id="kyusyu_okinawa" class="map_inner">
                                        <ul class="pref_grp">
                                            <li class="pref hidari_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('saga.svg', array('alt' =>'佐賀県')),
													'/area/pref41',
													array('escape' => false)); 
												?>
											</li>
                                            <li class="pref migi_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('fukuoka.svg', array('alt' =>'福岡県')),
													'/area/pref40',
													array('escape' => false)); 
												?>
											</li>
                                            <li class="pref hidari_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('nagasaki.svg', array('alt' =>'長崎県')),
													'/area/pref42',
													array('escape' => false)); 
												?>
											</li>
                                            <li class="pref migi_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('oita.svg', array('alt' =>'大分県')),
													'/area/pref44',
													array('escape' => false)); 
												?>
											</li>
                                            <li class="pref hidari_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('kumamoto.svg', array('alt' =>'熊本県')),
													'/area/pref43',
													array('escape' => false)); 
												?>
											</li>
                                            <li class="pref migi_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('miyazaki.svg', array('alt' =>'宮崎県')),
													'/area/pref45',
													array('escape' => false)); 
												?>
											</li>
                                            <li class="pref hidari_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('kagoshima.svg', array('alt' =>'鹿児島県')),
													'/area/pref46',
													array('escape' => false)); 
												?>
											</li>
                                            <li class="pref migi_step2">
												<?php 
													echo $this->Html->link(
													$this->Html->image('okinawa.svg', array('alt' =>'沖縄県')),
													'/area/pref47',
													array('escape' => false)); 
												?>
											</li>
                                        </ul>
                                        <div class="map_back pref">
											<a class="area_search_a">
												<?php 
													echo $this->Html->image('map_back.svg', array('alt' =>'戻る')); 
												?>
											</a>
                                        </div>
									</div>
									</div>
								</section>
                            </div>
                            <div class="map mapall pc">
                                <div class="japan_map">
                                <div class="map_pref01">
                                    <dl>
                                        <dt>北海道・東北</dt>
                                        <dd>
                                            <ul>
                                                <li class="map_btn_hokkaido"><a href="area/pref1/">北海道</a></li>
                                                <li class="map_btn_aomori"><a href="area/pref2/">青森</a></li>
                                                <li class="map_btn_akita"><a href="area/pref5/">秋田</a></li>
                                                <li class="map_btn_iwate"><a href="area/pref3/">岩手</a></li>
                                                <li class="map_btn_yamagata"><a href="area/pref6/">山形</a></li>
                                                <li class="map_btn_miyagi"><a href="area/pref4/">宮城</a></li>
                                                <li class="map_btn_fukushima"><a href="area/pref7/">福島</a></li>
                                            </ul>
                                        </dd>
                                    </dl>
                                </div>
                                <div class="map_pref02">
                                    <dl>
                                        <dt>首都圏</dt>
                                        <dd>
                                            <ul>
                                                <li class="map_btn_tochigi"><a href="area/pref9/">栃木</a></li>
                                                <li class="map_btn_ibaraki"><a href="area/pref8/">茨城</a></li>
                                                <li class="map_btn_gunma"><a href="area/pref10/">群馬</a></li>
                                                <li class="map_btn_chiba"><a href="area/pref12/">千葉</a></li>
                                                <li class="map_btn_saitama"><a href="area/pref11/">埼玉</a></li>
                                                <li class="map_btn_tokyo"><a href="area/pref13/">東京</a></li>
                                                <li class="map_btn_yamanashi"><a href="area/pref19/">山梨</a></li>
                                                <li class="map_btn_kanagawa"><a href="area/pref14/">神奈川</a></li>
                                            </ul>
                                        </dd>
                                    </dl>
                                </div>
                                <div class="map_pref03">
                                    <dl>
                                        <dt>北陸・甲信越</dt>
                                        <dd>
                                            <ul>
                                                <li class="map_btn_ishikawa"><a href="area/pref17/">石川</a></li>
                                                <li class="map_btn_niigata"><a href="area/pref15/">新潟</a></li>
                                                <li class="map_btn_fukui"><a href="area/pref18/">福井</a></li>
                                                <li class="map_btn_nagano"><a href="area/pref20/">長野</a></li>
                                                <li class="map_btn_toyama"><a href="area/pref16/">富山</a></li>
                                            </ul>
                                        </dd>
                                    </dl>
                                </div>
                                <div class="map_pref04">
                                    <dl>
                                        <dt>東海</dt>
                                        <dd>
                                            <ul>
                                                <li class="map_btn_gifu"><a href="area/pref21/">岐阜</a></li>
                                                <li class="map_btn_shizuoka"><a href="area/pref22/">静岡</a></li>
                                                <li class="map_btn_mie"><a href="area/pref24/">三重</a></li>
                                                <li class="map_btn_aichi"><a href="area/pref23/">愛知</a></li>
                                            </ul>
                                        </dd>
                                    </dl>
                                </div>
                                <div class="map_pref05">
                                    <dl>
                                        <dt>関西</dt>
                                        <dd>
                                            <ul>
                                                <li class="map_btn_osaka"><a href="area/pref27/">大阪</a></li>
                                                <li class="map_btn_shiga"><a href="area/pref25/">滋賀</a></li>
                                                <li class="map_btn_hyogo"><a href="area/pref28/">兵庫</a></li>
                                                <li class="map_btn_kyoto"><a href="area/pref26/">京都</a></li>
                                                <li class="map_btn_wakayama"><a href="area/pref30/">和歌山</a></li>
                                                <li class="map_btn_nara"><a href="area/pref29/">奈良</a></li>
                                            </ul>
                                        </dd>
                                    </dl>
                                </div>
                                <div class="map_pref06">
                                    <dl>
                                        <dt>中国・四国</dt>
                                        <dd>
                                            <ul>
                                                <li class="map_btn_hiroshima"><a href="area/pref34/">広島</a></li>
                                                <li class="map_btn_okayama"><a href="area/pref33/">岡山</a></li>
                                                <li class="map_btn_tottori"><a href="area/pref31/">鳥取</a></li>
                                                <li class="map_btn_shimane"><a href="area/pref32/">島根</a></li>
                                                <li class="map_btn_yamaguchi"><a href="area/pref35/">山口</a></li>
                                                <li class="map_btn_ehime"><a href="area/pref38/">愛媛</a></li>
                                                <li class="map_btn_kagawa"><a href="area/pref37/">香川</a></li>
                                                <li class="map_btn_tokushima"><a href="area/pref36/">徳島</a></li>
                                                <li class="map_btn_kochi"><a href="area/pref39/">高知</a></li>
                                            </ul>
                                        </dd>
                                    </dl>
                                </div>
                                <div class="map_pref07">
                                    <dl>
                                        <dt>九州・沖縄</dt>
                                        <dd>
                                            <ul>
                                                <li class="map_btn_saga"><a href="area/pref41/">佐賀</a></li>
                                                <li class="map_btn_fukuoka"><a href="area/pref40/">福岡</a></li>
                                                <li class="map_btn_nagasaki"><a href="area/pref42/">長崎</a></li>
                                                <li class="map_btn_oita"><a href="area/pref44/">大分</a></li>
                                                <li class="map_btn_kumamoto"><a href="area/pref43/">熊本</a></li>
                                                <li class="map_btn_miyazaki"><a href="area/pref45/">宮崎</a></li>
                                                <li class="map_btn_okinawa"><a href="area/pref47/">沖縄</a></li>
                                                <li class="map_btn_kagoshima"><a href="area/pref46/">鹿児島</a></li>
                                            </ul>
                                        </dd>
                                    </dl>
                                </div>
                                </div>
                                <div class="map_text_area">
                                    <h4 class="map01_text">＼市区町村で探したい!／</h4>
                                    <p class="map02_text">①まずは都道府県を選択</p>
                                    <p class="map02_text">②都道府県ページが表示</p>
                                    <p class="map02_text">③絞込で市区町村を指定</p>
                                </div>
                                <div class="map_text_area2">
                                    <h4 class="map01_text">＼より条件で探したい!／</h4>
                                    <div class="map_text_btn">
										<p class="map03_text">※都道府県ページからも検索が可能です。</p>
											<a id="modal_search_kuwashiku">
												<?php echo $this->Html->image('map_btn.svg', array('alt' =>'もっと詳しく検索したい')); ?>
											</a>
                                    </div>
									<?php 
										echo $this->Html->image('map_text.svg'); 
									?>
                                </div>
                            </div>
                        </div>
                        <div class="search_designated_cities_pc pc">
                            <div class="h2_title">
                                <span class="icon001">
									<?php 
										echo $this->Html->image('icon001.svg'); 
									?>
                                </span>
                                <h2 class="top_h2">政令指定都市から探したい</h2>
                            </div>
                            <div class="search_designated_cities_area">
                                <div class="search_designated_cities">
									<?php
										echo $this->Html->link(
											'<h3 class="search_designated_cities_btn">' . 
											$this->Html->image('sapporo_city.svg', array('alt' =>'札幌市')) . 
                                        	'</h3>',
											'/area/pref1/city1',
											array('escape' => false)
										);
									?>
                                </div>
                                <div class="search_designated_cities">
									<?php
										echo $this->Html->link(
											'<h3 class="search_designated_cities_btn">' . 
											$this->Html->image('sendai_city.svg', array('alt' =>'仙台市')) . 
                                        	'</h3>',
											'/area/pref4/city2',
											array('escape' => false)
										);
									?>
                                </div>
                                <div class="search_designated_cities">
									<?php
										echo $this->Html->link(
											'<h3 class="search_designated_cities_btn">' . 
											$this->Html->image('niigata_city.svg', array('alt' =>'新潟市')) . 
                                        	'</h3>',
											'/area/pref15/city3',
											array('escape' => false)
										);
									?>
                                </div>
                                <div class="search_designated_cities">
									<?php
										echo $this->Html->link(
											'<h3 class="search_designated_cities_btn">' . 
											$this->Html->image('chiba_city.svg', array('alt' =>'千葉市')) . 
                                        	'</h3>',
											'/area/pref12/city4',
											array('escape' => false)
										);
									?>
                                </div>
                                <div class="search_designated_cities">
									<?php
										echo $this->Html->link(
											'<h3 class="search_designated_cities_btn">' . 
											$this->Html->image('saitama_city.svg', array('alt' =>'さいたま市')) . 
                                        	'</h3>',
											'/area/pref11/city5',
											array('escape' => false)
										);
									?>
                                </div>
                                <div class="search_designated_cities">
									<?php
										echo $this->Html->link(
											'<h3 class="search_designated_cities_btn">' . 
											$this->Html->image('kawasaki_city.svg', array('alt' =>'川崎市')) . 
                                        	'</h3>',
											'/area/pref14/city6',
											array('escape' => false)
										);
									?>
                                </div>
                                <div class="search_designated_cities">
									<?php
										echo $this->Html->link(
											'<h3 class="search_designated_cities_btn">' . 
											$this->Html->image('yokohama_city.svg', array('alt' =>'横浜市')) . 
                                        	'</h3>',
											'/area/pref14/city7',
											array('escape' => false)
										);
									?>
                                </div>
                                <div class="search_designated_cities_last">
									<?php
										echo $this->Html->link(
											'<h3 class="search_designated_cities_btn">' . 
											$this->Html->image('sagamihara_city.svg', array('alt' =>'相模原市')) . 
                                        	'</h3>',
											'/area/pref14/city8',
											array('escape' => false)
										);
									?>
                                </div>
                                <div class="search_designated_cities">
									<?php
										echo $this->Html->link(
											'<h3 class="search_designated_cities_btn">' . 
											$this->Html->image('shizuoka_city.svg', array('alt' =>'静岡市')) . 
                                        	'</h3>',
											'/area/pref22/city9',
											array('escape' => false)
										);
									?>
                                </div>
                                <div class="search_designated_cities">
									<?php
										echo $this->Html->link(
											'<h3 class="search_designated_cities_btn">' . 
											$this->Html->image('hamamatsu_city.svg', array('alt' =>'浜松市')) . 
                                        	'</h3>',
											'/area/pref22/city10',
											array('escape' => false)
										);
									?>
                                </div>
                                <div class="search_designated_cities">
									<?php
										echo $this->Html->link(
											'<h3 class="search_designated_cities_btn">' . 
											$this->Html->image('nagoya_city.svg', array('alt' =>'名古屋市')) . 
                                        	'</h3>',
											'/area/pref23/city11',
											array('escape' => false)
										);
									?>
                                </div>
                                <div class="search_designated_cities">
									<?php
										echo $this->Html->link(
											'<h3 class="search_designated_cities_btn">' . 
											$this->Html->image('kyoto_city.svg', array('alt' =>'京都市')) . 
                                        	'</h3>',
											'/area/pref26/city12',
											array('escape' => false)
										);
									?>
                                </div>
                                <div class="search_designated_cities">
									<?php
										echo $this->Html->link(
											'<h3 class="search_designated_cities_btn">' . 
											$this->Html->image('osaka_city.svg', array('alt' =>'大阪市')) . 
                                        	'</h3>',
											'/area/pref27/city13',
											array('escape' => false)
										);
									?>
                                </div>
                                <div class="search_designated_cities">
									<?php
										echo $this->Html->link(
											'<h3 class="search_designated_cities_btn">' . 
											$this->Html->image('sakai_city.svg', array('alt' =>'堺市')) . 
                                        	'</h3>',
											'/area/pref27/city14',
											array('escape' => false)
										);
									?>
                                </div>
                                <div class="search_designated_cities">
									<?php
										echo $this->Html->link(
											'<h3 class="search_designated_cities_btn">' . 
											$this->Html->image('kobe_city.svg', array('alt' =>'神戸市')) . 
                                        	'</h3>',
											'/area/pref28/city15',
											array('escape' => false)
										);
									?>
                                </div>
                                <div class="search_designated_cities_last">									
									<?php
										echo $this->Html->link(
											'<h3 class="search_designated_cities_btn">' . 
											$this->Html->image('okayama_city.svg', array('alt' =>'岡山市')) . 
                                        	'</h3>',
											'/area/pref33/city16',
											array('escape' => false)
										);
									?>
                                </div>
                                <div class="search_designated_cities">
									<?php
										echo $this->Html->link(
											'<h3 class="search_designated_cities_btn">' . 
											$this->Html->image('hiroshima_city.svg', array('alt' =>'広島市')) . 
                                        	'</h3>',
											'/area/pref34/city17',
											array('escape' => false)
										);
									?>
                                </div>
                                <div class="search_designated_cities">									
									<?php
										echo $this->Html->link(
											'<h3 class="search_designated_cities_btn">' . 
											$this->Html->image('kitakyusyu_city.svg', array('alt' =>'北九州市')) . 
                                        	'</h3>',
											'/area/pref40/city18',
											array('escape' => false)
										);
									?>
                                </div>
                                <div class="search_designated_cities">
									<?php
										echo $this->Html->link(
											'<h3 class="search_designated_cities_btn">' . 
											$this->Html->image('fukuoka_city.svg', array('alt' =>'福岡市')) . 
                                        	'</h3>',
											'/area/pref40/city19',
											array('escape' => false)
										);
									?>
                                </div>
                                <div class="search_designated_cities">									
									<?php
										echo $this->Html->link(
											'<h3 class="search_designated_cities_btn">' . 
											$this->Html->image('kumamoto_city.svg', array('alt' =>'熊本市')) . 
                                        	'</h3>',
											'/area/pref43/city20',
											array('escape' => false)
										);
									?>
                                </div>
                                <div class="search_designated_cities_text">
                                    <h4 class="designated_cities01_text">＼人気の都市で探したい!／</h4>
                                    <p class="designated_cities02_text">各エリアの主要な都市になります。<br>エリアから条件を絞ると見つかりやすいです。<br>あまり絞り込み過ぎにはご注意を！</p>
                                </div>
                            </div>
                        </div>
                        <div class="search_widget">
                            <div class="h2_title">
                                <span class="icon001">
									<?php 
										echo $this->Html->image('icon002.svg'); 
									?>
                                </span>
                                <h2 class="top_h2">フリーワードから探したい</h2>
                            </div>
                            <div class="search_widget_area">
								<h3  class="free_title">＼調べたい言葉を入れて探してみましょう／</h3>
								

								<?php
									echo $this->Form->create('Recruit', array(
										'inputDefaults' => array(
											'label' => false,
											'wrapInput' => false,
											'div' => false,
											'class' => 'form-control'
										),
										'class' => 'form-inline',
										'url' => array(
											'plugin' => 'recruits',
											'controller' => 'recruits',
											'action' => 'results'
										)
									));
								?>
								<!-- <div class="form-group"> -->
									<div class="input-group">
										<?php echo $this->Form->input('keyword', array('type' => 'text', 'class' => 'input-lg')); ?>
										<div class="input-group-addon">
											<button type="button" class="search_btn" onclick="search_before()">
												<?php 
													echo $this->Html->image('search_btn.svg', array('alt' => '検索する')); 
												?>
											</button>
										</div>

									</div>
								<!-- </div> -->
								<?php echo $this->Form->end(); ?>

                                <p class="widget01_text">ページに検索ワードの記載があればヒットします。<br>複数のキーワードを入れるとヒットの確率が低いので<br>1キーワードから探してみましょう。</p>
                            </div>
                            <div class="search_widget_text">
                                <p class="widget02_text">＼探し方のポイント／</p>
                                <h4 class="widget03_text">施設名・細かい地域名・条件を入れてみる。</h4>
                                <p class="widget04_text">住宅手当・車通勤・アットホーム・給料高め・休み多めなどなど、探したいキーワードを入れてみましょう。全国のエリアから該当する求人がヒットします。地域を絞ると近くから見つけたい求人が見つかるかもしれません。</p>
                            </div>
						</div>

						<!-- 
                            20190604 sohnisi
                            全国の新着求人がない場合は、
                            枠ごと非表示とする
                        -->
                        <?php if (count($news) >= 1) {?>

                        <div class="new_job">
                            <div class="h2_title">
                                <span class="icon001">
									<?php 
										echo $this->Html->image('icon003.svg'); 
									?>
                                </span>
                                <h2 class="top_h2">全エリアの新着求人</h2>
                            </div>
                            <div class="new_job_area">
                                <ul class="horizontal-list">
									<?php 
										$newsForIndex = 0;
										foreach ($news as $key => $item) {
									?>
											<li class="item">
												<a href="/cw/recruits/detail/<?php echo $item['Recruit']['id']; ?>">
												<div class="new_job_post01">
													<h3 class="new_job_title">
														<?php
															if ($item['Facility']['FacilityShow']['groupmei_chk'] == 1) {
																echo empty($item['Facility']['group']) ? '' : $item['Facility']['group'] . '&nbsp;';
															}
															if ($item['Facility']['FacilityShow']['houjinmei_chk'] == 1) {
																echo empty($item['Facility']['corporation']) ? '' : $item['Facility']['corporation'] . '&nbsp;';
															}
															echo $item['Facility']['shisetsumei']; 
														?>
													</h3>
													<div class="details">

														<!-- 
															20190527 sohnisi
															掲載オプションが非掲載であれば、画像と値を非表示とする
															高さを保持するため、要素は出力する
														-->

														<div class="new_job_btn01">
															<span class="icon002">
																<?php 
																	if ($item['RecruitShow']['kyuyo_chk'] == 1) {
																		echo $this->Html->image('newjob_en.svg'); 
																	}
																?>
															</span>
															<p class="new_job_value01">
																<?php 
																	if ($item['RecruitShow']['kyuyo_chk'] == 1) {
																		echo nl2br($item['Recruit']['kyuyo']); 
																	}
																?>
															</p>
														</div>



														
														<div class="new_job_btn02">
															<span class="icon003">
																<?php 
																	echo $this->Html->image('newjob_map.svg'); 
																?>
															</span>
															<p class="new_job_value02">
																<?php echo $item['Facility']['shikutyouson']; ?>
															</p>
														</div>

														
														<!-- 
															20190527 sohnisi
															掲載オプションが非掲載であれば、枠自体非表示とする
														-->
														<?php if ($item['RecruitShow']['shokushu_chk']) { ?>

															<div class="new_job_btn02">
																<span class="icon003">
																	<?php 
																		echo $this->Html->image('newjob_shikaku.svg'); 
																	?>
																</span>
																<p class="new_job_value02">
																	<?php echo $shokushu_options[$item['Recruit']['shokushu']]; ?>
																</p>
															</div>

														<?php } ?>

														
                                                        <!-- 
															20190527 sohnisi
															掲載オプションが非掲載であれば、枠自体非表示とする
														-->
														<?php if ($item['RecruitShow']['kinmu_keitai_chk']) { ?>

															<div class="new_job_btn02">
																<span class="icon003">
																	<?php 
																		echo $this->Html->image('newjob_koyo.svg'); 
																	?>
																</span>
																<p class="new_job_value02">
																	<?php echo $kinmu_keitai_options[$item['Recruit']['kinmu_keitai']]; ?>
																</p>
															</div>
															
														<?php } ?>

													</div>
												</div>
												</a>
											</li>

									<?php
											$newsForIndex+=1;
										}
									?>
                                    
                                </ul>
                            </div>
                            <div class="new_job_text">
                                    <h4 class="widget05_text">近隣エリアの新着求人が見たい方</h4>
                                    <p class="widget06_text">上部にあるエリア検索をすると各エリア（都道府県・市区町村）毎に新着の順番で掲載されるようになっております。</p>
                            </div>
                        </div>
						
						<?php } ?>


                        <div class="search_facility">
                            <div class="h2_title">
                                <span class="icon001">
									<?php 
										echo $this->Html->image('icon004.svg'); 
									?>
                                </span>
                                <h2 class="top_h2">施設の種類から求人を探したい</h2>
                            </div>
                            <div class="search_facility_area sp">
                                <div class="search_facility_nyukyo">
                                    <div class="facility_btn">
										<?php 
											echo $this->Html->link(
												'<h3 class="btn_2_hidari">' . 
														$this->Html->image('tokuyo.svg', array('alt' =>'特別養護老人ホーム')) . 
												'</h3>',
												'/facility/12/',
												array('escape' => false)
											); 
										?>
                                    </div>
                                    <div class="facility_btn">
										<?php 
											echo $this->Html->link(
												'<h3 class="btn_2_migi">' . 
													$this->Html->image('yuro.svg', array('alt' =>'有料老人ホーム')) . 
												'</h3>',
												'/facility/13/',
												array('escape' => false)
											); 
										?>
                                    </div>
                                    <div class="facility_btn">
										<?php 
											echo $this->Html->link(
												'<h3 class="btn_2_hidari">' . 
													$this->Html->image('rouken.svg', array('alt' =>'老人保健施設')) . 
												'</h3>',
												'/facility/15/',
												array('escape' => false)
											); 
										?>
                                    </div>
                                    <div class="facility_btn">
										<?php 
											echo $this->Html->link(
												'<h3 class="btn_2_migi">' . 
													$this->Html->image('grphm.svg', array('alt' =>'グループホーム')) . 
												'</h3>',
												'/facility/16/',
												array('escape' => false)
											); 
										?>
                                    </div>
                                    <div class="facility_btn">
										<?php 
											echo $this->Html->link(
												'<h3 class="btn_2_hidari">' . 
													$this->Html->image('sakojyu.svg', array('alt' =>'サービス付き高齢者向け住宅')) . 
												'</h3>',
												'/facility/14/',
												array('escape' => false)
											); 
										?>
                                    </div>
                                    <div class="facility_btn">
										<?php 
											echo $this->Html->link(
												'<h3 class="btn_2_migi">' . 
													$this->Html->image('dayser.svg', array('alt' =>'デイサービス')) . 
												'</h3>',
												'/facility/20/',
												array('escape' => false)
											); 
										?>
                                    </div>
                                    <div class="facility_btn">
										<?php 
											echo $this->Html->link(
												'<h3 class="btn_2_hidari">' . 
													$this->Html->image('houkaigo.svg', array('alt' =>'訪問介護')) . 
												'</h3>',
												'/facility/19/',
												array('escape' => false)
											); 
										?>
                                    </div>
                                    <div class="facility_btn">
										<?php 
											echo $this->Html->link(
												'<h3 class="btn_2_migi">' . 
													$this->Html->image('daycare.svg', array('alt' =>'デイケア')) . 
												'</h3>',
												'/facility/21/',
												array('escape' => false)
											); 
										?>
                                    </div>
                                    <div class="facility_btn">
										<?php 
											echo $this->Html->link(
												'<h3 class="btn_2_hidari">' . 
													$this->Html->image('hp.svg', array('alt' =>'病院（看護助手）')) . 
												'</h3>',
												'/facility/17/',
												array('escape' => false)
											); 
										?>
                                    </div>
                                    <div class="facility_btn">
										<?php 
											echo $this->Html->link(
												'<h3 class="btn_2_migi">' . 
													$this->Html->image('syougai.svg', array('alt' =>'障害者施設')) . 
												'</h3>',
												'/facility/18/',
												array('escape' => false)
											); 
										?>
                                    </div>
                                </div>
                            </div>
                            <div class="search_facility_area pc">
                                <div class="search_facility_nyukyo">
                                    <h3 class="facility01">入居施設・病院【正社員（夜勤あり・日勤のみ・夜勤のみ）・バイト（夜勤のみ・日勤のみ】</h3>
                                    <div class="facility_nyukyo">
                                    <div class="facility_btn">
										<?php 
											echo $this->Html->link(
												'<h3 class="facility01_btn">' . 
													$this->Html->image('tokuyo_pc.svg', array('alt' =>'特別養護老人ホーム')) . 
												'</h3>',
												'/facility/12/',
												array('escape' => false)
											); 
										?>
                                    </div>
                                    <div class="facility_btn">
										<?php 
											echo $this->Html->link(
												'<h3 class="facility01_btn">' . 
													$this->Html->image('yuro_pc.svg', array('alt' =>'有料老人ホーム')) . 
												'</h3>',
												'/facility/13/',
												array('escape' => false)
											); 
										?>
                                    </div>
                                    <div class="facility_btn">
										<?php 
											echo $this->Html->link(
												'<h3 class="facility01_btn">' . 
													$this->Html->image('sakojyu_pc.svg', array('alt' =>'サービス付き高齢者向け住宅')) . 
												'</h3>',
												'/facility/14/',
												array('escape' => false)
											); 
										?>
                                    </div>
                                    <div class="facility_btn">
										<?php 
											echo $this->Html->link(
												'<h3 class="facility01_btn">' . 
													$this->Html->image('rouken_pc.svg', array('alt' =>'老人保健施設')) . 
												'</h3>',
												'/facility/15/',
												array('escape' => false)
											); 
										?>
                                    </div>
                                    <div class="facility_btn">
										<?php 
											echo $this->Html->link(
												'<h3 class="facility01_btn">' . 
													$this->Html->image('grphm_pc.svg', array('alt' =>'グループホーム')) . 
												'</h3>',
												'/facility/16/',
												array('escape' => false)
											); 
										?>
                                    </div>
                                    <div class="facility_btn">
										<?php 
											echo $this->Html->link(
												'<h3 class="facility01_btn">' . 
													$this->Html->image('hp_pc.svg', array('alt' =>'病院（看護助手）')) . 
												'</h3>',
												'/facility/17/',
												array('escape' => false)
											); 
										?>
                                    </div>
                                    <div class="facility_btn">
										<?php 
											echo $this->Html->link(
												'<h3 class="facility01_btn">' . 
													$this->Html->image('syougai_pc.svg', array('alt' =>'障害者施設')) . 
												'</h3>',
												'/facility/18/',
												array('escape' => false)
											); 
										?>
                                    </div>
                                    </div>
                                </div>
                                <div class="search_facility_houmon">
                                    <h3 class="facility02">訪問【正社員・バイト（日勤のみ）】</h3>
                                    <div class="facility_houmon">
                                    <div class="facility_btn">										
										<?php 
											echo $this->Html->link(
												'<h3 class="facility03_btn">' . 
													$this->Html->image('houkaigo_pc.svg', array('alt' =>'訪問介護')) . 
												'</h3>',
												'/facility/19/',
												array('escape' => false)
											); 
										?>
                                    </div>
                                    </div>
                                </div>
                                <div class="search_facility_tsusho">
                                    <h3 class="facility03">通所施設【正社員・バイト（日勤のみ）】</h3>
                                    <div class="facility_tsusho">
                                    <div class="facility_btn">										
										<?php 
											echo $this->Html->link(
												'<h3 class="facility04_btn">' . 
													$this->Html->image('dayser_pc.svg', array('alt' =>'デイサービス')) . 
												'</h3>',
												'/facility/20/',
												array('escape' => false)
											); 
										?>
                                    </div>
                                    <div class="facility_btn">										
										<?php 
											echo $this->Html->link(
												'<h3 class="facility05_btn">' . 
													$this->Html->image('daycare_pc.svg', array('alt' =>'デイケア')) . 
												'</h3>',
												'/facility/21/',
												array('escape' => false)
											); 
										?>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div class="facility_text">
                                <h4 class="widget05_text">近隣の施設種類から求人を探したい方</h4>
                                <p class="widget06_text">上記の施設種類を選択した後に次の画面でエリアを指定することできますので、近隣の求人を探すことが可能です。</p>
                            </div>
                        </div>
                        <div class="search_popularity">
                            <div class="h2_title">
                                <span class="icon001">
									<?php 
										echo $this->Html->image('icon005.svg'); 
									?>
                                </span>
                                <h2 class="top_h2">人気・オススメの特集について</h2>
                            </div>
                            <div class="search_popularity_area">
                                <div class="popularity_btn fst">									
									<?php 
										echo $this->Html->link(
											'<h3 class="btn_2_hidari">' . 
											$this->Html->image('kyuryo.svg', array('alt' =>'給料高め')) . 
											'</h3>',
											'/feature/8',
											array('escape' => false)
										); 
									?>
                                </div>
                                <div class="popularity_btn">
									<?php 
										echo $this->Html->link(
											'<h3 class="btn_2_migi">' . 
											$this->Html->image('yasumi.svg', array('alt' =>'休み多め')) . 
											'</h3>',
											'/feature/9',
											array('escape' => false)
										); 
									?>
                                </div>
                                <div class="popularity_btn">									
									<?php 
										echo $this->Html->link(
											'<h3 class="btn_2_hidari">' . 
											$this->Html->image('car.svg', array('alt' =>'車通勤OK')) . 
											'</h3>',
											'/feature/1',
											array('escape' => false)
										); 
									?>
                                </div>
                                <div class="popularity_btn">									
									<?php 
										echo $this->Html->link(
											'<h3 class="btn_2_migi">' . 
											$this->Html->image('nikkin.svg', array('alt' =>'日勤のみ')) . 
											'</h3>',
											'/feature/12',
											array('escape' => false)
										); 
									?>
                                </div>
                                <div class="popularity_btn fst">
									<?php 
										echo $this->Html->link(
											'<h3 class="btn_2_hidari">' . 
											$this->Html->image('fukki.svg', array('alt' =>'ブランク復帰')) . 
											'</h3>',
											'/feature/2',
											array('escape' => false)
										); 
									?>
                                </div>
                                <div class="popularity_btn">
									<?php 
										echo $this->Html->link(
											'<h3 class="btn_2_migi">' . 
											$this->Html->image('senjyu.svg', array('alt' =>'夜勤専従')) . 
											'</h3>',
											'/feature/13',
											array('escape' => false)
										); 
									?>
                                </div>
                                <div class="popularity_btn">
									<?php 
										echo $this->Html->link(
											'<h3 class="btn_2_hidari">' . 
											$this->Html->image('mikeiken.svg', array('alt' =>'未経験歓迎')) . 
											'</h3>',
											'/feature/3',
											array('escape' => false)
										); 
									?>
                                </div>
                                <div class="popularity_btn">									
									<?php 
										echo $this->Html->link(
											'<h3 class="btn_2_migi">' . 
											$this->Html->image('yakinba.svg', array('alt' =>'夜勤バイト')) . 
											'</h3>',
											'/feature/14',
											array('escape' => false)
										); 
									?>
                                </div>
                                <div class="popularity_btn fst">									
									<?php 
										echo $this->Html->link(
											'<h3 class="btn_2_hidari">' . 
											$this->Html->image('kensyu.svg', array('alt' =>'研修支援あり')) . 
											'</h3>',
											'/feature/7',
											array('escape' => false)
										); 
									?>
                                </div>
                                <div class="popularity_btn">									
									<?php 
										echo $this->Html->link(
											'<h3 class="btn_2_migi">' . 
											$this->Html->image('ryou.svg', array('alt' =>'寮住宅補助あり')) . 
											'</h3>',
											'/feature/5',
											array('escape' => false)
										); 
									?>
                                </div>
                            </div>
                            <div class="popularity_text">
                                <h4 class="widget05_text">近隣から特集の求人を探したい方</h4>
                                <p class="widget06_text">上記の施設種類を選択した後に次の画面でエリアを指定することできますので、近隣の求人を探すことが可能です。</p>
                            </div>
                        </div>
                        <div class="service_user_voice">
                            <div class="service">
                            <div class="h2_title title2">
                                <span class="icon001">
									<?php 
										echo $this->Html->image('icon005.svg'); 
									?>
                                </span>
                                <h2 class="top_h2 narabi">ご利用の流れ</h2>
                            </div>
                            <div class="service_flow_area">
                                <div class="service_all">
                                <div class="service_flow">
                                    <div class="service_text">
                                        <span class="service_no">1</span>
                                        <span class="service_midashi">登録</span>
                                        <span class="service_memo">※カンタン入力でご登録できます。</span>
                                    </div>
                                </div>
                                <div class="service_flow">
                                    <div class="service_text">
                                        <span class="service_no">2</span>
                                        <span class="service_midashi">ヒアリング</span>
                                        <span class="service_memo">※悩みや条件を伝えてください。</span>
                                    </div>
                                </div>
                                <div class="service_flow">
                                    <div class="service_text">
                                        <span class="service_no">3</span>
                                        <span class="service_midashi">求人を紹介</span>
                                        <span class="service_memo">※条件にあった求人を紹介します。</span>
                                    </div>
                                </div>
                                <div class="service_flow">
                                    <div class="service_text">
                                        <span class="service_no">4</span>
                                        <span class="service_midashi">面接</span>
                                        <span class="service_memo">※内定率アップの面接対策。</span>
                                    </div>
                                </div>
                                <div class="service_flow">
                                    <div class="service_text">
                                        <span class="service_no">5</span>
                                        <span class="service_midashi">内定・調整</span>
                                        <span class="service_memo">※交渉や調整を行います。</span>
                                    </div>
                                </div>
                                <div class="service_flow">
                                    <div class="service_text">
                                        <span class="service_no">6</span>
                                        <span class="service_midashi">入職</span>
                                        <span class="service_memo">※入職後もサポートします。</span>
                                    </div>
                                </div>
                                </div>                             
                            </div>
                            </div>
                            <div class="user_voice">
                                <div class="user_voice_area">
                                    <div class="h2_title title2">
                                        <span class="icon001">
											<?php 
												echo $this->Html->image('icon006.svg'); 
											?>
                                        </span>
                                        <h2 class="top_h2 narabi">ご利用者の声</h2>
                                    </div>
                                    <div class="user_voice_all">
                                    <p class="widget03_text">生の声を聞きたい！メリットもデメリットも。</p>
                                    <div class="user_voice_btn">
										<?php 
											echo $this->Html->link(
											$this->Html->image('user_voice.svg', array('alt' =>'ジョブアルクって実際にどうなの？利用者のお言葉をお伝えします。')),
											'/user_voice',
											array('escape' => false)); 
										?>
                                    </div>
                                    <p class="widget07_text">サービスの改善してほしいポイントがあれば
                                    <br>教えてください。みなさまの声を待っています。</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--登録フォーム-->
                        <div class="private">
                            <div class="private_area">
                                <div class="h2_title">
                                    <span class="icon001">
										<?php 
											echo $this->Html->image('icon007.svg'); 
										?>
                                    </span>
                                    <h2 class="top_h2">いい求人が見つからなかった方</h2>
                                </div>
                                <div class="private_text">
                                    <p class="widget02_text">公開している求人だけではない？</p>
                                    <h4 class="widget03_text">非公開を希望されるケースは少なくありません。</h4>
                                    <p class="widget04_text">現在、働いている方への影響を避けたいなど、管理職の求人など、さまざま事情により公開を避けたい場合もあります。非公開求人として管理しているので、希望の条件などをお伝えし、相談してみましょう。</p>
                                </div>
                                <div class="private_btn">
                                    <a href="/cw/private-form/">
                                        <span>
											<?php 
												echo $this->Html->image('private.svg', array('alt' =>'登録者限定 非公開求人 ※求人数や募集期間に限りがございます。')); 
											?>
										</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </main>
                    <aside id="sidebar" class="pc">
						<div class="sidebox hidden-xs">
							<div class="support_widget">
                                <h4>
									<?php 
                                        echo $this->Html->link(
										$this->Html->image('touroku.svg', array('alt' =>'登録 希望の条件のお仕事を紹介してもらう。')),
                                        '/entry-form/',
                                        array('escape' => false));
                                    ?>
                                </h4>
                            </div>
                            <div class="support_widget">
                                <h4>
									<?php 
										echo $this->Html->link(
										$this->Html->image('start.svg', array('alt' =>'はじめての方はこちらから')),
										'/about',
										array('escape' => false)); 
									?>
                                </h4>
                            </div>
                            <div class="list_widget">
                                <h4>施設の種類から探す</h4>
                                <ul>
									
									<?php 
										$shisetsukeitaiFirstsForIndex = 0;
										foreach ($shisetsukeitaiFirsts as $key => $value) {
											if ($shisetsukeitaiFirstsForIndex == 0) {
												echo '<li class="li_fst">';
											} else {
												echo '<li>';
											}
									?>
											
												<?php 
													echo $this->Html->link(
														$value,
														'/facility/' . $key,
														array('escape' => false)
													); 
													echo $this->Html->image('yajirushi.svg'); 
												?>
											</li>
									<?php
											$shisetsukeitaiFirstsForIndex+=1;
										}
									?>
                                    
                                </ul>
                            </div>
                            <div class="list_widget">
                                <h4>職種から探す</h4>
                                <ul>
									<?php 
										$shokushuOptionsForIndex = 0;
										foreach ($shokushu_options as $key => $value) {
											if ($shokushuOptionsForIndex == 0) {
												echo '<li class="li_fst">';
											} else {
												echo '<li>';
											}
									?>
											
												<?php 
													echo $this->Html->link(
														$value,
														'/job/' . $key,
														array('escape' => false)
													); 
													echo $this->Html->image('yajirushi.svg'); 
												?>
											</li>
									<?php
											$shokushuOptionsForIndex+=1;
										}
									?>
                                </ul>
                            </div>
                            <div class="list_widget">
                                <h4>勤務形態から探す</h4>
                                <ul>

									<?php 
										$kinmuKeitaiOptionsForIndex = 0;
										foreach ($kinmu_keitai_options as $key => $value) {
											if ($kinmuKeitaiOptionsForIndex == 0) {
												echo '<li class="li_fst">';
											} else {
												echo '<li>';
											}
									?>
											
												<?php 
													echo $this->Html->link(
														$value,
														'/employment/' . $key,
														array('escape' => false)
													); 
													echo $this->Html->image('yajirushi.svg'); 
												?>
											</li>
									<?php
											$kinmuKeitaiOptionsForIndex+=1;
										}
									?>
                                    
                                </ul>
                            </div>
                            <div class="list_widget">
                                <h4>資格から探す</h4>
                                <ul>

									<?php 
										$shikakuOptionsForIndex = 0;
										foreach ($shikaku_options as $key => $value) {
											if ($shikakuOptionsForIndex == 0) {
												echo '<li class="li_fst">';
											} else {
												echo '<li>';
											}
									?>
											
												<?php 
													echo $this->Html->link(
														$value,
														'/qualification/' . $key,
														array('escape' => false)
													); 
													echo $this->Html->image('yajirushi.svg'); 
												?>
											</li>
									<?php
											$shikakuOptionsForIndex+=1;
										}
									?>
                                </ul>
                            </div>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
		
        <footer class="footer_sp sp">
            <div class="container">
				<div class="row">
                    <div class="footer">
                        <div class="footer_Menus">
                            <ul class="footer_menu">
                                <li class="footerMenu_list">
									<?php 
                                        echo $this->Html->link(
										$this->Html->image('1.svg', array('alt' => '無料登録')),
                                        '/entry-form/',
                                        array('escape' => false));
                                    ?>
								</li>
                                <li class="footerMenu_list">
									<?php 
										echo $this->Html->link(
										$this->Html->image('2.svg', array('alt' => '個人情報')),
										'/privacy',
										array('escape' => false)); 
									?>
								</li>
                                <li class="footerMenu_list">
									<?php 
										echo $this->Html->link(
										$this->Html->image('3.svg', array('alt' => '会社概要')),
										'/corporation',
										array('escape' => false)); 
									?>
								</li>
								<li class="footerMenu_list" id="modal_search_footer_parent_sp">
                                    <a class='modal_search_footer' id='modal_search_footer_sp'><?php echo $this->Html->image('4.svg', array('alt' => '求人検索')) ?></a>
								</li>
                                <li class="footerMenu_list_last">
									<?php 
										echo $this->Html->link(
										$this->Html->image('5.svg', array('alt' => '利用規約')),
										'/terms',
										array('escape' => false)); 
									?>
								</li>
                            </ul>
                        </div>
                    </div>
				</div>
				<div class="footer-bottom text-center">
					<address class="address"> Copyright © 2019 ALC Co.Ltd All Rights Reserved.</address>
				</div>
			</div>
		</footer>
        <footer class="footer_pc pc">
            <div class="container">
				<div class="row">
                    <div class="footer">
                        <div class="footer_Menus">
                            <ul class="footer_menu">
                                <li class="footerMenu_title">メニュー</li>
                                <li class="footerMenu_list">
								<a href="">ホーム</a></li>
                                <li class="footerMenu_list"><a href="/cw/about/">はじめの方</a></li>
                                <li class="footerMenu_list" id="modal_search_footer_parent"><a class='modal_search_footer' id='modal_search_footer_pc'>求人検索</a></li>
                                <li class="footerMenu_list"><a href="/cw/user_voice/">ご利用者の声</a></li>
                                <li class="footerMenu_list"><a href="/cw/service/">ご利用の流れ</a></li>
                            </ul>
                            <ul class="footer_menu">
                                <li class="footerMenu_title">転職コラム</li>
                                <li class="footerMenu_list"><a href="/">転職コラムTOP</a></li>
                                <li class="footerMenu_list"><a href="/">求人探しのポイント</a></li>
                                <li class="footerMenu_list"><a href="/">履歴書の書き方</a></li>
                                <li class="footerMenu_list"><a href="/">面接の対策</a></li>
                                <li class="footerMenu_list"><a href="/">入職への心構え</a></li>
                            </ul>
                            <ul class="footer_menu">
                                <li class="footerMenu_title">運用情報</li>
                                <li class="footerMenu_list"><a href="/cw/corporation/">会社概要</a></li>
                                <li class="footerMenu_list"><a href="/cw/terms/">利用規約</a></li>
                                <li class="footerMenu_list"><a href="/cw/privacy/">プライバシーポリシー</a></li>
                            </ul>
                            <ul class="footer_menu">
                                <li class="footerMenu_title">アルクの運営サイト</li>
                                <li class="footerMenu_list"><a href="/ns/">ジョブアルク看護師</a></li>
                                <li class="footerMenu_list"><a href="/fm/">ジョブアルク薬剤師</a></li>
                                <li class="footerMenu_list"><a href="/rh/">ジョブアルクPOS</a></li>
                            </ul>
                        </div>
                    </div>
					<div class="footer_tel">
						<div class="information">
							<?php 
								echo $this->Html->image('top_tel.svg', array('alt' => 'お電話でのお仕事相談・登録ができます。0120-932-929：【受付】9:30～20:00（土・日・祝も可能）')); 
							?>
                        </div>
                    </div>
					<div class="footer_btn">
						<div class="footer_entry">
							<?php 
								echo $this->Html->link(
								$this->Html->image('btn001_pc.svg', array('alt' => '無料　アドバイザーに相談したい')),
								'/consultation-form/',
								array('escape' => false)); 
							?>
                        </div>
                    </div>
				</div>
				<div class="footer-bottom text-center">
					<div>厚生大臣許可 27-ュ-201641</div>
					<address> Copyright © 2019 ALC Co.Ltd All Rights Reserved.</address>
				</div>
			</div>
		</footer>
	</div>






	<!-- 検索モーダル -->

	<div class="modal_content">
        <div class="modal_window">
			<!-- <form action="/r/search.php" method="post" class="modal modal-foot is-none" data-modal-name="search" id="sform"> -->
			<?php
				echo $this->Form->create($model, array(
					'inputDefaults' => array(
						'label' => false,
						'inputWrap' => false,
						'div' => false,
					),
					'class' => 'form-control modal modal-foot is-none',
					'data-modal-name' => 'search',
					'id' => 'sform',
					// 'url' => '/recruits/results/'
					'url' => array_merge(array('action' => 'results'), $this->params['pass'])
				));
			?>
			<!-- <input type="hidden" name="formredirect" value="1"> -->
			<div class="modal_close_wrapper">
              <div class="modal_close js-modal" data-target-modal="search">
                <i class="fas fa-times close_icon"></i>
			  </div>
			</div>
              <div class="modal_inner">
                <div class="modal_head">
                  <h1 class="modal_headTit">求人検索</h1>
                  <p class="modal_headLink"><a id="restbtn">条件をクリア</a></p>
                  <!-- <p class="modal_headLink"><a href="/search-list/">履歴から検索</a></p> -->
                </div>
                <div class="modal_body">
                  <h2 class="ttl-m" id="area_sentaku">エリアを選択</h2>

                  <div class="l-grid l-grid-margin-xs-3 l-grid-margin-md-5 u-mb-xs-15">
                    <div class="l-grid_col col-xs-4 col-md-2">
                      <label class="input-radio js-tab" data-tab-group="area" data-tab-parent-name="1">
                        <input type="radio" id="select_area" checked="">
                        <span class="input-radioInner" id = "chiiki_select">
                          <span class="input-radioIcon"></span>
                          <span class="input-radioTxt">地域</span>
                        </span>
                      </label>
                    </div>
                    <div class="l-grid_col col-xs-5 col-md-2">
                      <label class="input-radio js-tab" data-tab-group="area" data-tab-parent-name="2">
                        <input type="radio" id="select_ln">
                        <span class="input-radioInner" id = "rosen_select">
                          <span class="input-radioIcon"></span>
                          <span class="input-radioTxt">路線・駅</span>
                        </span>
                      </label>
                    </div>
                  </div>

                  <div class="" data-tab-group="area" data-tab-child-name="1" id="chiiki_div">
                    <div class="l-grid u-mb-xs-30">

                      <!-- 地域 選択時に表示 -->
                      <div class="l-grid_col col-xs-12 col-md-4 u-mb-xs-10">
                        <label class="select js-selectToggle" data-target-toggle="chiiki_region" id="chiiki_region">
							<label for="pr" class="chdown"><i class="fas fa-angle-down chdown_icon"></i></label>
							<?php echo $this->Form->select('todoufuken', null, array('div' => false, 'empty' => '都道府県を選択', 'class' => 'reset prlist areapr')); ?>
                        </label>
                      </div>
                      <!-- /地域 選択時に表示 -->

                      <!-- 地域 → 都道府県 選択時に表示 -->
                      <div class="l-grid_col col-xs-12 col-md-4" data-toggle-name="chiiki_region">
                        <div class="js-checkBoxToggle" data-target-toggle="shikuchoson">
                            <label class="select js-selectToggle" data-target-toggle="chiiki_region">
							<label for="pr" class="chdown"><i class="fas fa-angle-down chdown_icon"></i></label>
							<?php echo $this->Form->select('shikutyouson', null, array('div' => false, 'empty' => '市区町村を選択', 'class' => 'reset prlist areapr')); ?>
                              <!-- <select name="pr" class="reset prlist areapr">
                                <option value="">市区町村を選択</option>
                              </select> -->
                            </label>
                        </div>
                      </div>
                      <!-- /地域 → 都道府県 選択時に表示 -->

                    </div>
                  </div>


                  <div class=" u-d-n" data-tab-group="area" data-tab-child-name="2" id="rosen_div">
                    <div class="l-grid u-mb-xs-30">
                      <!-- 路線 選択時に表示 -->

                      <div class="l-grid_col col-xs-12 col-md-4 u-mb-xs-10">
                        <label class="select js-selectToggle" data-target-toggle="rosen_region" id="rosen_region">
							<label for="pr" class="chdown"><i class="fas fa-angle-down chdown_icon"></i></label>
							<?php echo $this->Form->select('pref_cd', $prefs, array('div' => false, 'empty' => '都道府県を選択', 'class' => 'reset prlist areapr')); ?>
                        </label>
                      </div>
                      <!-- /路線 選択時に表示 -->

                      <!-- 路線 → 都道府県 選択時に表示 -->
                      <div class="l-grid_col col-xs-12 col-md-4 lnclick" data-toggle-name="rosen_region">
                        <div class="js-checkBoxToggle" data-target-toggle="rosen">
                            <label class="select js-selectToggle select_mbt" data-target-toggle="rosen_region">
							<label for="pr" class="chdown"><i class="fas fa-angle-down chdown_icon"></i></label>
							<?php echo $this->Form->select('line_cd', $lines, array('div' => false, 'empty' => '最寄り路線を選択', 'class' => 'reset prlist areapr')); ?>
                              <!-- <select name="pr" class="reset prlist lnpr">
                                <option value="">路線を選択</option>
							 </select> -->
						</label>
                        </div>
                      </div>
                      <!-- /路線 → 都道府県 選択時に表示 -->

                      <!-- 路線 → 都道府県 → 路線 選択時に表示 -->
                      <div class="l-grid_col col-xs-12 col-md-4 lnclick" data-toggle-name="rosen_region">
                        <div class="js-checkBoxToggle" data-target-toggle="rosen">
                            <label class="select js-selectToggle" data-target-toggle="rosen_region">
							<label for="pr" class="chdown"><i class="fas fa-angle-down chdown_icon"></i></label>
							<?php echo $this->Form->select('station_cd', $stations, array('div' => false, 'empty' => '最寄り駅を選択', 'class' => 'reset prlist areapr')); ?>
                              <!-- <select name="pr" class="reset prlist lnpr">
                                <option value="">駅を選択</option>
							  </select> -->
						</label>
                        </div>
                      </div>
                      <!-- 路線 → 都道府県 → 路線 選択時に表示 -->

                    </div>
				  </div>
				  


				  <h2 class="ttl-m">職種を選択<span class="ttl-m_sub">（複数選択可）</span></h2>
                  <div class="l-grid l-grid-margin-xs-2 l-grid-margin-md-5 u-mb-xs-25">

                    <?php 
                        foreach ($shokushu_options as $key => $value) {
                    ?>
                            
                            <div class="l-grid_col col-xs-6 col-md-2 u-mb-xs-5 u-mb-md-10">
                            <label class="input-checkbox">
                                <?php
                                    echo $this->Form->input('Recruit.shokushu.' . $key, array('type' => 'checkbox', 'class' => 'reset', 'value' => $key, 'hiddenField' => false));
                                ?>
                                <span class="input-checkboxInner">
                                    <span class="input-checkboxIcon"></span>
                                    <span class="input-checkboxTxt"><?php echo $value; ?></span>
                                </span>
                            </label>
                            </div>
                    <?php
                        }
                    ?>

                  </div>


                  <h2 class="ttl-m">勤務形態を選択<span class="ttl-m_sub">（複数選択可）</span></h2>
                  <div class="l-grid l-grid-margin-xs-2 l-grid-margin-md-5 u-mb-xs-25">

                    <?php 
                        foreach ($kinmu_keitai_options as $key => $value) {
                    ?>
                            
                            <div class="l-grid_col col-xs-6 col-md-2 u-mb-xs-5 u-mb-md-10">
                            <label class="input-checkbox">
                                <?php
                                    echo $this->Form->input('Recruit.kinmu_keitai.' . $key, array('type' => 'checkbox', 'class' => 'reset', 'value' => $key, 'hiddenField' => false));
                                ?>
                                <span class="input-checkboxInner">
                                    <span class="input-checkboxIcon"></span>
                                    <span class="input-checkboxTxt"><?php echo $value; ?></span>
                                </span>
                            </label>
                            </div>
                    <?php
                        }
                    ?>

				  </div>
				  

                  <h2 class="ttl-m" id="shikaku_sentaku">資格を選択<span class="ttl-m_sub">（複数選択可）</span></h2>
                  <div class="l-grid l-grid-margin-xs-2 l-grid-margin-md-5 u-mb-xs-25">

                  	<?php 
                        foreach ($shikaku_options as $key => $value) {
                    ?>
                            
                            <div class="l-grid_col col-xs-6 col-md-2 u-mb-xs-5 u-mb-md-10">
                            <label class="input-checkbox">
                                <?php 
									echo $this->Form->input('Recruit.shikaku.' . $key, array('type' => 'checkbox', 'class' => 'reset', 'value' => $key, 'hiddenField' => false));
                                ?>
                                <span class="input-checkboxInner">
                                    <span class="input-checkboxIcon"></span>
                                    <span class="input-checkboxTxt"><?php echo $value; ?></span>
                                </span>
                            </label>
                            </div>
                    <?php
                        }
                    ?>

                    
                  </div>

                  <h2 class="ttl-m" id="shisetsu_sentaku">施設業態を選択<span class="ttl-m_sub">（複数選択可）</span></h2>
                  <div class="l-grid l-grid-margin-xs-2 l-grid-margin-md-5 u-mb-xs-25">
                    

                    <?php 
                        foreach ($shisetsukeitaiFirsts as $key => $value) {
                    ?>
                            <div class="l-grid_col col-xs-6 col-md-2 u-mb-xs-5 u-mb-md-10">
                                <label class="input-checkbox">
                                    <?php 
										echo $this->Form->input('Recruit.shisetsukeitai_first.' . $key, array('type' => 'checkbox', 'class' => 'reset', 'value' => $key, 'hiddenField' => false));
                                    ?>
                                    <span class="input-checkboxInner">
                                        <span class="input-checkboxIcon"></span>
                                        <span class="input-checkboxTxt"><?php echo $value; ?></span>
                                    </span>
                                </label>
                            </div>
                    <?php
                        }
                    ?>
                  
                  </div>

                  <h2 class="ttl-m" id="jouken_sentaku">こだわり条件を選択<span class="ttl-m_sub">（複数選択可）</span></h2>
                  <div class="l-grid l-grid-margin-xs-2 l-grid-margin-md-5 u-mb-xs-25">

                    <?php 
                        foreach ($kodawaris as $key => $value) {
                    ?>
                            <div class="l-grid_col col-xs-6 col-md-2 u-mb-xs-5 u-mb-md-10">
                                <label class="input-checkbox">
                                    <?php 
										echo $this->Form->input('Recruit.kodawari.' . $key, array('type' => 'checkbox', 'class' => 'reset', 'value' => $key, 'hiddenField' => false));
                                    ?>
                                    <span class="input-checkboxInner">
                                        <span class="input-checkboxIcon"></span>
                                        <span class="input-checkboxTxt"><?php echo $value; ?></span>
                                    </span>
                                </label>
                            </div>
                    <?php
                        }
                    ?>
                  </div>

                  <h2 class="ttl-m">キーワードを入力</h2>
                  <div class="l-grid">
                    <div class="l-grid_col col-xs-12 col-md-6">
                        <div>
                            <?php echo $this->Form->input('keyword', array('type' => 'text', 'class' => 'input-txt resettext', 'placeholder' => 'ここにキーワードを入力')); ?>
                        </div>
                      <p class="u-fz-xs-12rem">例：東京駅、世田谷、◯◯◯老人ホーム</p>
                    </div>
                  </div>
                </div>
              </div>

              <div class="modal_foot">
                <div class="l-grid l-grid-ali-xs-c l-grid-juc-md-c">
                  <div class="l-grid_col col-xs-7 col-md-3">
                    <p class="modal_footResult"><span class="modal_footResultTxt" style="font-size: inherit" id="gai">該当求人数</span><span class="js-count" id="real_time_count"><?php echo $activeCount; ?></span><span style="font-size: inherit" id="ken">件</span></p>
                  </div>
                  <div class="l-grid_col col-xs-5 col-md-3">


                    <!-- desabledパターン is-disabled -->
                    <p class="modal_footSubmit">
					
                        <button type="button" class="btn-m btn-search2" onclick="search_before()">
                            <span class="btn_inner">
                                <div class="absearch">
									<?php
										echo $this->Html->image('icon002.svg', array('alt' => 'ジョブアルク介護ロゴ', 'class' => 'search_icon', 'style'=>'width: 40px'));
									?>
                                        <p>検索する<p>
                                </div>
                            </span>
                        </button>
                    </p>
                  </div>
                </div>
			  </div>
			  
			  <?php echo $this->Form->end(); ?>

        </div>
    </div>

	<!-- 検索モーダル -->
<?php echo $this->Html->scriptStart(); ?>

// 検索フォーム - 条件クリア押下時にセットする掲載求人数
var activeCount = '<?php echo $activeCount; ?>';
// 条件をクリア押下フラグ
var isClear;

// results.ctpでしか使用しないが、同じaddrajax.jsを参照しているため、変数のみ宣言しておく
var RecruitShikutyousonVal;
	
$(function () {
	var dd = new ADDRAjax('data[Recruit][todoufuken]', 'data[Recruit][shikutyouson]');
	dd.JSONDATA = '<?php echo $this->Html->url('/theme/Jobalc/js/addrajax/data'); ?>';
	dd.init();
});


$("#sform").change(function(){
	// 条件をクリア押下時は件数を取得しない
    if (!isClear) {
		$.ajax({
			type: 'POST',
			url: '<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'searchCount')); ?>',
			data: $(this).serializeArray(),
			success: function (data, textStatus, jpXHR) {
				// 該当求人数更新
				$('#real_time_count').html(data);
			},
			error: function (jpXHR, textStatus, errorThrown) {
				
			}
		});
	}
});

$("#map_modal_sp").steps({
	headerTag: "h3",
	bodyTag: "section",
	transitionEffect: "fade",
	autoFocus: true, 
	titleTemplate: '<span class="number">#index#</span>',
	enableAllSteps: true,
	showBackButton: false,
	showFooterButtons: false
});
// ナンバーと進む戻るボタン非表示
$(".steps.clearfix").css('display', 'none');
$(".actions.clearfix").css('display', 'none');

// エリア選択時の遷移
$('.area_move').click(function() {		
	$("#map_modal_sp-t-" + $(this).prop('id').substr(2)).click();
});
// 戻るボタン押下時の遷移
$('.area_search_a').click(function() {
	$("#map_modal_sp-t-0").click();
});

function search_before() {
	
	var pref_val = $('#RecruitTodoufuken').val();
	var city_val = $('#RecruitShikutyouson').val();
	var form_values = $("#sform").serializeArray();

	var excludeds = new Array('_method', 'data[_Token][key]', 'data[Recruit][todoufuken]', 'data[Recruit][shikutyouson]', 'data[_Token][fields]');
	var submit_flg = 0;
	form_values.forEach((item) => {
		if (item['value'] != null && item['value'] != '') {
			if (excludeds.indexOf(item['name']) == -1){
				// 存在しない
				submit_flg = 1;
			}
		}
	});
	
	if (submit_flg == 0) {
		if (pref_val != null && pref_val != '') {
			var redirect_url = '/cw/area/pref';
			var prefs = <?php echo json_encode($todoufukens); ?>;
			const pref_key = Object.keys(prefs).reduce( (r, key) => { 
				return prefs[key] === pref_val ? key : r 
			}, null);
			redirect_url = redirect_url + pref_key + '/';

			if (city_val != null && city_val != '') {
				var cities = <?php echo json_encode($cities); ?>;
				const city_key = Object.keys(cities).reduce( (r, key) => { 
					return cities[key] === city_val ? key : r 
				}, null);
				redirect_url = redirect_url + 'city' + city_key + '/';
			}

			location.href = redirect_url;
			
		} else {
			$("#sform").submit();
		}
	} else {
		$("#sform").submit();
	}
	
}
<?php echo $this->Html->scriptEnd(); ?>
	
<?php echo $this->Html->script('modal'); ?>

<?php
// 最寄り路線・駅
$datas = array(
    'lineEmptyText' => '最寄り路線を選択',
    'stationEmptyText' => '最寄り駅を選択',
    'prefId' => '#RecruitPrefCd',
    'lineId' => '#RecruitLineCd',
    'stationId' => '#RecruitStationCd'
);
echo $this->element('interface', $datas, array('plugin' => 'Ekidata'));
?>
</body>
</html>