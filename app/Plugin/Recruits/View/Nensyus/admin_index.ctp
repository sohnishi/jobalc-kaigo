<div class="row">
	<div class="col-xs-12 contents-actions">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#">給与（年収）一覧</a></li>
			<li><a href="<?php echo $this->Html->url('/admin/recruits/nensyus/add'); ?>">給与（年収）を新規登録</a></li>
		</ul>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">

		<div class="panel panel-default">
			<div class="panel-heading">給与（年収）一覧</div>
			<div class="panel-body">

				<table class="table table-striped">
					<thead>
						<tr>
							<th><?php echo $this->Paginator->sort('id', 'ID'); ?></th>
							<th><?php echo $this->Paginator->sort('name', '給与（年収）'); ?></th>
							<th><?php echo $this->Paginator->sort('weight', '並び順'); ?></th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($nensyus as $nensyu) : ?>
						<tr>
							<td><?php echo $nensyu[$model]['id']; ?></td>
							<td><?php echo $nensyu[$model]['name']; ?></td>
							<td><?php echo $nensyu[$model]['weight']; ?></td>
							<td>
								<ul class="list-inline">
									<li><?php echo $this->Html->link('編集', array('action' => 'edit', $nensyu[$model]['id'])); ?></li>
									<li><?php echo $this->Form->postLink('削除', array('action' => 'delete', $nensyu[$model]['id']), '', '本当に削除しますか？元には戻せません'); ?></li>
								</ul>
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
				<div class="text-center">
					<?php echo $this->Paginator->pagination(array('ul' => 'pagination')); ?>
				</div>

			</div>
		</div>

	</div>
</div>