<div class="row">
	<div class="col-xs-12 contents-actions">
		<ul class="nav nav-tabs">
			<li><a href="<?php echo $this->Html->url('/admin/recruits/recruitPreliminaryItems'); ?>">求人予備項目一覧</a></li>
			<li class="active"><a href="#">求人予備項目を更新</a></li>
		</ul>
	</div>
</div>

<div class="row">
	<div class="col-xs-4 col-xs-offset-4">

		<div class="panel panel-default">
			<div class="panel-heading">求人予備項目を更新</div>
			<?php
				echo $this->Form->create($model, array(
					'inputDefaults' => array(
						'label' => false,
						'wrapInput' => false,
						'div' => false,
						'class' => 'form-control'
					),
				));

				echo $this->Form->input('id');
			?>
			<div class="panel-body">

				<div class="form-group">
					<?php echo $this->Form->input('name', array('label' => '求人予備項目')); ?>
				</div>

			</div>
			<div class="panel-footer">
				<?php echo $this->Form->submit('求人予備項目を更新する', array('class' => 'btn btn-primary')); ?>
			</div>
			<?php echo $this->Form->end(); ?>
		</div>

	</div>
</div>
