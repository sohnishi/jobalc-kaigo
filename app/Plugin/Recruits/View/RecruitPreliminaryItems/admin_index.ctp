<div class="row">
	<div class="col-xs-12 contents-actions">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#">求人予備項目一覧</a></li>
			<li><a href="<?php echo $this->Html->url('/admin/recruits/recruitPreliminaryItems/add'); ?>">求人予備項目を新規登録</a></li>
		</ul>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">

		<div class="panel panel-default">
			<div class="panel-heading">求人予備項目一覧</div>
			<div class="panel-body">

				<table class="table table-striped">
					<thead>
						<tr>
							<th><?php echo $this->Paginator->sort('id', 'ID'); ?></th>
							<th><?php echo $this->Paginator->sort('name', '求人予備項目'); ?></th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($recruitPreliminaryItems as $recruitPreliminaryItem) : ?>
						<tr>
							<td><?php echo $recruitPreliminaryItem[$model]['id']; ?></td>
							<td><?php echo $recruitPreliminaryItem[$model]['name']; ?></td>
							<td>
								<ul class="list-inline">
									<li><?php echo $this->Html->link('編集', array('action' => 'edit', $recruitPreliminaryItem[$model]['id'])); ?></li>
									<li><?php echo $this->Form->postLink('削除', array('action' => 'delete', $recruitPreliminaryItem[$model]['id']), '', '本当に削除しますか？元には戻せません'); ?></li>
								</ul>
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>

				<div class="text-center">
					<?php echo $this->Paginator->pagination(array('ul' => 'pagination')); ?>
				</div>
			</div>
		</div>

	</div>
</div>
