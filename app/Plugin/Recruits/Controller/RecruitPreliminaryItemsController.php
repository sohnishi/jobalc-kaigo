<?php

App::uses('RecruitsAppController', 'Recruits.Controller');

class RecruitPreliminaryItemsController extends RecruitsAppController {

	public $name = 'RecruitPreliminaryItems';

	public $uses = array('Recruits.RecruitPreliminaryItems');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('model', $this->modelClass);
		$this->set('title_for_layout', '求人予備項目マスタ');
		$this->Security->unlockedActions = array('admin_typeahead');
		$this->set('currentUser', $this->Auth->user());

	}

	public function admin_index() {
		$this->Paginator->settings[$this->modelClass] = array(
			'order' => array($this->modelClass . '.created' => 'DESC')
		);
		$this->set('recruitPreliminaryItems', $this->Paginator->paginate());
	}

	public function admin_add() {
		if (!empty($this->request->data)) {
			$this->{$this->modelClass}->create();
			if ($this->{$this->modelClass}->save($this->request->data)) {
				$this->flashMsg('求人予備項目を登録しました');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->flashMsg('求人予備項目を登録中にエラーが発生しました', 'alert-warning');
			}
		}
	}

	public function admin_edit($id = null) {
		if (!$id || !$this->{$this->modelClass}->exists($id)) {
			$this->flashMsg('無効な操作です');
			$this->redirect(array('action' => 'index'));
		}

		if (!empty($this->request->data)) {
			if ($this->{$this->modelClass}->save($this->request->data)) {
				$this->flashMsg('求人予備項目を更新しました');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->flashMsg('求人予備項目を更新中にエラーが発生しました', 'alert-warning');
			}
		} else {
			$this->request->data = $this->{$this->modelClass}->read(null, $id);
		}
	}

	public function admin_delete($id = null) {
		if (!$id || !$this->{$this->modelClass}->exists($id)) {
			$this->flashMsg('無効な操作です', 'alert-danger');
			$this->redirect(array('action' => 'index'));
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->{$this->modelClass}->delete($id)) {
				$this->flashMsg('求人予備項目を削除しました');
			} else {
				$this->flashMsg('求人予備項目を削除中にエラーが発生しました', 'alert-warning');
			}
		} else {
			$this->flashMsg('無効な操作です', 'alert-danger');
		}
		$this->redirect(array('action' => 'index'));
	}

	public function admin_typeahead() {
		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			Configure::write('debug', 0);
			$turm = $this->request->query['q'];
			$results = $this->{$this->modelClass}->find('all', array(
				'conditions' => array(
					$this->modelClass . '.name LIKE' => $turm . '%'
				)
			));
			$groups = array();
			foreach ($results as $key => $result) {
				$val['id'] = $result[$this->modelClass]['id'];
				$val['name'] = $result[$this->modelClass]['name'];
				array_push($groups, $val);
			}
			echo json_encode($groups);
		}
	}
}
