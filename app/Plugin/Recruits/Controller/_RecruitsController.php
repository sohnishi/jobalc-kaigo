<?php

App::uses('RecruitsAppController', 'Recruits.Controller');
App::uses('Hash', 'Utility');

class RecruitsController extends RecruitsAppController {

	public $name = 'Recruits';

	public $uses = array('Recruits.Recruit');

	public $components = array(
		'Ekidata.Railroader' => array(
			'pref' => array(
				'id' => 'pref_cd',
				'list' => 'prefs'
			),
			'line' => array(
				'id' => 'line_cd',
				'list' => 'lines'
			),
			'station' => array(
				'id' => 'station_cd',
				'list' => 'stations'
			)
		),
		'Search.Prg',
		'Csv'
	);

	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('model', $this->modelClass);
		$this->set('title_for_layout', '求人管理');
		$this->Security->unlockedActions = array('admin_index', 'search', 'results');
	}

	public function admin_index() {
		$this->Prg->commonProcess();
		if (!empty($this->request->data)) {
			$this->Paginator->settings['conditions'] = array(
				$this->{$this->modelClass}->parseCriteria($this->Prg->parsedParams())
			);
			$this->Session->write('recruitquerystring', $this->request->query);
		} else {
			$this->Session->delete('recruitquerystring');
		}

		$this->Paginator->settings['contain'] = array(
			'Facility',
			'ShisetsukeitaiSecond'
		);
		$this->Paginator->settings['order'] = array(
			$this->modelClass . '.modified' => 'DESC'
		);
		$this->Paginator->settings['limit'] = 50;
		$this->set('recruits', $this->Paginator->paginate());

		// 部署リストを取得
		$shisetsukeitaiSeconds = $this->{$this->modelClass}->ShisetsukeitaiSecond->find('list', array('order' => 'ShisetsukeitaiSecond.weight ASC'));
		$this->set('shisetsukeitaiSeconds', $shisetsukeitaiSeconds);
		// 年収リストを取得
		$nensyus = $this->{$this->modelClass}->Nensyu->find('list', array('order' => 'Nensyu.weight ASC'));
		$this->set('nensyus', $nensyus);
		// 休日体制リストを取得
		$kyujitsus = $this->{$this->modelClass}->Kyujitsu->find('list', array('order' => 'Kyujitsu.weight ASC'));
		$this->set('kyujitsus', $kyujitsus);
		// こだわりリストを取得
		$kodawaris = $this->{$this->modelClass}->Kodawari->find('list', array('order' => 'Kodawari.weight ASC'));
		$this->set('kodawaris', $kodawaris);
	}

	public function admin_view() {
		$this->Prg->commonProcess();

		$params = Router::getParams();
		$facility_id = null;
		if (isset($params['named']['facility'])) {
			$facility_id = $params['named']['facility'];
		}

		$this->Paginator->settings['conditions'] = array(
			$this->{$this->modelClass}->parseCriteria($this->Prg->parsedParams())
		);
		if ($facility_id) {
			$this->Paginator->settings['conditions'] = array(
				$this->modelClass . '.facility_id' => $facility_id
			);
		}
		$this->Paginator->settings['contain'] = array(
			'Facility',
			'ShisetsukeitaiSecond',
			'RecruitShow',
			'Nensyu',
			'Kyujitsu',
			'Kodawari',
			'Customer' => array(
				'CustomerCertificate',
				'CustomerCondition'
			)
		);
		$this->Paginator->settings['order'] = array(
			$this->modelClass . '.modified' => 'DESC'
		);
		$this->Paginator->settings['limit'] = 1;
		$this->set('recruits', $this->Paginator->paginate());
	}

	public function admin_add() {
		$facilityId = empty($this->request->params['pass']) ? '' : $this->request->params['pass'][0];

		if (!empty($this->request->data)) {
			$this->{$this->modelClass}->create();
			if ($this->{$this->modelClass}->saveAll($this->request->data)) {
				$this->flashMsg('求人情報を登録しました');
				if (!empty($facilityId)) {
					$this->redirect(Hash::merge(array('plugin' => 'facilities', 'controller' => 'facilities', 'action' => 'view'), $this->request->params['named'], array('?' => $this->Session->read('querystring'))));
				} else {
					$this->redirect(array('action' => 'index'));
				}
			} else {
				$this->flashMsg('求人情報を登録中にエラーが発生しました', 'alert-warning');
			}
		} else {
			$this->request->data[$this->modelClass]['facility_id'] = $facilityId;
		}

		// 施設情報リストを取得
		$facilities = $this->{$this->modelClass}->Facility->find('list', array('fields' => array('id', 'shisetsumei')));
		$this->set('facilities', $facilities);
		// 部署リストを取得
		$shisetsukeitaiSeconds = $this->{$this->modelClass}->ShisetsukeitaiSecond->find('list', array('order' => 'ShisetsukeitaiSecond.weight ASC'));
		$this->set('shisetsukeitaiSeconds', $shisetsukeitaiSeconds);
		// 年収リストを取得
		$nensyus = $this->{$this->modelClass}->Nensyu->find('list', array('order' => 'Nensyu.weight ASC'));
		$this->set('nensyus', $nensyus);
		// 休日体制リストを取得
		$kyujitsus = $this->{$this->modelClass}->Kyujitsu->find('list', array('order' => 'Kyujitsu.weight ASC'));
		$this->set('kyujitsus', $kyujitsus);
		// こだわりリストを取得
		$kodawaris = $this->{$this->modelClass}->Kodawari->find('list', array('order' => 'Kodawari.weight ASC'));
		$this->set('kodawaris', $kodawaris);
	}

	public function admin_edit($id = null) {
		if (!$id || !$this->{$this->modelClass}->exists($id)) {
			$this->flashMsg('無効な操作です', 'alert-danger');
			$this->redirect(array('action' => 'index'));
		}

		if (!empty($this->request->data)) {
			if ($this->{$this->modelClass}->saveAll($this->request->data)) {
				$this->flashMsg('求人情報を更新しました');
				$this->redirect(Hash::merge(array('action' => 'view'), $this->request->params['named'], array('?' => $this->Session->read('recruitquerystring'))));
			} else {
				$this->flashMsg('求人情報を更新中にエラーが発生しました', 'alert-warning');
			}
		} else {
			$this->{$this->modelClass}->recursive = 1;
			$this->request->data = $this->{$this->modelClass}->read(null, $id);
		}

		// 施設情報リストを取得
		$facilities = $this->{$this->modelClass}->Facility->find('list', array('fields' => array('id', 'shisetsumei')));
		$this->set('facilities', $facilities);
		// 部署リストを取得
		$shisetsukeitaiSeconds = $this->{$this->modelClass}->ShisetsukeitaiSecond->find('list', array('order' => 'ShisetsukeitaiSecond.weight ASC'));
		$this->set('shisetsukeitaiSeconds', $shisetsukeitaiSeconds);
		// 年収リストを取得
		$nensyus = $this->{$this->modelClass}->Nensyu->find('list', array('order' => 'Nensyu.weight ASC'));
		$this->set('nensyus', $nensyus);
		// 休日体制リストを取得
		$kyujitsus = $this->{$this->modelClass}->Kyujitsu->find('list', array('order' => 'Kyujitsu.weight ASC'));
		$this->set('kyujitsus', $kyujitsus);
		// こだわりリストを取得
		$kodawaris = $this->{$this->modelClass}->Kodawari->find('list', array('order' => 'Kodawari.weight ASC'));
		$this->set('kodawaris', $kodawaris);
	}

	public function admin_delete($id = null) {
		if (!$id || !$this->{$this->modelClass}->exists($id)) {
			$this->flashMsg('無効な操作です', 'alert-danger');
			$this->redirect(array('action' => 'index'));
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->{$this->modelClass}->delete($id)) {
				$this->flashMsg('求人情報を削除しました');
			} else {
				$this->flashMsg('求人情報を削除中にエラーが発生しました', 'alert-warning');
			}
		}
		$this->redirect($this->referer(array('action' => 'index')));
	}

	public function admin_import() {
		$this->set('title_for_layout', '求人情報CSVインポート');
		if ($this->request->is('post')) {
			// 現在のレコードをカウント
			$record_count = $this->{$this->modelClass}->find('count');

			$err_flg = false;
			$err_msg = array();

			$_data = $this->Csv->import($this->request->data[$this->modelClass]['csvfile']['tmp_name'], array(), array('encode' => 'sjis-win'));

			foreach ($_data as $key => $item) {
				// 社会保険処理
				if (isset($item['Recruit']['syakaihoken']) && !empty($item['Recruit']['syakaihoken'])) {
					$_data[$key]['Recruit']['syakaihoken'] = explode('|', $item['Recruit']['syakaihoken']);
				}
				// こだわり処理
				if (isset($item['Kodawari']['Kodawari']) && !empty($item['Kodawari']['Kodawari'])) {
					$_data[$key]['Kodawari']['Kodawari'] = explode('|', $item['Kodawari']['Kodawari']);
				}
				// 年収処理
				if (isset($item['Nensyu']['Nensyu']) && !empty($item['Nensyu']['Nensyu'])) {
					$_data[$key]['Nensyu']['Nensyu'] = explode('|', $item['Nensyu']['Nensyu']);
				}
				// 休日処理
				if (isset($item['Kyujitsu']['Kyujitsu']) && !empty($item['Kyujitsu']['Kyujitsu'])) {
					$_data[$key]['Kyujitsu']['Kyujitsu'] = explode('|', $item['Kyujitsu']['Kyujitsu']);
				}
			}

			// バリデーション処理
			foreach ($_data as $key => $item) {
				if (!$this->{$this->modelClass}->saveAll($item, array('validate' => 'only', 'deep' => true))) {
					$err_msg[$key]['row'] = $key + 1;
					$err_msg[$key]['msg'] = Hash::flatten($this->{$this->modelClass}->validationErrors);
					$err_flg = true;
				}
			}

			if ($err_flg) {
				// バリデーションエラーをビューへ渡す
				$errors = $err_msg;
				$this->set('errors', $errors);
				$this->flashMsg('インポートエラー' . ' ' . $this->request->data[$this->modelClass]['csvfile']['name'] . ', データに不具合があります', 'alert-danger');
			} else {
				if ($_data) {
					foreach ($_data as $key => $item) {
						$this->{$this->modelClass}->create();
						$this->{$this->modelClass}->saveAll($item, array('validate' => false, 'deep' => true));
					}
					// 保存後のレコードをカウントして差分を求める
					$new_records_count = $this->{$this->modelClass}->find('count') - $record_count;
					$this->flashMsg('インポートが完了しました' . ' ' . $new_records_count . ' レコード : ' . $this->request->data[$this->modelClass]['csvfile']['name']);
					$this->redirect(array('action' => 'index'));
				}
			}
		}
	}

	public function index() {
		$this->set('title_for_layout', 'ホーム');
		$this->layout = 'top';

		// 新着求人
		$newsQuery = array(
			'conditions' => array(
				'Recruit.active' => 1,
				'Recruit.shintyaku_kyujin' => 1
			),
			'order' => array('Recruit.modified' => 'DESC'),
			'limit' => 5,
			'contain' => array(
				'RecruitShow',
				'ShisetsukeitaiSecond',
				'Kodawari',
				'Facility' => array(
					'FacilityShow',
					'ShisetsukeitaiSecond',
					'Roseneki' => array(
						'RailroadPref',
						'RailroadLine',
						'RailroadStation'
					)
				)
			)
		);
		//$news = $this->{$this->modelClass}->find('formatedSearch', $newsQuery);
		$news = $this->{$this->modelClass}->find('all', $newsQuery);
		$this->set('news', $news);

		// 人気求人
		$popQuery = array(
			'conditions' => array(
				'Recruit.active' => 1,
				'Recruit.ninki_kyujin' => 1
			),
			'order' => array('Recruit.modified' => 'DESC'),
			'limit' => 5,
			'contain' => array(
				'RecruitShow',
				'ShisetsukeitaiSecond',
				'Kodawari',
				'Facility' => array(
					'FacilityShow',
					'ShisetsukeitaiSecond',
					'Roseneki' => array(
						'RailroadPref',
						'RailroadLine',
						'RailroadStation'
					)
				)
			)
		);
		//$populars = $this->{$this->modelClass}->find('formatedSearch', $popQuery);
		$populars = $this->{$this->modelClass}->find('all', $popQuery);
		$this->set('populars', $populars);

		// 施設形態リストを取得
		$shisetsukeitaiSeconds = $this->{$this->modelClass}->ShisetsukeitaiSecond->find('list', array('order' => 'ShisetsukeitaiSecond.weight ASC'));
		$this->set('shisetsukeitaiSeconds', $shisetsukeitaiSeconds);
	}

	public function detail($id = null) {
		if (!$id || !$this->{$this->modelClass}->exists($id)) {
			$this->flashMsg('お探しのページが見つかりません', 'alert-danger');
			$this->redirect(array('action' => 'index'));
		}

		$detailQuery = array(
			'conditions' => array(
				$this->modelClass . '.id' => $id,
			),
			'contain' => array(
				'Nensyu',
				'Kyujitsu',
				'RecruitShow',
				'Kodawari',
				'Facility' => array(
					'ShisetsukeitaiSecond',
					'Shinryoukamoku',
					'FacilityShow',
					'Roseneki' => array(
						'RailroadPref',
						'RailroadLine',
						'RailroadStation'
					)
				)
			)
		);
		$detail = $this->{$this->modelClass}->find('first', $detailQuery);
		if ($detail['Recruit']['active'] != 1) {
			$this->flashMsg('お探しの情報は非公開です', 'alert-warning');
			$this->redirect(array('action' => 'index'));
		}
		$this->set('detail', $detail);

		// 関連求人
		$relatedRecruits = $this->{$this->modelClass}->find('all', array(
			'conditions' => array(
				$this->modelClass . '.facility_id' => $detail['Recruit']['facility_id'],
				$this->modelClass . '.active' => 1
			),
			'contain' => array(
				'RecruitShow'
			)
		));
		$this->set('relatedRecruits', $relatedRecruits);

		// こだわりリスト
		$kodawaris = $this->{$this->modelClass}->Kodawari->find('list', array('order' => 'Kodawari.weight ASC'));
		$this->set('kodawaris', $kodawaris);
		// 施設形態リストを取得
		$shisetsukeitaiSeconds = $this->{$this->modelClass}->ShisetsukeitaiSecond->find('list', array('order' => 'ShisetsukeitaiSecond.weight ASC'));
		$this->set('shisetsukeitaiSeconds', $shisetsukeitaiSeconds);

		$this->set('title_for_layout', sprintf('%sの求人詳細', $detail['Facility']['shisetsumei']));
	}

	public function search() {
		$this->set('title_for_layout', '希望条件から求人を探す');

		// 施設リスト
		$shisetsukeitaiFirsts = $this->{$this->modelClass}->Facility->ShisetsukeitaiFirst->find('list', array('order' => 'ShisetsukeitaiFirst.weight ASC'));
		$this->set('shisetsukeitaiFirsts', $shisetsukeitaiFirsts);
		// 部署リスト
		$shisetsukeitaiSeconds = $this->{$this->modelClass}->ShisetsukeitaiSecond->find('list', array('order' => 'ShisetsukeitaiSecond.weight ASC'));
		$this->set('shisetsukeitaiSeconds', $shisetsukeitaiSeconds);
		// 診療科目リスト
		$shinryoukamokus = $this->{$this->modelClass}->Facility->Shinryoukamoku->find('list', array('order' => 'Shinryoukamoku.weight ASC'));
		$this->set('shinryoukamokus', $shinryoukamokus);
		// こだわりリスト
		$kodawaris = $this->{$this->modelClass}->Kodawari->find('list', array('order' => 'Kodawari.weight ASC'));
		$this->set('kodawaris', $kodawaris);
	}

	public function results() {
		$this->set('title_for_layout', '検索結果');
		$this->layout = 'result';

		$this->Prg->commonProcess($this->modelClass);

		if (!empty($this->request->data)) {
			$this->Paginator->settings = array(
				'conditions' => array(
						$this->{$this->modelClass}->parseCriteria($this->Prg->parsedParams()),
						$this->modelClass . '.active' => 1
				),
				'contain' => array(
					'RecruitShow',
					'ShisetsukeitaiSecond',
					'Kodawari',
					'Facility' => array(
						'FacilityShow',
						'ShisetsukeitaiSecond',
						'Roseneki' => array(
							'RailroadPref',
							'RailroadLine',
							'RailroadStation'
						)
					)
				),
				'order' => array(
					$this->modelClass . '.modified' => 'DESC'
				),
				'limit' => 25,
				//'findType' => 'formatedSearch'
			);
			$results = $this->Paginator->paginate($this->modelClass);
			$this->set('results', $results);
		} else {
			$this->Paginator->settings = array(
				'conditions' => array(
					$this->modelClass . '.active' => 1
				),
				'contain' => array(
					'RecruitShow',
					'ShisetsukeitaiSecond',
					'Kodawari',
					'Facility' => array(
						'FacilityShow',
						'ShisetsukeitaiSecond',
						'Roseneki' => array(
							'RailroadPref',
							'RailroadLine',
							'RailroadStation'
						)
					)
				),
				'order' => array(
					$this->modelClass . '.modified' => 'DESC'
				),
				'limit' => 25,
				//'findType' => 'formatedSearch'
			);
			$results = $this->Paginator->paginate($this->modelClass);
			$this->set('results', $results);
		}

		// 施設形態リストを取得
		$shisetsukeitaiFirsts = $this->{$this->modelClass}->Facility->ShisetsukeitaiFirst->find('list', array('order' => 'ShisetsukeitaiFirst.weight ASC'));
		$this->set('shisetsukeitaiFirsts', $shisetsukeitaiFirsts);
		$shisetsukeitaiSeconds = $this->{$this->modelClass}->ShisetsukeitaiSecond->find('list', array('order' => 'ShisetsukeitaiSecond.weight ASC'));
		$this->set('shisetsukeitaiSeconds', $shisetsukeitaiSeconds);
		$shinryoukamokus = $this->{$this->modelClass}->Facility->Shinryoukamoku->find('list', array('order' => 'Shinryoukamoku.weight ASC'));
		$this->set('shinryoukamokus', $shinryoukamokus);
		$kodawaris = $this->{$this->modelClass}->Kodawari->find('list', array('order' => 'Kodawari.weight ASC'));
		$this->set('kodawaris', $kodawaris);
	}

}
