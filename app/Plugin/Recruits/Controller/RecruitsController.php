<?php

App::uses('RecruitsAppController', 'Recruits.Controller');
App::uses('Hash', 'Utility');

class RecruitsController extends RecruitsAppController {
                                                                                                                                                                                     
	public $name = 'Recruits';

	public $uses = array(
		'Recruits.Recruit', 
		'Recruits.RecruitPreliminaryItem', 
		'Facilities.ShisetsukeitaiFirst', 
		'Ekidata.RailroadPref', 
		'Ekidata.RailroadLine', 
		'Ekidata.RailroadStation', 
		'Contents.PrefectureContent', 
		'Contents.FacilityContent', 
		'Contents.JobContent', 
		'Contents.EmploymentContent', 
		'Contents.QualificationContent'
	);

	public $components = array(
		'Ekidata.Railroader' => array(
			'pref' => array(
				'id' => 'pref_cd',
				'list' => 'prefs'
			),
			'line' => array(
				'id' => 'line_cd',
				'list' => 'lines'
			),
			'station' => array(
				'id' => 'station_cd',
				'list' => 'stations'
			)
		),
		/*
			20190514 sohnishi
			求人予備項目がArrayのため未入力でも検索に走ってしまう事象を回避するため、filterEmptyを追加
		*/
		'Search.Prg' => array(
			'commonProcess' => array(	
				'paramType' => 'named',
				'filterEmpty' =>  true,	
			)
		),
		'Csv'
	);

	
	public function beforeFilter() {
		parent::beforeFilter();

		Configure::write('debug', 0);
		/*
			20190523 sohnishi
			Layoutを使用しない方針に変更
			
			20190604 sohnishi
			Admin以外の時のみLayoutを使用しない方針に変更
		*/
		if (!array_key_exists('admin', $this->request->params) || !$this->request->params['admin']) {
			$this->layout = '';
		}
		

		$this->set('model', $this->modelClass);
		$this->set('title_for_layout', '求人管理');
		$this->Security->unlockedActions = array('admin_index', 'search', 'results', 'searchCount', 'updateFavorites', 'deleteFavorite');
		$this->set('currentUser', $this->Auth->user());

	}

	public function admin_index() {
		$this->Prg->commonProcess();

		if (!empty($this->request->data)) {
			$this->Paginator->settings['conditions'] = array(
				$this->{$this->modelClass}->parseCriteria($this->Prg->parsedParams())
			);
			$this->Session->write('recruitquerystring', $this->request->query);
		} else {
			$this->Session->delete('recruitquerystring');
		}
		$this->Paginator->settings['contain'] = array(
			'Facility',
			'ShisetsukeitaiSecond',
			'RecruitPreliminaryItem'
		);
		$this->Paginator->settings['order'] = array(
			$this->modelClass . '.modified' => 'DESC'
		);
		$this->Paginator->settings['limit'] = 50;
		$this->set('recruits', $this->Paginator->paginate());

		// 部署リストを取得
		$shisetsukeitaiSeconds = $this->{$this->modelClass}->ShisetsukeitaiSecond->find('list', array('order' => 'ShisetsukeitaiSecond.weight ASC'));
		$this->set('shisetsukeitaiSeconds', $shisetsukeitaiSeconds);
		// 年収リストを取得
		$nensyus = $this->{$this->modelClass}->Nensyu->find('list', array('order' => 'Nensyu.weight ASC'));
		$this->set('nensyus', $nensyus);
		// 休日体制リストを取得
		$kyujitsus = $this->{$this->modelClass}->Kyujitsu->find('list', array('order' => 'Kyujitsu.weight ASC'));
		$this->set('kyujitsus', $kyujitsus);
		// こだわりリストを取得
		$kodawaris = $this->{$this->modelClass}->Kodawari->find('list', array('order' => 'Kodawari.weight ASC'));
		$this->set('kodawaris', $kodawaris);
		/*
			20190513 sohnishi
			求人予備項目リストを取得
		*/
		$recruitPreliminaryItems = $this->RecruitPreliminaryItem->find('list', array('order' => 'RecruitPreliminaryItem.id ASC'));
		$this->set('recruitPreliminaryItems', $recruitPreliminaryItems);
	}

	public function admin_view() {
		$this->Prg->commonProcess();

		$params = Router::getParams();
		$facility_id = null;
		if (isset($params['named']['facility'])) {
			$facility_id = $params['named']['facility'];
		}

		$this->Paginator->settings['conditions'] = array(
			$this->{$this->modelClass}->parseCriteria($this->Prg->parsedParams())
		);
		if ($facility_id) {
			$this->Paginator->settings['conditions'] = array(
				$this->modelClass . '.facility_id' => $facility_id
			);
		}
		$this->Paginator->settings['contain'] = array(
			'Facility',
			'ShisetsukeitaiSecond',
			'RecruitShow',
			'Nensyu',
			'Kyujitsu',
			'Kodawari',
			'Customer' => array(
				'CustomerCertificate',
				'CustomerCondition'
			),
			/*
				20190513 sohnishi
				求人予備項目登録内容を追加
			*/
			'RecruitPreliminaryItem' => array(
				'order' => 'RecruitPreliminaryItem.id ASC',
			)
		);
		$this->Paginator->settings['order'] = array(
			$this->modelClass . '.modified' => 'DESC'
		);
		$this->Paginator->settings['limit'] = 1;
		$this->set('recruits', $this->Paginator->paginate());

		/*
			20190513 sohnishi
			求人予備項目リストを取得
		*/
		$recruitPreliminaryItems = $this->RecruitPreliminaryItem->find('list', array('order' => 'RecruitPreliminaryItem.id ASC'));
		$this->set('recruitPreliminaryItems', $recruitPreliminaryItems);
	}

	public function admin_add() {

		$facilityId = empty($this->request->params['pass']) ? '' : $this->request->params['pass'][0];

		if (!empty($this->request->data)) {
			$this->{$this->modelClass}->create();
			if ($this->{$this->modelClass}->saveAll($this->request->data)) {
				$this->flashMsg('求人情報を登録しました');
				if (!empty($facilityId)) {
					$this->redirect(Hash::merge(array('plugin' => 'facilities', 'controller' => 'facilities', 'action' => 'view'), $this->request->params['named'], array('?' => $this->Session->read('querystring'))));
				} else {
					$this->redirect(array('action' => 'index'));
				}
			} else {
				$this->flashMsg('求人情報を登録中にエラーが発生しました', 'alert-warning');
			}
		} else {
			$this->request->data[$this->modelClass]['facility_id'] = $facilityId;
		}

		// 施設情報リストを取得
		$facilities = $this->{$this->modelClass}->Facility->find('list', array('fields' => array('id', 'shisetsumei')));
		$this->set('facilities', $facilities);
		// 部署リストを取得
		$shisetsukeitaiSeconds = $this->{$this->modelClass}->ShisetsukeitaiSecond->find('list', array('order' => 'ShisetsukeitaiSecond.weight ASC'));
		$this->set('shisetsukeitaiSeconds', $shisetsukeitaiSeconds);
		// 年収リストを取得
		$nensyus = $this->{$this->modelClass}->Nensyu->find('list', array('order' => 'Nensyu.weight ASC'));
		$this->set('nensyus', $nensyus);
		// 休日体制リストを取得
		$kyujitsus = $this->{$this->modelClass}->Kyujitsu->find('list', array('order' => 'Kyujitsu.weight ASC'));
		$this->set('kyujitsus', $kyujitsus);
		// こだわりリストを取得
		$kodawaris = $this->{$this->modelClass}->Kodawari->find('list', array('order' => 'Kodawari.weight ASC'));
		$this->set('kodawaris', $kodawaris);
		// 施設予備項目リストを取得
		$recruitPreliminaryItems = $this->RecruitPreliminaryItem->find('list', array('order' => 'RecruitPreliminaryItem.id ASC'));
		$this->set('recruitPreliminaryItems', $recruitPreliminaryItems);
	}

	public function admin_edit($id = null) {

		if (!$id || !$this->{$this->modelClass}->exists($id)) {
			$this->flashMsg('無効な操作です', 'alert-danger');
			$this->redirect(array('action' => 'index'));
		}

		if (!empty($this->request->data)) {
			if ($this->{$this->modelClass}->saveAll($this->request->data)) {
				$this->flashMsg('求人情報を更新しました');
				/*
					20190604 sohnishi
					更新完了後、更新された求人は最上位に表示されるため、
					page:でリダイレクトすると、別の求人Viewを表示してしまうバグがあったため、修正
				*/
				// $this->redirect(Hash::merge(array('action' => 'view'), $this->request->params['named'], array('?' => $this->Session->read('recruitquerystring'))));
				$this->redirect(Hash::merge(array('controller' => 'view', 'action' => $id), array('?' => $this->Session->read('recruitquerystring'))));
			} else {
				$this->flashMsg('求人情報を更新中にエラーが発生しました', 'alert-warning');
			}
		} else {
			$this->{$this->modelClass}->recursive = 1;
			$this->request->data = $this->{$this->modelClass}->read(null, $id);
		}

		// 施設情報リストを取得
		$facilities = $this->{$this->modelClass}->Facility->find('list', array('fields' => array('id', 'shisetsumei')));
		$this->set('facilities', $facilities);
		// 部署リストを取得
		$shisetsukeitaiSeconds = $this->{$this->modelClass}->ShisetsukeitaiSecond->find('list', array('order' => 'ShisetsukeitaiSecond.weight ASC'));
		$this->set('shisetsukeitaiSeconds', $shisetsukeitaiSeconds);
		// 年収リストを取得
		$nensyus = $this->{$this->modelClass}->Nensyu->find('list', array('order' => 'Nensyu.weight ASC'));
		$this->set('nensyus', $nensyus);
		// 休日体制リストを取得
		$kyujitsus = $this->{$this->modelClass}->Kyujitsu->find('list', array('order' => 'Kyujitsu.weight ASC'));
		$this->set('kyujitsus', $kyujitsus);
		// こだわりリストを取得
		$kodawaris = $this->{$this->modelClass}->Kodawari->find('list', array('order' => 'Kodawari.weight ASC'));
		$this->set('kodawaris', $kodawaris);

		/*
			20190513 sohnishi
			求人予備項目リストを取得
		*/
		$recruitPreliminaryItems = $this->RecruitPreliminaryItem->find('list', array('order' => 'RecruitPreliminaryItem.id ASC'));
		$this->set('recruitPreliminaryItems', $recruitPreliminaryItems);
		/*
			20190513 sohnishi
			求人 - 求人予備項目中間テーブルリストを取得
		*/
		$recruitsRecruitPreliminaryItems = $this->{$this->modelClass}->RecruitsRecruitPreliminaryItem->find('list', 
			array(
				'order' => 'RecruitsRecruitPreliminaryItem.id ASC',
				'conditions' => 
					array(
						 'RecruitsRecruitPreliminaryItem.recruit_id' => $id
					),
				'fields' => array('RecruitsRecruitPreliminaryItem.recruit_preliminary_item_id', 'RecruitsRecruitPreliminaryItem.content')
				));
		$this->set('recruitsRecruitPreliminaryItems', $recruitsRecruitPreliminaryItems);

		$recruit = $this->{$this->modelClass}->find('all', array(
			'conditions' => 
				array(
						'Recruit.id' => $id
				),
		));
		$this->set('recruit', $recruit);
	}

	public function admin_delete($id = null) {

		if (!$id || !$this->{$this->modelClass}->exists($id)) {
			$this->flashMsg('無効な操作です', 'alert-danger');
			$this->redirect(array('action' => 'index'));
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->{$this->modelClass}->delete($id)) {
				$this->flashMsg('求人情報を削除しました');
			} else {
				$this->flashMsg('求人情報を削除中にエラーが発生しました', 'alert-warning');
			}
		}
		$this->redirect($this->referer(array('action' => 'index')));
	}

	public function admin_import() {

		$this->set('title_for_layout', '求人情報CSVインポート');
		if ($this->request->is('post')) {
			// 現在のレコードをカウント
			$record_count = $this->{$this->modelClass}->find('count');

			$err_flg = false;
			$err_msg = array();
			$_data = $this->Csv->import($this->request->data[$this->modelClass]['csvfile']['tmp_name'], array(), array('encode' => 'sjis-win'));

			foreach ($_data as $key => $item) {
				// 社会保険処理
				if (isset($item['Recruit']['syakaihoken']) && !empty($item['Recruit']['syakaihoken'])) {
					$_data[$key]['Recruit']['syakaihoken'] = explode('|', $item['Recruit']['syakaihoken']);
				}
				// こだわり処理
				if (isset($item['Kodawari']['Kodawari']) && !empty($item['Kodawari']['Kodawari'])) {
					$_data[$key]['Kodawari']['Kodawari'] = explode('|', $item['Kodawari']['Kodawari']);
				}
				// 年収処理
				if (isset($item['Nensyu']['Nensyu']) && !empty($item['Nensyu']['Nensyu'])) {
					$_data[$key]['Nensyu']['Nensyu'] = explode('|', $item['Nensyu']['Nensyu']);
				}
				// 休日処理
				if (isset($item['Kyujitsu']['Kyujitsu']) && !empty($item['Kyujitsu']['Kyujitsu'])) {
					$_data[$key]['Kyujitsu']['Kyujitsu'] = explode('|', $item['Kyujitsu']['Kyujitsu']);
				}
				/*
					20190514 sohnishi
					求人予備項目処理追加
				*/
				if (isset($item['RecruitsRecruitPreliminaryItem']['RecruitPreliminaryItem']) && !empty($item['RecruitsRecruitPreliminaryItem']['RecruitPreliminaryItem'])) {
					// CSVからデータを取得し、カンマで区切って配列にする(2件以上のデータを処理するため)
					// データ構成: recruit_preliminary_item_id|content|is_show,recruit_preliminary_item_id|content|is_show

					$replaced = preg_replace('/\n/', '', $item['RecruitsRecruitPreliminaryItem']['RecruitPreliminaryItem']);
					$tmpArray1 = explode(',', $replaced);
					$index = 0;
					// データ数ごとに分けた配列をカラムごとに分け直す
					foreach ($tmpArray1 as $tmp1) {
						$tmpArray2[$index] = explode(':', $tmp1);
						$index += 1;
					}
					// CSVで受け取ったデータを削除する(このままの構成ではSaveできないため)
					unset($_data[$key]['RecruitsRecruitPreliminaryItem']['RecruitPreliminaryItem']);
					$index = 0;
					// Saveできる構成で値を入れ直す
					$recruitsRecruitPreliminaryItems = array();
					foreach ($tmpArray2 as $recruitsRecruitPreliminaryItem) {
						$recruitsRecruitPreliminaryItems['recruit_preliminary_item_id'] = $recruitsRecruitPreliminaryItem[0];
						$recruitsRecruitPreliminaryItems['content'] = $recruitsRecruitPreliminaryItem[1];
						$recruitsRecruitPreliminaryItems['is_show'] = $recruitsRecruitPreliminaryItem[2];
						$_data[$key]['RecruitPreliminaryItem'][$index] = $recruitsRecruitPreliminaryItems;
						$index += 1;
					}
				}
			}

			// バリデーション処理
			foreach ($_data as $key => $item) {
				if (!$this->{$this->modelClass}->saveAll($item, array('validate' => 'only', 'deep' => true))) {
					$err_msg[$key]['row'] = $key + 1;
					$err_msg[$key]['msg'] = Hash::flatten($this->{$this->modelClass}->validationErrors);
					$err_flg = true;
				}
			}

			if ($err_flg) {
				// バリデーションエラーをビューへ渡す
				$errors = $err_msg;
				$this->set('errors', $errors);
				$this->flashMsg('インポートエラー' . ' ' . $this->request->data[$this->modelClass]['csvfile']['name'] . ', データに不具合があります', 'alert-danger');
			} else {
				if ($_data) {
					foreach ($_data as $key => $item) {
						$this->{$this->modelClass}->create();
						$this->{$this->modelClass}->saveAll($item, array('validate' => false, 'deep' => true));
					}
					// 保存後のレコードをカウントして差分を求める
					$new_records_count = $this->{$this->modelClass}->find('count') - $record_count;
					$this->flashMsg('インポートが完了しました' . ' ' . $new_records_count . ' レコード : ' . $this->request->data[$this->modelClass]['csvfile']['name']);
					$this->redirect(array('action' => 'index'));
				}
			}
		}
	}

	public function index() {
		$this->set('title_for_layout', 'ホーム');

		$favoritesArray = array();
		$favoritesString = $_COOKIE["job-alc-kaigo-favorites"];
		if ($favoritesString) {
			$revisedString = str_replace('[', '', $favoritesString);
			$revisedString = str_replace('"', '', $revisedString);
			$revisedString = str_replace(']', '', $revisedString);
			$favoritesArray = explode(",", $revisedString);
		}
		$favoriteRecruits = $this->{$this->modelClass}->find('all', 
			array(
				'conditions' => array(
					'id' => $favoritesArray
				)
			)
		);
		
		$shapingFavoritesArray = array();
		foreach ($favoriteRecruits as $recruit) {
			if($recruit['Recruit']['active'] == 0) {
				continue;
			}
			array_push($shapingFavoritesArray, $recruit['Recruit']['id']);
		}
		
		$favoriteString = implode(",", $shapingFavoritesArray);
		setcookie('job-alc-kaigo-favorites',$favoriteString,time()+60*60*24*30, "/");
		$this->set('favoritesArray', $shapingFavoritesArray);


		// 新着求人
		$newsQuery = array(
			'conditions' => array(
				'Recruit.active' => 1,
				'Recruit.shintyaku_kyujin' => 1
			),
			'order' => array('Recruit.modified' => 'DESC'),
			'limit' => 5,
			'contain' => array(
				'RecruitShow',
				'ShisetsukeitaiSecond',
				'Kodawari',
				'Facility' => array(
					'FacilityShow',
					'ShisetsukeitaiSecond',
					'Roseneki' => array(
						'RailroadPref',
						'RailroadLine',
						'RailroadStation'
					)
				)
			)
		);
		//$news = $this->{$this->modelClass}->find('formatedSearch', $newsQuery);
		$news = $this->{$this->modelClass}->find('all', $newsQuery);
		$this->set('news', $news);

		// 掲載求人数をセット
		$activeCount = $this->{$this->modelClass}->find('count', array(
			'conditions' => array(
				$this->{$this->modelClass}->parseCriteria($this->Prg->parsedParams()),
				$this->modelClass . '.active' => 1
			),
			'contain' => array(
				'RecruitShow',
				'ShisetsukeitaiSecond',
				'Kodawari',
				'Facility' => array(
					'FacilityShow',
					'ShisetsukeitaiFirst',
					'ShisetsukeitaiSecond',
					'Roseneki' => array(
						'RailroadPref',
						'RailroadLine',
						'RailroadStation'
					)
				)
			),
		));
		$this->set('activeCount', $activeCount);

		/*
			20190524 sohnishi
			介護では人気求人不要
		*/
		// // 人気求人
		// $popQuery = array(
		// 	'conditions' => array(
		// 		'Recruit.active' => 1,
		// 		'Recruit.ninki_kyujin' => 1
		// 	),
		// 	'order' => array('Recruit.modified' => 'DESC'),
		// 	'limit' => 5,
		// 	'contain' => array(
		// 		'RecruitShow',
		// 		'ShisetsukeitaiSecond',
		// 		'Kodawari',
		// 		'Facility' => array(
		// 			'FacilityShow',
		// 			'ShisetsukeitaiSecond',
		// 			'Roseneki' => array(
		// 				'RailroadPref',
		// 				'RailroadLine',
		// 				'RailroadStation'
		// 			)
		// 		)
		// 	)
		// );
		// //$populars = $this->{$this->modelClass}->find('formatedSearch', $popQuery);
		// $populars = $this->{$this->modelClass}->find('all', $popQuery);
		// $this->set('populars', $populars);


		/*
			20190524 sohnishi
			サイドバーに表示する施設形態1リストを取得
		*/
		$shisetsukeitaiFirsts = $this->ShisetsukeitaiFirst->find('list', array('order' => 'ShisetsukeitaiFirst.weight ASC'));
		$this->set('shisetsukeitaiFirsts', $shisetsukeitaiFirsts);

		// 施設形態2リストを取得
		$shisetsukeitaiSeconds = $this->{$this->modelClass}->ShisetsukeitaiSecond->find('list', array('order' => 'ShisetsukeitaiSecond.weight ASC'));
		$this->set('shisetsukeitaiSeconds', $shisetsukeitaiSeconds);

		// こだわり
		$kodawaris = $this->{$this->modelClass}->Kodawari->find('list', array('order' => 'Kodawari.weight ASC'));
		$this->set('kodawaris', $kodawaris);

		// パンくずリストセッション削除
		session_start();
		unset($_SESSION[search_pref]);
		unset($_SESSION[results_url]);
	}

	public function detail($id = null) {

		if (!$id || !$this->{$this->modelClass}->exists($id)) {
			$this->flashMsg('お探しのページが見つかりません', 'alert-danger');
			$this->redirect(array('action' => 'index'));
		}
		
		$favoritesArray = array();
		$favoritesString = $_COOKIE["job-alc-kaigo-favorites"];
		if ($favoritesString) {
			$revisedString = str_replace('[', '', $favoritesString);
			$revisedString = str_replace('"', '', $revisedString);
			$revisedString = str_replace(']', '', $revisedString);
			$favoritesArray = explode(",", $revisedString);
		}
		$favoriteRecruits = $this->{$this->modelClass}->find('all', 
			array(
				'conditions' => array(
					'id' => $favoritesArray
				)
			)
		);
		
		$shapingFavoritesArray = array();
		foreach ($favoriteRecruits as $recruit) {
			if($recruit['Recruit']['active'] == 0) {
				continue;
			}
			array_push($shapingFavoritesArray, $recruit['Recruit']['id']);
		}
		
		$favoriteString = implode(",", $shapingFavoritesArray);
		setcookie('job-alc-kaigo-favorites',$favoriteString,time()+60*60*24*30, "/");
		$this->set('favoritesArray', $shapingFavoritesArray);
		
		$detailQuery = array(
			'conditions' => array(
				$this->modelClass . '.id' => $id,
			),
			'contain' => array(
				'Nensyu',
				'Kyujitsu',
				'RecruitShow',
				'Kodawari',
				'Facility' => array(
					'ShisetsukeitaiFirst',
					'ShisetsukeitaiSecond',
					'Shinryoukamoku',
					'FacilityShow',
					'Roseneki' => array(
						'RailroadPref',
						'RailroadLine',
						'RailroadStation'
					)
				),
				/*
					20190513 sohnishi
					求人予備項目登録内容を追加
				*/
				'RecruitPreliminaryItem' => array(
					// 'fields' => array('id', 'name'),
					'order' => 'RecruitPreliminaryItem.id ASC',
				)
			)
		);
		$detail = $this->{$this->modelClass}->find('first', $detailQuery);
		if ($detail['Recruit']['active'] != 1) {
			$this->flashMsg('お探しの情報は非公開です', 'alert-warning');
			$this->redirect(array('action' => 'index'));
		}
		// RecruitPreliminaryItemのキーを主キーに変換
		$detail['RecruitPreliminaryItem'] = Hash::combine($detail, 'RecruitPreliminaryItem.{n}.id', 'RecruitPreliminaryItem.{n}');
		$this->set('detail', $detail);

		// 関連求人
		$relatedRecruits = $this->{$this->modelClass}->find('all', array(
			'conditions' => array(
				$this->modelClass . '.facility_id' => $detail['Recruit']['facility_id'],
				$this->modelClass . '.active' => 1
			),
			'contain' => array(
				'RecruitShow'
			)
		));
		$this->set('relatedRecruits', $relatedRecruits);

		// こだわりリスト
		$kodawaris = $this->{$this->modelClass}->Kodawari->find('list', array('order' => 'Kodawari.weight ASC'));
		$this->set('kodawaris', $kodawaris);
		// 施設形態リストセカンドを取得
		$shisetsukeitaiSeconds = $this->{$this->modelClass}->ShisetsukeitaiSecond->find('list', array('order' => 'ShisetsukeitaiSecond.weight ASC'));
		$this->set('shisetsukeitaiSeconds', $shisetsukeitaiSeconds);
		// 施設形態リストファーストを取得
		$shisetsukeitaiFirsts = $this->{$this->modelClass}->Facility->ShisetsukeitaiFirst->find('list', array('order' => 'ShisetsukeitaiFirst.weight ASC'));
		$this->set('shisetsukeitaiFirsts', $shisetsukeitaiFirsts);
		// 診療科目リストを取得
		$shinryoukamokus = $this->{$this->modelClass}->Facility->Shinryoukamoku->find('list', array('order' => 'Shinryoukamoku.weight ASC'));
		$this->set('shinryoukamokus', $shinryoukamokus);

		$this->set('title_for_layout', sprintf('%sの求人詳細', $detail['Facility']['shisetsumei']));



		// 近隣の新着求人　市区町村+職種で検索 5件まで取得
		$newsQuery = array(
			'conditions' => array(
				'Recruit.active' => 1,
				'Recruit.shintyaku_kyujin' => 1,
				'Facility.shikutyouson LIKE' => $detail['Facility']['shikutyouson'],
				'Recruit.shokushu' => $detail['Recruit']['shokushu']
			),
			'order' => array('Recruit.modified' => 'DESC'),
			'limit' => 5,
			'contain' => array(
				'RecruitShow',
				'ShisetsukeitaiSecond',
				'Kodawari',
				'Facility' => array(
					'FacilityShow',
					'ShisetsukeitaiSecond',
					'Roseneki' => array(
						'RailroadPref',
						'RailroadLine',
						'RailroadStation'
					)
				)
			)
		);
		//$news = $this->{$this->modelClass}->find('formatedSearch', $newsQuery);
		$news = $this->{$this->modelClass}->find('all', $newsQuery);
		$this->set('news', $news);

		// 掲載求人数をセット
		$activeCount = $this->{$this->modelClass}->find('count', array(
			'conditions' => array(
				$this->{$this->modelClass}->parseCriteria($this->Prg->parsedParams()),
				$this->modelClass . '.active' => 1
			),
			'contain' => array(
				'RecruitShow',
				'ShisetsukeitaiSecond',
				'Kodawari',
				'Facility' => array(
					'FacilityShow',
					'ShisetsukeitaiFirst',
					'ShisetsukeitaiSecond',
					'Roseneki' => array(
						'RailroadPref',
						'RailroadLine',
						'RailroadStation'
					)
				)
			),
		));
		
		/**
		 * 20190806 sohnishi
		 * 詳細画面のパンくずリストを作成
		 * エリアで固定
		 * TOP=>都道府県=>市区=>施設名
		 */
		$middle_breadcrumb = array();

		$todoufukens = Configure::read('todoufukens');
		$pref_key = array_search($detail['Facility']['todoufuken'], $todoufukens);
		$middle_breadcrumb[$todoufukens[$pref_key]] = '/area/pref' . $pref_key . '/';

		$cities = Configure::read('cities');
		$city_key = array_search($detail['Facility']['shikutyouson'], $cities);
		$middle_breadcrumb[$cities[$city_key]] = '/area/pref' . $pref_key . '/city' . $city_key;

		$this->set('middle_breadcrumb', $middle_breadcrumb);


		$this->set('activeCount', $activeCount);
	}

	public function search() {
		$this->set('title_for_layout', '希望条件から求人を探す');

		// 施設リスト
		$shisetsukeitaiFirsts = $this->{$this->modelClass}->Facility->ShisetsukeitaiFirst->find('list', array('order' => 'ShisetsukeitaiFirst.weight ASC'));
		$this->set('shisetsukeitaiFirsts', $shisetsukeitaiFirsts);
		// 部署リスト
		$shisetsukeitaiSeconds = $this->{$this->modelClass}->ShisetsukeitaiSecond->find('list', array('order' => 'ShisetsukeitaiSecond.weight ASC'));
		$this->set('shisetsukeitaiSeconds', $shisetsukeitaiSeconds);
		// 診療科目リスト
		$shinryoukamokus = $this->{$this->modelClass}->Facility->Shinryoukamoku->find('list', array('order' => 'Shinryoukamoku.weight ASC'));
		$this->set('shinryoukamokus', $shinryoukamokus);
		// こだわりリスト
		$kodawaris = $this->{$this->modelClass}->Kodawari->find('list', array('order' => 'Kodawari.weight ASC'));
		$this->set('kodawaris', $kodawaris);
	}

	public function results() {

		$this->layout = '';
		$condition_detail_txt = '';

		/*
			20190524 sohnishi
			$this->Prg->commonProcess($this->modelClass);で、
			paramsデータを取得できないため、
			namedとpassedArgsに直接値を入れる
		*/
		if ($this->request->is('get')) {

			$favoritesArray = array();
			$favoritesString = $_COOKIE["job-alc-kaigo-favorites"];
			if ($favoritesString) {
				$revisedString = str_replace('[', '', $favoritesString);
				$revisedString = str_replace('"', '', $revisedString);
				$revisedString = str_replace(']', '', $revisedString);
				$favoritesArray = explode(",", $revisedString);
			}

			$favoriteRecruits = $this->{$this->modelClass}->find('all', 
				array(
					'conditions' => array(
						'id' => $favoritesArray
					)
				)
			);
			
			$shapingFavoritesArray = array();
			foreach ($favoriteRecruits as $recruit) {
				if($recruit['Recruit']['active'] == 0) {
					continue;
				}
				array_push($shapingFavoritesArray, $recruit['Recruit']['id']);
			}
			
			$favoriteString = implode(",", $shapingFavoritesArray);
			setcookie('job-alc-kaigo-favorites',$favoriteString,time()+60*60*24*30, "/");
			$this->set('favoritesArray', $shapingFavoritesArray);

			$id = $this->request->params['id'];
			// paginatorに渡す用の修正前パラメータ
			$beforId = $this->request->params['id'];
			$search_pref = '全国';
			$middle_breadcrumb = array();
			$paginatorUrlArray;
			if (isset($id)) {
				$this->set('id', $id);
				$url = strstr($this->request->url, '/', true);
				$this->set('url', $url);
				
				// 施設から探す
				if ($url == 'facility') {
					// $this->request->params['named'] = array('shisetsukeitai_first' => array(0 => $id));
					$this->request->params['named'] = array_merge($this->request->params['named'], array('shisetsukeitai_first' => array($id => $id)));
					$this->passedArgs['shisetsukeitai_first'] = array($id => $id);
					$shisetsukeitaiFirst = $this->{$this->modelClass}->Facility->ShisetsukeitaiFirst->find('first', 
						array(
							'fields' => 'name',
							'conditions' => array(
								'id' => $id
							)
						)
					);
					if ($shisetsukeitaiFirst) {
						$search_pref = $shisetsukeitaiFirst['ShisetsukeitaiFirst']['name'];
						$condition_detail_txt = $condition_detail_txt . '【施設】' . $search_pref . '<br>';
						// 施設形態別コンテンツ
						$facilityContent = $this->FacilityContent->find('first', 
							array(
								'conditions' => array('FacilityContent.shisetsukeitai_first_id' => $id)
							)
						);
						$this->set('facilityContent', $facilityContent);
					}
				} else if ($url == 'employment') {
				// 勤務形態から探す
					// $this->request->params['named'] = array('kinmu_keitai' => array(0 => $id));
					$this->request->params['named'] = array_merge($this->request->params['named'], array('kinmu_keitai' => array($id => $id)));
					$this->passedArgs['kinmu_keitai'] = array($id => $id);
					
					$kinmu_keitais = Configure::read('kinmu_keitai');
					$kinmu_keitai = $kinmu_keitais[$id];
					$search_pref = $kinmu_keitai;
					$condition_detail_txt = $condition_detail_txt . '【勤務形態】' . $search_pref . '<br>';

					// 勤務形態別コンテンツ
					$employmentContent = $this->EmploymentContent->find('first', 
						array(
							'conditions' => array('EmploymentContent.employment_id' => $id)
						)
					);
					$this->set('employmentContent', $employmentContent);
					
				} else if ($url == 'area') {
				// エリアから探す
					$id = substr($id, 4);
					// pref以降が数字であるか確認
					if (ctype_digit($id)) {

						// 都道府県別コンテンツ
						$prefectureContent = $this->PrefectureContent->find('first', 
							array(
								'conditions' => array('PrefectureContent.prefecture_code' => $id)
							)
						);
						$this->set('prefectureContent', $prefectureContent);

						$todoufukens = Configure::read('todoufukens');
						$todoufuken = $todoufukens[$id];
						$this->set('todoufuken', $todoufuken);
						$search_pref = $todoufuken;

						$this->request->params['named'] = array_merge($this->request->params['named'], array('todoufuken' => $todoufuken));
						$this->passedArgs['todoufuken'] = $todoufuken;

						// 都道府県マスタを使用
						// $railroadPref = $this->RailroadPref->find('first', array(
						// 	'fields' => 'pref_name',
						// 	'conditions' => array('RailroadPref.pref_cd' => $id)
						// ));
						// $this->request->params['named'] = array('todoufuken' => $railroadPref['RailroadPref']['pref_name']);
						// $this->passedArgs['todoufuken'] = $railroadPref['RailroadPref']['pref_name'];

						$city = $this->request->params['city'];
						// paginatorに渡す用の修正前パラメータ
						$beforeCity = $this->request->params['city'];
						// cityがあれば政令都市で市区町村を絞る
						if (!empty($city)) {
							if(strpos($city,'page:') !== false){
							// page:が含まれている場合はcity検索ではない
								$city = substr($city, 5);
								// page:以降が数字であるか確認
								if (ctype_digit($city)) {
									$this->request->params['named'] = array_merge($this->request->params['named'], array('page' => $city));
									$this->passedArgs['page'] = $city;
								}

								$todoufukens = Configure::read('todoufukens');
								$todoufuken = $todoufukens[$id];
								$search_pref = $todoufuken;
								$condition_detail_txt = $condition_detail_txt . '【エリア】' . $search_pref . '<br>';

							} else {
								$this->set('isCitySearch', true);
								$this->set('city', $city);
								$city = substr($city, 4);
								// city以降が数字であるか確認
								if (ctype_digit($city)) {
									$cities = Configure::read('cities');
									$city = $cities[$city];
									$this->set('city', $city);
									// $search_pref = $search_pref . $city;

									// $todoufukens = Configure::read('todoufukens');
									// $pref_key = array_search($search_pref, $todoufukens);
									$middle_breadcrumb[$search_pref] = '/' . substr($this->request->url, 0, 12);
									// $middle_breadcrumb['url'] = $this->request->webroot . 'area/pref' . $pref_key . '/';

									$search_pref = $city;
									$condition_detail_txt = $condition_detail_txt . '【エリア】' . $search_pref . '<br>';
									// $this->request->params['named'] = array('shikutyouson' => $city);
									$this->request->params['named'] = array_merge($this->request->params['named'], array('shikutyouson' => $city));
									$this->passedArgs['shikutyouson'] = $city;
									$paginatorUrlArray = array('plugin' => $url, 'controller' => $beforId, 'action' => $beforeCity);
								}
							}							   
						} else {
							$condition_detail_txt = $condition_detail_txt . '【エリア】' . $search_pref . '<br>';
						}

					}
				} else if ($url == 'qualification') {
				// 資格から探す
					// $this->request->params['named'] = array('shikaku' => array(0 => $id));
					$this->request->params['named'] = array_merge($this->request->params['named'], array('shikaku' => array($id => $id)));
					$this->passedArgs['shikaku'] = array($id => $id);

					$shikakus = Configure::read('shikaku');
					$shikaku = $shikakus[$id];
					$search_pref = $shikaku;
					$condition_detail_txt = $condition_detail_txt . '【資格】' . $search_pref . '<br>';

					// 資格別コンテンツ
					$qualificationContent = $this->QualificationContent->find('first', 
						array(
							'conditions' => array('QualificationContent.qualification_id' => $id)
						)
					);
					$this->set('qualificationContent', $qualificationContent);

				} else if ($url == 'job') {
				// 職種から探す
					// $this->request->params['named'] = array('shokushu' => array(0 => $id));
					$this->request->params['named'] = array_merge($this->request->params['named'], array('shokushu' => array($id => $id)));
					$this->passedArgs['shokushu'] = array($id => $id);

					$jobs = Configure::read('shokushu');
					$job = $jobs[$id];
					$search_pref = $job;
					$condition_detail_txt = $condition_detail_txt . '【職種】' . $search_pref . '<br>';

					// 職種別コンテンツ
					$jobContent = $this->JobContent->find('first', 
						array(
							'conditions' => array('JobContent.job_id' => $id)
						)
					);
					$this->set('jobContent', $jobContent);

				} else if ($url == 'feature') {
				// こだわりから探す
					// $this->request->params['named'] = array('kodawari' => array(0 => $id));
					$this->request->params['named'] = array_merge($this->request->params['named'], array('kodawari' => array($id => $id)));
					$this->passedArgs['kodawari'] = array($id => $id);

					$kodawari = $this->{$this->modelClass}->Kodawari->find('first', 
						array(
							'fields' => 'name',
							'conditions' => array(
								'id' => $id
							)
						)
					);
					if ($kodawari) {
						$search_pref = $kodawari['Kodawari']['name'];
						$condition_detail_txt = $condition_detail_txt . '【条件】' . $search_pref . '<br>';
					}

				}

				if (empty($paginatorUrlArray)) {
					$paginatorUrlArray = array('plugin' => '', 'controller' => $url, 'action' => $beforId);
				}
				$this->set('paginatorUrlArray', $paginatorUrlArray);
				
			} else {
				if ($this->request->url == 'favorites') {
				// お気に入り
					$favoritesArray = array();
					$favoritesString = $_COOKIE["job-alc-kaigo-favorites"];
					if ($favoritesString) {
						$revisedString = str_replace('[', '', $favoritesString);
						$revisedString = str_replace('"', '', $revisedString);
						$revisedString = str_replace(']', '', $revisedString);
						$favoritesArray = explode(",", $revisedString);
					}
					$search_pref = 'お気に入り';
					// お気に入りがある場合
					if (count($favoritesArray) >= 1) {
						$this->request->params['named'] = array_merge($this->request->params['named'], array('id' => $favoritesArray));
						$this->passedArgs['id'] = $favoritesArray;
					} else {
						// お気に入りがない場合　件数を0にするため、存在しないIDを指定
						$this->request->params['named'] = array_merge($this->request->params['named'], array('id' => 0));
						$this->passedArgs['id'] = 0;
					}
				}
			}
		}
		
		$this->Prg->commonProcess($this->modelClass);

		if (!empty($this->request->data)) {

			// 検索条件テキスト
			if ($search_pref == '全国') {

				// 都道府県
				if (isset($this->request->data['Recruit']['todoufuken'])) {

					$search_pref = $this->request->data['Recruit']['todoufuken'];

					// 市区町村
					if (isset($this->request->data['Recruit']['shikutyouson'])) {
						// $search_pref = $search_pref . $this->request->data['Recruit']['shikutyouson'];
						$search_pref = $this->request->data['Recruit']['shikutyouson'];
						$condition_detail_txt = $condition_detail_txt . '【エリア】' . $search_pref . '<br>';
					} else {
						$condition_detail_txt = $condition_detail_txt . '【エリア】' . $search_pref . '<br>';
					}

				}

				// 路線・駅
				if (isset($this->request->data['Recruit']['pref_cd'])) {

					$railroadPref = $this->RailroadPref->find('first', array(
						'conditions' => array(
							'pref_cd' => $this->request->data['Recruit']['pref_cd']
						)
					));
					$railroadLine;
					if ($this->request->data['Recruit']['line_cd'] != null) {
						$railroadLine = $this->RailroadLine->find('first', array(
							'conditions' => array(
								'line_cd' => $this->request->data['Recruit']['line_cd']
							)
						));
					}
					$railroadStation;
					if ($this->request->data['Recruit']['station_cd'] != null) {
						$railroadStation = $this->RailroadStation->find('first', array(
							'conditions' => array(
								'station_cd' => $this->request->data['Recruit']['station_cd']
							)
						));
					}
					if ($railroadLine != null) {
						$search_pref = $railroadLine['RailroadLine']['line_name'];
						if ($railroadStation != null) {
							$search_pref = $search_pref . ' ' . $railroadStation['RailroadStation']['station_name'] . '駅';
						}
					} else {
						$search_pref = $railroadPref['RailroadPref']['pref_name'];
					}
					$condition_detail_txt = $condition_detail_txt . '【路線・駅】' . $search_pref . '<br>';

				}

				// 職種
				if (isset($this->request->data['Recruit']['shokushu'])) {
					$shokushu = '';
					$shokushuConditionIndex = 0;
					foreach ($this->request->data['Recruit']['shokushu'] as $key => $value) {
						$shokushus = Configure::read('shokushu');
						if ($shokushuConditionIndex == 0) {
							$shokushu = $shokushus[$value];
						} else {
							$shokushu = $shokushu . '、' . $shokushus[$value];
						}
						$shokushuConditionIndex = $shokushuConditionIndex + 1;
					}
					$condition_detail_txt = $condition_detail_txt . '【職種】' . $shokushu . '<br>';
				}


				// 勤務形態
				if (isset($this->request->data['Recruit']['kinmu_keitai'])) {
					$kinmuKeitai = '';
					$kinmuKeitaiConditionIndex = 0;
					foreach ($this->request->data['Recruit']['kinmu_keitai'] as $key => $value) {
						$kinmuKeitais = Configure::read('kinmu_keitai');
						if ($kinmuKeitaiConditionIndex == 0) {
							$kinmuKeitai = $kinmuKeitais[$value];
						} else {
							$kinmuKeitai = $kinmuKeitai . '、' . $kinmuKeitais[$value];
						}
						$kinmuKeitaiConditionIndex = $kinmuKeitaiConditionIndex + 1;
					}
					$condition_detail_txt = $condition_detail_txt . '【勤務形態】' . $kinmuKeitai . '<br>';
				}


				// 資格
				if (isset($this->request->data['Recruit']['shikaku'])) {
					$shikaku = '';
					$shikakuConditionIndex = 0;
					foreach ($this->request->data['Recruit']['shikaku'] as $key => $value) {
						$shikakus = Configure::read('shikaku');
						if ($shikakuConditionIndex == 0) {
							$shikaku = $shikakus[$value];
						} else {
							$shikaku = $shikaku . '、' . $shikakus[$value];
						}
						$shikakuConditionIndex = $shikakuConditionIndex + 1;
					}
					$condition_detail_txt = $condition_detail_txt . '【資格】' . $shikaku . '<br>';
				}

				// 施設業態
				if (isset($this->request->data['Recruit']['shisetsukeitai_first'])) {
					$shisetsukeitaiFirstString = '';
					$shisetsukeitaiFirstConditionIndex = 0;
					foreach ($this->request->data['Recruit']['shisetsukeitai_first'] as $key => $value) {
						$shisetsukeitaiFirst = $this->{$this->modelClass}->Facility->ShisetsukeitaiFirst->find('first', 
							array(
								'fields' => 'name',
								'conditions' => array(
									'id' => $value
								)
							)
						);
						if ($shisetsukeitaiFirst) {
							if ($shisetsukeitaiFirstConditionIndex == 0) {
								$shisetsukeitaiFirstString = $shisetsukeitaiFirst['ShisetsukeitaiFirst']['name'];
							} else {
								$shisetsukeitaiFirstString = $shisetsukeitaiFirstString . '、' . $shisetsukeitaiFirst['ShisetsukeitaiFirst']['name'];
							}
						}
						$shisetsukeitaiFirstConditionIndex = $shisetsukeitaiFirstConditionIndex + 1;
					}
					$condition_detail_txt = $condition_detail_txt . '【施設】' . $shisetsukeitaiFirstString . '<br>';
				}

				// こだわり
				if (isset($this->request->data['Recruit']['kodawari'])) {
					$kodawariString = '';
					$kodawariConditionIndex = 0;
					foreach ($this->request->data['Recruit']['kodawari'] as $key => $value) {
						$kodawari = $this->{$this->modelClass}->Kodawari->find('first', 
							array(
								'fields' => 'name',
								'conditions' => array(
									'id' => $value
								)
							)
						);
						if ($kodawari) {
							if ($kodawariConditionIndex == 0) {
								$kodawariString = $kodawari['Kodawari']['name'];
							} else {
								$kodawariString = $kodawariString . '、' . $kodawari['Kodawari']['name'];
							}
						}
						$kodawariConditionIndex = $kodawariConditionIndex + 1;
					}
					$condition_detail_txt = $condition_detail_txt . '【条件】' . $kodawariString . '<br>';
				}

				// フリーワード
				if (isset($this->request->data['Recruit']['keyword'])) {

					$condition_detail_txt = $condition_detail_txt . '【キーワード】' . $this->request->data['Recruit']['keyword'] . '<br>';

				}

			}
			$this->Paginator->settings = array(
				'conditions' => array(
						$this->{$this->modelClass}->parseCriteria($this->Prg->parsedParams()),
						$this->modelClass . '.active' => 1
				),
				'contain' => array(
					'RecruitShow',
					'ShisetsukeitaiSecond',
					'Kodawari',
					'Facility' => array(
						'FacilityShow',
						'ShisetsukeitaiFirst',
						'ShisetsukeitaiSecond',
						'Roseneki' => array(
							'RailroadPref',
							'RailroadLine',
							'RailroadStation'
						)
					)
				),
				/* 
					20190527 sohnishi
					HTMLが10件表示であったため25=>10に変更
				*/
				'limit' => 10,
				//'findType' => 'formatedSearch'
			);

			// お気に入りページはソートをかけないように修正
			if($this->request->url != 'favorites') {
				$this->Paginator->settings = array_merge($this->Paginator->settings, array('order' => array(
					$this->modelClass . '.modified' => 'DESC'
				)));
			}
			
			$results = $this->Paginator->paginate($this->modelClass);
			$this->set('results', $results);

			if (empty($paginatorUrlArray)) {
				$url = strstr($this->request->url, 'results/', false);
				$url = strstr($url, '/', false);
				$pos = strpos($url, '/page:');
				if ($pos === false) {
				} else {
					$url = substr($url, 0, $pos);
				}
				$this->set('search_param', $url);
			}
		} else {
			
			$this->Paginator->settings = array(
				'conditions' => array(
					$this->modelClass . '.active' => 1
				),
				'contain' => array(
					'RecruitShow',
					'ShisetsukeitaiSecond',
					'Kodawari',
					'Facility' => array(
						'FacilityShow',
						'ShisetsukeitaiFirst',
						'ShisetsukeitaiSecond',
						'Roseneki' => array(
							'RailroadPref',
							'RailroadLine',
							'RailroadStation'
						)
					)
				),
				'order' => array(
					$this->modelClass . '.modified' => 'DESC'
				),
				'limit' => 10,
				//'findType' => 'formatedSearch'
			);
			$results = $this->Paginator->paginate($this->modelClass);
			$this->set('results', $results);
			
		}

		// 施設形態リストを取得
		$shisetsukeitaiFirsts = $this->{$this->modelClass}->Facility->ShisetsukeitaiFirst->find('list', array('order' => 'ShisetsukeitaiFirst.weight ASC'));
		$this->set('shisetsukeitaiFirsts', $shisetsukeitaiFirsts);
		$shisetsukeitaiSeconds = $this->{$this->modelClass}->ShisetsukeitaiSecond->find('list', array('order' => 'ShisetsukeitaiSecond.weight ASC'));
		$this->set('shisetsukeitaiSeconds', $shisetsukeitaiSeconds);
		$shinryoukamokus = $this->{$this->modelClass}->Facility->Shinryoukamoku->find('list', array('order' => 'Shinryoukamoku.weight ASC'));
		$this->set('shinryoukamokus', $shinryoukamokus);
		$kodawaris = $this->{$this->modelClass}->Kodawari->find('list', array('order' => 'Kodawari.weight ASC'));
		$this->set('kodawaris', $kodawaris);
		// 検索した都道府県等
		$this->set('search_pref', $search_pref);
		// 検索した都道府県等(パンくずリストの二階層目　都道府県=>市区町村などの場合に使用)
		$this->set('middle_breadcrumb', $middle_breadcrumb);
		// 検索した条件全て
		$this->set('condition_detail_txt', $condition_detail_txt);

		// 掲載求人数をセット
		$activeCount = $this->{$this->modelClass}->find('count', array(
			'conditions' => array(
				$this->{$this->modelClass}->parseCriteria($this->Prg->parsedParams()),
				$this->modelClass . '.active' => 1
			),
			'contain' => array(
				'RecruitShow',
				'ShisetsukeitaiSecond',
				'Kodawari',
				'Facility' => array(
					'FacilityShow',
					'ShisetsukeitaiFirst',
					'ShisetsukeitaiSecond',
					'Roseneki' => array(
						'RailroadPref',
						'RailroadLine',
						'RailroadStation'
					)
				)
			),
		));
		$this->getLastQuery();
		
		$this->set('activeCount', $activeCount);
	}

	public function searchCount() {
		if ($this->request->is('ajax')) {
			
			$this->autoRender = false;
			Configure::read('debug', 0);
			$this->Prg->commonProcess($this->modelClass);
			// 現在のレコードをカウント
			$record_count = $this->{$this->modelClass}->find('count', array(
				'conditions' => array(
					$this->{$this->modelClass}->parseCriteria($this->Prg->parsedParams()),
					$this->modelClass . '.active' => 1
				),
				'contain' => array(
					'RecruitShow',
					'ShisetsukeitaiSecond',
					'Kodawari',
					'Facility' => array(
						'FacilityShow',
						'ShisetsukeitaiFirst',
						'ShisetsukeitaiSecond',
						'Roseneki' => array(
							'RailroadPref',
							'RailroadLine',
							'RailroadStation'
						)
					)
				),
			));
			$_data = $record_count;
			$this->_jsonRender($_data);
			
		}
	}

	private function getLastQuery() { //privateはあるかどうか構わないがprivateの方をおすすめ
        $dbo = $this->{$this->modelClass}->getDatasource(); //モデル名はArticleとする時
        $logData = $dbo->getLog();
        $getLog = end($logData['log']);
        $this->log($getLog['query'],LOG_DEBUG);
	}

	public function updateFavorites() {
		if ($this->request->is('ajax')) {
			
			$this->autoRender = false;
			Configure::read('debug', 0);
			$rec_id = $this->request->data['rec_id'];
			$favorites = array();
			if (array_key_exists('job-alc-kaigo-favorites', $_COOKIE)) {
				// 既存のお気に入りが存在する場合
                $favorites = explode(",", $_COOKIE['job-alc-kaigo-favorites']);
                // 重複しなければ追加を行う
				if(!($result = array_search($favorites, $rec_id))) {
					array_unshift($favorites, $rec_id);
				}
			} else {
				array_unshift($favorites, $rec_id);
			}
			
			$favoriteString = implode(",", $favorites);
			setcookie('job-alc-kaigo-favorites',$favoriteString,time()+60*60*24*30, "/");
			// $this->setcookies('job-alc-kaigo-favorites', $favorites,60*60*24*30);
			$_data = array(
				'success'
			);
			$this->_jsonRender();
			
		}
	}

	/*
	* cookie を配列で設定する
	*/
	// function setcookies($name, array $values, $lifetime) {
	// 	foreach ($values as $key => $value) {
	// 		setcookie($name . '[' . $key . ']', $value, time() + $lifetime, "/");
	// 	}
	// }
 
	public function deleteFavorite() {
		if ($this->request->is('ajax')) {
			
			$this->autoRender = false;
			Configure::read('debug', 0);
			$rec_id = $this->request->data['rec_id'];
			$favorites = explode(",", $_COOKIE['job-alc-kaigo-favorites']);
			if(($key = array_search($rec_id, $favorites)) !== false) {
				unset($favorites[$key]);
			} 
			$favoriteString = implode(",", $favorites);
			if (strlen($favoriteString) >= 1) {
				setcookie('job-alc-kaigo-favorites',$favoriteString,time()+60*60*24*30, "/");
			} else {
				setcookie('job-alc-kaigo-favorites',$favoriteString,time()-(60*60*24*30), "/");
			}
			
			$_data = array(
				'success'
			);
			$this->_jsonRender($_data);
			
		}
	}

	private function _jsonRender($_data = array()) {
		header("Content-Type: application/json; charset=utf-8");
		echo json_encode($_data);
	}
}
