<?php

App::uses('RecruitsAppController', 'Recruits.Controller');

class KyujitsusController extends RecruitsAppController {

	public $name = 'Kyujitsus';

	public $uses = array('Recruits.Kyujitsu');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('model', $this->modelClass);
		$this->set('title_for_layout', '休日体制マスタ');
		$this->set('currentUser', $this->Auth->user());

	}

	public function admin_index() {
		$this->Paginator->settings[$this->modelClass] = array(
			'order' => array($this->modelClass . '.weight' => 'ASC')
		);
		$this->set('kyujitsus', $this->Paginator->paginate());
	}

	public function admin_add() {
		if (!empty($this->request->data)) {
			$this->{$this->modelClass}->create();
			if ($this->{$this->modelClass}->save($this->request->data)) {
				$this->flashMsg('休日体制を登録しました');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->flashMsg('休日体制を登録中にエラーが発生しました', 'alert-warning');
			}
		}
		$this->set('kinmus', $this->kinmus);
	}

	public function admin_edit($id = null) {
		if (!$id || !$this->{$this->modelClass}->exists($id)) {
			$this->flashMsg('無効な操作です', 'alert-danger');
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->request->data)) {
			if ($this->{$this->modelClass}->save($this->request->data)) {
				$this->flashMsg('休日体制を更新しました');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->flashMsg('休日体制を更新中にエラーが発生しました', 'alert-warning');
			}
		} else {
			$this->request->data = $this->{$this->modelClass}->read(null, $id);
		}
		$this->set('kinmus', $this->kinmus);
	}

	public function admin_delete($id = null) {
		if (!$id || !$this->{$this->modelClass}->exists($id)) {
			$this->flashMsg('無効な操作です', 'alert-warning');
			$this->redirect(array('action' => 'index'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->{$this->modelClass}->delete($id)) {
				$this->flashMsg('休日体制を削除しました');
			} else {
				$this->flashMsg('休日体制を削除中にエラーが発生しました', 'alert-warning');
			}
		}
		$this->redirect(array('action' => 'index'));
	}
}
