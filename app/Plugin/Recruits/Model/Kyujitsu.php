<?php

App::uses('RecruitsAppModel', 'Recruits.Model');

class Kyujitsu extends RecruitsAppModel {

	public $name = 'Kyujitsu';

	public $validate = array(
		'name' => array(
			'required' => array(
				'rule' => array('notEmpty', 'name'),
				'message' => '休日を入力してください'
			)
		),
		'weight' => array(
			'numeric' => array(
				'rule' => array('numeric', 'weight'),
				'allowEmpty' => true,
				'message' => '半角数字で入力してください'
			)
		)
	);
}
