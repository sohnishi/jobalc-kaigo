<?php

App::uses('RecruitsAppModel', 'Recruits.Model');
App::uses('SerializableBehavior', 'Model/Behavior');
App::uses('SearchableBehavior', 'Search.Model/Behavior');

class Recruit extends RecruitsAppModel {

	public $name = 'Recruit';

	public $actsAs = array(
		'Serializable' => array(
			'fields' => array(
				'syakaihoken'
			)
		),
		'Search.Searchable',
	);

//	public $findMethods = array(
//		'formatedSearch' => true
//	);

	public $filterArgs = array(
		'todoufuken' => array('type' => 'subquery', 'method' => 'findByTodoufuken', 'field' => 'Recruit.facility_id', 'empty' => true),
		'shikutyouson' => array('type' => 'subquery', 'method' => 'findByShikutyouson', 'field' => 'Recruit.facility_id', 'empty' => true),
		'pref_cd' => array('type' => 'subquery', 'method' => 'findByPref', 'field' => 'Recruit.facility_id', 'empty' => true),
		'line_cd' => array('type' => 'subquery', 'method' => 'findByLine', 'field' => 'Recruit.facility_id', 'empty' => true),
		'station_cd' => array('type' => 'subquery', 'method' => 'findByStation', 'field' => 'Recruit.facility_id', 'empty' => true),
		'shikaku' => array('type' => 'value', 'empty' => true),
		/*
			20190528 sohnihsi
			職種追加
		*/
		'shokushu' => array('type' => 'value', 'empty' => true),
		'kinmu_keitai' => array('type' => 'value', 'empty' => true),
		'shisetsukeitai_first' => array('type' => 'subquery', 'method' => 'findByShisetsukeitaiFirst', 'field' => 'Recruit.facility_id', 'empty' => true),
		'shisetsukeitai_second_id' => array('type' => 'value', 'empty' => true),
		'shinryoukamoku' => array('type' => 'subquery', 'method' => 'findByShinryoukamoku', 'field' => 'Recruit.facility_id', 'empty' => true),
		'kodawari' => array('type' => 'subquery', 'method' => 'findByKodawari', 'field' => 'Recruit.id', 'empty' => true),
		'nensyu' => array('type' => 'subquery', 'method' => 'findByNensyu', 'field' => 'Recruit.id', 'empty' => true),
		'sonota_teate' => array('type' => 'like', 'empty' => true),
		'syoukyu' => array('type' => 'value', 'empty' => true),
		'syouyo_kaisu' => array('type' => 'value', 'empty' => true),
		'syouyo_jisseki' => array('type' => 'query', 'method' => 'findBySyouyoJisseki', 'empty' => true),
		'kyujitsu' => array('type' => 'subquery', 'method' => 'findByKyujitsu', 'field' => 'Recruit.id', 'empty' => true),
		'nenkan_kyujitsu' => array('type' => 'query', 'method' => 'findByNenkanKyujitsu', 'empty' => true),
		'sankyu' => array('type' => 'value', 'empty' => true),
		'ryou' => array('type' => 'value', 'empty' => true),
		'takujisyo' => array('type' => 'value', 'empty' => true),
		'tukin' => array('type' => 'value', 'empty' => true),
		'taisyokukin' => array('type' => 'value', 'empty' => true),
		'shintyaku_kyujin' => array('type' => 'value', 'empty' => true),
		'ninki_kyujin' => array('type' => 'value', 'empty' => true),
		'keyword' => array('type' => 'query', 'method' => 'findByKeyword', 'empty' => true),
		/*
			20190509 sohnishi
			求人予備項目を追加
		*/
		'recruit_preliminary_item' => array('type' => 'subquery', 'method' => 'findByRecruitPreliminaryItem', 'field' => 'Recruit.id', 'empty' => true),
		'id' => array('type' => 'value', 'empty' => true)
	);

	public $validate = array(
		'kyuyo' => array(
			'required' => array(
				'rule' => array('notBlank', 'kyuyo'),
				'message' => '給与（月給）を入力してください'
			),
		),
		'syouyo_jisseki' => array(
			'numeric' => array(
				'rule' => array('numeric', 'syouyo_jisseki'),
				'message' => '半角数字で入力してください',
				'allowEmpty' => true
			)
		),
		'nenkan_kyujitsu' => array(
			'numeric' => array(
				'rule' => array('numeric', 'nenkan_kyujitsu'),
				'message' => '半角数字で入力してください',
				'allowEmpty' => true
			),
			'maxLength' => array(
				'rule' => array('maxLength', 3),
				'message' => '半角数字3桁までです',
				'allowEmpty' => true
			)
		),
		'yukyu_syouka' => array(
			'numeric' => array(
				'rule' => array('numeric', 'yukyu_syouka'),
				'message' => '半角数字で入力してください',
				'allowEmpty' => true
			),
			'maxLength' => array(
				'rule' => array('maxLength', 3),
				'message' => '半角数字3桁までです',
				'allowEmpty' => true
			)
		),
		'csvfile' => array(
			'required' => array(
				'rule' => array('notBlank', 'csvfile'),
				'message' => 'CSVファイルを選択してください'
			)
		)
	);

	public $hasOne = array(
		'RecruitShow' => array(
			'className' => 'Recruits.RecruitShow',
			'foreignKey' => 'recruit_id',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'dependent' => true
		)
	);

	public $hasMany = array(
		'Customer' => array(
			'className' => 'Customers.Customer',
			'foreignKey' => 'recruit_id'
		)
	);

	public $hasAndBelongsToMany = array(
		'Nensyu' => array(
			'className' => 'Recruits.Nensyu',
			'joinTable' => 'recruits_nensyus',
			'foreignKey' => 'recruit_id',
			'associationForeignKey' => 'nensyu_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => 'Nensyu.weight ASC',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Kyujitsu' => array(
			'className' => 'Recruits.Kyujitsu',
			'joinTable' => 'recruits_kyujitsus',
			'foreignKey' => 'recruit_id',
			'associationForeignKey' => 'kyujitsu_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => 'Kyujitsu.weight ASC',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Kodawari' => array(
			'className' => 'Recruits.Kodawari',
			'joinTable' => 'recruits_kodawaris',
			'foreignKey' => 'recruit_id',
			'associationForeignKey' => 'kodawari_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => 'Kodawari.weight ASC',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
		,
		/*
			20190513 sohnishi
			求人予備項目を追加
		*/
		'RecruitPreliminaryItem' => array(
			'className' => 'Recruits.RecruitPreliminaryItem',
			'joinTable' => 'recruits_recruit_preliminary_items',
			'foreignKey' => 'recruit_id',
			'associationForeignKey' => 'recruit_preliminary_item_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => 'RecruitPreliminaryItem.id ASC',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);

	public $belongsTo = array(
		'Facility' => array(
			'className' => 'Facilities.Facility',
			'foreignKey' => 'facility_id',
			'conditions' => '',
			'type' => '',
			'fields' => '',
			'order' => '',
			'counterCache' => true,
			'counterScope' => array('Recruit.active' => 1)
		),
		'ShisetsukeitaiSecond' => array(
			'className' => 'Facilities.ShisetsukeitaiSecond',
			'foreignKey' => 'shisetsukeitai_second_id',
			'conditions' => '',
			'type' => '',
			'fields' => '',
			'order' => '',
			'counterCache' => ''
		)
	);

	public function findByTodoufuken($data = array()) {
		$query = $this->Facility->getQuery('all', array(
			'conditions' => array(
				'todoufuken LIKE' => '%' . $data['todoufuken'] . '%'
			),
			'fields' => array(
				'id'
			)
		));
		return $query;
	}

	public function findByShikutyouson($data = array()) {
		$query = $this->Facility->getQuery('all', array(
			'conditions' => array(
				'shikutyouson LIKE' => '%' . $data['shikutyouson'] . '%'
			),
			'fields' => array(
				'id'
			)
		));
		return $query;
	}

	public function findByPref($data = array()) {
		$query = $this->Facility->Roseneki->getQuery('all', array(
			'conditions' => array(
				'pref_cd' => $data['pref_cd']
			),
			'fields' => array(
				'facility_id'
			)
		));
		return $query;
	}

	public function findByLine($data = array()) {
		$query = $this->Facility->Roseneki->getQuery('all', array(
			'conditions' => array(
				'line_cd' => $data['line_cd']
			),
			'fields' => array(
				'facility_id'
			)
		));
		return $query;
	}

	public function findByStation($data = array()) {
		$query = $this->Facility->Roseneki->getQuery('all', array(
			'conditions' => array(
				'station_cd' => $data['station_cd']
			),
			'fields' => array(
				'facility_id'
			)
		));
		return $query;
	}

	public function findByShisetsukeitaiFirst($data = array()) {
		$this->Facility->FacilitiesShisetsukeitaiFirst->Behaviors->attach('Containable', array('autoFields' => false));
		$this->Facility->FacilitiesShisetsukeitaiFirst->Behaviors->attach('Search.Searchable');
		$query = $this->Facility->FacilitiesShisetsukeitaiFirst->getQuery('all', array(
			'conditions' => array(
				'shisetsukeitai_first_id' => $data['shisetsukeitai_first']
			),
			'fields' => array(
				'facility_id'
			)
		));
		return $query;
	}

	public function findByShinryoukamoku($data = array()) {
		$this->Facility->FacilitiesShinryoukamokus->Behaviors->attach('Containable', array('autoFields' => false));
		$this->Facility->FacilitiesShinryoukamokus->Behaviors->attach('Search.Searchable');
		$query = $this->Facility->FacilitiesShinryoukamokus->getQuery('all', array(
			'conditions' => array(
				'shinryoukamoku_id' => $data['shinryoukamoku']
			),
			'fields' => array(
				'facility_id'
			)
		));
		return $query;
	}

	public function findByKodawari($data = array()) {
		$this->RecruitsKodawari->Behaviors->attach('Containable', array('autoFields' => false));
		$this->RecruitsKodawari->Behaviors->attach('Search.Searchable');
		$query = $this->RecruitsKodawari->getQuery('all', array(
			'conditions' => array(
				'kodawari_id' => $data['kodawari']
			),
			'fields' => array(
				'recruit_id'
			)
		));
		return $query;
	}

	public function findByNensyu($data = array()) {
		$this->RecruitsNensyus->Behaviors->attach('Containable', array('autoFields' => false));
		$this->RecruitsNensyus->Behaviors->attach('Search.Searchable');
		$query = $this->RecruitsNensyus->getQuery('all', array(
			'conditions' => array(
				'nensyu_id' => $data['nensyu']
			),
			'fields' => array(
				'recruit_id'
			)
		));
		return $query;
	}

	public function findBySyouyoJisseki($data = array()) {
		$find = array(
			array($this->alias . '.syouyo_jisseki <=' => '2.0'),
			array($this->alias . '.syouyo_jisseki BETWEEN ? AND ?' => array('2.1', '2.9')),
			array($this->alias . '.syouyo_jisseki BETWEEN ? AND ?' => array('3.0', '3.9')),
			array($this->alias . '.syouyo_jisseki >=' => '4.0'),
		);
		$query['OR'] = array();
		foreach ($data['syouyo_jisseki'] as $val) {
			$query['OR'][] = $find[$val];
		}
		return $query;
	}

	public function findByKyujitsu($data = array()) {
		$this->RecruitsKyujitsus->Behaviors->attach('Containable', array('autoField' => false));
		$this->RecruitsKyujitsus->Behaviors->attach('Search.Searchable');
		$query = $this->RecruitsKyujitsus->getQuery('all', array(
			'conditions' => array(
				'kyujitsu_id' => $data['kyujitsu']
			),
			'fields' => array(
				'recruit_id'
			)
		));
		return $query;
	}

	public function findByNenkanKyujitsu($data = array()) {
		$find = array(
			array($this->alias . '.nenkan_kyujitsu <=' => '100'),
			array($this->alias . '.nenkan_kyujitsu BETWEEN ? AND ?' => array('101', '109')),
			array($this->alias . '.nenkan_kyujitsu BETWEEN ? AND ?' => array('110', '119')),
			array($this->alias . '.nenkan_kyujitsu >=' => '120')
		);
		$query['OR'] = array();
		foreach ($data['nenkan_kyujitsu'] as $val) {
			$query['OR'][] = $find[$val];
		}
		return $query;
	}

	public function findByKeyword($data = array()) {
		$query = array(
			'OR' => array(
				array($this->alias . '.shigoto LIKE' => '%' . $data['keyword'] . '%'),
				array('Facility.shisetsumei LIKE' => '%' . $data['keyword'] . '%'),
				array('Facility.group LIKE' => '%' . $data['keyword'] . '%'),
				array('Facility.corporation LIKE' =>'%' . $data['keyword'] . '%'),
				array('Facility.todoufuken LIKE' => '%' . $data['keyword'] . '%'),
				array('Facility.shikutyouson LIKE' => '%' . $data['keyword'] . '%')
			)
		);
		return $query;
	}

	/*
		20190513 sohnishi
		求人予備項目を追加
	 */
	public function findByRecruitPreliminaryItem($data = array()) {

		$this->RecruitsRecruitPreliminaryItem->Behaviors->attach('Containable', array('autoFields' => false));
		$this->RecruitsRecruitPreliminaryItem->Behaviors->attach('Search.Searchable');

		$conditions = array();
        if(isset($data['recruit_preliminary_item'])){
            foreach ($data['recruit_preliminary_item'] as $recruit_preliminary_item) {
				if(!empty($recruit_preliminary_item)){
					$conditions['OR']['content LIKE'] = '%' . $recruit_preliminary_item . '%';
				}
            }
        }
		
		$query = $this->RecruitsRecruitPreliminaryItem->getQuery('all', array(
			'conditions' => $conditions,
			'fields' => array(
				'recruit_id'
			)
		));
		return $query;

	}

/*
	protected function _findFormatedSearch($state, $query, $results = array()) {
		if ($state == 'before') {
			$this->Behaviors->load('Containable', array('autoFields' => false));
			if (isset($query['operation']) && $query['operation'] == 'count') {
				$query['fields'] = array('COUNT(DISTINCT ' . $this->alias . '.id)');
			}
			return $query;
		} elseif ($state == 'after') {
			if (isset($query['operation']) && $query['operation'] == 'count') {
				if (isset($query['group']) && is_array($query['group']) && !empty($query['group'])) {
					return count($results);
				}
				return $results[0][0]['COUNT(DISTINCT ' . $this->alias . '.id)'];
			} else {
				$data = array();
				$facilities = array();
				foreach ($results as $key => $val) {
					$facilities[$val['Facility']['id']]['Facility'] = $val['Facility'];
					unset($results[$key]['Facility']);
				}
				foreach ($facilities as $id => $facility) {
					$data[$id] = $facility;
					foreach ($results as $recruit) {
						if ($id == $recruit['Recruit']['facility_id']) {
							$data[$id]['Facility']['Recruit'][] = $recruit;
						}
					}
				}
				return $data;
			}
			return $results;
		}
	}

	public function paginate($conditions, $fields, $order, $limit, $page = 1, $recursive = null, $extra = array()) {
		$tmpParams = compact('conditions', 'fields', 'order', 'limit', 'page', 'recursive', 'extra');
		$parameters = array_merge($tmpParams, $extra);
		if ($recursive != $this->recursive) {
			$parameters['recursive'] = $recursive;
		}
		// custom find 切り替え処理
		if (isset($extra['type']) && isset($this->findMethods[$extra['type']])) {
			return $this->find($extra['type'], $parameters);
		} else {
			return $this->find('all', $parameters);
		}
	}

	public function paginateCount($conditions = array(), $recursive = 0, $extra = array()) {
		$parameters = compact('conditions');
		if ($recursive != $this->recursive) {
			$parameters['recursive'] = $recursive;
		}
		// custom find 切り替え処理
		if (isset($extra['type']) && isset($this->findMethods[$extra['type']])) {
			$extra['operation'] = 'count';
			return $this->find($extra['type'], array_merge($parameters, $extra));
		} else {
			return $this->find('count', array_merge($parameters, $extra));
		}
	}
 */
}
