<?php

App::uses('RecruitsAppModel', 'Recruits.Model');

class Kodawari extends RecruitsAppModel {

	public $name = 'Kodawari';

	public $validate = array(
		'name' => array(
			'required' => array(
				'rule' => array('notEmpty', 'name'),
				'required' => true,
				'allowEmpty' => false,
				'message' => 'こだわり名を入力してください'
			)
		),
		'weight' => array(
			'numeric' => array(
				'rule' => array('numeric', 'weight'),
				'allowEmpty' => true,
				'message' => '半角数字で入力してください'
			)
		)
	);

	public $hasAndBelongsToMany = array(
		'Recruit' => array(
			'className' => 'Recruits.Recruit',
			'joinTable' => 'recruits_kodawaris',
			'foreignKey' => 'kodawari_id',
			'associationForeignKey' => 'recruit_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);
}
