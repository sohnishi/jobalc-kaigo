<?php

App::uses('RecruitsAppModel', 'Recruits.Model');

class RecruitPreliminaryItem extends RecruitsAppModel {

	public $name = 'RecruitPreliminaryItem';

	public $validate = array(
		'name' => array(
			'required' => array(
				'rule' => array('notBlank'),
				'required' => true,
				'notBlank' => false,
				'message' => '求人予備項目名を入力してください'
			)
		)
	);
}
