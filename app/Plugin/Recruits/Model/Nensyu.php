<?php

App::uses('RecruitsAppModel', 'Recruits.Model');

class Nensyu extends RecruitsAppModel {

	public $name = 'Nensyu';

	public $validate = array(
		'name' => array(
			'required' => array(
				'rule' => array('notEmpty', 'name'),
				'message' => '給与（年収）を入力してください'
			)
		),
		'weight' => array(
			'numeric' => array(
				'rule' => array('numeric', 'weight'),
				'allowEmpty' => true,
				'message' => '半角数字で入力してください'
			)
		)
	);

	public $hasAndBelongsToMany = array(
		'Recruit' => array(
			'className' => 'Recruit',
			'joinTable' => 'recruits_nensyus',
			'foreignKey' => 'nensyu_id',
			'associationForeignKey' => 'recruit_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);
}
