<?php

App::uses('CustomersAppModel', 'Customers.Model');

class CustomerRoseneki extends CustomersAppModel {

	public $name = 'CustomerRoseneki';

	public $validate = array(
		'pref_cd' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'message' => '都道府県を選択してください'
			)
		),
		'line_cd' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'message' => '最寄り路線を選択してください'
			)
		),
		'station_cd' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'message' => '最寄り駅を選択してください'
			)
		)
	);

	public $belongsTo = array(
		'RailroadPref' => array(
			'className' => 'Ekidata.RailroadPref',
			'foreignKey' => 'pref_cd',
		),
		'RailroadLine' => array(
			'className' => 'Ekidata.RailroadLine',
			'foreignKey' => 'line_cd',
		),
		'RailroadStation' => array(
			'className' => 'Ekidata.RailroadStation',
			'foreignKey' => 'station_cd',
		),
	);
}
