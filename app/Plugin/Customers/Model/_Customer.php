<?php

App::uses('CustomersAppModel', 'Customers.Model');
App::uses('CakeEmail', 'Network/Email');

class Customer extends CustomersAppModel {

	public $name = 'Customer';

	public $actsAs = array(
		'Search.Searchable'
	);

	public $validate = array(
		'name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => '名前を入力してください'
			)
		),
		'hurigana' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'フリガナを入力してください'
			),
			'katakana' => array(
				'rule' => 'katakana',
				'message' => 'カタカナで入力してください'
			)
		),
		'keitai_tel' => array(
			//'rule' => array('custom', '/^(0\d{1,4}-\d{1,4}-\d{4})$/'),
			// ハイフン不問
			'rule' => array('custom', '/^(0\d{1,4}[\s-]?\d{1,4}[\s-]?\d{4})$/'),
			'allowEmpty' => true,
			'message' => '電話番号の入力形式が違います'
		),
		'jitaku_tel' => array(
			//'rule' => array('custom', '/^(0\d{1,4}-\d{1,4}-\d{4})$/'),
			// ハイフン不問
			'rule' => array('custom', '/^(0\d{1,4}[\s-]?\d{1,4}[\s-]?\d{4})$/'),
			'allowEmpty' => true,
			'message' => '電話番号の入力形式が違います'
		),
		'email' => array(
			'email' => array(
				//'rule' => 'email',
				'rule' => array('custom', '/^[_a-z0-9-]+(\.[_a-z0-9+-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i'),
				'allowEmpty' => true,
				'message' => '有効なメールアドレスを入力してください'
			)
		)
	);

	public $filterArgs = array(
		'number' => array('type' => 'value', 'empty' => true),
		'user_id' => array('type' => 'value', 'empty' => true),
		'rank' => array('type' => 'value', 'empty' => true),
		'touroku_keiro' => array('type' => 'value', 'empty' => true),
		'marital_status' => array('type' => 'value', 'empty' => true),
		'syoukai' => array('type' => 'like', 'field' => 'Customer.syoukai', 'empty' => true),
		'hurigana' => array('type' => 'like', 'field' => 'Customer.hurigana', 'empty' => true),
		'name' => array('type' => 'like', 'field' => 'Customer.name', 'empty' => true),
		'age' => array('type' => 'query', 'method' => 'findByAge', 'empty' => true),
		'keitai_tel' => array('type' => 'like', 'field' => 'keitai_tel', 'empty' => true),
		'jitaku_tel' => array('type' => 'like', 'field' => 'jitaku_tel', 'empty' => true),
		'email' => array('type' => 'like', 'field' => 'Customer.email', 'empty' => true),
		'zip' => array('type' => 'like', 'field' => 'Customer.zip', 'empty' => true),
		'todoufuken' => array('type' => 'like', 'field' => 'Customer.todoufuken', 'empty' => true),
		'shikutyouson' => array('type' => 'like', 'field' => 'Customer.shikutyouson', 'empty' => true),
		'banchi' => array('type' => 'like', 'field' => 'Customer.banchi', 'empty' => true),
		'pref_cd' => array('type' => 'subquery', 'method' => 'findByCustomerRosenekiPref', 'field' => 'Customer.id', 'empty' => true),
		'line_cd' => array('type' => 'subquery', 'method' => 'findByCustomerRosenekiLine', 'field' => 'Customer.id', 'empty' => true),
		'station_cd' => array('type' => 'subquery', 'method' => 'findByCustomerRosenekiStation', 'field' => 'Customer.id', 'empty' => true),
		'distance' => array('type' => 'subquery', 'method' => 'findByCustomerRosenekiDistance', 'field' => 'Customer.id', 'empty' => true),
		'memo' => array('type' => 'like', 'field' => 'Customer.memo', 'empty' => true),
		'shikaku' => array('type' => 'subquery', 'method' => 'findByShikaku', 'field' => 'Customer.id', 'empty' => true),
		'corporation' => array('type' => 'subquery', 'method' => 'findByCorporation', 'field' => 'Customer.id', 'empty' => true),
		'shisetsukeitai_first' => array('type' => 'subquery', 'method' => 'findByShisetsukeitaiFirst', 'field' => 'Customer.id', 'empty' => true),
		'shisetsukeitai_second' => array('type' => 'subquery', 'method' => 'findByShisetsukeitaiSecond', 'field' => 'Customer.id', 'empty' => true),
		'shinryoukamoku' => array('type' => 'subquery', 'method' => 'findByShinryoukamoku', 'field' => 'Customer.id', 'empty' => true),
		'kinmu_keitai' => array('type' => 'subquery', 'method' => 'findByKinmuKeitai', 'field' => 'Customer.id', 'empty' => true),
		'koyoukeitai' => array('type' => 'subquery', 'method' => 'findByKoyoukeitai', 'field' => 'Customer.id', 'empty' => true),
		'kibou_busyo' => array('type' => 'subquery', 'method' => 'findByKibouBusyo', 'field' => 'Customer.id', 'empty' => true),
		'kinmukaishijiki' => array('type' => 'subquery', 'method' => 'findByKinmukaishijiki', 'field' => 'Customer.id', 'empty' => true),
		'tsukinhouhou' => array('type' => 'subquery', 'method' => 'findByTsukinhouhou', 'field' => 'Customer.id', 'empty' => true),
		'tsukinjikan' => array('type' => 'subquery', 'method' => 'findByTsukinjikan', 'field' => 'Customer.id', 'empty' => true),
		'kiboukyuyo' => array('type' => 'subquery', 'method' => 'findByKiboukyuyo', 'field' => 'Customer.id', 'empty' => true),
		'kyujitsu_id' => array('type' => 'subquery', 'method' => 'findByKyujitsu', 'field' => 'Customer.id', 'empty' => true),
		'ryou' => array('type' => 'subquery', 'method' => 'findByRyou', 'field' => 'Customer.id', 'empty' => true),
		'takujisyo' => array('type' => 'subquery', 'method' => 'findByTakujisyo', 'field' => 'Customer.id', 'empty' => true)
	);

	public function katakana($check) {
		return preg_match("/^[ァ-ヾ　 ]+$/u", $check['hurigana']);
	}

	public function afterSave($created, $options = array()) {
		if ($created) {
			// 施設IDの重複を無くすためMySQLのオートインクリメント値を使う
			if (!isset($this->data[$this->alias]['number']) && empty($this->data[$this->alias]['number'])) {
				$this->saveField('number', $this->getLastInsertID());
			}
		}
	}

	public $belongsTo = array(
		'User' => array(
			'className' => 'Users.User',
			'foreignKey' => 'user_id'
		),
		'Recruit' => array(
			'className' => 'Recruits.Recruit',
			'foreignKey' => 'recruit_id',
			'counterCache' => true
		)
	);

	public $hasMany = array(
		'CustomerRoseneki' => array(
			'className' => 'Customers.CustomerRoseneki',
			'foreignKey' => 'customer_id',
			'dependent' => true
		),
		'CustomerCertificate' => array(
			'className' => 'Customers.CustomerCertificate',
			'foreignKey' => 'customer_id',
			'dependent' => true
		),
		'CustomerCondition' => array(
			'className' => 'Customers.CustomerCondition',
			'foreignKey' => 'customer_id',
			'dependent' => true
		),
		'CustomerCareer' => array(
			'className' => 'Customers.CustomerCareer',
			'foreignKey' => 'customer_id',
			'dependent' => true
		)
	);

	public function findByAge($data = array()) {
		$age = date('Y') - $data['age'];
		$query = array($this->modelClass . 'birth LIKE' => $age . '%');
		return $query;
	}

	public function findByCustomerRosenekiPref($data = array()) {
		$this->CustomerRoseneki->Behaviors->attach('Containable', array('autoFields' => false));
		$this->CustomerRoseneki->Behaviors->attach('Search.Searchable');
		$query = $this->CustomerRoseneki->getQuery('all', array(
			'conditions' => array(
				'CustomerRoseneki.pref_cd' => $data['pref_cd']
			),
			'fields' => array('customer_id')
		));
		return $query;
	}

	public function findByCustomerRosenekiLine($data = array()) {
		$this->CustomerRoseneki->Behaviors->attach('Containable', array('autoFields' => false));
		$this->CustomerRoseneki->Behaviors->attach('Search.Searchable');
		$query = $this->Roseneki->getQuery('all', array(
			'conditions' => array(
				'CustomerRoseneki.line_cd' => $data['line_cd']
			),
			'fields' => array('customer_id')
		));
		return $query;
	}

	public function findByCustomerRosenekiStation($data = array()) {
		$this->CustomerRoseneki->Behaviors->attach('Containable', array('autoFields' => false));
		$this->CustomerRoseneki->Behaviors->attach('Search.Searchable');
		$query = $this->CustomerRoseneki->getQuery('all', array(
			'conditions' => array(
				'CustomerRoseneki.station_cd' => $data['station_cd']
			),
			'fields' => array('customer_id')
		));
		return $query;
	}

	public function findByCustomerRosenekiDistance($data = array()) {
		$this->CustomerRoseneki->Behaviors->attach('Containable', array('autoFields' => false));
		$this->CustomerRoseneki->Behaviors->attach('Search.Searchable');
		$query = $this->CustomerRoseneki->getQuery('all', array(
			'conditions' => array(
				'CustomerRoseneki.distance' => $data['distance']
			),
			'fields' => array('customer_id')
		));
		return $query;
	}

	public function findByShikaku($data = array()) {
		$this->CustomerCertificate->Behaviors->attach('Containable', array('autoFields' => false));
		$this->CustomerCertificate->Behaviors->attach('Search.Searchable');
		$query = $this->CustomerCertificate->getQuery('all', array(
			'conditions' => array(
				'CustomerCertificate.shikaku' => $data['shikaku']
			),
			'fields' => array(
				'customer_id'
			)
		));
		return $query;
	}

	public function findByCorporation($data = array()) {
		$this->CustomerCareer->Behaviors->attach('Containable', array('autoFields' => false));
		$this->CustomerCareer->Behaviors->attach('Search.Searchable');
		$query = $this->CustomerCareer->getQuery('all', array(
			'conditions' => array(
				'CustomerCareer.corporation LIKE' => '%' . $data['corporation'] . '%'
			),
			'fields' => array(
				'customer_id'
			)
		));
		return $query;
	}

	public function findByShisetsukeitaiFirst($data = array()) {
		$this->CustomerCareer->Behaviors->attach('Containable', array('autoFields' => false));
		$this->CustomerCareer->Behaviors->attach('Search.Searchable');
		$query = $this->CustomerCareer->getQuery('all', array(
			'conditions' => array(
				'CustomerCareer.shisetsukeitai_first_id' => $data['shisetsukeitai_first']
			),
			'fields' => array(
				'customer_id'
			)
		));
		return $query;
	}

	public function findByShisetsukeitaiSecond($data = array()) {
		$this->CustomerCareer->Behaviors->attach('Containable', array('autoFields' => false));
		$this->CustomerCareer->Behaviors->attach('Search.Searchable');
		$query = $this->CustomerCareer->getQuery('all', array(
			'conditions' => array(
				'CustomerCareer.shisetsukeitai_second_id' => $data['shisetsukeitai_second']
			),
			'fields' => array(
				'customer_id'
			)
		));
		return $query;
	}

	public function findByShinryoukamoku($data = array()) {
		$this->CustomerCareer->Behaviors->attach('Containable', array('autoFields' => false));
		$this->CustomerCareer->Behaviors->attach('Search.Searchable');
		$query = $this->CustomerCareer->getQuery('all', array(
			'conditions' => array(
				'CustomerCareer.shinryoukamoku_id' => $data['shinryoukamoku']
			),
			'fields' => array(
				'customer_id'
			)
		));
		return $query;
	}

	public function findByKinmuKeitai($data = array()) {
		$this->CustomerCareer->Behaviors->attach('Containable', array('autoFields' => false));
		$this->CustomerCareer->Behaviors->attach('Search.Searchable');
		$query = $this->CustomerCareer->getQuery('all', array(
			'conditions' => array(
				'CustomerCareer.kinmu_keitai' => $data['kinmu_keitai']
			),
			'fields' => array(
				'customer_id'
			)
		));
		return $query;
	}

	public function findByKoyoukeitai($data = array()) {
		$this->CustomerCondition->Behaviors->attach('Containable', array('autoFields' => false));
		$this->CustomerCondition->Behaviors->attach('Search.Searchable');
		$query = $this->CustomerCondition->getQuery('all', array(
			'conditions' => array(
				'CustomerCondition.koyoukeitai' => $data['koyoukeitai']
			),
			'fields' => array(
				'customer_id'
			)
		));
		return $query;
	}

	public function findByKibouBusyo($data = array()) {
		$this->CustomerCondition->Behaviors->attach('Containable', array('autoFields' => false));
		$this->CustomerCondition->Behaviors->attach('Search.Searchable');
		$query = $this->CustomerCondition->getQuery('all', array(
			'conditions' => array(
				'CustomerCondition.shisetsukeitai_second_id' => $data['kibou_busyo']
			),
			'fields' => array(
				'customer_id'
			)
		));
		return $query;
	}

	public function findByKinmukaishijiki($data = array()) {
		$this->CustomerCondition->Behaviors->attach('Containable', array('autoFields' => false));
		$this->CustomerCondition->Behaviors->attach('Search.Searchable');
		$query = $this->CustomerCondition->getQuery('all', array(
			'conditions' => array(
				'CustomerCondition.kinmukaishijiki' => $data['kinmukaishijiki']
			),
			'fields' => array(
				'customer_id'
			)
		));
		return $query;
	}

	public function findByTsukinhouhou($data = array()) {
		$this->CustomerCondition->Behaviors->attach('Containable', array('autoFields' => false));
		$this->CustomerCondition->Behaviors->attach('Search.Searchable');
		$query = $this->CustomerCondition->getQuery('all', array(
			'conditions' => array(
				'CustomerCondition.tsukinhouhou' => $data['tsukinhouhou']
			),
			'fields' => array(
				'customer_id'
			)
		));
		return $query;
	}

	public function findByTsukinjikan($data = array()) {
		$this->CustomerCondition->Behaviors->attach('Containable', array('autoFields' => false));
		$this->CustomerCondition->Behaviors->attach('Search.Searchable');
		$query = $this->CustomerCondition->getQuery('all', array(
			'conditions' => array(
				'CustomerCondition.tsukinjikan' => $data['tsukinjikan']
			),
			'fields' => array(
				'customer_id'
			)
		));
		return $query;
	}

	public function findByKiboukyuyo($data = array()) {
		$this->CustomerCondition->Behaviors->attach('Containable', array('autoFields' => false));
		$this->CustomerCondition->Behaviors->attach('Search.Searchable');
		$query = $this->CustomerCondition->getQuery('all', array(
			'conditions' => array(
				'CustomerCondition.kiboukyuyo' => $data['kiboukyuyo']
			),
			'fields' => array(
				'customer_id'
			)
		));
		return $query;
	}

	public function findByKyujitsu($data = array()) {
		$this->CustomerCondition->Behaviors->attach('Containable', array('autoFields' => false));
		$this->CustomerCondition->Behaviors->attach('Search.Searchable');
		$query = $this->CustomerCondition->getQuery('all', array(
			'conditions' => array(
				'CustomerCondition.kyujitsu_id' => $data['kyujitsu_id']
			),
			'fields' => array(
				'customer_id'
			)
		));
		return $query;
	}

	public function findByRyou($data = array()) {
		$this->CustomerCondition->Behaviors->attach('Containable', array('autoFields' => false));
		$this->CustomerCondition->Behaviors->attach('Search.Searchable');
		$query = $this->CustomerCondition->getQuery('all', array(
			'conditions' => array(
				'CustomerCondition.ryou' => $data['ryou']
			),
			'fields' => array(
				'customer_id'
			)
		));
		return $query;
	}

	public function findByTakujisyo($data = array()) {
		$this->CustomerCondition->Behaviors->attach('Containable', array('autoFields' => false));
		$this->CustomerCondition->Behaviors->attach('Search.Searchable');
		$query = $this->CustomerCondition->getQuery('all', array(
			'conditions' => array(
				'CustomerCondition.takujisyo' => $data['takujisyo']
			),
			'fields' => array(
				'customer_id'
			)
		));
		return $query;
	}

	public function formDataSave($data = array()) {
		// 顧客担当は管理者に固定
		$data['Customer']['user_id'] = '1';
		// 生年月日整形
		if (!empty($data['Customer']['birth'])) {
			$birth = $data['Customer']['birth'];
			unset($data['Customer']['birth']);
			$data['Customer']['birth']['year'] = $birth['year']['year'];
			$data['Customer']['birth']['month'] = $birth['month']['month'];
			$data['Customer']['birth']['day'] = $birth['day']['day'];
		}
		// 保有資格整形
		if (!empty($data['Customer']['shikaku'])) {
			foreach ($data['Customer']['shikaku'] as $shikaku) {
				$data['CustomerCertificate'][]['shikaku'] = $shikaku;
			}
			unset($data['Customer']['shikaku']);
		}
		// 希望の働き方整形
		// マルチチェックボックスから文字列に変換
		$kibouworkOpt = Configure::read('kibouwork');
		$kibouwork = '';
		$tmpKibouwork = $data['Customer']['kibouwork'];
		if (!empty($data['Customer']['kibouwork'])) {

			foreach ($data['Customer']['kibouwork'] as $val) {
				$kibouwork = $kibouwork . $kibouworkOpt[$val];
				if (next($tmpKibouwork)) {
					$kibouwork = $kibouwork . "、";
				}
			}
			$data['CustomerCondition'][0]['kibouwork'] = $kibouwork;
			$data['CustomerCondition'][0]['kinmukaishijiki'] = $data['Customer']['kinmukaishijiki'];
			unset($data['Customer']['kibouwork']);
			unset($data['Customer']['kinmukaishijiki']);
		}

		$this->set($data);
		if ($this->saveAll($data, array('validate' => false))) {
			// 運営者宛に送信
			$this->__siteSend($data);
			// 申込者宛に送信
			$this->__customerSend($data);
			return true;
		}
		return false;
	}

	private function __siteSend($data) {
		$email = new CakeEmail();
		$email->config('contact')
				->viewVars($data);
		$email->send();
	}

	private function __customerSend($data) {
		if (!empty($data['Customer']['email'])) {
			$email = new CakeEmail();
			$email->config('reply')
					->to($data['Customer']['email'])
					->subject($data['Customer']['name'] . '様　ご登録ありがとうございます。【ジョブアルク転職サポート（運営：株式会社ALC）】')
					->viewVars($data);
			$email->send();
		}
	}
}
