<?php

App::uses('CustomersAppModel', 'Customers.Model');

class CustomerCareer extends CustomersAppModel {

	public $name = 'CustomerCareer';

	public $validate = array(
		'corporation' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'message' => '勤務先を入力してください'
			)
		),
//		'shisetsukeitai_first_id' => array(
//			'notEmpty' => array(
//				'rule' => 'notEmpty',
//				'message' => 'エントリーを選択してください'
//			)
//		),
		'shisetsukeitai_second_id' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'message' => '配属先を選択してください'
			)
		),
//		'shinryoukamoku_id' => array(
//			'notEmpty' => array(
//				'rule' => 'notEmpty',
//				'message' => '科目を選択してください'
//			)
//		)
	);

	public $belongsTo = array(
		'ShisetsukeitaiFirst' => array(
			'className' => 'Facilities.ShisetsukeitaiFirst',
			'foreign_key' => 'shisetsukeitai_first_id'
		),
		'ShisetsukeitaiSecond' => array(
			'className' => 'Facilities.ShisetsukeitaiSecond',
			'foreign_key' => 'shisetsukeitai_second_id'
		),
	);
}
