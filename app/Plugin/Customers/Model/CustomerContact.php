<?php

App::uses('CustomersAppModel', 'Customers.Model');

class CustomerContact extends CustomersAppModel {

	public $name = 'CustomerContact';

	public $validate = array(
		'user_id' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'message' => '担当者を選択してください'
			)
		),
		'houhou' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'message' => '連絡方法を選択してください'
			),
		),
		'syousai' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'message' => '連絡詳細を入力してください'
			)
		)
	);


	public $hasMany = array(
		'ContactsCustomerContact' => array(
			'className' => 'Contacts.ContactsCustomerContact',
			'foreignKey' => 'customer_contact_id',
			'conditions' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'dependent' => true,
			'exclusive' => '',
			'finderQuery' => ''
		),
	);

	// public $hasAndBelongsToMany = array(
	// 	/*
	// 		20190522 sohnishi
	// 		顧客連絡履歴を追加
	// 	*/
	// 	'Contact' => array(
	// 		'className' => 'Contacts.Contact',
	// 		'joinTable' => 'contacts_customer_contacts',
	// 		'foreignKey' => 'customer_contact_id',
	// 		'associationForeignKey' => 'contact_id',
	// 		'unique' => true,
	// 		'conditions' => '',
	// 		'fields' => '',
	// 		'order' => 'Contact.id ASC',
	// 		'limit' => '',
	// 		'offset' => '',
	// 		'finderQuery' => '',
	// 		'deleteQuery' => '',
	// 		'insertQuery' => ''
	// 	)
	// );
}
