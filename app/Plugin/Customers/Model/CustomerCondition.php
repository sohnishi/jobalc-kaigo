<?php

App::uses('CustomersAppModel', 'Customers.Model');

class CustomerCondition extends CustomersAppModel {

	public $name = 'CustomerCondition';

	public $validate = array(
		'koyoukeitai' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'message' => '雇用形態を選択してください'
			)
		),
		'shisetsukeitai_second_id' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'message' => '希望部署を選択してください'
			)
		),
		'kinmukaishijiki' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'message' => '勤務開始時期を選択してください'
			)
		)
	);
}
