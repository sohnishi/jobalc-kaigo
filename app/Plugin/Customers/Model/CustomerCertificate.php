<?php

App::uses('CustomersAppModel', 'Customers.Model');

class CustomerCertificate extends CustomersAppModel {

	public $name = 'CustomerCertificate';

	public $validate = array(
		'shikaku' => array(
			'rule' => 'notEmpty',
			'message' => '資格を選択してください'
		),
		'syutokubi' => array(
			'rule' => 'notEmpty',
			'message' => '取得日を選択してください'
		)
	);
}
