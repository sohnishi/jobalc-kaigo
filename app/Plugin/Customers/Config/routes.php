<?php

/**
 * Customer plugin routes
 */
Router::connect('/admin/customers/index/*', array('plugin' => 'customers', 'controller' => 'customers', 'action' => 'index', 'admin' => true));
Router::connect('/admin/customers/add', array('plugin' => 'customers', 'controller' => 'customers', 'action' => 'add', 'admin' => true));
Router::connect('/admin/customers/edit/*', array('plugin' => 'customers', 'controller' => 'customers', 'action' => 'edit', 'admin' => true));
Router::connect('/admin/customers/view/*', array('plugin' => 'customers', 'controller' => 'customers', 'action' => 'view', 'admin' => true));
Router::connect('/customers/forms', array('plugin' => 'customers', 'controller' => 'customers', 'action' => 'forms', 'admin' => false));
Router::connect('/customers/thanks', array('plugin' => 'customers', 'controller' => 'customers', 'action' => 'thanks', 'admin' => false));
Router::connect('/customers/phpexcel', array('plugin' => 'customers', 'controller' => 'customers', 'action' => 'phpexcel', 'admin' => false));
Router::connect('/customers/forms_lp', array('plugin' => 'customers', 'controller' => 'customers', 'action' => 'forms_lp', 'admin' => false));
