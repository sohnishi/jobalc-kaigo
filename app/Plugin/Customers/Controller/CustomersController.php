<?php

App::uses('CustomersAppController', 'Customers.Controller');

class CustomersController extends CustomersAppController {

	public $name = 'Customers';

	public $uses = array(
		'Customers.Customer',
		'Facilities.ShisetsukeitaiFirst',
		'Facilities.ShisetsukeitaiSecond',
		'Facilities.Shinryoukamoku',
		'Recruits.Kyujitsu',
		'Users.User'
	);

	public $helpers = array('Customers.Form');

	public $components = array(
		'Ekidata.Railroader' => array(
			'pref' => array(
				'id' => 'pref_cd',
				'list' => 'prefs'
			),
			'line' => array(
				'id' => 'line_cd',
				'list' => 'lines'
			),
			'station' => array(
				'id' => 'station_cd',
				'list' => 'stations'
			)
		),
		/*
			201905145 sohnishi
			kibouworkがArrayのため未入力でも検索に走ってしまう事象を回避するため、filterEmptyを追加
		*/
		'Search.Prg' => array(
			'commonProcess' => array(	
				'filterEmpty' =>  true,	
			)
		),
	);

	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('model', $this->modelClass);
		$this->set('title_for_layout', '顧客管理');
		$this->Security->unlockedActions = array('admin_index', 'admin_add', 'forms','forms_lp');

	}

	public function admin_index() {
		$this->Prg->commonProcess();
		$currentUser = $this->Auth->user();
		if($currentUser['allow_edit_customer']!=1){
			echo "権限がありません。";
			exit;
		}
		
		$this->set('currentUser',$currentUser);

		if (!empty($this->request->data)) {
			$this->Paginator->settings['conditions'] = array(
				$this->{$this->modelClass}->parseCriteria($this->Prg->parsedParams())
			);
			$this->Session->write('customerquerystring', $this->request->query);
		} else {
			$this->Session->delete('customerquerystring');
		}

		$this->Paginator->settings['limit'] = 50;
		//$this->Paginator->settings['order'] = array($this->modelClass . '.number' => 'ASC');
		$this->Paginator->settings['order'] = array($this->modelClass . '.number' => 'desc');

		$this->{$this->modelClass}->recursive = 1;
		$this->set('customers', $this->Paginator->paginate());

		// 社内担当
		$users = $this->User->find('list');
		$this->set('users', $users);
		// 施設形態 - 大区分
		$shisetsukeitaiFirsts = $this->ShisetsukeitaiFirst->find('list', array('order' => 'ShisetsukeitaiFirst.weight ASC'));
		$this->set('shisetsukeitaiFirsts', $shisetsukeitaiFirsts);
		// 施設形態 - 中区分
		$shisetsukeitaiSeconds = $this->ShisetsukeitaiSecond->find('list', array('order' => 'ShisetsukeitaiSecond.weight ASC'));
		$this->set('shisetsukeitaiSeconds', $shisetsukeitaiSeconds);
		// 診療科目リスト
		$shinryoukamokus = $this->Shinryoukamoku->find('list', array('order' => 'Shinryoukamoku.weight ASC'));
		$this->set('shinryoukamokus', $shinryoukamokus);
		// 休日体制リストを取得
		$kyujitsus = $this->Kyujitsu->find('list', array('order' => 'Kyujitsu.weight ASC'));
		$this->set('kyujitsus', $kyujitsus);
	}

	public function admin_view() {
		$this->Prg->commonProcess();
		$currentUser = $this->Auth->user();
		if($currentUser['allow_edit_customer']!=1){
			echo "権限がありません。";
			exit;
		}
		$this->set('currentUser',$currentUser);


		$this->Paginator->settings['conditions'] = array(
				$this->{$this->modelClass}->parseCriteria($this->Prg->parsedParams())
		);
		$this->Paginator->settings['limit'] = 1;
		//$this->Paginator->settings['order'] = array($this->modelClass . '.number' => 'ASC');
		$this->Paginator->settings['order'] = array($this->modelClass . '.number' => 'desc');

		$this->set('customers', $this->Paginator->paginate());

		// 社内担当
		$users = $this->User->find('list');
		$this->set('users', $users);
		// 施設形態 - 大区分
		$shisetsukeitaiFirsts = $this->ShisetsukeitaiFirst->find('list', array('order' => 'ShisetsukeitaiFirst.weight ASC'));
		$this->set('shisetsukeitaiFirsts', $shisetsukeitaiFirsts);
		// 施設形態 - 中区分
		$shisetsukeitaiSeconds = $this->ShisetsukeitaiSecond->find('list', array('order' => 'ShisetsukeitaiSecond.weight ASC'));
		$this->set('shisetsukeitaiSeconds', $shisetsukeitaiSeconds);
		// 診療科目リスト
		$shinryoukamokus = $this->Shinryoukamoku->find('list', array('order' => 'Shinryoukamoku.weight ASC'));
		$this->set('shinryoukamokus', $shinryoukamokus);
		// 休日体制リストを取得
		$kyujitsus = $this->Kyujitsu->find('list', array('order' => 'Kyujitsu.weight ASC'));
		$this->set('kyujitsus', $kyujitsus);
		// ログインユーザー情報
		$currentUser = $this->Auth->user();
		$this->set('currentUser', $currentUser);
	}

	public function admin_add() {
		$currentUser = $this->Auth->user();
		if($currentUser['allow_edit_customer']!=1){
			echo "権限がありません。";
			exit;
		}

		$this->set('currentUser',$currentUser);

		if (!empty($this->request->data)) {
			$this->{$this->modelClass}->create();
			if ($this->{$this->modelClass}->save($this->request->data)) {
				$this->flashMsg('顧客情報を登録しました');
				$this->redirect(array('action' => 'index', '?' => $this->Session->read('customerquerystring')));
			} else {
				$this->flashMsg('顧客情報を登録中にエラーが発生しました', 'alert-warning');
			}
		}

		// 社内担当
		$users = $this->User->find('list');
		$this->set('users', $users);
	}

	public function admin_edit($id = null) {
		$currentUser = $this->Auth->user();
		if($currentUser['allow_edit_customer']!=1){
			echo "権限がありません。";
			exit;
		}
		
		$this->set('currentUser',$currentUser);


		if (!$id || !$this->{$this->modelClass}->exists($id)) {
			$this->flashMsg('無効な操作です', 'alert-danger');
			$this->redirect(array('action' => 'index'));
		}

		if (!empty($this->request->data)) {
			if ($this->{$this->modelClass}->save($this->request->data)) {
				$this->flashMsg('顧客情報を更新しました');
				$this->redirect(Hash::merge(array('action' => 'view'), $this->request->params['named'], array('?' => $this->Session->read('customerquerystring'))));
			} else {
				$this->flashMsg('顧客情報を更新中にエラーが発生しました', 'alert-warning');
			}
		} else {
			$this->{$this->modelClass}->recursive = 1;
			$this->request->data = $this->{$this->modelClass}->read(null, $id);
		}

		// 社内担当
		$users = $this->User->find('list');
		$this->set('users', $users);
	}

	public function admin_delete($id = null) {
		$currentUser = $this->Auth->user();
		if($currentUser['allow_edit_customer']!=1){
			echo "権限がありません。";
			exit;
		}

		$this->set('currentUser',$currentUser);

		if (!$id || !$this->{$this->modelClass}->exists($id)) {
			$this->flashMsg('無効な操作です', 'alert-danger');
			$this->redirect(array('action' => 'index'));
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->{$this->modelClass}->delete($id)) {
				$this->flashMsg('顧客情報を削除しました');
			} else {
				$this->flashMsg('顧客情報を削除中にエラーが発生しました', 'alert-warning');
			}
		}
		$this->redirect($this->referer(array('action' => 'index')));
	}

	public function admin_typeahead_career() {
		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			Configure::write('debug', 0);
			$turm = $this->request->query['q'];
			$results = $this->Customer->CustomerCareer->find('all', array(
				'conditions' => array(
					'CustomerCareer.corporation LIKE' => '%' . $turm . '%'
				)
			));
			$corporations = array();
			foreach ($results as $result) {
				$val['corporation'] = $result['CustomerCareer']['corporation'];
				array_push($corporations, $val);
			}
			echo json_encode($corporations);
		}
	}

	/*
		20190523 sohnishi
		顧客名typehead追加
		顧客名で検索し、顧客IDと顧客番号と顧客名を返す
	*/
	public function admin_typeahead() {
		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			Configure::write('debug', 0);
			$turm = $this->request->query['q'];
			// $this->{$this->modelClass}->recursive = 1;
			$results = $this->{$this->modelClass}->find('all', array(
				'conditions' => array(
					$this->modelClass . '.name LIKE' => $turm . '%'
				)
			));
			$customers = array();
			foreach ($results as $key => $result) {
				$val['id'] = $result[$this->modelClass]['id'];
				$val['number'] = $result[$this->modelClass]['number'];
				$val['name'] = $result[$this->modelClass]['name'];
				array_push($customers, $val);
			}
			echo json_encode($customers);
		}
	}

		/*
		20190523 sohnishi
		顧客番号で顧客を検索
	*/
	public function admin_find_by_number() {
		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			Configure::write('debug', 0);
			$turm = $this->request->query['q'];
			$this->{$this->modelClass}->recursive = 1;
			$results = $this->{$this->modelClass}->find('all', array(
				'conditions' => array(
					$this->modelClass . '.number' => $turm
				)
			));
			$customers = array();
			foreach ($results as $key => $result) {
				$val['id'] = $result[$this->modelClass]['id'];
				$val['number'] = $result[$this->modelClass]['number'];
				$val['name'] = $result[$this->modelClass]['name'];
				array_push($customers, $val);
			}
			echo json_encode($customers);
		}
	}

	public function forms($id = null) {
		$this->set('title_for_layout', '無料転職登録フォーム');
		$this->layout = 'form';
		if ($this->request->is('post') || $this->request->is('put')) {
			$data = $this->request->data;

			// 求人経由であれば求人IDをセットする
			if (!empty($id)) {
				$data['Customer']['recruit_id'] = $id;
			}

			// 保存が成功したらメール送信
			if ($this->Customer->formDataSave($data)) {
				$this->redirect(array('action' => 'thanks'));
			} else {
				$this->flashMsg('お申込みフォーム送信中に予期せぬエラーが発生しました。', 'alert-warning');
			}
		}
	}
	
	public function forms_lp($id = null) {
		$this->set('title_for_layout', '無料転職登録フォーム');
		$this->layout = 'form';
		if ($this->request->is('post') || $this->request->is('put')) {
			$data = $this->request->data;

			// 求人経由であれば求人IDをセットする
			if (!empty($id)) {
				$data['Customer']['recruit_id'] = $id;
			}

			// 保存が成功したらメール送信
			if ($this->Customer->formDataSave($data)) {
    			$thanks_url=Configure::read('LP_THANKS_URL');
				$this->redirect($thanks_url);
			} else {
				$this->flashMsg('お申込みフォーム送信中に予期せぬエラーが発生しました。', 'alert-warning');
			}
		}
	}
	
		

	public function thanks() {
		$this->set('title_for_layout', '無料転職登録のお申込みありがとうございます');
		// サンクスページ表示
	}

	public function admin_phpexcel($id = null) {
		$this->layout = false;
		if ($id) {
			$data = $this->{$this->modelClass}->find('first', array(
				'conditions' => array(
					'Customer.id' => $id
				),
				'contain' => array(
					'User',
					'CustomerCertificate',
					'CustomerCondition',
					'CustomerCareer' => array(
						'order' => array(
							'CustomerCareer.order' => 'ASC',
							'CustomerCareer.modified' => 'DESC'
						),
						'ShisetsukeitaiFirst',
						'ShisetsukeitaiSecond'
					)
				),
				'limit' => 8
			));

			// 年齢計算
			$data[$this->modelClass]['age'] = $this->age($data[$this->modelClass]['birth']);
			$this->set('data', $data);
		} else {
			$this->set('data', array());
		}

		// 資格
		// $shikaku = Configure::read('shikaku');
		/*
			20190610 sohnishi
			求人と顧客の資格を分ける
		*/
		$shikaku = Configure::read('customer_shikaku');
		// 性別
		$sex = Configure::read('sex');
		// 雇用形態
		$kinmukeitai = Configure::read('kinmu_keitai');
		// 希望の働き方
		$kibouwork = Configure::read('kibouwork');
		// 勤務開始時期
		$kinmukaishijiki = Configure::read('kinmukaishijiki');
		// 通勤方法
		$tsukinhouhou = Configure::read('tsukinhouhou');

		// 施設形態 - 中区分
		$shisetsukeitaiSeconds = $this->ShisetsukeitaiSecond->find('list', array('order' => 'ShisetsukeitaiSecond.weight ASC'));
		$this->set('shisetsukeitaiSeconds', $shisetsukeitaiSeconds);
		// 診療科目リスト
		$shinryoukamokus = $this->Shinryoukamoku->find('list', array('order' => 'Shinryoukamoku.weight ASC'));
		$this->set('shinryoukamokus', $shinryoukamokus);

		$kibou = array('0' => '不要', '1' => '希望');
		$this->set(compact('shikaku', 'sex', 'kinmukeitai', 'kibouwork', 'kinmukaishijiki', 'tsukinhouhou', 'kibou'));
	}

	public function age($ymd = null) {
		$age = '';
		if (!empty($ymd)) {
			$base = new DateTime();
			$today = $base->format('Ymd');

			$birth = new DateTime($ymd);
			$birthday = $birth->format('Ymd');

			$age = (int)(($today - $birthday) / 10000);
		}
		return $age;
	}

}
