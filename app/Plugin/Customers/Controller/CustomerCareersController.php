<?php

App::uses('CustomersAppController', 'Customers.Controller');

class CustomerCareersController extends CustomersAppController {

	public $name = 'CustomerCareers';

	public $uses = array('Customers.CustomerCareer');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('model', $this->modelClass);
		$this->Security->unlockedActions = array('admin_update', 'admin_read');
	}

	public function admin_read($id = null) {
		if (!$id) {
			$this->_jsonRender();
			exit;
		}

		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			Configure::read('debug', 0);

			$career = $this->{$this->modelClass}->read(null, $id);
			$_data = $career;
			$this->_jsonRender($_data);
		}
	}

	public function admin_update() {
		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			Configure::write('debug', 0);

			if (empty($this->request->data[$this->modelClass]['id'])) {
				$this->{$this->modelClass}->create();
			}

			// 自動採番
			if (empty($this->request->data[$this->modelClass]['order'])) {
				$order = $this->{$this->modelClass}->find('count', array(
					'conditions' => array(
						$this->modelClass . '.customer_id' => $this->request->data[$this->modelClass]['customer_id']
					)
				));
				$this->request->data[$this->modelClass]['order'] = $order + 1;
			}

			$succeed = $this->{$this->modelClass}->save($this->request->data);
			$message = $succeed ? '更新しました' : '更新に失敗しました';

			if (!$succeed && $this->{$this->modelClass}->validationErrors) {
				$validationError = array_shift($this->{$this->modelClass}->validationErrors);
				$message = $validationError[0];
			}

			$_data = compact('succeed', 'message');
			$this->_jsonRender($_data);
		}
	}

	public function admin_ajaxDelete($id = null) {
		if (!$id) {
			$this->_jsonRender();
			exit;
		}

		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			Configure::write('debug', 0);
			$succeed = $this->{$this->modelClass}->delete($id);
			$message = $succeed ? '更新しました' : '更新に失敗しました';
			$_data = compact('succeed', 'message');
			$this->_jsonRender($_data);
		}
	}

	public function admin_getLatest() {
		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			Configure::write('debug', 0);

			$customerId = $this->request->params['pass'];
			$query = array(
				'conditions' => array(
					$this->modelClass . '.customer_id' => $customerId
				),
				'order' => array(
					$this->modelClass . '.order' => 'ASC',
					$this->modelClass . '.modified' => 'DESC'
				)
			);
			$careers = $this->{$this->modelClass}->find('all', $query);
			$_data = $careers;
			$this->_jsonRender($_data);
		}
	}

	private function _jsonRender($_data = array()) {
		header("Content-Type: application/json; charset=utf-8");
		echo json_encode($_data);
	}
}
