<?php

App::uses('CustomersAppController', 'Customers.Controller');

class CustomerConditionsController extends CustomersAppController {

	public $name = 'CustomerConditions';

	public $uses = array('Customers.CustomerCondition');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('model', $this->modelClass);
		$this->Security->unlockedActions = array('admin_update', 'admin_read');
	}

	public function admin_read($id = null) {
		if (!$id) {
			$this->_jsonRender();
			exit;
		}

		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			Configure::read('debug', 0);

			$condition = $this->{$this->modelClass}->read(null, $id);
			$_data = $condition;
			$this->_jsonRender($_data);
		}
	}

	public function admin_update() {
		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			Configure::write('debug', 0);

			if (!isset($this->request->data[$this->modelClass]['id']) && empty($this->request->data[$this->modelClass]['id'])) {
				$this->{$this->modelClass}->create();
			}

			$succeed = $this->{$this->modelClass}->save($this->request->data);
			$message = $succeed ? '更新しました' : '更新に失敗しました';

			if (!$succeed && $this->{$this->modelClass}->validationErrors) {
				$validationError = array_shift($this->{$this->modelClass}->validationErrors);
				$message = $validationError[0];
			}

			$_data = compact('succeed', 'message');
			$this->_jsonRender($_data);
		}
	}

	public function admin_ajaxDelete($id = null) {
		if (!$id) {
			$this->_jsonRender();
			exit;
		}

		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			Configure::write('debug', 0);
			$succeed = $this->{$this->modelClass}->delete($id);
			$message = $succeed ? '更新しました' : '更新に失敗しました';
			$_data = compact('succeed', 'message');
			$this->_jsonRender($_data);
		}
	}

	public function admin_getLatest() {
		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			Configure::write('debug', 0);

			$customerId = $this->request->params['pass'];
			$query = array(
				'conditions' => array(
					$this->modelClass . '.customer_id' => $customerId
				)
			);
			$conditions = $this->{$this->modelClass}->find('all', $query);
			$_data = $conditions;
			$this->_jsonRender($_data);
		}
	}

	private function _jsonRender($_data = array()) {
		header("Content-Type: application/json; charset=utf-8");
		echo json_encode($_data);
	}
}
