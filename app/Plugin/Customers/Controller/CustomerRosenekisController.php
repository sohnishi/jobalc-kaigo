<?php

App::uses('CustomersAppController', 'Customers.Controller');

class CustomerRosenekisController extends CustomersAppController {

	public $name = 'CustomerRosenekis';

	public $uses = array('Customers.CustomerRoseneki');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('model', $this->modelClass);
		$this->Security->unlockedActions = array('admin_update', 'admin_read');
	}

	public function admin_update() {
		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			Configure::write('debug', 0);

			if (empty($this->request->data['id'])) {
				unset($this->request->data['id']);
				$this->{$this->modelClass}->create();
			}

			$succeed = $this->{$this->modelClass}->save($this->request->data);
			$message = $succeed ? '更新しました' : '更新に失敗しました';

			if (!$succeed && $this->{$this->modelClass}->validationErrors) {
				$validationError = array_shift($this->{$this->modelClass}->validationErrors);
				$message = $validationError[0];
			}

			$_data = compact('succeed', 'message');
			$this->__jsonRender($_data);
		}
	}

	public function admin_ajaxDelete($id = null) {
		if (!$id) {
			$this->__jsonRender();
			exit;
		}

		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			Configure::write('debug', 0);
			$succeed = $this->{$this->modelClass}->delete($id);
			$message = $succeed ? '更新しました' : '更新に失敗しました';
			$_data = compact('succeed', 'message');
			$this->__jsonRender($_data);
		}
	}

	public function admin_getLatest() {
		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			Configure::write('debug', 0);

			$customerId = $this->request->params['pass'];
			$query = array(
				'conditions' => array(
					$this->modelClass . '.customer_id' => $customerId
				),
				'contain' => array(
					'RailroadPref',
					'RailroadLine',
					'RailroadStation'
				),
				'order' => array(
					$this->modelClass . '.station_cd' => 'ASC'
				)
			);
			$_data = $this->{$this->modelClass}->find('all', $query);
			$this->set('roseneki', $_data);
			$this->__jsonRender($_data);
		}
	}

	private function __jsonRender($_data = array()) {
		header("Content-Type: application/json; charset=utf-8");
		echo json_encode($_data);
	}
}
