<?php

App::uses('CustomersAppController', 'Customers.Controller');

class CustomerContactsController extends CustomersAppController {

	public $name = 'CustomerContacts';

	public $uses = array('Customers.CustomerContact', 'Contacts.ContactsCustomerContact', 'Contacts.Contact', 'Facilities.Facility');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('model', $this->modelClass);
		$this->Security->unlockedActions = array('admin_update', 'admin_read');
	}

	public function admin_read($id = null) {
		if (!$id) {
			$this->_jsonRender();
			exit;
		}

		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			Configure::read('debug', 0);

			$this->{$this->modelClass}->recursive = 2;
			$contact = $this->{$this->modelClass}->read(null, $id);
			// 日付分解
			list($yy, $mm, $dd) = explode('-', $contact['CustomerContact']['date']);
			$contact['CustomerContact']['year'] = $yy;
			$contact['CustomerContact']['month'] = $mm;
			$contact['CustomerContact']['day'] = $dd;

			// 時間分解
			list($hh, $ii) = explode(':', $contact['CustomerContact']['time']);
			$contact['CustomerContact']['hour'] = $hh;
			$contact['CustomerContact']['min'] = $ii;

			$_data = $contact;
			$this->_jsonRender($_data);
		}
	}

	public function admin_update() {
		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			Configure::write('debug', 0);

			// 中間テーブルに保存するのかCustomerContactに保存するのかフラグ
			$isContactsCustomerContact = false;
			/* 
				CustomerContact.IDがない場合は、
				施設IDがあれば、中間テーブルに新規保存
				施設IDがなければ、CustomerContactに保存
			*/
			if (!isset($this->request->data[$this->modelClass]['id']) && empty($this->request->data[$this->modelClass]['id'])) {
				if (empty($this->request->data['Contact']['facility_id'])) {
					$this->{$this->modelClass}->create();
					$isContactsCustomerContact = false;
				} else {
					$this->ContactsCustomerContact->create();
					$isContactsCustomerContact = true; 
				}
			} else {
				/* 
					CustomerContact.IDがある場合は、
					施設IDがあれば、中間テーブルに保存(CustomerContactは更新、Contactは新規保存)
				*/
				if (!empty($this->request->data['Contact']['facility_id'])) {
					$this->ContactsCustomerContact->create();
					$isContactsCustomerContact = true; 
				}
			}

			
			
			$succeed = $isContactsCustomerContact ? $this->ContactsCustomerContact->saveAll($this->request->data) : $this->{$this->modelClass}->saveAll($this->request->data);
			$message = $succeed ? '更新しました' : '更新に失敗しました';

			// if (!$succeed && !$isContactsCustomerContact && $this->{$this->modelClass}->validationErrors) {
			// 	$validationError = array_shift($this->ContactsCustomerContact->validationErrors);
			// 	$message = $validationError[0];
			// }

			$_data = compact('succeed', 'message');
			$this->_jsonRender($_data);
		}
	}

	public function admin_ajaxDelete($id = null) {
		if (!$id) {
			$this->_jsonRender();
			exit;
		}

		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			Configure::write('debug', 0);
			/*
				20190522 sohnishi
				CustomerContactを削除する前に中間テーブルから該当のデータを取得する
			*/
			$conditions = array('customer_contact_id'=>$id);
			$contactsCustomerContact = $this->ContactsCustomerContact->find('all', array(
				'conditions' => $conditions,
				'fields' => 'contact_id'
			));
			$contact_ids = array();
			foreach($contactsCustomerContact as $key => $value){
				array_push($contact_ids, $value['ContactsCustomerContact']['contact_id']);
			}

			// CustomerContactの削除
			$succeed = $this->{$this->modelClass}->delete($id);
			
			/*
				20190522 sohnishi
				該当のCustomerContactに関連するContactのデータも削除
			*/
			$succeed = $this->Contact->deleteAll(
				array(
					'Contact.id' => $contact_ids
			), true);

			$message = $succeed ? '更新しました' : '更新に失敗しました';
			$_data = compact('succeed', 'message');
			$this->_jsonRender($_data);
		}
	}

	public function admin_getLatest() {
		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			Configure::write('debug', 0);

			$customerId = $this->request->params['pass'];
			$query = array(
				'conditions' => array(
					$this->modelClass . '.customer_id' => $customerId
				),
				'order' => array(
					$this->modelClass . '.date' => 'DESC',
					$this->modelClass . '.time' => 'DESC'
				)
			);
			$this->{$this->modelClass}->recursive = 2;
			$tmpContacts = $this->{$this->modelClass}->find('all', $query);
			$contacts = array();

			foreach ($tmpContacts as $key => $contact) {
				$contact['CustomerContact']['date'] = date('Y年n月j日', strtotime($contact['CustomerContact']['date']));
				$contact['CustomerContact']['time'] = date('H:i', strtotime($contact['CustomerContact']['time']));
				$contact_facilities = $this->Facility->find('all', array(
					'conditions' => array(
						'id' => $contact['ContactsCustomerContact'][0]['Contact']['facility_id']
					)
				));
				// foreach ($contact['ContactsCustomerContact'] as $contactsCustomerContact) {
				// 	$contact_facilities = $this->Facility->find('all', array(
				// 		'conditions' => array(
				// 			'id' => $contactsCustomerContact['Contact']['facility_id']
				// 		)
				// 	));
				// }
				// if ($contact_facilities != null) {
					$contact['Facility'] = $contact_facilities;
				// }
				// $contact_facilities = null;
				$contacts[$key] = $contact;
			}

			$_data = $contacts;
			$this->_jsonRender($_data);
		}
	}

	private function _jsonRender($_data = array()) {
		header("Content-Type: application/json; charset=utf-8");
		echo json_encode($_data);
	}
}
