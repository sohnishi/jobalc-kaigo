<div class="panel panel-default">
	<div class="panel-heading">
		<a href="#rosenekiForm" data-toggle="collapse" aria-expanded="false" aria-controls="rosenekiForm"><i class="fa fa-plus"></i> 最寄り路線・最寄り駅を追加</a>
	</div>
	<div class="panel-body collapse" id="rosenekiForm">

		<?php
			echo $this->Form->create('CustomerRoseneki', array(
				'inputDefaults' => array(
					'label' => false,
					'wrapInput' => false,
					'div' => false,
					'class' => 'form-control'
				),
				'url' => array(
					'plugin' => 'customers',
					'controller' => 'customers',
					'action' => 'update',
					'admin' => true
				),
				'default' => false
			));
			echo $this->Form->input('CustomerRoseneki.customer_id', array('type' => 'hidden', 'value' => $customerId));
		?>
		<div class="row">
			<div class="col-xs-3">
				<div class="form-group">
					<?php echo $this->Form->input('CustomerRoseneki.pref_cd', array('type' => 'select', 'options' => $prefs, 'empty' => '選択してください', 'label' => '都道府県')); ?>
				</div>
			</div>
			<div class="col-xs-3">
				<div class="form-group">
					<?php echo $this->Form->input('CustomerRoseneki.line_cd', array('type' => 'select', 'options' => $lines, 'empty' => '選択してください', 'label' => '最寄り路線')); ?>
				</div>
			</div>
			<div class="col-xs-3">
				<div class="form-group">
					<?php echo $this->Form->input('CustomerRoseneki.station_cd', array('type' => 'select', 'options' => $stations, 'empty' => '選択してください', 'label' => '最寄り駅')); ?>
				</div>
			</div>
			<div class="col-xs-3">
				<div class="form-group">
					<?php echo $this->Form->input('CustomerRoseneki.distance', array('type' => 'text', 'label' => '距離')); ?>
				</div>
			</div>
		</div>

		<div class="form-group">
			<?php echo $this->Form->submit('送信する', array('id' => 'rosenekiSubmit', 'class' => 'btn btn-primary', 'div' => false)); ?>
			<?php echo $this->Form->button('リセット', array('type' => 'button', 'id' => 'rosenekiReset', 'class' => 'btn btn-warning', 'div' => false)); ?>
		</div>

		<?php echo $this->Form->end(); ?>

	</div>
</div>
<div id="roseneki-message"></div>
