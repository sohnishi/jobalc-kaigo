<script>
$(function () {

	function careerRecords() {
		$('#career-latest').empty();
		var url = '<?php echo $this->Html->url(array('plugin' => 'customers', 'controller' => 'customer_careers', 'action' => 'getLatest', $customerId, 'admin' => true)); ?>';
		$.getJSON(url, function (json, status) {
			if (status == 'success') {
				if ($.isEmptyObject(json)) {
					$('#career-latest').append('<p>職務履歴が登録されていません</p>');
				} else {
					$.each(json, function () {
						var _this = this;
						var career = _this.CustomerCareer;
						var shisetsukeitaiFirsts = <?php echo json_encode($shisetsukeitaiFirsts); ?>;
						var shisetsukeitaiSeconds = <?php echo json_encode($shisetsukeitaiSeconds); ?>;
						var shinryoukamokus = <?php echo json_encode($shinryoukamokus); ?>;
						var kinmuKeitai = <?php echo json_encode($kinmu_keitai); ?>;
						var html = '<table class="table table-bordered table-condenced table-fixed" style="margin-bottom:5px;"><tbody>';
						html += '<tr>';
						html += '<th>勤務先</th><td>'
						html += career.corporation;
						html += '</td>';
						html += '<th>エントリー</th><td>';
						//html += shisetsukeitaiFirsts[career.shisetsukeitai_first_id];
						html += career.entry;
						html += '</td>';
						html += '</tr>';
						html += '<tr>';
						html += '<th>配属先</th><td>';
						html += shisetsukeitaiSeconds[career.shisetsukeitai_second_id];
						html += '</td>';
						html += '<th>科目</th><td>';
						html += shinryoukamokus[career.shinryoukamoku_id];
						html += '</td>';
						html += '</tr>';
						html += '<tr>';
						html += '<th>在籍期間</th><td>';
						html += career.zaiseki;
						html += '</td>';
						html += '<th>雇用形態</th><td>'
						html += career.kinmu_keitai ? kinmuKeitai[career.kinmu_keitai] : '';
						html += '</td>';
						html += '</tr>';
						html += '<tr>';
						html += '<th>入社順番</th>';
						html += '<td>';
						html += career.order;
						html += '</td>';
						html += '<td colspan="2"><div class="pull-left">';
						html += '更新日：' + career.modified;
						html += '</div>';
						html += '<div class="pull-right">';
						html += '<button type="button" class="btn btn-danger btn-xs career-delete" data-career-id="' + career.id + '">削除</button>&nbsp;&nbsp;';
						html += '<button type="button" class="btn btn-default btn-xs career-edit" data-career-id="' + career.id + '">更新</button>';
						html += '</div></td>';
						html += '</tr>';
						html += '</tbody></table>';
						$('#career-latest').append(html);
					});
				}
			}
		});
	}

	$('#careerSubmit').on('click', function () {
		$.ajax({
			type: 'POST',
			url: '<?php echo $this->Html->url(array('plugin' => 'customers', 'controller' => 'customer_careers', 'action' => 'update', 'admin' => true)); ?>',
			data: $('#CustomerCareerAdminViewForm').serializeArray(),
			success: function (data, textStatus, jpXHR) {
				var jsonObj;
				/* 
					20190619 sohnishi
					型判定し、objectでなければjson変換する
				*/
				if (typeof data != 'object') {
					jsonObj = $.parseJSON(data);
				} else {
					jsonObj = data;
				}
				careerMessage(jsonObj.message, jsonObj.succeed);
				if (jsonObj.succeed) {
					$('#CustomerCareerAdminViewForm').find('#CustomerCareerId, input[type="text"], select').val('');
					$('#careerForm').removeClass('in').parent().find(".fa-minus").removeClass("fa-minus").addClass("fa-plus");
					careerRecords();
				}
			},
			error: function (jpXHR, textStatus, errorThrown) {
				careerMessage('通信に失敗しました' + errorThrown, false);
			}
		});
	});

	$('#careerReset').on('click', function () {
		$('#CustomerCareerAdminViewForm').find('#CustomerCareerId, input[type="text"], select').val('');
	});

	$('#career-latest').on('click', '.career-edit', function () {
		/**
		 *	20190619 sohnishi
		 *	更新ボタン押下->閉じる->更新ボタン押下でレイアウトが崩れる事象に対応
		 */
		$('#careerForm').css('height', 'initial');
		$('#careerForm').addClass('in').parent().find(".fa-plus").removeClass("fa-plus").addClass("fa-minus");
		var careerId = $(this).data('career-id');
		var url = '<?php echo $this->Html->url(array('plugin' => 'customers', 'controller' => 'customer_careers', 'action' => 'read', 'admin' => true)); ?>';
		$.getJSON(url + '/' + careerId, function (json, status) {
			var career = json.CustomerCareer;
			if (status == 'success') {
				$('#CustomerCareerId').val(career.id);
				$('#CustomerCareerCustomerId').val(career.customer_id);
				$('#CustomerCareerOrder').val(career.order);
				$('#CustomerCareerCorporation').val(career.corporation);
				$('#CustomerCareerEntry').val(career.entry);
				$('#CustomerCareerShisetsukeitaiFirstId').val(career.shisetsukeitai_first_id);
				$('#CustomerCareerShisetsukeitaiSecondId').val(career.shisetsukeitai_second_id);
				$('#CustomerCareerShinryoukamokuId').val(career.shinryoukamoku_id);
				$('#CustomerCareerZaiseki').val(career.zaiseki);
				$('#CustomerCareerKinmuKeitai').val(career.kinmu_keitai);
			}
		});
	});

	$('#career-latest').on('click', '.career-delete', function () {
		var careerId = $(this).data('career-id');
		if (confirm('本当に削除しても宜しいですか？')) {
			var url='<?php echo $this->Html->url(array('plugin' => 'customers', 'controller' => 'customer_careers', 'action' => 'ajaxDelete', 'admin' => true)); ?>';
			$.getJSON(url + '/' + careerId, function (json, status) {
				if (status == 'success') {
					careerMessage(json.message, json.succeed);
					if (json.succeed) {
						careerRecords();
					}
				}
			});
		}
	});

	function careerMessage(text, succeed) {
		var color = succeed ? 'text-success' : 'text-warning';
		var msg = $('<p />').attr('class', color).text(text);
		$('#career-message').append(msg);
		setTimeout(function () {
			$('#career-message').find('p').remove();
		}, 3000);
	}

	$(window).load(function () {
		careerRecords();
	});
});
</script>
