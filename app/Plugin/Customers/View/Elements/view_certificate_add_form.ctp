<div class="panel panel-default">
	<div class="panel-heading">
		<a href="#shikakuForm" data-toggle="collapse" aria-expanded="false" aria-controls="shikakuForm"><i class="fa fa-plus"></i> 資格情報を追加・更新</a>
	</div>
	<div class="panel-body collapse" id="shikakuForm">

		<?php
			echo $this->Form->create('CustomerCertificate', array(
				'inputDefaults' => array(
					'label' => false,
					'wrapInput' => false,
					'div' => false,
					'class' => 'form-control'
				),
				'url' => array(
					'plugin' => 'customers',
					'controller' => 'customer_certificates',
					'action' => 'update',
					'admin' => true
				),
				'default' => false
			));

			echo $this->Form->input('id', array('type' => 'hidden'));
			echo $this->Form->input('customer_id', array('type' => 'hidden', 'value' => $customerId));
		?>
		<div class="row">
			<div class="col-xs-5">
				<div class="form-group">
					<?php echo $this->Form->input('shikaku', array('type' => 'select', 'options' => $shikaku, 'empty' => '選択してください', 'label' => '資格')); ?>
				</div>
			</div>
			<div class="col-xs-5">
				<div class="form-group">
					<?php echo $this->Form->label('syutokubi', '取得日'); ?>
					<div class="input-group">
						<?php echo $this->Form->year('syutokubi', date('Y') - 60, date('Y'), array('class' => 'form-control', 'empty' => '選択してください')); ?>
						<div class="input-group-addon">年取得</div>
					</div>
				</div>
			</div>
		</div>

		<div class="form-group">
			<?php echo $this->Form->submit('送信する', array('id' => 'shikakuSubmit', 'class' => 'btn btn-primary', 'div' => false)); ?>
			<?php echo $this->Form->button('リセット', array('type' => 'button', 'id' => 'shikakuReset', 'class' => 'btn btn-warning', 'div' => false)); ?>
		</div>

		<?php echo $this->Form->end(); ?>
	</div>
</div>
<div id="shikaku-message"></div>
