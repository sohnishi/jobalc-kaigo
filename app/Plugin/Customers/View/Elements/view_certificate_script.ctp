<script>
$(function () {

	function shikakuRecords() {
		$('#shikaku-latest').empty();
		var url = '<?php echo $this->Html->url(array('plugin' => 'customers', 'controller' => 'customer_certificates', 'action' => 'getLatest', $customerId, 'admin' => true)); ?>';
		$.getJSON(url, function (json, status) {
			if (status == 'success') {
				if ($.isEmptyObject(json)) {
					$('#shikaku-latest').append('<p>資格情報が登録されていません</p>');
				} else {
					var shikaku = <?php echo json_encode($shikaku); ?>;
					$.each(json, function () {
						var _this = this;
						var html = '<table class="table table-bordered table-condenced table-fixed"><tbody>';
						html += '<tr>';
						html += '<th>資格</th>';
						html += '<td>';
						html += shikaku[_this.CustomerCertificate.shikaku];
						html += '</td>';
						html += '<th>取得日</th>';
						html += '<td>';
						html += _this.CustomerCertificate.syutokubi + '年取得';
						html += '</td>';
						html += '<td>';
						html += '<button type="button" class="btn btn-danger btn-xs shikaku-delete" data-shikaku-id="' + _this.CustomerCertificate.id + '">削除</button>&nbsp;&nbsp;';
						html += '<button type="button" class="btn btn-default btn-xs shikaku-edit" data-shikaku-id="' + _this.CustomerCertificate.id + '">更新</button>';
						html += '</td>';
						html += '</tr>';
						html += '</tbody></table>';
						$('#shikaku-latest').append(html);
					});
				}
			}
		});
	}

	$('#shikakuSubmit').on('click', function () {
		$.ajax({
			type: 'POST',
			url: '<?php echo $this->Html->url(array('plugin' => 'customers', 'controller' => 'customer_certificates', 'action' => 'update', 'admin' => true)); ?>',
			data: $('#CustomerCertificateAdminViewForm').serializeArray(),
			success: function (data, textStatus, jpXHR) {
				var jsonObj;
				/* 
					20190617 sohnishi
					型判定し、objectでなければjson変換する
				*/
				if (typeof data != 'object') {
					jsonObj = $.parseJSON(data);
				} else {
					jsonObj = data;
				}
				shikakuMessage(jsonObj.message, jsonObj.succeed);
				if (jsonObj.succeed) {
					$('#CustomerCertificateAdminViewForm').find('#CustomerCertificateId, select').val('');
					$('#shikakuForm').removeClass('in').parent().find(".fa-minus").removeClass("fa-minus").addClass("fa-plus");
					shikakuRecords();
				}
			},
			error: function (jpXHR, textStatus, errorThrown) {
				shikakuMessage('通信に失敗しました' + errorThrown, false);
			}
		});
	});

	$('#shikakuReset').on('click', function () {
		$('#CustomerCertificateAdminViewForm').find('#CustomerCertificateId, select').val('');
	});

	$('#shikaku-latest').on('click', '.shikaku-edit', function () {
		/**
		 *	20190619 sohnishi
		 *	更新ボタン押下->閉じる->更新ボタン押下でレイアウトが崩れる事象に対応
		 */
		$('#shikakuForm').css('height', 'initial');
		$('#shikakuForm').addClass('in').parent().find(".fa-plus").removeClass("fa-plus").addClass("fa-minus");
		var shikakuId = $(this).data('shikaku-id');
		var url = '<?php echo $this->Html->url(array('plugin' => 'customers', 'controller' => 'customer_certificates', 'action' => 'read', 'admin' => true)); ?>';
		$.getJSON(url + '/' + shikakuId, function (json, status) {
			if (status == 'success') {
				$('#CustomerCertificateId').val(json.CustomerCertificate.id);
				$('#CustomerCertificateCustomerId').val(json.CustomerCertificate.customer_id);
				$('#CustomerCertificateShikaku').val(json.CustomerCertificate.shikaku);
				$('#CustomerCertificateSyutokubiYear').val(json.CustomerCertificate.syutokubi);
			}
		});
	});

	$('#shikaku-latest').on('click', '.shikaku-delete', function () {
		var shikakuId = $(this).data('shikaku-id');
		if (confirm('本当に削除しても宜しいですか？')) {
			var url = '<?php echo $this->Html->url(array('plugin' => 'customers', 'controller' => 'customer_certificates', 'action' => 'ajaxDelete', 'admin' => true)); ?>';
			$.getJSON(url + '/' + shikakuId, function (json, status) {
				if (status == 'success') {
					shikakuMessage(json.message, json.succeed);
					if (json.succeed) {
						shikakuRecords();
					}
				}
			});
		}
	});

	function shikakuMessage(text, succeed) {
		var color = succeed ? 'text-success' : 'text-warning';
		var msg = $('<p />').attr('class', color).text(text);
		$('#shikaku-message').append(msg);
		setTimeout(function () {
			$('#shikaku-message').find('p').remove();
		}, 3000);
	}

	$(window).load(function () {
		shikakuRecords();
	});
});
</script>
