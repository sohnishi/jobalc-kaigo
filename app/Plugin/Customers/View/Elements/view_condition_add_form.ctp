<div class="panel panel-default">
	<div class="panel-heading">
		<a href="#conditionForm" data-toggle="collapse" aria-expanded="false" aria-controls="conditionForm"><i class="fa fa-plus"></i> 希望条件を追加</a>
	</div>
	<div class="panel-body collapse" id="conditionForm">

		<?php
			echo $this->Form->create('CustomerCondition', array(
				'inputDefaults' => array(
					'label' => false,
					'wrapInput' => false,
					'div' => false,
					'class' => 'form-control'
				),
				'url' => array(
					'plugin' => 'customers',
					'controller' => 'customer_conditions',
					'action' => 'update',
					'admin' => true
				),
				'default' => false
			));

			echo $this->Form->input('id', array('type' => 'hidden'));
			echo $this->Form->input('customer_id', array('type' => 'hidden', 'value' => $customerId));
		?>

		<div class="row">
			<div class="col-xs-6">
				<div class="form-group">
					<?php echo $this->Form->input('kibouwork', array('type' => 'textarea', 'rows' => 2, 'label' => '希望の働き方')); ?>
				</div>
			</div>
			<div class="col-xs-6">
				<div class="form-group">
					<?php echo $this->Form->input('kiboubusyo', array('type' => 'textarea', 'rows' => 2, 'label' => '希望部署')); ?>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-3">
				<div class="form-group">
					<?php echo $this->Form->input('kinmukaishijiki', array('type' => 'select', 'options' => $kinmukaishijiki, 'empty' => '選択してください', 'label' => '勤務開始時期')); ?>
				</div>
			</div>
			<div class="col-xs-3">
				<div class="form-group">
					<?php echo $this->Form->input('tsukinhouhou', array('type' => 'select', 'options' => $tsukinhouhou, 'empty' => '選択してください', 'label' => '通勤方法')); ?>
				</div>
			</div>
			<div class="col-xs-3">
				<div class="form-group">
					<?php echo $this->Form->input('tsukinjikan', array('type' => 'select', 'options' => $tsukinjikan, 'empty' => '選択してください', 'label' => '通勤時間')); ?>
				</div>
			</div>
			<div class="col-xs-3">
				<div class="form-group">
					<?php echo $this->Form->input('kiboukyuyo', array('type' => 'select', 'options' => $kiboukyuyo, 'empty' => '選択してください', 'label' => '希望給与')); ?>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-3">
				<div class="form-group">
					<?php echo $this->Form->input('kyujitsu_id', array('empty' => '選択してください', 'label' => '希望休日')); ?>
				</div>
			</div>
			<div class="col-xs-3">
				<div class="form-group">
					<?php echo $this->Form->label('huyouninzu', '扶養人数'); ?>
					<div class="input-group">
						<?php echo $this->Form->input('huyouninzu', array('type' => 'text')); ?>
						<span class="input-group-addon">人</span>
					</div>
				</div>
			</div>
			<div class="col-xs-3">
				<div class="form-group">
					<?php echo $this->Form->input('ryou', array('type' => 'select', 'options' => $nashiari, 'empty' => '選択してください', 'label' => '寮')); ?>
				</div>
			</div>
			<div class="col-xs-3">
				<div class="form-group">
					<?php echo $this->Form->input('takujisyo', array('type' => 'select', 'options' => $nashiari, 'empty' => '選択してください', 'label' => '託児所')); ?>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-6">
				<div class="form-group">
					<?php echo $this->Form->input('genjou', array('type' => 'textarea', 'rows' => 2, 'label' => '現状', 'style' => 'resize:vertical;')); ?>
				</div>
			</div>
			<div class="col-xs-6">
				<div class="form-group">
					<?php echo $this->Form->input('mensetsukanoubi', array('type' => 'textarea', 'rows' => 2, 'label' => '面接可能日', 'style' => 'resize:vertical;')); ?>
				</div>
			</div>
		</div>

		<div class="form-group">
			<?php echo $this->Form->submit('送信する', array('id' => 'conditionSubmit', 'class' => 'btn btn-primary', 'div' => false)); ?>
			<?php echo $this->Form->button('リセット', array('type' => 'button', 'id' => 'conditionReset', 'class' => 'btn btn-warning', 'div' => false)); ?>
		</div>

		<?php echo $this->Form->end(); ?>
	</div>
</div>
<div id="condition-message"></div>
