<?php
	echo $this->Html->script('typeahead.bundle.min', array('inline' => false));
?>

<?php echo $this->Html->scriptStart(); ?>
$(function () {
	var corporations = new Bloodhound({
		remote: {
				url: '<?php echo $this->Html->url(array('plugin' => 'customers', 'controller' => 'customers', 'action' => 'typeahead_career', '?' => 'q=%QUERY', 'admin' => true)); ?>',
				cache: false
			},
		datumTokenizer: function (d) {
			return Bloodhound.tokenizers.whitespace(d.name);
		},
		queryTokenizer: Bloodhound.tokenizers.whitespace,
	});

	corporations.initialize();

	$('#CustomerCareerCorporation').typeahead({
		hint: false,
		highlight: true,
		minLength: 2
	},
	{
		name: 'corporations',
		displayKey: 'corporation',
		source: corporations.ttAdapter()
	});
});
<?php echo $this->Html->scriptEnd(); ?>

<div class="panel panel-default">
	<div class="panel-heading">
		<a href="#careerForm" data-toggle="collapse" aria-expanded="false" aria-controls="careerForm"><i class="fa fa-plus"></i> 職務経歴を追加</a>
	</div>
	<div class="panel-body collapse" id="careerForm">

		<?php
			echo $this->Form->create('CustomerCareer', array(
				'inputDefaults' => array(
					'label' => false,
					'wrapInput' => false,
					'div' => false,
					'class' => 'form-control'
				),
				'url' => array(
					'plugin' => 'customers',
					'controller' => 'customer_careers',
					'action' => 'update',
					'admin' => true
				),
				'default' => false
			));

			echo $this->Form->input('id', array('type' => 'hidden'));
			echo $this->Form->input('customer_id', array('type' => 'hidden', 'value' => $customerId));
		?>

		<div class="row">
			<div class="col-xs-3">
				<div class="form-group">
					<?php echo $this->Form->input('order', array('type' => 'text', 'label' => '入社順番')); ?>
					<div class="help-block">空欄は自動採番</div>
				</div>
			</div>
			<div class="col-xs-3">
				<div class="form-group">
					<?php echo $this->Form->input('corporation', array('type' => 'text', 'label' => '勤務先')); ?>
				</div>
			</div>
			<div class="col-xs-3">
				<div class="form-group">
					<?php //echo $this->Form->input('shisetsukeitai_first_id', array('empty' => '選択してください', 'label' => 'エントリー')); ?>
					<?php echo $this->Form->input('shisetsukeitai_first_id', array('type' => 'hidden')); ?>
					<?php echo $this->Form->input('entry', array('type' => 'text', 'label' => 'エントリー')); ?>
				</div>
			</div>
			<div class="col-xs-3">
				<div class="form-group">
					<?php echo $this->Form->input('shisetsukeitai_second_id', array('empty' => '選択してください', 'label' => '配属先')); ?>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-3">
				<div class="form-group">
					<?php echo $this->Form->input('shinryoukamoku_id', array('empty' => '選択してください', 'label' => '科目')); ?>
				</div>
			</div>
			<div class="col-xs-3">
				<div class="form-group">
					<?php echo $this->Form->input('zaiseki', array('type' => 'select', 'options' => $zaiseki, 'empty' => '選択してください', 'label' => '在籍期間')); ?>
				</div>
			</div>
			<div class="col-xs-3">
				<div class="form-group">
					<?php echo $this->Form->input('kinmu_keitai', array('type' => 'select', 'options' => Configure::read('kinmu_keitai'), 'empty' => '選択してください', 'label' => '雇用形態')); ?>
				</div>
			</div>
		</div>

		<div class="form-group">
			<?php echo $this->Form->submit('送信する', array('id' => 'careerSubmit', 'class' => 'btn btn-primary', 'div' => false)); ?>
			<?php echo $this->Form->button('リセット', array('type' => 'reset', 'id' => 'careerReset', 'class' => 'btn btn-warning', 'div' => false)); ?>
		</div>

		<?php echo $this->Form->end(); ?>

	</div>
</div>
<div id="career-message"></div>
