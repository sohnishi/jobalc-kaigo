<script>
$(function () {

	// 施設番号
	var facility_number;
	// 施設名をtypeheadを使用して入力したかどうか
	var isFacilityTypeHead = false;
	// 施設番号を入力後に施設がヒットして値をセットしたかどうか
	var isFindByNumberSuccessed = false;
	// 施設番号から検索した施設名をセット
	var shisetsumei;
	// 施設担当者をセット
	var staffs;


	var facilities = new Bloodhound({
		remote: '<?php echo $this->Html->url(array('plugin' => 'facilities', 'controller' => 'facilities', 'action' => 'typeahead', '?' => 'q=%QUERY', 'admin' => true)); ?>',
		datumTokenizer: function (d) {
			return Bloodhound.tokenizers.whitespace(d.name);
		},
		queryTokenizer: Bloodhound.tokenizers.whitespace
	});


	facilities.initialize();
	$('#CustomerContactShisetsumei').typeahead({
		hint: false,
		highlight: true,
		minLength: 2
	},
	{
		name: 'facilities',
		displayKey: 'name',
		source: facilities.ttAdapter()
	}).on("typeahead:opened", function(e, datum) {
	}).on("typeahead:closed", function(e, datum) {
		/*
			施設名からフォーカスが外れる際に、
			施設番号からの検索値が入っていれば、そのまま値をセットしておく
		*/
		if (isFindByNumberSuccessed) {
			$('#CustomerContactShisetsumei').val(shisetsumei);
		}
		// 施設番号の値が空の場合は施設名に値が入っていても初期化する
		if ($('#ContactFacilityId').val().length < 1) {
			$('#CustomerContactShisetsumei').val('');
		}
	}).on("typeahead:selected typeahead:autocomplete", function(e, datum) {
		/*
			Callback
			施設numberをセット
			スタッフ配列をセット
			typeaheadを使用したため、isFacilityTypeHeadをtrue
			施設番号からの検索ではないため、isFindByNumberSuccessedをfalse
		*/
		facility_number = datum['number'];
		$('#ContactFacilityId').val(facility_number);

		$("#ContactStaffId").children().remove();
		staffs = datum['staffs'];
		$.each(staffs, function(index, value){
			var option = "<option value=";
			option += value['id'];
			option += ">";
			option += value['name'];
			option += "</option>";
			$("#ContactStaffId").append(option);
		})

		isFacilityTypeHead = true;
		isFindByNumberSuccessed = false;
	});


	/*
		20190521 sohnishi
		入力補完を行わずに施設名を入力された際にも、
		施設番号を検索してセットする対応
	*/
	$('#CustomerContactShisetsumei').on('blur', function(){
		if (isFacilityTypeHead) {
			// typeheadを使用した

			// 2回目の入力に対応するため、isFacilityTypeHeadをFalseに戻す
			isFacilityTypeHead = false;

		} else {
			// typeheadを使用していない

			// 空文字判定
			if ($(this).val() != null && $(this).val().length >= 1) {
				$.ajax({
					type: 'GET',
					url: '<?php echo $this->Html->url(array('plugin' => 'facilities', 'controller' => 'facilities', 'action' => 'typeahead', 'admin' => true)); ?>',
					data: {
						'q':$(this).val(),
					},
					success: function (data, textStatus, jpXHR) {
						if (data != null) {
							var jsonObj = $.parseJSON(data);
							/*
								検索結果が1件であれば、
								施設numberをセット
							*/
							isFacilityTypeHead = false;
							if (jsonObj.length == 1) {
								facility_number = jsonObj[0]['id'];
								$('#ContactFacilityId').val(facility_number);

								$("#ContactStaffId").children().remove();
								staffs = jsonObj[0]['staffs'];
								$.each(staffs, function(index, value){
									var option = "<option value=";
									option += value['id'];
									option += ">";
									option += value['name'];
									option += "</option>";
									$("#ContactStaffId").append(option);
								})
							} else {
								/* 
									typeaheadもしくは施設番号での検索を使用していない場合に限り、全項目削除
									typeaheadを使用しない施設名入力は上の判定を通るため不要
								*/
								if (!isFacilityTypeHead && !isFindByNumberSuccessed) {
									$('#CustomerContactShisetsumei').val('');
									$('#ContactFacilityId').val('');	
									$("#ContactStaffId").children().remove();
								}
							}
						}
					},
					error: function (jpXHR, textStatus, errorThrown) {
						console.log('通信に失敗しました' + errorThrown, false);
					}
				});

			}
		}
	});


	/*
		20190521 sohnishi
		施設番号入力後に施設の検索を行い、
		法人名 + 施設名項目に値をセット
	*/
	$('#ContactFacilityId').on('blur', function(){
		// 空文字判定
		if ($(this).val() != null && $(this).val().length >= 1) {
			$.ajax({
				type: 'GET',
				url: '<?php echo $this->Html->url(array('plugin' => 'facilities', 'controller' => 'facilities', 'action' => 'find_by_number', 'admin' => true)); ?>',
				data: {
					'q':$(this).val(),
				},
				success: function (data, textStatus, jpXHR) {
					var jsonObj = $.parseJSON(data);
					/*
						検索結果がnotnull且つ1件であれば、
						法人名 + 施設名をセット
					*/
					if (jsonObj != null && jsonObj.length == 1) {
						shisetsumei = jsonObj[0]['name'];
						$('#CustomerContactShisetsumei').val(shisetsumei);

						$("#ContactStaffId").children().remove();
						staffs = jsonObj[0]['staffs'];
						$.each(staffs, function(index, value){
							var option = "<option value=";
							option += value['id'];
							option += ">";
							option += value['name'];
							option += "</option>";
							$("#ContactStaffId").append(option);
						})

						isFindByNumberSuccessed = true;
					} else {
						// 施設番号での検索時は、データが見つからない場合、全項目削除
						$('#CustomerContactShisetsumei').val('');
						$('#ContactFacilityId').val('');	
						$("#ContactStaffId").children().remove();
						isFindByNumberSuccessed = false;
					}

				},
				error: function (jpXHR, textStatus, errorThrown) {
					console.log('通信に失敗しました' + errorThrown, false);
				}
			});
		} else {
			// 施設番号の値が空の場合は施設名に値が入っていても初期化する
			$('#CustomerContactShisetsumei').val('');
		}
	});

	// 施設名を手入力した場合、isFindByNumberSuccessedをfalseとする
	$('#CustomerContactShisetsumei').on('change', function(){
		isFindByNumberSuccessed = false;
	});

	function contactRecords() {
		$('#contact-latest').empty();
		var url = '<?php echo $this->Html->url(array('plugin' => 'customers', 'controller' => 'customer_contacts', 'action' => 'getLatest', $customerId, 'admin' => true)); ?>';
		$.getJSON(url, function (json, status) {
			if (status == 'success') {
				if ($.isEmptyObject(json)) {
					$('#contact-latest').append('<p>連絡履歴が登録されていません</p>');
				} else {
					$.each(json, function () {
						var _this = this;
						var contact = _this.CustomerContact;
						var contactsCustomerContact = _this.ContactsCustomerContact;
						var facility = _this.Facility;
						var user = <?php echo json_encode($users); ?>;
						var houhou = <?php echo json_encode($renraku_houhou); ?>;
						var html = '<table class="table table-bordered table-condenced table-fixed" style="margin-bottom:5px;"><tbody>';
						html += '<tr><th>';
						html += contact.date;
						html += '</th><td rowspan="';
						/*
							20190522 sohnishi
							段数を合わせるため、施設連絡履歴が存在する場合は6
						*/
						html += contactsCustomerContact.length > 0 ? '6">' : '5">';
						var syousai = contact.syousai;
						syousai = syousai.replace(/\r\n/g, "<br>");
						if (facility != null && facility.length >= 1) {
							html += '[法人名]<br>';
							html += '[' + facility[0]['Facility']['shisetsumei'] + ']<br><br>';
						} 
						html += syousai;
						html += '</td></tr>';
						html += '<tr><th>';
						html += contact.time;
						html += '</th></tr>';
						html += '<tr><th>';
						html += user[contact.user_id];
						html += '</th></tr>';
						html += '<tr><th>';
						html += houhou[contact.houhou];
						html += '</th></tr>';
						/*
							20190522 sohnishi
							施設連絡履歴が存在する場合のみリンクを貼る
						*/
						if (contactsCustomerContact.length > 0) {
							html += '<tr><th>';
							html += '施設番号：';
							html += "<a href='/cw/admin/facilities/view/?number=";
							html += contactsCustomerContact[0]['Contact']['facility_id'] + "'>";
							html += contactsCustomerContact[0]['Contact']['facility_id'];
							html += "</a>";
							html += '</th></tr>';
						}
						html += '<tr><td>'
						html += '<button type="button" class="btn btn-danger btn-xs contact-delete" data-contact-id="' + contact.id + '">削除</button>&nbsp;&nbsp;';
						html += '<button type="button" class="btn btn-default btn-xs contact-edit" data-contact-id="' + contact.id + '">更新</button>';
						html += '</td></tr>';
						html += '</tbody></table>';
						$('#contact-latest').append(html);
					});
				}
			}
		});
	}

	$('#contactSubmit').on('click', function () {

		// submit前に施設連絡履歴用のhidden項目を追加
		// 連絡日時・年
		$('<input>').attr({
			type: 'hidden',
			id: 'ContactDateYear',
			name: 'data[Contact][date][year]',
			value: $('#CustomerContactDateYear').val()
		}).appendTo('#CustomerContactAdminViewForm');

		// 連絡日時・月
		$('<input>').attr({
			type: 'hidden',
			id: 'ContactDateMonth',
			name: 'data[Contact][date][month]',
			value: $('#CustomerContactDateMonth').val()
		}).appendTo('#CustomerContactAdminViewForm');

		// 連絡日時・日
		$('<input>').attr({
			type: 'hidden',
			id: 'ContactDateDay',
			name: 'data[Contact][date][day]',
			value: $('#CustomerContactDateDay').val()
		}).appendTo('#CustomerContactAdminViewForm');

		// 連絡時間・時
		$('<input>').attr({
			type: 'hidden',
			id: 'ContactTimeHour',
			name: 'data[Contact][time][hour]',
			value: $('#CustomerContactTimeHour').val()
		}).appendTo('#CustomerContactAdminViewForm');
		
		// 連絡時間・分
		$('<input>').attr({
			type: 'hidden',
			id: 'ContactTimeMin',
			name: 'data[Contact][time][min]',
			value: $('#CustomerContactTimeMin').val()
		}).appendTo('#CustomerContactAdminViewForm');

		// user_id
		$('<input>').attr({
			type: 'hidden',
			id: 'ContactUserId',
			name: 'data[Contact][user_id]',
			value: $('#CustomerContactUserId').val()
		}).appendTo('#CustomerContactAdminViewForm');
		
		// 連絡方法
		$('<input>').attr({
			type: 'hidden',
			id: 'ContactHouhou',
			name: 'data[Contact][houhou]',
			value: $('#CustomerContactHouhou').val()
		}).appendTo('#CustomerContactAdminViewForm');

		// 連絡詳細
		$('<input>').attr({
			type: 'hidden',
			id: 'ContactSyousai',
			name: 'data[Contact][syousai]',
			value: $('#CustomerContactSyousai').val()
		}).appendTo('#CustomerContactAdminViewForm');

		$.ajax({
			type: 'POST',
			url: '<?php echo $this->Html->url(array('plugin' => 'customers', 'controller' => 'customer_contacts', 'action' => 'update', 'admin' => true)); ?>',
			data: $('#CustomerContactAdminViewForm').serializeArray(),
			success: function (data, textStatus, jpXHR) {
				/* 
					本番環境ではjsonではなくStringで返ってくる事象があったため、
					型判定し、objectでなければjson変換する
				*/
				if (typeof data != 'object') {
					data = (new Function("return " + data))();
				}
				contactMessage(data.message, data.succeed);
				if (data.succeed) {
					$('#CustomerContactAdminViewForm').find('#CustomerContactId, select, textarea').val('');
					/*
						20190522 sohnishi
						施設項目追加
					*/
					$('#ContactFacilityId').val('');
					$('#CustomerContactShisetsumei').val('');
					$("#ContactStaffId").children().remove();

					$('#contactForm').removeClass('in').parent().find(".fa-minus").removeClass("fa-minus").addClass("fa-plus");

					var datetime = new Date();
					var year = datetime.getFullYear();
					var month = ("0" + (datetime.getMonth() + 1)).slice(-2);
					var day = ("0" + datetime.getDate()).slice(-2);
					var hour = ("0" + datetime.getHours()).slice(-2);
					var minute = ("0" + datetime.getMinutes()).slice(-2);
					$('#CustomerContactDateYear').val(year);
					$('#CustomerContactDateMonth').val(month);
					$('#CustomerContactDateDay').val(day);
					$('#CustomerContactTimeHour').val(hour);
					$('#CustomerContactTimeMin').val(minute);
					
					contactRecords();
				}
				
			},
			error: function (jpXHR, textStatus, errorThrown) {
				contactMessage('通信に失敗しました' + errorThrown, false);
			}
		});
	});

	$('#contactReset').on('click', function () {
		$('#CustomerContactAdminViewForm').find('#CustomerContactId, textarea').val('');
		$('#CustomerContactDateYear, #CustomerContactDateMonth, #CustomerContactDateDay, #CustomerContactTimeHour, #CustomerContactTimeMin, #CustomerContactHouhou').each(function () {
			this.selectedIndex = 0;
		});
		var datetime = new Date();
		var year = datetime.getFullYear();
		var month = ("0" + (datetime.getMonth() + 1)).slice(-2);
		var day = ("0" + datetime.getDate()).slice(-2);
		var hour = ("0" + datetime.getHours()).slice(-2);
		var minute = ("0" + datetime.getMinutes()).slice(-2);
		$('#CustomerContactDateYear').val(year);
		$('#CustomerContactDateMonth').val(month);
		$('#CustomerContactDateDay').val(day);
		$('#CustomerContactTimeHour').val(hour);
		$('#CustomerContactTimeMin').val(minute);
		/*
			20190522 sohnishi
			施設項目追加
		*/
		$('#ContactFacilityId').val('');
		$('#CustomerContactShisetsumei').val('');
		$("#ContactStaffId").children().remove();
	});

	$('#contact-latest').on('click', '.contact-edit', function () {
		/**
		 *	20190619 sohnishi
		 *	更新ボタン押下->閉じる->更新ボタン押下でレイアウトが崩れる事象に対応
		 */
		$('#contactForm').css('height', 'initial');
		$('#contactForm').addClass('in').parent().find(".fa-plus").removeClass("fa-plus").addClass("fa-minus");
		var contactId = $(this).data('contact-id');
		var url = '<?php echo $this->Html->url(array('plugin' => 'customers', 'controller' => 'customer_contacts', 'action' => 'read', 'admin' => true)); ?>';
		$.getJSON(url + '/' + contactId, function (json, status) {
			var contact = json.CustomerContact;
			var contactsCustomerContact = json.ContactsCustomerContact;
			if (status == 'success') {
				
				if (contactsCustomerContact.length >= 1) {

					var facility_id = contactsCustomerContact[0]['Contact']['facility_id'];
					var staff_id = contactsCustomerContact[0]['Contact']['staff_id'];
					// 施設担当者をセット
					var staffs;

					// 空文字判定
					if (facility_id != null && facility_id.length >= 1) {


						$.ajax({
							type: 'GET',
							url: '<?php echo $this->Html->url(array('plugin' => 'facilities', 'controller' => 'facilities', 'action' => 'find_by_number', 'admin' => true)); ?>',
							data: {
								'q':facility_id,
							},
							success: function (data, textStatus, jpXHR) {
								var jsonObj = $.parseJSON(data);
								/*
									検索結果がnotnull且つ1件であれば、
									法人名 + 施設名をセット
								*/
								if (jsonObj != null && jsonObj.length == 1) {
									shisetsumei = jsonObj[0]['name'];
									$('#ContactFacilityId').val(facility_id);
									$('#CustomerContactShisetsumei').val(shisetsumei);

									$("#ContactStaffId").children().remove();
									staffs = jsonObj[0]['staffs'];
									$.each(staffs, function(index, value){
										var option = "<option value=";
										option += value['id'];
										option += ">";
										option += value['name'];
										option += "</option>";
										$("#ContactStaffId").append(option);
									})

									$('#ContactStaffId').val(staff_id);
									isFindByNumberSuccessed = true;
								} else {
									// 施設番号での検索時は、データが見つからない場合、全項目削除
									$('#CustomerContactShisetsumei').val('');
									$('#ContactFacilityId').val('');	
									$("#ContactStaffId").children().remove();
									isFindByNumberSuccessed = false;
								}

							},
							error: function (jpXHR, textStatus, errorThrown) {
								console.log('通信に失敗しました' + errorThrown, false);
							}
						});
					}

				}
				
				$('#CustomerContactId').val(contact.id);
				$('#CustomerContactCustomerId').val(contact.customer_id);
				$('#CustomerContactDateYear').val(contact.year);
				$('#CustomerContactDateMonth').val(contact.month);
				$('#CustomerContactDateDay').val(contact.day);
				$('#CustomerContactTimeHour').val(contact.hour);
				$('#CustomerContactTimeMin').val(contact.min);
				$('#CustomerContactUserId').val(contact.user_id);
				$('#CustomerContactHouhou').val(contact.houhou);
				$('#CustomerContactSyousai').val(contact.syousai);
			}
		});
	});

	$('#contact-latest').on('click', '.contact-delete', function () {
		var contactId = $(this).data('contact-id');
		if (confirm('本当に削除しても宜しいですか？')) {
			var url = '<?php echo $this->Html->url(array('plugin' => 'customers', 'controller' => 'customer_contacts', 'action' => 'ajaxDelete', 'admin' => true)); ?>';
			$.getJSON(url + '/' + contactId, function (json, status) {
				if (status == 'success') {
					contactMessage(json.message, json.succeed);
					if (json.succeed) {
						contactRecords();
					}
				}
			});
		}
	});

	function contactMessage(text, succeed) {
		var color = succeed ? 'text-success' : 'text-warning';
		var msg = $('<p />').attr('class', color).text(text);
		$('#contact-message').append(msg);
		setTimeout(function () {
			$('#contact-message').find('p').remove();
		}, 3000);
	}

	// 20190701 sohnishi
	// window.loadでは呼ばれない事象が発生したため、下記対応
	contactRecords();
	$(window).load(function () {
		// contactRecords();
	});
});
</script>
