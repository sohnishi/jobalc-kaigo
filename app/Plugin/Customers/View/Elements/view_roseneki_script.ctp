<script>
$(function () {

	function rosenekiRecords() {
		$('#roseneki-latest').empty();
		var url = '<?php echo $this->Html->url(array('plugin' => 'customers', 'controller' => 'customer_rosenekis', 'action' => 'getLatest', $customerId, 'admin' => true)); ?>';
		$.getJSON(url, function (json, status) {
			if (status == 'success') {
				if ($.isEmptyObject(json)) {
					$('#roseneki-latest').append('<p>最寄り路線・最寄り駅が登録されていません</p>');
				} else {
					$.each(json, function () {
						var _this = this;
						var html = '<table class="table table-bordered table-condensed table-fixed" style="margin-bottom:5px;"><tbody>';
						html += '<tr>';
						html += '<th>最寄り路線・駅</th>';
						html += '<td>';
						html += _this.RailroadLine.line_name;
						html += ' ' + _this.RailroadStation.station_name + '駅';
						html += '</td>';
						html += '<td>';
						html += _this.CustomerRoseneki.distance;
						html += '</td>';
						html += '<td style="width:60px;text-align:right;">';
							html += '<button type="button" class="btn btn-danger btn-xs roseneki-delete" data-roseneki-id="' + _this.CustomerRoseneki.id + '">削除</button>&nbsp;&nbsp;';
						html += '</td>';
						html += '</tr>';
						html += '</tbody></table>';
						$('#roseneki-latest').append(html);
					});
				}
			}
		});
	}

	$('#rosenekiSubmit').on('click', function () {
		$.ajax({
			type: 'POST',
			url: '<?php echo $this->Html->url(array('plugin' => 'customers', 'controller' => 'customer_rosenekis', 'action' => 'update', 'admin' => true)); ?>',
			data: $('#CustomerRosenekiAdminViewForm').serializeArray(),
			success: function (data, textStatus, jqXHR) {
				var jsonObj;
				/* 
					20190618 sohnishi
					型判定し、objectでなければjson変換する
				*/
				if (typeof data != 'object') {
					jsonObj = $.parseJSON(data);
				} else {
					jsonObj = data;
				}
				rosenekiMessage(jsonObj.message, jsonObj.succeed);
				if (jsonObj.succeed) {
					$('#CustomerRosenekiAdminViewForm').find('CustomerRosenekiCustomerId, input[type="text"], select').val('');
					$('#rosenekiForm').removeClass('in').parent().find(".fa-minus").removeClass("fa-minus").addClass("fa-plus");
					rosenekiRecords();
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				rosenekiMessage('通信に失敗しました', false);
			}
		});
	});

	$('#rosenekiReset').on('click', function () {
		$('#CustomerRosenekiAdminViewForm').find('CustomerRosenekiCustomerId, input[type="text"], select').val('');
	});

	$('#roseneki-latest').on('click', '.roseneki-delete', function () {
		var rosenId = $(this).data('roseneki-id');
		if (confirm('本当に削除しても宜しいですか？')) {
			var url = '<?php echo $this->Html->url(array('plugin' => 'customers', 'controller' => 'customer_rosenekis', 'action' => 'ajaxDelete', 'admin' => true)); ?>';
			$.getJSON(url + '/' + rosenId, function (json, status) {
				console.log(json);
				if (status == 'success') {
					rosenekiMessage(json.message, json.succeed);
					if (json.succeed) {
						rosenekiRecords();
					}
				}
			});
		}
	});

	function rosenekiMessage(text, succeed) {
		var color = succeed ? 'text-success' : 'text-warning';
		var msg = $('<p />').attr('class', color).text(text);
		$('#roseneki-message').append(msg);
		setTimeout(function () {
			$('#roseneki-message').find('p').remove();
		}, 3000);
	}

	$(window).load(function () {
		rosenekiRecords();
	});
});
</script>