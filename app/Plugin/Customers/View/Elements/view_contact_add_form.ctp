
<div class="panel panel-default">
	<div class="panel-heading">
		<a href="#contactForm" data-toggle="collapse" aria-expanded="false" aria-controls="contactForm"><i class="fa fa-plus"></i> 連絡履歴を追加</a>
	</div>
	<div class="panel-body collapse" id="contactForm">

		<?php
			echo $this->Form->create('CustomerContact', array(
				'inputDefaults' => array(
					'label' => false,
					'wrapInput' => false,
					'div' => false,
					'class' => 'form-control'
				),
				'url' => array(
					'plugin' => 'customers',
					'controller' => 'customer_contacts',
					'action' => 'update',
					'admin' => true
				),
				'default' => false
			));

			echo $this->Form->input('id', array('type' => 'hidden'));
			echo $this->Form->input('customer_id', array('type' => 'hidden', 'value' => $customerId));
		?>

		<div class="row">
			<div class="col-xs-12">
				<?php echo $this->Form->label('date', '連絡日時'); ?>
				<div class="form-group">
					<?php echo $this->Form->input('date', array('type' => 'date', 'dateFormat' => 'YMD', 'monthNames' => false, 'minYear' => date('Y') - 60, 'maxYear' => date('Y'), 'separator' => array('年', '月', '日'), 'style' => 'width:25%;display:inline-block;margin-right:5px;margin-left:5px;')); ?>
				</div>

				<?php echo $this->Form->label('time', '連絡時間'); ?>
				<div class="form-group">
					<?php echo $this->Form->input('time', array('type' => 'time', 'timeFormat' => 24, 'style' => 'width:20%;display:inline-block;margin-right:5px;margin-left:5px;')); ?>
				</div>

				<!-- 
					20190521 sohnishi
					施設番号追加(施設ID)
				 -->
				<?php echo $this->Form->label('Facility.id', '施設番号'); ?>
				<div class="row">
					<div class="col-xs-6">
						<div class="form-group">
							<?php echo $this->Form->input('Contact.facility_id', array('type' => 'number')); ?>
						</div>
					</div>
				</div>

				<!-- 
					20190521 sohnishi
					施設名追加
				 -->
				<?php echo $this->Form->label('shisetsumei', '施設名'); ?>
				<div class="row">
					<div class="col-xs-6">
						<div class="form-group">
							<?php echo $this->Form->input('shisetsumei'); ?>
							<span class='help-block' style="font-size:11px;">2文字以上入力すると自動補完されます</span>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-6">
						<div class="form-group">
							<?php echo $this->Form->input('Contact.staff_id', array('label' => '施設担当者')); ?>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-6">
						<div class="form-group">
							<?php echo $this->Form->input('user_id', array('value' => $currentUser['id'], 'empty' => '選択してください', 'label' => '担当者')); ?>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-6">
						<div class="form-group">
						<?php echo $this->Form->input('houhou', array('type' => 'select', 'options' => $renraku_houhou, 'empty' => '選択してください', 'label' => '連絡方法')); ?>
						</div>
					</div>
				</div>

				<div class="form-group">
					<?php echo $this->Form->input('syousai', array('type' => 'textarea', 'style' => 'resize:vertical;', 'label' => '連絡詳細')); ?>
				</div>
			</div>
		</div>

		<div class="form-group">
			<?php echo $this->Form->button('送信する', array('type' => 'button', 'id' => 'contactSubmit', 'class' => 'btn btn-primary', 'div' => false)); ?>
			<?php echo $this->Form->button('リセット', array('type' => 'button', 'id' => 'contactReset', 'class' => 'btn btn-warning', 'div' => false)); ?>
		</div>

		<?php echo $this->Form->end(); ?>
	</div>
</div>
<div id="contact-message"></div>