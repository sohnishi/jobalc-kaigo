<script>
$(function () {

	function conditionRecords() {
		$('#condition-latest').empty();
		var url = '<?php echo $this->Html->url(array('plugin' => 'customers', 'controller' => 'customer_conditions', 'action' => 'getLatest', $customerId, 'admin' => true)); ?>';
		$.getJSON(url, function (json, status) {
			if (status == 'success') {
				if ($.isEmptyObject(json)) {
					$('#condition-latest').append('<p>希望条件が登録されていません</p>');
				} else {
					$.each(json, function () {
						var _this = this;
						var condition = _this.CustomerCondition;
						var shisetsukeitaiSeconds = <?php echo json_encode($shisetsukeitaiSeconds); ?>;
						var kinmukaishijiki = <?php echo json_encode($kinmukaishijiki); ?>;
						var tsukinhouhou = <?php echo json_encode($tsukinhouhou); ?>;
						var tsukinjikan = <?php echo json_encode($tsukinjikan); ?>;
						var kiboukyuyo = <?php echo json_encode($kiboukyuyo); ?>;
						var kyujitsus = <?php echo json_encode($kyujitsus); ?>;
						var nashiari = <?php echo json_encode($nashiari); ?>;
						var html = '<table class="table table-bordered table-condenced table-fixed" style="margin-bottom:5px;"><tbody>';
						html += '<tr>';
						html += '<th>希望の働き方</th><td>';
						html += condition.kibouwork;
						html += '</td>';
						html += '<th>希望給与</th><td>';
						html += condition.kiboukyuyo ? kiboukyuyo[condition.kiboukyuyo] : '';
						html += '</td>';
						html += '</tr>';
						html += '<tr>';
						html += '<th>希望部署</th><td>';
						var kiboubusyo = '';
						if (condition.kiboubusyo !== null) {
							kiboubusyo = condition.kiboubusyo.replace(/\n/g, "<br>");
						}
						html += kiboubusyo;
						html += '</td>';
						html += '<th>希望休日</th><td>';
						html += condition.kyujitsu_id ? kyujitsus[condition.kyujitsu_id] : '';
						html += '</td>';
						html += '</tr>';
						html += '<tr>';
						html += '<th>勤務開始時期</th><td>';
						html += condition.kinmukaishijiki ? kinmukaishijiki[condition.kinmukaishijiki] : '';
						html += '</td>';
						html += '<th>扶養人数</th><td>';
						html += condition.huyouninzu > 0 ? condition.huyouninzu + "人" : '';
						html += '</td>';
						html += '</tr>';
						html += '<tr>';
						html += '<th>現状</th><td>';
						var genjou = '';
						if (condition.genjou !== null) {
							genjou = condition.genjou.replace(/\n/g, "<br>");
						}
						html += genjou;
						html += '</td>';
						html += '<th>寮</th><td>';
						html += condition.ryou ? nashiari[condition.ryou] : '';
						html += '</td>';
						html += '</tr>';
						html += '<tr>';
						html += '<th>通勤方法</th><td>';
						html += condition.tsukinhouhou ? tsukinhouhou[condition.tsukinhouhou] : '';
						html += '</td>';
						html += '<th>託児所</th><td>';
						html += condition.takujisyo ? nashiari[condition.takujisyo] : '';
						html += '</td>';
						html += '</tr>';
						html += '<tr>';
						html += '<th>通勤時間</th><td>';
						html += condition.tsukinjikan ? tsukinjikan[condition.tsukinjikan] : '';
						html += '</td>';
						html += '<th>面接可能日</th><td>';
						var mensetsukanoubi = '';
						if (condition.mensetsukanoubi !== null) {
							mensetsukanoubi = condition.mensetsukanoubi.replace(/\n/g, "<br>");
						}
						html += mensetsukanoubi;
						html += '</td>';
						html += '</tr>';
						html += '<tr>';
						html += '<td colspan="4"><div class="pull-left">';
						html += '更新日：' + condition.modified;
						html += '</div>';
						html += '<div class="pull-right">';
						html += '<button type="button" class="btn btn-danger btn-xs condition-delete" data-condition-id="' + condition.id + '">削除</button>&nbsp;&nbsp;';
						html += '<button type="button" class="btn btn-default btn-xs condition-edit" data-condition-id="' + condition.id + '">更新</button>';
						html += '</div></td>';
						html += '</tr>';
						html += '</tbody></table>';
						$('#condition-latest').append(html);
					});
				}
			}
		});
	}

	$('#conditionSubmit').on('click', function () {
		$.ajax({
			type: 'POST',
			url: '<?php echo $this->Html->url(array('plugin' => 'customers', 'controller' => 'customer_conditions', 'action' => 'update', 'admin' => true)); ?>',
			data: $('#CustomerConditionAdminViewForm').serializeArray(),
			success: function (data, textStatus, jpXHR) {
				var jsonObj;
				/* 
					20190619 sohnishi
					型判定し、objectでなければjson変換する
				*/
				if (typeof data != 'object') {
					jsonObj = $.parseJSON(data);
				} else {
					jsonObj = data;
				}
				conditionMessage(jsonObj.message, jsonObj.succeed);
				if (jsonObj.succeed) {
					$('#CustomerConditionAdminViewForm').find('#CustomerConditionId, input[type="text"], select, textarea').val('');
					$('#conditionForm').removeClass('in').parent().find(".fa-minus").removeClass("fa-minus").addClass("fa-plus");
					conditionRecords();
				}
			},
			error: function (jpXHR, textStatus, errorThrown) {
				conditionMessage('通信に失敗しました' + errorThrown, false);
			}
		});
	});

	$('#conditionReset').on('click', function () {
		$('#CustomerConditionAdminViewForm').find('#CustomerConditionId, input[type="text"], select, textarea').val('');
	});

	$('#condition-latest').on('click', '.condition-edit', function () {
		/**
		 *	20190619 sohnishi
		 *	更新ボタン押下->閉じる->更新ボタン押下でレイアウトが崩れる事象に対応
		 */
		$('#conditionForm').css('height', 'initial');
		$('#conditionForm').addClass('in').parent().find(".fa-plus").removeClass("fa-plus").addClass("fa-minus");
		var conditionId = $(this).data('condition-id');
		var url = '<?php echo $this->Html->url(array('plugin' => 'customers', 'controller' => 'customer_conditions', 'action' => 'read', 'admin' => true)); ?>';
		$.getJSON(url + '/' + conditionId, function (json, status) {
			var condition = json.CustomerCondition;
			if (status == 'success') {
				$('#CustomerConditionId').val(condition.id);
				$('#CustomerConditionCustomerId').val(condition.customer_id);
				$('#CustomerConditionKibouwork').val(condition.kibouwork);
				$('#CustomerConditionKiboubusyo').val(condition.kiboubusyo);
				$('#CustomerConditionKinmukaishijiki').val(condition.kinmukaishijiki);
				$('#CustomerConditionTsukinhouhou').val(condition.tsukinhouhou);
				$('#CustomerConditionTsukinjikan').val(condition.tsukinjikan);
				$('#CustomerConditionKiboukyuyo').val(condition.kiboukyuyo);
				$('#CustomerConditionKyujitsuId').val(condition.kyujitsu_id);
				$('#CustomerConditionHuyouninzu').val(condition.huyouninzu);
				$('#CustomerConditionRyou').val(condition.ryou);
				$('#CustomerConditionTakujisyo').val(condition.takujisyo);
				$('#CustomerConditionMensetsukanoubi').val(condition.mensetsukanoubi);
				$('#CustomerConditionGenjou').val(condition.genjou);
			}
		});
	});

	$('#condition-latest').on('click', '.condition-delete', function () {
		var conditionId = $(this).data('condition-id');
		if (confirm('本当に削除しても宜しいですか？')) {
			var url = '<?php echo $this->Html->url(array('plugin' => 'customers', 'controller' => 'customer_conditions', 'action' => 'ajaxDelete', 'admin' => true)); ?>';
			$.getJSON(url + '/' + conditionId, function (json, status) {
				if (status == 'success') {
					conditionMessage(json.message, json.succeed);
					if (json.succeed) {
						conditionRecords();
					}
				}
			});
		}
	});

	function conditionMessage(text, succeed) {
		var color = succeed ? 'text-success' : 'text-warning';
		var msg = $('<p />').attr('class', color).text(text);
		$('#condition-message').append(msg);
		setTimeout(function () {
			$('#condition-message').find('p').remove();
		}, 3000);
	}

	$(window).load(function () {
		conditionRecords();
	});
});
</script>
