<?php
//----- Excel出力用ライブラリ読み込み
App::import('Vendor', 'PHPExcel', array('file' => 'PhpExcel' . DS . 'PHPExcel.php'));
App::import('Vendor', 'PHPExcel_IOFactory', array('file' => 'PhpExcel' . DS . 'PHPExcel' . DS . 'IOFactory.php'));
App::import('Vendor', 'PHPExcel_Cell_AdvancedValueBinder', array('file' => 'PhpExcel' . DS . 'PHPExcel' . DS . 'Cell' . DS . 'AdvancedValueBinder.php'));

//----- PHPExcelオブジェクト作成
$template = APP . DS . 'Config' . DS . 'excel' . DS . 'careersheet.xlsx';
$obj = PHPExcel_IOFactory::createReader('Excel2007');
$book = $obj->load($template);

//----- シートの設定
$book->setActiveSheetIndex(0);
$sheet = $book->getActiveSheet();

//----- セルにデータをセット
// 担当者
$sheet->setCellValue('L3', $data['User']['name']);
$sheet->setCellValue('E42', $data['User']['name']);
// タイトル 最初資格
$certificate = '';
if (count($data['CustomerCertificate']) > 0) {
	$certificate = $shikaku[$data['CustomerCertificate'][0]['shikaku']];
}
$sheet->setCellValue('A4', $certificate);
// 顧客番号
$sheet->setCellValue('D11', $data['Customer']['number']);
// 年齢
$sheet->setCellValue('D12', sprintf('%s 歳', $data['Customer']['age']));
// 性別
$seibetsu = '';
if ($data['Customer']['sex'] != 0) {
	$seibetsu = $sex[$data['Customer']['sex']];
}
$sheet->setCellValue('D13', $seibetsu);
// 住所 市区町村まで
$todoufuken = empty($data['Customer']['todouhuken']) ? '' : $data['Customer']['todoufuken'];
$shikutyouson = empty($data['Customer']['shikutyouson']) ? '' : $data['Customer']['shikutyouson'];
$address = sprintf('%s%s', $todoufuken, $shikutyouson);
$sheet->setCellValue('D14', $address);
// 現状
$genjou = '';
if (count($data['CustomerCondition']) > 0) {
	$genjou = $data['CustomerCondition'][0]['genjou'];
}
$sheet->setCellValue('D15', $genjou);
// 保有資格
$hoyu1shikaku = '';
$hoyu1syutokubi = '';
$hoyu2shikaku = '';
$hoyu2syutokubi = '';
if (count($data['CustomerCertificate']) > 0) {
	foreach ($data['CustomerCertificate'] as $key => $value) {
		if ($key == 0) {
			$hoyu1shikaku = ($value['shikaku'] == null) ? '' : $shikaku[$value['shikaku']];
			$hoyu1syutokubi = empty($value['syutokubi']) ? '' : sprintf('%s年取得', $value['syutokubi']);
		}
		if ($key == 1) {
			$hoyu2shikaku = ($value['shikaku'] == null) ? '' : $shikaku[$value['shikaku']];
			$hoyu2syutokubi = empty($value['syutokubi']) ? '' : sprintf('%s年取得', $value['syutokubi']);
		}
	}
}
$sheet->setCellValue('L12', $hoyu1shikaku);
$sheet->setCellValue('L13', $hoyu1syutokubi);
$sheet->setCellValue('L14', $hoyu2shikaku);
$sheet->setCellValue('L15', $hoyu2syutokubi);


// 職務経歴
$office1 = $keitai1 = $kikan1 = '';
$office2 = $keitai2 = $kikan2 = '';
$office3 = $keitai3 = $kikan3 = '';
$office4 = $keitai4 = $kikan4 = '';
$office5 = $keitai5 = $kikan5 = '';
$office6 = $keitai6 = $kikan6 = '';
$office7 = $keitai7 = $kikan7 = '';
$office8 = $keitai8 = $kikan8 = '';

if (count($data['CustomerCareer']) > 0) {
	foreach ($data['CustomerCareer'] as $i => $career) {
		if ($i == 0) {
			//$office1 = $career['ShisetsukeitaiFirst']['name'];
			//$office1 = $career['entry'];
			$office1 = $career['entry'] .'　'. $shisetsukeitaiSeconds[$career['shisetsukeitai_second_id']] .'　'. $shinryoukamokus[$career['shinryoukamoku_id']];
			$keitai1 = ($career['kinmu_keitai'] == null) ? '' : $kinmukeitai[$career['kinmu_keitai']];
			$kikan1 = $career['zaiseki'];
		}
		if ($i == 1) {
			//$office2 = $career['ShisetsukeitaiFirst']['name'];
			//$office2 = $career['entry'];
			$office2 = $career['entry'] .'　'. $shisetsukeitaiSeconds[$career['shisetsukeitai_second_id']] .'　'. $shinryoukamokus[$career['shinryoukamoku_id']];
			$keitai2 = ($career['kinmu_keitai'] == null) ? '' : $kinmukeitai[$career['kinmu_keitai']];
			$kikan2 = $career['zaiseki'];
		}
		if ($i == 2) {
			//$office3 = $career['ShisetsukeitaiFirst']['name'];
			//$office3 = $career['entry'];
			$office3 = $career['entry'] .'　'. $shisetsukeitaiSeconds[$career['shisetsukeitai_second_id']] .'　'. $shinryoukamokus[$career['shinryoukamoku_id']];
			$keitai3 = ($career['kinmu_keitai'] == null) ? '' : $kinmukeitai[$career['kinmu_keitai']];
			$kikan3 = $career['zaiseki'];
		}
		if ($i == 3) {
			//$office4 = $career['ShisetsukeitaiFirst']['name'];
			//$office4 = $career['entry'];
			$office4 = $career['entry'] .'　'. $shisetsukeitaiSeconds[$career['shisetsukeitai_second_id']] .'　'. $shinryoukamokus[$career['shinryoukamoku_id']];
			$keitai4 = ($career['kinmu_keitai'] == null) ? '' : $kinmukeitai[$career['kinmu_keitai']];
			$kikan4 = $career['zaiseki'];
		}
		if ($i == 4) {
			//$office5 = $career['ShisetsukeitaiFirst']['name'];
			//$office5 = $career['entry'];
			$office5 = $career['entry'] .'　'. $shisetsukeitaiSeconds[$career['shisetsukeitai_second_id']] .'　'. $shinryoukamokus[$career['shinryoukamoku_id']];
			$keitai5 = ($career['kinmu_keitai'] == null) ? '' : $kinmukeitai[$career['kinmu_keitai']];
			$kikan5 = $career['zaiseki'];
		}
		if ($i == 5) {
			//$office6 = $career['ShisetsukeitaiFirst']['name'];
			//$office6 = $career['entry'];
			$office6 = $career['entry'] .'　'. $shisetsukeitaiSeconds[$career['shisetsukeitai_second_id']] .'　'. $shinryoukamokus[$career['shinryoukamoku_id']];
			$keitai6 = ($career['kinmu_keitai'] == null) ? '' : $kinmukeitai[$career['kinmu_keitai']];
			$kikan6 = $career['zaiseki'];
		}
		if ($i == 6) {
			//$office7 = $career['ShisetsukeitaiFirst']['name'];
			//$office7 = $career['entry'];
			$office7 = $career['entry'] .'　'. $shisetsukeitaiSeconds[$career['shisetsukeitai_second_id']] .'　'. $shinryoukamokus[$career['shinryoukamoku_id']];
			$keitai7 = ($career['kinmu_keitai'] == null) ? '' : $kinmukeitai[$career['kinmu_keitai']];
			$kikan7 = $career['zaiseki'];
		}
		if ($i == 7) {
			//$office8 = $career['ShisetsukeitaiFirst']['name'];
			//$office8 = $career['entry'];
			$office8 = $career['entry'] .'　'. $shisetsukeitaiSeconds[$career['shisetsukeitai_second_id']] .'　'. $shinryoukamokus[$career['shinryoukamoku_id']];
			$keitai8 = ($career['kinmu_keitai'] == null) ? '' : $kinmukeitai[$career['kinmu_keitai']];
			$kikan8 = $career['zaiseki'];
		}
	}
}

// 1
$sheet->setCellValue('D19', $office1);
$sheet->setCellValue('L19', $keitai1);
$sheet->setCellValue('M19', $kikan1);
// 2
$sheet->setCellValue('D20', $office2);
$sheet->setCellValue('L20', $keitai2);
$sheet->setCellValue('M20', $kikan2);
// 3
$sheet->setCellValue('D21', $office3);
$sheet->setCellValue('L21', $keitai3);
$sheet->setCellValue('M21', $kikan3);
// 4
$sheet->setCellValue('D22', $office4);
$sheet->setCellValue('L22', $keitai4);
$sheet->setCellValue('M22', $kikan4);
// 5
$sheet->setCellValue('D23', $office5);
$sheet->setCellValue('L23', $keitai5);
$sheet->setCellValue('M23', $kikan5);
// 6
$sheet->setCellValue('D24', $office6);
$sheet->setCellValue('L24', $keitai6);
$sheet->setCellValue('M24', $kikan6);
// 7
$sheet->setCellValue('D25', $office7);
$sheet->setCellValue('L25', $keitai7);
$sheet->setCellValue('M25', $kikan7);
// 8
$sheet->setCellValue('D26', $office8);
$sheet->setCellValue('L26', $keitai8);
$sheet->setCellValue('M26', $kikan8);

//転職希望条件
$kibouwork = '';
$kinmujiki = '';
$tsukin = '';
$kiboubusyo = '';
$ryoutakujisyo = '';
$huyouninzu = '';
if (count($data['CustomerCondition']) > 0) {
	// 雇用形態(希望の働き方)
	$kibouwork = $data['CustomerCondition'][0]['kibouwork'];
	// 勤務開始
	$kinmujiki = $kinmukaishijiki[$data['CustomerCondition'][0]['kinmukaishijiki']];
	// 通勤手段
	$tsukin = $tsukinhouhou[$data['CustomerCondition'][0]['tsukinhouhou']];
	// 配属希望部署
	$kiboubusyo = $data['CustomerCondition'][0]['kiboubusyo'];
	// 寮・託児所
	$ryou = $kibou[$data['CustomerCondition'][0]['ryou']];
	$takujisyo = $kibou[$data['CustomerCondition'][0]['takujisyo']];
	$ryoutakujisyo = sprintf('寮：%s 託児所：%s', $ryou, $takujisyo);
	// 扶養人数
	$huyouninzu = empty($data['CustomerCondition'][0]['huyouninzu']) || ($data['CustomerCondition'][0]['huyouninzu'] == 0) ? 'なし' : sprintf('%s人', $data['CustomerCondition'][0]['huyouninzu']);
}
$sheet->setCellValue('D30', $kibouwork);
$sheet->setCellValue('D31', $kinmujiki);
$sheet->setCellValue('D32', $tsukin);
$sheet->setCellValue('L30', $kiboubusyo);
$sheet->setCellValue('L31', $ryoutakujisyo);
$sheet->setCellValue('L32', $huyouninzu);

//----- 入力規則設定
// 住所
$jyusyo = $sheet->getCell('D43')->getDataValidation();
$jyusyo->setType(PHPExcel_Cell_DataValidation::TYPE_LIST);
$jyusyo->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
$jyusyo->setAllowBlank(false);
$jyusyo->setShowInputMessage(true);
$jyusyo->setShowErrorMessage(true);
$jyusyo->setShowDropDown(true);
$jyusyo->setFormula1('入力補助!$A$1:$A$3');

// 社用携帯
$tantoutel = $sheet->getCell('F44')->getDataValidation();
$tantoutel->setType(PHPExcel_Cell_DataValidation::TYPE_LIST);
$tantoutel->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
$tantoutel->setAllowBlank(false);
$tantoutel->setShowInputMessage(true);
$tantoutel->setShowErrorMessage(true);
$tantoutel->setShowDropDown(true);
$tantoutel->setFormula1('入力補助!$B$1:$B$30');

//----- 出力ファイル名
// customer_顧客番号_年月日_時分.xlsx
$filename = 'customer_';
$filename .= $data['Customer']['number'];
$filename .= '_' . date('Ymd_Hi');
$filename .= '.xlsx';

//----- Excel2007形式で出力する準備
Configure::write('debug', 0);
header('Content-Type: application/octet-stream');
// ダウンロードするファイル名を設定
header('Content-Disposition: attachment;filename="' . $filename . '"');

//----- Excel2007形式で出力
$writer = PHPExcel_IOFactory::createWriter($book, "Excel2007");
$writer->save('php://output');
