<?php
	$rank = Configure::read('rank');
	$sex = Configure::read('sex');
	$touroku_keiro = Configure::read('touroku_keiro');
	$marital_status = Configure::read('marital_status');
?>
<?php
	echo $this->Html->script('ajaxzip3', array('inline' => false));
	echo $this->Html->script('jquery.autoKana', array('inline' => false));
?>
<?php echo $this->Html->scriptStart(); ?>
$(document).ready(function () {
	AjaxZip3.JSONDATA = '<?php echo $this->Html->url('/js/zipdata'); ?>';
	$.fn.autoKana('#CustomerName', '#CustomerHurigana', {katakana: true});
});
<?php echo $this->Html->scriptEnd(); ?>
<div class="row">
	<div class="col-xs-12 contents-actions">
		<ul class="nav nav-tabs">
			<li><?php echo $this->Html->link('顧客検索', array('action' => 'index', '?' => $this->Session->read('customerquerystring'))); ?></li>
			<li class="active"><a href="javascript:void();">顧客を新規登録</a></li>
		</ul>
	</div>
</div>

<?php
	echo $this->Form->create($model, array(
		'inputDefaults' => array(
			'label' => false,
			'wrapInput' => false,
			'div' => false,
			'class' => 'form-control'
		)
	));
?>

<div class="row">
	<div class="col-xs-6">

		<div class="panel panel-default">
			<div class="panel-heading">基本情報</div>

			<table class="table table-bordered">
				<tbody>
					<tr>
						<th>担当者</th>
						<td colspan="3"><?php echo $this->Form->input('user_id', array('type' => 'select', 'options' => $users)); ?></td>
					</tr>
					<tr>
						<th>フリガナ <span class="req">*</span></th>
						<td colspan="3">
							<div class="row">
								<div class="col-xs-6">
									<?php echo $this->Form->input('hurigana'); ?>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<th>名前 <span class="req">*</span></th>
						<td colspan="3">
							<div class="row">
								<div class="col-xs-6">
									<?php echo $this->Form->input('name'); ?>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<th>生年月日</th>
						<td colspan="3"><?php echo $this->Form->input('birth', array('type' => 'date', 'dateFormat' => 'YMD', 'monthNames' => false, 'minYear' => date('Y') - 70, 'maxYear' => date('Y') - 16, 'separator' => array('年', '月', '日'), 'style' => 'width:25%;display:inline-block;margin-right:5px;margin-left:5px;')); ?></td>
					</tr>
					<tr>
						<th>ランク</th>
						<td><?php echo $this->Form->input('rank', array('type' => 'select', 'options' => $rank)); ?></td>
						<th>性別</th>
						<td><?php echo $this->Form->input('sex', array('type' => 'select', 'options' => $sex)); ?></td>
					</tr>
					<tr>
						<th>登録経路</th>
						<td><?php echo $this->Form->input('touroku_keiro', array('type' => 'select', 'options' => $touroku_keiro)); ?></td>
						<th>既婚・未婚</th>
						<td><?php echo $this->Form->input('marital_status', array('type' => 'select', 'options' => $marital_status)); ?></td>
					</tr>
					<tr>
						<th>紹介者</th>
						<td colspan="3"><?php echo $this->Form->input('syoukai'); ?></td>
					</tr>
					<tr>
						<th>郵便番号</th>
						<td colspan="3">
							<div class="row">
								<div class="col-xs-4">
									<?php echo $this->Form->input('zip', array('onKeyup' => 'AjaxZip3.zip2addr(this, "", "data[Customer][todoufuken]", "data[Customer][shikutyouson]")')); ?>
								</div>
								<div class="col-xs-8">
									<span class="help-block">ハイフン「-」無し7桁の半角数字で入力してください</span>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<th>都道府県</th>
						<td colspan="3">
							<div class="row">
								<div class="col-xs-6">
									<?php echo $this->Form->input('todoufuken'); ?>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<th>市区町村</th>
						<td colspan="3"><?php echo $this->Form->input('shikutyouson'); ?></td>
					</tr>
					<tr>
						<th>番地</th>
						<td colspan="3"><?php echo $this->Form->input('banchi'); ?></td>
					</tr>
					<tr>
						<th>建物</th>
						<td colspan="3"><?php echo $this->Form->input('tatemono'); ?></td>
					</tr>
					<tr>
						<th>携帯TEL</th>
						<td colspan="3"><?php echo $this->Form->input('keitai_tel'); ?></td>
					</tr>
					<tr>
						<th>自宅TEL</th>
						<td colspan="3"><?php echo $this->Form->input('jitaku_tel'); ?></td>
					</tr>
					<tr>
						<th>メールアドレス</th>
						<td colspan="3"><?php echo $this->Form->input('email'); ?></td>
					</tr>
				</tbody>
			</table>

		</div>

	</div>
	<div class="col-xs-6">

		<div class="panel panel-default">
			<div class="panel-heading">メモ</div>
			<div class="panel-body">

				<?php echo $this->Form->input('memo', array('type' => 'textarea', 'rows' => 10, 'style' => 'resize:vertical;')); ?>

			</div>
		</div>

	</div>
</div>

<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<?php echo $this->Form->submit('顧客情報を登録する', array('class' => 'btn btn-primary', 'div' => false)); ?>
			</div>
		</div>
	</div>
</div>

<?php echo $this->Form->end();