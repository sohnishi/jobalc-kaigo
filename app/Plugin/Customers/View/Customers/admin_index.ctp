<?php
	echo $this->Html->css('bootstrap-multiselect', array('inline' => false));
	echo $this->Html->script('bootstrap-multiselect', array('inline' => false));
	echo $this->Html->script('ajaxzip3', array('inline' => false));

	$sex = Configure::read('sex');
	$rank = Configure::read('rank');
	$kinmukaishijiki = Configure::read('kinmukaishijiki');
	/*
		20190610 sohnishi
		求人と顧客の資格を分ける
	*/
	$shikaku = Configure::read('customer_shikaku');
	// $shikaku = Configure::read('shikaku');
	$tsukinhouhou = Configure::read('tsukinhouhou');
	$tsukinjikan = Configure::read('tsukinjikan');
	$kiboukyuyo = Configure::read('kiboukyuyo');

	// 最寄り路線・駅
	$datas = array(
		'lineEmptyText' => '最寄り路線を選択',
		'stationEmptyText' => '最寄り駅を選択',
		'prefId' => '#CustomerPrefCd',
		'lineId' => '#CustomerLineCd',
		'stationId' => '#CustomerStationCd'
	);
	echo $this->element('interface', $datas, array('plugin' => 'Ekidata'));
?>
<div class="row">
	<div class="col-xs-12 contents-actions">
		<ul class="nav nav-tabs">
			<li class="active"><a href="javascript:void(0);">顧客検索</a></li>
			<li><?php echo $this->Html->link('顧客を新規登録', array('action' => 'add')); ?></li>
		</ul>
	</div>
</div>

<?php echo $this->Html->scriptStart(); ?>
$(function () {
	$('.collapse').on('hidden.bs.collapse', function () {
		$(this).parent().find(".fa-minus").removeClass("fa-minus").addClass("fa-plus");
	}).on('show.bs.collapse', function () {
		$(this).parent().find(".fa-plus").removeClass("fa-plus").addClass("fa-minus");
	});

	$('#CustomerRank, #CustomerUserId, #CustomerTourokuKeiro, #CustomerMaritalStatus, #CustomerShisetsukeitaiFirst, #CustomerShisetsukeitaiSecond, #CustomerShinryoukamoku, #CustomerKinmuKeitai, #CustomerKoyoukeitai, #CustomerKibouBusyo, #CustomerKinmukaishijiki, #CustomerTsukinhouhou, #CustomerTsukinjikan, #CustomerKiboukyuyo, #CustomerKyujitsuId').multiselect({
		nonSelectedText: '選択してください',
		nSelectedText: '選択',
		allSelectedText: '全選択',
		numberDisplayed: 1
	});

	AjaxZip3.JSONDATA = '<?php echo $this->Html->url('/js/zipdata'); ?>';
});
<?php echo $this->Html->scriptEnd(); ?>

<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<a href="#customer-search" data-toggle="collapse" aria-expanded="false" aria-controls="customer-search"><i class="fa fa-plus"></i> 顧客を検索</a>
			</div>
			<div class="panel-body collapse" id="customer-search">

				<?php
					echo $this->Form->create($model, array(
						'inputDefaults' => array(
							'label' => false,
							'wrapInput' => false,
							'div' => false,
							'class' => 'form-control'
						),
						'url' => array_merge(array(
							'plugin' => 'customers',
							'controller' => 'customers',
							'action' => 'index',
							'admin' => true
						), $this->params['pass'])
					));
				?>

				<div class="row">
					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->label('number', '顧客番号'); ?>
							<?php echo $this->Form->input('number', array('type' => 'text')); ?>
						</div>
					</div>

					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->label('user_id', '担当者'); ?>
							<div class="input-group">
								<?php echo $this->Form->input('user_id', array('type' => 'select', 'multiple' => true)); ?>
							</div>
						</div>
					</div>

					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->label('rank', 'ランク'); ?>
							<div class="input-group">
								<?php echo $this->Form->input('rank', array('type' => 'select', 'options' => Configure::read('rank'), 'multiple' => true)); ?>
							</div>
						</div>
					</div>

					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->label('touroku_keiro', '登録経路'); ?>
							<div class="input-group">
								<?php echo $this->Form->input('touroku_keiro', array('type' => 'select', 'options' => Configure::read('touroku_keiro'), 'multiple' => true)); ?>
							</div>
						</div>
					</div>

					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->label('marital_status', '既婚・未婚'); ?>
							<div class="input-group">
								<?php echo $this->Form->input('marital_status', array('type' => 'select', 'options' => Configure::read('marital_status'), 'multiple' => true)); ?>
							</div>
						</div>
					</div>

					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->label('syoukai', '紹介者'); ?>
							<?php echo $this->Form->input('syoukai', array('type' => 'text')); ?>
						</div>
					</div>
				</div>

				<div class="row">

					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->label('hurigana', 'フリガナ'); ?>
							<?php echo $this->Form->input('hurigana', array('type' => 'text', 'required' => false)); ?>
						</div>
					</div>

					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->label('name', '名前'); ?>
							<?php echo $this->Form->input('name', array('type' => 'text', 'required' => false)); ?>
						</div>
					</div>

					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->label('age', '年齢'); ?>
							<div class="input-group">
								<?php echo $this->Form->input('age', array('type' => 'text')); ?>
								<div class="input-group-addon">歳</div>
							</div>
						</div>
					</div>

					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->label('keitai_tel', '携帯TEL'); ?>
							<?php echo $this->Form->input('keitai_tel', array('type' => 'text')); ?>
						</div>
					</div>

					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->label('jitaku_tel', '自宅TEL'); ?>
							<?php echo $this->Form->input('jitaku_tel', array('type' => 'text')); ?>
						</div>
					</div>

					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->label('email', 'メールアドレス'); ?>
							<?php echo $this->Form->input('email', array('type' => 'text', 'required' => false)); ?>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->label('zip', '郵便番号'); ?>
							<?php echo $this->Form->input('zip', array('type' => 'text', 'onKeyup' => 'AjaxZip3.zip2addr(this, "", "data[Customer][todoufuken]", "data[Customer][shikutyouson]")')); ?>
							<span class="help-block">例）0000000</span>
						</div>
					</div>

					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->label('todoufuken', '都道府県'); ?>
							<?php echo $this->Form->input('todoufuken', array('type' => 'text')); ?>
						</div>
					</div>

					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->label('shikutyouson', '市区町村'); ?>
							<?php echo $this->Form->input('shikutyouson', array('type' => 'text')); ?>
						</div>
					</div>

					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->label('banchi', '番地'); ?>
							<?php echo $this->Form->input('banchi', array('type' => 'text')); ?>
						</div>
					</div>

				</div>

				<div class="row">
					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->label('pref_cd', '都道府県選択'); ?>
							<?php echo $this->Form->input('pref_cd', array('type' => 'select', 'options' => $prefs, 'empty' => '選択してください')); ?>
						</div>
					</div>

					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->label('line_cd', '最寄り路線'); ?>
							<?php echo $this->Form->input('line_cd', array('type' => 'select', 'options' => $lines, 'empty' => '選択してください')); ?>
						</div>
					</div>

					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->label('station_cd', '最寄り駅'); ?>
							<?php echo $this->Form->input('station_cd', array('type' => 'select', 'options' => $stations, 'empty' => '選択してください')); ?>
						</div>
					</div>

					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->label('distance', '距離'); ?>
							<?php echo $this->Form->input('distance', array('type' => 'text')); ?>
						</div>
					</div>

					<div class="col-xs-4">
						<div class="form-group">
							<?php echo $this->Form->label('memo', 'メモ'); ?>
							<?php echo $this->Form->input('memo', array('type' => 'text')); ?>
						</div>
					</div>
				</div>

				<h4 class="page-header">資格情報</h4>

				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<?php echo $this->Form->label('shikaku', '資格'); ?>
							<div class="input-group">
								<?php echo $this->Form->input('shikaku', array('type' => 'select', 'options' => $shikaku, 'multiple' => 'checkbox', 'class' => 'checkbox-inline')); ?>
							</div>
						</div>
					</div>
				</div>

				<h4 class="page-header">職務経歴</h4>

				<div class="row">
					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->label('corporation', '勤務先'); ?>
							<?php echo $this->Form->input('corporation', array('type' => 'text')); ?>
						</div>
					</div>

					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->label('shisetsukeitai_first', 'エントリー'); ?>
							<div class="input-group">
								<?php echo $this->Form->input('shisetsukeitai_first', array('type' => 'select', 'options' => $shisetsukeitaiFirsts, 'multiple' => true)); ?>
							</div>
						</div>
					</div>
					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->label('shisetsukeitai_second', '配属先'); ?>
							<div class="input-group">
								<?php echo $this->Form->input('shisetsukeitai_second', array('type' => 'select', 'options' => $shisetsukeitaiSeconds, 'multiple' => true)); ?>
							</div>
						</div>
					</div>

					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->label('shinryoukamoku', '科目'); ?>
							<div class="input-group">
								<?php echo $this->Form->input('shinryoukamoku', array('type' => 'select', 'options' => $shinryoukamokus, 'multiple' => true)); ?>
							</div>
						</div>
					</div>

					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->label('kinmu_keitai', '雇用形態'); ?>
							<div class="input-group">
								<?php echo $this->Form->input('kinmu_keitai', array('type' => 'select', 'options' => Configure::read('kinmu_keitai'), 'multiple' => true)); ?>
							</div>
						</div>
					</div>
				</div>

				<h4 class="page-header">希望条件</h4>

				<div class="row">
					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->label('kibouwork', '希望の働き方'); ?>
								<?php //echo $this->Form->input('kibouwork', array('type' => 'select', 'options' => Configure::read('kibouwork'), 'multiple' => true)); ?>
								<?php echo $this->Form->input('kibouwork', array('type' => 'text')); ?>
						</div>
					</div>

					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->label('kibou_busyo', '希望部署'); ?>
							<div class="input-group">
								<?php echo $this->Form->input('kibou_busyo', array('type' => 'select', 'options' => $shisetsukeitaiSeconds, 'multiple' => true)); ?>
							</div>
						</div>
					</div>

					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->label('kinmukaishijiki', '勤務開始時期'); ?>
							<div class="input-group">
								<?php echo $this->Form->input('kinmukaishijiki', array('type' => 'select', 'options' => $kinmukaishijiki, 'multiple' => true)); ?>
							</div>
						</div>
					</div>

					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->label('tsukinhouhou', '通勤方法'); ?>
							<div class="input-group">
								<?php echo $this->Form->input('tsukinhouhou', array('type' => 'select', 'options' => $tsukinhouhou, 'multiple' => true)); ?>
							</div>
						</div>
					</div>

					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->label('tsukinjikan', '通勤時間'); ?>
							<div class="input-group">
								<?php echo $this->Form->input('tsukinjikan', array('type' => 'select', 'options' => $tsukinjikan, 'multiple' => true)); ?>
							</div>
						</div>
					</div>

					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->label('kiboukyuyo', '希望給与'); ?>
							<div class="input-group">
								<?php echo $this->Form->input('kiboukyuyo', array('type' => 'select', 'options' => $kiboukyuyo, 'multiple' => true)); ?>
							</div>
						</div>
					</div>

				</div>

				<div class="row">
					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->label('kyujitsu_id', '希望休日'); ?>
							<div class="input-group">
								<?php echo $this->Form->input('kyujitsu_id', array('multiple' => true)); ?>
							</div>
						</div>
					</div>

					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->label('ryou', '寮'); ?>
							<?php echo $this->Form->input('ryou', array('type' => 'select', 'options' => Configure::read('nashiari'), 'empty' => '選択してください')); ?>
						</div>
					</div>

					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->label('takujisyo', '託児所'); ?>
							<?php echo $this->Form->input('takujisyo', array('type' => 'select', 'options' => Configure::read('nashiari'), 'empty' => '選択してください')); ?>
						</div>
					</div>

				</div>



				<!-- 
					20190514 sohnishi
					連絡履歴を追加
				 -->
				<h4 class="page-header">連絡履歴</h4>

				<div class="row">

					<div class="col-xs-4">
						<div class="form-group">
							<?php echo $this->Form->input('Customer.syousai', array('type' => 'text', 'label' => '連絡詳細')); ?>
						</div>
					</div>

				</div>

				<hr>

				

				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> 顧客検索</button>
							<a href="<?php echo $this->Html->url(array('action' => 'index')); ?>" class="btn btn-warning"><i class="fa fa-refresh"></i> リセット</a>
						</div>
					</div>
				</div>

				<?php echo $this->Form->end(); ?>

			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">

		<div class="panel panel-default">
			<div class="panel-heading">顧客情報一覧</div>
			<div class="panel-body">

				<table class="table table-striped">
					<thead>
						<tr>
						    <th>表示</th>
							<th>顧客番号</th>
							<th>顧客名</th>
							<th>市区町村</th>
							<th>資格</th>
							<th>電話番号</th>
							<th>担当者</th>
							<th>ランク</th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($customers as $i => $customer) : ?>
						<?php
							$paging = $this->request->params['paging'][$model];
							$start = ($paging['page'] - 1) * $paging['limit'] + 1;
							$pageNum = $start + $i;
							$sort = isset($paging['options']['sort']) ? $paging['options']['sort'] : '';
							$viewLink = Hash::merge(
								array('action' => 'view'),
								$this->request->params['named'],
								array('page' => $pageNum),
								array('?' => $this->request->query)
							);
						?>
						<tr>
							<td><?php echo $this->Html->link('表示', $viewLink); ?></td>
							<td><?php echo $customer[$model]['number']; ?></td>
							<td><?php echo $customer[$model]['name']; ?></td>
							<td><?php echo $customer[$model]['shikutyouson']; ?></td>
							<!-- 
								20190617 sohnishi
								複数資格が存在する場合は改行する
							-->
							<td>
								<?php 
									foreach ($customer['CustomerCertificate'] as $i => $certificate) {
										echo $shikaku[$certificate['shikaku']] . '<br>';
									}
								?>
							</td>
							<td><?php echo $customer[$model]['keitai_tel']; ?></td>
							<td><?php echo $users[$customer[$model]['user_id']]; ?></td>
							<td><?php echo $rank[$customer[$model]['rank']]; ?></td>
							<td><?php echo $this->Form->postLink('削除', array('plugin' => 'customers', 'controller' => 'customers', 'action' => 'delete', $customer[$model]['id']), array(), '本当に削除してもよろしいですか？'); ?></td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
				<div class="text-center">
					<?php echo $this->Paginator->pagination(array('ul' => 'pagination')); ?>
				</div>

			</div>
		</div>

	</div>
</div>
