<div id="thanks">
	<div class="section thanks">
		<h1><?php echo $this->Html->image('form_img/thanks-h1.png', array('class' => 'center-block img-responsive', 'alt' => '無料転職登録フォーム')); ?></h1>
		<p>この度は、ジョブアルク転職サポートにご登録いただきまして、誠にありがとうございます。</p>
		<p>今後の流れについての説明やご登録の内容についての確認を24時間以内に、お電話にてご連絡をさせていただきます。</p>
		<p>しばらくお待ちくださいますようお願いいたします。</p>
	</div>
</div>

<?php $this->start('page-bottom'); ?>
<div class="section">
	<?php echo $this->Html->image('howto.jpg', array('alt' => '', 'class' => 'img-responsive')); ?>
</div>

<?php $this->end(); ?>

<?php
$this->start('sidebox-pc');
echo $this->element('support');
echo $this->element('search_link');
echo $this->element('guide');
echo $this->element('blog');
echo $this->element('search_form');
echo $this->element('categories');
echo $this->element('sidebar_link');
$this->end();
