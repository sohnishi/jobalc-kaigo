<?php
	$sex = Configure::read('sex');
	$rank = Configure::read('rank');
	$touroku_keiro = Configure::read('touroku_keiro');
	$marital_status = Configure::read('marital_status');
	/*
		20190610 sohnishi
		求人と顧客の資格を分ける
	*/
	$shikaku = Configure::read('customer_shikaku');
	$kibouwork = Configure::read('kibouwork');
	$kinmu_keitai = Configure::read('kinmu_keitai');
	$kinmukaishijiki = Configure::read('kinmukaishijiki');
	$tsukinhouhou = Configure::read('tsukinhouhou');
	$tsukinjikan = Configure::read('tsukinjikan');
	$kiboukyuyo = Configure::read('kiboukyuyo');
	$nashiari = Configure::read('nashiari');
	$zaiseki = Configure::read('zaiseki');
	$renraku_houhou = Configure::read('renraku_houhou');

	$customer = $customers[0];
	$customerId = $customer[$model]['id'];

	$datas = array(
		'lineEmptyText' => '最寄り路線を選択',
		'stationEmptyText' => '最寄り駅を選択',
		'prefId' => '#CustomerRosenekiPrefCd',
		'lineId' => '#CustomerRosenekiLineCd',
		'stationId' => '#CustomerRosenekiStationCd'
	);
	echo $this->element('interface', $datas, array('plugin' => 'Ekidata'));

	// 路線・駅
	$rosenekiDatas = array(
		'customerId' => $customerId
	);
	echo $this->element('Customers.view_roseneki_script', $rosenekiDatas);

	// 資格情報
	$certificateDatas = array(
		'customerId' => $customerId,
		'shikaku' => $shikaku
	);
	echo $this->element('Customers.view_certificate_script', $certificateDatas);

	// 希望条件
	$conditionDatas = array(
		'customerId' => $customerId,
		'kibouwork' => $kibouwork,
		'kinmukaishijiki' => $kinmukaishijiki,
		'tsukinhouhou' => $tsukinhouhou,
		'tsukinjikan' => $tsukinjikan,
		'kiboukyuyo' => $kiboukyuyo,
		'nashiari' => $nashiari
	);
	echo $this->element('Customers.view_condition_script', $conditionDatas);

	// 職務経歴
	$careerDatas = array(
		'customerId' => $customerId,
		'zaiseki' => $zaiseki,
		'shisetsukeitaiFirsts' => $shisetsukeitaiFirsts,
		'shisetsukeitaiSeconds' => $shisetsukeitaiSeconds,
		'shinryoukamokus' => $shinryoukamokus,
		'kinmu_keitai' => $kinmu_keitai,
	);
	echo $this->element('Customers.view_career_script', $careerDatas);

	// 連絡履歴
	$contactDatas = array(
		'customerId' => $customerId,
		'users' => $users,
		'renraku_houhou' => $renraku_houhou
	);
	echo $this->element('Customers.view_contact_script', $contactDatas);
?>

<?php echo $this->Html->scriptStart(); ?>
$(function () {

	$('.collapse').on('hidden.bs.collapse', function () {
		$(this).parent().find(".fa-minus").removeClass("fa-minus").addClass("fa-plus");
	}).on('show.bs.collapse', function () {
		$(this).parent().find(".fa-plus").removeClass("fa-plus").addClass("fa-minus");
	});

});
<?php echo $this->Html->scriptEnd(); ?>

<div class="row">
	<div class="col-xs-12 contents-actions">
		<ul class="nav nav-tabs">
			<li><?php echo $this->Html->link('顧客検索', array('action' => 'index', '?' => $this->Session->read('customerquerystring'))); ?></li>
			<li><?php echo $this->Html->link('顧客を更新', Hash::merge(array('action' => 'edit', $customer[$model]['id']), $this->request->params['named'], array('?' => $this->Session->read('customerquerystring')))); ?></li>
			<li class="active"><a href="javascript:void();">顧客を表示</a></li>
		</ul>
	</div>
</div>

<div class="row">
	<div class="col-xs-7 text-right">
		<?php echo $this->Html->link('顧客情報出力', array('plugin' => 'customers', 'controller' => 'customers', 'action' => 'phpexcel', $customer[$model]['id'], 'admin' => true), array('class' => 'btn btn-primary', 'target' => '_blank')); ?>
	</div>
	<div class="col-xs-5">
		<ul class="pager">
			<?php echo $this->Paginator->prev('< 前へ', array('class' => 'previous', 'disabled' => 'previous disabled')); ?>
			<li><?php echo $this->Paginator->counter('{:page} / {:pages}'); ?></li>
			<?php echo $this->Paginator->next('次へ >', array('class' => 'next', 'disabled' => 'next disabled')); ?>
		</ul>
	</div>
</div>

<div class="row">
	<div class="col-xs-7">

		<div class="panel panel-default">
			<div class="panel-heading">基本情報</div>
			<table class="table table-bordered table-fixed">
				<tbody>
					<tr>
						<th>顧客番号</th>
						<td><?php echo $customer[$model]['number']; ?></td>
						<th>ランク</th>
						<td><?php echo $rank[$customer[$model]['rank']]; ?></td>
						<th>顧客担当</th>
						<td><?php echo $users[$customer[$model]['user_id']]; ?></td>
					</tr>
					<tr>
						<th>フリガナ</th>
						<td colspan="3"><?php echo $customer[$model]['hurigana']; ?></td>
						<th>生年月日</th>
						<td><?php echo date('Y年n月j日', strtotime($customer[$model]['birth'])); ?></td>
					</tr>
					<tr>
						<th rowspan="2">名前</th>
						<td colspan="3" rowspan="2" style="font-size:20px;vertical-align: middle;"><?php echo $customer[$model]['name']; ?></td>
						<th>年齢</th>
						<td><?php echo $this->Custom->birthToAge($customer[$model]['birth']); ?>歳</td>
					</tr>
					<tr>
						<th>性別</th>
						<td><?php echo $sex[$customer[$model]['sex']]; ?></td>
					</tr>
					<tr>
						<th>登録経路</th>
						<td colspan="3"><?php echo $touroku_keiro[$customer[$model]['touroku_keiro']]; ?></td>
						<th>既婚・未婚</th>
						<td><?php echo $marital_status[$customer[$model]['marital_status']]; ?></td>
					</tr>
					<tr>
						<th>紹介者</th>
						<td colspan="5"><?php echo $customer[$model]['syoukai']; ?></td>
					</tr>
					<tr>
						<th>郵便番号</th>
						<td colspan="4"><?php echo preg_replace("/^(\d{3})(\d{4})$/", "$1-$2", $customer[$model]['zip']); ?></td>
						<td><a href="http://www.google.co.jp/maps/place/<?php echo $customer[$model]['todoufuken'] . $customer[$model]['shikutyouson'] . $customer[$model]['banchi']; ?>" class="btn btn-primary btn-xs" target="_blank">地図を表示</a></td>
					</tr>
					<tr>
						<th rowspan="2">住所</th>
						<td><?php echo $customer[$model]['todoufuken']; ?></td>
						<td colspan="2"><?php echo $customer[$model]['shikutyouson']; ?></td>
						<td colspan="2"><?php echo $customer[$model]['banchi']; ?></td>
					</tr>
					<tr>
						<td colspan="5"><?php echo $customer[$model]['tatemono']; ?></td>
					</tr>
					<tr>
						<th>自宅TEL</th>
						<td colspan="5"><?php echo $customer[$model]['jitaku_tel']; ?></td>
					</tr>
					<tr>
						<th>携帯TEL</th>
						<td colspan="5"><?php echo $customer[$model]['keitai_tel']; ?></td>
					</tr>
					<tr>
						<th>メールアドレス</th>
						<td colspan="5"><?php echo $customer[$model]['email']; ?></td>
					</tr>

				</tbody>
			</table>
		</div>

		<?php echo $this->element('Customers.view_roseneki_add_form', $rosenekiDatas); ?>
		<div class="panel">
			<div id="roseneki-latest"></div>
		</div>

		<?php echo $this->element('Customers.view_certificate_add_form', $certificateDatas); ?>
		<div class="panel">
			<div id="shikaku-latest"></div>
		</div>

		<?php echo $this->element('Customers.view_condition_add_form', $conditionDatas); ?>
		<div class="panel">
			<div id="condition-latest"></div>
		</div>

		<?php echo $this->element('Customers.view_career_add_form', $careerDatas); ?>
		<div class="panel">
			<div id="career-latest"></div>
		</div>

	</div>

	<div class="col-xs-5">

		<?php echo $this->element('Customers.view_contact_add_form', $contactDatas); ?>
		<div class="panel">
			<div id="contact-latest"></div>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading">メモ</div>
			<div class="panel-body">
				<?php echo nl2br($customer[$model]['memo']); ?>
			</div>
		</div>

		<div class="panel panel-default">
			<table class="table table-bordered table-condensed">
				<tbody>
					<tr>
						<th>登録日</th>
						<td><?php echo date('Y年n月j日', strtotime($customer[$model]['created'])); ?></td>
					</tr>
					<?php if (!empty($customer[$model]['recruit_id'])) : ?>
					<tr>
						<th>応募求人詳細URL</th>
						<td><a href="<?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'detail', $customer[$model]['recruit_id'], 'admin' => false)); ?>" target="_blank"><?php echo $this->Html->url(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'detail', $customer[$model]['recruit_id'], 'admin' => false)); ?></a></td>
					</tr>
					<?php endif; ?>
				</tbody>
			</table>
		</div>

	</div>
</div>
