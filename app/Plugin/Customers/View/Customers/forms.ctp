<?php
	echo $this->Html->script('ajaxzip3', array('inline' => false));
	echo $this->Html->script('jquery.autoKana', array('inline' => false));
	echo $this->Html->css('validationEngine.jquery', array('inline' => false));
	echo $this->Html->script(array(
		'jquery.validationEngine-ja',
		'jquery.validationEngine'
	), array('inline' => false));
?>
<?php echo $this->Html->scriptStart(); ?>
$(document).ready(function () {
	AjaxZip3.JSONDATA = '<?php echo $this->Html->url('/js/zipdata'); ?>';
	$.fn.autoKana('#CustomerName', '#CustomerHurigana', {katakana: true});
});

$(document).ready(function () {
	$('#CustomerFormsForm').validationEngine({
		promptPosition: "inline",
		scroll: false,
		addSuccessCssClassToField: 'inputbox-success',
		addFailureCssClassToField: 'inputbox-error'
	});

	function checkMark(id, errorFound) {
		if (errorFound) {
			$(id).html('<?php echo $this->Html->image('ng_mark.png'); ?>');
		} else {
			$(id).html('<?php echo $this->Html->image('ok_mark.png'); ?>');
		}
	}

	$('#CustomerFormsForm').bind('jqv.field.result', function (event, field, errorFound, prompText) {
		if (field.attr('name') == 'data[Customer][name]') { checkMark('#name_check', errorFound); }
		if (field.attr('name') == 'data[Customer][hurigana]') { checkMark('#hurigana_check', errorFound); }
		if (field.attr('name') == 'data[Customer][birth][year][year]') { checkMark('#birth_check', errorFound); }
		if (field.attr('name') == 'data[Customer][birth][month][month]') { checkMark('#birth_check', errorFound); }
		if (field.attr('name') == 'data[Customer][birth][day][day]') { checkMark('#birth_check', errorFound); }
		if (field.attr('name') == 'data[Customer][zip]') { checkMark('#zip_check', errorFound); }
		if (field.attr('name') == 'data[Customer][todoufuken]') { checkMark('#todoufuken_check', errorFound); }
		if (field.attr('name') == 'data[Customer][shikutyouson]') { checkMark('#shikutyouson_check', errorFound); }
		if (field.attr('name') == 'data[Customer][keitai_tel]') { checkMark('#keitai_tel_check', errorFound); }
		if (field.attr('name') == 'data[Customer][email]') { checkMark('#email_check', errorFound); }
		if (field.attr('name') == 'data[Customer][shikaku][]') {
			checkMark('#shikaku_check', errorFound);
		}
		if (field.attr('name') == 'data[Customer][kibouwork][]') {
			checkMark('#kibouwork_check', errorFound);
		}
		if (field.attr('id') == 'CustomerKinmukaishijiki') { checkMark('#kinmukaishijiki_check', errorFound); }
	});

	$('#CustomerName').on('change', function () {
		if ($('#CustomerHurigana').val().length) {
			$('#CustomerHurigana').validationEngine('validate');
		} else {
			$('#CustomerHurigana').validationEngine('validate');
		}
	});

	$('#CustomerZip').on('change', function () {
		if ($('#CustomerTodoufuken').val()) {
			$('#CustomerTodoufuken').validationEngine('validate');
		}
	});

	$('#CustomerFormsForm').bind('jqv.field.result', function (event, field, errorFound, prompText) {
		if ($('#CustomerName').val().length
			&& $('#CustomerHurigana').val().length
			&& $('#CustomerBirthYearYear').val().length
			&& $('#CustomerBirthMonthMonth').val().length
			&& $('#CustomerBirthDayDay').val().length
			&& $('#CustomerZip').val().length
			&& $('#CustomerTodoufuken').val().length
			&& $('#CustomerShikutyouson').val().length
			&& $('#CustomerKeitaiTel').val().length
			&& ($('#CustomerShikaku0').prop('checked')
				|| $('#CustomerShikaku1').prop('checked')
				|| $('#CustomerShikaku2').prop('checked')
				|| $('#CustomerShikaku3').prop('checked')
				|| $('#CustomerShikaku4').prop('checked')
				|| $('#CustomerShikaku5').prop('checked')
				|| $('#CustomerShikaku6').prop('checked')
				|| $('#CustomerShikaku7').prop('checked')
				|| $('#CustomerShikaku8').prop('checked')
				|| $('#CustomerShikaku9').prop('checked')
				|| $('#CustomerShikaku10').prop('checked')
				|| $('#CustomerShikaku11').prop('checked')
				|| $('#CustomerShikaku12').prop('checked')
				|| $('#CustomerShikaku13').prop('checked')
				|| $('#CustomerShikaku14').prop('checked')
				|| $('#CustomerShikaku15').prop('checked')
				|| $('#CustomerShikaku16').prop('checked')
				|| $('#CustomerShikaku17').prop('checked')
				|| $('#CustomerShikaku18').prop('checked')
				|| $('#CustomerShikaku19').prop('checked')
				|| $('#CustomerShikaku20').prop('checked')
			)
			&& ($('#CustomerKibouwork0').prop('checked')
				|| $('#CustomerKibouwork1').prop('checked')
				|| $('#CustomerKibouwork2').prop('checked')
				|| $('#CustomerKibouwork3').prop('checked')
				|| $('#CustomerKibouwork4').prop('checked')
				|| $('#CustomerKibouwork5').prop('checked')
				|| $('#CustomerKibouwork6').prop('checked')
				|| $('#CustomerKibouwork7').prop('checked')
				|| $('#CustomerKibouwork8').prop('checked')
			)
			&& $('#CustomerKinmukaishijiki').val().length
		) {
			$('#form-submit').prop('disabled', false);
			$('#form-submit').removeClass('btn-default').addClass('btn-primary');
			$('.button-desc').hide();
			$(window).off('beforeunload');
		} else {
			$('#form-submit').prop('disabled', true);
			$('#form-submit').removeClass('btn-primary').addClass('btn-default');
			$('.button-desc').show();
			$(window).on('beforeunload', function () {
				return '無料転職登録フォームが完了していません。';
			});
		}

	});

});
<?php echo $this->Html->scriptEnd(); ?>
<div id="contact-form">
	<div class="section contact-form">
		<h1><?php echo $this->Html->image('form_img/otf-h1.png', array('class' => 'center-block img-responsive', 'alt' => '無料転職登録フォーム')); ?></h1>
		<?php
			echo $this->Form->create('Customer', array(
				'inputDefaults' => array(
					'label' => false,
					'wrapInput' => false,
					'div' => false,
					'class' => 'form-control'
				),
				//'class' => 'validating'
			));
		?>
		<div class="row">
			<div class="col-sm-3">
				<?php echo $this->Form->label('name', '<span class="label label-danger">必須</span> お名前', array('class' => 'control-label')); ?>
				<div id="name_check" class="check-mark"></div>
			</div>
			<div class="col-sm-6">
				<div class="form-group">
					<?php echo $this->Form->input('name', array('placeholder' => '例：山田 花子', 'class' => 'form-control validate[required]')); ?>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-3">
				<?php echo $this->Form->label('hurigana', '<span class="label label-danger">必須</span> フリガナ', array('class' => 'control-label')); ?>
				<div id="hurigana_check" class="check-mark"></div>
			</div>
			<div class="col-sm-6">
				<div class="form-group">
					<?php echo $this->Form->input('hurigana', array('placeholder' => '例：ヤマダ ハナコ', 'class' => 'form-control validate[required,custom[onlyKatakana]]')); ?>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-3">
				<?php echo $this->Form->label('birth', '<span class="label label-danger">必須</span> 生年月日', array('class' => 'control-label')); ?>
				<div id="birth_check" class="check-mark"></div>
			</div>
			<div class="col-sm-6">
				<div class="form-group birthday">
					<div class="year">
						<div class="input-group">
							<?php echo $this->Form->year('Customer.birth.year', date('Y') - 70, date('Y') -16, array('empty' => '---', 'class' => 'form-control validate[required,custom[birthday]]', 'data-prompt-target' => 'birth_error')); ?>
							<div class="input-group-addon">年</div>
						</div>
					</div>
					<div class="month">
						<div class="input-group">
							<?php echo $this->Form->month('Customer.birth.month', array('empty' => '---', 'monthNames' => false, 'class' => 'form-control validate[required,custom[birthday]]', 'data-prompt-target' => 'birth_error')); ?>
							<div class="input-group-addon">月</div>
						</div>
					</div>
					<div class="day">
						<div class="input-group">
							<?php echo $this->Form->day('Customer.birth.day', array('empty' => '---', 'class' => 'form-control validate[required,custom[birthday]]', 'data-prompt-target' => 'birth_error')); ?>
							<div class="input-group-addon">日</div>
						</div>
					</div>
					<div id="birth_error"></div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-3">
				<?php echo $this->Form->label('zip', '<span class="label label-danger">必須</span> 郵便番号', array('class' => 'control-label')); ?>
				<div id="zip_check" class="check-mark"></div>
			</div>
			<div class="col-sm-3">
				<div class="form-group">
					<?php echo $this->Form->input('zip', array('placeholder' => '例：1234567', 'class' => 'form-control validate[required,custom[zip]]', 'onKeyUp' => "AjaxZip3.zip2addr(this, '', 'data[Customer][todoufuken]', 'data[Customer][shikutyouson]');")); ?>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-3">
				<?php echo $this->Form->label('todoufuken', '<span class="label label-danger">必須</span> 都道府県', array('class' => 'control-label')); ?>
				<div id="todoufuken_check" class="check-mark"></div>
			</div>
			<div class="col-sm-3">
				<div class="form-group">
					<?php echo $this->Form->input('todoufuken', array('type' => 'select', 'options' => Configure::read('todoufuken'), 'empty' => '---', 'class' => 'form-control validate[required]')); ?>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-3">
				<?php echo $this->Form->label('shikutyouson', '<span class="label label-danger">必須</span> 市区町村', array('class' => 'control-label')); ?>
				<div id="shikutyouson_check" class="check-mark"></div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<?php echo $this->Form->input('shikutyouson', array('class' => 'form-control validate[required]')); ?>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-3">
				<?php echo $this->Form->label('banchi', '番地・建物名', array('class' => 'control-label')); ?>
				<div id="banchi_check" class="check-mark"></div>
			</div>
			<div class="col-sm-6">
				<div class="form-group">
					<?php echo $this->Form->input('banchi', array('placeholder' => '例：大阪市北区梅田0-0-00マンション101', 'class' => 'form-control')); ?>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-3">
				<?php echo $this->Form->label('keitai_tel', '<span class="label label-danger">必須</span> お電話番号', array('class' => 'control-label')); ?>
				<div id="keitai_tel_check" class="check-mark"></div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<?php echo $this->Form->input('keitai_tel', array('class' => 'form-control validate[required,minSize[9],maxSize[13],custom[phone]]')); ?>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-3">
				<?php echo $this->Form->label('email', 'メールアドレス', array('class' => 'control-label')); ?>
				<div id="email_check" class="check-mark"></div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<?php echo $this->Form->input('email', array('placeholder' => '有効なメールアドレス', 'class' => 'form-control validate[custom[email]]', 'required' => false)); ?>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-3">
				<?php echo $this->Form->label('shikaku', '<span class="label label-danger">必須</span> 保有資格<br>※複数可／取得見込の資格も含む'); ?>
				<div id="shikaku_check" class="check-mark"></div>
			</div>
			<div class="col-sm-9">
				<div class="form-group">
					<?php
						$shikaku = Configure::read('shikaku');
						for ($i = 0; $i < count($shikaku); $i++) :
					?>
					<div class="checkbox-inline">
						<input id="CustomerShikaku<?php echo $i; ?>" class="validate[minCheckbox[1]]" type="checkbox" value="<?php echo $i; ?>" name="data[Customer][shikaku][]" data-prompt-target="shikaku_error">
						<label for="CustomerShikaku<?php echo $i; ?>"><?php echo $shikaku[$i]; ?></label>
					</div>
					<?php endfor; ?>
					<div id="shikaku_error"></div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-3">
				<?php echo $this->Form->label('kibouwork', '<span class="label label-danger">必須</span> 希望の働き方<br>※複数可'); ?>
				<div id="kibouwork_check" class="check-mark"></div>
			</div>
			<div class="col-sm-9">
				<div class="form-group">
					<?php
						$kibouwork = Configure::read('kibouwork');
						for ($i = 0; $i < count($kibouwork); $i++) :
					?>
					<div class="checkbox-inline">
						<input id="CustomerKibouwork<?php echo $i; ?>" class="validate[minCheckbox[1]]" type="checkbox" value="<?php echo $i; ?>" name="data[Customer][kibouwork][]" data-prompt-target="kibouwork_error">
						<label for="CustomerKibouwork<?php echo $i; ?>"><?php echo $kibouwork[$i]; ?></label>
					</div>
					<?php endfor; ?>
					<div id="kibouwork_error"></div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-3">
				<?php echo $this->Form->label('kinmukaishijiki', '<span class="label label-danger">必須</span> 希望入職時期'); ?>
				<div id="kinmukaishijiki_check" class="check-mark"></div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<?php echo $this->Form->input('kinmukaishijiki', array('type' => 'select', 'options' => Configure::read('kinmukaishijiki'), 'empty' => '---', 'class' => 'form-control validate[required]')); ?>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-3">
				<?php echo $this->Form->label('memo', 'その他相談内容など'); ?>
			</div>
			<div class="col-sm-9">
				<div class="form-group">
					<?php echo $this->Form->input('memo', array('rows' => 10, 'style' => 'resize:vertical;')); ?>
				</div>
			</div>
		</div>

		<div class="form-group text-center">
			<?php echo $this->Form->submit('無料登録に申込する', array('id' => 'form-submit', 'class' => 'btn btn-default btn-lg', 'disabled' => true)); ?>
			<p class="button-desc text-danger">※必須項目を全て入力すると登録ボタンが表示されます。</p>
		</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
