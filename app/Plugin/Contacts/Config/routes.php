<?php

/**
 * Contacts Plugin Routes
 */

Router::connect('/admin/contacts', array('plugin' => 'contacts', 'controller' => 'contacts', 'admin' => true));
Router::connect('/admin/contacts/:action/*', array('plugin' => 'contacts', 'controller' => 'contacts', 'admin' => true));

