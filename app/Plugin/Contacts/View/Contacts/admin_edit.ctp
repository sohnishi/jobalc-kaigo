<?php
	$renraku_houhou_options = Configure::read('renraku_houhou');

	$datas = array(
		'lineEmptyText' => '最寄り路線を選択',
		'stationEmptyText' => '最寄り駅を選択',
		'facilityId' => '#ContactFacilityId',
		'staffId' => '#ContactStaffId'
	);
	echo $this->element('interface', $datas, array('plugin' => 'contacts'));
?>
<div class="row">
	<div class="col-xs-12 contents-actions">
		<ul class="nav nav-tabs">
			<li><?php echo $this->Html->link('連絡履歴一覧', array('action' => 'index')); ?></li>
			<li class="active"><a href="javascript:void(0);">連絡履歴を更新</a></li>
		</ul>
	</div>
</div>

<div class="row">
	<div class="col-xs-10 col-xs-offset-1">

		<div class="panel panel-default">
			<div class="panel-heading">連絡履歴の更新</div>
			<?php
				echo $this->Form->create($model, array(
					'inputDefaults' => array(
						'label' => false,
						'wrapInput' => false,
						'div' => false,
						'class' => 'form-control'
					)
				));
				echo $this->Form->input('id');
			?>
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-5">

						<div class="row">
							<div class="form-group col-xs-8">
								<?php echo $this->Form->input('facility_id', array('label' => '対象施設', 'empty' => '-- 選択してください --')); ?>
							</div>
						</div>

						<div class="row">
							<div class="form-group col-xs-12">
								<?php echo $this->Form->label('date', '連絡日付'); ?>
								<div class="input-group">
									<?php echo $this->Form->year('date', 2000, date('Y'), array('class' => 'select-control')); ?>年
									<?php echo $this->Form->month('date', array('monthNames' => false, 'empty' => false, 'class' => 'select-control')); ?>月
									<?php echo $this->Form->day('date', array('class' => 'select-control')); ?>日
								</div>
							</div>
						</div>

						<div class="row">
							<div class="form-group col-xs-12">
								<?php echo $this->Form->label('time', '連絡時間'); ?>
								<div class="input-group">
									<?php echo $this->Form->hour('time', 24, array('class' => 'select-control')); ?>:
									<?php echo $this->Form->minute('time', array('class' => 'select-control')); ?>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="form-group col-xs-8">
								<?php echo $this->Form->input('user_id', array('label' => '対応者')); ?>
							</div>
						</div>

						<div class="row">
							<div class="form-group col-xs-8">
								<?php echo $this->Form->input('staff_id', array('label' => '担当者', 'empty' => '-- 選択してください --')); ?>
							</div>
						</div>

						<div class="row">
							<div class="form-group col-xs-8">
								<?php echo $this->Form->input('houhou', array('options' => $renraku_houhou_options, 'label' => '連絡方法')); ?>
							</div>
						</div>
					</div>

					<div class="col-xs-7">
						<div class="form-group">
							<?php echo $this->Form->input('syousai', array('label' => '連絡詳細')); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="panel-footer">
				<?php echo $this->Form->submit('連絡履歴を更新する', array('class' => 'btn btn-primary')); ?>
			</div>
			<?php echo $this->Form->end(); ?>
		</div>

	</div>
</div>
