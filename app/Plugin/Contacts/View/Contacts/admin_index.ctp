<?php

	$renraku_houhou_options = Configure::read('renraku_houhou');
?>
<div class="row">
	<div class="col-xs-12 contents-actions">
		<ul class="nav nav-tabs">
			<li class="active"><a href="javascript:void(0);">連絡履歴一覧</a></li>
			<li><?php echo $this->Html->link('連絡履歴を新規登録', array('action' => 'add')); ?></li>
		</ul>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">

		<div class="panel panel-default">
			<div class="panel-heading">連絡履歴一覧</div>
			<div class="panel-body">

				<table class="table table-striped">
					<thead>
						<tr>
							<th><?php echo $this->Paginator->sort('id', 'ID'); ?></th>
							<th><?php echo $this->Paginator->sort('facility_id', '対象施設'); ?></th>
							<th><?php echo $this->Paginator->sort('date', '連絡日付'); ?></th>
							<th><?php echo $this->Paginator->sort('date', '連絡時間'); ?></th>
							<th><?php echo $this->Paginator->sort('user_id', '対応者'); ?></th>
							<th><?php echo $this->Paginator->sort('staff_id', '担当者'); ?></th>
							<th><?php echo $this->Paginator->sort('houhou', '方法'); ?></th>
							<th>連絡詳細</th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($contacts as $contact) : ?>
						<tr>
							<td><?php echo $contact['Contact']['id']; ?></td>
							<td><?php echo $contact['Facility']['shisetsumei']; ?></td>
							<td><?php echo date('Y年m月d日', strtotime($contact['Contact']['date'])); ?></td>
							<td><?php echo date('H:i', strtotime($contact['Contact']['time'])); ?></td>
							<td><?php echo $contact['User']['name']; ?></td>
							<td><?php echo $contact['Staff']['name']; ?></td>
							<td><?php echo $renraku_houhou_options[$contact['Contact']['houhou']]; ?></td>
							<td><?php echo $this->Text->truncate($contact['Contact']['syousai'], 30, array('ellipsis' => '...')); ?></td>
							<td>
								<ul class="list-inline">
									<li><?php echo $this->Html->link('編集', array('action' => 'edit', $contact['Contact']['id'])); ?></li>
									<li><?php echo $this->Form->postLink('削除', array('action' => 'delete', $contact['Contact']['id']), '', '本当に削除しますか？元には戻せません'); ?></li>
								</ul>
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>

				<div class="text-center">
					<?php echo $this->Paginator->pagination(array('ul' => 'pagination')); ?>
				</div>

			</div>
		</div>

	</div>
</div>