<script>
	$(function () {
		var settings = {
			urls : {
				staffs : '<?php echo $this->Html->url(array('plugin' => 'contacts', 'controller' => 'contacts', 'action' => 'ajax', 'admin' => true)); ?>',
			},
			emptyText: '<?php echo $emptyText; ?>'
		}

		function initSelect(id) {
			$(id).empty().append($('<option />').html(settings.emptyText).val(''));
		}

		<?php if (isset($facilityId) && isset($staffId)) : ?>
		$('<?php echo $facilityId; ?>').on('change', function () {
			initSelect('<?php echo $staffId; ?>');

			var url = settings.urls.staffs + '/' + $(this).val();
			$.getJSON(url, function (json, status) {
				if (status == 'success') {
					$.each(json, function () {
						var _this = this;
						$('<?php echo $staffId; ?>').append(
							$('<option />').val(_this.Staff.id).html(_this.Staff.name)
						);
					});
				}
			});
		});
		<?php endif; ?>
	});
</script>