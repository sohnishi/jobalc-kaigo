<?php

App::uses('ContactsAppModel', 'Contacts.Model');

class ContactsCustomerContact extends ContactsAppModel {

    public $belongsTo = array(
		'Contact' => array(
			'className' => 'Contacts.Contact',
			'foreignKey' => 'contact_id',
        ),
        'CustomerContact' => array(
			'className' => 'Customers.CustomerContact',
			'foreignKey' => 'customer_contact_id',
		),
    );
    
}