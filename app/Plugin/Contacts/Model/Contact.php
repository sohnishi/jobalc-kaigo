<?php

App::uses('ContactsAppModel', 'Contacts.Model');

class Contact extends ContactsAppModel {

	public $name = 'Contact';

	public $validate = array(
		'syousai' => array(
			'required' => array(
				'rule' => array('notEmpty', 'body'),
				'message' => '詳細を入力してください'
			)
		)
	);

	public $belongsTo = array(
		'Facility' => array(
			'className' => 'Facilities.Facility',
			'foreignKey' => 'facility_id',
		),
		'User' => array(
			'className' => 'Users.User',
			'foreignKey' => 'user_id'
		),
		'Staff' => array(
			'className' => 'Staffs.Staff',
			'foreignKey' => 'staff_id'
		)
	);

	public $hasMany = array(
		'ContactsCustomerContact' => array(
			'className' => 'Contacts.ContactsCustomerContact',
			'foreignKey' => 'contact_id',
			'conditions' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'dependent' => true,
			'exclusive' => '',
			'finderQuery' => ''
		),
	);

}
