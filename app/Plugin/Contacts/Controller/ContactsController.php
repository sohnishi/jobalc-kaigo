<?php

App::uses('ContactsAppController', 'Contacts.Controller');

class ContactsController extends ContactsAppController {

	public $name = 'Contacts';

	public $uses = array('Contacts.Contact', 'Contacts.ContactsCustomerContact', 'Customers.CustomerContact');

	public $components = array('Contacts.Contacter');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('model', $this->modelClass);
		$this->set('title_for_layout', '連絡履歴管理');
		$this->Security->unlockedActions = array('admin_update', 'admin_read');
		$this->set('currentUser', $this->Auth->user());

	}

	public function admin_index() {
		$this->Paginator->settings['limit'] = 50;
		$this->Paginator->settings['order'] = array(
			$this->modelClass . '.date' => 'DESC'
		);
		$this->Paginator->settings['contain'] = array(
			'Facility',
			'User',
			'Staff'
		);
		$this->set('contacts', $this->Paginator->paginate());
	}

	public function admin_add() {
		if (!empty($this->request->data)) {
			$this->{$this->modelClass}->create();
			if ($this->{$this->modelClass}->save($this->request->data)) {
				$this->flashMsg('連絡履歴を登録しました');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->flashMsg('連絡履歴を登録中にエラーが発生しました', 'alert-warning');
			}
		}

		// ユーザー情報を取得
		$users = $this->{$this->modelClass}->User->find('list');
		$this->set('users', $users);
	}

	public function admin_edit($id = null) {
		if (!$id || !$this->{$this->modelClass}->exists($id)) {
			$this->flashMsg('無効な操作です', 'alert-danger');
			$this->redirect(array('action' => 'index'));
		}

		if (!empty($this->request->data)) {
			if ($this->{$this->modelClass}->save($this->request->data)) {
				$this->flashMsg('連絡履歴を更新しました');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->flashMsg('連絡履歴を更新中にエラーが発生しました', 'alert-warning');
			}
		} else {
			$this->request->data = $this->{$this->modelClass}->read(null, $id);
		}

		// ユーザー情報を取得
		$users = $this->{$this->modelClass}->User->find('list');
		$this->set('users', $users);
	}

	public function admin_delete($id = null) {
		if (!$id || !$this->{$this->modelClass}->exists($id)) {
			$this->flashMsg('無効な操作です', 'alert-danger');
			$this->redirect(array('action' => 'index'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->{$this->modelClass}->delete($id)) {
				$this->flashMsg('連絡履歴を削除しました');
			} else {
				$this->flashMsg('連絡履歴を削除中にエラーが発生しました', 'alert-warning');
			}
		}
		$this->redirect(array('action' => 'index'));
	}

	public function admin_read($id = null) {
		if (!$id) {
			$this->_jsonRender();
			exit;
		}

		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			Configure::read('debug', 0);
			$this->{$this->modelClass}->recursive = 2;
			$contact = $this->{$this->modelClass}->read(null, $id);
			// 日付分解
			list($yy, $mm, $dd) = explode('-', $contact['Contact']['date']);
			$contact['Contact']['year'] = $yy;
			$contact['Contact']['month'] = $mm;
			$contact['Contact']['day'] = $dd;

			// 時間分解
			list($hh, $ii) = explode(':', $contact['Contact']['time']);
			$contact['Contact']['hour'] = $hh;
			$contact['Contact']['min'] = $ii;

			$_data = $contact;
			$this->_jsonRender($_data);
		}
	}

	public function admin_update() {
		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			Configure::write('debug', 0);

			// if (empty($this->request->data['id'])) {
			// 	unset($this->request->data['id']);
			// 	$this->{$this->modelClass}->create();
			// }

			// $succeed = $this->{$this->modelClass}->save($this->request->data);
			// $message = $succeed ? '更新しました' : '更新に失敗しました';


			// 中間テーブルに保存するのかContactに保存するのかフラグ
			$isContactsCustomerContact = false;
			/* 
				Contact.IDがない場合は、
				顧客IDがあれば、中間テーブルに新規保存
				顧客IDがなければ、Contactに保存
			*/
			if (empty($this->request->data['id'])) {
				if (empty($this->request->data['CustomerContact']['customer_id'])) {
					unset($this->request->data['id']);
					$this->{$this->modelClass}->create();
					$isContactsCustomerContact = false;
				} else {
					$this->ContactsCustomerContact->create();
					$isContactsCustomerContact = true; 
				}
			} else {
				/* 
					Contact.IDがある場合は、
					顧客IDがあれば、中間テーブルに保存(Contactは更新、CustomerContactは新規保存)
				*/
				if (!empty($this->request->data['CustomerContact']['customer_id'])) {
					$this->ContactsCustomerContact->create();
					$isContactsCustomerContact = true; 
				}
			}
			
			
			$succeed = $isContactsCustomerContact ? $this->ContactsCustomerContact->saveAll($this->request->data) : $this->{$this->modelClass}->saveAll($this->request->data);
			$message = $succeed ? '更新しました' : '更新に失敗しました';


			if (!$succeed && $this->{$this->modelClass}->validationErrors) {
				$validationError = array_shift($this->{$this->modelClass}->validationErrors);
				$message = $validationError[0];
			}

			$_data = compact('succeed', 'message');
			$this->_jsonRender($_data);
		}
	}

	public function admin_ajaxDelete($id = null) {
		if (!$id) {
			$this->_jsonRender();
			exit;
		}

		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			Configure::write('debug', 0);
			// $succeed = $this->{$this->modelClass}->delete($id);
			// $message = $succeed ? '更新しました' : '更新に失敗しました';
			// $_data = compact('succeed', 'message');
			// $this->_jsonRender($_data);

			/*
				20190533 sohnishi
				Contactを削除する前に中間テーブルから該当のデータを取得する
			*/
			$conditions = array('contact_id'=>$id);
			$contactsCustomerContact = $this->ContactsCustomerContact->find('all', array(
				'conditions' => $conditions,
				'fields' => 'customer_contact_id'
			));
			$customer_contact_ids = array();
			foreach($contactsCustomerContact as $key => $value){
				array_push($customer_contact_ids, $value['ContactsCustomerContact']['customer_contact_id']);
			}

			// CustomerContactの削除
			$succeed = $this->{$this->modelClass}->delete($id);
			
			/*
				20190533 sohnishi
				該当のCustomerContactに関連するContactのデータも削除
			*/
			$succeed = $this->CustomerContact->deleteAll(
				array(
					'CustomerContact.id' => $customer_contact_ids
			), true);

			$message = $succeed ? '更新しました' : '更新に失敗しました';
			$_data = compact('succeed', 'message');
			$this->_jsonRender($_data);
		}
	}

	public function admin_getLatest() {
		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			Configure::write('debug', 0);

			$facilityId = $this->request->params['pass'];
			$query = array(
				'conditions' => array(
					$this->modelClass . '.facility_id' => $facilityId
				),
				'order' => array(
					$this->modelClass . '.date' => 'DESC',
					$this->modelClass . '.time' => 'DESC'
				),
				'contain' => array(
					'User',
					'Staff',
					'ContactsCustomerContact',
					'ContactsCustomerContact.CustomerContact'
				)
			);
			$this->{$this->modelClass}->recursive = 2;
			$tmpContacts = $this->{$this->modelClass}->find('all', $query);
			$contacts = array();
			foreach ($tmpContacts as $key => $contact) {
				$contact['Contact']['date'] = date('Y年n月j日', strtotime($contact['Contact']['date']));
				$contact['Contact']['time'] = date('H:i', strtotime($contact['Contact']['time']));
				$contacts[$key] = $contact;
			}

			$renraku = Configure::read('renraku_houhou');
			$_data = compact('contacts', 'renraku');
			$this->_jsonRender($_data);
		}
	}

	public function admin_ajax($facilityId = null) {
		if (empty($facilityId)) {
			$this->_jsonRender();
			exit;
		}

		$this->autoRender = false;
		$this->layout = 'ajax';
		if ($this->request->is('ajax')) {
			Configure::write('debug', 0);
			$_data = $this->{$this->modelClass}->Staff->find('all', array(
				'conditions' => array(
					'Staff.facility_id' => $facilityId,
				),
				'order' => array(
					'Staff.primary_check' => 'DESC',
					'Staff.busyo' => 'ASC'
				)
			));
			$this->_jsonRender($_data);
		}
	}

	private function _jsonRender($_data = array()) {
		header("Content-Type: application/json; charset=utf-8");
		echo json_encode($_data);
	}
}
