<?php

App::uses('Component', 'Controller');
App::uses('Facility', 'Facilities.Model');
App::uses('Staff', 'Staffs.Model');
App::uses('Hash', 'Utility');

class ContacterComponent extends Component {

	public $settings = array(
		'facility' => array(
			'id' => 'id',
			'list' => 'facilities'
		),
		'staff' => array(
			'id' => 'id',
			'list' => 'staffs'
		)
	);

	public function __construct(ComponentCollection $collection, $settings = array()) {
		$default = $this->settings;
		parent::__construct($collection, $settings);
		$this->settings = Hash::merge($default, $settings);
	}

	public function initialize(Controller $controller) {
		parent::initialize($controller);
	}

	public function startup(Controller $controller) {
		$this->Facility = new Facility;
		$this->Staff = new Staff;
	}

	public function beforeRender(Controller $controller) {
		$facilities = $this->Facility->find('list', array('fields' => array('id', 'shisetsumei')));

		$staffs = array();
		if (!empty($controller->request->data[$controller->modelClass][$this->settings['facility']['id']])) {
			$query = array(
				'conditions' => array(
					'Staff.facility_id' => $controller->request->data[$controller->modelClass][$this->settings['facility']['id']]
				),
				'order' => array(
					'Staff.primary_check' => 'DESC',
					'Staff.modified' => 'DESC'
				),
				'fields' => array(
					'id',
					'name'
				)
			);
			$staffs = $this->Staff->find('list', $query);
		}

		$controller->set(array(
			$this->settings['facility']['list'] => $facilities,
			$this->settings['staff']['list'] => $staffs
		));
	}
}