<?php

App::uses('FormsAppController', 'Forms.Controller');

/**
 * フォームページコントローラ
 *
 * Class FormController
 */
class FormsController extends FormsAppController
{
    public $name = 'Forms';

    public $uses = array('Customers.Customer', 'Recruits.Recruit');

    public function beforeFilter()
    {
        parent::beforeFilter();

        // $this->set('title_for_layout', Configure::read('form_default_title'));
        $this->Security->unlockedActions = array('forms');

        // レイアウト
        $this->layout = '';
    }

    /**
     * お申し込みフォーム表示処理
     * ・URLを基準にページを読み込むため適切なviewが必要
     * ・viewをコピーして別のページを作ることが可能
     * ・view名が一致すればroutingを指定することなく表示可能
     */
    public function display()
    {

        if ($this->request->is('get')) {

                $url = strstr($this->request->url, '/', true);
                $id = $this->request->params['id'];
                $headerImg = array();

            if ($url == 'private-form' || $url == 'entry-form' || $url == 'average-form') {

                // 詳細画面もしくは一覧の問い合わせるボタンからの場合
                if (isset($id)) {
                    // 求人を取得

                    $recruit = $this->Recruit->find('first', array(
                        'conditions' => 
                            array(
                                'Recruit.id' => $id,
                                'Recruit.active' => '1',
                            ),
                        'contain' => array(
                            'RecruitShow',
                            'ShisetsukeitaiSecond',
                            'Kodawari',
                            'Facility' => array(
                                'FacilityShow',
                                'ShisetsukeitaiSecond',
                                'Roseneki' => array(
                                    'RailroadPref',
                                    'RailroadLine',
                                    'RailroadStation'
                                )
                            )
                        )
                    ));

                    // JOBALC-TODO: 画像名をフォーム名+_top.svgとしてif文をなくす
                    if ($url == 'private-form') {
                        // 詳細画面からの場合は詳細画面から用のヘッダ
                        // JOBALC-TODO: 画像変更
                        $headerImg = array('name' => 'hikoukai_top.svg', 'alt' => '非公開求人 サイトでは公開できない求人は全体の68% 求人を閲覧できず、誠に申し訳ございません。お見せしたいのは山々ですが。詳しくはご登録後にご紹介致します。登録者限定で非公開求人をご紹介します。口コミや内部情報もご案内します。内定率UP対策のサービス。代行交渉・アフターフォローと体制は万全です。');
                    } else if ($url == 'entry-form') {
                        // 詳細画面からの場合は詳細画面から用のヘッダ
                        // JOBALC-TODO: 画像変更
                        $headerImg = array('name' => 'hikoukai_top.svg', 'alt' => '非公開求人 サイトでは公開できない求人は全体の68% 求人を閲覧できず、誠に申し訳ございません。お見せしたいのは山々ですが。詳しくはご登録後にご紹介致します。登録者限定で非公開求人をご紹介します。口コミや内部情報もご案内します。内定率UP対策のサービス。代行交渉・アフターフォローと体制は万全です。');
                    } else if ($url == 'average-form') {
                        // 詳細画面からの場合は詳細画面から用のヘッダ
                        // JOBALC-TODO: 画像変更
                        $headerImg = array('name' => 'hikoukai_top.svg', 'alt' => '非公開求人 サイトでは公開できない求人は全体の68% 求人を閲覧できず、誠に申し訳ございません。お見せしたいのは山々ですが。詳しくはご登録後にご紹介致します。登録者限定で非公開求人をご紹介します。口コミや内部情報もご案内します。内定率UP対策のサービス。代行交渉・アフターフォローと体制は万全です。');
                    }

                    $this->set('recruit', $recruit);
                } else {
                    // JOBALC-TODO: 画像名をフォーム名+_default_top.svgとしてif文をなくす
                    if ($url == 'private-form') {
                        // 詳細画面からでない場合はデフォルトヘッダ
                        // JOBALC-TODO: 画像変更
                        $headerImg = array('name' => 'hikoukai_top.svg', 'alt' => '非公開求人 サイトでは公開できない求人は全体の68% 求人を閲覧できず、誠に申し訳ございません。お見せしたいのは山々ですが。詳しくはご登録後にご紹介致します。登録者限定で非公開求人をご紹介します。口コミや内部情報もご案内します。内定率UP対策のサービス。代行交渉・アフターフォローと体制は万全です。');
                    } else if ($url == 'entry-form') {
                        // 詳細画面からでない場合はデフォルトヘッダ
                        // JOBALC-TODO: 画像変更
                        $headerImg = array('name' => 'hikoukai_top.svg', 'alt' => '非公開求人 サイトでは公開できない求人は全体の68% 求人を閲覧できず、誠に申し訳ございません。お見せしたいのは山々ですが。詳しくはご登録後にご紹介致します。登録者限定で非公開求人をご紹介します。口コミや内部情報もご案内します。内定率UP対策のサービス。代行交渉・アフターフォローと体制は万全です。');
                    } else if ($url == 'average-form') {
                        // 詳細画面からでない場合はデフォルトヘッダ
                        // JOBALC-TODO: 画像変更
                        $headerImg = array('name' => 'hikoukai_top.svg', 'alt' => '非公開求人 サイトでは公開できない求人は全体の68% 求人を閲覧できず、誠に申し訳ございません。お見せしたいのは山々ですが。詳しくはご登録後にご紹介致します。登録者限定で非公開求人をご紹介します。口コミや内部情報もご案内します。内定率UP対策のサービス。代行交渉・アフターフォローと体制は万全です。');
                    }
                }

                $this->set('headerImg', $headerImg);
                $this->render($url);

            } else if ($url == 'consultation-form') {
                // 相談フォーム
                // JOBALC-TODO: 画像変更
                $headerImg = array('name' => 'hikoukai_top.svg', 'alt' => '非公開求人 サイトでは公開できない求人は全体の68% 求人を閲覧できず、誠に申し訳ございません。お見せしたいのは山々ですが。詳しくはご登録後にご紹介致します。登録者限定で非公開求人をご紹介します。口コミや内部情報もご案内します。内定率UP対策のサービス。代行交渉・アフターフォローと体制は万全です。');
                $this->set('headerImg', $headerImg);
                $this->render($url);
            } else {
                // URLが不適切な場合はトップページへ戻す
                return $this->redirect('/');
            }

        }

    }

    /**
     * フォーム内容を受け取りデータベース保存後にメールを送信する
     * 求人IDと関連付けされた場合は管理画面に表示される
     *
     * @param null $id recruit id
     */
    public function forms()
    {
        $this->autoRender = false;

        if ($this->request->is('post') || $this->request->is('put')) {

            $data = $this->request->data;
            $referer = $this->referer(null, true);
            $uri = explode('/', $referer);

            $data['Customer']['reception'] = $reception;


            // 求人IDがあればデータに含める
            $id = $this->request->params['id'];
            if (!empty($id)) {
                $data['Customer']['recruit_id'] = $id;
            }

            // DB必須項目を埋める
            $data['Customer']['birth']['month']['month'] = '01';
            $data['Customer']['birth']['day']['day'] = '01';
            $this->log($data, 'debug');

            // 保存が成功したらメール送信 $uri[1] => フォームURL
            if ($this->Customer->formDataSaveByKaigo($data, $uri[1])) {
                $this->redirect(array('action' => 'thanks'));
            } else {
                // 保存に失敗したらリファラで戻す
                $this->redirect($this->referer());
            }
        }
    }

    /**
     * お申し込み完了ページ
     */
    public function thanks()
    {
        $this->set('title_for_layout', '無料転職登録のお申込みありがとうございます');
        // viewの表示のみ
    }

}