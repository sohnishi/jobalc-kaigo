<?php

// 資格
Configure::write('form_shikaku', 
    array(
        '4' => '介護福祉士', 
        '24' => '介護職',
        '6' => 'ケアマネ', // 介護支援専門員
        '20' => 'その他',
        )
    );

// 希望の働き方
Configure::write('form_kibouwork', 
    array(
        '0' => '正社員', 
        '1' => 'パート', 
        '2' => '日勤のみ', 
        '3' => '夜勤のみ', 
        '4' => 'その他'
    )
);
// 勤務開始時期
Configure::write('form_kinmukaishijiki', 
    array(
        '9' => '探し中', 
        '10' => '退職調整中', 
        '11' => '退職済', 
        '8' => 'その他'
    )
);