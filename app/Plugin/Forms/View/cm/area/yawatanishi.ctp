<?php
    // ページタイトル
    $page_title = '北九州市八幡西区にある保育の非公開求人をご紹介｜ジョブアルク';
    $this->assign('title', $page_title);
    // 使用レイアウト
    $this->layout = 'cm_hikoukai-header-footer';
?>
<div id="masthead" class="section">
    <div class="container">
        <?php echo $this->Html->image('/landing/img/yawatanishi-catch-cm.png', array('alt' => '八幡西区の保育求人をご紹介します。民間のハローワークです。ジョブアルクにしかない非公開求人をご紹介します！')); ?>
    </div>
</div>
<div id="form-wizard" class="section">
    <div class="container">
        <div class="form-box">
            <div class="form-inner">
                <?php
                    echo $this->Form->create('Customer', array(
                        'inputDefaults' => array('label' => false, 'div' => false),
                        'id' => 'landing-form',
                        'class' => 'form-horizontal',
                        'url' => '/landing/forms'
                    ));
                ?>
                <div id="landing-form-content">
                    <h3>保有資格</h3>
                    <section>
                        <h4 class="question">あなたの保有資格を選択してください<br><span>(※複数選択可)</span></h4>
                        <div class="answer">
                            <div id="shikaku_error"></div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="checkbox">
                                        <input type="checkbox" name="data[Customer][shikaku][]" value="21" id="CustomerShikaku21" data-validation-engine="validate[minCheckbox[1]]" data-prompt-target="shikaku_error">
                                        <label for="CustomerShikaku21">保育士</label>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="checkbox">
                                        <input type="checkbox" name="data[Customer][shikaku][]" value="22" id="CustomerShikaku22" data-validation-engine="validate[minCheckbox[1]]" data-prompt-target="shikaku_error">
                                        <label for="CustomerShikaku22">幼稚園教論</label>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="checkbox">
                                        <input type="checkbox" name="data[Customer][shikaku][]" value="20" id="CustomerShikaku20" data-validation-engine="validate[minCheckbox[1]]" data-prompt-target="shikaku_error">
                                        <label for="CustomerShikaku20">その他</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <h3>雇用形態</h3>
                    <section>
                        <h4 class="question">ご希望の雇用形態を選択してください</h4>
                        <div class="answer mb30">
                            <div id="kibouwork_error"></div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="checkbox">
                                        <input type="checkbox" name="data[Customer][kibouwork][]" value="27" id="CustomerKibouwork27" data-validation-engine="validate[minCheckbox[1]]" data-prompt-target="kibouwork_error">
                                        <label for="CustomerKibouwork27">正社員・<span class="break"></span>パート(フル)</label>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="checkbox">
                                        <input type="checkbox" name="data[Customer][kibouwork][]" value="28" id="CustomerKibouwork28" data-validation-engine="validate[minCheckbox[1]]" data-prompt-target="kibouwork_error">
                                        <label for="CustomerKibouwork28">パート<span class="break"></span>(週20時間)</label>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="checkbox">
                                        <input type="checkbox" name="data[Customer][kibouwork][]" value="29" id="CustomerKibouwork29" data-validation-engine="validate[minCheckbox[1]]" data-prompt-target="kibouwork_error">
                                        <label for="CustomerKibouwork29">その他</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h4 class="question">転職を希望する時期を選択してください</h4>
                        <div class="answer">
                            <div class="radio-style clearfix">
                                <div class="radio">
                                    <input type="radio" name="data[Customer][kinmukaishijiki]" value="6" checked="checked" id="CustomerKinmukaishijiki6">
                                    <label for="CustomerKinmukaishijiki6">いますぐ</label>
                                    <div class="check"></div>
                                </div>
                                <div class="radio">
                                    <input type="radio" name="data[Customer][kinmukaishijiki]" value="1" id="CustomerKinmukaishijiki1">
                                    <label for="CustomerKinmukaishijiki1">3ヶ月以内</label>
                                    <div class="check"></div>
                                </div>
                                <div class="radio">
                                    <input type="radio" name="data[Customer][kinmukaishijiki]" value="7" id="CustomerKinmukaishijiki7">
                                    <label for="CustomerKinmukaishijiki7">年度末</label>
                                    <div class="check"></div>
                                </div>
                                <div class="radio">
                                    <input type="radio" name="data[Customer][kinmukaishijiki]" value="8" id="CustomerKinmukaishijiki8">
                                    <label for="CustomerKinmukaishijiki8">その他</label>
                                    <div class="check"></div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <h3>住所</h3>
                    <section>
                        <h4 class="question">お住まい、生まれた年を入力してください</h4>
                        <div class="answer">
                            <div class="form-group zip">
                                <?php echo $this->Form->label('zip', '郵便番号', array('class' => 'control-label col-xs-3')); ?>
                                <div class="col-xs-9">
                                    <?php echo $this->Form->input('zip', array('type' => 'text', 'wrapInput' => false, 'class' => 'form-control', 'placeholder' => '例）000-0000')); ?>
                                    <div class="addon">※ハイフンなしOK</div>
                                </div>
                            </div>
                            <div id="birth_error"></div>
                            <div class="form-group birth">
                                <?php echo $this->Form->label('Customer.birth.year', '生まれ年', array('class' => 'control-label col-xs-3')); ?>
                                <div class="col-xs-9">
                                    <?php echo $this->Form->year('Customer.birth.year', '1958', date('Y') - 18, array('class' => 'form-control', 'empty' => '西暦', 'data-validation-engine' => 'validate[required]', 'data-prompt-target' => 'birth_error')); ?>
                                    <div class="addon">年生まれ</div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <h3>連絡先</h3>
                    <section>
                        <h4 class="question">お名前、ご連絡先を入力してください</h4>
                        <div class="answer">
                            <div class="form-group">
                                <?php echo $this->Form->label('name', 'お名前', array('class' => 'control-label col-xs-3')); ?>
                                <div class="col-xs-9">
                                    <?php echo $this->Form->input('name', array('type' => 'text', 'class' => 'form-control', 'placeholder' => '例）大阪花子', 'data-validation-engine' => 'validate[required]')); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo $this->Form->label('hurigana', 'フリガナ', array('class' => 'control-label col-xs-3')); ?>
                                <div class="col-xs-9">
                                    <?php echo $this->Form->input('hurigana', array('type' => 'text', 'class' => 'form-control', 'placeholder' => '例）オオサカハナコ', 'data-validation-engine' => 'validate[required,custom[onlyKatakana]]')); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo $this->Form->label('keitai_tel', '電話番号', array('class' => 'control-label col-xs-3')); ?>
                                <div class="col-xs-9">
                                    <?php echo $this->Form->input('keitai_tel', array('type' => 'text', 'class' => 'form-control', 'placeholder' => '例）000-0000-0000', 'data-validation-engine' => 'validate[required,minSize[9],maxSize[13],custom[phone]]')); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo $this->Form->label('email', 'メール', array('class' => 'control-label col-xs-3')); ?>
                                <div class="col-xs-9">
                                    <?php echo $this->Form->input('email', array('type' => 'email', 'class' => 'form-control', 'placeholder' => '例）jobalk@info.jp', 'data-validation-engine' => 'validate[required, custom[email]]')); ?>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="actions clearfix">
                    <div class="row">
                        <div class="col-xs-6">
                            <button type="button" id="stepsPrev" class="btn btn-block disabled" disabled="disabled"><span>もどる</span></button>
                        </div>
                        <div class="col-xs-6">
                            <button type="button" id="stepsNext" class="btn btn-block"><span>つぎへ</span></button>
                            <button type="button" id="stepsFinish" class="btn btn-block"><span>登録する</span></button>
                        </div>
                    </div>
                </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>

<div class="contact-banner section">
    <p class="tel-micro">※入力が苦手やめんどくさい方も電話でOK</p>
    <div class="container">
        <?php echo $this->Html->link($this->Html->image('/landing/img/hikoukai-tel.png'), 'tel:0120016898', array('escape' => false)); ?>
    </div>
    <p class="tel-micro">※無理に転職をお勧めすることはありません。</p>
</div>