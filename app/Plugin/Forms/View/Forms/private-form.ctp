<?php
    $shokushu_options = Configure::read('shokushu');
    $kinmu_keitai_options = Configure::read('kinmu_keitai');
    // 構造化データ用
    $employmentType = Configure::read('employmentType');
    $shikaku_options = Configure::read('shikaku');
    $todoufukens = Configure::read('todoufukens');

    $shikakus = Configure::read('form_shikaku');
    $kibouworks = Configure::read('form_kibouwork');
    $kinmukaishijiki = Configure::read('form_kinmukaishijiki');
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="format-detection" content="telephone=no">
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
<title>非公開求人のお問い合わせ【ジョブアルク介護】</title>
<link href="<?php echo $this->Html->url('/favicon.ico'); ?>" type="image/x-icon" rel="icon">
<link href="<?php echo $this->Html->url('/favicon.ico'); ?>" type="image/x-icon" rel="shortcut icon">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
<?php 
    echo $this->Html->css('base');
    echo $this->Html->css('modal');
    echo $this->Html->css('validationEngine.jquery'); 
    echo $this->Html->css('Forms.form');
?>
<?php echo $this->Html->script('jquery-1.11.2.min'); ?>
<?php echo $this->Html->script('jquery.cookie'); ?>
<?php echo $this->Html->script('bootstrap.min'); ?>
<?php echo $this->Html->script('addrajax/addrajax'); ?>
<?php echo $this->Html->script('jquery.steps.min'); ?>
<?php echo $this->Html->script('jquery.validationEngine-ja'); ?>
<?php echo $this->Html->script('jquery.validationEngine'); ?>
<?php echo $this->Html->script('jquery.autoKana'); ?>
<?php echo $this->Html->script('landing'); ?>
<!--[if lt IE 9]>
	<script type="text/javascript" src="theme/Jobalc/js/html5shiv.min.js"></script>
	<script type="text/javascript" src="theme/Jobalc/js/respond.min.js"></script>
	<script type="text/javascript">document.createElement('main');</script>
<![endif]-->



<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TMH9GK');</script>
<!-- End Google Tag Manager -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TMH9GK"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <div id="body_wrapper">
		<header>
			<div class="container">
                <div class="header">
                    <div class="logo">
						<?php 
							echo $this->Html->link(
							$this->Html->image('logo.svg', array('alt' => 'ジョブアルク介護ロゴ')),
							'/',
							array('escape' => false)); 
						?>
                    </div>
                    <div class="header_tel pc">
                        <span class="header_tel" title="お電話でのお仕事相談・登録ができます。0120-932-929：【受付】9:30～20:00（土・日・祝も可能）">
								<?php echo $this->Html->image('top_tel.svg', array('alt' => 'お電話でのお仕事相談・登録ができます。0120-932-929：【受付】9:30～20:00（土・日・祝も可能）')); ?>
                        </span>
                    </div>
                </div>
			</div>
		</header>
        <div id="main">
			<div class="container">
				<div class="row">
                    <div class="hikoukai_top sp">
                        <img src="/cw/theme/Jobalc/img/hikoukai_top.svg" alt="非公開求人 ※閲覧できず、誠に申し訳ございません。お見せしたいのは山々ですが詳しくはご登録後にご紹介致します。サイトでは公開できない求人全体の68% 登録者限定で非公開求人 内定率UP対策 口コミや内部情報 代行交渉・アフターフォロー">
                    </div>
                    <div class="hikoukai_top pc">
                        <img src="/cw/theme/Jobalc/img/hikoukai_top_pc.svg" alt="非公開求人 ※閲覧できず、誠に申し訳ございません。お見せしたいのは山々ですが詳しくはご登録後にご紹介致します。サイトでは公開できない求人全体の68% 登録者限定で非公開求人 内定率UP対策 口コミや内部情報 代行交渉・アフターフォロー">
                    </div>
                    <main id="contents">
                    <!-- 求人情報 -->
                    <?php if (!empty($recruit)) { ?>
                        <div class="form_rectuit_info_div">
                            <p class="form_recruit_info">
                                【<?php echo $recruit['Recruit']['id']; ?>】
                                <!-- <br> -->
                                <?php 
                                    if ($recruit['Facility']['FacilityShow']['groupmei_chk'] == 1) {
                                        echo empty($recruit['Facility']['group']) ? '' : $recruit['Facility']['group'] . '&nbsp;';
                                    }
                                    if ($recruit['Facility']['FacilityShow']['houjinmei_chk'] == 1) {
                                        echo empty($recruit['Facility']['corporation']) ? '' : $recruit['Facility']['corporation'] . '&nbsp;';
                                    }
                                    echo $recruit['Facility']['shisetsumei']; 
                                ?>
                                <br>
                                <?php echo $recruit['Facility']['todoufuken'] . $recruit['Facility']['shikutyouson'] . $recruit['Facility']['banchi'] . $recruit['Facility']['tatemono']; ?>

                                <?php if ($detail['RecruitShow']['shokushu_chk']) { ?>
                                    <?php echo $shokushu_options[$detail['Recruit']['shokushu']]; ?>
                                <?php } ?>
                                <!-- <br> -->
                            </p>

                        </div>
                    <?php } ?>
                        
                    <!--登録フォーム-->
                    <div id="form-wizard" class="section">
                        <h3 class="form_title pc">【無料】非公開求人のお申し込み</h3>
                        <div class="container">
                            <div class="form-box">
                                <div class="form-inner">
                                    <?php
                                        $url = '/private-form/add';
                                        if (!empty($recruit)) {
                                            $url = $url . '/' . $recruit['Recruit']['id'];
                                        }
                                        echo $this->Form->create('Customer', array(
                                            'inputDefaults' => array('label' => false, 'div' => false),
                                            'id' => 'landing-form',
                                            'class' => 'form-horizontal',
                                            'url' => $url
                                        ));
                                    ?>
                                    <div id="landing-form-content">
                                        <h3>保有資格</h3>
                                        <section>
                                            <h4 class="question">お持ちの資格を教えてください<br><span>(※複数選択可)</span></h4>
                                            <div class="answer">
                                                <div id="shikaku_error"></div>
                                                <div class="row">
                                                    <?php foreach ($shikakus as $key => $value) : ?>
                                                        <div class="col-xs-6">
                                                            <div class="checkbox">
                                                                <input type="checkbox" name="data[Customer][shikaku][]" value="<?php echo $key; ?>" id="CustomerShikaku<?php echo $key; ?>" data-validation-engine="validate[minCheckbox[1]]" data-prompt-target="shikaku_error">
                                                                <label for="CustomerShikaku<?php echo $key; ?>"><?php echo $value; ?></label>
                                                            </div>
                                                        </div>
                                                    <?php endforeach; ?>
                                                </div>
                                            </div>
                                        </section>
                                        <h3>今の状況と働き方</h3>
                                        <section>
                                            <h4 class="question">希望の働き方を選択してください<br><span>(※複数選択可)</span></h4>
                                            <div class="answer mb30">
                                                <div id="kibouwork_error"></div>
                                                <div class="row">
                                                    <?php foreach ($kibouworks as $key => $value) : ?>
                                                        <div class="col-xs-6">
                                                            <div class="checkbox">
                                                                <input type="checkbox" name="data[Customer][kibouwork][]" value="<?php echo $key; ?>" id="CustomerKibouwork<?php echo $key; ?>" data-validation-engine="validate[minCheckbox[1]]" data-prompt-target="kibouwork_error">
                                                                <label for="CustomerKibouwork<?php echo $key; ?>"><?php echo $value; ?></label>
                                                            </div>
                                                        </div>
                                                    <?php endforeach; ?>
                                                </div>
                                            </div>
                                            <h4 class="question">今の状況を教えてください</h4>
                                            <div class="answer">
                                                <div class="radio-style clearfix">
                                                    <?php foreach ($kinmukaishijiki as $key => $value) : ?>
                                                        <div class="radio">
                                                            <input type="radio" name="data[Customer][kinmukaishijiki]" value="<?php echo $key; ?>" checked="checked" id="CustomerKinmukaishijiki<?php echo $key; ?>">
                                                            <label for="CustomerKinmukaishijiki<?php echo $key; ?>"><?php echo $value; ?></label>
                                                            <div class="check"></div>
                                                        </div>
                                                    <?php endforeach; ?>
                                                </div>
                                            </div>
                                        </section>
                                        <h3>住所</h3>
                                        <section>
                                            <h4 class="question">郵便番号を入力、生まれ年を選択</h4>
                                            <div class="answer">
                                                <div class="form-group zip">
                                                    <?php echo $this->Form->label('zip', '郵便番号', array('class' => 'control-label col-xs-3')); ?>
                                                    <div class="col-xs-9" id="post_number_wrap">
                                                        <?php echo $this->Form->input('zip', array('type' => 'text', 'wrapInput' => false, 'class' => 'form-control', 'placeholder' => '例）000-0000', 'maxlength' => '128')); ?>
                                                        <div class="addon">※ハイフンなしOK</div>
                                                    </div>
                                                </div>
                                                <div id="birth_error"></div>
                                                <div class="form-group birth">
                                                    <?php echo $this->Form->label('Customer.birth.year', '生まれ年', array('class' => 'control-label col-xs-3')); ?>
                                                    <div class="col-xs-9">
                                                        <!-- 
                                                            20190610 sohnishi
                                                            西暦/和暦表記に変更
                                                        -->
                                                        <?php echo $this->Form->year('Customer.birth.year', '1958', date('Y') - 18, array('class' => 'form-control', 'empty' => '西暦/和暦', 'data-validation-engine' => 'validate[required]', 'data-prompt-target' => 'birth_error')); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <h3>連絡先</h3>
                                        <section>
                                            <h4 class="question">こちらが最後です☆<br>名前、連絡先を入力してください</h4>
                                            <div class="answer">
                                                <div class="form-group">
                                                    <?php echo $this->Form->label('name', 'お名前', array('class' => 'control-label col-xs-3')); ?>
                                                    <div class="col-xs-9">
                                                        <?php echo $this->Form->input('name', array('type' => 'text', 'class' => 'form-control', 'placeholder' => '例）大阪花子', 'data-validation-engine' => 'validate[required]')); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <?php echo $this->Form->label('hurigana', 'フリガナ', array('class' => 'control-label col-xs-3')); ?>
                                                    <div class="col-xs-9">
                                                        <?php echo $this->Form->input('hurigana', array('type' => 'text', 'class' => 'form-control', 'placeholder' => '例）フクオカハナコ', 'data-validation-engine' => 'validate[required,custom[onlyKatakana]]')); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <?php echo $this->Form->label('keitai_tel', '電話番号', array('class' => 'control-label col-xs-3')); ?>
                                                    <div class="col-xs-9">
                                                        <?php echo $this->Form->input('keitai_tel', array('type' => 'text', 'class' => 'form-control', 'placeholder' => '例）000-0000-0000', 'data-validation-engine' => 'validate[required,minSize[9],maxSize[13],custom[phone]]')); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <?php echo $this->Form->label('email', 'メール', array('class' => 'control-label col-xs-3')); ?>
                                                    <div class="col-xs-9">
                                                        <?php echo $this->Form->input('email', array('type' => 'email', 'class' => 'form-control', 'placeholder' => '例）jobalc@info.jp', 'data-validation-engine' => 'validate[required, custom[email]]')); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                    <div class="actions clearfix">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <button type="button" id="stepsPrev" class="btn btn-block disabled" disabled="disabled"><span>もどる</span></button>
                                            </div>
                                            <div class="col-xs-6">
                                                <button type="button" id="stepsNext" class="btn btn-block"><span>つぎへ</span></button>
                                                <button type="button" id="stepsFinish" class="btn btn-block"><span>登録する</span></button>
                                            </div>
                                        </div>
                                    </div>
                                    <?php echo $this->Form->end(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    </main>
                </div>
            </div>
        </div>
		
        <footer class="footer_sp sp">
            <div class="container">
				<div id="footer-bottom" class="text-center">
					<address class="address"> Copyright © 2019 ALC Co.Ltd All Rights Reserved.</address>
				</div>
			</div>
		</footer>
        <footer class="footer_pc pc">
            <div class="container">
				
				<div id="footer-bottom" class="text-center">
					<div>厚生大臣許可 27-ュ-201641</div>
					<address> Copyright © 2019 ALC Co.Ltd All Rights Reserved.</address>
				</div>
			</div>
		</footer>
	</div>






	<!-- 検索モーダル -->

	<div class="modal_content">
        <div class="modal_window">
			<!-- <form action="/r/search.php" method="post" class="modal modal-foot is-none" data-modal-name="search" id="sform"> -->
			<?php
				echo $this->Form->create($model, array(
					'inputDefaults' => array(
						'label' => false,
						'inputWrap' => false,
						'div' => false,
					),
					'class' => 'form-control modal modal-foot is-none',
					'data-modal-name' => 'search',
					'id' => 'sform',
					// 'url' => '/recruits/results/'
					'url' => array_merge(array('action' => 'results'), $this->params['pass'])
				));
			?>
			<!-- <input type="hidden" name="formredirect" value="1"> -->
			<div class="modal_close_wrapper">
              <div class="modal_close js-modal" data-target-modal="search">
                <i class="fas fa-times close_icon"></i>
			  </div>
			</div>
              <div class="modal_inner">
                <div class="modal_head">
                  <h1 class="modal_headTit">求人検索</h1>
                  <p class="modal_headLink"><a id="restbtn">条件をクリア</a></p>
                  <!-- <p class="modal_headLink"><a href="/search-list/">履歴から検索</a></p> -->
                </div>
                <div class="modal_body">
                  <h2 class="ttl-m" id="area_sentaku">エリアを選択</h2>

                  <div class="l-grid l-grid-margin-xs-3 l-grid-margin-md-5 u-mb-xs-15">
                    <div class="l-grid_col col-xs-4 col-md-2">
                      <label class="input-radio js-tab" data-tab-group="area" data-tab-parent-name="1">
                        <input type="radio" id="select_area" checked="">
                        <span class="input-radioInner" id = "chiiki_select">
                          <span class="input-radioIcon"></span>
                          <span class="input-radioTxt">地域</span>
                        </span>
                      </label>
                    </div>
                    <div class="l-grid_col col-xs-5 col-md-2">
                      <label class="input-radio js-tab" data-tab-group="area" data-tab-parent-name="2">
                        <input type="radio" id="select_ln">
                        <span class="input-radioInner" id = "rosen_select">
                          <span class="input-radioIcon"></span>
                          <span class="input-radioTxt">路線・駅</span>
                        </span>
                      </label>
                    </div>
                  </div>

                  <div class="" data-tab-group="area" data-tab-child-name="1" id="chiiki_div">
                    <div class="l-grid u-mb-xs-30">

                      <!-- 地域 選択時に表示 -->
                      <div class="l-grid_col col-xs-12 col-md-4 u-mb-xs-10">
                        <label class="select js-selectToggle" data-target-toggle="chiiki_region" id="chiiki_region">
							<label for="pr" class="chdown"><i class="fas fa-angle-down chdown_icon"></i></label>
							<?php echo $this->Form->select('todoufuken', null, array('div' => false, 'empty' => '都道府県を選択', 'class' => 'reset prlist areapr')); ?>
                        </label>
                      </div>
                      <!-- /地域 選択時に表示 -->

                      <!-- 地域 → 都道府県 選択時に表示 -->
                      <div class="l-grid_col col-xs-12 col-md-4" data-toggle-name="chiiki_region">
                        <div class="js-checkBoxToggle" data-target-toggle="shikuchoson">
                            <label class="select js-selectToggle" data-target-toggle="chiiki_region">
							<label for="pr" class="chdown"><i class="fas fa-angle-down chdown_icon"></i></label>
							<?php echo $this->Form->select('shikutyouson', null, array('div' => false, 'empty' => '市区町村を選択', 'class' => 'reset prlist areapr')); ?>
                              <!-- <select name="pr" class="reset prlist areapr">
                                <option value="">市区町村を選択</option>
                              </select> -->
                            </label>
                        </div>
                      </div>
                      <!-- /地域 → 都道府県 選択時に表示 -->

                    </div>
                  </div>


                  <div class=" u-d-n" data-tab-group="area" data-tab-child-name="2" id="rosen_div">
                    <div class="l-grid u-mb-xs-30">
                      <!-- 路線 選択時に表示 -->

                      <div class="l-grid_col col-xs-12 col-md-4 u-mb-xs-10">
                        <label class="select js-selectToggle" data-target-toggle="rosen_region" id="rosen_region">
							<label for="pr" class="chdown"><i class="fas fa-angle-down chdown_icon"></i></label>
							<?php echo $this->Form->select('pref_cd', $prefs, array('div' => false, 'empty' => '都道府県を選択', 'class' => 'reset prlist areapr')); ?>
                        </label>
                      </div>
                      <!-- /路線 選択時に表示 -->

                      <!-- 路線 → 都道府県 選択時に表示 -->
                      <div class="l-grid_col col-xs-12 col-md-4 lnclick" data-toggle-name="rosen_region">
                        <div class="js-checkBoxToggle" data-target-toggle="rosen">
                            <label class="select js-selectToggle select_mbt" data-target-toggle="rosen_region">
							<label for="pr" class="chdown"><i class="fas fa-angle-down chdown_icon"></i></label>
							<?php echo $this->Form->select('line_cd', $lines, array('div' => false, 'empty' => '最寄り路線を選択', 'class' => 'reset prlist areapr')); ?>
                              <!-- <select name="pr" class="reset prlist lnpr">
                                <option value="">路線を選択</option>
							 </select> -->
						</label>
                        </div>
                      </div>
                      <!-- /路線 → 都道府県 選択時に表示 -->

                      <!-- 路線 → 都道府県 → 路線 選択時に表示 -->
                      <div class="l-grid_col col-xs-12 col-md-4 lnclick" data-toggle-name="rosen_region">
                        <div class="js-checkBoxToggle" data-target-toggle="rosen">
                            <label class="select js-selectToggle" data-target-toggle="rosen_region">
							<label for="pr" class="chdown"><i class="fas fa-angle-down chdown_icon"></i></label>
							<?php echo $this->Form->select('station_cd', $stations, array('div' => false, 'empty' => '最寄り駅を選択', 'class' => 'reset prlist areapr')); ?>
                              <!-- <select name="pr" class="reset prlist lnpr">
                                <option value="">駅を選択</option>
							  </select> -->
						</label>
                        </div>
                      </div>
                      <!-- 路線 → 都道府県 → 路線 選択時に表示 -->

                    </div>
				  </div>
				  


				  <h2 class="ttl-m">職種を選択<span class="ttl-m_sub">（複数選択可）</span></h2>
                  <div class="l-grid l-grid-margin-xs-2 l-grid-margin-md-5 u-mb-xs-25">

                    <?php 
                        foreach ($shokushu_options as $key => $value) {
                    ?>
                            
                            <div class="l-grid_col col-xs-6 col-md-2 u-mb-xs-5 u-mb-md-10">
                            <label class="input-checkbox">
                                <?php
                                    echo $this->Form->input('Recruit.shokushu.' . $key, array('type' => 'checkbox', 'class' => 'reset', 'value' => $key, 'hiddenField' => false));
                                ?>
                                <span class="input-checkboxInner">
                                    <span class="input-checkboxIcon"></span>
                                    <span class="input-checkboxTxt"><?php echo $value; ?></span>
                                </span>
                            </label>
                            </div>
                    <?php
                        }
                    ?>

                  </div>


                  <h2 class="ttl-m">勤務形態を選択<span class="ttl-m_sub">（複数選択可）</span></h2>
                  <div class="l-grid l-grid-margin-xs-2 l-grid-margin-md-5 u-mb-xs-25">

                    <?php 
                        foreach ($kinmu_keitai_options as $key => $value) {
                    ?>
                            
                            <div class="l-grid_col col-xs-6 col-md-2 u-mb-xs-5 u-mb-md-10">
                            <label class="input-checkbox">
                                <?php
                                    echo $this->Form->input('Recruit.kinmu_keitai.' . $key, array('type' => 'checkbox', 'class' => 'reset', 'value' => $key, 'hiddenField' => false));
                                ?>
                                <span class="input-checkboxInner">
                                    <span class="input-checkboxIcon"></span>
                                    <span class="input-checkboxTxt"><?php echo $value; ?></span>
                                </span>
                            </label>
                            </div>
                    <?php
                        }
                    ?>

				  </div>
				  

                  <h2 class="ttl-m" id="shikaku_sentaku">資格を選択<span class="ttl-m_sub">（複数選択可）</span></h2>
                  <div class="l-grid l-grid-margin-xs-2 l-grid-margin-md-5 u-mb-xs-25">

                  	<?php 
                        foreach ($shikaku_options as $key => $value) {
                    ?>
                            
                            <div class="l-grid_col col-xs-6 col-md-2 u-mb-xs-5 u-mb-md-10">
                            <label class="input-checkbox">
                                <?php 
									echo $this->Form->input('Recruit.shikaku.' . $key, array('type' => 'checkbox', 'class' => 'reset', 'value' => $key, 'hiddenField' => false));
                                ?>
                                <span class="input-checkboxInner">
                                    <span class="input-checkboxIcon"></span>
                                    <span class="input-checkboxTxt"><?php echo $value; ?></span>
                                </span>
                            </label>
                            </div>
                    <?php
                        }
                    ?>

                    
                  </div>

                  <h2 class="ttl-m" id="shisetsu_sentaku">施設業態を選択<span class="ttl-m_sub">（複数選択可）</span></h2>
                  <div class="l-grid l-grid-margin-xs-2 l-grid-margin-md-5 u-mb-xs-25">
                    

                    <?php 
                        foreach ($shisetsukeitaiFirsts as $key => $value) {
                    ?>
                            <div class="l-grid_col col-xs-6 col-md-2 u-mb-xs-5 u-mb-md-10">
                                <label class="input-checkbox">
                                    <?php 
										echo $this->Form->input('Recruit.shisetsukeitai_first.' . $key, array('type' => 'checkbox', 'class' => 'reset', 'value' => $key, 'hiddenField' => false));
                                    ?>
                                    <span class="input-checkboxInner">
                                        <span class="input-checkboxIcon"></span>
                                        <span class="input-checkboxTxt"><?php echo $value; ?></span>
                                    </span>
                                </label>
                            </div>
                    <?php
                        }
                    ?>
                  
                  </div>

                  <h2 class="ttl-m" id="jouken_sentaku">こだわり条件を選択<span class="ttl-m_sub">（複数選択可）</span></h2>
                  <div class="l-grid l-grid-margin-xs-2 l-grid-margin-md-5 u-mb-xs-25">

                    <?php 
                        foreach ($kodawaris as $key => $value) {
                    ?>
                            <div class="l-grid_col col-xs-6 col-md-2 u-mb-xs-5 u-mb-md-10">
                                <label class="input-checkbox">
                                    <?php 
										echo $this->Form->input('Recruit.kodawari.' . $key, array('type' => 'checkbox', 'class' => 'reset', 'value' => $key, 'hiddenField' => false));
                                    ?>
                                    <span class="input-checkboxInner">
                                        <span class="input-checkboxIcon"></span>
                                        <span class="input-checkboxTxt"><?php echo $value; ?></span>
                                    </span>
                                </label>
                            </div>
                    <?php
                        }
                    ?>
                  </div>

                  <h2 class="ttl-m">キーワードを入力</h2>
                  <div class="l-grid">
                    <div class="l-grid_col col-xs-12 col-md-6">
                        <div>
                            <?php echo $this->Form->input('keyword', array('type' => 'text', 'class' => 'input-txt resettext', 'placeholder' => 'ここにキーワードを入力')); ?>
                        </div>
                      <p class="u-fz-xs-12rem">例：東京駅、世田谷、◯◯◯老人ホーム</p>
                    </div>
                  </div>
                </div>
              </div>

              <div class="modal_foot">
                <div class="l-grid l-grid-ali-xs-c l-grid-juc-md-c">
                  <div class="l-grid_col col-xs-7 col-md-3">
                    <p class="modal_footResult"><span class="modal_footResultTxt" style="font-size: inherit" id="gai">該当求人数</span><span class="js-count" id="real_time_count"><?php echo $activeCount; ?></span><span style="font-size: inherit" id="ken">件</span></p>
                  </div>
                  <div class="l-grid_col col-xs-5 col-md-3">


                    <!-- desabledパターン is-disabled -->
                    <p class="modal_footSubmit">
					
                        <button type="submit" class="btn-m btn-search2">
                            <span class="btn_inner">
                                <div class="absearch">
									<?php
										echo $this->Html->image('icon002.svg', array('alt' => 'ジョブアルク介護ロゴ', 'class' => 'search_icon', 'style'=>'width: 40px'));
									?>
                                        <p>検索する<p>
                                </div>
                            </span>
                        </button>
                    </p>
                  </div>
                </div>
			  </div>
			  
			  <?php echo $this->Form->end(); ?>

        </div>
    </div>

	<!-- 検索モーダル -->
<?php echo $this->Html->scriptStart(); ?>

/**
 *  20190620 sohnishi
 *  デバイスHeightがFooterの座標より長い場合、
 *  Footerを最下に固定する
 */
//$(function () {
//  var off = $('.footer_sp').offset();
//  if ($(window).height() > off.top) {
//    $('.footer_sp').css('position', 'absolute');
//  $('.footer_sp').css('bottom', '0');
//  }
//});

<?php echo $this->Html->scriptEnd(); ?>

</body>
</html>