<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="format-detection" content="telephone=no">
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
<title>登録の完了ページ【ジョブアルク介護】</title>
<link href="<?php echo $this->Html->url('/favicon.ico'); ?>" type="image/x-icon" rel="icon">
<link href="<?php echo $this->Html->url('/favicon.ico'); ?>" type="image/x-icon" rel="shortcut icon">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
<?php 
    echo $this->Html->css('base');
    echo $this->Html->css('modal');
    echo $this->Html->css('validationEngine.jquery'); 
    echo $this->Html->css('Forms.form');
?>
<?php echo $this->Html->script('jquery-1.11.2.min'); ?>
<?php echo $this->Html->script('jquery.cookie'); ?>
<?php echo $this->Html->script('bootstrap.min'); ?>
<?php echo $this->Html->script('addrajax/addrajax'); ?>
<?php echo $this->Html->script('jquery.steps.min'); ?>
<?php echo $this->Html->script('jquery.validationEngine-ja'); ?>
<?php echo $this->Html->script('jquery.validationEngine'); ?>
<?php echo $this->Html->script('jquery.autoKana'); ?>
<?php echo $this->Html->script('landing'); ?>
<!--[if lt IE 9]>
	<script type="text/javascript" src="theme/Jobalc/js/html5shiv.min.js"></script>
	<script type="text/javascript" src="theme/Jobalc/js/respond.min.js"></script>
	<script type="text/javascript">document.createElement('main');</script>
<![endif]-->



<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TMH9GK');</script>
<!-- End Google Tag Manager -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TMH9GK"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <div id="body_wrapper">
		<header>
			<div class="container">
                <div class="header">
                    <div class="logo">
						<?php 
							echo $this->Html->link(
							$this->Html->image('logo.svg', array('alt' => 'ジョブアルク介護ロゴ')),
							'/',
							array('escape' => false)); 
						?>
                    </div>
                </div>
			</div>
		</header>
        <div id="main">
			<div class="container">
                <div class="thanks-box">
                    <h2 class="thanks-title">無料web登録</h2>
                    <div class="thanks-inner">
                        <p>この度は、ジョブアルク転職サポートにご登録いただきまして、誠にありがとうございます。</p>
                        <p>今後の流れについての説明やご登録の内容についての確認を24時間以内に、お電話にてご連絡をさせていただきます。</p>
                        <p>しばらくお待ちくださいますようお願いいたします。</p>
                    </div>
                </div>
            </div>
        </div>
        <footer class="footer_sp sp">
            <div class="container">
				<div id="footer-bottom" class="text-center">
					<address class="address"> Copyright © 2019 ALC Co.Ltd All Rights Reserved.</address>
				</div>
			</div>
		</footer>
        <footer class="footer_pc pc">
            <div class="container">
				
				<div id="footer-bottom" class="text-center">
					<div>厚生大臣許可 27-ュ-201641</div>
					<address> Copyright © 2019 ALC Co.Ltd All Rights Reserved.</address>
				</div>
			</div>
		</footer>
	</div>







<?php echo $this->Html->scriptStart(); ?>

/**
 *  20190620 sohnishi
 *  デバイスHeightがFooterの座標より長い場合、
 *  Footerを最下に固定する
 */
//$(function () {
//  var off = $('.footer_sp').offset();
//  if ($(window).height() > off.top) {
//    $('.footer_sp').css('position', 'absolute');
//  $('.footer_sp').css('bottom', '0');
//  }
//});

<?php echo $this->Html->scriptEnd(); ?>

</body>
</html>
