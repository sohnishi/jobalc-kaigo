<?php

App::uses('EkidataAppModel', 'Ekidata.Model');

class RailroadPref extends EkidataAppModel {

	public $name = 'RailroadPref';

	public $useTable = 'railroad_preves';

	public $primaryKey = 'pref_cd';

	public $actsAs = array(
		'CsvImport' => array(
			'delimiter' => ','
		)
	);

	public $validate = array(
		'csvfile' => array(
			'required' => array(
				'rule' => array('notEmpty', 'csvfile'),
				'message' => 'CSVファイルを選択してください'
			)
		)
	);

	public $displayField = 'pref_name';

	public $hasMany = array(
		'RailroadStation' => array(
			'className' => 'Ekidata.RailroadStation',
			'foreignKey' => 'pref_cd',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
}
