<?php

App::uses('EkidataAppModel', 'Ekidata.Model');
App::uses('Hash', 'Utility');

class RailroadStation extends EkidataAppModel {

	public $displayField = 'station_name';

	public $useTable = 'railroad_stations';

	public $primaryKey = 'station_cd';

	public $actsAs = array(
		'CsvImport' => array(
			'delimiter' => ','
		)
	);

	public $validate = array(
		'csvfile' => array(
			'required' => array(
				'rule' => array('notEmpty', 'csvfile'),
				'message' => 'CSVファイルを選択してください'
			)
		)
	);

	public $belongsTo = array(
		'RailroadLine' => array(
			'className' => 'Ekidata.RailroadLine',
			'foreignKey' => 'line_cd',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'RailroadPref' => array(
			'className' => 'Ekidata.RailroadPref',
			'foreignKey' => 'pref_cd',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public function getPrefLineId($prefId = null) {
		$query = array(
			'conditions' => array($this->alias . '.pref_cd' => $prefId),
			'group' => array($this->alias . '.line_cd'),
			'fields' => array($this->alias . '.line_cd')
		);
		$lines = $this->find('all', $query);

		if (!empty($lines)) {
			return Hash::extract($lines, '{n}.' . $this->alias . '.line_cd');
		} else {
			return false;
		}
	}
}
