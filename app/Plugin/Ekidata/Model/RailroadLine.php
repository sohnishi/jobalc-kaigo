<?php

App::uses('EkidataAppModel', 'Ekidata.Model');
App::uses('RailroadStation', 'Ekidata.Model');
App::uses('Hash', 'Utility');

class RailroadLine extends EkidataAppModel {

	public $name = 'RailroadLine';

	public $displayField = 'line_name';

	public $useTable = 'railroad_lines';

	public $primaryKey = 'line_cd';

	public $actsAs = array(
		'CsvImport' => array(
			'delimiter' => ','
		)
	);

	public $validate = array(
		'csvfile' => array(
			'required' => array(
				'rule' => array('notEmpty', 'csvfile'),
				'message' => 'CSVファイルを選択してください'
			)
		)
	);

	public $hasMany = array(
		'RailroadStation' => array(
			'className' => 'Ekidata.RailroadStation',
			'foreignKey' => 'line_cd',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
}
