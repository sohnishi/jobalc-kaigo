<?php

App::uses('EkidataAppController', 'Ekidata.Controller');
App::uses('Hash', 'Utility');

class ApiController extends EkidataAppController {

	public $name = 'Api';

	public $uses = array('Ekidata.RailroadCompany', 'Ekidata.RailroadLine', 'Ekidata.RailroadStation');

	public $helpers = array('Form', 'Html', 'Session', 'Js');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->autoRender = false;
		$this->layout = 'ajax';

		if (Configure::read('debug') == 0 && !$this->request->is('ajax')) {
			throw new MethodNotAllowedException();
		}
	}

	public function lines($prefId = null) {
		$this->_lines($prefId);
	}

	public function line_stations($prefId = null) {
		$this->_lines($prefId, array('RailroadStation'));
	}

	private function _lines($prefId = null, $contain = array()) {
		if (empty($prefId)) {
			$this->_jsonRender();
			exit;
		}

		$conditions = array();

		if (!empty($prefId)) {
			$lineId = $this->RailroadStation->getPrefLineId($prefId);
			if (!empty($lineId)) {
				$conditions = Hash::merge($conditions, array(
					'RailroadLine.line_cd' => $lineId
				));
			}
		}

		$query = array(
			'conditions' => $conditions,
			'contain' => $contain,
			'order' => array(
				'RailroadLine.e_sort' => 'ASC'
			)
		);

		$_data = $this->RailroadLine->find('all', $query);
		$this->_jsonRender($_data);
	}

	public function stations($lineId = null) {
		if (empty($lineId)) {
			$this->_jsonRender();
			exit;
		}

		$query = array(
			'conditions' => array(
				'RailroadStation.line_cd' => $lineId
			),
			'order' => array(
				'RailroadStation.e_sort' => 'ASC'
			)
		);
		$_data = $this->RailroadStation->find('all', $query);
		$this->_jsonRender($_data);
	}

	private function _jsonRender($_data = array()) {
		header("Content-Type: application/json; charset=utf-8");
		echo json_encode($_data);
	}
}
