<?php

App::uses('Component', 'Controller');
App::uses('RailroadPref', 'Ekidata.Model');
App::uses('RailroadLine', 'Ekidata.Model');
App::uses('RailroadStation', 'Ekidata.Model');
App::uses('Hash', 'Utility');

class RailroaderComponent extends Component {

	public $settings = array(
		'pref' => array(
			'id' => 'pref_cd',
			'list' => 'railroadPrefs'
		),
		'line' => array(
			'id' => 'line_cd',
			'list' => 'railroadLines'
		),
		'station' => array(
			'id' => 'station_cd',
			'list' => 'railroadStations'
		)
	);

	public function __construct(ComponentCollection $collection, $settings = array()) {
		$default = $this->settings;
		parent::__construct($collection, $settings);
		$this->settings = Hash::merge($default, $settings);
	}

	public function initialize(Controller $controller) {
	}

	public function startup(Controller $controller) {
		$this->RailroadPref = new RailroadPref;
		$this->RailroadLine = new RailroadLine;
		$this->RailroadStation = new RailroadStation;
	}

	public function beforeRender(Controller $controller) {
		$railroadPrefs = $this->RailroadPref->find('list');

		$railroadLines = array();
		if (!empty($controller->request->data[$controller->modelClass][$this->settings['pref']['id']])) {
			$lineId = $this->RailroadStation->getPrefLineId($controller->request->data[$controller->modelClass][$this->settings['pref']['id']]);
			if (!empty($lineId)) {
				$query = array(
					'conditions' => array(
						'RailroadLine.line_cd' => $lineId
					),
					'order' => array(
						'RailroadLine.e_sort' => 'ASC'
					),
					'fields' => array(
						'line_cd',
						'line_name'
					)
				);
				$railroadLines = $this->RailroadLine->find('list', $query);
			}
		}

		$railroadStations = array();
		if (!empty($controller->request->data[$controller->modelClass][$this->settings['line']['id']])) {
			$query = array(
				'conditions' => array(
					'RailroadStation.line_cd' => $controller->request->data[$controller->modelClass][$this->settings['line']['id']]
				),
				'order' => array(
					'RailroadStation.e_sort' => 'ASC'
				),
				'fields' => array(
					'station_cd',
					'station_name'
				)
			);
			$railroadStations = $this->RailroadStation->find('list', $query);
		}

		$controller->set(array(
			$this->settings['pref']['list'] => $railroadPrefs,
			$this->settings['line']['list'] => $railroadLines,
			$this->settings['station']['list'] => $railroadStations
		));
	}
}
