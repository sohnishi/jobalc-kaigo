<?php

App::uses('EkidataAppController', 'Ekidata.Controller');

class RailroadPrevesController extends EkidataAppController {

	public $name = 'RailroadPreves';

	public $uses = array('Ekidata.RailroadPref');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('model', $this->modelClass);
		$this->set('title_for_layout', '都道府県マスタ');
		$this->set('currentUser', $this->Auth->user());

	}

	public function admin_index() {
		$this->Paginator->settings[$this->modelClass] = array(
			'order' => array($this->modelClass . '.pref_cd' => 'ASC')
		);
		$this->set('prefs', $this->Paginator->paginate());
	}

	public function admin_import() {
		if ($this->request->is('post')) {
			$records_count = $this->{$this->modelClass}->find('count');
			try {
				$this->{$this->modelClass}->importCSV($this->request->data[$this->modelClass]['csvfile']['tmp_name']);
			} catch (Exception $e) {
				$import_errors = $this->{$this->modelClass}->getImportErrors();
				$this->set('import_errors', $import_errors);
				$this->flashMsg('インポートエラー' . ' ' . $this->request->data[$this->modelClass]['csvfile']['name'] . ', カラム名に不具合があります', 'alert-danger');
				$this->redirect(array('action' => 'index'));
			}

			$new_records_count = $this->{$this->modelClass}->find('count') - $records_count;
			$this->flashMsg('インポートが完了しました' . ' ' . $new_records_count . ' レコード : ' . $this->request->data[$this->modelClass]['csvfile']['name']);
			$this->redirect(array('action' => 'index'));
		}
	}

	public function admin_edit($id = null) {
		if (!$id || !$this->{$this->modelClass}->exists($id)) {
			$this->flashMsg('無効な操作です', 'alert-danger');
			$this->redirect(array('action' => 'index'));
		}

		if (!empty($this->request->data)) {
			if ($this->{$this->modelClass}->save($this->request->data)) {
				$this->flashMsg('都道府県名を更新しました');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->flashMsg('都道府県名を更新中にエラーが発生しました', 'alert-warning');
			}
		} else {
			$this->request->data = $this->{$this->modelClass}->read(null, $id);
		}
	}

	public function admin_delete($id = null) {
		if (!$id || !$this->{$this->modelClass}->exists($id)) {
			$this->flashMsg('無効な操作です', 'alert-danger');
			$this->redirect(array('action' => 'index'));
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->{$this->modelClass}->delete($id)) {
				$this->flashMsg('都道府県を削除しました');
			} else {
				$this->flashMsg('都道府県を削除中にエラーが発生しました', 'alert-warning');
			}
		}
		$this->redirect(array('action' => 'index'));
	}

	public function admin_allDelete() {
		if ($this->request->is('post') || $this->request->is('put')) {
			$result = $this->{$this->modelClass}->find('first');
			if (!empty($result)) {
				// 問答無用でテーブルを空にしてしまう
				$this->{$this->modelClass}->query('TRUNCATE railroad_preves');
				$this->flashMsg('テーブルを空にしました');
			} else {
				$this->flashMsg('既にテーブルは空です', 'alert-warning');
			}
		}
		$this->redirect(array('action' => 'index'));
	}
}
