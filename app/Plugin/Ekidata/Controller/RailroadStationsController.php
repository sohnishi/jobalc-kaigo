<?php

App::uses('EkidataAppController', 'Ekidata.Controller');

class RailroadStationsController extends EkidataAppController {

	public $name = 'RailroadStations';

	public $uses = array('Ekidata.RailroadStation');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('model', $this->modelClass);
		$this->set('title_for_layout', '駅マスタ');
		$this->set('currentUser', $this->Auth->user());

	}

	public function admin_index() {
		$this->Paginator->settings[$this->modelClass] = array(
			'limit' => 50,
			'order' => array($this->modelClass . '.station_cd' => 'asc')
		);
		$this->set('stations', $this->Paginator->paginate());
		$lines = $this->{$this->modelClass}->RailroadLine->find('list', array('fields' => array('line_cd', 'line_name')));
		$this->set('lines', $lines);
		$preves = $this->{$this->modelClass}->RailroadPref->find('list', array('fields' => array('pref_cd', 'pref_name')));
		$this->set('preves', $preves);
	}

	public function admin_import() {
		if ($this->request->is('post')) {
			$records_count = $this->{$this->modelClass}->find('count');
			try {
				$this->{$this->modelClass}->importCSV($this->request->data[$this->modelClass]['csvfile']['tmp_name']);
			} catch (Exception $e) {
				$import_errors = $this->{$this->modelClass}->getImportErrors();
				$this->set('import_errors', $import_errors);
				$this->flashMsg('インポートエラー' . ' ' . $this->request->data[$this->modelClass]['csvfile']['name'] . ', カラム名に不具合があります', 'alert-danger');
				$this->redirect(array('action' => 'index'));
			}

			$new_records_count = $this->{$this->modelClass}->find('count') - $records_count;
			$this->flashMsg('インポートが完了しました' . ' ' . $new_records_count . ' レコード : ' . $this->request->data[$this->modelClass]['csvfile']['name']);
			$this->redirect(array('action' => 'index'));
		}
	}

	public function admin_edit($id = null) {
		if (!$id || !$this->{$this->modelClass}->exists($id)) {
			$this->flashMsg('無効な操作です', 'alert-danger');
			$this->redirect(array('action' => 'index'));
		}

		if (!empty($this->request->data)) {
			if ($this->{$this->modelClass}->save($this->request->data)) {
				$this->flashMsg('駅を更新しました');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->flashMsg('駅を更新中にエラーが発生しました', 'alert-warning');
			}
		} else {
			$this->request->data = $this->{$this->modelClass}->read(null, $id);
		}
		$lines = $this->{$this->modelClass}->RailroadLine->find('list', array('fields' => array('line_cd', 'line_name')));
		$this->set('lines', $lines);
		$preves = $this->{$this->modelClass}->RailroadPref->find('list', array('fields' => array('pref_cd', 'pref_name')));
		$this->set('preves', $preves);
	}

	public function admin_delete($id = null) {
		if (!$id || !$this->{$this->modelClass}->exists($id)) {
			$this->flashMsg('無効な操作です', 'alert-danger');
			$this->redirect(array('action' => 'index'));
		}

		if ($this->{$this->modelClass}->delete($id)) {
			$this->flashMsg('駅を削除しました');
			$this->redirect(array('action' => 'index'));
		} else {
			$this->flashMsg('駅を削除中にエラーが発生しました', 'alert-warning');
		}
	}

	public function admin_allDelete() {
		if ($this->request->is('post') || $this->request->is('put')) {
			$result = $this->{$this->modelClass}->find('first');
			if (!empty($result)) {
				// 問答無用でテーブルを空にしてしまう
				$this->{$this->modelClass}->query('TRUNCATE railroad_stations');
				$this->flashMsg('テーブルを空にしました');
			} else {
				$this->flashMsg('既にテーブルは空です', 'alert-warning');
			}
		}
		$this->redirect(array('action' => 'index'));
	}
}
