<?php

App::uses('EkidataAppController', 'Ekidata.Controller');

class RailroadLinesController extends EkidataAppController {

	public $name = 'RailroadLines';

	public $uses = array('Ekidata.RailroadLine');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('model', $this->modelClass);
		$this->set('title_for_layout', '路線マスタ');
		$this->set('currentUser', $this->Auth->user());

	}

	public function admin_index() {
		$this->Paginator->settings[$this->modelClass] = array(
			'limit' => 50,
			'order' => array($this->modelClass . '.line_cd' => 'asc')
		);
		$this->set('lines', $this->Paginator->paginate());
	}

	public function admin_import() {
		if ($this->request->is('post')) {
			$records_count = $this->{$this->modelClass}->find('count');
			try {
				$this->{$this->modelClass}->importCSV($this->request->data[$this->modelClass]['csvfile']['tmp_name']);
			} catch (Exception $e) {
				$import_errors = $this->{$this->modelClass}->getImportErrors();
				$this->set('import_errors', $import_errors);
				$this->flashMsg('インポートエラー' . ' ' . $this->request->data[$this->modelClass]['csvfile']['name'] . ', カラム名に不具合があります', 'alert-danger');
				$this->redirect(array('action' => 'index'));
			}

			$new_records_count = $this->{$this->modelClass}->find('count') - $records_count;
			$this->flashMsg('インポートが完了しました' . ' ' . $new_records_count . ' レコード : ' . $this->request->data[$this->modelClass]['csvfile']['name']);
			$this->redirect(array('action' => 'index'));
		}
	}

	public function admin_edit($id = null) {
		if (!$id || !$this->{$this->modelClass}->exists($id)) {
			$this->flashMsg('無効な操作です', 'alert-danger');
			$this->redirect(array('action' => 'index'));
		}

		if (!empty($this->request->data)) {
			if ($this->{$this->modelClass}->save($this->request->data)) {
				$this->flashMsg('路線を更新しました');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->flashMsg('路線を更新中にエラーが発生しました', 'alert-warning');
			}
		} else {
			$this->request->data = $this->{$this->modelClass}->read(null, $id);
		}
	}

	public function admin_delete($id = null) {
		if (!$id || !$this->{$this->modelClass}->exists($id)) {
			$this->flashMsg('無効な操作です', 'alert-danger');
			$this->redirect(array('action' => 'index'));
		}

		if ($this->{$this->modelClass}->delete($id)) {
			$this->flashMsg('路線を削除しました');
			$this->redirect(array('action' => 'index'));
		} else {
			$this->flashMsg('路線を削除中にエラーが発生しました', 'alert-warning');
		}
	}

	public function admin_allDelete() {
		if ($this->request->is('post') || $this->request->is('put')) {
			$result = $this->{$this->modelClass}->find('first');
			if (!empty($result)) {
				// 問答無用でテーブルを空にしてしまう
				$this->{$this->modelClass}->query('TRUNCATE railroad_lines');
				$this->flashMsg('テーブルを空にしました');
			} else {
				$this->flashMsg('既にテーブルは空です', 'alert-warning');
			}
		}
		$this->redirect(array('action' => 'index'));
	}
}
