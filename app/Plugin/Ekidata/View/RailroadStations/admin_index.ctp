<div class="row">
	<div class="col-xs-12 contents-actions">
		<ul class="nav nav-tabs">
			<li class="active"><a href="javascript:void(0);">駅一覧</a></li>
			<li><?php echo $this->Html->link('CSVインポート', array('action' => 'import')); ?></li>
		</ul>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">

		<div class="panel panel-default">
			<div class="panel-heading">駅一覧</div>
			<div class="panel-body">

				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th><?php echo $this->Paginator->sort('station_cd', '駅コード'); ?></th>
							<th><?php echo $this->Paginator->sort('station_g_cd', '駅グループコード'); ?></th>
							<th><?php echo $this->Paginator->sort('station_name', '駅名称'); ?></th>
							<th><?php echo $this->Paginator->sort('line_cd', '路線'); ?></th>
							<th><?php echo $this->Paginator->sort('pref_cd', '都道府県'); ?></th>
							<th><?php echo $this->Paginator->sort('post', '駅郵便番号'); ?></th>
							<th><?php echo $this->Paginator->sort('add', '住所'); ?></th>
							<th><?php echo $this->Paginator->sort('lon', '経度'); ?></th>
							<th><?php echo $this->Paginator->sort('lat', '緯度'); ?></th>
							<th><?php echo $this->Paginator->sort('e_sort', '並び順'); ?></th>
							<th>操作</th>
						</tr>
					</thead>
					<tdoby>
						<?php foreach ($stations as $station) : ?>
						<tr>
							<td><?php echo $station[$model]['station_cd']; ?></td>
							<td><?php echo $station[$model]['station_g_cd']; ?></td>
							<td><?php echo $station[$model]['station_name']; ?></td>
							<td><?php echo isset($station[$model]['line_cd']) ? $lines[$station[$model]['line_cd']] : ''; ?></td>
							<td><?php echo isset($station[$model]['pref_cd']) ? $preves[$station[$model]['pref_cd']] : ''; ?></td>
							<td><?php echo $station[$model]['post']; ?></td>
							<td><?php echo $station[$model]['add']; ?></td>
							<td><?php echo $station[$model]['lon']; ?></td>
							<td><?php echo $station[$model]['lat']; ?></td>
							<td><?php echo $station[$model]['e_sort']; ?></td>
							<td>
								<ul class="list-inline">
									<li><?php echo $this->Html->link('編集', array('action' => 'edit', $station[$model]['station_cd'])); ?></li>
									<li><?php echo $this->Form->postLink('削除', array('action' => 'delete', $station[$model]['station_cd']), '', '本当に削除しますか？元には戻せません'); ?></li>
								</ul>
							</td>
						</tr>
						<?php endforeach; ?>
					</tdoby>
				</table>
				<div class="text-center">
					<?php echo $this->Paginator->pagination(array('ul' => 'pagination')); ?>
				</div>
			</div>
		</div>

	</div>
</div>