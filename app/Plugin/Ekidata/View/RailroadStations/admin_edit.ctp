<div class="row">
	<div class="col-xs-12 contents-actions">
		<ul class="nav nav-tabs">
			<li><?php echo $this->Html->link('駅一覧', array('action' => 'index')); ?></li>
			<li class="active"><a href="javascript:void(0);">駅の更新</a></li>
		</ul>
	</div>
</div>

<div class="row">
	<div class="col-xs-4 col-xs-offset-4">

		<?php
			echo $this->Form->create($model, array(
				'inputDefaults' => array(
					'div' => 'form-group',
					'class' => 'form-control'
				)
			));
		?>
		<div class="panel panel-default">
			<div class="panel-heading">駅の更新</div>
			<div class="panel-body">
				<?php

					echo $this->Form->input('station_cd');
					echo $this->Form->input('station_g_cd', array('type' => 'text', 'label' => '駅グループコード'));
					echo $this->Form->input('station_name', array('label' => '駅名'));
					echo $this->Form->input('line_cd', array('type' => 'select', 'options' => $lines, 'label' => '路線'));
					echo $this->Form->input('pref_cd', array('type' => 'select', 'options' => $preves, 'label' => '都道府県'));
					echo $this->Form->input('post', array('駅郵便番号'));
					echo $this->Form->input('add', array('type' => 'text', 'label' => '住所'));
					echo $this->Form->input('lon', array('label' => '経度'));
					echo $this->Form->input('lat', array('label' => '緯度'));
					echo $this->Form->input('e_sort', array('type' => 'text', 'label' => '並び順'));
				?>
			</div>
			<div class="panel-footer">
				<?php echo $this->Form->submit('駅を更新する', array('class' => 'btn btn-primary')); ?>
			</div>
		</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>