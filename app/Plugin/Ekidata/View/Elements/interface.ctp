<script>
	$(function () {
		var settings = {
			urls : {
				lines : '<?php echo $this->Html->url(array('plugin' => 'ekidata', 'controller' => 'api', 'action' => 'lines', 'admin' => false)); ?>',
				stations : '<?php echo $this->Html->url(array('plugin' => 'ekidata', 'controller' => 'api', 'action' => 'stations', 'admin' => false)); ?>'
			},
			/*
				20190529 sohnishi
				選択してくださいで統一されていたものを路線と駅で分けた
			*/
			lineEmptyText : '<?php echo $lineEmptyText ?>',
			stationEmptyText : '<?php echo $stationEmptyText ?>'
		}

		function initSelect(id) {
			/*
				20190529 sohnishi
				idで判定してemptyTextを切り分ける
			*/
			$(id).empty().append($('<option />').html(id == '#RecruitLineCd' ? settings.lineEmptyText : settings.stationEmptyText).val(''));
		}

		<?php if (isset($prefId) && isset($lineId)) : ?>
		$('<?php echo $prefId; ?>').on('change', function () {
			<?php if (isset($lineId)) : ?>initSelect('<?php echo $lineId; ?>');<?php endif; ?>
			<?php if (isset($stationId)) : ?>initSelect('<?php echo $stationId; ?>');<?php endif; ?>

			var url = settings.urls.lines + '/' + $(this).val();
			$.getJSON(url, function (json, status) {
				if (status == 'success') {
					$.each(json, function () {
						var _this = this;
						$('<?php echo $lineId; ?>').append(
								$('<option />').val(_this.RailroadLine.line_cd).html(_this.RailroadLine.line_name)
						);
					});
				}
			});
		});
		<?php endif; ?>

		<?php if (isset($lineId) && isset($stationId)) : ?>
		$('<?php echo $lineId; ?>').on('change', function () {
			initSelect('<?php echo $stationId; ?>');

			var url = settings.urls.stations + '/' + $(this).val();
			$.getJSON(url, function (json, status) {
				if (status == 'success') {
					$.each(json, function () {
						var _this = this;
						$('<?php echo $stationId; ?>').append(
								$('<option />').val(_this.RailroadStation.station_cd).html(_this.RailroadStation.station_name)
						);
					});
				}
			});
		});
		<?php endif; ?>
	});
</script>