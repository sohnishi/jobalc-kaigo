<div class="row">
	<div class="col-xs-12 contents-actions">
		<ul class="nav nav-tabs">
			<li><?php echo $this->Html->link('路線一覧', array('action' => 'index')); ?></li>
			<li class="active"><a href="javascript:void(0);">路線の更新</a></li>
		</ul>
	</div>
</div>

<div class="row">
	<div class="col-xs-4 col-xs-offset-4">

		<?php
			echo $this->Form->create($model, array(
				'inputDefaults' => array(
					'div' => 'form-group',
					'class' => 'form-control'
				)
			));
		?>
		<div class="panel panel-default">
			<div class="panel-heading">路線の更新</div>
			<div class="panel-body">
				<?php
					echo $this->Form->input('line_cd');
					echo $this->Form->input('line_name', array('label' => '路線名（一般）'));
					echo $this->Form->input('line_name_k', array('label' => '路線名（一般・カナ）'));
					echo $this->Form->input('line_name_h', array('label' => '路線名（正式名称）'));
					echo $this->Form->input('e_sort', array('type' => 'text', 'label' => '並び順'));
				?>
			</div>
			<div class="panel-footer">
				<?php echo $this->Form->submit('路線を更新する', array('class' => 'btn btn-primary')); ?>
			</div>
		</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>