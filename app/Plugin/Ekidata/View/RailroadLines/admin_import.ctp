<div class="row">
	<div class="col-xs-12 contents-actions">
		<ul class="nav nav-tabs">
			<li><?php echo $this->Html->link('路線一覧', array('action' => 'index')); ?></li>
			<li class="active"><a href="javascript:void(0);">CSVインポート</a></li>
		</ul>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">
		<div class="alert alert-danger">
			<ul class="list-unstyled">
				<li>CSVデータをインポート後にデータを確認して不要なデータは削除してください。</li>
				<li>路線データは「<a href="http://www.ekidata.jp/" target="_blank">駅データ.jp</a>」のデータ(UTF-8)を使用しています。</li>
				<li><?php echo $this->Form->postLink('すべてのデータを削除する', array('action' => 'allDelete'), '', '全てのデータを削除しますか？元には戻せません'); ?></li>
			</ul>

		</div>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">

		<?php
			echo $this->Form->create(array(
				'inputDefaults' => array(
					'label' => false,
					'wrapInput' => false,
					'div' => false,
					'class' => 'form-control'
				),
				'action' => 'import',
				'type' => 'file'
			));
		?>
		<div class="panel panel-default">
			<div class="panel-heading">CSVファイルをインポート</div>
			<div class="panel-body">
				<div class="form-group">
					<?php echo $this->Form->input('csvfile', array('type' => 'file', 'label' => 'CSVファイルを選択')); ?>
				</div>
			</div>
			<div class="panel-footer">
				<?php echo $this->Form->submit('アップロードする', array('class' => 'btn btn-primary')); ?>
			</div>
		</div>
		<?php echo $this->Form->end(); ?>

	</div>
</div>