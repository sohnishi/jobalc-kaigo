<div class="row">
	<div class="col-xs-12 contents-actions">
		<ul class="nav nav-tabs">
			<li class="active"><a href="javascript:void(0);">路線一覧</a></li>
			<li><?php echo $this->Html->link('CSVインポート', array('action' => 'import')); ?></li>
		</ul>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">

		<div class="panel panel-default">
			<div class="panel-heading">路線一覧</div>
			<div class="panel-body">

				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th><?php echo $this->Paginator->sort('line_cd', '路線コード'); ?></th>
							<th><?php echo $this->Paginator->sort('line_name', '路線名称（一般）'); ?></th>
							<th><?php echo $this->Paginator->sort('line_name_k', '路線名称（一般・カナ）'); ?></th>
							<th><?php echo $this->Paginator->sort('line_name_h', '路線名称（正式名称）'); ?></th>
							<th><?php echo $this->Paginator->sort('e_sort', '並び順'); ?></th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($lines as $line) : ?>
						<tr>
							<td><?php echo $line[$model]['line_cd']; ?></td>
							<td><?php echo $line[$model]['line_name']; ?></td>
							<td><?php echo $line[$model]['line_name_k']; ?></td>
							<td><?php echo $line[$model]['line_name_h']; ?></td>
							<td><?php echo $line[$model]['line_cd']; ?></td>
							<td>
								<ul class="list-inline">
									<li><?php echo $this->Html->link('編集', array('action' => 'edit', $line[$model]['line_cd'])); ?></li>
									<li><?php echo $this->Form->postLink('削除', array('action' => 'delete', $line[$model]['line_cd']), '', array('本当に削除しますか？元には戻せません')); ?></li>
								</ul>
							</td>
						</tr>
						<?php endforeach ?>
					</tbody>
				</table>
				<div class="text-center">
					<?php echo $this->Paginator->pagination(array('ul' => 'pagination')); ?>
				</div>
			</div>
		</div>

	</div>
</div>