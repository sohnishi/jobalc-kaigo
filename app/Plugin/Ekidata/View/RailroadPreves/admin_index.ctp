<div class="row">
	<div class="col-xs-12 contents-actions">
		<ul class="nav nav-tabs">
			<li class="active"><a href="javascript:void(0);">都道府県一覧</a></li>
			<li><?php echo $this->Html->link('CSVインポート', array('action' => 'import')); ?></li>
		</ul>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">

		<div class="panel panel-default">
			<div class="panel-heading">都道府県一覧</div>
			<div class="panel-body">
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th><?php echo $this->Paginator->sort('pref_cd', '都道府県コード'); ?></th>
							<th><?php echo $this->Paginator->sort('pref_name', '都道府県名'); ?></th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($prefs as $pref) : ?>
						<tr>
							<td><?php echo $pref[$model]['pref_cd']; ?></td>
							<td><?php echo $pref[$model]['pref_name']; ?></td>
							<td>
								<ul class="list-inline">
									<li><?php echo $this->Html->link('編集', array('action' => 'edit', $pref[$model]['pref_cd'])); ?></li>
									<li><?php echo $this->Form->postLink('削除', array('action' => 'delete', $pref[$model]['pref_cd'])); ?></li>
								</ul>
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
				<div class="text-center">
					<?php echo $this->Paginator->pagination(array('ul' => 'pagination')); ?>
				</div>
			</div>
		</div>

	</div>
</div>