<div class="row">
	<div class="col-xs-12 contents-actions">
		<ul class="nav nav-tabs">
			<li><?php echo $this->Html->link('都道府県一覧', array('action' => 'index')); ?></li>
			<li class="active"><a href="javascript:void(0);">都道府県の更新</a></li>
		</ul>
	</div>
</div>

<div class="row">
	<div class="col-xs-4 col-xs-offset-4">

		<div class="panel panel-default">
			<div class="panel-heading">都道府県名の更新</div>
			<?php
				echo $this->Form->create($model, array(
					'inputDefaults' => array(
						'div' => 'form-group',
						'class' => 'form-control'
					)
				));
			?>
			<div class="panel-body">

				<?php echo $this->Form->input('pref_cd'); ?>
				<div class="form-group">
					<?php echo $this->Form->input('pref_name', array('label' => '都道府県名')); ?>
				</div>

			</div>
			<div class="panel-footer">
				<?php echo $this->Form->submit('都道府県名を更新する', array('class' => 'btn btn-primary')); ?>
			</div>
			<?php echo $this->Form->end(); ?>
		</div>

	</div>
</div>