<?php
	// 施設オプション
	$kangokijun_options = Configure::read('kangokijun_options');
	$kyukyushitei_options = Configure::read('kyukyushitei_options');
	$karute_options = Configure::read('karute_options');
	// 求人オプション
	$shikaku_options = Configure::read('shikaku');
	$kinmu_keitai_options = Configure::read('kinmu_keitai');
?>

<?php
$this->start('page-top');
	$title = $detail['Facility']['shisetsumei'] . 'の求人一覧';
	$this->Html->addCrumb($title);
	echo $this->Html->getCrumbList(array('lastClass' => 'active', 'class' => 'breadcrumb'), 'トップ');
$this->end();
?>

<article class="recruit-information">
	<div class="facility-data">
		<div class="row">
			<div class="col-xs-12">
				<hgroup>
					<h2>
						<?php
							if ($detail['FacilityShow']['groupmei_chk'] == 1) {
								echo empty($detail['Facility']['group']) ? '' : $detail['Facility']['group'] . '&nbsp;';
							}
							if ($detail['FacilityShow']['houjinmei_chk'] == 1) {
								echo empty($detail['Facility']['corporation']) ? '' : $detail['Facility']['corporation'] . '&nbsp;';
							}
						?>
					</h2>
					<h3><?php echo $detail['Facility']['shisetsumei']; ?></h3>
				</hgroup>
				<div class="address">
					<?php if (!empty($detail['Facility']['todoufuken']) || !empty($detail['Facility']['shikutyouson'])) : ?>
						<i class="fa fa-circle text-primary"></i>&nbsp;
						<?php echo !empty($detail['Facility']['todoufuken']) ? $detail['Facility']['todoufuken'] : ''; ?>
						<?php echo !empty($detail['Facility']['shikutyouson']) ? $detail['Facility']['shikutyouson'] : ''; ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6">
				<div class="facility-image">
					<?php
						$facilityImg = !empty($detail['Facility']['facility_image']) ? '/files/facility/facility_image/' . $detail['Facility']['facility_dir'] . '/' . $detail['Facility']['facility_image'] : 'no_image.jpg';
						echo $this->Html->image($facilityImg, array('class' => 'img-responsive'));
					?>
				</div>
			</div>
			<div class="col-xs-6">
				<p class="description"><?php echo !empty($detail['Facility']['kanrenshisetsu']) ? nl2br($detail['Facility']['kanrenshisetsu']) : ''; ?></p>
			</div>
		</div>
	</div>

	<h2 class="page-header">施設詳細</h2>

	<div id="facility-detail">
		<table class="table table-bordered">
			<tbody>
				<tr>
					<th>郵便番号</th>
					<td><?php echo !empty($detail['Facility']['zip']) ? '〒' . preg_replace("/^(\d{3})(\d{4})$/", "$1-$2", $detail['Facility']['zip']) : ''; ?></td>
				</tr>
				<tr>
					<th>所在地</th>
					<td>
						<?php echo $detail['Facility']['todoufuken']; ?>
						<?php echo $detail['Facility']['shikutyouson']; ?>
						<?php echo $detail['Facility']['banchi']; ?>
						<?php echo $detail['Facility']['tatemono']; ?>
					</td>
				</tr>
				<tr>
					<th>最寄り駅</th>
					<td>
						<ul class="list-unstyled">
							<?php foreach ($detail['Roseneki'] as $roseneki) : ?>
							<li>
								<?php echo isset($roseneki['RailroadLine']['line_name']) ? $roseneki['RailroadLine']['line_name'] : ''; ?>
								<?php echo isset($roseneki['RailroadStation']['station_name']) ? $roseneki['RailroadStation']['station_name'] . '駅' : ''; ?>
							</li>
							<?php endforeach; ?>
						</ul>
					</td>
				</tr>
				<tr>
					<th>電話番号</th>
					<td><?php echo $detail['Facility']['tel']; ?></td>
				</tr>
				<tr>
					<th>FAX番号</th>
					<td><?php echo $detail['Facility']['fax']; ?></td>
				</tr>
				<?php if (!empty($detail['FacilityShow']['shisetsu_url_chk'])) : ?>
				<tr>
					<th>ホームページ</th>
					<td><?php echo $this->Html->link($detail['Facility']['shisetsu_url'], $detail['Facility']['shisetsu_url'], array('target' => '_blank')); ?></td>
				</tr>
				<?php endif; ?>
				<?php if (!empty($detail['FacilityShow']['saiyou_url_chk'])) : ?>
				<tr>
					<th>採用ページ</th>
					<td><?php echo $this->Html->link($detail['Facility']['saiyou_url'], $detail['Facility']['saiyou_url'], array('target' => '_blank')); ?></td>
				</tr>
				<?php endif; ?>

				<?php if (!empty($detail['FacilityShow']['shinryoukamoku_chk'])) : ?>
				<tr>
					<th>診療科目</th>
					<td>
						<?php
							$tmp = $detail['Shinryoukamoku'];
							foreach ($detail['Shinryoukamoku'] as $shinryoukamoku) {
								echo '<span>' . $shinryoukamoku['name'] . '</span>';
								if (next($tmp)) {
									echo '、';
								}
							}
						?>
					</td>
				</tr>
				<?php endif; ?>

				<?php if (!empty($detail['FacilityShow']['byousyou_chk'])) : ?>
				<tr>
					<th>病床数</th>
					<td><?php echo !empty($detail['Facility']['byousyou']) ? $detail['Facility']['byousyou'] . '床' : ''; ?></td>
				</tr>
				<?php endif; ?>

				<?php if (!empty($detail['FacilityShow']['byoutousyousai_chk'])) : ?>
				<tr>
					<th>病棟詳細</th>
					<td><?php echo !empty($detail['Facility']['byoutousyousai']) ? nl2br($detail['Facility']['byoutousyousai']) : ''; ?></td>
				</tr>
				<?php endif; ?>

				<?php if (!empty($detail['FacilityShow']['kangokijun_chk'])) : ?>
				<tr>
					<th>看護基準</th>
					<td><?php echo $kangokijun_options[$detail['Facility']['kangokijun']]; ?></td>
				</tr>
				<?php endif; ?>

				<?php if (!empty($detail['FacilityShow']['kyukyushitei_chk'])) : ?>
				<tr>
					<th>救急指定</th>
					<td><?php echo $kyukyushitei_options[$detail['Facility']['kyukyushitei']]; ?></td>
				</tr>
				<?php endif; ?>

				<?php if (!empty($detail['FacilityShow']['kyukyusyousai_chk'])) : ?>
				<tr>
					<th>救急指定詳細</th>
					<td><?php echo !empty($detail['Facility']['kyukyusyousai']) ? nl2br($detail['Facility']['kyukyusyousai']) : ''; ?></td>
				</tr>
				<?php endif; ?>

				<?php if (!empty($detail['FacilityShow']['syujyutsusyousai_chk'])) : ?>
				<tr>
					<th>手術詳細</th>
					<td><?php echo !empty($detail['Facility']['syujyutsusyousai']) ? nl2br($detail['Facility']['syujyutsusyousai']) : ''; ?></td>
				</tr>
				<?php endif; ?>

				<?php if (!empty($detail['FacilityShow']['karute_chk'])) : ?>
				<tr>
					<th>カルテ</th>
					<td><?php echo $karute_options[$detail['Facility']['karute']]; ?></td>
				</tr>
				<?php endif; ?>

				<?php if (!empty($detail['FacilityShow']['syokuinkousei_chk'])) : ?>
				<tr>
					<th>関連施設</th>
					<td><?php echo !empty($detail['Facility']['syokuinkousei']) ? nl2br($detail['Facility']['syokuinkousei']) : ''; ?></td>
				</tr>
				<?php endif; ?>

				<?php if (!empty($detail['FacilityShow']['kanrenshisetsu_chk'])) : ?>
				<tr>
					<th>HP案内文</th>
					<td><?php echo !empty($detail['Facility']['kanrenshisetsu']) ? nl2br($detail['Facility']['kanrenshisetsu']) : ''; ?></td>
				</tr>
				<?php endif; ?>

				<?php if (!empty($detail['FacilityShow']['kyouikukensyu_chk'])) : ?>
				<tr>
					<th>その他情報</th>
					<td><?php echo !empty($detail['Facility']['kyouikukensyu']) ? nl2br($detail['Facility']['kyouikukensyu']) : ''; ?></td>
				</tr>
				<?php endif; ?>
			</tbody>
		</table>
	</div>

	<div class="section hidden-xs">
		<div class="action text-center">
			<a href="<?php echo $this->Html->url('/forms'); ?>" class="btn btn-lg"><span class="free">無料</span>この施設の求人について問い合わせる</a>
		</div>
	</div>

	<div class="section visible-xs">
		<div class="action">
			<a href="tel:<?php echo str_replace("-", "", $detail['Facility']['tel']); ?>" class="btn btn-lg btn-block"><span class="phone"><i class="fa fa-phone"></i></span>電話で施設の求人を問い合わせる<span class="arrow fa fa-chevron-right"></span></a>
			<a href="<?php echo $this->Html->url('/forms'); ?>" class="btn btn-lg btn-block"><span class="free">無料</span>施設の求人について問い合わせる<span class="arrow fa fa-chevron-right"></span></a>
		</div>
	</div>

	<h2 class="page-header"><i class="fa fa-heart"></i> <?php echo $detail['Facility']['shisetsumei']; ?>の求人情報<?php echo sprintf('（全%s件）', $detail['Facility']['recruit_count']); ?></h2>

	<?php foreach ($detail['Recruit'] as $recruit) : ?>
	<div class="recruit-item">
		<div class="row">
			<div class="col-xs-7">
				<?php if ($recruit['RecruitShow']['shikaku_chk']) : ?>
				<dl>
					<dt><span>資　　格：</span></dt>
					<dd><?php echo $shikaku_options[$recruit['shikaku']]; ?></dd>
				</dl>
				<?php endif; ?>
				<?php if ($recruit['RecruitShow']['busyo_chk']) : ?>
				<dl>
					<dt><span>部　　署：</span></dt>
					<dd><?php echo $recruit['ShisetsukeitaiSecond']['name']; ?></dd>
				</dl>
				<?php endif; ?>
				<?php if ($recruit['RecruitShow']['kinmu_keitai_chk']) : ?>
				<dl>
					<dt><span>勤務形態：</span></dt>
					<dd><?php echo $kinmu_keitai_options[$recruit['kinmu_keitai']]; ?></dd>
				</dl>
				<?php endif; ?>
				<?php if ($recruit['RecruitShow']['kyuyo_chk']) : ?>
				<dl>
					<dt><span>給与(月給)：</span></dt>
					<dd style="margin-left:85px;"><?php echo nl2br($recruit['kyuyo']); ?><dd>
				</dl>
				<?php endif; ?>
				<div class="arrow visible-xs">
					<span class="fa fa-chevron-right"></span>
				</div>
			</div>
			<div class="col-xs-5 text-right">
				<a href="<?php echo $this->Html->url('/recruits/detail/' . $recruit['id']); ?>" class="recruit-btn btn btn-primary btn-lg hidden-xs">この求人の詳細を見る</a>
			</div>
		</div>
		<a class="sp-link" href="<?php echo $this->Html->url('/recruits/detail/' . $recruit['id']); ?>"></a>
	</div>
	<?php endforeach; ?>

</article>

<div class="section visible-xs">
	<div class="spacer-20"></div>
	<a href="#contents" class="btn btn-default btn-block">ページトップへ戻る</a>
</div>

<?php

$this->start('sidebox-pc');
echo $this->element('support');
echo $this->element('search_link');
echo $this->element('guide');
echo $this->element('blog');
echo $this->element('search_form');
echo $this->element('categories');
echo $this->element('sidebar_link');
$this->end();

$this->start('sidebox-sp');
$this->end();
