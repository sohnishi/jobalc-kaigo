<?php
	echo $this->Html->script('ajaxzip3', array('inline' => false));
	echo $this->Html->css('datepicker3', array('inline' => false));
	echo $this->Html->script('bootstrap-datepicker', array('inline' => false));
	echo $this->Html->script('bootstrap-datepicker.ja', array('inline' => false));
	echo $this->Html->script('typeahead.bundle.min', array('inline' => false));

	// 各オプション値
	$keiyaku_options = Configure::read('keiyaku_options');
	$henkinkitei_options = Configure::read('henkinkitei_options');
	$kangokijun_options = Configure::read('kangokijun_options');
	$kyukyushitei_options = Configure::read('kyukyushitei_options');
	$karute_options = Configure::read('karute_options');
	$show_options = Configure::read('facility_show_options');

	echo $this->element('add_edit_script');

	/*
		20190614 sohnishi
		自動かな入力
	*/
	echo $this->Html->script('Forms.jquery.autoKana.js');
?>
<?php echo $this->Html->scriptStart(); ?>
$(document).ready(function () {
	AjaxZip3.JSONDATA = '<?php echo $this->Html->url('/js/zipdata'); ?>';
});
<?php echo $this->Html->scriptEnd(); ?>

<div class="row">
	<div class="col-xs-12 contents-actions">
		<ul class="nav nav-tabs">
			<li><?php echo $this->Html->link('施設検索', array('action' => 'index', '?' => $this->Session->read('querystring'))); ?></li>
			<li class="active"><a href="javascript:void(0);">施設を更新</a></li>
			<li><?php echo $this->Html->link('施設を表示', Hash::merge(array('action' => 'view'), $this->request->params['named'], array('?' => $this->Session->read('querystring')))); ?></li>
		</ul>
	</div>
</div>

<?php
	echo $this->Form->create($model, array(
		'inputDefaults' => array(
			'label' => false,
			'wrapInput' => false,
			'div' => false,
			'class' => 'form-control'
		),
		'id' => 'facilities-form',
		'type' => 'file'
	));
	echo $this->Form->input('id');
?>
<div class="row">
	<div class="col-xs-6">

		<div class="panel panel-default">
			<div class="panel-heading">施設基本情報</div>

			<table class="table table-bordered">
					<tbody>
						<tr>
							<th><?php echo $this->Form->label('group', 'グループ名'); ?></th>
							<td colspan="2">
								<div class="row">
									<div class="col-xs-6">
										<?php echo $this->Form->input('group', array('type' => 'text')); ?>
									</div>
									<div class="col-xs-6">
										<span class='help-block'>2文字以上入力すると自動補完されます</span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<th><?php echo $this->Form->label('group_hurigana', 'グループ名ふりがな'); ?></th>
							<td colspan="2"><?php echo $this->Form->input('group_hurigana'); ?></td>
						</tr>
						<tr>
							<th><?php echo $this->Form->label('corporation', '法人名'); ?></th>
							<td colspan="2">
								<div class="row">
									<div class="col-xs-6">
										<?php echo $this->Form->input('corporation', array('type' => 'text')); ?>
									</div>
									<div class="col-xs-6">
										<span class='help-block'>2文字以上入力すると自動補完されます</span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<th><?php echo $this->Form->label('corporation_hurigana', '法人名ふりがな'); ?></th>
							<td colspan="2"><?php echo $this->Form->input('corporation_hurigana'); ?></td>
						</tr>
						<tr>
							<th><?php echo $this->Form->label('shisetsumei', '施設名 <span class="req">*</span>'); ?></th>
							<td colspan="2"><?php echo $this->Form->input('shisetsumei'); ?></td>
						</tr>
						<tr>
							<th><?php echo $this->Form->label('shisetsumei_hurigana', '施設名ふりがな'); ?></th>
							<td colspan="2"><?php echo $this->Form->input('shisetsumei_hurigana'); ?></td>
						</tr>
						<tr>
							<th><?php echo $this->Form->label('ShisetsukeitaiFirst', '施設形態 - 大区分'); ?></th>
							<td colspan="2"><?php echo $this->Form->input('ShisetsukeitaiFirst', array('type' => 'select', 'multiple' => 'checkbox', 'class' => 'checkbox-inline')); ?></td>
						</tr>
						<tr>
							<th><?php echo $this->Form->label('ShisetsukeitaiSecond', '施設形態 - 中区分'); ?></th>
							<td colspan="2"><?php echo $this->Form->input('ShisetsukeitaiSecond', array('type' => 'select', 'multiple' => 'checkbox', 'class' => 'checkbox-inline')); ?></td>
						</tr>
						<tr>
							<th><?php echo $this->Form->label('zip', '郵便番号'); ?></th>
							<td colspan="2">
								<div class="row">
									<div class="col-xs-4">
										<?php echo $this->Form->input('zip', array('onKeyup' => 'AjaxZip3.zip2addr(this, "", "data[Facility][todoufuken]", "data[Facility][shikutyouson]");')); ?>
									</div>
									<span class="help-block">ハイフン「-」無し7桁の半角数字で入力してください</span>
								</div>
							</td>
						</tr>
						<tr>
							<th><?php echo $this->Form->label('todoufuken', '都道府県'); ?></th>
							<td colspan="2">
								<div class="row">
									<div class="col-xs-6">
										<?php echo $this->Form->input('todoufuken'); ?>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<th><?php echo $this->Form->label('shikutyouson', '市区町村'); ?></th>
							<td colspan="2">
								<div class="row">
									<div class="col-xs-6">
										<?php echo $this->Form->input('shikutyouson'); ?>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<th><?php echo $this->Form->label('banchi', '番地'); ?></th>
							<td colspan="2">
								<div class="row">
									<div class="col-xs-6">
										<?php echo $this->Form->input('banchi'); ?>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<th><?php echo $this->Form->label('tatemono', '建物名'); ?></th>
							<td colspan="2"><?php echo $this->Form->input('tatemono'); ?></td>
						</tr>
						<tr>
							<th><?php echo $this->Form->label('shisetsu_url', '施設ホームページ'); ?></th>
							<td colspan="2">
								<?php echo $this->Form->input('shisetsu_url'); ?>
								<span class="help-block">http:// から正確に入力してください</span>
							</td>
						</tr>
						<tr>
							<th><?php echo $this->Form->label('saiyou_url', '採用情報URL'); ?></th>
							<td colspan="2">
								<?php echo $this->Form->input('saiyou_url'); ?>
								<span class="help-block">http:// から正確に入力してください</span>
							</td>
						</tr>
					</tbody>
				</table>

		</div>

		<div class="panel panel-default">
			<div class="panel-heading">最寄り路線・最寄り駅</div>
			<div class="panel-body">
				<div class="alert alert-info"><p>最寄り路線・最寄り駅は施設表示画面より追加・更新を行ってください</p></div>
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading">契約情報</div>
			<table class="table table-bordered">
				<tbody>
					<tr>
						<th><?php echo $this->Form->label('number', '施設番号'); ?></th>
						<td>
							<div class="row">
								<div class="col-xs-4">
									<?php echo $this->request->data['Facility']['number']; ?>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<th><?php echo $this->Form->label('keiyaku', '契約状態'); ?></th>
						<td>
							<?php echo $this->Form->input('keiyaku', array('type' => 'radio', 'options' => $keiyaku_options, 'label' => true, 'legend' => false, 'class' => 'radio-inline')); ?>
						</td>
					</tr>
					<tr>
						<th><?php echo $this->Form->label('keiyakubi', '契約日'); ?></th>
						<td>
							<div class="row">
								<div class="col-xs-4">
									<?php echo $this->Form->input('keiyakubi', array('type' => 'text')); ?>
								</div>
								<span class="help-block">yyyy-mm-dd 形式で入力してください</span>
							</div>
						</td>
					</tr>
					<tr>
						<th><?php echo $this->Form->label('tesuryou', '手数料'); ?></th>
						<td>
							<div class="row">
								<div class="col-xs-4">
									<?php echo $this->Form->input('tesuryou', array('type' => 'text', 'beforeInput' => '<div class="input-group">', 'afterInput' => '<span class="input-group-addon">%</span></div>')); ?>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<th><?php echo $this->Form->label('henkinkitei', '返金規定'); ?></th>
						<td><?php echo $this->Form->input('henkinkitei', array('type' => 'radio', 'options' => $henkinkitei_options, 'legend' => false, 'label' => true, 'class' => 'radio-inline')); ?></td>
					</tr>
					<tr>
						<th><?php echo $this->Form->label('henkinkiteisyousai', '契約詳細'); ?></th>
						<td><?php echo $this->Form->input('henkinkiteisyousai', array('rows' => 3)); ?></td>
					</tr>
					<tr>
						<th>施設画像</th>
						<?php if (empty($this->request->data['Facility']['facility_image'])) : ?>
						<td>
							<?php echo $this->Form->input('facility_image', array('type' => 'file', 'class' => false)); ?>
							<?php echo $this->Form->input('facility_dir', array('type' => 'hidden')); ?>
							<span class="help-block">ファイルサイズは2MB以下でアップロードできます</span>
						</td>
						<?php else : ?>
						<td>
							<div class="row">
								<div class="col-xs-6">
									<!-- 
										20190522 sohnishi
										small画像のパスが切れていたため、元画像を表示してmax-height,max-widthを表示するように変更
									-->
									<?php echo $this->Html->image('/files/facility/facility_image/' . $this->request->data['Facility']['facility_dir'] . '/' . $this->request->data['Facility']['facility_image'], array('class' => 'img-responsive', 'style' => 'max-height: 180px;max-width: 180px;')); ?>
								</div>
								<div class="col-xs-6">
									<?php echo $this->Form->input('Facility.facility_image.remove', array('type' => 'checkbox', 'class' => 'checkbox-inline', 'label' => '施設画像を削除する')); ?>
									<span class="help-block">画像変更は一度削除してください</span>
								</div>
							</div>
						</td>
						<?php endif; ?>
					</tr>
					<tr>
						<th><?php echo $this->Form->label('tesuryousyousai', 'メモ'); ?></th>
						<td><?php echo $this->Form->input('tesuryousyousai', array('rows' => 3)); ?></td>
					</tr>
				</tbody>
			</table>
		</div>

	</div>

	<div class="col-xs-6">
		<div class="panel panel-default">
			<div class="panel-heading">連絡先情報</div>
			<table class="table table-bordered">
					<tbody>
						<tr>
							<th><?php echo $this->Form->label('tel', '施設TEL'); ?></th>
							<td>
								<div class="row">
									<div class="col-xs-6">
										<?php echo $this->Form->input('tel'); ?>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<th><?php echo $this->Form->label('fax', '施設FAX'); ?></th>
							<td>
								<div class="row">
									<div class="col-xs-6">
										<?php echo $this->Form->input('fax'); ?>
									</div>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading">担当者情報</div>
			<div class="panel-body">
				<div class="alert alert-info"><p>担当者は施設表示画面より追加・更新を行ってください</p></div>
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading">連絡履歴</div>
			<div class="panel-body">
				<div class="alert alert-info"><p>連絡履歴は施設表示画面より追加・更新を行ってください</p></div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-default">
			<div class="panel-heading">施設詳細情報</div>
			<div class="panel-body">

				<div class="row">
					<div class="col-xs-6">
						<table class="table table-bordered">
							<tbody>
								<tr>
									<th><?php echo $this->Form->label('Shinryoukamoku', '診療科目'); ?></th>
									<td><?php echo $this->Form->input('Shinryoukamoku', array('type' => 'select', 'multiple' => 'checkbox', 'class' => 'checkbox-inline')); ?></td>
								</tr>
								<tr>
									<th><?php echo $this->Form->label('byousyou', '病床数'); ?></th>
									<td>
										<div class="row">
											<div class="col-xs-3">
												<?php echo $this->Form->input('byousyou', array('type' => 'text', 'beforeInput' => '<div class="input-group">', 'afterInput' => '<span class="input-group-addon">床</span></div>')); ?>
											</div>
											<span class="help-block">半角数字で入力してください</span>
										</div>
									</td>
								</tr>
								<tr>
									<th><?php echo $this->Form->label('kangokijun', '看護基準'); ?></th>
									<td>
										<div class="row">
											<div class="col-xs-4">
												<?php echo $this->Form->input('kangokijun', array('type' => 'select', 'options' => $kangokijun_options)); ?>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<th><?php echo $this->Form->label('karute', 'カルテ'); ?></th>
									<td>
										<div class="row">
											<div class="col-xs-4">
												<?php echo $this->Form->input('karute', array('type' => 'select', 'options' => $karute_options)); ?>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<th><?php echo $this->Form->label('kyukyushitei', '救急指定'); ?></th>
									<td>
										<div class="row">
											<div class="col-xs-4">
												<?php echo $this->Form->input('kyukyushitei', array('type' => 'select', 'options' => $kyukyushitei_options)); ?>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<th><?php echo $this->Form->label('kyukyusyousai', '救急指定詳細'); ?></th>
									<td><?php echo $this->Form->input('kyukyusyousai', array('rows' => 3)); ?></td>
								</tr>
							</tbody>
						</table>
					</div>

					<div class="col-xs-6">
						<table class="table table-bordered">
							<tbody>
								<tr>
									<th><?php echo $this->Form->label('byoutousyousai', '病棟詳細'); ?></th>
									<td><?php echo $this->Form->input('byoutousyousai', array('rows' => 3)); ?></td>
								</tr>
								<tr>
									<th><?php echo $this->Form->label('syujyutsusyousai', '手術詳細'); ?></th>
									<td><?php echo $this->Form->input('syujyutsusyousai', array('rows' => 3)); ?></td>
								</tr>
								<tr>
									<!-- 関連施設=>職員構成 -->
									<th><?php echo $this->Form->label('syokuinkousei', '職員構成'); ?></th>
									<td><?php echo $this->Form->input('syokuinkousei', array('rows' => 3)); ?></td>
								</tr>
								<tr>
									<th><?php echo $this->Form->label('tokutyou', '施設特徴'); ?></th>
									<td><?php echo $this->Form->input('tokutyou', array('rows' => 3)); ?></td>
								</tr>
								<tr>
									<!-- HP案内文=>関連施設 -->
									<th><?php echo $this->Form->label('kanrenshisetsu', '関連施設'); ?></th>
									<td><?php echo $this->Form->input('kanrenshisetsu', array('rows' => 3)); ?></td>
								</tr>
								<tr>
									<th><?php echo $this->Form->label('kyouikukensyu', 'その他情報'); ?></th>
									<td><?php echo $this->Form->input('kyouikukensyu', array('rows' => 3)); ?></td>
								</tr>
								<!-- 
									20190508 sohnishi
									施設予備項目を追加
									$index：配列作成用のインデックス アソシエーションモデルのデータを同時に保存するために配列構造にする必要がある
									$facilityPreliminaryItems：施設予備項目マスタリスト
									$facilitiesFacilityPreliminaryItems：施設 - 施設予備項目 中間テーブルの値リスト　キー：施設予備項目マスタID　値：中間テーブルCONTENT
								 -->
								<?php
									$index = 0;
									foreach($facilityPreliminaryItems as $id => $facilityPreliminaryItem) {
								?>
										<tr>

											<th><?php echo $this->Form->label('FacilitiesFacilityPreliminaryItem', $facilityPreliminaryItem); ?></th>
											<?php echo $this->Form->input('FacilityPreliminaryItem.' . $index . '.facility_preliminary_item_id', array('type' => 'hidden', 'value' => $id)); ?>
											<!-- issetで存在チェック -->
											<td><?php echo $this->Form->input('FacilityPreliminaryItem.' . $index . '.content', array('rows' => 3, 'value' => isset($facilitiesFacilityPreliminaryItems[$id])?$facilitiesFacilityPreliminaryItems[$id]:'')); ?></td>
										</tr>
								<?php
									$index += 1;
									}
								?>
							</tbody>
						</table>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-default">
			<div class="panel-heading">掲載オプション</div>
			<div class="panel-body">

				<?php echo $this->Form->input('FacilityShow.id'); ?>
				<div class="form-inline">
					<?php echo $this->Form->input('FacilityShow.groupmei_chk', array('label' => $show_options['groupmei'], 'class' => false)); ?>
					<?php echo $this->Form->input('FacilityShow.houjinmei_chk', array('label' => $show_options['houjinmei'], 'class' => false)); ?>
					<?php echo $this->Form->input('FacilityShow.shinryoukamoku_chk', array('label' => $show_options['shinryoukamoku'], 'class' => false)); ?>
					<?php echo $this->Form->input('FacilityShow.byousyou_chk', array('label' => $show_options['byousyou'], 'class' => false)); ?>
					<?php echo $this->Form->input('FacilityShow.byoutousyousai_chk', array('label' => $show_options['byoutousyousai'], 'class' => false)); ?>
					<?php echo $this->Form->input('FacilityShow.kangokijun_chk', array('label' => $show_options['kangokijun'], 'class' => false)); ?>
					<?php echo $this->Form->input('FacilityShow.kyukyushitei_chk', array('label' => $show_options['kyukyushitei'], 'class' => false)); ?>
					<?php echo $this->Form->input('FacilityShow.kyukyusyousai_chk', array('label' => $show_options['kyukyusyousai'], 'class' => false)); ?>
					<?php echo $this->Form->input('FacilityShow.syujyutsusyousai_chk', array('label' => $show_options['syujyutsusyousai'], 'class' => false)); ?>
					<?php echo $this->Form->input('FacilityShow.karute_chk', array('label' => $show_options['karute'], 'class' => false)); ?>
					<?php echo $this->Form->input('FacilityShow.syokuinkousei_chk', array('label' => $show_options['syokuinkousei'], 'class' => false)); ?>
					<?php echo $this->Form->input('FacilityShow.tokutyou_chk', array('label' => $show_options['tokutyou'], 'class' => false)); ?>
					<?php echo $this->Form->input('FacilityShow.kanrenshisetsu_chk', array('label' => $show_options['kanrenshisetsu'], 'class' => false)); ?>
					<?php echo $this->Form->input('FacilityShow.kyouikukensyu_chk', array('label' => $show_options['kyouikukensyu'], 'class' => false)); ?>
					<?php echo $this->Form->input('FacilityShow.shisetsu_url_chk', array('label' => $show_options['shisetsu_url'], 'class' => false)); ?>
					<?php echo $this->Form->input('FacilityShow.saiyou_url_chk', array('label' => $show_options['saiyou_url'], 'class' => false)); ?>
					<?php echo $this->Form->input('FacilityShow.facility_tel_chk', array('label' => $show_options['facility_tel'], 'class' => false)); ?>
					<?php echo $this->Form->input('FacilityShow.facility_fax_chk', array('label' => $show_options['facility_fax'], 'class' => false)); ?>
					<!-- 
						20190508 sohnishi
						施設予備項目を追加
						$index: 配列作成用のインデックス アソシエーションモデルのデータを同時に保存するために配列構造にする必要がある
					-->
					<?php
						$index = 0;
						foreach($facilityPreliminaryItems as $id => $facilityPreliminaryItem) {
					?>
						<?php echo $this->Form->input('FacilityPreliminaryItem.' . $index . '.is_show', array('type' => 'checkbox', 'label' => $facilityPreliminaryItem, 'class' => false, 'default' => !empty($facility[0]['FacilityPreliminaryItem']) ? $facility[0]['FacilityPreliminaryItem'][$index]['FacilitiesFacilityPreliminaryItem']['is_show'] : 0)); ?>
					<?php
							$index += 1;
						}
					?>
				</div>
				<span class="help-block">求人情報ごとに掲載の有無をチェックできます</span>
			</div>
		</div>
	</div>
</div>

<div class="row" >
	<div class="col-xs-12">
		<?php echo $this->Form->submit('施設情報を更新する', array('class' => 'btn btn-primary')); ?>
	</div>
</div>

<?php echo $this->Form->end(); ?>
<!-- 
20190614 sohnishi
自動かな入力
-->
<script>
$.fn.autoKana('#FacilityShisetsumei', '#FacilityShisetsumeiHurigana', {katakana: false});
</script>
