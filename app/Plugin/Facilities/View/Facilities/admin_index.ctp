<?php
	echo $this->Html->css('bootstrap-multiselect', array('inline' => false));
	echo $this->Html->script('bootstrap-multiselect', array('inline' => false));
	echo $this->Html->script('ajaxzip3', array('inline' => false));
	echo $this->Html->script('typeahead.bundle.min', array('inline' => false));

	$keiyaku_options = Configure::read('keiyaku_options');
	$kangokijun_options = Configure::read('kangokijun_options');
	$kyukyushitei_options = Configure::read('kyukyushitei_options');
	$karute_options = Configure::read('karute_options');

	// 最寄り路線・駅
	$datas = array(
		'lineEmptyText' => '最寄り路線を選択',
		'stationEmptyText' => '最寄り駅を選択',
		'prefId' => '#FacilityPrefCd',
		'lineId' => '#FacilityLineCd',
		'stationId' => '#FacilityStationCd'
	);
	echo $this->element('interface', $datas, array('plugin' => 'Ekidata'));
?>
<?php echo $this->Html->scriptStart(); ?>
$(document).ready(function () {
	AjaxZip3.JSONDATA = '<?php echo $this->Html->url('/js/zipdata'); ?>';
});
<?php echo $this->Html->scriptEnd(); ?>

<?php echo $this->Html->scriptStart(); ?>
$(function () {
	$('.collapse').on('hidden.bs.collapse', function () {
		$(this).parent().find(".fa-minus").removeClass("fa-minus").addClass("fa-plus");
	}).on('show.bs.collapse', function () {
		$(this).parent().find(".fa-plus").removeClass("fa-plus").addClass("fa-minus");
	});

	var groups = new Bloodhound({
		remote: '<?php echo $this->Html->url(array('plugin' => 'facilities', 'controller' => 'groups', 'action' => 'typeahead', '?' => 'q=%QUERY', 'admin' => true)); ?>',
		datumTokenizer: function (d) {
			return Bloodhound.tokenizers.whitespace(d.name);
		},
		queryTokenizer: Bloodhound.tokenizers.whitespace,
	});

	groups.initialize();

	$('#FacilityGroup').typeahead({
		hint: false,
		highlight: true,
		minLength: 2
	},
	{
		name: 'groups',
		displayKey: 'name',
		source: groups.ttAdapter()
	});

	var corporations = new Bloodhound({
		remote: '<?php echo $this->Html->url(array('plugin' => 'facilities', 'controller' => 'corporations', 'action' => 'typeahead', '?' => 'q=%QUERY', 'admin' => true)); ?>',
		datumTokenizer: function (d) {
			return Bloodhound.tokenizers.whitespace(d.name);
		},
		queryTokenizer: Bloodhound.tokenizers.whitespace,
	});

	corporations.initialize();

	$('#FacilityCorporation').typeahead({
		hint: false,
		highlight: true,
		minLength: 2
	},
	{
		name: 'corporations',
		displayKey: 'name',
		source: corporations.ttAdapter()
	});

	$('#FacilityShisetsukeitaiFirst, #FacilityShisetsukeitaiSecond, #FacilityShinryoukamoku').multiselect({
		nonSelectedText: '選択してください',
		nSelectedText: '選択',
		allSelectedText: '全選択',
		numberDisplayed: 1
	});
});
<?php echo $this->Html->scriptEnd(); ?>

<div class="row">
	<div class="col-xs-12 contents-actions">
		<ul class="nav nav-tabs">
			<li class="active"><a href="javascript:void(0);">施設検索</a></li>
			<li><?php echo $this->Html->link('施設を新規登録', array('action' => 'add')); ?></li>
		</ul>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<a href="#facility-search" data-toggle="collapse" aria-expanded="false" aria-controls="facility-search"><i class="fa fa-plus"></i> 施設を検索</a>
			</div>
			<div class="panel-body collapse" id="facility-search">

				<?php
					echo $this->Form->create($model, array(
						'inputDefaults' => array(
							'label' => false,
							'wrapInput' => false,
							'div' => false,
							'class' => 'form-control'
						),
						'url' => array_merge(
								array(
									'plugin' => 'facilities',
									'controller' => 'facilities',
									'action' => 'index',
									'admin' => true
								),
								$this->params['pass']
						)
					));
				?>

				<div class="row">
					<div class="col-xs-3">
						<div class="form-group">
							<?php echo $this->Form->input('number', array('type' => 'text', 'label' => '施設番号', 'required' => false)); ?>
						</div>
					</div>
					<div class="col-xs-3">
						<div class="form-group">
							<?php echo $this->Form->input('shisetsumei', array('label' => '施設名', 'required' => false)); ?>
						</div>
					</div>
					<div class="col-xs-3">
						<div class="form-group">
							<?php echo $this->Form->input('shisetsumei_hurigana', array('label' => '施設名ふりがな', 'required' => false)); ?>
						</div>
					</div>
					<div class="col-xs-3">
						<div class="form-group">
							<?php echo $this->Form->input('group', array('label' => 'グループ名')); ?>
						</div>
					</div>
					<div class="col-xs-3">
						<div class="form-group">
							<?php echo $this->Form->input('group_hurigana', array('label' => 'グループ名ふりがな')); ?>
						</div>
					</div>
					<div class="col-xs-3">
						<div class="form-group">
							<?php echo $this->Form->input('corporation', array('label' => '法人名')); ?>
						</div>
					</div>
					<div class="col-xs-3">
						<div class="form-group">
							<?php echo $this->Form->input('corporation_hurigana', array('label' => '法人名ふりがな')); ?>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->label('shisetsukeitai_first', '施設形態 - 大区分'); ?>
							<div class="input-group">
								<?php echo $this->Form->input('shisetsukeitai_first', array('type' => 'select', 'multiple' => true)); ?>
							</div>
						</div>
					</div>
					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->label('shisetsukeitai_second', '施設形態 - 中区分'); ?>
							<div class="input-group">
								<?php echo $this->Form->input('shisetsukeitai_second', array('type' => 'select', 'multiple' => true)); ?>
							</div>
						</div>
					</div>
					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->label('shinryoukamoku', '診療科目'); ?>
							<div class="input-group">
								<?php echo $this->Form->input('shinryoukamoku', array('type' => 'select', 'multiple' => true)); ?>
							</div>
						</div>
					</div>
					<div class="col-xs-2">
						<div class="form-group">
							<?php echo $this->Form->label('keiyaku', '契約状態'); ?>
							<div class="input-group">
								<?php echo $this->Form->input('keiyaku', array('type' => 'select', 'options' => $keiyaku_options, 'multiple' => 'checkbox', 'class' => 'checkbox-inline')); ?>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="form-group col-xs-2">
						<?php echo $this->Form->input('zip', array('label' => '郵便番号', 'onKeyup' => 'AjaxZip3.zip2addr(this, "", "data[Facility][todoufuken]", "data[Facility][shikutyouson]");')); ?>
						<span class="help-block">例）0000000</span>
					</div>
					<div class="form-group col-xs-2">
						<?php echo $this->Form->input('todoufuken', array('label' => '都道府県')); ?>
					</div>
					<div class="form-group col-xs-2">
						<?php echo $this->Form->input('shikutyouson', array('label' => '市区町村')); ?>
					</div>
					<div class="form-group col-xs-2">
						<?php echo $this->Form->input('banchi', array('label' => '番地')); ?>
					</div>
					<div class="form-group col-xs-4">
						<?php echo $this->Form->input('tatemono', array('label' => '建物名')); ?>
					</div>
				</div>

				<div class="row">
					<div class="form-group col-xs-2">
						<?php echo $this->Form->input('pref_cd', array('type' => 'select', 'options' => $prefs, 'empty' => '-- 選択してください --', 'label' => '最寄り都道府県')); ?>
					</div>
					<div class="form-group col-xs-2">
						<?php echo $this->Form->input('line_cd', array('type' => 'select', 'options' => $lines, 'empty' => '-- 選択してください --', 'label' => '最寄り路線')); ?>
					</div>
					<div class="form-group col-xs-2">
						<?php echo $this->Form->input('station_cd', array('type' => 'select', 'options' => $stations, 'empty' => '-- 選択してください --', 'label' => '最寄り駅')); ?>
					</div>
					<div class="form-group col-xs-2">
						<?php echo $this->Form->input('kangokijun', array('type' => 'select', 'options' => $kangokijun_options, 'label' => '看護基準', 'empty' => '-- 未選択 --')); ?>
					</div>
					<div class="form-group col-xs-2">
						<?php echo $this->Form->input('kyukyushitei', array('type' => 'select', 'options' => $kyukyushitei_options, 'label' => '救急指定', 'empty' => '-- 未選択 --')); ?>
					</div>
					<div class="form-group col-xs-2">
						<?php echo $this->Form->input('karute', array('type' => 'select', 'options' => $karute_options, 'label' => 'カルテ', 'empty' => '-- 未選択 --')); ?>
					</div>
				</div>

				<!-- 
					20190513 sohnishi
					求人予備項目を追加
					$index: 配列作成用のインデックス アソシエーションモデルのデータを同時に保存するために配列構造にする必要がある
				-->
				<?php
					$index = 0;
					foreach($facilityPreliminaryItems as $id => $facilityPreliminaryItem) {
				?>
						<?php if ($index == 0 || ($index + 1) % 3 == 0) { echo '<div class="row">'; } ?>
							<div class="col-xs-4">
								<div class="form-group">
									<?php echo $this->Form->input('Facility.facility_preliminary_item.' . $index, array('type' => 'text', 'label' => $facilityPreliminaryItem, 'required' => 'false')); ?>
								</div>
							</div>
						<?php if (($index + 1) % 3 == 0 || (count($facilityPreliminaryItems) % 3 != 0 && count($facilityPreliminaryItems) == ($index + 1))) { echo '</div>'; } ?>
				<?php
						$index += 1;
					}
				?>

				<!-- 
					20190514 sohnishi
					連絡履歴を追加
				 -->
				 <h4 class="page-header">連絡履歴</h4>

				<div class="row">

					<div class="col-xs-4">
						<div class="form-group">
							<?php echo $this->Form->input('Facility.syousai', array('type' => 'text', 'label' => '連絡詳細')); ?>
						</div>
					</div>

				</div>

				<hr>

				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> 施設検索</button>
							<a href="<?php echo $this->Html->url(array('action' => 'index')); ?>" class="btn btn-warning"><i class="fa fa-refresh"></i> リセット</a>
						</div>
					</div>
				</div>
				<?php echo $this->Form->end(); ?>

			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">

		<div class="panel panel-default">
			<div class="panel-heading">施設情報一覧</div>
			<div class="panel-body">

				<table class="table table-striped">
					<thead>
						<tr>
							<th><?php echo $this->Paginator->sort('keiyaku', '契約'); ?></th>
							<th><?php echo $this->Paginator->sort('number', '施設番号'); ?></th>
							<th><?php echo $this->Paginator->sort('corporation', '法人名'); ?></th>
							<th><?php echo $this->Paginator->sort('shisetsumei', '施設名'); ?></th>
							<th><?php echo $this->Paginator->sort('todoufuken', '都道府県'); ?></th>
							<th><?php echo $this->Paginator->sort('shikutyouson', '市区町村'); ?></th>
							<th><?php echo $this->Paginator->sort('tel', '施設TEL'); ?></th>
							<th><?php echo $this->Paginator->sort('fax', '施設FAX'); ?></th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($facilities as $i => $facility) : ?>
						<?php
							$paging = $this->request->params['paging'][$model];
							$start = ($paging['page'] - 1) * $paging['limit'] + 1;
							$pageNum = $start + $i;
							$sort = isset($paging['options']['sort']) ? $paging['options']['sort'] : '';
							$viewLink = Hash::merge(
								array('action' => 'view'),
								$this->request->params['named'],
								// 20190708 sohnishi
								// 施設表示でpage:パラメータだと該当の施設が表示されない事象があったため、idに変更
								array('page' => $pageNum),
								// $facility[$model]['id']
								array('?' => $this->request->query)
							);
						?>
						<tr>
							<td><?php echo $facility[$model]['keiyaku']; ?></td>
							<td><?php echo $facility[$model]['number']; ?></td>
							<td><?php echo $facility[$model]['corporation']; ?></td>
							<td><?php echo $this->Html->link($facility[$model]['shisetsumei'], $viewLink); ?></td>
							<td><?php echo $facility[$model]['todoufuken']; ?></td>
							<td><?php echo $facility[$model]['shikutyouson']; ?></td>
							<td><?php echo $facility[$model]['tel']; ?></td>
							<td><?php echo $facility[$model]['fax']; ?></td>
							<td><?php echo date('Y-m-d', strtotime($facility[$model]['modified'])); ?></td>
							<td>
								<ul class="list-inline">
									<li><?php echo $this->Form->postLink('削除', array('action' => 'delete', $facility[$model]['id']), '', '本当に削除しますか?元には戻せません'); ?></li>
								</ul>
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
				<div class="text-center">
					<?php echo $this->Paginator->pagination(array('ul' => 'pagination')); ?>
				</div>

			</div>
		</div>

	</div>
</div>


    <?php //echo $this->element('sql_dump'); ?>
