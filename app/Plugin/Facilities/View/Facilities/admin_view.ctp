<?php
	// オプション値
	$keiyaku_options = Configure::read('keiyaku_options');
	$henkinkitei_options = Configure::read('henkinkitei_options');
	$kangokijun_options = Configure::read('kangokijun_options');
	$karute_options = Configure::read('karute_options');
	$kyukyushitei_options = Configure::read('kyukyushitei_options');

	$show_portal = ' <span class="text-success">*</span>';

	$facility = $facilities[0];

	$datas = array(
		'lineEmptyText' => '最寄り路線を選択',
		'stationEmptyText' => '最寄り駅を選択',
		'prefId' => '#RosenekiPrefCd',
		'lineId' => '#RosenekiLineCd',
		'stationId' => '#RosenekiStationCd'
	);
	echo $this->element('interface', $datas, array('plugin' => 'Ekidata'));

	$rosenekiDatas = array(
		'facilityId' => $facility[$model]['id']
	);
	echo $this->element('view_roseneki_script', $rosenekiDatas);

	$staffDatas = array(
		'facilityId' => $facility[$model]['id']
	);
	echo $this->element('view_staff_script', $staffDatas);

	$contactDatas = array(
		'facilityId' => $facility[$model]['id'],
		'userId' => $currentUser['id'],
		'staffs' => Hash::combine($facility['Staff'], '{n}.id', '{n}.name')
	);
	echo $this->element('view_contact_script', $contactDatas);
?>

<?php echo $this->Html->scriptStart(); ?>
$(function () {

	$('.collapse').on('hidden.bs.collapse', function () {
		$(this).parent().find(".fa-minus").removeClass("fa-minus").addClass("fa-plus");
	}).on('show.bs.collapse', function () {
		$(this).parent().find(".fa-plus").removeClass("fa-plus").addClass("fa-minus");
	});

});
<?php echo $this->Html->scriptEnd(); ?>

<div class="row">
	<div class="col-xs-12 contents-actions">
		<ul class="nav nav-tabs">
			<li><?php echo $this->Html->link('施設検索', array('action' => 'index', '?' => $this->request->query)); ?></li>
			<li><?php echo $this->Html->link('施設を更新', Hash::merge(array('action' => 'edit'), $facility[$model]['id'], $this->request->params['named'], array('?' => $this->Session->read('querystring')))); ?></li>
			<li class="active"><a href="javascript:void(0);">施設を表示</a></li>
			<li><?php echo $this->Html->link('求人を登録', Hash::merge(array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'add'), $facility[$model]['id'], $this->request->params['named'], array('?' => $this->Session->read('querystring')))); ?></li>
		</ul>
	</div>
</div>

<div class="row">
	<div class="col-xs-5">
		<h3><?php echo $facility[$model]['shisetsumei']; ?></h3>
	</div>
	<div class="col-xs-7">
		<ul class="pager">
			<?php echo $this->Paginator->prev('< 前へ', array('class' => 'previous', 'disabled' => 'previous disabled')); ?>
			<li><?php echo $this->Paginator->counter('{:page} / {:pages}'); ?></li>
			<?php echo $this->Paginator->next('次へ >', array('class' => 'next', 'disabled' => 'next disabled')); ?>
		</ul>
	</div>
</div>

<div class="row">
	<div class="col-xs-5">
		<div class="panel panel-default">
			<div class="panel-heading">施設基本情報 [<span class="text-success">*</span>] はポータルサイトに表示</div>
			<table class="table table-bordered table-condensed">
				<tbody>
					<tr>
						<th>グループ名<?php if ($facility['FacilityShow']['groupmei_chk']) { echo $show_portal; } ?></th>
						<td colspan="3"><?php echo $facility[$model]['group']; ?></td>
					</tr>
					<tr>
						<th>グループ名ふりがな</th>
						<td colspan="3"><?php echo $facility[$model]['group_hurigana']; ?></td>
					</tr>
					<tr>
						<th>法人名<?php if ($facility['FacilityShow']['houjinmei_chk']) { echo $show_portal; } ?></th>
						<td colspan="3"><?php echo $facility[$model]['corporation']; ?></td>
					</tr>
					<tr>
						<th>法人名ふりがな</th>
						<td colspan="3"><?php echo $facility[$model]['corporation_hurigana']; ?></td>
					</tr>
					<tr>
						<th>施設名</th>
						<td colspan="3"><?php echo $facility[$model]['shisetsumei']; ?></td>
					</tr>
					<tr>
						<th>施設名ふりがな</th>
						<td colspan="3"><?php echo $facility[$model]['shisetsumei_hurigana']; ?></td>
					</tr>
					<tr>
						<th>施設形態 - 大区分</th>
						<td colspan="3">
							<?php if (!empty($facility['ShisetsukeitaiFirst'])) : ?>
							<?php foreach ($facility['ShisetsukeitaiFirst'] as $shisetsukeitaiFirst) : ?>
							<span>[<?php echo $shisetsukeitaiFirst['name']; ?>]</span>
							<?php endforeach; ?>
							<?php endif; ?>
						</td>
					</tr>
					<tr>
						<th>施設形態 - 中区分</th>
						<td colspan="3">
							<?php if (!empty($facility['ShisetsukeitaiSecond'])) : ?>
							<?php foreach ($facility['ShisetsukeitaiSecond'] as $shisetsukeitaiSecond) : ?>
							<span>[<?php echo $shisetsukeitaiSecond['name']; ?>]</span>
							<?php endforeach; ?>
							<?php endif; ?>
						</td>
					</tr>

					<tr>
						<th>郵便番号</th>
						<td colspan="3"><?php echo preg_replace("/^(\d{3})(\d{4})$/", "$1-$2", $facility[$model]['zip']); ?></td>
					</tr>
					<?php
						$todoufuken = $facility[$model]['todoufuken'];
						$shikutyouson = $facility[$model]['shikutyouson'];
						$banchi = $facility[$model]['banchi'];
						$mapLink = $todoufuken . $shikutyouson . $banchi;
					?>
					<tr>
						<th rowspan="2">住所　<a href="http://www.google.co.jp/maps/place/<?php echo $mapLink; ?>" class="btn btn-primary btn-xs" target="_blank">地図</a></th>
						<td><?php echo $facility[$model]['todoufuken']; ?></td>
						<td><?php echo $facility[$model]['shikutyouson']; ?></td>
						<td><?php echo $facility[$model]['banchi']; ?></td>
					</tr>
					<tr>
						<td colspan="3"><?php echo $facility[$model]['tatemono']; ?></td>
					</tr>

					<tr>
						<th>施設ホームページ<?php if ($facility['FacilityShow']['shisetsu_url_chk']) { echo $show_portal; } ?></th>
						<td colspan="3"><?php echo $this->Html->link($facility[$model]['shisetsu_url'], $facility[$model]['shisetsu_url'], array('target' => '_blank')); ?></td>
					</tr>
					<tr>
						<th>採用情報URL<?php if ($facility['FacilityShow']['saiyou_url_chk']) { echo $show_portal; } ?></th>
						<td colspan="3"><?php echo $this->Html->link($facility[$model]['saiyou_url'], $facility[$model]['saiyou_url'], array('target' => '_blank')); ?></td>
					</tr>
				</tbody>
			</table>
		</div>

		<?php echo $this->element('view_roseneki_add_form', $rosenekiDatas); ?>
		<div class="panel">
			<div id="roseneki-latest"></div>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading">施設基本情報</div>
			<table class="table table-bordered table-condensed">
					<tbody>
						<tr>
							<th>施設番号</th>
							<td><?php echo $facility[$model]['number']; ?></td>
						</tr>
						<tr>
							<th>登録日</th>
							<td><?php echo date('Y-m-d', strtotime($facility[$model]['created'])); ?></td>
						</tr>
						<tr>
							<th>契約</th>
							<td><?php echo $keiyaku_options[$facility[$model]['keiyaku']]; ?></td>
						</tr>
						<tr>
							<th>契約日</th>
							<td><?php echo !empty($facility[$model]['keiyakubi']) ? date('Y年n月j日', strtotime($facility[$model]['keiyakubi'])) : ''; ?></td>
						</tr>
						<tr>
							<th>手数料</th>
							<td>
								<?php if (!empty($facility[$model]['tesuryou'])) : ?>
								<?php echo $facility[$model]['tesuryou']; ?>%
								<?php endif; ?>
							</td>
						</tr>
						<tr>
							<th>返金規定</th>
							<td><?php echo $henkinkitei_options[$facility[$model]['henkinkitei']]; ?></td>
						</tr>
						<tr>
							<th>契約詳細</th>
							<td><?php echo nl2br($facility[$model]['henkinkiteisyousai']); ?></td>
						</tr>
						<tr>
							<th>施設画像</th>
							<td>
								<?php
									if (!empty($facility[$model]['facility_image'])) {
										/*
											20190522 sohnishi
											small画像のパスが切れていたため、元画像を表示してmax-height,max-widthを表示するように変更
										*/
										echo $this->Html->image('/files/facility/facility_image/' . $facility[$model]['facility_dir'] . '/' . $facility[$model]['facility_image'], array('style' => 'max-height: 180px;max-width: 180px;'));
									}
								?>
							</td>
						</tr>
						<tr>
							<th>メモ</th>
							<td><?php echo nl2br($facility[$model]['tesuryousyousai']); ?></td>
						</tr>
					</tbody>
				</table>
		</div>
	</div>

	<div class="col-xs-7">
		<div class="panel panel-default">
			<div class="panel-heading">連絡先情報</div>
			<table class="table table-bordered table-condensed">
				<tbody>
					<tr>
						<th>施設TEL</th>
						<td><?php echo $facility[$model]['tel']; ?></td>
					</tr>
					<tr>
						<th>施設FAX</th>
						<td><?php echo $facility[$model]['fax']; ?></td>
					</tr>
				</tbody>
			</table>
		</div>

		<?php echo $this->element('view_staff_add_form', $staffDatas); ?>
		<div class="panel panel-default">
			<div class="panel-heading">担当者情報&nbsp;[<span class="text-success">緑文字はメイン担当者</span>]</div>
			<div id="staffs" class="panel-body">
				<p class="text-info"><i class="fa fa-info-circle"></i> 担当者はメイン担当者昇順、部署名昇順、登録日降順で整列します</p>
				<div id="staff-latest"></div>
			</div>
		</div>

		<?php echo $this->element('view_contact_add_form', $contactDatas); ?>
		<div class="panel panel-default">
			<div class="panel-heading">連絡履歴</div>
			<div id="contacts" class="panel-body">
				<div id="contact-latest"></div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-default">
			<div class="panel-heading">施設詳細情報</div>
			<div class="panel-body">

				<div class="row">
					<div class="col-xs-6">
						<table class="table table-bordered table-condensed">
							<tr>
								<th>診療科目<?php if ($facility['FacilityShow']['shinryoukamoku_chk']) { echo $show_portal; } ?></th>
								<td>
									<?php if (!empty($facility['Shinryoukamoku'])) : ?>
									<?php foreach ($facility['Shinryoukamoku'] as $shinryoukamoku) : ?>
									<span>[<?php echo $shinryoukamoku['name']; ?>]</span>
									<?php endforeach; ?>
									<?php endif; ?>
								</td>
							</tr>
							<tr>
								<th>病床数<?php if ($facility['FacilityShow']['byousyou_chk']) { echo $show_portal; } ?></th>
								<td>
									<?php if (!empty($facility[$model]['byousyou'])) : ?>
									<?php echo $facility[$model]['byousyou']; ?>床
									<?php endif; ?>
								</td>
							</tr>
							<tr>
								<th>看護基準<?php if ($facility['FacilityShow']['kangokijun_chk']) { echo $show_portal; } ?></th>
								<td><?php echo $kangokijun_options[$facility[$model]['kangokijun']]; ?></td>
							</tr>
							<tr>
								<th>カルテ<?php if ($facility['FacilityShow']['karute_chk']) { echo $show_portal; } ?></th>
								<td><?php echo $karute_options[$facility[$model]['karute']]; ?></td>
							</tr>
							<tr>
								<th>救急指定<?php if ($facility['FacilityShow']['kyukyushitei_chk']) { echo $show_portal; } ?></th>
								<td><?php echo $kyukyushitei_options[$facility[$model]['kyukyushitei']]; ?></td>
							</tr>
							<tr>
								<th>救急指定詳細<?php if ($facility['FacilityShow']['kyukyusyousai_chk']) { echo $show_portal; } ?></th>
								<td><?php echo nl2br($facility[$model]['kyukyusyousai']); ?></td>
							</tr>
						</table>
					</div>
					<div class="col-xs-6">
						<table class="table table-bordered table-condensed">
							<tr>
								<th>病棟詳細<?php if ($facility['FacilityShow']['byoutousyousai_chk']) { echo $show_portal; } ?></th>
								<td><?php echo nl2br($facility[$model]['byoutousyousai']); ?></td>
							</tr>
							<tr>
								<th>手術詳細<?php if ($facility['FacilityShow']['syujyutsusyousai_chk']) { echo $show_portal; } ?></th>
								<td><?php echo nl2br($facility[$model]['syujyutsusyousai']); ?></td>
							</tr>
							<tr> <!-- 関連施設=>職員構成 -->
								<th>職員構成<?php if ($facility['FacilityShow']['syokuinkousei_chk']) { echo $show_portal; } ?></th>
								<td><?php echo nl2br($facility[$model]['syokuinkousei']); ?></td>
							</tr>
							<tr>
								<th>施設特徴<?php if ($facility['FacilityShow']['tokutyou_chk']) { echo $show_portal; } ?></th>
								<td><?php echo nl2br($facility[$model]['tokutyou']); ?></td>
							</tr>
							<tr>
								<!-- HP案内文=>関連施設 -->
								<th>関連施設<?php if ($facility['FacilityShow']['kanrenshisetsu_chk']) { echo $show_portal; } ?></th>
								<td><?php echo nl2br($facility[$model]['kanrenshisetsu']); ?></td>
							</tr>
							<tr>
								<th>その他情報<?php if ($facility['FacilityShow']['kyouikukensyu_chk']) { echo $show_portal; } ?></th>
								<td><?php echo nl2br($facility[$model]['kyouikukensyu']); ?></td>
							</tr>
							<!-- 
								20190522 sohnishi
								施設予備項目を追加
								$index：配列作成用のインデックス アソシエーションモデルのデータを同時に保存するために配列構造にする必要がある
								$facilityPreliminaryItems：求人予備項目マスタリスト
							-->
							<?php
								$index = 0;
								foreach($facilityPreliminaryItems as $id => $facilityPreliminaryItem) {
							?>
									<tr>
										<th><?php echo $facilityPreliminaryItem; if (count($facility['FacilityPreliminaryItem']) > $index && $facility['FacilityPreliminaryItem'][$index]['FacilitiesFacilityPreliminaryItem']['is_show']) { echo $show_portal; } ?></th>
										<td><?php if (count($facility['FacilityPreliminaryItem']) > $index) { echo nl2br($facility['FacilityPreliminaryItem'][$index]['FacilitiesFacilityPreliminaryItem']['content']); } ?></td>
									</tr>
							<?php
									$index += 1;
								}
							?>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-default">
			<div class="panel-heading">求人情報</div>
			<div class="panel-body">
				<table class="table">
					<thead>
						<tr>
							<th>表示</th>
							<th>資格</th>
							<th>部署</th>
							<th>勤務形態</th>
							<th>勤務時間</th>
							<th>給与（月給）</th>
							<th>公開</th>
							<th>新着</th>
							<th>人気</th>
							<th>登録日</th>
							<th>削除</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($facility['Recruit'] as $recruit) : ?>
						<?php echo $this->element('view_recruit_item', array('item' => $recruit)); ?>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
    <?php //echo $this->element('sql_dump'); ?>
