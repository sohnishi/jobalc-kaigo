<?php
// header
$thead = array(
	"Facility.id",
	"Facility.shisetsumei",
	// "Facility.shisetsumei_hurigana",
	// "Facility.tel",
	// "Facility.fax",
	// "Facility.group",
	// "Facility.group_hurigana",
	// "Facility.corporation",
	// "Facility.corporation_hurigana",
	// "Facility.keiyaku",
	// "Facility.keiyakubi",
	// "Facility.tesuryou",
	// "Facility.tesuryousyousai",
	// "Facility.henkinkitei",
	// "Facility.henkinkiteisyousai",
	// "Facility.zip",
	// "Facility.todoufuken",
	// "Facility.shikutyouson",
	// "Facility.banchi",
	// "Facility.tatemono",
	// "Facility.shisetsu_url",
	// "Facility.saiyou_url",
	// "Facility.byousyou",
	// "Facility.byoutousyousai",
	// "Facility.kangokijun",
	// "Facility.kyukyushitei",
	// "Facility.kyukyusyousai",
	// "Facility.syujyutsusyousai",
	// "Facility.karute",
	// "Facility.syokuinkousei",
	// "Facility.tokutyou",
	// "Facility.kanrenshisetsu",
	// "Facility.kyouikukensyu",
	// "FacilityPreliminaryItem.FacilityPreliminaryItem",
	// "ShisetsukeitaiFirst.ShisetsukeitaiFirst",
	// "ShisetsukeitaiSecond.ShisetsukeitaiSecond",
	// "Shinryoukamoku.Shinryoukamoku",
	// "FacilityShow.groupmei_chk",
	// "FacilityShow.houjinmei_chk",
	// "FacilityShow.shinryoukamoku_chk","FacilityShow.byousyou_chk",
	// "FacilityShow.byoutousyousai_chk",
	// "FacilityShow.kangokijun_chk",
	// "FacilityShow.kyukyushitei_chk",
	// "FacilityShow.kyukyusyousai_chk",
	// "FacilityShow.syujyutsusyousai_chk",
	// "FacilityShow.karute_chk",
	// "FacilityShow.syokuinkousei_chk",
	// "FacilityShow.tokutyou_chk",
	// "FacilityShow.kanrenshisetsu_chk",
	// "FacilityShow.kyouikukensyu_chk"
);

$exportData = array();
foreach ($facilities as $facility) {
	
	$exportData[] = array(
		$facility['Facility']['id'],
		$facility['Facility']['shisetsumei'],
		// $facility['Facility']['shisetsumei_hurigana'],
		// $facility['Facility']['tel'],
		// $facility['Facility']['fax'],
		// $facility['Facility']['group'],
		// $facility['Facility']['group_hurigana'],
		// $facility['Facility']['corporation'],
		// $facility['Facility']['corporation_hurigana'],
		// $facility['Facility']['keiyaku'],
		// $facility['Facility']['keiyakubi'],
		// $facility['Facility']['tesuryou'],
		// $facility['Facility']['tesuryousyousai'],
		// $facility['Facility']['henkinkitei'],
		// $facility['Facility']['henkinkiteisyousai'],
		// $facility['Facility']['zip'],
		// $facility['Facility']['todoufuken'],
		// $facility['Facility']['shikutyouson'],
		// $facility['Facility']['banchi'],
		// $facility['Facility']['tatemono'],
		// $facility['Facility']['shisetsu_url'],
		// $facility['Facility']['saiyou_url'],
		// $facility['Facility']['byousyou'],
		// $facility['Facility']['byoutousyousai'],
		// $facility['Facility']['kangokijun'],
		// $facility['Facility']['kyukyushitei'],
		// $facility['Facility']['kyukyusyousai'],
		// $facility['Facility']['syujyutsusyousai'],
		// $facility['Facility']['karute'],
		// $facility['Facility']['syokuinkousei'],
		// $facility['Facility']['tokutyou'],
		// $facility['Facility']['kanrenshisetsu'],
		// $facility['Facility']['kyouikukensyu'],
		// $facility['FacilityPreliminaryItem']['FacilityPreliminaryItem'],
		// $facility['ShisetsukeitaiFirst']['ShisetsukeitaiFirst'],
		// $facility['ShisetsukeitaiSecond']['ShisetsukeitaiSecond'],
		// $facility['Shinryoukamoku']['Shinryoukamoku'],
		// $facility['FacilityShow']['groupmei_chk'],
		// $facility['FacilityShow']['houjinmei_chk'],
		// $facility['FacilityShow']['shinryoukamoku_chk'],
		// $facility['FacilityShow']['byousyou_chk'],
		// $facility['FacilityShow']['byoutousyousai_chk'],
		// $facility['FacilityShow']['kangokijun_chk'],
		// $facility['FacilityShow']['kyukyushitei_chk'],
		// $facility['FacilityShow']['kyukyusyousai_chk'],
		// $facility['FacilityShow']['syujyutsusyousai_chk'],
		// $facility['FacilityShow']['karute_chk'],
		// $facility['FacilityShow']['syokuinkousei_chk'],
		// $facility['FacilityShow']['tokutyou_chk'],
		// $facility['FacilityShow']['kanrenshisetsu_chk'],
		// $facility['FacilityShow']['kyouikukensyu_chk']
	);
}

// Settings
$date = date('YmdHis');
$this->FastCSV->filename = '施設一覧' . $date;
$this->FastCSV->setRows($exportData);
$this->FastCSV->setFirstRow($thead);

// Export
$this->FastCSV->export();





	// "施設ID", //id
	// "求人数", // recruit_count
	// "施設番号", // number
	// "施設名", // shisetsumei
	// "施設名ふりがな", // shisetsumei_hurigana
	// "電話番号", // tel
	// "FAX番号", //fax
	// // "施設画像", // facility_image
	// // "施設画像ディレクトリ", // facility_dir
	// "グループ", // group
	// "グループ名ふりがな", // group_hurigana
	// "法人名", // corporation
	// "法人名ふりがな", // corporation_hurigana
	// "契約状態", // keiyaku
	// "契約日", // keiyakubi
	// "手数料", // tesuryou
	// "メモ", // tesuryouyousai 
	// "返金規定", // henkinkitei
	// "契約詳細", // henkinkiteisyousai 
	// "郵便番号", // zip
	// "都道府県", // todoufuken
	// "市区町村", // shikutyouson
	// "番地", // banchi
	// "建物名", // tatemono
	// "施設ホームページ", // shisetsu_url
	// "採用情報URL", // saiyou_url
	// "病床数", // byousyou
	// "病棟詳細", // byoutousyousai
	// "看護基準", // kangokijun
	// "カルテ",
	// "",
	// "",
	// "",
	// ""
