<script>
$(function () {

	function staffRecords() {
		$('#staff-latest').empty();
		var url = '<?php echo $this->Html->url(array('plugin' => 'staffs', 'controller' => 'staffs', 'action' => 'getLatest', $facilityId, 'admin' => true)); ?>';
		$.getJSON(url, function (json, status) {
			if (status == 'success') {
				if ($.isEmptyObject(json)) {
					$('#staff-latest').append('<p>担当者が登録されていません</p>');
				} else {
					$.each(json, function () {
						var _this = this;
						var primary = ' text-success';
						var memo = _this.Staff.memo;
						memo = memo.replace(/\r\n/g, "<br>");
						memo = memo.replace(/\r|\n/g, "<br>");
						var html = '<table class="table table-bordered table-condensed';
						if (_this.Staff.primary_check == 1) {
							html += primary;
						}
						html += '"><tbody>';
						html += '<tr>';
							html += '<th>' + _this.Staff.busyo + '</th>';
							html += '<th>TEL:' + _this.Staff.primary_tel + '</th>';
							html += '<td rowspan="4">' + memo + '</td>';
						html += '</tr>';
						html += '<tr>';
							html += '<th>' + _this.Staff.yakuwari + '</th>';
							html += '<th>FAX:' + _this.Staff.primary_fax + '</th>';
						html += '</tr>';
						html += '<tr>';
							html += '<th>' + _this.Staff.name + '</th>';
							html += '<th>携帯:' +_this.Staff.keitai_tel + '</th>';
						html += '</tr>';
						html += '<tr>';
							html += '<td>';
									html += '<button type="button" class="btn btn-danger btn-xs staff-delete" data-staff-id="' + _this.Staff.id + '">削除</button>&nbsp;&nbsp;';
									html += '<button type="button" class="btn btn-default btn-xs staff-edit" data-staff-id="' + _this.Staff.id + '">更新</button>';
							html += '</td>';
							html += '<th>';
							if (_this.Staff.email) {
								html += '<a href="mailto:' + _this.Staff.email + '">' + _this.Staff.email + '</a>'
							}
							html += '</th>';
						html += '</tr>';
						html += '</tbody></table>';
						$('#staff-latest').append(html);
					});
				}
			}
		});
	}

	$('#staffSubmit').on('click', function () {
		$.ajax({
			type: 'POST',
			url: '<?php echo $this->Html->url(array('plugin' => 'staffs', 'controller' => 'staffs', 'action' => 'update', 'admin' => true)); ?>',
			data: $('#StaffAdminViewForm').serializeArray(),
			success: function (data, textStatus, jqXHR) {
				var jsonObj = $.parseJSON(data);
				staffMessage(jsonObj.message, jsonObj.succeed);
				if (jsonObj.succeed) {
					$('#StaffAdminViewForm').find('#StaffId, input[type="text"], input[type="email"], select, textarea').val('');
					staffRecords();
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				staffMessage('通信に失敗しました' + errorThrown, false);
			}
		});
	});

	$('#staffReset').on('click', function () {
		$('#StaffAdminViewForm').find('#StaffId, input[type="text"], input[type="email"], textarea').val('');
		$('#StaffPrimaryCheck').each(function () {
			this.selectedIndex = 0;
		});
	});

	$('#staff-latest').on('click', '.staff-edit', function () {
		/**
		 *	20190619 sohnishi
		 *	更新ボタン押下->閉じる->更新ボタン押下でレイアウトが崩れる事象に対応
		 */
		$('#staffForm').css('height', 'initial');
		$('#staffForm').addClass('in').parent().find(".fa-plus").removeClass("fa-plus").addClass("fa-minus");
		var staffId = $(this).data('staff-id');
		var url = '<?php echo $this->Html->url(array('plugin' => 'staffs', 'controller' => 'staffs', 'action' => 'read', 'admin' => true)); ?>';
		$.getJSON(url + '/' + staffId, function (json, status) {
			if (status == 'success') {
				$('#StaffId').val(json.Staff.id);
				$('#StaffFacilityId').val(json.Staff.facility_id);
				$('#StaffBusyo').val(json.Staff.busyo);
				$('#StaffYakuwari').val(json.Staff.yakuwari);
				$('#StaffName').val(json.Staff.name);
				$('#StaffPrimaryTel').val(json.Staff.primary_tel);
				$('#StaffPrimaryFax').val(json.Staff.primary_fax);
				$('#StaffKeitaiTel').val(json.Staff.keitai_tel);
				$('#StaffEmail').val(json.Staff.email);
				$('#StaffPrimaryCheck').val(json.Staff.primary_check);
				$('#StaffMemo').val(json.Staff.memo);
			}
		});
	});

	$('#staff-latest').on('click', '.staff-delete', function () {
		var staffId = $(this).data('staff-id');
		if (confirm('本当に削除しても宜しいですか？')) {
			var url = '<?php echo $this->Html->url(array('plugin' => 'staffs', 'controller' => 'staffs', 'action' => 'ajaxDelete', 'admin' => true)); ?>';
			$.getJSON(url + '/' + staffId, function (json, status) {
				if (status == 'success') {
					staffMessage(json.message, json.succeed);
					if (json.succeed) {
						staffRecords();
					}
				}
			});
		}
	});

	function staffMessage(text, succeed) {
		var color = succeed ? 'text-success' : 'text-warning';
		var msg = $('<p />').attr('class', color).text(text);
		$('#staff-message').append(msg);
		setTimeout(function () {
			$('#staff-message').find('p').remove();
		}, 3000);
	}

	$(window).load(function () {
		staffRecords();
	});

});
</script>