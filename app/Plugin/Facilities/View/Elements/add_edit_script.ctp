<script>
$(function () {
	if ($('#FacilityKeiyakubi').length) {
		$('#FacilityKeiyakubi').datepicker({
			format: 'yyyy-mm-dd',
			language: 'ja',
			autoclose: true
		});
	}

	var groups = new Bloodhound({
		remote: '<?php echo $this->Html->url(array('plugin' => 'facilities', 'controller' => 'groups', 'action' => 'typeahead', '?' => 'q=%QUERY', 'admin' => true)); ?>',
		datumTokenizer: function (d) {
			return Bloodhound.tokenizers.whitespace(d.name);
		},
		queryTokenizer: Bloodhound.tokenizers.whitespace,
	});

	groups.initialize();

	$('#FacilityGroup').typeahead({
		hint: false,
		highlight: true,
		minLength: 2
	},
	{
		name: 'groups',
		displayKey: 'name',
		source: groups.ttAdapter()
	});

	var corporations = new Bloodhound({
		remote: '<?php echo $this->Html->url(array('plugin' => 'facilities', 'controller' => 'corporations', 'action' => 'typeahead', '?' => 'q=%QUERY', 'admin' => true)); ?>',
		datumTokenizer: function (d) {
			return Bloodhound.tokenizers.whitespace(d.name);
		},
		queryTokenizer: Bloodhound.tokenizers.whitespace,
	});

	corporations.initialize();

	$('#FacilityCorporation').typeahead({
		hint: false,
		highlight: true,
		minLength: 2
	},
	{
		name: 'corporations',
		displayKey: 'name',
		source: corporations.ttAdapter()
	});

});
</script>