<?php
	// 求人オプション
	$shikaku_options = Configure::read('shikaku');
	$kinmu_keitai_options = Configure::read('kinmu_keitai');

?>
<tr>
	<td><?php echo $this->Html->link('表示', array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'view', 'facility' => $item['facility_id'], 'admin' => true)); ?></td>
	<td><?php echo $shikaku_options[$item['shikaku']]; ?></td>
	<td><?php echo $item['ShisetsukeitaiSecond']['name']; ?></td>
	<td><?php echo $kinmu_keitai_options[$item['kinmu_keitai']]; ?></td>
	<td><?php echo nl2br($item['kinmu_jikan']); ?></td>
	<td><?php echo nl2br($item['kyuyo']); ?></td>
	<td><?php echo $item['active'] == 1 ? '<i class="fa fa-check"></i>' : ''; ?></td>
	<td><?php echo $item['shintyaku_kyujin'] == 1 ? '<i class="fa fa-check"></i>' : ''; ?></td>
	<td><?php echo $item['ninki_kyujin'] == 1 ? '<i class="fa fa-check"></i>' : ''; ?></td>
	<td><?php echo date('Y-m-d', strtotime($item['created'])); ?></td>
	<td><?php echo $this->Form->postLink('削除', array('plugin' => 'recruits', 'controller' => 'recruits', 'action' => 'delete', $item['id']), array(), '本当に削除しますか？元には戻せません'); ?></td>
</tr>
