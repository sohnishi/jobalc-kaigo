<script>
$(function () {

	// 顧客番号
	var customer_number;
	// 顧客名をtypeaheadを使用して入力したかどうか
	var isCustomerTypeaHead = false;
	// 顧客番号を入力後に顧客がヒットして値をセットしたかどうか
	var isFindByNumberSuccessed = false;
	// 顧客番号から検索した顧客名をセット
	var customer_name;


	var customers = new Bloodhound({
		remote: '<?php echo $this->Html->url(array('plugin' => 'customers', 'controller' => 'customers', 'action' => 'typeahead', '?' => 'q=%QUERY', 'admin' => true)); ?>',
		datumTokenizer: function (d) {
			return Bloodhound.tokenizers.whitespace(d.name);
		},
		queryTokenizer: Bloodhound.tokenizers.whitespace
	});


	customers.initialize();
	$('#CustomerName').typeahead({
		hint: false,
		highlight: true,
		minLength: 2
	},
	{
		name: 'customers',
		displayKey: 'name',
		source: customers.ttAdapter()
	}).on("typeahead:opened", function(e, datum) {
	}).on("typeahead:closed", function(e, datum) {
		/*
			顧客名からフォーカスが外れる際に、
			顧客番号からの検索値が入っていれば、そのまま値をセットしておく
		*/
		if (isFindByNumberSuccessed) {
			$('#CustomerName').val(customer_name);
		}
		// 顧客番号の値が空の場合は顧客名に値が入っていても初期化する
		if ($('#CustomerContactCustomerId').val().length < 1) {
			$('#CustomerName').val('');
		}
	}).on("typeahead:selected typeahead:autocomplete", function(e, datum) {
		/*
			Callback
			顧客numberをセット
			typeaheadを使用したため、isCustomerTypeaHeadをtrue
			顧客番号からの検索ではないため、isFindByNumberSuccessedをfalse
		*/
		customer_number = datum['number'];
		$('#CustomerContactCustomerId').val(customer_number);

		isCustomerTypeaHead = true;
		isFindByNumberSuccessed = false;
	});


	/*
		20190521 sohnishi
		入力補完を行わずに顧客名を入力された際にも、
		顧客番号を検索してセットする対応
	*/
	$('#CustomerName').on('blur', function(){
		if (isCustomerTypeaHead) {
			// typeaheadを使用した

			// 2回目の入力に対応するため、isCustomerTypeaHeadをFalseに戻す
			isCustomerTypeaHead = false;

		} else {
			// typeaheadを使用していない

			// 空文字判定
			if ($(this).val() != null && $(this).val().length >= 1) {
				$.ajax({
					type: 'GET',
					url: '<?php echo $this->Html->url(array('plugin' => 'customers', 'controller' => 'customers', 'action' => 'typeahead', 'admin' => true)); ?>',
					data: {
						'q':$(this).val(),
					},
					success: function (data, textStatus, jpXHR) {
						if (data != null) {
							var jsonObj = $.parseJSON(data);
							/*
								検索結果が1件であれば、
								顧客番号をセット
							*/
							isCustomerTypeaHead = false;
							if (jsonObj.length == 1) {

								customer_number = jsonObj[0]['id'];
								$('#CustomerContactCustomerId').val(customer_number);

							} else {
								/* 
									typeaheadもしくは顧客番号での検索を使用していない場合に限り、全項目削除
									typeaheadを使用しない顧客名入力は上の判定を通るため不要
								*/
								if (!isCustomerTypeaHead && !isFindByNumberSuccessed) {
									$('#CustomerName').val('');
									$('#CustomerContactCustomerId').val('');	
								}
							}
						}
					},
					error: function (jpXHR, textStatus, errorThrown) {
						console.log('通信に失敗しました' + errorThrown, false);
					}
				});

			}
		}
	});


	/*
		20190521 sohnishi
		顧客番号入力後に顧客の検索を行い、値をセット
	*/
	$('#CustomerContactCustomerId').on('blur', function(){
		// 空文字判定
		if ($(this).val() != null && $(this).val().length >= 1) {
			$.ajax({
				type: 'GET',
				url: '<?php echo $this->Html->url(array('plugin' => 'customers', 'controller' => 'customers', 'action' => 'find_by_number', 'admin' => true)); ?>',
				data: {
					'q':$(this).val(),
				},
				success: function (data, textStatus, jpXHR) {
					var jsonObj = $.parseJSON(data);
					/*
						検索結果がnotnull且つ1件であれば値をセット
					*/
					if (jsonObj != null && jsonObj.length == 1) {

						customer_name = jsonObj[0]['name'];
						$('#CustomerName').val(customer_name);
						isFindByNumberSuccessed = true;

					} else {
						// 顧客番号での検索時は、データが見つからない場合、全項目削除
						$('#CustomerName').val('');
						$('#CustomerContactCustomerId').val('');	
						isFindByNumberSuccessed = false;
					}

				},
				error: function (jpXHR, textStatus, errorThrown) {
					console.log('通信に失敗しました' + errorThrown, false);
				}
			});

		} else {
			// 顧客番号の値が空の場合は顧客名に値が入っていても初期化する
			$('#CustomerName').val('');
		}
	});

	// 顧客名を手入力した場合、isFindByNumberSuccessedをfalseとする
	$('#CustomerName').on('change', function(){
		isFindByNumberSuccessed = false;
	});

	function contactRecords() {
		$('#contact-latest').empty();
		var url = '<?php echo $this->Html->url(array('plugin' => 'contacts', 'controller' => 'contacts', 'action' => 'getLatest', $facilityId, 'admin' => true)); ?>';
		$.getJSON(url, function (json, status) {
			if (status == 'success') {
				if ($.isEmptyObject(json.contacts)) {
					$('#contact-latest').append('<p>連絡履歴が登録されていません</p>');
				} else {
					var renraku = json.renraku;
					$.each(json.contacts, function () {
						var _this = this;
						var syousai = _this.Contact.syousai;
						var contactsCustomerContact = _this.ContactsCustomerContact;
						syousai = syousai.replace(/\r\n/g, "<br>");
						syousai = syousai.replace(/\r|\n/g, "<br>");
						var html = '<table class="table table-bordered table-condensed"><tbody>';
						html += '<tr>';
							html += '<th>' + _this.Contact.date + '</th>';
							html += '<td rowspan="';
							/*
								20190523 sohnishi
								段数を合わせるため、顧客連絡履歴が存在する場合は6
							*/
							html += contactsCustomerContact.length > 0 ? '7">' : '6">';
							html += syousai + '</td>';
						html += '</tr>';
						html += '<tr>';
							html += '<th>' + _this.Contact.time + '</th>';
						html += '</tr>';
						html += '<tr>';
							html += '<th>' + _this.User.name + '</th>';
						html += '</tr>';
						html += '<tr>';
							html += '<th>' + _this.Staff.name + '</th>';
						html += '</tr>';
						html += '<tr>';
							html += '<th>' + renraku[_this.Contact.houhou] + '</th>';
						html += '</tr>';
						/*
							20190523 sohnishi
							顧客連絡履歴が存在する場合のみリンクを貼る
						*/
						if (contactsCustomerContact.length > 0) {
							html += '<tr><th>';
							html += '顧客番号：';
							html += "<a href='/cw/admin/customers/view/?number=";
							html += contactsCustomerContact[0]['CustomerContact']['customer_id'] + "'>";
							html += contactsCustomerContact[0]['CustomerContact']['customer_id'];
							html += "</a>";
							html += '</th></tr>';
						}

						html += '<tr>';
							html += '<td>';
									html += '<button type="button" class="btn btn-danger btn-xs contact-delete" data-contact-id="' + _this.Contact.id + '">削除</button>&nbsp;&nbsp;';
									html += '<button type="button" class="btn btn-default btn-xs contact-edit" data-contact-id="' + _this.Contact.id + '">更新</button>';
							html += '</td>';
						html += '</tr>';
						html += '</tbody></table>';
						$('#contact-latest').append(html);
					});
				}
			}
		});
	}

	$('#contactSubmit').on('click', function () {

		// submit前に施設連絡履歴用のhidden項目を追加
		// 連絡日時・年
		$('<input>').attr({
			type: 'hidden',
			id: 'CustomerContactDateYear',
			name: 'data[CustomerContact][date][year]',
			value: $('#ContactDateYear').val()
		}).appendTo('#ContactAdminViewForm');

		// 連絡日時・月
		$('<input>').attr({
			type: 'hidden',
			id: 'CustomerContactDateMonth',
			name: 'data[CustomerContact][date][month]',
			value: $('#ContactDateMonth').val()
		}).appendTo('#ContactAdminViewForm');

		// 連絡日時・日
		$('<input>').attr({
			type: 'hidden',
			id: 'CustomerContactDateDay',
			name: 'data[CustomerContact][date][day]',
			value: $('#ContactDateDay').val()
		}).appendTo('#ContactAdminViewForm');

		// 連絡時間・時
		$('<input>').attr({
			type: 'hidden',
			id: 'CustomerContactTimeHour',
			name: 'data[CustomerContact][time][hour]',
			value: $('#ContactTimeHour').val()
		}).appendTo('#ContactAdminViewForm');
		
		// 連絡時間・分
		$('<input>').attr({
			type: 'hidden',
			id: 'CustomerContactTimeMin',
			name: 'data[CustomerContact][time][min]',
			value: $('#ContactTimeMin').val()
		}).appendTo('#ContactAdminViewForm');

		// user_id
		$('<input>').attr({
			type: 'hidden',
			id: 'CustomerContactUserId',
			name: 'data[CustomerContact][user_id]',
			value: $('#ContactUserId').val()
		}).appendTo('#ContactAdminViewForm');
		
		// 連絡方法
		$('<input>').attr({
			type: 'hidden',
			id: 'CustomerContactHouhou',
			name: 'data[CustomerContact][houhou]',
			value: $('#ContactHouhou').val()
		}).appendTo('#ContactAdminViewForm');

		// 連絡詳細
		$('<input>').attr({
			type: 'hidden',
			id: 'CustomerContactSyousai',
			name: 'data[CustomerContact][syousai]',
			value: $('#ContactSyousai').val()
		}).appendTo('#ContactAdminViewForm');


		$.ajax({
			type: 'POST',
			url: '<?php echo $this->Html->url(array('plugin' => 'contacts', 'controller' => 'contacts', 'action' => 'update', 'admin' => true)); ?>',
			data: $('#ContactAdminViewForm').serializeArray(),
			success: function (data, textStatus, jpXHR) {
				/* 
					20190618 sohnishi
					型判定し、objectでなければjson変換する
				*/
				if (typeof data != 'object') {
					data = $.parseJSON(data);
				}
				contactMessage(data.message, data.succeed);
				if (data.succeed) {
					$('#contactForm').removeClass('in').parent().find(".fa-minus").removeClass("fa-minus").addClass("fa-plus");
					$('#ContactAdminViewForm').find('#ContactId, select, textarea').val('');
					/*
						20190523 sohnishi
						顧客項目追加
					*/
					$('#CustomerContactCustomerId').val('');
					$('#CustomerName').val('');

					var datetime = new Date();
					var year = datetime.getFullYear();
					var month = ("0" + (datetime.getMonth() + 1)).slice(-2);
					var day = ("0" + datetime.getDate()).slice(-2);
					var hour = ("0" + datetime.getHours()).slice(-2);
					var minute = ("0" + datetime.getMinutes()).slice(-2);
					$('#ContactDateYear').val(year);
					$('#ContactDateMonth').val(month);
					$('#ContactDateDay').val(day);
					$('#ContactTimeHour').val(hour);
					$('#ContactTimeMin').val(minute);

					contactRecords();
				}
			},
			error: function (jpXHR, textStatus, errorThrown) {
				contactMessage('通信に失敗しました' + errorThrown, false);
			}
		});
	});

	$('#contactReset').on('click', function () {
		$('#ContactAdminViewForm').find('#ContactId, textarea').val('');
		$('#ContactDateYear, #ContactDateMonth, #ContactDateDay, #ContactTimeHour, #ContactTimeMin, #ContactHouhou').each(function () {
			this.selectedIndex = 0;
		});
		var datetime = new Date();
		var year = datetime.getFullYear();
		var month = ("0" + (datetime.getMonth() + 1)).slice(-2);
		var day = ("0" + datetime.getDate()).slice(-2);
		var hour = ("0" + datetime.getHours()).slice(-2);
		var minute = ("0" + datetime.getMinutes()).slice(-2);
		$('#ContactDateYear').val(year);
		$('#ContactDateMonth').val(month);
		$('#ContactDateDay').val(day);
		$('#ContactTimeHour').val(hour);
		$('#ContactTimeMin').val(minute);
		/*
			20190523 sohnishi
			顧客項目追加
		*/
		$('#CustomerContactCustomerId').val('');
		$('#CustomerName').val('');
	});

	$('#contact-latest').on('click', '.contact-edit', function () {
		/**
		 *	20190619 sohnishi
		 *	更新ボタン押下->閉じる->更新ボタン押下でレイアウトが崩れる事象に対応
		 */
		$('#contactForm').css('height', 'initial');
		$('#contactForm').addClass('in').parent().find(".fa-plus").removeClass("fa-plus").addClass("fa-minus");
		var contactId = $(this).data('contact-id');
		var url = '<?php echo $this->Html->url(array('plugin' => 'contacts', 'controller' => 'contacts', 'action' => 'read', 'admin' => true)); ?>';
		$.getJSON(url + '/' + contactId, function (json, status) {
			var contactsCustomerContact = json.ContactsCustomerContact;
			if (status == 'success') {

				if (contactsCustomerContact.length >= 1) {

					var customer_id = contactsCustomerContact[0]['CustomerContact']['customer_id'];

					// 空文字判定
					if (customer_id != null && customer_id.length >= 1) {


						$.ajax({
							type: 'GET',
							url: '<?php echo $this->Html->url(array('plugin' => 'customers', 'controller' => 'customers', 'action' => 'find_by_number', 'admin' => true)); ?>',
							data: {
								'q':customer_id,
							},
							success: function (data, textStatus, jpXHR) {
								var jsonObj = $.parseJSON(data);
								/*
									検索結果がnotnull且つ1件であれば、値をセット
								*/
								if (jsonObj != null && jsonObj.length == 1) {
									customer_name = jsonObj[0]['name'];
									console.log(customer_name);
									$('#CustomerContactCustomerId').val(customer_id);
									$('#CustomerName').val(customer_name);
									isFindByNumberSuccessed = true;
								} else {
									// 顧客番号での検索時は、データが見つからない場合、全項目削除
									$('#CustomerName').val('');
									$('#CustomerContactCustomerId').val('');	
									isFindByNumberSuccessed = false;
								}

							},
							error: function (jpXHR, textStatus, errorThrown) {
								console.log('通信に失敗しました' + errorThrown, false);
							}
						});
					}

				}

				var _this = json;
				$('#ContactId').val(_this.Contact.id);
				$('#ContactFacilityId').val(_this.Contact.facility_id);
				$('#ContactUserId').val(_this.Contact.user_id);
				$('#ContactDateYear').val(_this.Contact.year);
				$('#ContactDateMonth').val(_this.Contact.month);
				$('#ContactDateDay').val(_this.Contact.day);
				$('#ContactTimeHour').val(_this.Contact.hour);
				$('#ContactTimeMin').val(_this.Contact.min);
				$('#ContactStaffId').val([_this.Contact.staff_id]);
				$('#ContactHouhou').val(_this.Contact.houhou);
				$('#ContactSyousai').val(_this.Contact.syousai);
			}
		});
	});

	$('#contact-latest').on('click', '.contact-delete', function () {
		var contactId = $(this).data('contact-id');
		if (confirm('本当に削除しても宜しいですか？')) {
			var url = '<?php echo $this->Html->url(array('plugin' => 'contacts', 'controller' => 'contacts', 'action' => 'ajaxDelete', 'admin' => true)); ?>';
			$.getJSON(url + '/' + contactId, function (json, status) {
				if (status == 'success') {
					contactMessage(json.message, json.succeed);
					if (json.succeed) {
						contactRecords();
					}
				}
			});
		}
	});

	function contactMessage(text, succeed) {
		var color = succeed ? 'text-success' : 'text-warning';
		var msg = $('<p />').attr('class', color).text(text);
		$('#contact-message').append(msg);
		setTimeout(function () {
			$('#contact-message').find('p').remove();
		}, 3000);
	}

	$(window).load(function () {
		contactRecords();
	});
});
</script>