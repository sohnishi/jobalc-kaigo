<div class="panel panel-default">
	<div class="panel-heading">
		<a href="#staffForm" data-toggle="collapse" aria-expanded="false" aria-controls="staffForm"><i class="fa fa-plus"></i> 担当者を追加・更新</a>
	</div>
	<div class="panel-body collapse" id="staffForm">

		<?php
			echo $this->Form->create('Staff', array(
				'inputDefaults' => array(
					'label' => false,
					'wrapInput' => false,
					'div' => false,
					'class' => 'form-control'
				),
				'url' => array(
					'plugin' => 'staffs',
					'controller' => 'staffs',
					'action' => 'update',
					'admin' => true
				),
				'default' => false
			));
			echo $this->Form->input('id', array('type' => 'hidden'));
			echo $this->Form->input('facility_id', array('type' => 'hidden', 'value' => $facilityId));
		?>

		<div class="row">
			<div class="form-group col-xs-4">
				<?php echo $this->Form->input('busyo', array('label' => '部署')); ?>
			</div>
			<div class="form-group col-xs-4">
				<?php echo $this->Form->input('yakuwari', array('label' => '役職')); ?>
			</div>
			<div class="form-group col-xs-4">
				<?php echo $this->Form->input('name', array('label' => '担当者名 <span class="req">*</span>')); ?>
			</div>
		</div>

		<div class="row">
			<div class="form-group col-xs-4">
				<?php echo $this->Form->input('primary_tel', array('label' => '直通TEL')); ?>
				<span class="help-block">0xx-xxx-xxxxの形式で入力</span>
			</div>
			<div class="form-group col-xs-4">
				<?php echo $this->Form->input('primary_fax', array('label' => '直通FAX')); ?>
				<span class="help-block">0xx-xxx-xxxxの形式で入力</span>
			</div>
			<div class="form-group col-xs-4">
				<?php echo $this->Form->input('keitai_tel', array('label' => '携帯電話')); ?>
				<span class="help-block">0xx-xxxx-xxxxの形式で入力</span>
			</div>
		</div>

		<div class="row">
			<div class="form-group col-xs-4">
				<?php echo $this->Form->input('email', array('label' => 'メールアドレス')); ?>
			</div>
			<div class="form-group col-xs-4">
				<?php echo $this->Form->label('primary_check', 'メイン担当者'); ?>
				<div class="input-group">
					<?php echo $this->Form->input('primary_check', array('type' => 'select', 'options' => array(0 => 'いいえ', 1 => 'はい'))); ?>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="form-group col-xs-12">
				<?php echo $this->Form->input('memo', array('rows' => 3, 'label' => '備考')); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo $this->Form->submit('送信する', array('id' => 'staffSubmit', 'class' => 'btn btn-primary', 'div' => false)); ?>
			<?php echo $this->Form->button('リセット', array('type' => 'button', 'id' => 'staffReset', 'class' => 'btn btn-warning', 'div' => false)); ?>
		</div>
		<div class="row">
			<div class="col-xs-12">
			</div>
		</div>

		<?php echo $this->Form->end(); ?>

	</div>
</div>
<div id="staff-message"></div>
