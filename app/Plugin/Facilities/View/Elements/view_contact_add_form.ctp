<?php
	$renraku_houhou_options = Configure::read('renraku_houhou');
	echo $this->Html->script('typeahead.bundle.min', array('inline' => false));
?>
<div class="panel panel-default">
	<div class="panel-heading">
		<a href="#contactForm" data-toggle="collapse" aria-expanded="false" aria-controls="contactForm"><i class="fa fa-plus"></i> 連絡履歴を追加・更新</a>
	</div>
	<div class="panel-body collapse" id="contactForm">
		<div class="well well-sm">※担当者を更新した場合はブラウザを再読み込み（リロード）してください。</div>
		<?php
			echo $this->Form->create('Contact', array(
				'inputDefaults' => array(
					'label' => false,
					'wrapInput' => false,
					'div' => false,
					'class' => 'form-control'
				),
				'url' => array(
					'plugin' => 'contacts',
					'controller' => 'contacts',
					'action' => 'update',
					'admin' => true
				),
				'default' => false
			));

			echo $this->Form->input('id', array('type' => 'hidden'));
			echo $this->Form->input('facility_id', array('type' => 'hidden', 'value' => $facilityId));
			echo $this->Form->input('user_id', array('type' => 'hidden', 'value' => $userId));
		?>

		<div class="row">
			<div class="col-xs-6">
				<div class="form-group">
					<?php echo $this->Form->label('date', '連絡日付'); ?>
					<div class="input-group">
						<?php echo $this->Form->year('date', 2000, date('Y'), array('value' => date('Y'), 'class' => 'select-control', 'style' => 'margin-left:0px;')); ?>年
						<?php echo $this->Form->month('date', array('monthNames' => false, 'empty' => false, 'value' => date('m'), 'class' => 'select-control', 'style' => 'margin-left:0px;')); ?>月
						<?php echo $this->Form->day('date', array('value' => date('d'), 'class' => 'select-control', 'style' => 'margin-left:0px;')); ?>日
					</div>
				</div>
				<div class="form-group">
					<?php echo $this->Form->label('time', '連絡時間'); ?>
					<div class="input-group">
						<?php echo $this->Form->hour('time', 24, array('value' => date('H'), 'class' => 'select-control', 'style' => 'margin-left:0px;')); ?>:
						<?php echo $this->Form->minute('time', array('value' => date('i'), 'class' => 'select-control', 'style' => 'margin-left:0px;')); ?>
					</div>
				</div>
								<!-- 
					20190523 sohnishi
					顧客番号追加(顧客ID)
				 -->
				 <?php echo $this->Form->label('Customer.id', '顧客番号'); ?>
				<div class="row">
					<div class="col-xs-8">
						<div class="form-group">
							<?php echo $this->Form->input('CustomerContact.customer_id', array('type' => 'number')); ?>
						</div>
					</div>
				</div>

				<!-- 
					20190523 sohnishi
					顧客名追加
				 -->
				<?php echo $this->Form->label('Customer.name', '顧客名'); ?>
				<div class="row">
					<div class="col-xs-8">
						<div class="form-group">
							<?php echo $this->Form->input('Customer.name'); ?>
							<span class='help-block' style="font-size:10px;">2文字以上入力すると自動補完されます</span>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-8">
						<div class="form-group">
							<?php echo $this->Form->input('user_id', array('value' => $currentUser['id'], 'empty' => '選択してください', 'label' => '担当者')); ?>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="form-group col-xs-8">
						<?php echo $this->Form->input('staff_id', array('options' => $staffs, 'empty' => '選択してください', 'label' => '施設担当者')); ?>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-xs-8">
						<?php echo $this->Form->input('houhou', array('options' => $renraku_houhou_options, 'empty' => '選択してください', 'label' => '連絡方法')); ?>
					</div>
				</div>
			</div>

			<div class="col-xs-6">
				<div class="form-group">
					<?php echo $this->Form->input('syousai', array('type' => 'textarea', 'rows' => 10, 'label' => '連絡詳細 <span class="req">*</span>')); ?>
				</div>
			</div>
		</div>

		<div class="form-group">
			<?php echo $this->Form->submit('送信する', array('id' => 'contactSubmit', 'class' => 'btn btn-primary', 'div' => false)); ?>
			<?php echo $this->Form->button('リセット', array('type' => 'button', 'id' => 'contactReset', 'class' => 'btn btn-warning', 'div' => false)); ?>
		</div>

		<?php echo $this->Form->end(); ?>
	</div>
</div>
<div id="contact-message"></div>
