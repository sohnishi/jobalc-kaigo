<div class="panel panel-default">
	<div class="panel-heading">
		<a href="#rosenekiForm" data-toggle="collapse" aria-expanded="false" aria-controls="rosenekiForm"><i class="fa fa-plus"></i> 最寄り路線・最寄り駅を追加</a>
	</div>
	<div class="panel-body collapse" id="rosenekiForm">

		<?php
			echo $this->Form->create('Roseneki', array(
				'inputDefaults' => array(
					'label' => false,
					'wrapInput' => false,
					'div' => false,
					'class' => 'form-control'
				),
				'url' => array(
					'plugin' => 'facilities',
					'controller' => 'rosenekis',
					'action' => 'update',
					'admin' => true
				),
				'default' => false
			));
			echo $this->Form->input('Roseneki.facility_id', array('type' => 'hidden', 'value' => $facilityId));
		?>
		<div class="row">
			<div class="form-group col-xs-4">
				<?php echo $this->Form->input('Roseneki.pref_cd', array('type' => 'select', 'options' => $prefs, 'empty' => '選択してください', 'label' => '都道府県')); ?>
			</div>
			<div class="form-group col-xs-4">
				<?php echo $this->Form->input('Roseneki.line_cd', array('type' => 'select', 'options' => $lines, 'empty' => '選択してください', 'label' => '最寄り路線')); ?>
			</div>
			<div class="form-group col-xs-4">
				<?php echo $this->Form->input('Roseneki.station_cd', array('type' => 'select', 'options' => $stations, 'empty' => '選択してください', 'label' => '最寄り駅')); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo $this->Form->submit('送信する', array('id' => 'rosenekiSubmit', 'class' => 'btn btn-primary', 'div' => false)); ?>
			<?php echo $this->Form->button('リセット', array('type' => 'button', 'id' => 'rosenekiReset', 'class' => 'btn btn-warning', 'div' => false)); ?>
		</div>

		<?php echo $this->Form->end(); ?>

	</div>
</div>
<div id="roseneki-message"></div>
