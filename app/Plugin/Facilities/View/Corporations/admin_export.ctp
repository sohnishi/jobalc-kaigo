<?php
// header
$thead = array(
	"法人ID",
	"法人名",
	// "ふりがな"
);

$exportData = array();
foreach ($corporations as $corporation) {
	$exportData[] = array(
		$corporation['Corporation']['id'],
		$corporation['Corporation']['name'],
		// $corporation['Corporation']['hurigana']
	);
}

// Settings
$date = date('YmdHis');
$this->FastCSV->filename = '法人ID一覧' . $date;
$this->FastCSV->setRows($exportData);
$this->FastCSV->setFirstRow($thead);
// $this->FastCSV->to_encoding = 'sjis';
// $this->FastCSV->from_encoding = 'utf8';

// Export
$this->FastCSV->export();
