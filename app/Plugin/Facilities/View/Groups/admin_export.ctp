<?php
// header
$thead = array(
	"グループID",
	"グループ名",
	// "ふりがな"
);

$exportData = array();
foreach ($groups as $group) {
	$exportData[] = array(
		$group['Group']['id'],
		$group['Group']['name'],
		// $group['Group']['hurigana']
	);
}

// Settings
$date = date('YmdHis');
$this->FastCSV->filename = 'グループID一覧' . $date;
$this->FastCSV->setRows($exportData);
$this->FastCSV->setFirstRow($thead);
// $this->FastCSV->to_encoding = 'sjis';
// $this->FastCSV->from_encoding = 'utf8';

// Export
$this->FastCSV->export();
