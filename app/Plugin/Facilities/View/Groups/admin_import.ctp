<?php if (!empty($errors)) : ?>
<div class="row">
	<div class="col-xs-12">
		<div class="alert alert-danger">
			<dl>
			<?php foreach ($errors as $error) : ?>
				<dt><?php echo sprintf('%s 行目の', $error['row']); ?></dt>
				<?php foreach ($error['msg'] as $key => $msg) : ?>
				<dd><?php echo sprintf('%s カラムは %s', substr($key, 0, -1), $msg); ?></dd>
				<?php endforeach; ?>
			<?php endforeach; ?>
			</dl>
		</div>
	</div>
</div>
<?php else : ?>
<div class="row">
	<div class="col-xs-12">
		<div class="alert alert-info">
			<ul class="list-unstyled">
				<li><i class="fa fa-info-circle"></i> グループのCSVファイルは規定のフォーマット(SJIS, ExcelからCSVエクスポートしたファイル)で作成してください。データ形式が違う場合は正常にインポートすることができません。</li>
			</ul>
		</div>
	</div>
</div>
<?php endif; ?>

<div class="row">
	<div class="col-xs-12">
		<?php
			echo $this->Form->create($model, array(
				'inputDefaults' => array(
					'label' => false,
					'wrapInput' => false,
					'div' => false,
					'class' => 'form-control'
				),
				'action' => 'import',
				'type' => 'file'
			));
		?>
		<div class="panel panel-default">
			<div class="panel-heading">CSVファイルをインポート</div>
			<div class="panel-body">
				<div class="form-group">
					<?php echo $this->Form->input('csvfile', array('type' => 'file', 'label' => 'CSVファイルを選択', 'class' => false)); ?>
				</div>
			</div>
			<div class="panel-footer">
				<?php echo $this->Form->submit('アップロードする', array('class' => 'btn btn-primary')); ?>
			</div>
		</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-default">
			<div class="panel-heading">グループ一覧をエクスポート</div>
			<div class="panel-body">
				<p>グループ一覧をCSV形式で書き出すことができます。</p>
				<a href="<?php echo $this->Html->url('/admin/facilities/groups/export'); ?>" class="btn btn-success">グループ一覧を書き出す</a>
			</div>
		</div>
	</div>
</div>
