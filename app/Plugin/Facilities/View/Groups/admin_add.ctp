<?php
/*
	20190614 sohnishi
	自動かな入力
*/
echo $this->Html->script('Forms.jquery.autoKana.js');
?>
<div class="row">
	<div class="col-xs-12 contents-actions">
		<ul class="nav nav-tabs">
			<li><a href="<?php echo $this->Html->url('/admin/facilities/groups'); ?>">グループ名一覧</a></li>
			<li class="active"><a href="#">グループ名を新規登録</a></li>
		</ul>
	</div>
</div>

<div class="row">
	<div class="col-xs-4 col-xs-offset-4">

		<div class="panel panel-default">
			<div class="panel-heading">グループ名を新規登録</div>
			<?php
				echo $this->Form->create($model, array(
					'inputDefaults' => array(
						'label' => false,
						'wrapInput' => false,
						'div' => false,
						'class' => 'form-control'
					)
				));
			?>
			<div class="panel-body">

				<div class="form-group">
					<?php echo $this->Form->input('name', array('label' => 'グループ名')); ?>
				</div>
				<!-- 
					20190614 sohnihsi
					ふりがな項目追加
				-->
				<!-- <div class="form-group">
					<?php //echo $this->Form->input('hurigana', array('label' => 'ふりがな')); ?>
				</div> -->

			</div>
			<div class="panel-footer">
				<?php echo $this->Form->submit('グループ名を登録する', array('class' => 'btn btn-primary')); ?>
			</div>
			<?php echo $this->Form->end(); ?>
		</div>

	</div>
</div>
<!-- 
	20190614 sohnishi
	自動かな入力
-->
<script>
$.fn.autoKana('#GroupName', '#GroupHurigana', {katakana: false});
</script>