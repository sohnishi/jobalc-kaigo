<div class="row">
	<div class="col-xs-12 contents-actions">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#">グループ名一覧</a></li>
			<li><a href="<?php echo $this->Html->url('/admin/facilities/groups/add'); ?>">グループ名を新規登録</a></li>
			<li><a href="<?php echo $this->Html->url('/admin/facilities/groups/import'); ?>">CSV</a></li>
		</ul>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">

		<div class="panel panel-default">
			<div class="panel-heading">グループ名一覧</div>
			<div class="panel-body">

				<table class="table table-striped">
					<thead>
						<tr>
							<th><?php echo $this->Paginator->sort('id', 'ID'); ?></th>
							<th><?php echo $this->Paginator->sort('name', 'グループ名'); ?></th>
							<!-- <th><?php //echo $this->Paginator->sort('hurigana', 'ふりがな'); ?></th> -->
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($groups as $group) : ?>
						<tr>
							<td><?php echo $group[$model]['id']; ?></td>
							<td><?php echo $group[$model]['name']; ?></td>
							<!-- <td><?php //echo $group[$model]['hurigana']; ?></td> -->
							<td>
								<ul class="list-inline">
									<li><?php echo $this->Html->link('編集', array('action' => 'edit', $group[$model]['id'])); ?></li>
									<li><?php echo $this->Form->postLink('削除', array('action' => 'delete', $group[$model]['id']), '', '本当に削除しますか？元には戻せません'); ?></li>
								</ul>
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>

				<div class="text-center">
					<?php echo $this->Paginator->pagination(array('ul' => 'pagination')); ?>
				</div>
			</div>
		</div>

	</div>
</div>
