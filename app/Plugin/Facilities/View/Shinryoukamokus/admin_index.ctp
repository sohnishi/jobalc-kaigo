<div class="row">
	<div class="col-xs-12 contents-actions">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#">診療科目一覧</a></li>
			<li><a href="<?php echo $this->Html->url('/admin/facilities/shinryoukamokus/add'); ?>">診療科目の新規登録</a></li>
		</ul>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">

		<div class="panel panel-default">
			<div class="panel-heading">診療科目一覧</div>
			<div class="panel-body">

				<table class="table table-striped">
					<thead>
						<tr>
							<th><?php echo $this->Paginator->sort('id', 'ID'); ?></th>
							<th><?php echo $this->Paginator->sort('name', '診療科目名'); ?></th>
							<th><?php echo $this->Paginator->sort('weight', '並び順'); ?></th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($shinryoukamokus as $shinryoukamoku) : ?>
						<tr>
							<td><?php echo $shinryoukamoku['Shinryoukamoku']['id']; ?></td>
							<td><?php echo $shinryoukamoku['Shinryoukamoku']['name']; ?></td>
							<td><?php echo $shinryoukamoku['Shinryoukamoku']['weight']; ?></td>
							<td>
								<ul class="list-inline">
									<li><?php echo $this->Html->link('編集', array('action' => 'edit', $shinryoukamoku['Shinryoukamoku']['id'])); ?></li>
									<li><?php echo $this->Form->postLink('削除', array('action' => 'delete', $shinryoukamoku['Shinryoukamoku']['id']), '', '本当に削除しますか？元には戻せません'); ?></li>
								</ul>
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>

				<div class="text-center">
					<?php echo $this->Paginator->pagination(array('ul' => 'pagination')); ?>
				</div>
			</div>
		</div>

	</div>
</div>