<div class="row">
	<div class="col-xs-12 contents-actions">
		<ul class="nav nav-tabs">
			<li><a href="<?php echo $this->Html->url('/admin/facilities/shinryoukamokus'); ?>">診療科目一覧</a></li>
			<li class="active"><a href="#">診療科目の更新</a></li>
		</ul>
	</div>
</div>

<div class="row">
	<div class="col-xs-4 col-xs-offset-4">

		<div class="panel panel-default">
			<div class="panel-heading">診療科目の更新</div>
			<?php
				echo $this->Form->create($model, array(
					'inputDefaults' => array(
						'label' => false,
						'wrapInput' => false,
						'div' => false,
						'class' => 'form-control'
					)
				));
				echo $this->Form->input('id');
			?>
			<div class="panel-body">
				<div class="form-group">
					<?php echo $this->Form->input('name', array('label' => '診療科目名')); ?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('weight', array('type' => 'text', 'label' => '並び順')); ?>
				</div>
			</div>
			<div class="panel-footer">
				<?php echo $this->Form->submit('診療科目を更新する', array('class' => 'btn btn-primary')); ?>
			</div>
			<?php echo $this->Form->end(); ?>
		</div>

	</div>
</div>
