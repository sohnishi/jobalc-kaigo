<?php

App::uses('FacilitiesAppController', 'Facilities.Controller');

class CorporationsController extends FacilitiesAppController {

	public $name = 'Corporations';

	public $uses = array('Facilities.Corporation');

	public $components = array(
		'Csv'
	);

	public $helpers = array(
		'FastCSV'
	);


	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('model', $this->modelClass);
		$this->set('title_for_layout', '法人名マスタ');
		$this->Security->unlockedActions = array('admin_typeahead');
		$this->set('currentUser', $this->Auth->user());

	}

	public function admin_index() {
		$this->Paginator->settings[$this->modelClass] = array(
			'order' => array($this->modelClass . '.id' => 'DESC')
		);
		$this->set('corporations', $this->Paginator->paginate());
	}

	public function admin_add() {
		if (!empty($this->request->data)) {
			$this->{$this->modelClass}->create();
			if ($this->{$this->modelClass}->save($this->request->data)) {
				$this->flashMsg('法人名を登録しました');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->flashMsg('法人名を登録中にエラーが発生しました', 'alert-warning');
			}
		}
	}

	public function admin_edit($id = null) {
		if (!$id || !$this->{$this->modelClass}->exists($id)) {
			$this->flashMsg('無効な操作です');
			$this->redirect(array('action' => 'index'));
		}

		if (!empty($this->request->data)) {
			if ($this->{$this->modelClass}->save($this->request->data)) {
				$this->flashMsg('法人名を更新しました');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->flashMsg('法人名を更新中にエラーが発生しました', 'alert-warning');
			}
		} else {
			$this->request->data = $this->{$this->modelClass}->read(null, $id);
		}
	}

	public function admin_delete($id = null) {
		if (!$id) {
			$this->flashMsg('無効な操作です', 'alert-danger');
			$this->redirect(array('action' => 'index'));
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->{$this->modelClass}->delete($id)) {
				$this->flashMsg('法人名を削除しました');
			} else {
				$this->flashMsg('法人名を削除中にエラーが発生しました', 'alert-warning');
			}
		} else {
			$this->flashMsg('無効な操作です', 'alert-danger');
		}
		$this->redirect(array('action' => 'index'));
	}

	public function admin_typeahead() {
		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			Configure::write('debug', 0);
			$turm = $this->request->query['q'];
			$results = $this->{$this->modelClass}->find('all', array(
				'conditions' => array(
					$this->modelClass . '.name LIKE' => $turm . '%'
				)
			));
			$corporations = array();
			foreach ($results as $key => $result) {
				$val['id'] = $result[$this->modelClass]['id'];
				$val['name'] = $result[$this->modelClass]['name'];
				array_push($corporations, $val);
			}
			echo json_encode($corporations);
		}
	}

	public function admin_import() {
		$this->set('title_for_layout', '法人CSVインポート');
		if ($this->request->is('post')) {
			// 現在のレコードをカウント
			$record_count = $this->{$this->modelClass}->find('count');

			$err_flg = false;
			$err_msg = array();

			$_data = $this->Csv->import($this->request->data[$this->modelClass]['csvfile']['tmp_name'], array(), array('encode' => 'sjis-win'));
			$data = array();
			foreach ($_data as $key => $item) {
				// 施設形態 - 大区分処理
				$data[$key]['Corporation']['id'] = current(array_slice($_data[$key]['Corporation'], 0, 1, true));
				$data[$key]['Corporation']['name'] = current(array_slice($_data[$key]['Corporation'], 1, 1, true));
				// $data[$key]['Corporation']['hurigana'] = current(array_slice($_data[$key]['Corporation'], 2, 1, true));
				
			}

			// バリデーション処理
			foreach ($data as $key => $item) {
				if (!$this->{$this->modelClass}->saveAll($item, array('validate' => 'only', 'deep' => true))) {
					$err_msg[$key]['row'] = $key + 1;
					$err_msg[$key]['msg'] = Hash::flatten($this->{$this->modelClass}->validationErrors);
					$err_flg = true;
				}
			}

			if ($err_flg) {
				// バリデーションエラーをビューへ渡す
				$errors = $err_msg;
				$this->set('errors', $errors);
				$this->flashMsg('インポートエラー' . ' ' . $this->request->data[$this->modelClass]['csvfile']['name'] . ', データに不具合があります', 'alert-danger');
			} else {
				if ($_data) {
					foreach ($data as $key => $item) {
						$this->{$this->modelClass}->create();
						$this->{$this->modelClass}->saveAll($item, array('validate' => false));
					}
					// 保存後のレコードをカウントして差分を求める
					$new_records_count = $this->{$this->modelClass}->find('count') - $record_count;
					$this->flashMsg('インポートが完了しました' . ' ' . $new_records_count . ' レコード : ' . $this->request->data[$this->modelClass]['csvfile']['name']);
					$this->redirect(array('action' => 'index'));
				}
			}
		}
	}

	public function admin_export() {
		$this->set('title_for_layout', '法人エクスポート');
		$this->layout = false;

		$corporations = $this->{$this->modelClass}->find('all');
		$this->set('corporations', $corporations);
	}

}
