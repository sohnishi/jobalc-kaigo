<?php

App::uses('FacilitiesAppController', 'Facilities.Controller');

class FacilitiesController extends FacilitiesAppController {

	public $name = 'Facilities';

	public $uses = array('Facilities.Facility', 'Facilities.Group', 'Facilities.Corporation', 'Facilities.FacilityPreliminaryItem', 'Users.User', 'Facilities.FacilityShow', 'Facilities.ShisetsukeitaiFirst', 'Facilities.ShisetsukeitaiSecond', 'Facilities.Shinryoukamoku');

	public $components = array(
		'Ekidata.Railroader' => array(
			'pref' => array(
				'id' => 'pref_cd',
				'list' => 'prefs'
			),
			'line' => array(
				'id' => 'line_cd',
				'list' => 'lines'
			),
			'station' => array(
				'id' => 'station_cd',
				'list' => 'stations'
			)
		),
		/*
			20190515 sohnishi
			施設予備項目がArrayのため未入力でも検索に走ってしまう事象を回避するため、filterEmptyを追加
		*/
		'Search.Prg' => array(
			'commonProcess' => array(	
				'filterEmpty' =>  true,	
			)
		),
		'Csv'
	);

	public $helpers = array(
		'FastCSV'
	);

	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('model', $this->modelClass);
		$this->set('title_for_layout', '施設管理');
		$this->Security->unlockedActions = array('admin_index', 'admin_add');
		$this->set('currentUser', $this->Auth->user());

	}

	
	function array_column ($target_data, $column_key, $column_key2, $wrap_key, $index_key = null) {

		if (is_array($target_data) === FALSE || count($target_data) === 0) return FALSE;

		$result = array();
		foreach ($target_data as $array) {
			if (array_key_exists($column_key, $array[$wrap_key]) === FALSE) continue;
			if (array_key_exists($column_key2, $array[$wrap_key]) === FALSE) continue;
			if (is_null($index_key) === FALSE && array_key_exists($index_key, $array) === TRUE) {
				$result[$array[$index_key]] = $array[$wrap_key][$column_key];
				continue;
			}
			if (array_key_exists($array[$wrap_key][$column_key], $result)) {
				if (is_string($result[$array[$wrap_key][$column_key]])) {

					$result[$array[$wrap_key][$column_key]] = array(
						$result[$array[$wrap_key][$column_key]],
						$array[$wrap_key][$column_key2]
					);

				} else {
					array_push($result[$array[$wrap_key][$column_key]], $array[$wrap_key][$column_key2]);
				}
			} else {
				$result[$array[$wrap_key][$column_key]] = $array[$wrap_key][$column_key2];
			}
		}

		if (count($result) === 0) return FALSE;

		return $result;

	}
	
	/**
	 * 指定したキーの値を取得する。2次元配列のみ対応
	 * @param target_data 値を取り出したい多次元配列
	 * @param column_key  値を返したいカラム
	 * @param index_key   返す配列のインデックスとして使うカラム
	 * return array       入力配列の単一のカラムを表す値の配列を返し
	 */
	function array_column2 ($target_data, $column_key, $column_key2, $index_key = null) {

		if (is_array($target_data) === FALSE || count($target_data) === 0) return FALSE;

		$result = array();
		foreach ($target_data as $array) {
			if (array_key_exists($column_key, $array) === FALSE) continue;
			if (array_key_exists($column_key2, $array[$column_key]) === FALSE) continue;
			if (is_null($index_key) === FALSE && array_key_exists($index_key, $array) === TRUE) {
				$result[$array[$index_key]] = $array[$column_key];
				continue;
			}
			$result[] = $array[$column_key][$column_key2];
		}

		if (count($result) === 0) return FALSE;
		return $result;

	}


	function array_column_by_facility_shows ($target_data) {

		if (is_array($target_data) === FALSE || count($target_data) === 0) return FALSE;

		$result = array();
		foreach ($target_data as $array) {

			$result[$array['facility_shows']['facility_id']] = array(
				'groupmei_chk' => $array['facility_shows']['groupmei_chk'] ? 1 : 0,
				'houjinmei_chk' => $array['facility_shows']['houjinmei_chk'] ? 1 : 0,
				'shinryoukamoku_chk' => $array['facility_shows']['shinryoukamoku_chk'] ? 1 : 0,
				'byousyou_chk' => $array['facility_shows']['byousyou_chk'] ? 1 : 0,
				'byoutousyousai_chk' => $array['facility_shows']['byoutousyousai_chk'] ? 1 : 0,
				'kangokijun_chk' => $array['facility_shows']['kangokijun_chk'] ? 1 : 0,
				'kyukyushitei_chk' => $array['facility_shows']['kyukyushitei_chk'] ? 1 : 0,
				'kyukyusyousai_chk' => $array['facility_shows']['kyukyusyousai_chk'] ? 1 : 0,
				'syujyutsusyousai_chk' => $array['facility_shows']['syujyutsusyousai_chk'] ? 1 : 0,
				'karute_chk' => $array['facility_shows']['karute_chk'] ? 1 : 0,
				'syokuinkousei_chk' => $array['facility_shows']['syokuinkousei_chk'] ? 1 : 0,
				'tokutyou_chk' => $array['facility_shows']['tokutyou_chk'] ? 1 : 0,
				'kanrenshisetsu_chk' => $array['facility_shows']['kanrenshisetsu_chk'] ? 1 : 0,
				'kyouikukensyu_chk' => $array['facility_shows']['kyouikukensyu_chk'] ? 1 : 0
			);
			
		}

		if (count($result) === 0) return FALSE;

		return $result;

	}

	public function admin_index() {
// Configure::write('debug', 0);
		$this->Prg->commonProcess();

		if (!empty($this->request->data)) {
			unset($this->{$this->modelClass}->validate['number']);
			unset($this->{$this->modelClass}->validate['shisetsumei']);
			unset($this->{$this->modelClass}->validate['zip']);
			$this->Paginator->settings['conditions'] = array(
					$this->{$this->modelClass}->parseCriteria($this->Prg->parsedParams())
			);
			$this->Session->write('querystring', $this->request->query);
		} else {
			$this->Session->delete('querystring');
		}

		$this->Paginator->settings['limit'] = 50;
		$this->Paginator->settings['order'] = array($this->modelClass . '.number' => 'ASC');
		$this->set('facilities', $this->Paginator->paginate());

		// 施設形態 - 大区分
		$shisetsukeitaiFirsts = $this->{$this->modelClass}->ShisetsukeitaiFirst->find('list', array('order' => 'ShisetsukeitaiFirst.weight ASC'));
		$this->set('shisetsukeitaiFirsts', $shisetsukeitaiFirsts);
		// 施設形態 - 中区分
		$shisetsukeitaiSeconds = $this->{$this->modelClass}->ShisetsukeitaiSecond->find('list', array('order' => 'ShisetsukeitaiSecond.weight ASC'));
		$this->set('shisetsukeitaiSeconds', $shisetsukeitaiSeconds);
		// 診療科目リストを取得
		$shinryoukamokus = $this->{$this->modelClass}->Shinryoukamoku->find('list', array('order' => 'Shinryoukamoku.weight ASC'));
		$this->set('shinryoukamokus', $shinryoukamokus);
		/*
			20190515 sohnishi
			施設予備項目リストを取得
		*/
		$facilityPreliminaryItems = $this->FacilityPreliminaryItem->find('list', array('order' => 'FacilityPreliminaryItem.id ASC'));
		$this->set('facilityPreliminaryItems', $facilityPreliminaryItems);

	}

	public function admin_view($id = null) {
Configure::write('debug', 0);
		$this->Prg->commonProcess();

		unset($this->{$this->modelClass}->validate['number']);
		unset($this->{$this->modelClass}->validate['shisetsumei']);
		unset($this->{$this->modelClass}->validate['zip']);
		// 20190708 sohnishi
		// 施設表示でpage:パラメータだと該当の施設が表示されない事象があったため、idに変更
		$this->Paginator->settings['conditions'] = array(
				$this->{$this->modelClass}->parseCriteria($this->Prg->parsedParams())
				// 'Facility.id' => $id
		);
		$this->Paginator->settings['contain'] = array(
			'FacilityShow',
			'ShisetsukeitaiFirst',
			'ShisetsukeitaiSecond',
			'Shinryoukamoku',
			/*
				20190508 sohnishi
				施設予備項目登録内容を追加
			*/
			'FacilityPreliminaryItem' => array(
				'order' => 'FacilityPreliminaryItem.id ASC',
			),
			'Staff' => array(
				'fields' => array('id', 'name'),
				'order' => array(
					'Staff.primary_check' => 'DESC',
					'Staff.busyo' => 'ASC'
				)
			),
			'Recruit' => array(
				'order' => array(
					'Recruit.created' => 'DESC'
				),
				'ShisetsukeitaiSecond'
			)
		);

		$this->Paginator->settings['limit'] = 1;
		$this->Paginator->settings['order'] = array($this->modelClass . '.number' => 'ASC');
		$aaa = $this->Paginator;
		$this->set('facilities', $this->Paginator->paginate());

		// 施設形態 - 大区分
		$shisetsukeitaiFirsts = $this->{$this->modelClass}->ShisetsukeitaiFirst->find('list', array('order' => 'ShisetsukeitaiFirst.weight ASC'));
		$this->set('shisetsukeitaiFirsts', $shisetsukeitaiFirsts);
		// 施設形態 - 中区分
		$shisetsukeitaiSeconds = $this->{$this->modelClass}->ShisetsukeitaiSecond->find('list', array('order' => 'ShisetsukeitaiSecond.weight ASC'));
		$this->set('shisetsukeitaiSeconds', $shisetsukeitaiSeconds);
		// 診療科目リストを取得
		$shinryoukamokus = $this->{$this->modelClass}->Shinryoukamoku->find('list', array('order' => 'Shinryoukamoku.weight ASC'));
		$this->set('shinryoukamokus', $shinryoukamokus);
		// ユーザー情報
		$this->set('currentUser', $this->Auth->user());
		/*
			20190508 sohnishi
			施設予備項目リストを取得
		*/
		$facilityPreliminaryItems = $this->FacilityPreliminaryItem->find('list', array('order' => 'FacilityPreliminaryItem.id ASC'));
		$this->set('facilityPreliminaryItems', $facilityPreliminaryItems);
		/*
			20190523 sohnishi
			連絡履歴相互のため、社内担当追加
		*/
		$users = $this->User->find('list');
		$this->set('users', $users);
	}

	public function admin_add() {

		if (!empty($this->request->data)) {

			$err_flg = false;

			// グループ名を処理
			// グループ名が存在しなければ保存する
			/**
			 * 	20190614 sohnishi
			 * 	存在しないグループ名はエラーとする
			 */
			$groupName = $this->request->data[$this->modelClass]['group'];
			if (!empty($groupName)) {
				$group = $this->Group->findByName($groupName);
				$_group = array();
				if (empty($group)) {
					$_group['Group']['name'] = $groupName;
					$this->Group->create();
					$this->Group->save($_group);
					// $this->flashMsg('グループ名マスタに存在しないグループ名は保存できません', 'alert-warning');
					// $err_flg = true;
				}
				//  else {
				// 	$this->request->data[$this->modelClass]['group_hurigana'] = $group['Group']['hurigana'];
				// }
			}

			// 法人名処理
			// 法人名が存在しなければ保存する

			/**
			 * 	20190614 sohnishi
			 * 	存在しない法人名はエラーとする
			 */
			$corporationName = $this->request->data[$this->modelClass]['corporation'];
			if (!empty($corporationName)) {
				$corporation = $this->Corporation->findByName($corporationName);
				$_corporation = array();
				if (empty($corporation)) {
					$_corporation['Corporation']['name'] = $corporationName;
					$this->Corporation->create();
					$this->Corporation->save($_corporation);
					// $this->flashMsg('法人名マスタに存在しない法人名は保存できません', 'alert-warning');
					// $err_flg = true;
				}
				//  else {
				// 	$this->request->data[$this->modelClass]['corporation_hurigana'] = $corporation['Corporation']['hurigana'];
				// }
			}

			if (!$err_flg) {
				$this->{$this->modelClass}->create();
				if ($this->{$this->modelClass}->saveAll($this->request->data)) {
					$this->flashMsg('施設を登録しました');
					$this->redirect(array('action' => 'index', '?' => $this->Session->read('querystring')));
				} else {
					$this->flashMsg('施設を登録中にエラーが発生しました', 'alert-warning');
				}
			}
		}

		// 施設形態 - 大区分リストを取得
		$shisetsukeitaiFirsts = $this->{$this->modelClass}->ShisetsukeitaiFirst->find('list', array('order' => 'ShisetsukeitaiFirst.weight ASC'));
		$this->set('shisetsukeitaiFirsts', $shisetsukeitaiFirsts);
		// 施設形態 - 中区分リストを取得
		$shisetsukeitaiSeconds = $this->{$this->modelClass}->ShisetsukeitaiSecond->find('list', array('order' => 'ShisetsukeitaiSecond.weight ASC'));
		$this->set('shisetsukeitaiSeconds', $shisetsukeitaiSeconds);
		// 診療科目リストを取得
		$shinryoukamokus = $this->{$this->modelClass}->Shinryoukamoku->find('list', array('order' => 'Shinryoukamoku.weight ASC'));
		$this->set('shinryoukamokus', $shinryoukamokus);
		// 施設予備項目リストを取得
		$facilityPreliminaryItems = $this->FacilityPreliminaryItem->find('list', array('order' => 'FacilityPreliminaryItem.id ASC'));
		$this->set('facilityPreliminaryItems', $facilityPreliminaryItems);
	}

	public function admin_edit($id = null) {
		if (!$id || !$this->{$this->modelClass}->exists($id)) {
			$this->flashMsg('無効な操作です', 'alert-danger');
			$this->redirect(array('action' => 'index'));
		}

		if (!empty($this->request->data)) {

			// グループ名を処理
			// グループ名が存在しなければ保存する
			$groupName = $this->request->data[$this->modelClass]['group'];
			if (!empty($groupName)) {
				$group = $this->Group->findByName($groupName);
				$_group = array();
				if (empty($group)) {
					$_group['Group']['name'] = $groupName;
					$this->Group->create();
					$this->Group->save($_group);
				}
			}

			// 法人名処理
			// 法人名が存在しなければ保存する
			$corporationName = $this->request->data[$this->modelClass]['corporation'];
			if (!empty($corporationName)) {
				$corporation = $this->Corporation->findByName($corporationName);
				$_corporation = array();
				if (empty($corporation)) {
					$_corporation['Corporation']['name'] = $corporationName;
					$this->Corporation->create();
					$this->Corporation->save($_corporation);
				}
			}

			if ($this->{$this->modelClass}->saveAll($this->request->data)) {
				$this->flashMsg('施設情報を更新しました');
				$this->redirect(Hash::merge(array('action' => 'view'), $this->request->params['named'], array('?' => $this->Session->read('querystring'))));
			} else {
				$this->flashMsg('施設情報を更新中にエラーが発生しました', 'alert-warning');
			}
		} else {
			$this->{$this->modelClass}->recursive = 1;
			$this->request->data = $this->{$this->modelClass}->read(null, $id);
		}

		// 施設形態 - 大区分リストを取得
		$shisetsukeitaiFirsts = $this->{$this->modelClass}->ShisetsukeitaiFirst->find('list', array('order' => 'ShisetsukeitaiFirst.weight ASC'));
		$this->set('shisetsukeitaiFirsts', $shisetsukeitaiFirsts);
		// 施設形態 - 中区分リストを取得
		$shisetsukeitaiSeconds = $this->{$this->modelClass}->ShisetsukeitaiSecond->find('list', array('order' => 'ShisetsukeitaiSecond.weight ASC'));
		$this->set('shisetsukeitaiSeconds', $shisetsukeitaiSeconds);
		// 診療科目リストを取得
		$shinryoukamokus = $this->{$this->modelClass}->Shinryoukamoku->find('list', array('order' => 'Shinryoukamoku.weight ASC'));
		$this->set('shinryoukamokus', $shinryoukamokus);
		/*
			20190508 sohnishi
			施設予備項目リストを取得
		*/
		$facilityPreliminaryItems = $this->FacilityPreliminaryItem->find('list', array('order' => 'FacilityPreliminaryItem.id ASC'));
		$this->set('facilityPreliminaryItems', $facilityPreliminaryItems);
		/*
			20190508 sohnishi
			施設 - 施設予備項目中間テーブルリストを取得
		*/
		$facilitiesFacilityPreliminaryItems = $this->{$this->modelClass}->FacilitiesFacilityPreliminaryItem->find('list', 
			array(
				'order' => 'FacilitiesFacilityPreliminaryItem.id ASC',
				'conditions' => 
					array(
						 'FacilitiesFacilityPreliminaryItem.facility_id' => $id
					),
				'fields' => array('FacilitiesFacilityPreliminaryItem.facility_preliminary_item_id', 'FacilitiesFacilityPreliminaryItem.content')
				));
		$this->set('facilitiesFacilityPreliminaryItems', $facilitiesFacilityPreliminaryItems);

		$facility = $this->{$this->modelClass}->find('all', array(
			'conditions' => 
				array(
						'Facility.id' => $id
				),
		));
		$this->set('facility', $facility);

	}

	public function admin_delete($id = null) {
		if (!$id || !$this->{$this->modelClass}->exists($id)) {
			$this->flashMsg('無効な操作です', 'alert-danger');
			$this->redirect(array('action' => 'index'));
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->{$this->modelClass}->delete($id)) {
				$this->flashMsg('施設情報を削除しました');
			} else {
				$this->flashMsg('施設情報を削除中にエラーが発生しました', 'alert-warning');
			}
		}
		$this->redirect(array('action' => 'index'));
	}

	/*
		20190521 sohnishi
		施設名typehead追加
		施設名で検索し、施設IDと施設numberと法人名と施設名を返す
	*/
	public function admin_typeahead() {
		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			Configure::write('debug', 0);
			$turm = $this->request->query['q'];
			$this->{$this->modelClass}->recursive = 1;
			$results = $this->{$this->modelClass}->find('all', array(
				'conditions' => array(
					$this->modelClass . '.shisetsumei LIKE' => $turm . '%'
				)
			));
			$facilities = array();
			foreach ($results as $key => $result) {
				$val['id'] = $result[$this->modelClass]['id'];
				$val['number'] = $result[$this->modelClass]['number'];
				$val['name'] = $result[$this->modelClass]['corporation'] . ' ' . $result[$this->modelClass]['shisetsumei'];
				$val['shisetsumei'] = $result[$this->modelClass]['shisetsumei'];
				$val['staffs'] = $result["Staff"];
				array_push($facilities, $val);
			}
			echo json_encode($facilities);
		}
	}

		/*
		20190521 sohnishi
		施設番号で施設を検索
	*/
	public function admin_find_by_number() {
		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			Configure::write('debug', 0);
			$turm = $this->request->query['q'];
			$this->{$this->modelClass}->recursive = 1;
			$results = $this->{$this->modelClass}->find('all', array(
				'conditions' => array(
					$this->modelClass . '.number' => $turm
				)
			));
			$facilities = array();
			foreach ($results as $key => $result) {
				$val['id'] = $result[$this->modelClass]['id'];
				$val['number'] = $result[$this->modelClass]['number'];
				$val['name'] = $result[$this->modelClass]['corporation'] . ' ' . $result[$this->modelClass]['shisetsumei'];
				$val['shisetsumei'] = $result[$this->modelClass]['shisetsumei'];
				$val['staffs'] = $result["Staff"];
				array_push($facilities, $val);
			}
			echo json_encode($facilities);
		}
	}

	public function admin_import() {
		$this->set('title_for_layout', '施設情報CSVインポート');
		if ($this->request->is('post')) {
			// 現在のレコードをカウント
			// $record_count = $this->{$this->modelClass}->find('count');

			$err_flg = false;
			$err_msg = array();

			// 事前にファイルを取得してbomを削除
			$data = file_get_contents($this->request->data[$this->modelClass]['csvfile']['tmp_name']);
			$bom = pack('H*', 'EFBBBF');
			$data = preg_replace("/^$bom/", '', $data);
			$characode = mb_detect_encoding($data).PHP_EOL;
			if(strpos($characode,'UTF-8') === false){
				// 文字コードがutf-8ではない場合、sjis-winをutf8に変換する
				$data = mb_convert_encoding($data, 'utf8', 'sjis-win');
			}
			
			file_put_contents('bom_del_csv.csv', $data);
			$_data = $this->Csv->import('bom_del_csv.csv', array(), array('encode' => 'utf8'));

			foreach ($_data as $key => $item) {
				// 施設形態 - 大区分処理
				if (isset($item['ShisetsukeitaiFirst']['ShisetsukeitaiFirst']) && !empty($item['ShisetsukeitaiFirst']['ShisetsukeitaiFirst'])) {
					$_data[$key]['ShisetsukeitaiFirst']['ShisetsukeitaiFirst'] = explode('|', $item['ShisetsukeitaiFirst']['ShisetsukeitaiFirst']);
				}
				// 施設形態 - 中区分処理
				if (isset($item['ShisetsukeitaiSecond']['ShisetsukeitaiSecond']) && !empty($item['ShisetsukeitaiSecond']['ShisetsukeitaiSecond'])) {
					$_data[$key]['ShisetsukeitaiSecond']['ShisetsukeitaiSecond'] = explode('|', $item['ShisetsukeitaiSecond']['ShisetsukeitaiSecond']);
				}
				// 診療科目処理
				if (isset($item['Shinryoukamoku']['Shinryoukamoku']) && !empty($item['Shinryoukamoku']['Shinryoukamoku'])) {
					$_data[$key]['Shinryoukamoku']['Shinryoukamoku'] = explode('|', $item['Shinryoukamoku']['Shinryoukamoku']);
				}
				// 掲載オプション処理
				if (isset($item['FacilityShow']) && !empty($item['FacilityShow']) && isset($item['Facility']['id']) && !empty($item['Facility']['id'])) {
					$_data[$key]['FacilityShow']['facility_id'] = $item['Facility']['id'];
				}
				/*
					20190514 sohnishi
					施設予備項目処理追加
				*/
				if (isset($item['FacilityPreliminaryItem']['FacilityPreliminaryItem']) && !empty($item['FacilityPreliminaryItem']['FacilityPreliminaryItem'])) {
					// CSVからデータを取得し、カンマで区切って配列にする(2件以上のデータを処理するため)
					// データ構成: facility_preliminary_item_id|content|is_show,facility_preliminary_item_id|content|is_show
					$tmpArray1 = explode(',', $item['FacilityPreliminaryItem']['FacilityPreliminaryItem']);
					$index = 0;
					// データ数ごとに分けた配列をカラムごとに分け直す
					foreach ($tmpArray1 as $tmp1) {
						$tmpArray2[$index] = explode(':', $tmp1);
						$index += 1;
					}
					// CSVで受け取ったデータを削除する(このままの構成ではSaveできないため)
					unset($_data[$key]['FacilityPreliminaryItem']['FacilityPreliminaryItem']);
					$index = 0;
					// Saveできる構成で値を入れ直す
					$facilitiesFacilityPreliminaryItems = array();
					foreach ($tmpArray2 as $facilitiesFacilityPreliminaryItem) {
						$facilitiesFacilityPreliminaryItems['facility_preliminary_item_id'] = $facilitiesFacilityPreliminaryItem[0];
						if (isset($facilitiesFacilityPreliminaryItem[1])) {
							$facilitiesFacilityPreliminaryItems['content'] = $facilitiesFacilityPreliminaryItem[1];
						}
						if (!isset($facilitiesFacilityPreliminaryItem[2])) {
							continue;
						}						
						$facilitiesFacilityPreliminaryItems['is_show'] = $facilitiesFacilityPreliminaryItem[2];
						$_data[$key]['FacilityPreliminaryItem'][$index] = $facilitiesFacilityPreliminaryItems;
						$index += 1;
					}
				}
			// }

			// バリデーション処理
			// foreach ($_data as $key => $item) {
				if (!$this->{$this->modelClass}->saveAll($item, array('validate' => 'only', 'deep' => true))) {
					$err_msg[$key]['row'] = $key + 1;
					$err_msg[$key]['msg'] = Hash::flatten($this->{$this->modelClass}->validationErrors);
					$err_flg = true;
				}
				// // グループ名を処理
				// // グループ名が存在しなければエラーとする
				// $groupName = $item[$this->modelClass]['group'];
				// if (!empty($groupName)) {
				// 	$group = $this->Group->findByName($groupName);
				// 	if (empty($group)) {
				// 		$err_msg[$key]['row'] = $key + 1;
				// 		$err_msg[$key]['msg'] = array_merge($err_msg[$key]['msg'], array('group.0' => 'グループ名マスタに存在するグループ名を入力してください'));
				// 		$err_flg = true;
				// 	}
				// }

				// // 法人名処理
				// // 法人名が存在しなければエラーとする
				// $corporationName = $item[$this->modelClass]['corporation'];
				// if (!empty($corporationName)) {
				// 	$corporation = $this->Corporation->findByName($corporationName);
				// 	if (empty($corporation)) {
				// 		$err_msg[$key]['row'] = $key + 1;
				// 		$err_msg[$key]['msg'] = array_merge($err_msg[$key]['msg'], array('corporation.0' => '法人名マスタに存在する法人名を入力してください'));
				// 		$err_flg = true;
				// 	}
				// }
			}

			if ($err_flg) {
				// バリデーションエラーをビューへ渡す
				$errors = $err_msg;
				$this->set('errors', $errors);
				$this->flashMsg('インポートエラー' . ' ' . $this->request->data[$this->modelClass]['csvfile']['name'] . ', データに不具合があります', 'alert-danger');
			} else {
				if ($_data) {

					foreach ($_data as $key => $item) {
						if (isset($item['Facility']['id']) && !empty($item['Facility']['id'])) {
							// $currentFacilityShow = $this->FacilityShow->find('first', array(
							// 		'fields' => array('id'),
							// 		'conditions' => array('FacilityShow.facility_id' => $item['Facility']['id'])
							// 	)
							// );
							// $item['FacilityShow']['id'] = $currentFacilityShow['FacilityShow']['id'];
							$item['FacilityShow']['id'] = $item['Facility']['id'];
						} else {
							$this->{$this->modelClass}->create();
						}
						$this->{$this->modelClass}->saveAll($item, array('validate' => false, 'deep' => true));
						

						// // グループ名を処理＿
						// // ふりがなを取得する
						// $groupName = $item[$this->modelClass]['group'];
						// if (!empty($groupName)) {
						// 	$group = $this->Group->findByName($groupName);
						// 	if (!empty($group)) {
						// 		$item['Facility']['group_hurigana'] = $group['Group']['hurigana'];
						// 	}
						// }

						// // 法人名処理
						// // 法人名が存在しなければ保存する
						// $corporationName = $item[$this->modelClass]['corporation'];
						// if (!empty($corporationName)) {
						// 	$corporation = $this->Corporation->findByName($corporationName);
						// 	if (!empty($corporation)) {
						// 		$item['Facility']['corporation_hurigana'] = $corporation['Corporation']['hurigana'];
						// 	}
						// }

					}
					// 保存後のレコードをカウントして差分を求める
					// $new_records_count = $this->{$this->modelClass}->find('count') - $record_count;
					// $this->flashMsg('インポートが完了しました' . ' ' . $new_records_count . ' レコード : ' . $this->request->data[$this->modelClass]['csvfile']['name']);
					$this->flashMsg('インポートが完了しました');
					$this->redirect(array('action' => 'index'));
				}
			}
			unlink('bom_del_csv.csv');
		}
	}

	public function admin_export() {
		// $this->set('title_for_layout', '施設一覧エクスポート');
		$this->layout = false;
		$this->autoRender = false;
		$facilities = $this->Facility->getAll();	
		$facilities_facility_shows = $this->FacilityShow->getAll();
		$facilities_shisetsukeitai_firsts = $this->Facility->getAllByShisetsukeitaiFirst();
		$facilities_shisetsukeitai_seconds = $this->Facility->getAllByShisetsukeitaiSecond();
		$facilities_facility_preliminary_items = $this->Facility->getAllByPreliminaryItems();
		$facilities_shinryoukamokus = $this->Facility->getAllByShinryoukamokus();	
		
		$facilities_shisetsukeitai_firsts_shapings = $this->array_column($facilities_shisetsukeitai_firsts, 'facility_id', 'shisetsukeitai_first_id', 'facilities_shisetsukeitai_firsts');
		$facilities_shisetsukeitai_seconds_shapings = $this->array_column($facilities_shisetsukeitai_seconds, 'facility_id', 'shisetsukeitai_second_id', 'facilities_shisetsukeitai_seconds');
		$facilities_shinryoukamokus_shapings = $this->array_column($facilities_shinryoukamokus, 'facility_id', 'shinryoukamoku_id', 'facilities_shinryoukamokus');
		$facilities_facility_shows_shapings = $this->array_column_by_facility_shows($facilities_facility_shows);
		// 出力する値の設定
		$exportData = array();
		foreach ($facilities as $facility) {
			$facility_preliminary_items = '';
			foreach ($facilities_facility_preliminary_items as $item) {
				if ($item['facilities_facility_preliminary_items']['facility_id'] == $facility['facilities']['id']) {
					$facility_preliminary_items = $facility_preliminary_items . ',' . $item['facilities_facility_preliminary_items']['facility_preliminary_item_id'] . ':' . $item['facilities_facility_preliminary_items']['content'] . ':';
					if ($item['facilities_facility_preliminary_items']['is_show']) {
						$facility_preliminary_items = $facility_preliminary_items . '1'; 
					} else {
						$facility_preliminary_items = $facility_preliminary_items . '0';
					}
				}
			}

			$shisetsukeitai_firsts = '';
			if (array_key_exists($facility['facilities']['id'], $facilities_shisetsukeitai_firsts_shapings)) {
				if (is_string($facilities_shisetsukeitai_firsts_shapings[$facility['facilities']['id']])) {
					$shisetsukeitai_firsts = $facilities_shisetsukeitai_firsts_shapings[$facility['facilities']['id']];
				} else {
					foreach ($facilities_shisetsukeitai_firsts_shapings[$facility['facilities']['id']] as $item) {
						if (strlen($shisetsukeitai_firsts) >= 1) {
							$shisetsukeitai_firsts = $shisetsukeitai_firsts . '|' . $item;	
						} else {
							$shisetsukeitai_firsts = $item;
						}
					}
				}
			}

			$shisetsukeitai_seconds = '';
			if (array_key_exists($facility['facilities']['id'], $facilities_shisetsukeitai_seconds_shapings)) {
				if (is_string($facilities_shisetsukeitai_seconds_shapings[$facility['facilities']['id']])) {
					$shisetsukeitai_seconds = $facilities_shisetsukeitai_seconds_shapings[$facility['facilities']['id']];
				} else {
					foreach ($facilities_shisetsukeitai_seconds_shapings[$facility['facilities']['id']] as $item) {
						if (strlen($shisetsukeitai_seconds) >= 1) {
							$shisetsukeitai_seconds = $shisetsukeitai_seconds . '|' . $item;	
						} else {
							$shisetsukeitai_seconds = $item;
						}
					}
				}
			}

			$shinryoukamokus = '';
			if (array_key_exists($facility['facilities']['id'], $facilities_shinryoukamokus_shapings)) {
				if (is_string($facilities_shinryoukamokus_shapings[$facility['facilities']['id']])) {
					$shinryoukamokus = $facilities_shinryoukamokus_shapings[$facility['facilities']['id']];
				} else {
					foreach ($facilities_shinryoukamokus_shapings[$facility['facilities']['id']] as $item) {
						if (strlen($shinryoukamokus) >= 1) {
							$shinryoukamokus = $shinryoukamokus . '|' . $item;	
						} else {
							$shinryoukamokus = $item;
						}
					}
				}
			}
			// $index = array_search($facility['facilities']['id'], $facilities_shisetsukeitai_firsts_shapings);
			// if ($index) {
			// 	$shisetsukeitai_firsts = $shisetsukeitai_firsts . $facilities_shisetsukeitai_firsts_shapings[$index] . '|';
			// }
			// $shisetsukeitai_firsts = '';
			// foreach ($facilities_shisetsukeitai_firsts as $item) {
			// 	if ($item['facilities_shisetsukeitai_firsts']['facility_id'] == $facility['facilities']['id']) {
			// 		$shisetsukeitai_firsts = $shisetsukeitai_firsts . $item['facilities_shisetsukeitai_firsts']['shisetsukeitai_first_id'] . '|';
			// 	}
			// }
			// $shisetsukeitai_seconds = '';
			// foreach ($facilities_shisetsukeitai_seconds as $item) {
			// 	if ($item['facilities_shisetsukeitai_seconds']['facility_id'] == $facility['facilities']['id']) {
			// 		$shisetsukeitai_seconds = $shisetsukeitai_seconds . $item['facilities_shisetsukeitai_seconds']['shisetsukeitai_second_id'] . '|';
			// 	}
			// }
			// $shinryoukamokus = '';
			// foreach ($facilities_shinryoukamokus as $item) {
			// 	if ($item['facilities_shinryoukamokus']['facility_id'] == $facility['facilities']['id']) {
			// 		$shinryoukamokus = $shinryoukamokus . $item['facilities_shinryoukamokus']['shinryoukamoku_id'] . '|';
			// 	}
			// }

			// $facility_shows = array();
			// if (array_key_exists($facility['facilities']['id'], $facilities_shinryoukamokus_shapings)) {
			// 	$index = array_search($facility['facilities']['id'], $facilities_shisetsukeitai_firsts_shapings);
			// 	if ($index) {
			// 		$shisetsukeitai_firsts = $shisetsukeitai_firsts . $facilities_shisetsukeitai_firsts_shapings[$index] . '|';
			// 	}
			// }
			// foreach ($facilities_facility_shows as $item) {
			// 	if ($item['facility_shows']['facility_id'] == $facility['facilities']['id']) {
			// 		$facility_shows = $item;
			// 	}
			// }

			// 0|1|2|3
			$exportData[] = array(
				$facility['facilities']['id'],
				$facility['facilities']['shisetsumei'],
				$facility['facilities']['shisetsumei_hurigana'],
				$facility['facilities']['tel'],
				$facility['facilities']['fax'],
				$facility['facilities']['group'],
				$facility['facilities']['group_hurigana'],
				$facility['facilities']['corporation'],
				$facility['facilities']['corporation_hurigana'],
				$facility['facilities']['keiyaku'],
				$facility['facilities']['keiyakubi'],
				$facility['facilities']['tesuryou'],
				$facility['facilities']['tesuryousyousai'],
				$facility['facilities']['henkinkitei'],
				$facility['facilities']['henkinkiteisyousai'],
				$facility['facilities']['zip'],
				$facility['facilities']['todoufuken'],
				$facility['facilities']['shikutyouson'],
				$facility['facilities']['banchi'],
				$facility['facilities']['tatemono'],
				$facility['facilities']['shisetsu_url'],
				$facility['facilities']['saiyou_url'],
				$facility['facilities']['byousyou'],
				$facility['facilities']['byoutousyousai'],
				$facility['facilities']['kangokijun'],
				$facility['facilities']['kyukyushitei'],
				$facility['facilities']['kyukyusyousai'],
				$facility['facilities']['syujyutsusyousai'],
				$facility['facilities']['karute'],
				$facility['facilities']['syokuinkousei'],
				$facility['facilities']['tokutyou'],
				$facility['facilities']['kanrenshisetsu'],
				$facility['facilities']['kyouikukensyu'],
				$facility_preliminary_items,
				$shisetsukeitai_firsts,
				$shisetsukeitai_seconds,
				$shinryoukamokus,
				$facilities_facility_shows_shapings[$facility['facilities']['id']]['groupmei_chk'],
				$facilities_facility_shows_shapings[$facility['facilities']['id']]['houjinmei_chk'],
				$facilities_facility_shows_shapings[$facility['facilities']['id']]['shinryoukamoku_chk'],
				$facilities_facility_shows_shapings[$facility['facilities']['id']]['byousyou_chk'],
				$facilities_facility_shows_shapings[$facility['facilities']['id']]['byoutousyousai_chk'],
				$facilities_facility_shows_shapings[$facility['facilities']['id']]['kangokijun_chk'],
				$facilities_facility_shows_shapings[$facility['facilities']['id']]['kyukyushitei_chk'],
				$facilities_facility_shows_shapings[$facility['facilities']['id']]['kyukyusyousai_chk'],
				$facilities_facility_shows_shapings[$facility['facilities']['id']]['syujyutsusyousai_chk'],
				$facilities_facility_shows_shapings[$facility['facilities']['id']]['karute_chk'],
				$facilities_facility_shows_shapings[$facility['facilities']['id']]['syokuinkousei_chk'],
				$facilities_facility_shows_shapings[$facility['facilities']['id']]['tokutyou_chk'],
				$facilities_facility_shows_shapings[$facility['facilities']['id']]['kanrenshisetsu_chk'],
				$facilities_facility_shows_shapings[$facility['facilities']['id']]['kyouikukensyu_chk']
			);
		}

		// 保存場所とファイルの設定

		$file = '施設一覧' . date('YmdHis') . '.csv';
 
		// ファイルを書き込み用で開く
		$f = fopen($file, 'w');
		// stream_filter_prepend($f, 'convert.iconv.utf-8/utf-16le');
		// stream_filter_prepend($f, 'convert.iconv.utf-8/cp932');
		fwrite($f, "\xEF\xBB\xBF");
	 
		// 正常にファイルを開けていれば書き込む
		if($f){
	 
			// ヘッダーの出力
			$header = array(
				"Facility.id",
				"Facility.shisetsumei",
				"Facility.shisetsumei_hurigana",
				"Facility.tel",
				"Facility.fax",
				"Facility.group",
				"Facility.group_hurigana",
				"Facility.corporation",
				"Facility.corporation_hurigana",
				"Facility.keiyaku",
				"Facility.keiyakubi",
				"Facility.tesuryou",
				"Facility.tesuryousyousai",
				"Facility.henkinkitei",
				"Facility.henkinkiteisyousai",
				"Facility.zip",
				"Facility.todoufuken",
				"Facility.shikutyouson",
				"Facility.banchi",
				"Facility.tatemono",
				"Facility.shisetsu_url",
				"Facility.saiyou_url",
				"Facility.byousyou",
				"Facility.byoutousyousai",
				"Facility.kangokijun",
				"Facility.kyukyushitei",
				"Facility.kyukyusyousai",
				"Facility.syujyutsusyousai",
				"Facility.karute",
				"Facility.syokuinkousei",
				"Facility.tokutyou",
				"Facility.kanrenshisetsu",
				"Facility.kyouikukensyu",
				"FacilityPreliminaryItem.FacilityPreliminaryItem",
				"ShisetsukeitaiFirst.ShisetsukeitaiFirst",
				"ShisetsukeitaiSecond.ShisetsukeitaiSecond",
				"Shinryoukamoku.Shinryoukamoku",
				"FacilityShow.groupmei_chk",
				"FacilityShow.houjinmei_chk",
				"FacilityShow.shinryoukamoku_chk","FacilityShow.byousyou_chk",
				"FacilityShow.byoutousyousai_chk",
				"FacilityShow.kangokijun_chk",
				"FacilityShow.kyukyushitei_chk",
				"FacilityShow.kyukyusyousai_chk",
				"FacilityShow.syujyutsusyousai_chk",
				"FacilityShow.karute_chk",
				"FacilityShow.syokuinkousei_chk",
				"FacilityShow.tokutyou_chk",
				"FacilityShow.kanrenshisetsu_chk",
				"FacilityShow.kyouikukensyu_chk"
			);
			fputcsv($f, $header, ",");
	 
			// データの出力
			foreach($exportData as $_exportData){
	 
				fputcsv($f, $_exportData, ",");
				// ファイルに書き込み
				// fputcsv($f, $data);
	 
			}
	 
			// mb_convert_encoding($f, 'SJIS-win', 'UTF-8');
			// ファイルを閉じる
			fclose($f);

			// ダウンロード開始
			// header('Content-Type: application/octet-stream');
			header('Content-Type: text/csv; charset=utf8');

			// ここで渡されるファイルがダウンロード時のファイル名になる
			
			header('Content-Disposition: attachment; filename=' . $file);
			// header('Content-Transfer-Encoding: binary');
			header('Content-Length: ' . filesize($file));
			// echo mb_convert_encoding($file,"sjis-win", "UTF-8");
			readfile($file);
		}

		
	}
	

	public function view($id = null) {
		
		if (!$id || !$this->{$this->modelClass}->exists($id)) {
			$this->flashMsg('お探しの施設は見つかりませんでした', 'alert-danger');
			$this->redirect('/');
		}
		
		$detail = $this->{$this->modelClass}->find('first', array(
			'conditions' => array(
				$this->modelClass . '.id' => $id
			),
			'contain' => array(
				'FacilityShow',
				'ShisetsukeitaiSecond',
				'Shinryoukamoku',
				'Roseneki' => array(
					'RailroadPref',
					'RailroadLine',
					'RailroadStation'
				),
				'Recruit' => array(
					'conditions' => array(
						'Recruit.active' => 1
					),
					'order' => array(
						'Recruit.modified' => 'DESC'
					),
					'RecruitShow',
					'ShisetsukeitaiSecond'
				)
			)
		));
		$this->set('detail', $detail);

		$this->set('title_for_layout', sprintf('%sの求人一覧', $detail['Facility']['shisetsumei']));
	}

	
}
