<?php

App::uses('FacilitiesAppController', 'Facilities.Controller');

class FacilitiesController extends FacilitiesAppController {

	public $name = 'Facilities';

	public $uses = array('Facilities.Facility', 'Facilities.Group', 'Facilities.Corporation');

	public $components = array(
		'Ekidata.Railroader' => array(
			'pref' => array(
				'id' => 'pref_cd',
				'list' => 'prefs'
			),
			'line' => array(
				'id' => 'line_cd',
				'list' => 'lines'
			),
			'station' => array(
				'id' => 'station_cd',
				'list' => 'stations'
			)
		),
		'Search.Prg',
		'Csv'
	);

	public $helpers = array(
		'FastCSV'
	);

	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('model', $this->modelClass);
		$this->set('title_for_layout', '施設管理');
		$this->Security->unlockedActions = array('admin_index', 'admin_add');
	}

	public function admin_index() {
Configure::write('debug', 0);
		$this->Prg->commonProcess();
		if (!empty($this->request->data)) {
			unset($this->{$this->modelClass}->validate['number']);
			unset($this->{$this->modelClass}->validate['shisetsumei']);
			unset($this->{$this->modelClass}->validate['zip']);
			$this->Paginator->settings['conditions'] = array(
					$this->{$this->modelClass}->parseCriteria($this->Prg->parsedParams())
			);
			$this->Session->write('querystring', $this->request->query);
		} else {
			$this->Session->delete('querystring');
		}

		$this->Paginator->settings['limit'] = 50;
		$this->Paginator->settings['order'] = array($this->modelClass . '.number' => 'ASC');
		$this->set('facilities', $this->Paginator->paginate());

		// 施設形態 - 大区分
		$shisetsukeitaiFirsts = $this->{$this->modelClass}->ShisetsukeitaiFirst->find('list', array('order' => 'ShisetsukeitaiFirst.weight ASC'));
		$this->set('shisetsukeitaiFirsts', $shisetsukeitaiFirsts);
		// 施設形態 - 中区分
		$shisetsukeitaiSeconds = $this->{$this->modelClass}->ShisetsukeitaiSecond->find('list', array('order' => 'ShisetsukeitaiSecond.weight ASC'));
		$this->set('shisetsukeitaiSeconds', $shisetsukeitaiSeconds);
		// 診療科目リストを取得
		$shinryoukamokus = $this->{$this->modelClass}->Shinryoukamoku->find('list', array('order' => 'Shinryoukamoku.weight ASC'));
		$this->set('shinryoukamokus', $shinryoukamokus);

	}

	public function admin_view() {
Configure::write('debug', 0);
		$this->Prg->commonProcess();
		unset($this->{$this->modelClass}->validate['number']);
		unset($this->{$this->modelClass}->validate['shisetsumei']);
		unset($this->{$this->modelClass}->validate['zip']);
		$this->Paginator->settings['conditions'] = array(
				$this->{$this->modelClass}->parseCriteria($this->Prg->parsedParams())
		);
		$this->Paginator->settings['contain'] = array(
			'FacilityShow',
			'ShisetsukeitaiFirst',
			'ShisetsukeitaiSecond',
			'Shinryoukamoku',
			'Staff' => array(
				'fields' => array('id', 'name'),
				'order' => array(
					'Staff.primary_check' => 'DESC',
					'Staff.busyo' => 'ASC'
				)
			),
			'Recruit' => array(
				'order' => array(
					'Recruit.created' => 'DESC'
				),
				'ShisetsukeitaiSecond'
			)
		);

		$this->Paginator->settings['limit'] = 1;
		$this->Paginator->settings['order'] = array($this->modelClass . '.number' => 'ASC');
		$this->set('facilities', $this->Paginator->paginate());

		// 施設形態 - 大区分
		$shisetsukeitaiFirsts = $this->{$this->modelClass}->ShisetsukeitaiFirst->find('list', array('order' => 'ShisetsukeitaiFirst.weight ASC'));
		$this->set('shisetsukeitaiFirsts', $shisetsukeitaiFirsts);
		// 施設形態 - 中区分
		$shisetsukeitaiSeconds = $this->{$this->modelClass}->ShisetsukeitaiSecond->find('list', array('order' => 'ShisetsukeitaiSecond.weight ASC'));
		$this->set('shisetsukeitaiSeconds', $shisetsukeitaiSeconds);
		// 診療科目リストを取得
		$shinryoukamokus = $this->{$this->modelClass}->Shinryoukamoku->find('list', array('order' => 'Shinryoukamoku.weight ASC'));
		$this->set('shinryoukamokus', $shinryoukamokus);
		// ユーザー情報
		$this->set('currentUser', $this->Auth->user());
	}

	public function admin_add() {
		if (!empty($this->request->data)) {

			// グループ名を処理
			// グループ名が存在しなければ保存する
			$groupName = $this->request->data[$this->modelClass]['group'];
			if (!empty($groupName)) {
				$group = $this->Group->findByName($groupName);
				$_group = array();
				if (empty($group)) {
					$_group['Group']['name'] = $groupName;
					$this->Group->create();
					$this->Group->save($_group);
				}
			}

			// 法人名処理
			// 法人名が存在しなければ保存する
			$corporationName = $this->request->data[$this->modelClass]['corporation'];
			if (!empty($corporationName)) {
				$corporation = $this->Corporation->findByName($corporationName);
				$_corporation = array();
				if (empty($corporation)) {
					$_corporation['Corporation']['name'] = $corporationName;
					$this->Corporation->create();
					$this->Corporation->save($_corporation);
				}
			}

			$this->{$this->modelClass}->create();
			if ($this->{$this->modelClass}->saveAll($this->request->data)) {
				$this->flashMsg('施設を登録しました');
				$this->redirect(array('action' => 'index', '?' => $this->Session->read('querystring')));
			} else {
				$this->flashMsg('施設を登録中にエラーが発生しました', 'alert-warning');
			}
		}

		// 施設形態 - 大区分リストを取得
		$shisetsukeitaiFirsts = $this->{$this->modelClass}->ShisetsukeitaiFirst->find('list', array('order' => 'ShisetsukeitaiFirst.weight ASC'));
		$this->set('shisetsukeitaiFirsts', $shisetsukeitaiFirsts);
		// 施設形態 - 中区分リストを取得
		$shisetsukeitaiSeconds = $this->{$this->modelClass}->ShisetsukeitaiSecond->find('list', array('order' => 'ShisetsukeitaiSecond.weight ASC'));
		$this->set('shisetsukeitaiSeconds', $shisetsukeitaiSeconds);
		// 診療科目リストを取得
		$shinryoukamokus = $this->{$this->modelClass}->Shinryoukamoku->find('list', array('order' => 'Shinryoukamoku.weight ASC'));
		$this->set('shinryoukamokus', $shinryoukamokus);
	}

	public function admin_edit($id = null) {
		if (!$id || !$this->{$this->modelClass}->exists($id)) {
			$this->flashMsg('無効な操作です', 'alert-danger');
			$this->redirect(array('action' => 'index'));
		}

		if (!empty($this->request->data)) {

			// グループ名を処理
			// グループ名が存在しなければ保存する
			$groupName = $this->request->data[$this->modelClass]['group'];
			if (!empty($groupName)) {
				$group = $this->Group->findByName($groupName);
				$_group = array();
				if (empty($group)) {
					$_group['Group']['name'] = $groupName;
					$this->Group->create();
					$this->Group->save($_group);
				}
			}

			// 法人名処理
			// 法人名が存在しなければ保存する
			$corporationName = $this->request->data[$this->modelClass]['corporation'];
			if (!empty($corporationName)) {
				$corporation = $this->Corporation->findByName($corporationName);
				$_corporation = array();
				if (empty($corporation)) {
					$_corporation['Corporation']['name'] = $corporationName;
					$this->Corporation->create();
					$this->Corporation->save($_corporation);
				}
			}

			if ($this->{$this->modelClass}->saveAll($this->request->data)) {
				$this->flashMsg('施設情報を更新しました');
				$this->redirect(Hash::merge(array('action' => 'view'), $this->request->params['named'], array('?' => $this->Session->read('querystring'))));
			} else {
				$this->flashMsg('施設情報を更新中にエラーが発生しました', 'alert-warning');
			}
		} else {
			$this->{$this->modelClass}->recursive = 1;
			$this->request->data = $this->{$this->modelClass}->read(null, $id);
		}

		// 施設形態 - 大区分リストを取得
		$shisetsukeitaiFirsts = $this->{$this->modelClass}->ShisetsukeitaiFirst->find('list', array('order' => 'ShisetsukeitaiFirst.weight ASC'));
		$this->set('shisetsukeitaiFirsts', $shisetsukeitaiFirsts);
		// 施設形態 - 中区分リストを取得
		$shisetsukeitaiSeconds = $this->{$this->modelClass}->ShisetsukeitaiSecond->find('list', array('order' => 'ShisetsukeitaiSecond.weight ASC'));
		$this->set('shisetsukeitaiSeconds', $shisetsukeitaiSeconds);
		// 診療科目リストを取得
		$shinryoukamokus = $this->{$this->modelClass}->Shinryoukamoku->find('list', array('order' => 'Shinryoukamoku.weight ASC'));
		$this->set('shinryoukamokus', $shinryoukamokus);
	}

	public function admin_delete($id = null) {
		if (!$id || !$this->{$this->modelClass}->exists($id)) {
			$this->flashMsg('無効な操作です', 'alert-danger');
			$this->redirect(array('action' => 'index'));
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->{$this->modelClass}->delete($id)) {
				$this->flashMsg('施設情報を削除しました');
			} else {
				$this->flashMsg('施設情報を削除中にエラーが発生しました', 'alert-warning');
			}
		}
		$this->redirect(array('action' => 'index'));
	}

	public function admin_import() {
		$this->set('title_for_layout', '施設情報CSVインポート');
		if ($this->request->is('post')) {
			// 現在のレコードをカウント
			$record_count = $this->{$this->modelClass}->find('count');

			$err_flg = false;
			$err_msg = array();

			$_data = $this->Csv->import($this->request->data[$this->modelClass]['csvfile']['tmp_name'], array(), array('encode' => 'sjis-win'));

			foreach ($_data as $key => $item) {
				// 施設形態 - 大区分処理
				if (isset($item['ShisetsukeitaiFirst']['ShisetsukeitaiFirst']) && !empty($item['ShisetsukeitaiFirst']['ShisetsukeitaiFirst'])) {
					$_data[$key]['ShisetsukeitaiFirst']['ShisetsukeitaiFirst'] = explode('|', $item['ShisetsukeitaiFirst']['ShisetsukeitaiFirst']);
				}
				// 施設形態 - 中区分処理
				if (isset($item['ShisetsukeitaiSecond']['ShisetsukeitaiSecond']) && !empty($item['ShisetsukeitaiSecond']['ShisetsukeitaiSecond'])) {
					$_data[$key]['ShisetsukeitaiSecond']['ShisetsukeitaiSecond'] = explode('|', $item['ShisetsukeitaiSecond']['ShisetsukeitaiSecond']);
				}
				// 診療科目処理
				if (isset($item['Shinryoukamoku']['Shinryoukamoku']) && !empty($item['Shinryoukamoku']['Shinryoukamoku'])) {
					$_data[$key]['Shinryoukamoku']['Shinryoukamoku'] = explode('|', $item['Shinryoukamoku']['Shinryoukamoku']);
				}
			}

			// バリデーション処理
			foreach ($_data as $key => $item) {
				if (!$this->{$this->modelClass}->saveAll($item, array('validate' => 'only', 'deep' => true))) {
					$err_msg[$key]['row'] = $key + 1;
					$err_msg[$key]['msg'] = Hash::flatten($this->{$this->modelClass}->validationErrors);
					$err_flg = true;
				}
			}

			if ($err_flg) {
				// バリデーションエラーをビューへ渡す
				$errors = $err_msg;
				$this->set('errors', $errors);
				$this->flashMsg('インポートエラー' . ' ' . $this->request->data[$this->modelClass]['csvfile']['name'] . ', データに不具合があります', 'alert-danger');
			} else {
				if ($_data) {
					foreach ($_data as $key => $item) {
						$this->{$this->modelClass}->create();

						// グループ名を処理
						// グループ名が存在しなければ保存する
						$groupName = $item[$this->modelClass]['group'];
						if (!empty($groupName)) {
							$group = $this->Group->findByName($groupName);
							if (empty($group)) {
								$this->Group->create();
								$this->Group->saveField('name', $groupName);
							}
						}

						// 法人名処理
						// 法人名が存在しなければ保存する
						$corporationName = $item[$this->modelClass]['corporation'];
						if (!empty($corporationName)) {
							$corporation = $this->Corporation->findByName($corporationName);
							if (empty($corporation)) {
								$this->Corporation->create();
								$this->Corporation->saveField('name', $corporationName);
							}
						}

						$this->{$this->modelClass}->saveAll($item, array('validate' => false, 'deep' => true));
					}
					// 保存後のレコードをカウントして差分を求める
					$new_records_count = $this->{$this->modelClass}->find('count') - $record_count;
					$this->flashMsg('インポートが完了しました' . ' ' . $new_records_count . ' レコード : ' . $this->request->data[$this->modelClass]['csvfile']['name']);
					$this->redirect(array('action' => 'index'));
				}
			}
		}
	}

	public function admin_export() {
		$this->set('title_for_layout', '施設IDエクスポート');
		$this->layout = false;

		$facilities = $this->{$this->modelClass}->find('all', array(
			'fields' => array(
				$this->modelClass . '.id',
				$this->modelClass . '.shisetsumei'
			)
		));
		$this->set('facilities', $facilities);
	}

	public function view($id = null) {
		if (!$id || !$this->{$this->modelClass}->exists($id)) {
			$this->flashMsg('お探しの施設は見つかりませんでした', 'alert-danger');
			$this->redirect('/');
		}

		$detail = $this->{$this->modelClass}->find('first', array(
			'conditions' => array(
				$this->modelClass . '.id' => $id
			),
			'contain' => array(
				'FacilityShow',
				'ShisetsukeitaiSecond',
				'Shinryoukamoku',
				'Roseneki' => array(
					'RailroadPref',
					'RailroadLine',
					'RailroadStation'
				),
				'Recruit' => array(
					'conditions' => array(
						'Recruit.active' => 1
					),
					'order' => array(
						'Recruit.modified' => 'DESC'
					),
					'RecruitShow',
					'ShisetsukeitaiSecond'
				)
			)
		));
		$this->set('detail', $detail);

		$this->set('title_for_layout', sprintf('%sの求人一覧', $detail['Facility']['shisetsumei']));
	}
}
