<?php

App::uses('FacilitiesAppModel', 'Facilities.Model');

class Group extends FacilitiesAppModel {

	public $name = 'Group';

	public $validate = array(
		'name' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'required' => true,
				'notEmpty' => false,
				'message' => 'グループ名を入力してください'
			)
		),
		/**
		 *	20190614 sohnishi
		 *	ふりがな追加
		 */
		// 'hurigana' => array(
		// 	'required' => array(
		// 		'rule' => array('notEmpty'),
		// 		'required' => true,
		// 		'notEmpty' => false,
		// 		'message' => 'ふりがなを入力してください'
		// 	)
		// )

	);
}
