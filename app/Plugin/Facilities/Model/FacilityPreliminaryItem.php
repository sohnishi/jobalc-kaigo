<?php

App::uses('FacilitiesAppModel', 'Facilities.Model');

class FacilityPreliminaryItem extends FacilitiesAppModel {

	public $name = 'FacilityPreliminaryItem';

	public $validate = array(
		'name' => array(
			'required' => array(
				'rule' => array('notBlank'),
				'required' => true,
				'notBlank' => false,
				'message' => '施設予備項目名を入力してください'
			)
		)
	);

	// public $hasAndBelongsToMany = array(
		/*
			20190509 sohnishi
			施設を追加
		*/
	// 	'Facility' => array(
	// 		'className' => 'Facilities.Facility',
	// 		'joinTable' => 'facilities_facility_preliminary_items',
	// 		'foreignKey' => 'facility_preliminary_item_id',
	// 		'associationForeignKey' => 'facility_id',
	// 		'unique' => true,
	// 		'conditions' => '',
	// 		'fields' => '',
	// 		'order' => 'Facility.id ASC',
	// 		'limit' => '',
	// 		'offset' => '',
	// 		'finderQuery' => '',
	// 		'deleteQuery' => '',
	// 		'insertQuery' => ''
	// 	)
	// );
	// public $hasMany = array(
    //     'FacilitiesFacilityPreliminaryItem' => array(
	// 		'className' => 'Facilities.FacilitiesFacilityPreliminaryItem',
	// 		'foreignKey' => 'facility_preliminary_item_id',
	// 		'conditions' => '',
	// 		'order' => '',
	// 		'limit' => '',
	// 		'offset' => '',
	// 		'dependent' => true,
	// 		'exclusive' => '',
	// 		'finderQuery' => ''
	// 	)
    // );
}
