<?php

App::uses('FacilitiesAppModel', 'Facilities.Model');

class Roseneki extends FacilitiesAppModel {

	public $name = 'Roseneki';

	public $belongsTo = array(
		'RailroadPref' => array(
			'className' => 'Ekidata.RailroadPref',
			'foreignKey' => 'pref_cd',
		),
		'RailroadLine' => array(
			'className' => 'Ekidata.RailroadLine',
			'foreignKey' => 'line_cd',
		),
		'RailroadStation' => array(
			'className' => 'Ekidata.RailroadStation',
			'foreignKey' => 'station_cd',
		),
	);
}
