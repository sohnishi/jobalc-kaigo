<?php

App::uses('FacilitiesAppModel', 'Facilities.Model');

class Corporation extends FacilitiesAppModel {

	public $name = 'Corporation';

	public $validate = array(
		'name' => array(
			'required' => array(
				'rule' => array('notEmpty', 'name'),
				'required' => true,
				'notEmpty' => false,
				'message' => '法人名を入力してください'
			)
		),
		/**
		 *	20190617 sohnishi
		 *	ふりがな追加
		 */
		// 'hurigana' => array(
		// 	'required' => array(
		// 		'rule' => array('notEmpty'),
		// 		'required' => true,
		// 		'notEmpty' => false,
		// 		'message' => 'ふりがなを入力してください'
		// 	)
		// )
	);
}
