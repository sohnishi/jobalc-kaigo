<?php

App::uses('FacilitiesAppModel', 'Facilities.Model');
App::uses('Hash', 'Utility');

class Facility extends FacilitiesAppModel {

	public $name = 'Facility';

	public $actsAs = array(
		'Upload.Upload' => array(
			'facility_image' => array(
				'fields' => array(
					'dir' => 'facility_dir'
				),
				'thumbnailSizes' => array(
					'large' => '1024x768',
					'medium' => '800x600',
					'small' => '180x180'
				),
				'thumbnailMethod' => 'php',
				'minetypes' => array('image/jpeg', 'image/gif', 'image/png'),
				'extensions' => array('jpg', 'jpeg', 'gif', 'png'),
				'maxSize' => 2097152
			)
		),
		'Search.Searchable'
	);

	public $filterArgs = array(
		'number' => array('type' => 'value', 'empty' => true),
		'shisetsumei' => array('type' => 'like', 'empty' => true),
		'shisetsumei_hurigana' => array('type' => 'like', 'empty' => true),
		'keiyaku' => array('type' => 'value', 'empty' => true),
		'shisetsukeitai_first' => array('type' => 'subquery', 'method' => 'findByShisetsukeitaiFirst', 'field' => 'Facility.id', 'empty' => true),
		'shisetsukeitai_second' => array('type' => 'subquery', 'method' => 'findByShisetsukeitaiSecond', 'field' => 'Facility.id', 'empty' => true),
		'group' => array('type' => 'like', 'empty' => true),
		'group_hurigana' => array('type' => 'like', 'empty' => true),
		'corporation' => array('type' => 'like', 'empty' => true),
		'corporation_hurigana' => array('type' => 'like', 'empty' => true),
		'zip' => array('type' => 'value', 'empty' => true),
		'todoufuken' => array('type' => 'like', 'empty' => true),
		'shikutyouson' => array('type' => 'like', 'empty' => true),
		'banchi' => array('type' => 'like', 'empty' => true),
		'tatemono' => array('type' => 'like', 'empty' => true),
		'pref_cd' => array('type' => 'subquery', 'method' => 'findByRosenekiPref', 'field' => 'Facility.id', 'empty' => true),
		'line_cd' => array('type' => 'subquery', 'method' => 'findByRosenekiLine', 'field' => 'Facility.id', 'empty' => true),
		'station_cd' => array('type' => 'subquery', 'method' => 'findByRosenekiStation', 'field' => 'Facility.id', 'empty' => true),
		'shinryoukamoku' => array('type' => 'subquery', 'method' => 'findByShinryoukamoku', 'field' => 'Facility.id', 'empty' => true),
		/*
			20190509 sohnishi
			施設予備項目を追加
		*/
		'facility_preliminary_item' => array('type' => 'subquery', 'method' => 'findByFacilityPreliminaryItem', 'field' => 'Facility.id', 'empty' => true),
		'kangokijun' => array('type' => 'value', 'empty' => true),
		'kyukyushitei' => array('type' => 'value', 'empty' => true),
		'karute' => array('type' => 'value', 'empty' => true),
		/*
			20190515 sohnishi
			連絡履歴追加
		*/
		'syousai' => array('type' => 'query', 'method' => 'findBySyousai', 'empty' => true),

	);

	public $validate = array(
		'number' => array(
			'rule' => array('numeric'),
			'message' => '半角数字で入力してください'
		),
		'shisetsumei' => array(
			'required' => array(
				'rule' => array('notEmpty', 'shisetsumei'),
				'required' => true,
				'allowEmpty' => false,
				'message' => array('施設名を入力してください')
			),
//			'unique' => array(
//				'rule' => array('isUnique', 'shisetsumei'),
//				'message' => '既に登録されている施設名です'
//			)
		),
// 		'shisetsumei_hurigana' => array(
// 			'required' => array(
// 				'rule' => array('notBlank', 'shisetsumei_hurigana'),
// 				'required' => true,
// 				'allowEmpty' => false,
// 				'message' => array('施設名ふりがなを入力してください')
// 			),
// //			'unique' => array(
// //				'rule' => array('isUnique', 'shisetsumei'),
// //				'message' => '既に登録されている施設名です'
// //			)
// 		),
		'tel' => array(
			'rule' => array('custom', '/^(0\d{1,4}-\d{1,4}-\d{4})$/'),
			// ハイフン不問
			//'rule' => array('custom', '/^(0\d{1,4}[\s-]?\d{1,4}[\s-]?\d{4})$/'),
			'allowEmpty' => true,
			'message' => '電話番号の入力形式が違います'
		),
		'fax' => array(
			'rule' => array('custom', '/^(0\d{1,4}-\d{1,4}-\d{4})$/'),
			// ハイフン不問
			//'rule' => array('custom', '/^(0\d{1,4}[\s-]?\d{1,4}[\s-]?\d{4})$/'),
			'allowEmpty' => true,
			'message' => 'FAX番号の入力形式が違います'
		),
		'tesuryou' => array(
			'rule' => array('numeric'),
			'allowEmpty' => true,
			'message' => '半角数字で入力してください'
		),
		'zip' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'allowEmpty' => true,
				'message' => '半角数字で入力してください'
			),
			'keta' => array(
				'rule' => array('custom', '/^(\d{7})$/'),
				'message' => '7桁の半角数字で入力してください'
			),
		),
		'shisetsu_url' => array(
			'url' => array(
				'rule' => array('url', true),
				'allowEmpty' => true,
				'message' => '「http://」から有効なURLを入力してください'
			)
		),
		'saiyou_url' => array(
			'url' => array(
				'rule' => array('url', true),
				'allowEmpty' => true,
				'message' => '「http://」から有効なURLを入力してください'
			)
		)
	);

	public $hasOne = array(
		'FacilityShow' => array(
			'className' => 'Facilities.FacilityShow',
			'foreignKey' => 'facility_id',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'dependent' => true
		)
	);

	public $hasMany = array(
		'Recruit' => array(
			'className' => 'Recruits.Recruit',
			'foreignKey' => 'facility_id',
			'conditions' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'dependent' => true,
			'exclusive' => '',
			'finderQuery' => '',
		),
		'Staff' => array(
			'className' => 'Staffs.Staff',
			'foreignKey' => 'facility_id',
			'conditions' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'dependent' => true,
			'exclusive' => '',
			'finderQuery' => '',
		),
		'Contact' => array(
			'className' => 'Contacts.Contact',
			'foreignKey' => 'facility_id',
			'conditions' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'dependent' => true,
			'exclusive' => '',
			'finderQuery' => ''
		),
		'Roseneki' => array(
			'className' => 'Facilities.Roseneki',
			'foreignKey' => 'facility_id',
			'conditions' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'dependent' => true,
			'exclusive' => '',
			'finderQuery' => ''
		),
		// 'FacilitiesFacilityPreliminaryItem' => array(
		// 	'className' => 'Facilities.FacilitiesFacilityPreliminaryItem',
		// 	'foreignKey' => 'facility_id',
		// 	'conditions' => '',
		// 	'order' => '',
		// 	'limit' => '',
		// 	'offset' => '',
		// 	'dependent' => true,
		// 	'exclusive' => '',
		// 	'finderQuery' => ''
		// )
	);

	public $hasAndBelongsToMany = array(
		'ShisetsukeitaiFirst' => array(
			'className' => 'Facilities.ShisetsukeitaiFirst',
			'joinTable' => 'facilities_shisetsukeitai_firsts',
			'foreignKey' => 'facility_id',
			'associationForeignKey' => 'shisetsukeitai_first_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => 'ShisetsukeitaiFirst.weight ASC',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'ShisetsukeitaiSecond' => array(
			'className' => 'Facilities.ShisetsukeitaiSecond',
			'joinTable' => 'facilities_shisetsukeitai_seconds',
			'foreignKey' => 'facility_id',
			'associationForeignKey' => 'shisetsukeitai_second_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => 'ShisetsukeitaiSecond.weight ASC',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Shinryoukamoku' => array(
			'className' => 'Facilities.Shinryoukamoku',
			'joinTable' => 'facilities_shinryoukamokus',
			'foreignKey' => 'facility_id',
			'associationForeignKey' => 'shinryoukamoku_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => 'Shinryoukamoku.weight ASC',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
		,
		/*
			20190509 sohnishi
			施設予備項目を追加
		*/
		'FacilityPreliminaryItem' => array(
			'className' => 'Facilities.FacilityPreliminaryItem',
			'joinTable' => 'facilities_facility_preliminary_items',
			'foreignKey' => 'facility_id',
			'associationForeignKey' => 'facility_preliminary_item_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => 'FacilityPreliminaryItem.id ASC',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);

	public function afterSave($created, $options = array()) {
		if ($created) {
			// 施設IDの重複を無くすためMySQLのオートインクリメント値を使う
			if (!isset($this->data[$this->alias]['number']) && empty($this->data[$this->alias]['number'])) {
				$this->saveField('number', $this->getLastInsertID());
			}
		}
	}

	/*
		20190515 sohnishi
		連絡履歴追加
	*/
	public function findBySyousai($data = array()) {
		/* 
			下記の実装だとサブクエリが非常に遅いため、
			CustomerContactを取得してから該当のcustomer_idを渡す実装に変更
		*/
		// $this->Contact->Behaviors->attach('Containable', array('autoFields' => false));
		// $this->Contact->Behaviors->attach('Search.Searchable');
		// $query = $this->Contact->getQuery('all', array(
		// 	'conditions' => array(
		// 		'Contact.syousai LIKE' => "%".$data['syousai']."%"
		// 	),
		// 	'fields' => array(
		// 		'facility_id'
		// 	)
		// ));

		// Contactをsyousaiで検索
		$contacts = $this->Contact->find('all', array(
			'conditions' => array(
				'Contact.syousai LIKE' => "%".$data['syousai']."%"
			)
		));
		// 取得したContactからfacility_idを取り出す
		$contact_ids = array();
		foreach($contacts as $key => $value){
			array_push($contact_ids, $value['Contact']['facility_id']);
		}
		// クエリ作成
		if (!empty($contact_ids)) {
			// syousai検索でContactが存在した場合
			$query = array($this->modelClass . 'Facility.id IN' => $contact_ids);
		} else {
			// syousai検索でContactが存在しなかった場合
			$query = array($this->modelClass . 'Facility.id' => '0');
		}
		return $query;

	}


	public function findByShisetsukeitaiFirst($data = array()) {
		$this->FacilitiesShisetsukeitaiFirst->Behaviors->attach('Containable', array('autoFields' => false));
		$this->FacilitiesShisetsukeitaiFirst->Behaviors->attach('Search.Searchable');
		$query = $this->FacilitiesShisetsukeitaiFirst->getQuery('all', array(
			'conditions' => array(
				'shisetsukeitai_first_id' => $data['shisetsukeitai_first']
			),
			'fields' => array('facility_id')
		));
		return $query;
	}

	public function findByShisetsukeitaiSecond($data = array()) {
		$this->FacilitiesShisetsukeitaiSecond->Behaviors->attach('Containable', array('autoFields' => false));
		$this->FacilitiesShisetsukeitaiSecond->Behaviors->attach('Search.Searchable');
		$query = $this->FacilitiesShisetsukeitaiSecond->getQuery('all', array(
			'conditions' => array(
				'shisetsukeitai_second_id' => $data['shisetsukeitai_second']
			),
			'fields' => array('facility_id')
		));
		return $query;
	}

	public function findByShinryoukamoku($data = array()) {
		$this->FacilitiesShinryoukamokus->Behaviors->attach('Containable', array('autoFields' => false));
		$this->FacilitiesShinryoukamokus->Behaviors->attach('Search.Searchable');
		$query = $this->FacilitiesShinryoukamokus->getQuery('all', array(
			'conditions' => array(
				'shinryoukamoku_id' => $data['shinryoukamoku']
			),
			'fields' => array('facility_id')
		));
		return $query;
	}

	/*
		20190515 sohnishi
		施設予備項目を追加
	 */
	public function findByFacilityPreliminaryItem($data = array()) {

		$this->FacilitiesFacilityPreliminaryItem->Behaviors->attach('Containable', array('autoFields' => false));
		$this->FacilitiesFacilityPreliminaryItem->Behaviors->attach('Search.Searchable');

		$conditions = array();
        if(isset($data['facility_preliminary_item'])){
            foreach ($data['facility_preliminary_item'] as $facility_preliminary_item) {
				if(!empty($facility_preliminary_item)){
					$conditions['OR']['content LIKE'] = '%' . $facility_preliminary_item . '%';
				}
            }
        }
		
		$query = $this->FacilitiesFacilityPreliminaryItem->getQuery('all', array(
			'conditions' => $conditions,
			'fields' => array(
				'facility_id'
			)
		));
		return $query;

	}

	public function findByRosenekiPref($data = array()) {
		$this->Roseneki->Behaviors->attach('Containable', array('autoFields' => false));
		$this->Roseneki->Behaviors->attach('Search.Searchable');
		$query = $this->Roseneki->getQuery('all', array(
			'conditions' => array(
				'Roseneki.pref_cd' => $data['pref_cd']
			),
			'fields' => array('facility_id')
		));
		return $query;
	}

	public function findByRosenekiLine($data = array()) {
		$this->Roseneki->Behaviors->attach('Containable', array('autoFields' => false));
		$this->Roseneki->Behaviors->attach('Search.Searchable');
		$query = $this->Roseneki->getQuery('all', array(
			'conditions' => array(
				'Roseneki.line_cd' => $data['line_cd']
			),
			'fields' => array('facility_id')
		));
		return $query;
	}

	public function findByRosenekiStation($data = array()) {
		$this->Roseneki->Behaviors->attach('Containable', array('autoFields' => false));
		$this->Roseneki->Behaviors->attach('Search.Searchable');
		$query = $this->Roseneki->getQuery('all', array(
			'conditions' => array(
				'Roseneki.station_cd' => $data['station_cd']
			),
			'fields' => array('facility_id')
		));
		return $query;
	}

	public function getAll(){
        // $sql = "SELECT * FROM jobalc_kaigo.facilities;";
        // $data = $this->query($sql);
		// return $data;
		$count = $this->find('count');
		
		// 5000件ずつデータを取得するようにする
		$limit = 5000;
		
		// ループする回数({データ件数 ÷ 1回の取得件数}の端数を切り上げた数)
		$loop  = ceil($count / $limit);
		$data = array();
		for ($i = 0; $i < $loop; $i++){
			// オフセット
			$offset = $limit * $i;
			
			$_data = $this->query("select * from jobalc_kaigo.facilities limit {$limit} offset {$offset};", $cachequeries = false);
			$data = array_merge($data, $_data);
		
		}

        return $data;
	}

	public function getAllByPreliminaryItems(){
        // $sql = "SELECT * FROM jobalc_kaigo.facilities_facility_preliminary_items;";
		// $data = $this->query($sql);
		// データの件数を取得する
		$count = $this->find('count');
		
		// 5000件ずつデータを取得するようにする
		$limit = 5000;
		
		// ループする回数({データ件数 ÷ 1回の取得件数}の端数を切り上げた数)
		$loop  = ceil($count / $limit);
		$data = array();
		for ($i = 0; $i < $loop; $i++){
			// オフセット
			$offset = $limit * $i;
			
			$data = $data + $this->query("select * from jobalc_kaigo.facilities_facility_preliminary_items limit {$limit} offset {$offset};", $cachequeries = false);
			
		
		}

        return $data;
	}
	
	public function getAllByShisetsukeitaiFirst(){
        // $sql = "SELECT * FROM jobalc_kaigo.facilities_shisetsukeitai_firsts;";
        // $data = $this->query($sql);
		// return $data;
		$count = $this->FacilitiesShisetsukeitaiFirst->find('count');
		
		// 5000件ずつデータを取得するようにする
		$limit = 5000;
		
		// ループする回数({データ件数 ÷ 1回の取得件数}の端数を切り上げた数)
		$loop  = ceil($count / $limit);
		$data = array();
		for ($i = 0; $i < $loop; $i++){
			// オフセット
			$offset = $limit * $i;
			
			$_data = $this->FacilitiesShisetsukeitaiFirst->query("select * from jobalc_kaigo.facilities_shisetsukeitai_firsts limit {$limit} offset {$offset};", $cachequeries = false);
			$data = array_merge($data, $_data);
		
		}

        return $data;
	}
	
	public function getAllByShisetsukeitaiSecond(){
        // $sql = "SELECT * FROM jobalc_kaigo.facilities_shisetsukeitai_seconds;";
        // $data = $this->query($sql);
		// return $data;
		$count = $this->FacilitiesShisetsukeitaiSecond->find('count');
		
		// 5000件ずつデータを取得するようにする
		$limit = 5000;
		
		// ループする回数({データ件数 ÷ 1回の取得件数}の端数を切り上げた数)
		$loop  = ceil($count / $limit);
		$data = array();
		for ($i = 0; $i < $loop; $i++){
			// オフセット
			$offset = $limit * $i;
			
			$_data = $this->FacilitiesShisetsukeitaiSecond->query("select * from jobalc_kaigo.facilities_shisetsukeitai_seconds limit {$limit} offset {$offset};", $cachequeries = false);
			$data = array_merge($data, $_data);
		
		}

        return $data;
	}
	
	public function getAllByShinryoukamokus(){
        // $sql = "SELECT * FROM jobalc_kaigo.facilities_shisetsukeitai_seconds;";
        // $data = $this->query($sql);
		// return $data;
		$count = $this->FacilitiesShinryoukamokus->find('count');
		
		// 5000件ずつデータを取得するようにする
		$limit = 5000;
		
		// ループする回数({データ件数 ÷ 1回の取得件数}の端数を切り上げた数)
		$loop  = ceil($count / $limit);
		$data = array();
		for ($i = 0; $i < $loop; $i++){
			// オフセット
			$offset = $limit * $i;
			
			$_data = $this->FacilitiesShinryoukamokus->query("select * from jobalc_kaigo.facilities_shinryoukamokus limit {$limit} offset {$offset};", $cachequeries = false);
			$data = array_merge($data, $_data);
		
		}

        return $data;
    }
}
