<?php

App::uses('FacilitiesAppModel', 'Facilities.Model');

class Shinryoukamoku extends FacilitiesAppModel {

	public $name = 'Shinryoukamoku';

	public $validate = array(
		'name' => array(
			'required' => array(
				'rule' => array('notEmpty', 'name'),
				'required' => true,
				'allowEmpty' => false,
				'message' => '診療科目名を入力してください'
			)
		),
		'weight' => array(
			'numeric' => array(
				'rule' => array('numeric', 'weight'),
				'allowEmpty' => true,
				'message' => '半角数字で入力してください'
			)
		)
	);

	public $hasAndBelongsToMany = array(
		'Facility' => array(
			'className' => 'Facilities.Facility',
			'joinTable' => 'facilities_shinryoukamokus',
			'foreignKey' => 'shinryoukamoku_id',
			'associationForeignKey' => 'facility_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);

	public function getAll(){
        // $sql = "SELECT * FROM jobalc_kaigo.shinryoukamokus;";
        // $data = $this->query($sql);
		// return $data;
		$count = $this->find('count');
		
		// 5000件ずつデータを取得するようにする
		$limit = 5000;
		
		// ループする回数({データ件数 ÷ 1回の取得件数}の端数を切り上げた数)
		$loop  = ceil($count / $limit);
		$data = array();
		for ($i = 0; $i < $loop; $i++){
			// オフセット
			$offset = $limit * $i;
			
			$_data = $this->query("select * from jobalc_kaigo.shinryoukamokus limit {$limit} offset {$offset};", $cachequeries = false);
			$data = array_merge($data, $_data);
		
		}

        return $data;
    }
}
