<?php

App::uses('FacilitiesAppModel', 'Facilities.Model');

class FacilityShow extends FacilitiesAppModel {

	public $name = 'FacilityShow';
	
	public function getAll(){

        $count = $this->find('count');
		
		// 5000件ずつデータを取得するようにする
		$limit = 5000;
		
		// ループする回数({データ件数 ÷ 1回の取得件数}の端数を切り上げた数)
		$loop  = ceil($count / $limit);
		$data = array();
		for ($i = 0; $i < $loop; $i++){
			// オフセット
			$offset = $limit * $i;
			
			$_data = $this->query("select * from jobalc_kaigo.facility_shows limit {$limit} offset {$offset};", $cachequeries = false);
			$data = array_merge($data, $_data);
		
		}

        return $data;
    }
}
