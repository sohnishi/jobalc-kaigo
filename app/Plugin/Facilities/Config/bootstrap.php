<?php
/**
 * フォームで使用するオプション値を設定
 */

// 契約オプション
Configure::write('keiyaku_options', array('未', '済'));
// 返金規定オプション
Configure::write('henkinkitei_options', array('通常', 'その他'));
// 看護基準オプション
Configure::write('kangokijun_options', array('7:1', '10:1', '13:1', '15:1', '18:1', '20:1', '25:1'));
// 救急指定オプション
Configure::write('kyukyushitei_options', array('救急指定なし', '初期救急', '二次救急', '三次救急'));
// カルテオプション
Configure::write('karute_options', array('紙カルテ', 'オーダリングのみ', '電子カルテ', '電子カルテ導入予定'));
// 掲載オプション
Configure::write('facility_show_options', array(
	'groupmei' => 'グループ名',
	'houjinmei' => '法人名',
	'shinryoukamoku' => '診療科目',
	'byousyou' => '病床数',
	'byoutousyousai' => '病棟詳細',
	'kangokijun' => '看護基準',
	'kyukyushitei' => '救急指定',
	'kyukyusyousai' => '救急詳細',
	'syujyutsusyousai' => '手術詳細',
	'karute' => 'カルテ',
	'syokuinkousei' => '関連施設',
	'tokutyou' => '特徴',
	'kanrenshisetsu' => 'HP案内文',
	'kyouikukensyu' => 'その他情報',
	'shisetsu_url' => '施設URL',
	'saiyou_url' => '採用URL',
	'facility_tel' => '施設TEL',
	'facility_fax' => '施設FAX'
));
// 返金手数料初期値
Configure::write('tesuryou', 20);