<?php

/**
 * Facilities Plugin Routes
 */
Router::connect('/facilities/view/*', array('plugin' => 'facilities', 'controller' => 'facilities', 'action' => 'view'));
Router::connect('/admin/facilities/index/*', array('plugin' => 'facilities', 'controller' => 'facilities', 'action' => 'index', 'admin' => true));
Router::connect('/admin/facilities/add', array('plugin' => 'facilities', 'controller' => 'facilities', 'action' => 'add', 'admin' => true));
Router::connect('/admin/facilities/edit/*', array('plugin' => 'facilities', 'controller' => 'facilities', 'action' => 'edit', 'admin' => true));
Router::connect('/admin/facilities/view/*', array('plugin' => 'facilities', 'controller' => 'facilities', 'action' => 'view', 'admin' => true));
Router::connect('/admin/facilities/import', array('plugin' => 'facilities', 'controller' => 'facilities', 'action' => 'import', 'admin' => true));
Router::connect('/admin/facilities/export', array('plugin' => 'facilities', 'controller' => 'facilities', 'action' => 'export', 'admin' => true));
Router::connect('/admin/facilities/:controller/:action/*', array('plugin' => 'facilities', 'admin' => true));

