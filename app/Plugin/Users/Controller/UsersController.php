<?php

App::uses('UsersAppController', 'Users.Controller');

class UsersController extends UsersAppController {

	public $name = 'Users';

	public $uses = array('Users.User');

	public $presetVars = true;

	public $components = array(
		'Users.RememberMe'
	);

	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('model', $this->modelClass);
		$this->set('title_for_layout', 'アカウント管理');
		$this->set('currentUser', $this->Auth->user());
	}

	public function admin_index() {
		$this->Prg->commonProcess();
		$this->Paginator->settings['conditions'] = array(
			$this->{$this->modelClass}->parseCriteria($this->Prg->parsedParams())
		);
		$this->Paginator->settings['limit'] = 20;
		$this->Paginator->settings['order'] = array(
			$this->modelClass . '.created' => 'DESC'
		);
		$this->set('users', $this->Paginator->paginate($this->modelClass));
	}

	public function admin_add() {
		if (!empty($this->request->data)) {
			$this->log('allow_edit'.$this->request->data['allow_edit_customer']);
			
			if ($this->{$this->modelClass}->add($this->request->data)) {
				$this->flashMsg('アカウントを登録しました');
				$this->redirect(array('action' => 'index'));
			}
		}
	}

	public function admin_edit($id = null) {
		$user = $this->{$this->modelClass}->exists($id);
		if (!$user) {
			$this->flashMsg('無効なアカウントです', 'alert-error');
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->request->data) && $this->request->is(array('post', 'put'))) {
			if (empty($this->request->data[$this->modelClass]['password'])) {
				unset($this->request->data[$this->modelClass]['password']);
				unset($this->request->data[$this->modelClass]['temppassword']);
			}
			$result = $this->{$this->modelClass}->edit($id, $this->request->data);
			if ($result === true) {
				$this->Session->write('Auth', $this->{$this->modelClass}->read(null, $this->Auth->user('id')));
				$this->flashMsg('アカウントを更新しました');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->flashMsg('アカウントを更新中にエラーが発生しました', 'alert-warning');
				$this->request->data = $result;
			}
		} else {
			$this->request->data = $this->{$this->modelClass}->read(null, $id);
			unset($this->request->data['User']['password']);
		}
	}

	public function admin_delete($id = null) {
		$user = $this->{$this->modelClass}->exists($id);
		if (!$user) {
			$this->flashMsg('無効なアカウントです', 'alert-danger');
			$this->redirect(array('action' => 'index'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->{$this->modelClass}->delete($id)) {
				$this->flashMsg('アカウントを削除しました');
			} else {
				$this->flashMsg('アカウントを削除中にエラーが発生しました', 'alert-danger');
			}
		}
		$this->redirect(array('action' => 'index'));
	}

	public function admin_change_password() {
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->request->data[$this->modelClass]['id'] = $this->Auth->user('id');
			if ($this->{$this->modelClass}->changePassword($this->request->data)) {
				$this->flashMsg('パスワードを変更しました');
				$this->RememberMe->destroyCookie();
				$this->redirect(array('action' => 'index'));
			}
		}
	}

	public function admin_login() {
		$this->layout = 'admin_login';

		if ($this->Auth->user('id')) {
			return $this->redirect($this->Auth->redirectUrl());
		}

		$Event = new CakeEvent(
				'Users.Controller.Users.beforeLogin',
				$this,
				array('data' => $this->request->data)
		);

		$this->getEventManager()->dispatch($Event);

		if ($Event->isStopped()) {
			return;
		}

		if ($this->request->is('post')) {
			if ($this->Auth->login()) {
				$this->Session->delete('Auth.redirect');
				$Event = new CakeEvent(
						'Users.Controller.Users.afterLogin',
						$this,
						array(
							'data' => $this->request->data,
							'isFirstLogin' => !$this->Auth->user('last_login')
						)
				);

				$this->getEventManager()->dispatch($Event);

				$this->{$this->modelClass}->id = $this->Auth->user('id');
				$this->{$this->modelClass}->saveField('last_login', date('Y-m-d H:i:s'));

				if ($this->here == $this->Auth->loginRedirect) {
					$this->Auth->loginRedirect = '/admin';
				}
				$this->flashMsg(sprintf('%s のアカウントでログインしました', $this->Auth->user($this->{$this->modelClass}->displayField)));
				if (!empty($this->request->data)) {
					$data = $this->request->data[$this->modelClass];
					if (empty($this->request->data[$this->modelClass]['remember_me'])) {
						$this->RememberMe->destroyCookie();
					} else {
						$this->_setCookie();
					}
				}

				if (empty($data[$this->modelClass]['return_to'])) {
					$data[$this->modelClass]['return_to'] = null;
				}
				$this->redirect($this->Auth->redirectUrl($data[$this->modelClass]['return_to']));
			} else {
				$this->Auth->flash('アカウント名かパスワードが違います');
			}
		}

		if (isset($this->request->params['named']['return_to'])) {
			$this->set('return_to', urldecode($this->request->params['named']['return_to']));
		} elseif (isset($this->request->query['return_to'])) {
			$this->set('return_to', $this->request->query['return_to']);
		} else {
			$this->set('return_to', false);
		}

		$this->set('title_for_layout', 'ログイン');
	}

	public function admin_logout() {
		$this->Session->destroy();
		$this->RememberMe->destroyCookie();
		$this->flashMsg('ログアウトしました', 'alert-success', 'auth');
		$this->redirect($this->Auth->logout());
	}

	protected function _setCookie($options = array(), $cookieKey = 'rememberMe') {
		$this->RememberMe->settings['cookieKey'] = $cookieKey;
		$this->RememberMe->configureCookie($options);
		$this->RememberMe->setCookie();
	}
}
