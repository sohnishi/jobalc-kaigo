<?php

App::uses('UsersAppModel', 'Users.Model');
App::uses('Security', 'Utility');
App::uses('SearchableBehavior', 'Search.Model/Behavior');
App::uses('Hash', 'Utility');

class User extends UsersAppModel {

	public $name = 'User';

	public $actsAs = array(
		'Search.Searchable'
	);

	public $filterArgs = array(
		'name' => array('type' => 'like'),
		'username' => array('type' => 'like'),
		'email' => array('type' => 'value')
	);

	public $displayField = 'name';

	public $validate = array(
		'username' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'required' => true,
				'allowEmpty' => false,
				'message' => 'アカウント名を入力してください'
			),
			'alpha' => array(
				'rule' => array('alphaNumeric'),
				'message' => '半角英数字で入力してください'
			),
			'unique_username' => array(
				'rule' => array('isUnique', 'username'),
				'message' => '既に登録されているアカウント名です'
			),
			'username_min' => array(
				'rule' => array('minLength', '3'),
				'message' => 'アカウント名は3文字以上で入力してください'
			)
		),
		'email' => array(
			'isValid' => array(
				'rule' => 'email',
				'required' => true,
				'message' => '有効なメールアドレスを入力してください'
			),
			'isUnique' => array(
				'rule' => array('isUnique', 'email'),
				'message' => '既に登録されているメールアドレスです'
			)
		),
		'password' => array(
			'too_short' => array(
				'rule' => array('minLength', 6),
				'message' => 'パスワードは6文字以上で入力してください'
			),
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'パスワードを入力してください'
			)
		),
		'temppassword' => array(
			'rule' => 'confirmPassword',
			'message' => 'パスワードが一致しません'
		)
	);

	public function __construct($id = false, $table = null, $ds = null) {
		$this->_setupValidation();
		parent::__construct($id, $table, $ds);
	}

	protected function _setupValidation() {
		$this->validatePasswordChange = array(
			'new_password' => $this->validate['password'],
			'confirm_password' => array(
				'required' => array(
					'rule' => array('compareFields', 'new_password', 'confirm_password'),
					'required' => true,
					'message' => 'パスワードが一致しません'
				)
			),
			'old_password' => array(
				'to_short' => array(
					'rule' => array('validateOldPassword'),
					'required' => true,
					'message' => '無効なパスワードです'
				)
			)
		);
	}

	public function confirmPassword($password = null) {
		if ((isset($this->data[$this->alias]['password']) && isset($password['temppassword']))
				&& !empty($password['temppassword'])
				&& ($this->data[$this->alias]['password'] === $password['temppassword'])
		) {
			return true;
		}
		return false;
	}

	public function compareFields($field1, $field2) {
		if (is_array($field1)) {
			$field1 = key($field1);
		}
		if (isset($this->data[$this->alias][$field1]) && isset($this->data[$this->alias][$field2])
				&& $this->data[$this->alias][$field1] == $this->data[$this->alias][$field2]
		) {
			return true;
		}
		return false;
	}

	public function validateOldPassword($password) {
		if (!isset($this->data[$this->alias]['id']) && empty($this->data[$this->alias]['id'])) {
			if (Configure::read('debug') > 0) {
				throw new OutOfBoundsException('$this->data[\'' . $this->alias . '\'][\'id\'] がセットされていないか空です');
			}
		}

		$currentPassword = $this->field('password', array($this->alias . '.id' => $this->data[$this->alias]['id']));
		return $currentPassword === $this->hash($password['old_password'], null, true);
	}

	public function add($postData = null) {
		if (!empty($postData)) {
			$this->data = $postData;
			if ($this->validates()) {
				// 管理者名が未入力ならアカウント名を代入する
				if (empty($postData[$this->alias]['name'])) {
					$postData[$this->alias]['name'] = $postData[$this->alias]['username'];
				}
				if (empty($postData[$this->alias]['is_admin'])) {
					$postData[$this->alias]['role'] = 'registered';
				} else {
					$postData[$this->alias]['role'] = 'admin';
				}
				
				if (empty($postData[$this->alias]['allow_edit_customer'])) {
					$postData[$this->alias]['allow_edit_customer'] = '0';
				} 
				
				$postData[$this->alias]['password'] = $this->hash($postData[$this->alias]['password'], 'sha1', true);
				$this->create();
				$result = $this->save($postData, false);
				if ($result) {
					$result[$this->alias][$this->primaryKey] = $this->id;
					$this->data = $result;
					return true;
				}
			}
		}
		return false;
	}

	public function edit($id = null, $postData = null) {
		$user = $this->getUserForEditing($id);

		$this->set($user);
		if (!empty($postData)) {
			$this->set($postData);
			
			if (empty($postData[$this->alias]['allow_edit_customer'])) {
				$postData[$this->alias]['allow_edit_customer'] = '0';
			} 


			if ($this->validates()) {
				if (isset($postData[$this->alias]['password'])) {
					$postData[$this->alias]['password'] = $this->hash($postData[$this->alias]['password'], 'sha1', true);
				}

				$result = $this->save($postData, false);
				if ($result) {
					$this->data = $result;
					return true;
				}
			} else {
				$postData[$this->alias]['username'] = $user[$this->alias]['username'];
				return $postData;
			}
		}
	}

	public function getUserForEditing($id = null, $options = array()) {
		$defaults = array(
			'contain' => array(),
			'conditions' => array(
				$this->alias . '.id' => $id
			)
		);
		$options = Hash::merge($defaults, $options);

		$user = $this->find('first', $options);

		return $user;
	}

	public function changePassword($postData = array()) {
		$this->validate = $this->validatePasswordChange;

		$this->set($postData);
		if ($this->validates()) {
			$this->data[$this->alias]['password'] = $this->hash($this->data[$this->alias]['new_password'], null, true);
			$this->save($postData, array(
				'validate' => false,
				'callback' => false
			));
			return true;
		}
		return false;
	}
}
