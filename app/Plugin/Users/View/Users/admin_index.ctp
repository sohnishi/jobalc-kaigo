<div class="row">
	<div class="col-xs-12 contents-actions">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#">管理者一覧</a></li>

			<?php if ($currentUser['is_admin'] || $currentUser['id'] == 1) : ?>
			<li><a href="<?php echo $this->Html->url('/admin/users/add'); ?>">管理者の新規登録</a></li>
		<?php endif; ?>
			<li><a href="<?php echo $this->Html->url('/admin/users/change_password'); ?>">パスワード変更</a></li>
		</ul>
	</div>
</div>

<?php if (CakePlugin::loaded('Search')) : ?>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-default">
			<div class="panel-heading">管理者検索</div>
			<div class="panel-body">
				<?php
					echo $this->Form->create($model, array(
						'inputDefaults' => array(
							'label' => false,
							'wrapInput' => false,
							'div' => false,
							'class' => 'form-control'
						),
						'class' => 'form-horizontal',
						'url' => array_merge(
								array('action' => 'index'),
								$this->params['pass']
						)
					));
				?>
				<div class="form-group">
					<?php echo $this->Form->label('name', '管理者名', array('class' => 'col-xs-2 control-label')); ?>
					<div class="col-xs-4">
						<?php echo $this->Form->input('name', array('required' => false)); ?>
					</div>
				</div>

				<div class="form-group">
					<?php echo $this->Form->label('username', 'アカウント名', array('class' => 'col-xs-2 control-label')); ?>
					<div class="col-xs-4">
						<?php echo $this->Form->input('username', array('required' => false)); ?>
					</div>
				</div>

				<div class="form-group">
					<?php echo $this->Form->label('email', 'メールアドレス', array('class' => 'col-xs-2 control-label')); ?>
					<div class="col-xs-4">
						<?php echo $this->Form->input('email', array('required' => false)); ?>
						<span class="help-block">メールアドレスは正確に入力してください</span>
					</div>
				</div>

				<div class="form-group">
					<div class="col-xs-4 col-xs-offset-2">
						<ul class="list-inline">
							<li><button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> 管理者検索</button></li>
							<li><a href="<?php echo $this->Html->url(array('action' => 'index')); ?>" class="btn btn-warning"><i class="fa fa-refresh"></i> リセット</a></li>
						</ul>
					</div>
				</div>
				<?php echo $this->Form->end(); ?>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>

<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				管理者一覧
			</div>
			<div class="panel-body">
				<table class="table table-striped">
					<thead>
						<tr>
							<th><?php echo $this->Paginator->sort('name', '管理者名'); ?></th>
							<th><?php echo $this->Paginator->sort('username', 'アカウント名'); ?></th>
							<th><?php echo $this->Paginator->sort('email', 'メールアドレス'); ?></th>
							<th><?php echo $this->Paginator->sort('active', 'アクティブ'); ?></th>
							<th><?php echo $this->Paginator->sort('last_login', '最終ログイン'); ?></th>
							<th><?php echo $this->Paginator->sort('created', '登録日'); ?></th>
							<?php if ($currentUser['is_admin'] == 1) { ?>
							<th><?php echo "管理者" ?></th>
							<th><?php echo "顧客編集" ?></th>
							<?php } ?>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($users as $user) : ?>
						<tr>
							<td><?php echo $user[$model]['name']; ?></td>
							<td><?php echo $user[$model]['username']; ?></td>
							<td><?php echo $user[$model]['email']; ?></td>
							<td><?php echo $user[$model]['active'] == 1 ? 'アクティブ' : 'ブロック'; ?></td>
							<td><?php echo !empty($user[$model]['last_login']) ? date('Y-m-d', strtotime($user[$model]['last_login'])) : '-----'; ?></td>
							<td><?php echo date('Y-m-d', strtotime($user[$model]['created'])); ?></td>
							<?php if ($currentUser['is_admin'] == 1) { ?>
							<td><?php echo $user[$model]['is_admin'] == 1 ? '<i class="fa fa-check"></i>' : ''; ?></td>
							<td>								
								<?php echo $user[$model]['allow_edit_customer'] == 1 ? '<i class="fa fa-check"></i>' : ''; ?>
							</td>

							 <?php } ?>
							
							<td class="action">
								<ul class="list-inline">
									<li><?php echo $this->Html->link('編集', array('action' => 'edit', $user[$model]['id'])); ?></li>
									<?php if (!$user[$model]['is_admin']) : ?>
										<?php if ($currentUser['id'] == 1) { ?>
									<li><?php echo $this->Form->postLink('削除', array('action' => 'delete', $user[$model]['id']), null, sprintf('%s を削除してもよろしいですか？元には戻せません', $user[$model]['username'])); ?></li>
										<?php } ?>
									<?php endif; ?>
								</ul>
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
				<div class="text-center">
					<?php echo $this->Paginator->pagination(array('ul' => 'pagination')); ?>
				</div>
			</div>
		</div>
	</div>
</div>
