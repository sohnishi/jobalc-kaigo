<div class="row">
	<div class="col-xs-12 contents-actions">
		<ul class="nav nav-tabs">
			<li><a href="<?php echo $this->Html->url('/admin/users'); ?>">管理者一覧</a></li>
			<li><a href="<?php echo $this->Html->url('/admin/users/add'); ?>">管理者の新規登録</a></li>
			<li class="active"><a href="#">パスワード変更</a></li>
		</ul>
	</div>
</div>

<div class="row">
	<div class="col-xs-4 col-xs-offset-4">
		<div class="panel panel-default">
			<div class="panel-heading"><?php echo $currentUser['name']; ?>のパスワード変更</div>
			<?php
				echo $this->Form->create($model, array(
					'inputDefaults' => array(
						'label' => false,
						'div' => false,
						'wrapInput' => false,
						'class' => 'form-control'
					),
					'action' => 'change_password'
				));
			?>
			<div class="panel-body">

				<div class="alert alert-info"><p>パスワード変更は現在ログインしている管理者のパスワードを変更します。</p></div>

				<div class="form-group">
					<?php echo $this->Form->input('old_password', array('type' => 'password', 'label' => '現在のパスワード')); ?>
					<span class="help-block">パスワードを変更するにはセキュリティ上の理由から現在のパスワードを入力してください</span>
				</div>

				<div class="form-group">
					<?php echo $this->Form->input('new_password', array('type' => 'password', 'label' => '新しいパスワード')); ?>
					<span class="help-block">パスワードは6文字以上で入力してください</span>
				</div>

				<div class="form-group">
					<?php echo $this->Form->input('confirm_password', array('type' => 'password', 'label' => 'パスワード再入力')); ?>
				</div>

			</div>
			<div class="panel-footer">
				<?php echo $this->Form->submit('パスワードを変更する', array('class' => 'btn btn-primary')); ?>
			</div>
			<?php echo $this->Form->end(); ?>
		</div>
	</div>
</div>