<div class="col-xs-4 col-xs-offset-4">
	<div class="login-panel panel panel-default">
		<div class="panel-heading"><?php echo $title_for_layout; ?></div>
		<div class="panel-body">
			<?php echo $this->Session->flash('auth'); ?>
			<?php
				echo $this->Form->create($model, array(
					'inputDefaults' => array(
						'label' => false,
						'div' => 'form-group',
						'wrapInput' => false,
						'class' => 'form-control'
					)
				));
			?>
			<fieldset>
				<?php echo $this->Form->input('username', array('label' => 'アカウント')); ?>
				<?php echo $this->Form->input('password', array('label' => 'パスワード')); ?>
				<?php echo $this->Form->input('remember_me', array('type' => 'checkbox', 'label' => 'ログイン状態を保存', 'div' => false, 'class' => false)); ?>
				<?php echo $this->Form->hidden('User.return_to', array('value' => $return_to)); ?>
				<?php echo $this->Form->submit('ログイン', array('class' => 'btn btn-primary btn-block', 'div' => 'form-group')); ?>
			</fieldset>
			<?php echo $this->Form->end(); ?>
		</div>
	</div>
</div>