<div class="row">
	<div class="col-xs-12 contents-actions">
		<ul class="nav nav-tabs">
			<li><a href="<?php echo $this->Html->url('/admin/users'); ?>">管理者一覧</a></li>
			<li class="active"><a href="#">管理者の更新</a></li>
			<li><a href="<?php echo $this->Html->url('/admin/users/change_password'); ?>">パスワード変更</a></li>
		</ul>
	</div>
</div>

<div class="row">
	<div class="col-xs-4 col-xs-offset-4">

		<div class="panel panel-default">
			<div class="panel-heading">管理者の更新</div>
			<?php
				echo $this->Form->create($model, array(
					'inputDefaults' => array(
						'label' => false,
						'wrapInput' => false,
						'div' => false,
						'class' => 'form-control'
					),
				));
				echo $this->Form->input('id');
			?>
			<div class="panel-body">
				<div class="form-group">
					<?php echo $this->Form->input('name', array('label' => '管理者名')); ?>
					<span class="help-block">管理内で使用される表示名です。</span>
				</div>

				<div class="form-group">
					<?php echo $this->data['User']['username']; ?>
					<span class="help-block">アカウント名は変更できません</span>
				</div>

				<div class="form-group">
					<?php echo $this->Form->input('email', array('label' => 'メールアドレス <span class="req">*</span>')); ?>
				</div>

				<?php if ($currentUser['is_admin'] || $currentUser['id'] == 1) : ?>
				<div class="form-group">
					<?php echo $this->Form->input('password', array('type' => 'password', 'label' => 'パスワード', 'required' => false)); ?>
					<span class="help-block">変更しない場合は空欄にしておきます。</span>
				</div>

				<div class="form-group">
					<?php echo $this->Form->input('temppassword', array('type' => 'password', 'label' => 'パスワード再入力', 'required' => false)); ?>
					<span class="help-block">変更しない場合は空欄にしておきます。</span>
				</div>


				<?php if ($this->data['User']['id'] != 1) : ?>

					<div class="form-group">
						<?php echo $this->Form->input('allow_edit_customer', array('type' => 'checkbox', 'label' => '顧客編集権限チェック', 'class' => false)); ?>
						<span class="help-block">顧客の編集権限を付与するにはチェックしてください。</span>
					</div>

				<div class="form-group">
					<?php echo $this->Form->input('is_admin', array('type' => 'checkbox', 'label' => '管理者権限', 'class' => false)); ?>
					<span class="help-block">サイトの管理者権限を付与するにはチェックしてください。</span>
				</div>
				<?php endif; ?>

				<div class="form-group">
					<?php echo $this->Form->input('active', array('type' => 'checkbox', 'label' => 'アカウント有効化', 'class' => false)); ?>
					<span class="help-block">アカウントを有効にするにはチェックしてください。</span>
				</div>
				<?php endif; ?>

			</div>
			<div class="panel-footer">
				<?php echo $this->Form->submit('管理者を更新する', array('div' => false, 'class' => 'btn btn-primary')); ?>
			</div>
			<?php echo $this->Form->end(); ?>
		</div>

	</div>
</div>
